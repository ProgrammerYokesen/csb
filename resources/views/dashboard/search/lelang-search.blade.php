<div class="row justify-content-center">
@forelse ($riwayatLelang as $rl)
<div class="col-lg-4 col-sm-4 col-6 lelang_history_item">
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <img src="https://data.sobatbadak.club/{{ ($rl->photo) }}" alt="">
        </div>
        <div class="card-body">
            <h5 class="mb-3">{{ $rl->name }}</h5>

            <div class="lelang_history_summary">
                <div class="row">
                    <div class="col-lg-6">
                        <span>Bid Tertinggi</span>
                        <p>{{ number_format($rl->bidTertinggi) }}</p>
                    </div>
                    <div class="col-lg-6">
                        <span>Pemenang</span>
                        <p>{{ $rl->username }}</p>
                    </div>
                </div>
            </div>

            <div class="lelang_history_summary">
                <span>Tanggal dan waktu</span>
                <p>{{ date('d M Y, H:i', strtotime($rl->starttime)) }}</p>
            </div>
        </div>
    </div>
</div>
@empty
<div class="text-cente">Hasil tidak ditemukan</div>
@endforelse
</div>