<div class="row justify-content-center">
    @forelse ($riwayatLelang as $ar)

        <!--======================================-->
        <!--begin::Col-->
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
            <!--begin::Card-->
            <div class="card card-custom gutter-b auction_upcoming_card">
                <!--begin::Body-->
                <div class="card-body">

                    <img src="https://data.sobatbadak.club/{{ $ar->photo }}" alt="img">

                    <!--begin::User-->
                    <div class="auction_upcoming_desc">
                        <!--begin::Pic-->
                        <div class="d-flex align-items-center mb-3">
                            <!--begin::Title-->
                            <div class="d-flex flex-column">
                                <h5 class="mb-0">{{ $ar->name }}</h5>
                            </div>
                            <!--end::Title-->
                        </div>
                        <!--end::User-->
                        <!--begin::Desc-->
                        <div class="d-flex align-items-center mb-3">
                            <div class="mr-10">
                                <span>Bid Tertinggi</span>
                                <h5>{{ number_format($ar->bidTertinggi, 0, ',', '.') }}</h5>
                            </div>
                            <div>
                                <span>Pemenang</span>
                                <h5>{{ $ar->username }}</h5>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="mr-10">
                                <span>Tanggal dan waktu</span>
                                <h5>{{ date('d M Y, H:i', strtotime($ar->starttime)) }}</h5>
                            </div>
                        </div>
                        <!--end::Desc-->
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Col-->
        <!--======================================-->

    @empty
        <div class="text-cente">Hasil tidak ditemukan</div>
    @endforelse
    {{-- endfor --}}
</div>
