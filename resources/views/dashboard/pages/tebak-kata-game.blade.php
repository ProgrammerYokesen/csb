@section('title')
    Quiz Tebak Kata
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom gutter-b quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body">


                        <div class="quiz_progress">
                            <span>{{ $datas->currentPage() }}/{{ $datas->lastPage() }} Pertanyaan</span>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{ $percentage }}%"
                                    aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>

                        <h1>{{ $datas[0]->deskripsi }}</h1>

                        <div class="quiz_answerbox">
                            @foreach ($kata as $huruf)
                                @if ($tebakan)
                                    @if (in_array($huruf, $tebakan))
                                        <div class="quiz_letter">{{ $huruf }}</div>
                                    @else
                                        <div class="quiz_letter"></div>
                                    @endif
                                @else
                                    <div class="quiz_letter"> </div>
                                @endif
                            @endforeach
                        </div>
                        @if (Session::get('nilai') != 1 && Session::get('salah') != 3)
                            <h5>Input huruf disini</h5>
                            <div class="quiz_inputbox">
                                <form action="{{ route('submitSatu', $datas[0]->pertanyaan_id) }}" method="post"
                                    id="hangman">
                                    @csrf
                                    <input type="text" name="huruf" maxlength="1" autocomplete="off" id="attemptInput">
                                    <button type="submit" class="btn">Enter</button>
                                </form>
                            </div>
                        @endif


                        @if (Session::get('nilai') == 1 || Session::get('salah') == 3)

                            {{-- Jika Jawaban Salah --}}
                            @if (Session::get('salah') == 3)
                                <div class="text-center quiz_jawaban">
                                    <h3>Jawaban: {{ $datas[0]->kata }}</h3>
                                </div>
                            @endif
                        @else
                            {{-- <div class="quiz_wrong">
                                <h6>Kesempatan Salah</h6>
                                <div class="quiz_attempt">

                                    @php
                                        $salah = [];
                                    @endphp

                                    @if (!empty(Session::get('tebak')))

                                        @foreach (Session::get('tebak') as $tebak)
                                            @if (!in_array($tebak, $kata))

                                                @php
                                                    array_push($salah, $tebak);
                                                @endphp

                                            @endif
                                        @endforeach
                                    @endif

                                </div>
                            </div> --}}

                            <div class="quiz_wrong">
                                <h6>Kesempatan Salah</h6>
                                <div class="quiz_attempt">
                                    @php
                                        $salah = [];
                                    @endphp

                                    @if (!empty(Session::get('tebak')))
                                        @foreach (Session::get('tebak') as $tebak)
                                            @if (!in_array($tebak, $kata))
                                                @php
                                                    array_push($salah, $tebak);
                                                @endphp
                                            @endif
                                        @endforeach
                                    @endif

                                    @if ($salah[0])
                                        <div class="quiz_attempt_wrong">
                                            <img src="{{ asset('images/icons/wrong.svg') }}" alt="" style="width: 100%">
                                            <h1>{{ $salah[0] }}</h1>
                                        </div>
                                    @else
                                        <div class="quiz_attempt_wrong"></div>
                                    @endif

                                    @if ($salah[1])
                                        <div class="quiz_attempt_wrong">
                                            <img src="{{ asset('images/icons/wrong.svg') }}" alt="" style="width: 100%">
                                            <h1>{{ $salah[1] }}</h1>
                                        </div>
                                    @else
                                        <div class="quiz_attempt_wrong"></div>
                                    @endif

                                    @if ($salah[2])
                                        <div class="quiz_attempt_wrong">
                                            <img src="{{ asset('images/icons/wrong.svg') }}" alt="" style="width: 100%">
                                            <h1>{{ $salah[2] }}</h1>
                                        </div>
                                    @else
                                        <div class="quiz_attempt_wrong"></div>
                                    @endif

                                </div>


                        @endif


                        @if (Session::get('nilai') == 1 || Session::get('salah') == 3)

                            {{-- Cek Pertanyaan Terakhir / BUkan --}}
                            @if ($datas->currentPage() != $datas->lastPage())
                                {{-- Jika Belum Pertanyaan Terakhir --}}
                                @if (Session::get('nilai') == 1)
                                    <div class="quiz_done_word">
                                        <h5 class="mt-5 win">Selamat! Jawaban kamu benar.</h5>
                                    </div>
                                @elseif (Session::get('salah') == 3)
                                    <div class="quiz_done_word">
                                        <h5 class="mt-5 lose">Maaf, kamu gagal menjawab pertanyaan ini.</h5>
                                    </div>
                                @endif
                                <div class="quiz_done">
                                    <a href="{{ route('nextQuiz', $datas[0]->pertanyaan_id) }}"><button
                                            class="btn">Pertanyaan Selanjutnya</button></a>
                                </div>
                            @else
                                {{-- Jika Pertanyaan Terakhir --}}
                                @if (Session::get('nilai') == 1)
                                    <div class="quiz_done_word">
                                        <h5 class="mt-5 win">Selamat! Jawaban kamu benar.</h5>
                                    </div>
                                @elseif (Session::get('salah') == 3)
                                    <div class="quiz_done_word">
                                        <h5 class="mt-5 lose">Maaf, kamu gagal menjawab pertanyaan ini.</h5>
                                    </div>
                                @endif
                                <div class="quiz_done">
                                    <a href="{{ route('nextQuiz', $datas[0]->pertanyaan_id) }}"><button
                                            class="btn">Selesai</button></a>
                                </div>
                            @endif
                        @endif

                        {{-- MODAL POP UP ANIMATION --}}
                        <div class="modal fade" id="failModal" data-backdrop="static" tabindex="-1" role="dialog"
                            aria-labelledby="staticBackdrop" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="d-flex justify-content-between">
                                            <h2 class="modal-title" style="font-weight: bold">Oooppss !</h2>
                                            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button> --}}
                                        </div>
                                        <img src="{{ asset('images/getol-boleng-wkwkw.gif') }}" alt=""
                                            style="width:100%">
                                        <div class="text-center quiz_jawaban">
                                            <h5>Wah sepertinya kamu masih perlu belajar hahaha</h5>
                                        </div>
                                        <div class="text-center quiz_jawaban">
                                            <h6 style="color: #50cd89">Jawaban benar: {{ $datas[0]->kata }}</h6>
                                        </div>
                                        <div class="d-flex mt-6 justify-content-center">
                                            {{-- <button type="button" class="btn btn-light-primary font-weight-bold"
                                                    data-dismiss="modal">Tutup</button> --}}

                                            <div onclick="linkClicked('{{ route('nextQuiz', $datas[0]->pertanyaan_id) }}')"
                                                class="btn custom-btn-primary-rounded font-weight-bold">
                                                @if ($datas->currentPage() != $datas->lastPage())
                                                    Selanjutnya
                                                @else
                                                    Selesai
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="successModal" data-backdrop="static" tabindex="-1" role="dialog"
                            aria-labelledby="staticBackdrop" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="d-flex justify-content-between">
                                            <h2 class="modal-title" style="font-weight: bold">Selamat !</h2>
                                            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button> --}}
                                        </div>
                                        <img src="{{ asset('images/getol-boleng-horee.gif') }}" alt=""
                                            style="width:100%">
                                        <div class="text-center quiz_jawaban">
                                            <h5>Waw, kamu menjawab dengan tepat!</h5>
                                        </div>
                                        <div class="d-flex mt-6 justify-content-center">
                                            {{-- <button type="button" class="btn btn-light-primary font-weight-bold"
                                                    data-dismiss="modal">Tutup</button> --}}
                                            {{-- <a href="{{ route('nextQuiz', $datas[0]->pertanyaan_id) }}"
                                                class="btn custom-btn-primary-rounded font-weight-bold">
                                                @if ($datas->currentPage() != $datas->lastPage())
                                                    Selanjutnya
                                                @else
                                                    Selesai
                                                @endif
                                            </a> --}}
                                            <div onclick="linkClicked('{{ route('nextQuiz', $datas[0]->pertanyaan_id) }}')"
                                                class="btn custom-btn-primary-rounded font-weight-bold">
                                                @if ($datas->currentPage() != $datas->lastPage())
                                                    Selanjutnya
                                                @else
                                                    Selesai
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (Session::get('salah') == 3)
                            <audio controls autoplay hidden>
                                <source src="/sounds/laugh.mp3" type="audio/mp3">
                            </audio>
                            {{-- <embed src="/sounds/laugh.mp3" autostart="true" style="display: none"> --}}
                            <script>
                                $(function() {
                                    $("#failModal").modal('show');
                                })
                            </script>
                        @elseif(Session::get('nilai') == 1)
                            <audio controls autoplay hidden>
                                <source src="/sounds/cheering.mp3" type="audio/mp3">
                            </audio>
                            <script>
                                $(function() {
                                    $("#successModal").modal('show');
                                })
                            </script>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function linkClicked(url) {
            // Check to make sure we got a url, otherwise we can't do anything
            if (url) {
                // Use window.location.href to navigate to that url
                window.location.href = url;
            }
        }
    </script>
@endsection

@section('pageJS')
    <script>
        // Focus On Input When Page Load
        window.onload = function() {
            var input = document.getElementById("attemptInput").focus();
        }
    </script>
@endsection
