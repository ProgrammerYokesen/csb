@foreach($riwayatLelang as $rl)
<!--begin::Col-->
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
    <!--begin::Card-->
    <div class="card card-custom gutter-b auction_upcoming_card">
        <!--begin::Body-->
        <div class="card-body">

            <!--<img src="{{ asset($rl->photo) }}" alt="img">-->
            <img loading=”lazy” src="https://data.sobatbadak.club/{{ $rl->photo }}" alt="img">

            <!--begin::User-->
            <div class="auction_upcoming_desc">
                <!--begin::Pic-->
                <div class="d-flex align-items-center mb-3">
                    <!--begin::Title-->
                    <div class="d-flex flex-column">
                        <h5 class="mb-0">{{ $rl->name }}</h5>
                    </div>
                    <!--end::Title-->
                </div>
                <!--end::User-->
                <!--begin::Desc-->
                <div class="d-flex align-items-center mb-3">
                    <div class="mr-10">
                        <span>Bid Tertinggi</span>
                        <h5>{{ number_format($rl->bidTertinggi, 0, ',', '.') }}</h5>
                    </div>
                    <div>
                        <span>Pemenang</span>
                        <h5>{{ $rl->username }}</h5>
                    </div>
                </div>

                <div class="d-flex align-items-center">
                    <div class="mr-10">
                        <span>Tanggal dan waktu</span>
                        <h5>{{ date('d M Y', strtotime($rl->starttime)) }},
                            {{ date('H:i', strtotime($rl->starttime)) }}</h5>
                    </div>
                </div>
                <!--end::Desc-->
            </div>
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
</div>
<!--end::Col-->
@endforeach