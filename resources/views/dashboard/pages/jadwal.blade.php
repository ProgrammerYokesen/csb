@section('title')
    Jadwal
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom gutter-b teman_sobat_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body jadwal_tabs">

                        <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                            <!--<li class="nav-item">-->
                            <!--    <a class="nav-link active" id="jadwal-csb" data-toggle="tab" href="#csb">-->
                            <!--        <span class="nav-text">Jadwal CSB</span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item">-->
                            <!--    <a class="nav-link" id="jadwal-ombadak" data-toggle="tab" href="#ombadak"-->
                            <!--        aria-controls="profile">-->
                            <!--        <span class="nav-text">Jadwal Om Badak</span>-->
                            <!--    </a>-->
                            <!--</li>-->
                        </ul>

                        <div class="tab-content mt-5" id="myTabContent1">
                            <div class="tab-pane fade show active" id="csb" role="tabpanel" aria-labelledby="jadwal-csb">

                                <div class="jadwal_csb">
                                    <h1>Jadwal Minggu Ini</h1>

                                    <ul class="nav nav-pills nav-fill" id="myTab1" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="lounge-tab" data-toggle="tab" href="#lounge">
                                                <span class="nav-text">Ngopi di Lounge</span>
                                            </a>
                                        </li>
                                        <!--<li class="nav-item">-->
                                        <!--    <a class="nav-link" id="ngopi-tab" data-toggle="tab" href="#ngopi"-->
                                        <!--        aria-controls="profile">-->
                                        <!--        <span class="nav-text">Ngopi</span>-->
                                        <!--    </a>-->
                                        <!--</li>-->
                                        <li class="nav-item">
                                            <a class="nav-link" id="wgm-tab" data-toggle="tab" href="#wgm"
                                                aria-controls="contact">
                                                <span class="nav-text">WGM Live</span>
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content mt-5" id="myTabContent1">
                                        <div class="tab-pane fade show active" id="lounge" role="tabpanel"
                                            aria-labelledby="lounge-tab">

                                            <div class="jadwal_table">
                                                <table class="table table-borderless mb-6 text-left">
                                                    <thead>
                                                        @if (count($jadwalLounge) > 0)
                                                            <tr>
                                                                <th scope="col">Waktu</th>
                                                                <th scope="col">Nama Acara</th>
                                                                <th scope="col">Host</th>
                                                            </tr>
                                                        @endif
                                                    </thead>
                                                    <tbody>
                                                        @forelse ($jadwalLounge as $jl)
                                                            <tr>
                                                                <td>{{ $jl->jam_mulai }} - {{ $jl->jam_selesai }}</td>
                                                                <td>{{ $jl->nama_acara }}</td>
                                                                <td>
                                                                    {{ $jl->host }}
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            <div class="text-center">
                                                                <img src="{{ asset('images/badak-baper.png') }}" alt=""
                                                                    style="width: 200px; margin-bottom: 15px">
                                                                <h6>Belum ada jadwal</h6>
                                                            </div>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="ngopi" role="tabpanel" aria-labelledby="ngopi-tab">
                                            <div class="jadwal_table">
                                                <table class="table table-borderless mb-6 text-left">
                                                    <thead>
                                                        @if (count($jadwalNgopi) > 0)
                                                            <tr>
                                                                <th scope="col">Waktu</th>
                                                                <th scope="col">Nama Acara</th>
                                                                <th scope="col">Host</th>
                                                            </tr>
                                                        @endif
                                                    </thead>
                                                    <tbody>

                                                        @forelse ($jadwalNgopi as $jl)
                                                            <tr>
                                                                <td>{{ $jl->jam_mulai }} - {{ $jl->jam_selesai }}</td>
                                                                <td>{{ $jl->nama_acara }}</td>
                                                                <td>
                                                                    {{ $jl->host }}
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            <div class="text-center">
                                                                <img src="{{ asset('images/badak-baper.png') }}" alt=""
                                                                    style="width: 200px; margin-bottom: 15px">
                                                                <h6>Belum ada jadwal</h6>
                                                            </div>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="wgm" role="tabpanel" aria-labelledby="wgm-tab">
                                            <div class="jadwal_table">
                                                <table class="table table-borderless mb-6 text-left">
                                                    <thead>
                                                        @if (count($jadwalWgmlive) > 0)
                                                            <tr>
                                                                <th scope="col">Waktu</th>
                                                                <th scope="col">Nama Acara</th>
                                                                <th scope="col">Host</th>
                                                            </tr>
                                                        @endif
                                                    </thead>
                                                    <tbody>
                                                        @forelse ($jadwalWgmlive as $jl)
                                                            <tr>
                                                                <td>{{ $jl->jam_mulai }} - {{ $jl->jam_selesai }}</td>
                                                                <td>{{ $jl->nama_acara }}</td>
                                                                <td>
                                                                    {{ $jl->host }}
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            <div class="text-center">
                                                                <img src="{{ asset('images/badak-baper.png') }}" alt=""
                                                                    style="width: 200px; margin-bottom: 15px">
                                                                <h6>Belum ada jadwal</h6>
                                                            </div>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="ombadak" role="tabpanel" aria-labelledby="jadwal-ombadak">

                                <h1>Jadwal Om Badak</h1>

                                @php
                                    $date = date('d-M-Y');
                                    $now = strtotime($date);
                                @endphp

                                <div class="row justify-content-center align-items-center" style="margin-top: 50px">

                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">Rabu, 4 Agustus</h3>
                                                    <h3>19.00 - 21.00</h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Mie Rebus vs Mie Goreng</h3>
                                                {{-- <h5>Kamu tim Mie Rebus atau Mie Goreng?</h5> --}}

                                                @if ($now > strtotime('04-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('04-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif
                                                {{-- <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                target="_blank">
                                                <button class="btn" {{ $now != strtotime('01-07-2021') ? 'disabled' : '' }}>Join
                                                    Sekarang</button>
                                            </a> --}}
                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        Jumat, 6 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Pamali</h3>
                                                {{-- <h5>Siapa disini yang percaya pamali?</h5> --}}

                                                @if ($now > strtotime('06-08-2021'))
                                                    <a href="javascript:void(0)">
                                                        <button class="btn expired" disabled>Expired</button>
                                                    </a>
                                                @elseif ($now != strtotime('06-08-2021'))
                                                    <a href="javascript:void(0)">
                                                        <button class="btn" disabled>Join
                                                            Sekarang</button>
                                                    </a>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        senin, 9 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Tetangga Kepo</h3>
                                                {{-- <h5>Kita bahas tentang tetangga yang suka kepo disini</h5> --}}

                                                @if ($now > strtotime('09-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('09-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        selasa, 10 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Pasangan Romantis vs Berkarir</h3>
                                                {{-- <h5>Tips bagi yang punya pasangan</h5> --}}

                                                @if ($now > strtotime('10-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('10-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        kamis, 12 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Perempuan Selalu Benar</h3>
                                                {{-- <h5>Kenapa perempuan selalu benar?</h5> --}}

                                                @if ($now > strtotime('12-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('12-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif


                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        jum'at, 13 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Forex untuk Pemula</h3>
                                                {{-- <h5>Yang mau belajar forex yuk!</h5> --}}

                                                @if ($now > strtotime('13-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('13-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        senin, 16 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Gunung vs Pantai</h3>
                                                {{-- <h5>Liburan ke Gunung atau ke Pantai</h5> --}}

                                                @if ($now > strtotime('16-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('16-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        rabu, 18 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Trend Ikan Cupang</h3>
                                                {{-- <h5>Bahas tentang trend ikan cupang</h5> --}}

                                                @if ($now > strtotime('18-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('18-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        Jum'at, 20 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Agustus Waktu Kecil</h3>
                                                {{-- <h5>Agustusan jaman dulu</h5> --}}

                                                @if ($now > strtotime('20-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('20-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        senin, 23 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Android vs iPhone</h3>
                                                {{-- <h5>kamu Tim Android / iPhone?</h5> --}}

                                                @if ($now > strtotime('23-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('23-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        rabu, 25 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Masak Yuk</h3>
                                                {{-- <h5>Belajar masak yuuuk</h5> --}}

                                                @if ($now > strtotime('25-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('25-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}
                                    {{-- Begin Item --}}
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 style="text-transform: uppercase">
                                                        jum'at, 27 Agustus
                                                    </h3>
                                                    <h3>19.00 - 21.00</h3>

                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h3>Prediksi Masa Depan</h3>
                                                {{-- <h5>Bisa gak kita prediksi masa depan?</h5> --}}

                                                @if ($now > strtotime('27-08-2021'))
                                                    <button class="btn expired" disabled>Expired</button>
                                                @elseif ($now != strtotime('27-08-2021'))
                                                    <button class="btn" disabled>Join
                                                        Sekarang</button>
                                                @else
                                                    <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                                        target="_blank">
                                                        <button class="btn">Join
                                                            Sekarang</button>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{-- End Item --}}

                                </div>

                            </div>
                        </div>







                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
