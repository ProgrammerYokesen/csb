@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom gutter-b quiz_win_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h2>Selamat!</h2>
                        <h5>Kamu berhasil menjawab {{ $answer }} pertanyaan dengan benar dari {{ $question }}
                            pertanyaan. <br> Nih Badak Baper kasih
                            kamu
                            Baper Poin:</h5>

                        <h1>{{ $poin }} Baper Poin</h1>

                        {{-- @php
                            dd($answer);
                        @endphp --}}


                        @if ($quizSobat == true)
                            <a href="{{ route('rejekiSobatAns') }}"><button class="btn answer">Lihat Jawaban</button></a>
                        @endif

                        <a href="{{ route('homeDash') }}"><button class="btn">Selesai</button></a>
                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
