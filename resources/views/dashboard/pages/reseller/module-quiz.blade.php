@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container">

            <div class="reseller__modul_quiz">
                <div class="card">
                    <div class="card-body">
                        <div class="academy__quiz">
                            <span>Pertanyaan [1]/[5]</span>

                            {{-- @php
                                $index = ($qs + 1) / count($questions);
                                $progress = $index * 100;
                            @endphp --}}

                            <div class="academy__progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 70%" aria-valuenow="25"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>


                            <h3>[Pernah Gak Ready?]</h3>

                            <div class="academy__ans">
                                {{-- <div class="row"> --}}

                                <label style="width: 100%">
                                    <input type="radio" name="qJawaban[1]" value="a" class="card-input-element"
                                        onclick="clickJawab(1)" />
                                    <div class="card card-input">
                                        <div class="card-body">
                                            {{-- <img src="{{ asset('images/reseller/icons/radio.png') }}" alt=""
                                                class="mr-5"> --}}
                                            [Jawaban A]
                                        </div>
                                    </div>

                                </label>
                                <label style="width: 100%">
                                    <input type="radio" name="qJawaban[1]" value="b" class="card-input-element"
                                        onclick="clickJawab(2)" />
                                    <div class="card card-input">
                                        <div class="card-body">
                                            {{-- <img src="{{ asset('images/reseller/icons/radio.png') }}" alt=""
                                                class="mr-5"> --}}
                                            [Jawaban B]
                                        </div>
                                    </div>

                                </label>
                                <label style="width: 100%">
                                    <input type="radio" name="qJawaban[1]" value="c" class="card-input-element"
                                        onclick="clickJawab(3)" />
                                    <div class="card card-input">
                                        <div class="card-body">
                                            {{-- <img src="{{ asset('images/reseller/icons/radio.png') }}" alt=""
                                                class="mr-5"> --}}
                                            [Jawaban C]
                                        </div>
                                    </div>

                                </label>
                                <label style="width: 100%">
                                    <input type="radio" name="qJawaban[1]" value="d" class="card-input-element"
                                        onclick="clickJawab(4)" />
                                    <div class="card card-input">
                                        <div class="card-body">
                                            {{-- <img src="{{ asset('images/reseller/icons/radio.png') }}" alt=""
                                                class="mr-5"> --}}
                                            [Jawaban D]
                                        </div>
                                    </div>

                                </label>

                                {{-- </div> --}}


                            </div>

                            <div class="academy__quiz_button">
                                @if ($qs + 1 == count($questions))
                                    <button type="submit" class="btn" id="button{{ $qs + 1 }}"
                                        disabled>Finish</button>
                                @else
                                    <button type="button" class="btn" id="button1" onclick="nextQuestion(1)"
                                        disabled>Lanjut</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>


@endsection


@section('pageJS')
    <script>
        function nextQuestion(num) {
            console.log(num)
            $(`#soal${num}`).addClass('d-none')
            $(`#soal${num + 1}`).removeClass('d-none')
        }

        function clickJawab(idnum) {
            console.log('click ')
            $(`#button${idnum}`).prop('disabled', false);
        }
    </script>

@endsection
