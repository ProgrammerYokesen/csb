@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container reseller__home">
            <div class="card card-custom gutter-b reseller__head_card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8">
                            <h1>CUAN Jutaan Rupiah cukup jadi Reseller!</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non id purus quam nunc nisl
                                tempus.
                                Habitasse nisl non ultricies non turpis morbi platea.</p>
                            <button class="btn">Tutorial</button>
                        </div>


                    </div>
                    <img src="{{ asset('images/reseller/head-orn.png') }}" alt="" class="reseller__head_orn">
                    <img src="{{ asset('images/reseller/head-img.svg') }}" alt="" class="reseller__head_img">
                </div>
            </div>


            <div class="row">
                <div class="col-xl-4">
                    <div class="card card-custom gutter-b dash_home_card_header">
                        <div class="card-header dash_header_red">
                            <div class="card-title">
                                <h3 class="card-label">
                                    Jumlah Pembelian Bulan Ini
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            12 Karton
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="card card-custom gutter-b dash_home_card_header">
                        <div class="card-header dash_header_blue">
                            <div class="card-title">
                                <h3 class="card-label">
                                    Persentase Pembelian Bulan Ini
                                </h3>
                            </div>
                        </div>
                        <div class="card-body" style="color: #EF003A">
                            <i class="fas fa-caret-down" style="font-size: 48px; line-height: 72px;color: #EF003A"></i> 15%
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="card card-custom gutter-b dash_home_card_header">
                        <div class="card-header dash_header_yellow">
                            <div class="card-title">
                                <h3 class="card-label">
                                    Peringkatmu Bulan Ini
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            103
                        </div>
                    </div>
                </div>
            </div>

            <div class="reseller__home_progress">
                <div class="progress" style="height: 25px;">
                    <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0"
                        aria-valuemax="100"></div>
                </div>

                <div class="progress_5">5</div>
                <div class="progress_10">10</div>
                <div class="progress_20">20</div>
                <div class="progress_50">50</div>
                <div class="progress_100">100</div>
            </div>
        </div>
        <!--end::Container-->
    </div>


@endsection
