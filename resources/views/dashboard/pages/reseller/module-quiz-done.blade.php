@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container">

            <div class="reseller__modul_quiz_done">
                <div class="card">
                    <div class="card-body winn">
                        <div class="row">
                            <div class="col-lg-6 d-flex align-items-center justify-content-center">
                                <img src="{{ asset('images/reseller/win-stempel.png') }}" alt="">
                            </div>
                            <div class="col-lg-6">
                                <h6>Skor kamu:</h6>
                                <div class="d-flex">
                                    <div class="reseller__modul_score">
                                        80
                                    </div>
                                </div>
                                <h1>Selamat!</h1>
                                <p>Kamu memperoleh 1 buah stampel!
                                    Kumpulkan hingga 10 stampel, untuk memenangkan voucher diskon menarik!</p>

                                <div class="d-flex align-items-center justify-space-between">
                                    <button class="btn" style="background: #FFAA3A; color: #ffffff">Lihat Kartu
                                        Stempel</button>
                                    <button class="btn"
                                        style="background: #ffffff; color: #FFAA3A">Lanjut</button>
                                </div>
                            </div>
                        </div>

                        <img src="{{ asset('images/reseller/winner.png') }}" alt=""
                            class="reseller__quiz_winner img-fluid">
                    </div>
                </div>

                <div class="card">
                    <div class="card-body" style="position: relative">
                        <div class="row">
                            <div class="col-lg-6 d-flex align-items-center justify-content-center">
                                {{-- <img src="{{ asset('images/reseller/win-stempel.png') }}" alt=""> --}}
                            </div>
                            <div class="col-lg-6">
                                <h6>Skor kamu:</h6>
                                <div class="d-flex">
                                    <div class="reseller__modul_score">
                                        60
                                    </div>
                                </div>
                                <h1>Maaf Kamu Gagal..</h1>
                                <p>Jangan Kahwatir, kamu dapat mengulangi tesnya kembali esok hari ya!
                                    Tetap semangat!</p>

                                <div class="d-flex align-items-center justify-space-between">
                                    <button class="btn" style="background: #FFAA3A; color: #ffffff">Lihat Kartu
                                        Stempel</button>
                                    <button class="btn"
                                        style="background: #ffffff; color: #FFAA3A">Lanjut</button>
                                </div>
                            </div>
                        </div>

                        {{-- <img src="{{ asset('images/reseller/winner.png') }}" alt=""
                            class="reseller__quiz_winner img-fluid"> --}}
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>


@endsection
