@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container reseller__stempel">
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <h1>Stempel yang kamu miliki</h1>

                    <div class="reseller__stempel_card">
                        <h3>Kumpulin Stempelnya!</h3>
                        <p>Lorem ipsum dolor set amet</p>

                        <div class="row justify-content-center align-items-center mb-5">
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp">
                                    <img src="{{ asset('images/reseller/stempel.png') }}" alt="" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp">
                                    <img src="{{ asset('images/reseller/stempel.png') }}" alt="" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp">
                                    <img src="{{ asset('images/reseller/stempel.png') }}" alt="" class="img-fluid">

                                </div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp">
                                    <img src="{{ asset('images/reseller/stempel.png') }}" alt="" class="img-fluid">

                                </div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp"></div>
                            </div>

                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp"></div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp"></div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp"></div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp"></div>
                            </div>
                            <div class="col-lg-2 col-2">
                                <div class="reseller__stamp">
                                    HADIAH!
                                </div>
                            </div>

                        </div>


                        <img src="{{ asset('images/reseller/stempel-bg.png') }}" alt="" class="reseller__stempel_bg">

                    </div>

                    <div class="reseller__stempel_progress">
                        <div class="d-flex justify-content-between align-items-center">
                            <p>Kumpulkan Stampelmu!</p>
                            <p>80%</p>
                        </div>
                        <div class="progress my-3">
                            <div class="progress-bar reseller__stempel_progress_bar" role="progressbar" style="width: 80%"
                                aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span>Kerjakan sisa modulmu dengan benar, dan menangkan hadiah voucher!</span>

                        <button class="btn">Selesaikan Modulmu!</button>
                    </div>
                </div>
            </div>



            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>


@endsection
