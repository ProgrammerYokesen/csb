@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container">

            <div class="reseller__modul_detail">
                <div class="card">
                    <div class="card-body">
                        {{-- <div class="container-fluid"> --}}
                        <div class="row">
                            <div class="col-lg-8">
                                {{-- <div class="reseller__modul_video">
                                    <div class="reseller__play_btn">
                                        <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                    </div>
                                </div> --}}
                                <iframe width="100%" height="500" src="https://www.youtube.com/embed/QW1EpaUnCJ0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                            </div>
                            <div class="col-lg-4 reseller__modul_desc">
                                <div>
                                    <h5>Lorem, ipsum dolor.</h5>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui ipsa porro
                                        voluptatum
                                        itaque
                                        modi
                                        ullam?</p>
                                </div>

                                <div>
                                    <a href="#">
                                        <button class="btn">Post Quiz</button>
                                    </a>
                                </div>

                            </div>
                        </div>
                        {{-- </div> --}}
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>


@endsection
