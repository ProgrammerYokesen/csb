@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container reseller__riwayat">

            <ul class="nav nav-pills reseller__riwayat_tabs" id="myTab1" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab-1" data-toggle="tab" href="#home-1">
                        <span class="nav-text">Riwayat Pesanan</span>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" id="profile-tab-1" data-toggle="tab" href="#profile-1" aria-controls="profile">
                        <span class="nav-text">Riwayat Hadiah</span>
                    </a>
                </li> --}}
            </ul>
            <div class="tab-content mt-5" id="myTabContent1">
                <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab-1">
                    <div class="card card-custom gutter-b">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Nama Produk</th>
                                            <th>Jumlah</th>
                                            <th>No Pesanan</th>
                                            <th class="text-right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left">LPCB Paket Jeruk</td>
                                            <td>3 Karton</td>
                                            <td>XXXXXX01092021</td>
                                            <td class="text-right">Rp XXXXXX</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">LPCB Paket Jeruk</td>
                                            <td>3 Karton</td>
                                            <td>XXXXXX01092021</td>
                                            <td class="text-right">Rp XXXXXX</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">LPCB Paket Jeruk</td>
                                            <td>3 Karton</td>
                                            <td>XXXXXX01092021</td>
                                            <td class="text-right">Rp XXXXXX</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">LPCB Paket Jeruk</td>
                                            <td>3 Karton</td>
                                            <td>XXXXXX01092021</td>
                                            <td class="text-right">Rp XXXXXX</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">LPCB Paket Jeruk</td>
                                            <td>3 Karton</td>
                                            <td>XXXXXX01092021</td>
                                            <td class="text-right">Rp XXXXXX</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">LPCB Paket Jeruk</td>
                                            <td>3 Karton</td>
                                            <td>XXXXXX01092021</td>
                                            <td class="text-right">Rp XXXXXX</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="reseller__hadiah_footer">
                        <span>Showing <strong>1-7</strong> from <strong>40</strong> data</span>

                        <div>Pagination</div>
                    </div>
                </div>
                {{-- <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab-1">
                    <div class="card card-custom gutter-b">
                        <div class="card-body">

                            <table class="table table-borderless reseller__riwayat_hadiah">
                                <thead>
                                    <tr>
                                        <th class="text-left">Nama Hadiah</th>
                                        <th>Bulan</th>
                                        <th>Nomor Hadiah</th>
                                        <th class="text-right">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left">Panci Baja Anti Karat</td>
                                        <td>Agustus</td>
                                        <td>XXXXXX01092021</td>
                                        <td class="text-right"><span class="onProgress">Dalam Proses</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Panci Baja Anti Karat</td>
                                        <td>Agustus</td>
                                        <td>XXXXXX01092021</td>
                                        <td class="text-right"><span class="notClaimed">Belum di Klaim</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Panci Baja Anti Karat</td>
                                        <td>Agustus</td>
                                        <td>XXXXXX01092021</td>
                                        <td class="text-right"><span class="delivered">Hadiah Diterima</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="reseller__hadiah_footer">
                        <span>Showing <strong>1-7</strong> from <strong>40</strong> data</span>

                        <div>Pagination</div>
                    </div>
                </div> --}}
            </div>





            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>


@endsection
