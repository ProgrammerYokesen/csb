@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container reseller__riwayat">

            <ul class="nav nav-pills reseller__riwayat_tabs" id="myTab1" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="tab-dasar" data-toggle="tab" href="#dasar">
                        <span class="nav-text">Dasar</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tab-menengah" data-toggle="tab" href="#menengah" aria-controls="menengah">
                        <span class="nav-text">Menengah</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tab-profesional" data-toggle="tab" href="#profesional"
                        aria-controls="profile">
                        <span class="nav-text">Profesional</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content mt-5" id="myTabContent1">
                <div class="tab-pane fade show active" id="dasar" role="tabpanel" aria-labelledby="home-tab-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="menengah" role="tabpanel" aria-labelledby="profile-tab-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profesional" role="tabpanel" aria-labelledby="profile-tab-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card reseller__modul_card">
                                <div class="card-body">
                                    <div class="reseller__modul_thumb">
                                        <div class="reseller__play_btn">
                                            <img src="{{ asset('images/reseller/icons/play.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5>Lorem ipsum dolor sit.</h5>
                                    <div class="reseller__modul_interaction">
                                        <div class="d-flex align-items-center mr-5">
                                            <img src="{{ asset('images/reseller/icons/view.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset('images/reseller/icons/like.svg') }}" alt=""
                                                class="mr-1">
                                            <span>123</span>
                                        </div>
                                    </div>
                                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>


@endsection
