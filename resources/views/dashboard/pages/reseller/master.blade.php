@section('title')
    Dashboard Reseller
@endsection

@extends('dashboard.master')

@section('dash-content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-12 subheader-transparent dash_home_header" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-center flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex flex-column">
                        <h2 class="text-dark font-weight-bold my-2 mr-5"
                            style="background: #fff; padding: 1rem; border-radius: 5px">Hi
                            <span class="font-weight-bolder" style="color: #ffaa3a">{{ Auth::user()->name }}</span>,
                            Selamat Datang
                            di Club Sobat Badak
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row justify-content-center dash_home_navigation">

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('homeReseller') }}">
                            <div class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/reseller/icons/home.svg') }}" alt="csb"
                                        class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Home</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('modulReseller') }}">
                            <div class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/reseller/icons/modul.svg') }}" alt="csb"
                                        class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Module</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('stempelReseller') }}">
                            <div class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/reseller/icons/stempel.svg') }}" alt="csb"
                                        class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Stampel</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('leaderboardReseller') }}">
                            <div class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/reseller/icons/leaderboard.svg') }}" alt="csb"
                                        class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Leaderboard</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('jadwalReseller') }}">
                            <div class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/reseller/icons/jadwal.svg') }}" alt="csb"
                                        class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Jadwal Workshop</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('riwayatReseller') }}">
                            <div class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/reseller/icons/riwayat.svg') }}" alt="csb"
                                        class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Riwayat</div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                {{-- BODY DISINI --}}
                @yield('reseller-contents')
            </div>
        </div>
    </div>
@endsection
