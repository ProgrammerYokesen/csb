@extends('dashboard.pages.reseller.master')

@section('reseller-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container reseller__leaderboard">
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <h1 class="reseller__leaderboard_title">
                        Peringkat Bulanan
                    </h1>
                    <p>Diperbaharui setiap tanggal 1 setiap bulannya</p>


                    <div class="reseller__leaderboard_rank">
                        <div class="mb-5">
                            <img src="{{ asset('images/reseller/rank2.png') }}" alt="">
                            <img src="{{ asset('images/reseller/rank1.png') }}" alt="">
                            <img src="{{ asset('images/reseller/rank3.png') }}" alt="">
                        </div>

                        <div>
                            <img src="{{ asset('images/reseller/rank4.png') }}" alt="">
                            <img src="{{ asset('images/reseller/rank5.png') }}" alt="">
                        </div>
                    </div>

                    <div class="reseller__leaderboard_card">
                        <div class="reseller__leaderboard_mine">
                            <div>
                                <h6>Peringkat Saya</h6>
                                <h1>103</h1>
                            </div>

                            <div>
                                <h6 style="color: #E78300;">Total Pesanan Bulan Ini</h6>
                                <h1 style="color: #E78300;">38/100</h1>
                            </div>
                        </div>

                        <div class="reseller__leaderboard_table">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th>Peringkat</th>
                                        <th>Username</th>
                                        <th>Total Pesanan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span>6</span></td>
                                        <td>Lorem, ipsum dolor.</td>
                                        <td>87/100</td>
                                    </tr>
                                    <tr>
                                        <td><span>7</span></td>
                                        <td>Lorem, ipsum dolor.</td>
                                        <td>87/100</td>
                                    </tr>
                                    <tr>
                                        <td><span>8</span></td>
                                        <td>Lorem, ipsum dolor.</td>
                                        <td>87/100</td>
                                    </tr>
                                    <tr>
                                        <td><span>9</span></td>
                                        <td>Lorem, ipsum dolor.</td>
                                        <td>87/100</td>
                                    </tr>
                                    <tr>
                                        <td><span>10</span></td>
                                        <td>Lorem, ipsum dolor.</td>
                                        <td>87/100</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>



            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>


@endsection
