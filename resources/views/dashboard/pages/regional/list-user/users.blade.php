@section('title')
  Pendaftaran Regional
@endsection

@extends('dashboard.pages.regional.master-regional')

@section('page_assets')
<style>
  .csb__regional .card-header {
    padding: 0;
  }

  .csb__regional .card-body {
    padding-top: 0 !important;
  }

  .csb__regional .card-body h2 {
    font-weight: 600;
    font-size: 24px;
    line-height: 36px;
    text-align: center;
    color: #000000;
    margin-bottom: 25px;
  }

  .csb__regional button {
    background: #ffaa3a;
    color: #ffffff;
    margin: 15px auto;
    padding: 0.7rem 3rem;
  }

  .csb__regional button:hover {
    color: #ffffff;
  }

  .csb__regional .card-body span {
    background: rgba(255, 170, 58, 0.3);
    border-radius: 15px;
    padding: 0.5rem 1rem;
    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    color: #FFAA3A;
  }

  .regional__benefit {
    background: #FFF7EC;
    border-radius: 9px;
    padding: 1rem;
    width: 80%;
    margin: 0 auto;
  }

  .regional__benefit h5 {
    font-weight: 600;
    font-size: 18px;
    line-height: 27px;
    text-align: center;
    color: #FFAA3A;
  }

  .regional__benefit p {
    font-weight: 600;
    font-size: 14px;
    line-height: 21px;
    color: rgba(0, 0, 0, 0.88);
  }

  .regional__benefit img {
    margin-bottom: 15px;
  }

  .regional__benefit button {
    font-weight: 700;
  }

  .regional__member {
    width: 80%;
    margin: 25px auto;
  }

  .regional__member h5 {
    font-weight: 600;
    font-size: 18px;
    line-height: 27px;
    text-align: center;
    color: #000000;
    margin: 0;
  }

  .regional__member h6 {
    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    text-align: center;

    color: rgba(0, 0, 0, 0.5);
  }

  .regional__member_nama {
    background: rgba(255, 170, 58, 0.1);
    border-radius: 8px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 1rem;
    margin-bottom: 10px;

    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    color: #000000;
  }

  .regional__member_nama img {
    margin-left: 5px;
  }

  .regional__comingsoon {
    background: #D10000;
    border-radius: 15px;
    font-weight: 500;
    font-size: 12px;
    line-height: 18px;
    color: #FFFFFF;
    padding: 5px;

    position: absolute;
    top: -10px;
    right: 5px;
    z-index: 3;
  }

  .modal_regional .modal-content {
    padding: 2rem;
  }

  .modal_regional .modal-content img {
    width: 150px;
    margin: 15px auto;
  }

  .modal_regional .modal-content h4 {
    text-align: center;
    font-weight: 600;
    margin-bottom: 25px
  }

  .modal_regional .modal-content p {
    text-align: center;
  }

  .modal_regional .modal-content button {
    padding: 7px 17px;
  }

  .modal_regional .modal-content button.ok {
    background: #ffaa3a;
    color: #ffffff;
    border: 2px solid #ffaa3a;
    margin-right: 7px;
  }

  @media screen and (max-width: 425px) {
    .csb__regional .card-body h2 {
      font-size: 18px;
      line-height: 24px;
      margin-bottom: 15px;
    }

    .csb__regional .card-body span {
      padding: 0.5rem 1rem;
      font-size: 12px;
      line-height: 18px;
    }

    .regional__benefit,
    .regional__member {
      width: 100%;
    }

    .regional__benefit h5 {
      font-size: 14px;
      line-height: 22px;
    }

    .regional__benefit p {
      font-size: 13px;
      line-height: 20px;
    }

    .regional__benefit img {
      margin-bottom: 5px;
    }

    .regional__benefit .col-6 {
      margin-bottom: 25px;
    }

    .regional__member h5 {
      font-size: 15px;
      line-height: 22px;
    }

    .regional__member h6 {
      font-size: 12px;
      line-height: 18px;
    }

    .regional__member_nama {
      font-size: 12px;
      line-height: 18px;
    }

    .regional__member .col-6 {
      padding: 0 5px;
    }

  }


</style>
@endsection

@section('regional-content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader" style="margin-bottom: 150px">
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row justify-content-center dash_home_navigation">

              <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                  <a href="{{ route('listUserReg') }}" id="tebak_kata">
                      <div class="card card-custom gutter-b card-stretch">
                          <div class="card-body text-center">
                              <img src="{{ asset('images/regional/reg-sekitar.svg') }}" alt="csb">
                              <div class="text-dark font-weight-bolder font-size-h4">
                                  Sobat Badak Sekitarmu</div>
                          </div>
                      </div>
                  </a>
              </div>

                <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                    <a href="{{ route('tebakKataRegional') }}" id="tebak_kata">
                        <div class="card card-custom gutter-b card-stretch">
                            <div class="card-body text-center">
                                <img src="{{ asset('images/regional/reg-quiz.svg') }}" alt="csb">
                                <div class="text-dark font-weight-bolder font-size-h4">
                                    Tebak Kata Spesial Regional</div>
                            </div>
                        </div>
                    </a>
                </div>


                <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu" style="position: relative">
                  <span class="regional__comingsoon">Coming Soon</span>
                    <a href="#" data-toggle="modal" data-target="#modal_korcab">
                        <div class="card card-custom gutter-b card-stretch">
                            <div class="card-body text-center">
                                <img src="{{ asset('images/regional/reg-pemilu.svg') }}" alt="csb">

                                <div class="text-dark font-weight-bolder font-size-h4">
                                    Pemilihan Korcab</div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu" style="position: relative">
                  <span class="regional__comingsoon">Coming Soon</span>
                    <a href="#" data-toggle="modal" data-target="#modal_kopdar">
                        <div class="card card-custom gutter-b card-stretch">
                            <div class="card-body text-center">
                                <img src="{{ asset('images/regional/reg-kopdar.svg') }}" alt="csb">

                                <div class="text-dark font-weight-bolder font-size-h4">
                                    Yuk Kopdar Yuk</div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>


    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container">
            <div class="card card-custom gutter-b csb__regional">
                <div class="card-header" style="border: transparent">
                    <div class="card-title">
                        <a href="{{ route('homeDash') }}" class="btn">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <h2 style="margin-bottom: 0">Halo <strong style="color: #ffaa3a">{{ Auth::user()->name }}</strong>, Selamat Datang di CSB Regional <strong style="color: #ffaa3a">{{ $regional->city_name }}</strong>!</h2>
                    <div class="text-center" style="margin: 15px auto; margin-bottom: 40px;">
                      <span>Regional Kamu: {{ $regional->city_name }}</span>
                    </div>

                    <div class="regional__member">
                      <h5>Sobat Badak Sekitarmu</h5>
                      <h6>Yang sudah daftar ke Regional {{ $regional->city_name }}: {{ $countUser }} Sobat Badak</h6>

                      <div class="row justify-content-center" style="margin-top: 25px">
                        @foreach($regionalUsers as $ru)
                          <div class="col-lg-4 col-6">
                            <div class="regional__member_nama">
                              {{$ru->name}} @if($ru->join_regional == 1) <img data-toggle="tooltip" title="Regional {{ $regional->city_name }}" src="{{ asset('images/regional/star.png') }}" alt=""> @endif
                            </div>
                          </div>
                        @endforeach


                      </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Banner-->
<div class="modal" id="modal_banner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" style="padding-right: 0">
    <div class="modal_banner">
        <div class="banner_wrapper">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i> Close
            </button>
            <div id="modal_banner_carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">

                  <div class="carousel-item active">
                    <img data-dismiss="modal" src="{{ asset('images/regional/banner.jpg') }}" alt=""
                        class="img-fluid banner_desktop" style="border-radius:10px">
                    <img data-dismiss="modal" src="{{ asset('images/regional/banner-mobile.jpg') }}" alt=""
                        class="img-fluid banner_mobile" style="border-radius:10px">
                  </div>

                </div>
                <!-- <a class="carousel-control-prev" href="#modal_banner_carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#modal_banner_carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_regional" id="modal_korcab" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <img src="{{asset('images/badak-baper.png')}}" alt="">
            <h4 class="ketentuan" style="margin-bottom: 10px;">Fitur Pemilihan Korcab Akan Segera Hadir</h4>
            <p>Mohon ditunggu yaa Sobat Badak</p>

            <div class="text-center" style="margin-top: 15px">
              <button class="btn ok" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_regional" id="modal_kopdar" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <img src="{{asset('images/badak-baper.png')}}" alt="">
            <h4 class="ketentuan" style="margin-bottom: 10px;">Fitur Kopdar Akan Segera Hadir</h4>
            <p>Mohon ditunggu yaa Sobat Badak</p>

            <div class="text-center" style="margin-top: 15px">
              <button class="btn ok" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- <button class="btn" data-toggle="modal" data-target="#modal_banner">Click</button> -->


@endsection

@section('pageJS')
<script>
$( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip()
});
</script>

@if(Session::has('success'))
<script>
  $(`#modal_banner`).modal('toggle')
</script>
@endif

@endsection
