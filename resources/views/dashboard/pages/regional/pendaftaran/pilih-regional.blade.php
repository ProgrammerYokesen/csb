@section('title')
  Pendaftaran Regional
@endsection

@extends('dashboard.pages.regional.master-regional')

@section('page_assets')
<style>
  .csb__regional .card-header {
    padding: 0;
  }

  .csb__regional .card-body {
    padding-top: 0 !important;
  }

  .csb__regional .card-body h2 {
    font-weight: 600;
    font-size: 24px;
    line-height: 36px;
    text-align: center;
    color: #000000;
    margin-bottom: 25px;
  }

  .regional__form {
    /* border: 1px solid red; */
    width: 60%;
    margin: 15px auto;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }

  .csb__regional button {
    background: #ffaa3a;
    color: #ffffff;
    margin: 15px auto;
    padding: 0.7rem 3rem;
  }

  .csb__regional button:hover {
    color: #ffffff;
  }

  .regional__form .select2 {
    width: 50%;
    margin-bottom: 15px;
  }

  .regional__form .select2-container--default .select2-selection--single .select2-selection__rendered {
    padding: unset;
  }

  .regional__form .select2-container .select2-selection--single {
    height: 40px;
    padding-left: 1rem;
    padding-top: 0.5rem;

    background: rgba(255, 170, 58, 0.3) !important;
    border-radius: 20px;
    border: transparent;
  }

  .regional__form .select2-selection__rendered,
  .regional__form .select2-selection__arrow:after {
    color: #ffaa3a !important;
  }

  .regional__form .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 7px;
  }

  .regional__form .select2-container--default.select2-container--open .select2-selection--single {
    border-color: #ffaa3a;
  }

  .modal_regional .modal-content {
    padding: 2rem;
  }

  .modal_regional .modal-content img {
    width: 150px;
    margin: 15px auto;
  }

  .modal_regional .modal-content h4 {
    text-align: center;
    font-weight: 600;
    margin-bottom: 25px
  }

  .modal_regional .modal-content p {
    text-align: center;
  }

  .modal_regional .modal-content button {
    padding: 7px 17px;
  }

  .modal_regional .modal-content button.ok {
    background: #ffaa3a;
    color: #ffffff;
    border: 2px solid #ffaa3a;
    margin-right: 7px;
  }

  .modal_regional .modal-content button.cancel {
    background: #ffffff;
    color: #ffaa3a;
    box-sizing: border-box;
    border: 2px solid #ffaa3a;
  }

  .modal_regional li {
    margin-bottom: 10px;
    font-weight: normal;
    letter-spacing: 0.5px;
  }

  .swal2-styled.swal2-confirm {
    background-color: #ffaa3a;
  }

  @media screen and (max-width: 425px) {
    .csb__regional .card-body h2 {
      font-size: 18px;
      line-height: 24px;
      margin-bottom: 15px;
    }

    .regional__form {
      width: 100%;
      margin: 15px auto;
    }

    .regional__form .select2 {
      width: 80%;
    }
  }


</style>
@endsection

@section('regional-content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
    </div>
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container">
            <div class="card card-custom gutter-b csb__regional">
                <div class="card-header" style="border: transparent">

                        <a href="{{ route('homeDash') }}" class="btn">
                          <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                        <div class="text-right">
                          <button class="btn" style="margin-right: 15px" data-toggle="modal" data-target="#modal_peraturan">Peraturan</button>
                        </div>

                </div>
                <div class="card-body">
                    <h2>Yuk Pilih Regional Kamu sesuai Domisili Kamu</h2>

                    <form id="formRegional" class="" action="{{route('submitRegional')}}" method="post">
                      @csrf
                    <div class="regional__form">

                        <select class="select2" id="select2_prov" name="province_id" required>
                          <option value="">Pilih Provinsi</option>
                          @foreach($province as $prov)
                            <option value="{{$prov->province_id}}">{{$prov->province_name}}</option>
                          @endforeach
                        </select>

                        <select class="select2" id="select2_city" name="city_id" required>
                          <option value="">Pilih Kota</option>
                        </select>

                        <select class="select2" id="select2_kec" name="subdistrict_id" required>
                          <option value="">Pilih Kecamatan</option>
                        </select>

                    </div>
                  </form>

                    <div class="text-center">
                      <button class="btn" onclick="confirmRegional()">Pilih Regional</button>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Confirmation-->
<div class="modal fade modal_regional" id="modal_confirmation" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <!-- <img src="{{asset('images/badak-baper.png')}}" alt=""> -->
            <h4 class="ketentuan">Apakah Sobat Badak sudah yakin?</h4>
            <p>Regional yang sudah dipilih tidak dapat diganti lagi. Dengan klik tombol "Lanjut", Sobat Badak telah menyetujui Syarat & Ketentuan</p>

            <div class="text-center">
              <p>Regional yang dipilih:</p>
              <p style="margin:0">Provinsi: <strong id="id_prov"></strong></p>
              <p style="margin:0">Kota/Kabupaten: <strong id="id_kota"></strong></p>
              <p style="margin:0">Kecamatan: <strong id="id_kec"></strong></p>
            </div>

            <div class="text-center" style="margin-top: 25px">
              <button class="btn ok" onclick="submitRegional()">Lanjut</button>
              <button class="btn cancel" data-dismiss="modal">Batalkan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Peraturan-->
<div class="modal fade modal_regional" id="modal_peraturan" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <h4 class="ketentuan">Peraturan Club Sobat Badak Regional</h4>

            <ol style="padding-left: 2rem">
              <li>Setiap member Club Sobat Badak diperbolehkan untuk mengikuti Club Sobat Badak regional sesuai dengan regionalnya masing-masing</li>
              <li>Untuk mengikuti Club Sobat Badak regional, Sobat Badak wajib memilih provinsi, kota, dan kecamatan sesuai dengan domisili Sobat Badak dan klik “Submit”</li>
              <li>Setelah itu klik “Yuk Join Regional”</li>
              <li>Setelah bergabung ke Club Sobat Badak regional, Sobat Badak akan mendapatkan banyak keuntungan seperti:
                <ul>
                  <li>Sobat Badak akan mendapatkan 100.000 Baper poin (hanya sekali setelah mendaftar)</li>
                  <li>Kesempatan untuk mendapatkan baper poin tambahan dari Quiz Tebak Kata Regional masing-masing</li>
                  <li>Mendapatkan identitas Club Sobat Badak Regional (berdasarkan regional yang dipilih)</li>
                  <li>Mendapatkan voucher diskon Warisan Gajahmada sebesar 25% yang akan dikirimkan setiap tanggal 15 dan 30 per bulannya oleh Admin Whatsapp Club Sobat Badak (hanya sekali setelah mendaftar)</li>
                </ul>
              </li>
            </ol>

            <div class="text-center" style="margin-top: 25px">
              <button class="btn ok" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pageJS')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>

<script>
  $(document).ready(function() {
      $('#select2_prov').select2();
      $('#select2_city').select2();
      $('#select2_kec').select2();
  });

  var selectProv = $('#select2_prov');
  $(selectProv).change(function () {

      var value = $(selectProv).val();
      // console.log(value)
      clearOptions('select2_city');
      clearOptions('select2_kec');

      if (value) {
          var text = $('#select2_prov :selected').text();
          // $('#province').val(text);
          getCity(value)
      }
  });

  var selectCity = $('#select2_city');
  $(selectCity).change(function () {

      var value = $(selectCity).val();
      // console.log(value)
      clearOptions('select2_kec');

      if (value) {
          var text = $('#select2_city :selected').text();
          // $('#province').val(text);
          getSubdistrict(value)
      }
  });

  const getCity = (id) => {
    $.ajax({
        type: 'GET',
        url: `/cities?province_id=${id}`,

        success: function(res) {
          // console.log(res)
          let cities = res.cities
          let city = cities.map(el => {
            return {
              id: el.id ,
              nama: el.city_name,
              text: el.city_name
            }
          })

          let data = [{
              id: "",
              nama: "Pilih Kota",
              text: "Pilih Kota"
          }].concat(city);

          $("#select2_city").select2({
              data: data
          })

        }
    });
  }

  const getSubdistrict = (id) => {
    $.ajax({
        type: 'GET',
        url: `/subdistricts?city_id=${id}`,

        success: function(res) {
          // console.log(res)
          let subdistrict = res.subdistricts
          let subd = subdistrict.map(el => {
            return {
              id: el.id ,
              nama: el.subdistrict_name,
              text: el.subdistrict_name
            }
          })

          let data = [{
              id: "",
              nama: "Pilih Kecamatan",
              text: "Pilih Kecamatan"
          }].concat(subd);

          $("#select2_kec").select2({
              data: data
          })
        }
    });
  }

  function clearOptions(id) {
    $('#' + id).empty().trigger('change');
  }

  const confirmRegional = () => {
    let prov = document.getElementById('select2_prov').value
    let city = document.getElementById('select2_city').value
    let subd = document.getElementById('select2_kec').value

    if (prov != "" && city != "" && subd != "") {
      var textprov = $('#select2_prov :selected').text();
      var textcity = $('#select2_city :selected').text();
      var textkec = $('#select2_kec :selected').text();
      $('#id_prov').text(`${textprov}`);
      $('#id_kota').text(`${textcity}`);
      $('#id_kec').text(`${textkec}`);
      $(`#modal_confirmation`).modal('toggle');
    }else{
      Swal.fire("Oops!", "Mohon lengkapi semua data terlebih dahulu!", "warning")
    }
  }

  const submitRegional = () => {
    let prov = document.getElementById('select2_prov').value
    let city = document.getElementById('select2_city').value
    let subd = document.getElementById('select2_kec').value

    if (prov != "" && city != "" && subd != "") {
      $('#formRegional').submit()
    }else{
      $(`#modal_confirmation`).modal('hide')
      Swal.fire("Oops!", "Mohon lengkapi semua data terlebih dahulu!", "warning")
    }
  }

</script>
@endsection
