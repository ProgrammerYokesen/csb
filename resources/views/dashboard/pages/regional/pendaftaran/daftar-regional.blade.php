@section('title')
  Pendaftaran Regional
@endsection

@extends('dashboard.pages.regional.master-regional')

@section('page_assets')
<style>
  .csb__regional .card-header {
    padding: 0;
  }

  .csb__regional .card-body {
    padding-top: 0 !important;
  }

  .csb__regional .card-body h2 {
    font-weight: 600;
    font-size: 24px;
    line-height: 36px;
    text-align: center;
    color: #000000;
    margin-bottom: 25px;
  }

  .csb__regional button {
    background: #ffaa3a;
    color: #ffffff;
    margin: 15px auto;
    padding: 0.7rem 3rem;
  }

  .csb__regional button:hover {
    color: #ffffff;
  }

  .csb__regional .card-body span {
    background: rgba(255, 170, 58, 0.3);
    border-radius: 15px;
    padding: 0.5rem 1rem;
    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    color: #FFAA3A;
  }

  .regional__benefit {
    background: #FFF7EC;
    border-radius: 9px;
    padding: 1rem;
    width: 80%;
    margin: 0 auto;
  }

  .regional__benefit h5 {
    font-weight: 600;
    font-size: 18px;
    line-height: 27px;
    text-align: center;
    color: #FFAA3A;
  }

  .regional__benefit p {
    font-weight: 600;
    font-size: 14px;
    line-height: 21px;
    color: rgba(0, 0, 0, 0.88);
  }

  .regional__benefit img {
    margin-bottom: 5px;
    width: 150px;
  }

  .regional__benefit button {
    font-weight: 700;
  }

  .regional__member {
    width: 80%;
    margin: 25px auto;
  }

  .regional__member h5 {
    font-weight: 600;
    font-size: 18px;
    line-height: 27px;
    text-align: center;
    color: #000000;
    margin: 0;
  }

  .regional__member h6 {
    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    text-align: center;

    color: rgba(0, 0, 0, 0.5);
  }

  .regional__member_nama {
    background: rgba(255, 170, 58, 0.1);
    border-radius: 8px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 1rem;
    margin-bottom: 10px;

    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    color: #000000;
  }

  .regional__member_nama img {
    margin-left: 5px;
  }

  .modal_regional .modal-content {
    padding: 2rem;
  }

  .modal_regional .modal-content img {
    width: 150px;
    margin: 15px auto;
  }

  .modal_regional .modal-content h4 {
    text-align: center;
    font-weight: 600;
    margin-bottom: 25px
  }

  .modal_regional .modal-content p {
    text-align: center;
  }

  .modal_regional .modal-content button {
    padding: 7px 17px;
  }

  .modal_regional .modal-content button.ok {
    background: #ffaa3a;
    color: #ffffff;
    border: 2px solid #ffaa3a;
    margin-right: 7px;
  }

  .modal_regional .modal-content button.cancel {
    background: #ffffff;
    color: #ffaa3a;
    box-sizing: border-box;
    border: 2px solid #ffaa3a;
  }

  .modal_regional li {
    margin-bottom: 10px;
    font-weight: normal;
    letter-spacing: 0.5px;
  }

  @media screen and (max-width: 425px) {
    .csb__regional .card-body h2 {
      font-size: 18px;
      line-height: 24px;
      margin-bottom: 15px;
    }

    .csb__regional .card-body span {
      padding: 0.5rem 1rem;
      font-size: 12px;
      line-height: 18px;
    }

    .regional__benefit,
    .regional__member {
      width: 100%;
    }

    .regional__benefit h5 {
      font-size: 14px;
      line-height: 22px;
    }

    .regional__benefit p {
      font-size: 13px;
      line-height: 20px;
    }

    .regional__benefit img {
      margin-bottom: 5px;
    }

    .regional__benefit .col-6 {
      margin-bottom: 25px;
    }

    .regional__member h5 {
      font-size: 15px;
      line-height: 22px;
    }

    .regional__member h6 {
      font-size: 12px;
      line-height: 18px;
    }

    .regional__member_nama {
      font-size: 12px;
      line-height: 18px;
    }

    .regional__member .col-6 {
      padding: 0 5px;
    }

  }


</style>
@endsection

@section('regional-content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
    </div>
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container">
            <div class="card card-custom gutter-b csb__regional">
                <div class="card-header" style="border: transparent">

                  <a href="{{ route('homeDash') }}" class="btn">
                    <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                  </a>
                  <div class="text-right">
                    <button class="btn" style="margin-right: 15px" data-toggle="modal" data-target="#modal_peraturan">Peraturan</button>
                  </div>

                </div>
                <div class="card-body">
                    <h2 style="margin-bottom: 0">Halo <strong style="color: #ffaa3a">{{ Auth::user()->name }}</strong>, Selamat Datang di CSB Regional <strong style="color: #ffaa3a">{{ $regional->city_name }}</strong>!</h2>
                    <div class="text-center" style="margin: 15px auto; margin-bottom: 40px;">
                      <span>Regional Kamu: {{ $regional->city_name }}</span>
                    </div>

                    <div class="regional__benefit">
                      <h5>Keuntungan Join Regional</h5>

                      <div class="row justify-content-center" style="margin: 25px auto">
                        <div class="col-lg-3 col-6 text-center">
                          <img src="{{ asset('images/regional/reg-bp.png') }}" alt="">
                          <p>Baper Poin</p>
                        </div>
                        <div class="col-lg-3 col-6 text-center">
                          <img src="{{ asset('images/regional/reg-voucher.png') }}" alt="">
                          <p>Voucher</p>
                        </div>
                        <div class="col-lg-3 col-6 text-center">
                          <img src="{{ asset('images/regional/reg-tebak.png') }}" alt="">
                          <p>Tebak Kata Regional</p>
                        </div>
                        <div class="col-lg-3 col-6 text-center">
                          <img src="{{ asset('images/regional/reg-id.png') }}" alt="">
                          <p>Identitas</p>
                        </div>

                      </div>


                      <div class="text-center">
                        <a href="{{route('joinRegional')}}">
                          <button class="btn">YUK JOIN REGIONAL {{ $regional->city_name }}!</button>
                        </a>
                      </div>

                    </div>

                    <div class="regional__member">
                      <h5>Sobat Badak Sekitarmu</h5>
                      <h6>Yang sudah daftar ke Regional {{ $regional->city_name }}: {{ $countUser }} Sobat Badak</h6>

                      <div class="row justify-content-center" style="margin-top: 25px">
                        @foreach($regionalUsers as $ru)
                          <div class="col-lg-4 col-6">
                            <div class="regional__member_nama">
                              {{$ru->name}} @if($ru->join_regional == 1) <img data-toggle="tooltip" title="Regional {{ $regional->city_name }}" src="{{ asset('images/regional/star.png') }}" alt=""> @endif
                            </div>
                          </div>
                        @endforeach


                      </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Peraturan-->
<div class="modal fade modal_regional" id="modal_peraturan" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <h4 class="ketentuan">Peraturan Club Sobat Badak Regional</h4>

            <ol style="padding-left: 2rem">
              <li>Setiap member Club Sobat Badak diperbolehkan untuk mengikuti Club Sobat Badak regional sesuai dengan regionalnya masing-masing</li>
              <li>Untuk mengikuti Club Sobat Badak regional, Sobat Badak wajib memilih provinsi, kota, dan kecamatan sesuai dengan domisili Sobat Badak dan klik “Submit”</li>
              <li>Setelah itu klik “Yuk Join Regional”</li>
              <li>Setelah bergabung ke Club Sobat Badak regional, Sobat Badak akan mendapatkan banyak keuntungan seperti:
                <ul>
                  <li>Sobat Badak akan mendapatkan 100.000 Baper poin (hanya sekali setelah mendaftar)</li>
                  <li>Kesempatan untuk mendapatkan baper poin tambahan dari Quiz Tebak Kata Regional masing-masing</li>
                  <li>Mendapatkan identitas Club Sobat Badak Regional (berdasarkan regional yang dipilih)</li>
                  <li>Mendapatkan voucher diskon Warisan Gajahmada sebesar 25% yang akan dikirimkan setiap tanggal 15 dan 30 per bulannya oleh Admin Whatsapp Club Sobat Badak (hanya sekali setelah mendaftar)</li>
                </ul>
              </li>
            </ol>

            <div class="text-center" style="margin-top: 25px">
              <button class="btn ok" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pageJS')
<script>
$( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip()
});
</script>
@endsection
