@extends('dashboard.pages.regional.master-regional')

@section('regional-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b quiz_win_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h2>Selamat!</h2>
                        <h5>Kamu berhasil menjawab {{ $answer }} pertanyaan dengan benar dari {{ $question }}
                            pertanyaan. <br> Nih Badak Baper kasih
                            kamu
                            Baper Poin:</h5>

                        <h1>{{ $poin }} Baper Poin</h1>

                        <a href="{{ route('tebakKataRegional') }}"><button class="btn">Selesai</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
