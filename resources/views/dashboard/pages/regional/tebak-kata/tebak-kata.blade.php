@section('title')
    Quiz Tebak Kata
@endsection

@extends('dashboard.pages.regional.master-regional')

@section('page_assets')
<style>
  .regional__comingsoon {
    background: #D10000;
    border-radius: 15px;
    font-weight: 500;
    font-size: 12px;
    line-height: 18px;
    color: #FFFFFF;
    padding: 5px;

    position: absolute;
    top: -10px;
    right: 5px;
    z-index: 3;
  }

  .modal_regional li{
    margin-bottom: 10px;
  }

  .modal_regional .modal-content {
    padding: 2rem;
  }

  .modal_regional .modal-content img {
    width: 150px;
    margin: 15px auto;
  }

  .modal_regional .modal-content h4 {
    text-align: center;
    font-weight: 600;
    margin-bottom: 25px
  }

  .modal_regional .modal-content p {
    text-align: center;
  }

  .modal_regional .modal-content button {
    padding: 7px 17px;
  }

  .modal_regional .modal-content button.ok {
    background: #ffaa3a;
    color: #ffffff;
    border: 2px solid #ffaa3a;
    margin-right: 7px;
  }
</style>
@endsection

@section('regional-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader" style="margin-bottom: 150px">
        </div>

        <div class="d-flex flex-column-fluid">
            <div class="container">
              <div class="row justify-content-center dash_home_navigation">

                <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                    <a href="{{ route('listUserReg') }}" id="tebak_kata">
                        <div class="card card-custom gutter-b card-stretch">
                            <div class="card-body text-center">
                                <img src="{{ asset('images/regional/reg-sekitar.svg') }}" alt="csb">
                                <div class="text-dark font-weight-bolder font-size-h4">
                                    Sobat Badak Sekitarmu</div>
                            </div>
                        </div>
                    </a>
                </div>

                  <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                      <a href="{{ route('tebakKataRegional') }}" id="tebak_kata">
                          <div class="card card-custom gutter-b card-stretch">
                              <div class="card-body text-center">
                                  <img src="{{ asset('images/regional/reg-quiz.svg') }}" alt="csb">
                                  <div class="text-dark font-weight-bolder font-size-h4">
                                      Tebak Kata Spesial Regional</div>
                              </div>
                          </div>
                      </a>
                  </div>


                  <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu" style="position: relative">
                    <span class="regional__comingsoon">Coming Soon</span>
                      <a href="#" data-toggle="modal" data-target="#modal_korcab">
                          <div class="card card-custom gutter-b card-stretch">
                              <div class="card-body text-center">
                                  <img src="{{ asset('images/regional/reg-pemilu.svg') }}" alt="csb">

                                  <div class="text-dark font-weight-bolder font-size-h4">
                                      Pemilihan Korcab</div>
                              </div>
                          </div>
                      </a>
                  </div>

                  <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu" style="position: relative">
                    <span class="regional__comingsoon">Coming Soon</span>
                      <a href="#" data-toggle="modal" data-target="#modal_kopdar">
                          <div class="card card-custom gutter-b card-stretch">
                              <div class="card-body text-center">
                                  <img src="{{ asset('images/regional/reg-kopdar.svg') }}" alt="csb">

                                  <div class="text-dark font-weight-bolder font-size-h4">
                                      Yuk Kopdar Yuk</div>
                              </div>
                          </div>
                      </a>
                  </div>

              </div>
            </div>
        </div>


        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b tebak_kata_card">
                    <div class="tebak_kata_card_head">
                        <a href="{{ route('homeDash') }}" class="btn">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                        <div class="btn cara_bermain" data-toggle="modal" data-target="#cara-tebak-kata" style="font-size: 14px; font-weight: 600">
                            Cara Bermain
                        </div>
                    </div>

                    <div class="card-body">

                        @if ($quiz == 0)
                            <h1 class="mb-5">Sedang tidak ada quiz hari ini...</h1>

                            <div class="d-flex justify-content-center">
                              <a href="{{ route('homeDash') }}" class="mt-3">
                                  <button class="btn">Kembali</button>
                              </a>
                            </div>
                        @elseif ($doneQuiz == true)
                            <h1 class="mb-3">Kamu sudah mengikuti Quiz Tebak Kata Spesial Regional hari ini.</h1>
                            <h5 class="mb-5">Yuk kembali lagi besok dan dapatkan Baper Poin sebanyak-banyaknya!</h5>

                            <div class="d-flex justify-content-center">
                              <a href="{{ route('homeDash') }}" class="mt-3">
                                  <button class="btn">Kembali</button>
                              </a>
                            </div>
                        @else
                            <h1>Quiz Tebak Kata Spesial Regional</h1>
                            <h4 style="margin-bottom: 15px; margin-top: 15px">Ayo tebak kuis yang mimin Badak kasih hari ini
                                dan menangkan hingga
                                <span class="custom-text-primary">{{ $totalPoin }} Baper Poin</span><br>
                               <span style="color: #FF8500;">Tenang, pertanyaannya kali ini sesuai dengan daerahmu kok!</span>
                            </h4>
                            <div class="d-flex justify-content-center">
                              <a href="{{ route('jawabTebakKataRegional') }}">
                                <button class="btn">Mulai</button>
                              </a>
                            </div>
                        @endif

                        <div class="submit_tebak_kata">
                            <h4 class="ugc_tebak_kata">Kamu punya pertanyaan menarik khas daerah mu?</h4>
                            <a href="{{ route('inputTebakKataRegional') }}">
                                <button class="btn">Submit Disini</button>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Tebak Kata --}}
    <div class="modal fade modal_regional" id="cara-tebak-kata" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Cara Bermain Quiz Tebak Kata</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Tebak jawaban dari pertanyaan yang akan muncul
                            </li>
                            <li>
                                Ketik huruf yang terkandung dari jawaban yang kamu tebak didalam box, lalu klik enter
                            </li>
                            <li>
                                Kamu memiliki 3 kali kesempatan salah, jika berhasil menjawab kamu akan memperoleh 1.000 Baper Poin,
                                jika gagal kamu tidak memperoleh poin
                            </li>
                            <li>
                                Kumpulkan Baper Poin dalam sesi ini, Selamat Bermain!
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Coming Soon -->
    <div class="modal fade modal_regional" id="modal_korcab" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <img src="{{asset('images/badak-baper.png')}}" alt="">
                <h4 class="ketentuan" style="margin-bottom: 10px;">Fitur Pemilihan Korcab Akan Segera Hadir</h4>
                <p>Mohon ditunggu yaa Sobat Badak</p>

                <div class="text-center" style="margin-top: 15px">
                  <button class="btn ok" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal_regional" id="modal_kopdar" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <img src="{{asset('images/badak-baper.png')}}" alt="">
                <h4 class="ketentuan" style="margin-bottom: 10px;">Fitur Kopdar Akan Segera Hadir</h4>
                <p>Mohon ditunggu yaa Sobat Badak</p>

                <div class="text-center" style="margin-top: 15px">
                  <button class="btn ok" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection
