@section('title')
    Tambal Ban Online
@endsection

@section('page_assets')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('user-dashboard/tambal.css') }}">
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">

                    <a href="{{ route('homeDash') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>

                </div>
                <div class="d-flex justify-content-center text-center game__header">
                    <img src="{{ asset('images/pompa.png') }}" alt="">
                </div>
                <div id="box_game" class="box__game d-flex justify-content-center">
                    <div class="game_layout">
                        <canvas id="game_lay">
                            <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                                try
                                another.</p>
                        </canvas>
                        <img class="jamet__img" src="{{ asset('images/jamet_pompa.png') }}" alt="">
                        <img class="ban__img" src="{{ asset('images/ban_1.png') }}" alt="">
                        {{-- <img class="pointer__img" src="{{ asset('images/ban_pointer.png') }}" alt=""> --}}
                        <img class="lovebar__img" src="{{ asset('images/lifebar_love.png') }}" alt="">
                        <img class="love1 love__img" src="{{ asset('images/love_bar.png') }}" alt="">
                        <img class="love2 love__img" src="{{ asset('images/love_bar.png') }}" alt="">
                        <img class="love3 love__img" src="{{ asset('images/love_bar.png') }}" alt="">
                        <img class="lifebar__img" src="{{ asset('images/lifebar.png') }}" alt="">
                        <img class="bh1 bh__img" src="{{ asset('images/ban_health.png') }}" alt="">
                        <img class="bh2 bh__img" src="{{ asset('images/ban_health.png') }}" alt="">
                        <img class="bh3 bh__img" src="{{ asset('images/ban_health.png') }}" alt="">
                        <img onclick="stop()" class="stop__img" src="{{ asset('images/ban_stop.png') }}" alt="">
                        <img onclick="run()" class="go__img" src="{{ asset('images/ban_go.png') }}" alt="">
                        <img onclick="startGame()" class="playagain__img" src="{{ asset('images/ban_go.png') }}" alt="">
                        <img class="time__img" src="{{ asset('images/timebar_ban.png') }}" alt="">
                        <div class="timer_text d-flex">
                            <div class="t_min">00:</div>
                            <div class="t_sec">00:</div>
                            <div class="t_ms">00</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal__bg_light fade" id="play__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <img onclick="initGame()" id="play__btn" src="{{ asset('images/play__ban.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="modal modal__bg_light fade" id="warning__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <p class="modal__title_black">
                        Kamu membutuhkan 1 Tiket 17an untuk bermain Pompa Ban Online
                    </p>
                    <p class="modal__desc_gray">
                        1 Tiket 17an = 1.000 Baper Poin. Baper Poin akan terpotong saat kamu klik tombol “MULAI”.
                    </p>
                    <div class="btn__result d-flex justify-content-center">
                        <img onclick="$('#caramain__modal').modal('toggle');$('#warning__modal').modal('toggle')"
                            class="peringkat__btn mx-4" src="{{ asset('images/lomba/mobile/cara-btn.png') }}" alt="">
                        <a href="{{ route('goBanMobile') }}">
                            <img class="lanjut__btn mx-4" src="{{ asset('images/play_btn__modal.png') }}" alt="">
                        </a>
                    </div>
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/modal__label.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white">
                                    POMPA BAN
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="caramain__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <ol>
                        <li>
                            Kamu diwajibkan untuk memompa 3 ban dengan waktu secepat mungkin
                        </li>
                        <li>
                            Tekan tombol hijau hingga mencapai garis berwarna hijau
                        </li>
                        <li>
                            Jika sudah mencapai garis hijau, tekan tombol merah untuk berhenti
                        </li>
                        <li>
                            Jika kamu berhenti di garis kuning atau garis merah, maka kesempatanmu akan berkurang satu
                        </li>
                        <li>
                            Di games ini, kamu memiliki tiga kesempatan dan bisa dilihat dari jumlah hati yang kamu
                            miliki
                        </li>
                        <li>
                            Pemenang ditentukan berdasarkan waktu tercepat dalam memompa 3 ban secara tepat
                        </li>
                    </ol>
                    <img class="modal__close" data-dismiss="modal"
                        onclick="$('#play__modal').modal('toggle');$('#caramain__modal').modal('toggle')"
                        src="{{ asset('images/modal__close.png') }}" alt="">
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/modal__label.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white">
                                    Cara Bermain
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="gagal__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <p class="modal__title_black">
                        Tiket yang kamu butuhkan tidak mencukupi
                    </p>
                    <p class="modal__desc_gray">
                        Ayo tambah baper poin kamu dengan melakukan tugas harian di menu dashboard!
                    </p>
                    <a href="{{ route('leaderboardTambalBan') }}">
                        <img class="modal__play" src="{{ asset('images/modal__close.png') }}" alt="">
                    </a>
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/lomba/mobile/main-lagi.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white">
                                    GAGAL
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="result__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <img class="jamet_result" src="{{ asset('images/fail_jamet.png') }}" alt="">
                    <div class="d-flex result__ban justify-content-center">
                        <img class="mx-1 res_ban rb1" src="{{ asset('images/ban_health.png') }}" alt="">
                        <img class="mx-1 res_ban rb2" src="{{ asset('images/ban_health.png') }}" alt="">
                        <img class="mx-1 res_ban rb3" src="{{ asset('images/empty_ban.png') }}" alt="">
                    </div>
                    <div class="res__score">
                        <img class="res__score__img" src="{{ asset('images/result_bar.png') }}" alt="">
                        <p class="modal__title_black txt_res_score">
                            00:30:68
                        </p>
                    </div>
                    <div class="btn__result d-flex justify-content-center">
                        <a href="{{ route('leaderboardTambalBan') }}">
                            <img class="peringkat__btn mx-4" src="{{ asset('images/peringkat__btn.png') }}" alt="">
                        </a>
                        <img class="lanjut__btn mx-4" src="{{ asset('images/lomba/mobile/main-lagi.png') }}" alt=""
                            onclick="window.location.reload()">
                    </div>
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/modal__label.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white title_of_modal">
                                    BERHASIL
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <audio src="{{ asset('sounds/panjat-win.wav') }}" id="winSound"></audio>
    <audio src="{{ asset('sounds/pompa.wav') }}" id="pompaSound"></audio>
    <audio src="{{ asset('sounds/crowd.wav') }}" id="crowdSound"></audio>
    <audio src="{{ asset('sounds/meledak.wav') }}" id="meledakSound"></audio>
@endsection

@section('pageJS')

    @if (Session::has('go'))

        <script>
            window.location.hash = 'box_game'
            var myGamePiece = [];
            let score = []
            let totalScore = 0
            let pointer = new Image()
            pointer.src = "{{ asset('images/ban_pointer.png') }}"
            let imgIndex = 1
            var life = [$(".love3"), $(".love2"), $(".love1")]
            let idxLife, banCounter = 0
            var ban = [$(".bh3"), $(".bh2"), $(".bh1")]
            var ms = 0
            var sc = 0
            var mn = 0
            var clickable = true;
            var step = 10
            var stopp = false
            let lowGravity = 0.04
            let middleGravity = 0.0000000005 //0.018
            let highGravity = 0.000093 //0.002
            let vx = 2
            let gravitySpeed = 0

            // dif flex justify center
            // div boxgame / card
            // canvas

            var myGameArea = {
                canvas: $("#game_lay"),
                start: function() {
                    this.canvas[0].width = 385;
                    this.canvas[0].height = 700;
                    this.context = this.canvas[0].getContext("2d");
                    this.frameNo = 0;
                    this.interval = setInterval(animate, 20)
                },
                clear: function() {
                    this.context.clearRect(0, 0, this.canvas[0].width, this.canvas[0].height)
                }
            };

            $(function() {
                let crowd = document.querySelector('#crowdSound')
                crowd.play()
                let pompa = document.querySelector('#pompaSound')
                pompa.play()
                $(".jamet__img").attr("src", "{{ asset('images/jamet_gif.gif') }}");
                startGame()
                stopWatch()
                $('.playagain__img').hide()
            })

            function sendScore(finalScore, totalBan) {
                console.log('final score ===+>' + finalScore)
                console.log('ban score ===+>' + totalBan)
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('endTambalBan') }}",
                    type: "POST",
                    data: {
                        time: finalScore,
                        totalBan: totalBan
                    },
                    success: function(res) {
                        console.log(res)
                        if (res.Status == "Success") {
                            // show modal
                            $("#result__modal").modal('toggle')
                        } else {
                            $("#result__modal").modal('toggle')
                        }
                    },
                    error: function(err) {
                        console.log(err)
                        // show modal
                        $("#result__modal").modal('toggle')
                    }
                })
            }

            function changeBan(uri) {
                var imgBan = $(".ban__img");
                imgBan.fadeOut('fast', function() {
                    imgBan.attr("src", uri);
                    imgBan.fadeIn('fast');
                })
            }

            function formatTime(val) {
                val = parseInt(val)
                return val >= 10 ? `${val}` : `0${val}`
            }

            function stopWatch() {
                var timerr = setInterval(function() {
                    if (stopp) {
                        clearInterval(timerr)
                        console.log("STOP")
                    }

                    ms += 10
                    myGameArea.frameNo += 1
                    if (ms > 999) {
                        sc++
                        ms = 0
                    }
                    if (sc > 59) {
                        mn++
                        sc = 0
                    }

                    let minimized = ms > 100 ? ms / 10 : ms
                    $(".t_ms").text(formatTime(minimized))
                    $(".t_sec").text(`${formatTime(sc)}:`)
                    $(".t_min").text(`${formatTime(mn)}:`)
                }, 10)
            }

            function startGame() {

                clickable = true

                // make layout
                myGameArea.start()

                // draw pointer 
                myGameArea.context.drawImage(pointer, vx, 638)

                // show run button 
                $('.go__img').show()
                $('.playagain__img').hide()

                // run stopwatch
                stopWatch()
            }

            function run() {
                vx += (1.5 * 20)
                myGameArea.clear()
                myGameArea.context.drawImage(pointer, vx, 638)
            }

            function setLife() {
                if (life.length == 1) {
                    life[0].css('opacity', '0.2')
                    life.shift()
                } else {
                    let tempItem = life.shift()
                    tempItem.addClass('blink_me')
                    // blink the love for 2.5s 
                    setTimeout(function() {
                        tempItem.removeClass('blink_me')
                        tempItem.css('opacity', '0.2')
                    }, 2000)
                }
            }

            function setBan() {
                ban[0].css('opacity', '1')
                ban.shift()
                banCounter++
            }

            function setScore(val) {
                score.push(val)
                totalScore += (val)
            }

            function stop() {
                if (clickable) {
                    gravitySpeed = 0

                    if (vx < 170 || vx > 280) {
                        // if hit yellow or red bar
                        setLife()
                        vx = 2

                    } else {
                        // hit the green area 
                        setBan()
                        vx = 2

                    }

                    if (life.length == 0 || ban.length == 0) {
                        $('.go__img').hide()
                        $('.playagain__img').hide()
                        stopp = true

                        // set score
                        // life == 0
                        if (life.length == 0) {
                            setScore(0)
                            mn = 0
                            sc = 0
                            ms = 0
                            $('.jamet_result').attr("src", "{{ asset('images/fail_jamet.png') }}");
                            $('.title_of_modal').text("GAGAL")
                        }
                        // success
                        else {
                            setScore(myGameArea.frameNo)
                            $('.jamet_result').attr("src", "{{ asset('images/success_jamet.png') }}");
                            $('.title_of_modal').text("BERHASIL")
                            let winAudio = document.querySelector('#winSound')
                            winAudio.play()
                        }

                        // set image of tires in modal
                        for (let index = 1; index <= 3; index++) {
                            if (index <= banCounter) {
                                // succeded ban
                                $(`.rb${index}`).attr("src", "{{ asset('images/ban_health.png') }}");
                            } else {
                                // fail ban 
                                $(`.rb${index}`).attr("src", "{{ asset('images/empty_ban.png') }}");
                            }
                        }

                        // set text score
                        $('.txt_res_score').text(`${formatTime(mn)}:${formatTime(sc)}:${formatTime(ms/10)}`)

                        // should add code to end the game and show modal final score
                        sendScore(totalScore, banCounter)

                        // show modal
                        // $("#result__modal").modal('toggle')

                        console.log("SCORE ===> " + totalScore)
                        console.log("BAN ===> " + banCounter)
                    } else {
                        // change button
                        $('.playagain__img').fadeIn('fast')
                        $('.go__img').fadeOut('fast')
                    }

                    // stop game
                    myGameArea.context = null

                    // stop time
                    clearInterval(myGameArea.interval)

                    // prevent click stop 
                    clickable = false
                }
            }

            function animate() {
                if (isFinish()) {
                    vx = 2
                    gravitySpeed = 0
                    let meledak = document.querySelector('#meledakSound')
                    meledak.play()
                    setTimeout(function() {
                        meledak.pause()
                        meledak.currentTime = 0
                    }, 2000)

                    // stop game and set score 
                    setScore(vx)
                    myGameArea.context = null

                    // stop time
                    clearInterval(myGameArea.interval)

                    // explosion of the tire 
                    imgIndex = 5
                    changeBan("{{ asset('images/boom.png') }}")


                    // love item -1
                    setLife()

                    //change button
                    $('.playagain__img').show()
                    $('.go__img').hide()

                    if (life.length == 0) {
                        $('.go__img').hide()
                        $('.playagain__img').hide()
                        stopp = true
                        setScore(0)
                        mn = 0
                        sc = 0
                        ms = 0
                        $('.jamet_result').attr("src", "{{ asset('images/fail_jamet.png') }}");
                        $('.title_of_modal').text("GAGAL")

                        // set image of tires in modal
                        for (let index = 1; index <= 3; index++) {
                            if (index <= banCounter) {
                                // succeded ban
                                $(`.rb${index}`).attr("src", "{{ asset('images/ban_health.png') }}");
                            } else {
                                // fail ban 
                                $(`.rb${index}`).attr("src", "{{ asset('images/empty_ban.png') }}");
                            }
                        }

                        // set text score
                        $('.txt_res_score').text(`${formatTime(mn)}:${formatTime(sc)}:${formatTime(ms/10)}`)

                        // should add code to end the game and show modal final score
                        sendScore(totalScore, banCounter)

                    }
                } else if (isBottom()) {
                    vx = 2
                    gravitySpeed = 0
                } else {
                    if (vx <= 100) {
                        if (imgIndex != 1) {
                            imgIndex = 1
                            changeBan("{{ asset('images/ban_1.png') }}")
                        }

                        gravitySpeed += lowGravity
                    } else if (vx <= 165) {
                        if (imgIndex != 2) {
                            imgIndex = 2
                            changeBan("{{ asset('images/ban_2.png') }}")
                        }
                        gravitySpeed += lowGravity
                    } else if (vx <= 280) {
                        if (imgIndex != 3) {
                            imgIndex = 3
                            changeBan("{{ asset('images/ban_3.png') }}")
                        }
                        // middleGravity = Math.random() * (0.018 - 1.4) + 1.4
                        gravitySpeed += middleGravity
                    } else if (vx > 280) {
                        if (imgIndex != 4) {
                            imgIndex = 4
                            changeBan("{{ asset('images/ban_4.png') }}")
                        }
                        // highGravity = Math.random() * (0.002 - 1.2) + 1.2
                        gravitySpeed += highGravity
                    }

                    vx -= gravitySpeed

                    myGameArea.clear()
                    myGameArea.context.drawImage(pointer, vx, 638)
                }
            }

            function isFinish() {
                return vx > 333
            }

            function isBottom() {
                return vx < 3
            }
        </script>
    @else
        <script>
            var myGamePiece = [];
            let score = []
            let totalScore = 0
            let pointer = new Image()
            pointer.src = "{{ asset('images/ban_pointer.png') }}"
            let lowGravity = 0.06
            let middleGravity = 0.0000000005 //0.018
            let highGravity = 0.000093 //0.002
            let vx = 2
            let gravitySpeed = 0
            let imgIndex = 1
            var life = [$(".love3"), $(".love2"), $(".love1")]
            let idxLife, banCounter = 0
            var ban = [$(".bh3"), $(".bh2"), $(".bh1")]
            var ms = 0
            var sc = 0
            var mn = 0
            var clickable = true;
            var step = 10
            var stopp = false

            var myGameArea = {
                canvas: $("#game_lay"),
                start: function() {
                    this.canvas[0].width = 385;
                    this.canvas[0].height = 700;
                    this.context = this.canvas[0].getContext("2d");
                    this.frameNo = 0;
                    // this.interval = setInterval(animate, 20)
                },
                clear: function() {
                    this.context.clearRect(0, 0, this.canvas[0].width, this.canvas[0].height)
                }
            };

            function initGame() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('joinTambalBan') }}",
                    type: "POST",
                    data: {},
                    beforeSend: function() {
                        $("#play__modal").modal('toggle')
                    },
                    success: function(res) {
                        if (res.Status == "Success") {
                            $("#warning__modal").modal('toggle')
                            console.log(res.Status)
                        } else {
                            $("#warning__modal").modal('toggle')
                            $("#gagal__modal").modal('toggle')
                        }
                    },
                    error: function(err) {
                        console.log(err)
                        // show modal
                        $("#warning__modal").modal('toggle')
                        $("#gagal__modal").modal('toggle')
                    }
                })
            }

            $(function() {
                myGameArea.start()
                $("#play__modal").modal('toggle')
            })
        </script>
    @endif
@endsection
