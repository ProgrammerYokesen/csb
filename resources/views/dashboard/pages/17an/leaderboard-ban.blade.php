@section('title')
    Lomba 17 an
@endsection

@section('page_assets')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('user-dashboard/tambal.css') }}">
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">

                    <a href="{{ route('tambalBan') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>
                    
                    <a href="{{ route('hadiahPompa') }}">
                        <button class="btn" style="background: #f3a237;border-radius: 5px;color: #ffffff;">Daftar Hadiah</button>
                    </a>

                </div>
                {{-- <div class="d-flex justify-content-center text-center gacha-header">
                    <div class="redeem coin-header-text">Spin It Yourself!</div>
                </div> --}}
                <div class="card-body">
                    <div class="text-center">
                        <div>
                            <img class="img__leaderboards" src="{{ asset('images/pompa.png') }}" alt="">
                        </div>
                        <div style="position: relative">
                            <img class="img__leaderboards" src="{{ asset('images/modal__label.png') }}" alt="">
                            <p class="modal__title_white leader__title">
                                PERINGKAT
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <table class="leader__table table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" scope="col">Posisi</th>
                                        <th class="text-center" scope="col">Nama</th>
                                        <th class="text-center" scope="col">Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 0; $i < 10; $i++)
                                        <tr>
                                            <td class="text-center">{{ $i + 1 }}</td>
                                            <td class="text-center">{{ $leaderboard[$i] ? $leaderboard[$i]->name : '-' }}
                                            </td>
                                            <td class="text-center">
                                                {{ $leaderboard[$i] ? $leaderboard[$i]->time : '-' }}
                                            </td>
                                        </tr>
                                    @endfor
                                    {{-- <tr>
                                        <td class="text-center">Nick</td>
                                        <td class="text-center">Stone</td>
                                        <td class="text-center">
                                            <span class="label label-inline label-light-primary font-weight-bold">
                                                Pending
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Ana</td>
                                        <td class="text-center">Jacobs</td>
                                        <td class="text-center">
                                            <span class="label label-inline label-light-success font-weight-bold">
                                                Approved
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Larry</td>
                                        <td class="text-center">Pettis</td>
                                        <td class="text-center">
                                            <span class="label label-inline label-light-danger font-weight-bold">
                                                New
                                            </span>
                                        </td>
                                    </tr> --}}
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6 col-12">
                            <table class="leader__table table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" scope="col">Posisi</th>
                                        <th class="text-center" scope="col">Nama</th>
                                        <th class="text-center" scope="col">Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 10; $i < 20; $i++)
                                        <tr>
                                            <td class="text-center">{{ $i + 1 }}</td>
                                            <td class="text-center">{{ $leaderboard[$i] ? $leaderboard[$i]->name : '-' }}
                                            </td>
                                            <td class="text-center">
                                                {{ $leaderboard[$i] ? $leaderboard[$i]->time : '-' }}
                                            </td>
                                        </tr>
                                    @endfor
                                    {{-- <tr>
                                        <td class="text-center">Nick</td>
                                        <td class="text-center">Stone</td>
                                        <td class="text-center">
                                            <span class="label label-inline label-light-primary font-weight-bold">
                                                Pending
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Ana</td>
                                        <td class="text-center">Jacobs</td>
                                        <td class="text-center">
                                            <span class="label label-inline label-light-success font-weight-bold">
                                                Approved
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Larry</td>
                                        <td class="text-center">Pettis</td>
                                        <td class="text-center">
                                            <span class="label label-inline label-light-danger font-weight-bold">
                                                New
                                            </span>
                                        </td>
                                    </tr> --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pageJS')
@endsection
