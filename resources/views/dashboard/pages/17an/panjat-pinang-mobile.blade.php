<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak | @yield('title') </title>
    @laravelPWA

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta property="og:title" content="Club Sobat Badak" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://sobatbadak.club" />
    <meta property="og:image" content="https://sobatbadak.club/images/share.png" />
    <meta property="og:description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />


    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, shrink-to-fit=no" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />

    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('user-dashboard/style.css') }}">
    @yield('page_assets')

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '806244510020586');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199518915-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199518915-2');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NVTPTBJ');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 695110688 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-695110688"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-695110688');
    </script>


</head>
<!--end::Head-->
<!--begin::Body-->

<body style="background: #ffffff">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVTPTBJ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!--begin::Main-->

    {{-- <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper" style="position: relative;"> --}}

    <div class="panjat__mobile">
        {{-- <div class="card"> --}}
        {{-- <div class="card-body"> --}}
        <canvas id="panjat_overlay"></canvas>

        {{-- <img src="{{ asset('images/lomba/mobile/bg.png') }}" alt=""> --}}

        {{-- Timer --}}
        {{-- <div class="panjat__timer">
            <img src="{{ asset('images/lomba/mobile/timer.svg') }}" alt="">
        </div> --}}

        {{-- Help --}}
        <div class="panjat__help" data-toggle="modal" data-target="#modal_cara">
            <img src="{{ asset('images/lomba/mobile/help.svg') }}" alt="">
        </div>

        {{-- Mulai --}}
        <div class="panjat__mulai" id="panjat_menu" data-toggle="modal" data-target="#modal_mulai">
            <img src="{{ asset('images/lomba/mobile/mulai.svg') }}" alt="">
        </div>
        <div class="panjat__mulai d-none" id="panjat_game" onmousedown="accelerateMobile(-1)">
            <img src="{{ asset('images/lomba/mobile/mulai.svg') }}" alt="">
        </div>
        {{-- </div> --}}
        {{-- </div> --}}
    </div>

    <div class="panjat__landscape">
        <h1>Mohon Putar Device anda ke mode Portrait untuk main</h1>
    </div>

    <audio src="{{ asset('sounds/panjat-win.wav') }}" id="winSound"></audio>
    <audio src="{{ asset('sounds/panjat-count.wav') }}" id="panjatCount"></audio>
    <audio src="{{ asset('sounds/crowd.wav') }}" id="panjatCrowd"></audio>


    {{-- Modal Mulai --}}
    <div class="modal fade modal__mulai" id="modal_mulai" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <img src="{{ asset('images/lomba/panjat-pinang.png') }}" alt="" class="modal__logo">
                <img src="{{ asset('images/lomba/mobile/panjat.png') }}" alt="" class="modal__title">

                <div class="modal_ctn">
                    <h3>Kamu membutuhkan 1 Tiket PANJAT PINANG untuk bermain Panjat Pinang</h3>
                    <p>1 Tiket PANJAT PINANG = 1.000 Baper Poin. Baper Poin akan terpotong saat kamu klik
                        tombol
                        “MULAI”.</p>
                    {{-- <h3>Hari ini Mimin kasih GRATIS untuk cobain Game Panjat Pinang</h3>
                    <p>Tapi mulai tanggal 17 nanti bayar pake baper poin yaa</p> --}}
                </div>

                <div class="mulai__btn">
                    <img src="{{ asset('images/lomba/mobile/cara-btn.png') }}" alt="" onclick="toCara()">
                    <img src="{{ asset('images/lomba/mobile/mulai-btn.png') }}" alt="" onclick="potongBaper()">
                </div>

            </div>
        </div>
    </div>

    {{-- Modal cara --}}
    <div class="modal fade modal__mulai modal__cara" id="modal_cara" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <img src="{{ asset('images/lomba/panjat-pinang.png') }}" alt="" class="modal__logo">
                <img src="{{ asset('images/lomba/mobile/cara-title.png') }}" alt="" class="modal__title">

                <div class="modal_ctn">

                    <ol class="text-left">
                        <li>Tekan tombol hijau secara terus menerus
                        </li>
                        <li>Apabila kamu berhenti menekan, karaktermu akan turun secara perlahan
                        </li>
                        <li>Kamu diharuskan untuk terus menekan tombol hijau agar karaktermu bisa sampai ke puncak
                        </li>
                        <li>Pastikan kamu mencapai puncak dengan waktu secepat mungkin
                        </li>
                        <li>Pemenangnya ditentukan berdasarkan waktu tercepat mencapai puncak</li>

                    </ol>
                    {{-- <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Soluta est assumenda
                        voluptatem voluptatibus! Praesentium voluptate eum voluptatem aperiam obcaecati
                        alias recusandae magni, eius et suscipit vitae. In nihil suscipit deserunt magni,
                        facere inventore repellat quisquam. Eligendi, laudantium! Omnis deserunt nihil
                        fugiat saepe beatae mollitia. Nihil, aliquam modi reiciendis molestias maiores odit
                        quae id corrupti veniam doloribus aliquid harum explicabo vero, consectetur vitae
                        placeat iste saepe odio eaque obcaecati esse laboriosam. Molestias mollitia quaerat
                        tempora dolore harum id porro nisi, enim ipsa ad ipsam maiores ullam, reiciendis
                        modi optio deleniti adipisci nobis amet hic dicta officiis est! Ea veniam dolore
                        nostrum.</p> --}}
                </div>

                <div class="mulai__btn">
                    <img src="{{ asset('images/lomba/mobile/mulai-btn.png') }}" alt="" data-dismiss="modal">
                </div>

            </div>
        </div>
    </div>


    {{-- Modal Leaderboard --}}
    <div class="modal fade modal__mulai modal__cara" id="modal_peringkat" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <img src="{{ asset('images/lomba/panjat-pinang.png') }}" alt="" class="modal__logo">
                <img src="{{ asset('images/lomba/mobile/peringkat-title.png') }}" alt="" class="modal__title">

                <img src="{{ asset('images/lomba/mobile/close.svg') }}" alt="" class="modal__close"
                    data-dismiss="modal">

                <div class="modal_ctn">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>Posisi</th>
                                <th>Nama</th>
                                <th>Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for ($i = 0; $i < 10; $i++)
                                <tr>
                                    <td>{{ $i + 1 }}</td>
                                    <td>Bambang</td>
                                    <td>09:00</td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    {{-- Modal Win --}}
    <div class="modal fade modal__mulai" id="modal_win" data-backdrop="static" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <img src="{{ asset('images/lomba/panjat-pinang.png') }}" alt="" class="modal__logo">
                <img src="{{ asset('images/lomba/mobile/win.png') }}" alt="" class="modal__title">

                <div class="modal__win">
                    <img src="{{ asset('images/lomba/mobile/baper-win.png') }}" alt="">
                    <div class="panjat__score_section">
                        <img src="{{ asset('images/lomba/mobile/win-leaderboard.png') }}" alt="">
                        <p id="skorUser"></p>
                    </div>
                    {{-- <img src="{{ asset('images/lomba/mobile/win-time.png') }}" alt="">
                    <img src="{{ asset('images/lomba/mobile/win-leaderboard.png') }}" alt=""> --}}
                </div>

                <div class="mulai__btn">
                    <a href="{{ route('panjatLeaderboard') }}">
                        <img src="{{ asset('images/lomba/mobile/peringkat.png') }}" alt="">
                    </a>
                    <img src="{{ asset('images/lomba/mobile/main-lagi.png') }}" alt=""
                        onclick="window.location.reload()">
                </div>

            </div>
        </div>
    </div>

    {{-- Modal Lose --}}
    <div class="modal fade modal__mulai" id="modal_lose" data-backdrop="static" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <img src="{{ asset('images/lomba/panjat-pinang.png') }}" alt="" class="modal__logo">
                <img src="{{ asset('images/lomba/mobile/lose.png') }}" alt="" class="modal__title">

                <div class="modal__win">
                    <img src="{{ asset('images/lomba/mobile/baper-lose.png') }}" alt="">
                    <div class="panjat__score_section">
                        <img src="{{ asset('images/lomba/mobile/win-leaderboard.png') }}" alt="">
                        <p id="skorUser"></p>
                    </div>
                    {{-- <img src="{{ asset('images/lomba/mobile/win-leaderboard.png') }}" alt=""> --}}
                </div>

                <div class="mulai__btn">
                    <a href="{{ route('panjatLeaderboard') }}">
                        <img src="{{ asset('images/lomba/mobile/peringkat.png') }}" alt="">
                    </a>
                    <img src="{{ asset('images/lomba/mobile/main-lagi.png') }}" alt=""
                        onclick="window.location.reload()">
                </div>

            </div>
        </div>
    </div>

    {{-- </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main--> --}}



    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=806244510020586&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->



    <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
    </script>
    <!--begin::Global Config(global config for global JS scripts)-->
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#6993FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1E9FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <!--end::Global Theme Bundle-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>
    <script src="{{ asset('assets/js/pages/features/miscellaneous/blockui.js') }}"></script>
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <!--end::Page Scripts-->


    <script>
        function toCara() {
            $('#modal_mulai').modal('toggle')
            $('#modal_cara').modal('toggle')
        }

        // if (window.innerHeight < window.innerWidth) {
        //     alert("Please use Portrait Mode!");
        // }
    </script>

    <script>
        var screenHeight
        $(document).ready(function() {
            if (innerWidth <= 425 && innerWidth > 375) {
                screenHeight = 125
            } else if (innerWidth <= 375) {
                screenHeight = 110
            }
        });

        var myGamePiece;
        var myObstacles = [];
        var myScore;
        var myPrize;
        var score;
        var userScore;

        function startGame(nmModal) {
            // $(`#${nmModal}`).modal('toggle')
            $('#panjat_menu').addClass('d-none')
            $('#panjat_game').removeClass('d-none')
            myGamePiece = new component(125, 125, "badak", screenHeight, 600,
                'img'); //SUBJECT --> ganti ini dengan badakbaper (w,h,color,start-w,start-h)
            myGamePiece.gravity = 0.03;
            myScore = new component("18px", "Consolas", "#A2600A", 100, 55, "text"); //KOMPONEN SKOR
            myPrize = new component(10, 10, "green", 180, 200);
            myGameArea.start();

            let panjatCrowd = document.querySelector('#panjatCrowd')
            panjatCrowd.loop = true;
            panjatCrowd.play()
        }

        var myGameArea = {
            canvas: $("#panjat_overlay"),
            start: function() {
                this.canvas[0].width = innerWidth;
                this.canvas[0].height = innerHeight;
                this.context = this.canvas[0].getContext("2d");
                document.body.insertBefore(this.canvas[0], document.body.childNodes[0]);
                this.frameNo = 0;
                this.interval = setInterval(updateGameArea, 20);
            },
            clear: function() {
                this.context.clearRect(0, 0, this.canvas[0].width, this.canvas[0].height);
            },
            stop: function() {
                clearInterval(this.interval)
            }
        };

        function component(width, height, color, x, y, type) {
            this.type = type;
            if (type == "img") {
                this.image = new Image();
                this.image.src = "{{ asset('images/lomba/mobile/badak.gif') }}";
            }
            this.score = 0;
            this.width = width;
            this.height = height;
            this.speedX = 0;
            this.speedY = 0;
            this.x = x;
            this.y = y;
            this.gravity = 0;
            this.gravitySpeed = 0;
            this.update = function() {
                ctx = myGameArea.context;
                if (type == "img") {
                    ctx.drawImage(this.image,
                        this.x,
                        this.y,
                        this.width, this.height);
                } else if (this.type == "text") {
                    ctx.font = this.width + " " + this.height;
                    ctx.fillStyle = color;
                    ctx.fillText(this.text, this.x, this.y);
                } else {
                    ctx.fillStyle = color;
                    ctx.fillRect(this.x, this.y, this.width, this.height);
                }
            };
            this.newPos = function() {
                this.gravitySpeed += this.gravity;
                this.x += this.speedX;
                this.y += this.speedY + this.gravitySpeed;
                this.hitBottom();
                console.log('newPos')
            };
            this.hitBottom = function() {
                // var rockbottom = myGameArea.canvas[0].height - this.height;
                var rockbottom = myGameArea.canvas[0].height - 200;
                if (this.y > rockbottom) {
                    this.y = rockbottom;
                    this.gravitySpeed = 0;
                }

                console.log('hitbottom')
            };
            this.crashWith = function(otherobj) {
                var mytop = this.y + 20;
                var otherbottom = otherobj.y + otherobj.height;
                var crash = true;
                if (mytop > otherbottom) {
                    crash = false;
                }
                console.log('crash')
                return crash;


            };
        }

        function updateGameArea() {
            var x, height, gap, minHeight, maxHeight, minGap, maxGap;
            if (myGamePiece.crashWith(myPrize) || score < 1) {
                // WIN OR LOSE
                myGameArea.stop()
                if (score < 1) {
                    $('#skorUser').text(score)
                    $('#modal_lose').modal('toggle')
                    panjatCrowd.pause();
                    panjatCrowd.currentTime = 0;
                } else if (myGamePiece.crashWith(myPrize)) {
                    $('#skorUser').text(score)
                    sendPoint()
                    $('#modal_win').modal('toggle')
                    // console.log(score)
                    panjatCrowd.pause();
                    panjatCrowd.currentTime = 0;
                    let winAudio = document.querySelector('#winSound')
                    winAudio.play()
                }

                return;
            }
            myGameArea.clear();
            myGameArea.frameNo += 1;
            score = 1000 - myGameArea.frameNo;

            // userScore = (((score * 20) % 60000) / 1000).toFixed(0);

            // myScore.text = '00' + ':' +
            //     userScore + '.' + score;
            myScore.text = score;
            myScore.update();
            myPrize.update();
            myGamePiece.newPos();
            myGamePiece.update();
        }

        function everyinterval(n) {
            if ((myGameArea.frameNo / n) % 1 == 0) {
                return true;
            }
            return false;
        }

        function accelerate(n) {
            myGamePiece.gravity = n;
        }

        function accelerateDesktop(n) {
            myGamePiece.y += n * 28;

            var audio = document.createElement("audio");
            audio.src = "{{ asset('sounds/panjat-panjat.wav') }}";
            audio.addEventListener("ended", function() {
                document.removeChild(this);
            }, false);
            audio.play();
        }

        function accelerateMobile(n) {
            myGamePiece.y += n * 18;

            var audio = document.createElement("audio");
            audio.src = "{{ asset('sounds/panjat-panjat.wav') }}";
            audio.addEventListener("ended", function() {
                document.removeChild(this);
            }, false);
            audio.play();
        }


        const potongBaper = () => {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('joinPanjatPinang') }}',
                success: function(data) {
                    // console.log(data)
                    if (data.Status == "Success") {
                        let panjatAudio = document.querySelector('#panjatCount')
                        panjatAudio.play()

                        $('#modal_mulai').modal('toggle')
                        KTApp.blockPage({
                            overlayColor: '#000000',
                            state: 'primary',
                            message: 'Get Ready...'
                        });


                        setTimeout(function() {
                            KTApp.unblockPage();
                            startGame('modal_mulai')
                        }, 3000);
                    }
                },
                error: function(data) {
                    console.log(data)
                }

            });
        }

        const sendPoint = () => {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('endPanjatPinang') }}',
                data: {
                    time: score,
                },
                success: function(data) {
                    console.log(data)
                    // if (data.Status == "Success") {
                    //     let panjatAudio = document.querySelector('#panjatCount')
                    //     panjatAudio.play()
                    //     setTimeout(function() {
                    //         startGame('modal_mulai')
                    //     }, 3000);
                    // }
                },
                error: function(data) {
                    console.log(data)
                }

            });
        }
    </script>



</body>
<!--end::Body-->

</html>
