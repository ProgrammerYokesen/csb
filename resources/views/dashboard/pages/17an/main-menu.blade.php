@section('title')
    Lomba 17an
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom gutter-b teman_sobat_card mainmenu__top_card">
                    <div class="card-body">
                        <div class="mainmenu__header">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <a href="{{ route('hadiahPanjat') }}">
                                <button class="btn">Daftar Hadiah</button>
                            </a>
                        </div>

                        <div class="mainmenu__text">
                            <h1>YUK DAFTAR DISINI!</h1>
                            <p>PPKM (Pesta Penuh Kejutan Menarik) adalah rangkaian acara yang diadakan oleh Club Sobat Badak
                                dalam rangka merayakan HUT RI ke-76. Acara ini akan diisi dengan berbagai lomba menarik dan
                                juga konser online yang pastinya seru banget! Jadi, jangan lupa ikutan, ya!</p>

                            @if ($userRegistered == 0)
                                <button class="btn" data-toggle="modal" data-target="#modal_daftar">DAFTAR</button>
                            @else
                                <button class="btn" style="cursor: default">TERDAFTAR</button>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="lomba__thumb">
                    <div class="row g-3">
                        <div class="col-lg-4 lomba__thumb_img">
                            @if ($userRegistered == 0)
                                <img src="{{ asset('images/lomba/mainmenu/panjat.png') }}" alt="" class="img-fluid"
                                    style="opacity: 0.4">
                            @else
                                <a href="{{ route('panjatPinang') }}">
                                    <img src="{{ asset('images/lomba/mainmenu/panjat.png') }}" alt="" class="img-fluid"
                                        style="cursor: pointer">
                                </a>
                            @endif
                        </div>
                        <div class="col-lg-4 lomba__thumb_img">
                            @if ($userRegistered == 0)
                                <img src="{{ asset('images/lomba/mainmenu/pompa.png') }}" alt="" class="img-fluid"
                                    style="opacity: 0.4">
                            @else
                                <a href="{{ route('tambalBan') }}">
                                    <img src="{{ asset('images/lomba/mainmenu/pompa.png') }}" alt="" class="img-fluid"
                                        style="cursor: pointer">
                                </a>
                            @endif
                        </div>
                        <div class="col-lg-4 lomba__thumb_img">
                            @if ($userRegistered == 0)
                                <img src="{{ asset('images/lomba/mainmenu/kerupuk.png') }}" alt="" class="img-fluid"
                                    style="opacity: 0.4">
                            @else
                                <img src="{{ asset('images/lomba/mainmenu/kerupuk.png') }}" alt="" class="img-fluid"
                                    data-toggle="modal" data-target="#modal_kerupuk" style="cursor: pointer">
                            @endif
                        </div>
                        <div class="col-lg-4 lomba__thumb_img">
                            @if ($userRegistered == 0)
                                <img src="{{ asset('images/lomba/mainmenu/fashion.png') }}" alt="" class="img-fluid"
                                    style="opacity: 0.4">
                            @else
                                <img src="{{ asset('images/lomba/mainmenu/fashion.png') }}" alt="" class="img-fluid"
                                    data-toggle="modal" data-target="#modal_fashion" style="cursor: pointer">
                            @endif
                        </div>
                        <div class="col-lg-4 lomba__thumb_img">
                            @if ($userRegistered == 0)
                                <img src="{{ asset('images/lomba/mainmenu/tebak.png') }}" alt="" class="img-fluid"
                                    style="opacity: 0.4">
                            @else
                                <img src="{{ asset('images/lomba/mainmenu/tebak.png') }}" alt="" class="img-fluid"
                                    data-toggle="modal" data-target="#modal_tebak" style="cursor: pointer">
                            @endif
                        </div>
                        <div class="col-lg-4 lomba__thumb_img">
                            @if ($userRegistered == 0)
                                <img src="{{ asset('images/lomba/mainmenu/lagu.png') }}" alt="" class="img-fluid"
                                    style="opacity: 0.4">
                            @else
                                <img src="{{ asset('images/lomba/mainmenu/lagu.png') }}" alt="" class="img-fluid"
                                    data-toggle="modal" data-target="#modal_lagu" style="cursor: pointer">
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    {{-- Modal Daftar --}}
    <div class="modal fade mainmenu__modal_daftar" id="modal_daftar" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h6>Dengan mendaftar di Acara Pesta Penuh Kejutan Menarik, Kamu wajib mengikuti Zoom CSB pada tanggal 17
                        Agustus</h6>
                    <a href="{{ route('preRegisterHUTRI') }}">
                        <button class="btn">OK!</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Confirm --}}
    <div class="modal fade mainmenu__modal_daftar" id="modal_confirm_kerupuk" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h6>Dengan mendaftar di Event Lomba Makan Kerupuk ini, Kamu mengikuti semua aturan yang ada pada Event
                        Lomba Makan Kerupuk ini. Kamu wajib
                        mengikuti Zoom CSB pada tanggal 17 Agustus</h6>
                    <a href="{{ route('registerMakanKrupuk') }}">
                        <button class="btn">OK!</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Confirm --}}
    <div class="modal fade mainmenu__modal_daftar" id="modal_confirm_fashion" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h6>Dengan mendaftar di Event CSB Fashion Show ini, Kamu mengikuti semua aturan yang ada pada Event CSB
                        Fashion Show ini. Kamu wajib
                        mengikuti Zoom CSB pada tanggal 17 Agustus</h6>
                    <a href="{{ route('registerFashionShow') }}">
                        <button class="btn">OK!</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Confirm --}}
    <div class="modal fade mainmenu__modal_daftar" id="modal_confirm_tebak" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h6>Dengan mendaftar di Event Tebak Rumah & Pakaian Adat ini, Kamu mengikuti semua aturan yang ada pada
                        Event Tebak Rumah & Pakaian Adat ini. Kamu wajib
                        mengikuti Zoom CSB pada tanggal 17 Agustus</h6>
                    <a href="{{ route('registerTebakan') }}">
                        <button class="btn">OK!</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Confirm --}}
    <div class="modal fade mainmenu__modal_daftar" id="modal_confirm_lagu" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h6>Dengan mendaftar di Event Tebak Lagu Nasional ini, Kamu mengikuti semua aturan yang ada pada Event
                        Tebak Lagu Nasional ini. Kamu wajib
                        mengikuti Zoom CSB pada tanggal 17 Agustus</h6>
                    <a href="{{ route('registerTebakLagu') }}">
                        <button class="btn">OK!</button>
                    </a>
                </div>
            </div>
        </div>
    </div>



    {{-- Modal Kerupuk --}}
    <div class="modal fade mainmenu__modal" id="modal_kerupuk" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="mainmenu__modal_content">
                    <img src="{{ asset('images/lomba/mainmenu/kerupuk-title.png') }}" alt=""
                        class="mainmenu__modal_title">

                    <div class="mainmenu__modal_inner">
                        <p>Ini bukan sembarang Lomba Makan Kerupuk karen di sini Sobat Badak akan bertanding dengan Sobat
                            Badak
                            lainnya dari seluruh Indonesia melalui Zoom Club Sobat Badak, loh!
                            Jam Acara: 16:30 WIB</p>

                        <img src="{{ asset('images/lomba/mainmenu/cara.png') }}" alt="">

                        <div class="mainmenu__cara text-left">
                            <ol>
                                <li>Peserta wajib menyediakan kerupuk sesuai dengan gambar yang telah diumumkan di postingan
                                    HUT
                                    RI pada Instagram Club Sobat Badak</li>
                                <li>Peserta wajib memakan 2 kerupuk
                                </li>
                                <li>Gunakan fitur “Raise Hand” di Zoom jika kamu telah selesai</li>
                                <li>Peserta diwajibkan untuk menyalakan kamera
                                </li>
                                <li>Pemenang ditentukan berdasarkan waktu tercepat dalam menghabiskan 2 kerupuk</li>
                            </ol>
                        </div>

                        @if ($makanKrupuk == 0)
                            <button class="btn" data-toggle="modal" data-target="#modal_confirm_kerupuk"
                                onclick="daftarLomba('modal_kerupuk')">Ikutan!</button>
                        @else
                            <button class="btn" disabled style="cursor: default">Kamu berhasil Ikutan!</button>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{-- Modal Fashion --}}
    <div class="modal fade mainmenu__modal" id="modal_fashion" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="mainmenu__modal_content">
                    <img src="{{ asset('images/lomba/mainmenu/fashion-title.png') }}" alt=""
                        class="mainmenu__modal_title">

                    <div class="mainmenu__modal_inner">
                        <p>Sobat Badak bisa menampilkan kreativitas dalam berpakaian dengan mengikuti CSB Fashion Show ini.
                            Ingat! Harus ada unsur merah putihnya ya!
                            Jam Acara: 16:30 WIB</p>

                        <img src="{{ asset('images/lomba/mainmenu/cara.png') }}" alt="">

                        <div class="mainmenu__cara text-left">
                            <ol>
                                <li>Peserta wajib menampilkan kostum dengan membawa tema Kemerdekaan Indonesia (menggunakan
                                    warna merah putih) sekreatif mungkin.
                                </li>
                                <li>Setiap peserta akan mendapatkan kesempatan selama 4-5 menit untuk berjalan dan
                                    menjelaskan mengenai makna keseluruhan kostum yang digunakan
                                </li>
                                <li>Peserta diwajibkan untuk menyalakan kamera
                                </li>
                            </ol>
                        </div>


                        @if ($fashionShow == 0)
                            <button class="btn" data-toggle="modal" data-target="#modal_confirm_fashion"
                                onclick="daftarLomba('modal_fashion')">Ikutan!</button>
                        @else
                            <button class="btn" disabled style="cursor: default">Kamu berhasil Ikutan!</button>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{-- Modal Tebak --}}
    <div class=" modal fade mainmenu__modal" id="modal_tebak" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="mainmenu__modal_content">
                    <img src="{{ asset('images/lomba/mainmenu/tebak-title.png') }}" alt="" class="mainmenu__modal_title">

                    <div class="mainmenu__modal_inner">
                        <p>Sobat Badak masih ingat dengan berbagai Pakaian/Rumah Adat yang kita pelajari
                            dulu? Buktikan
                            kemampuan kamu dengan mencoba menebak pertanyaan-pertanyaan dari kita!
                            Jam Acara: 16:30 WIB</p>

                        <img src="{{ asset('images/lomba/mainmenu/cara.png') }}" alt="">

                        <div class="mainmenu__cara text-left">
                            <ol>
                                <li>Peserta yang ingin menjawab diwajibkan untuk menggunakan fitur
                                    “Raise Hand”
                                </li>
                                <li>Host akan memilih peserta yang berhak menjawab
                                </li>
                                <li>Peserta dilarang menjawab sebelum dipersilahkan oleh Host
                                </li>
                                <li>Apabila peserta salah menjawab, maka akan dilempar ke peserta lain
                                    yang dipilih oleh
                                    Host

                                </li>
                                <li>Peserta diwajibkan untuk menyalakan kamera</li>
                            </ol>
                        </div>

                        @if ($tebakan == 0)
                            <button class="btn" data-toggle="modal" data-target="#modal_confirm_tebak"
                                onclick="daftarLomba('modal_tebak')">Ikutan!</button>
                        @else
                            <button class="btn" disabled style="cursor: default">Kamu berhasil Ikutan!</button>
                        @endif


                    </div>

                </div>
            </div>
        </div>
    </div>

    {{-- Modal Lagu --}}
    <div class=" modal fade mainmenu__modal" id="modal_lagu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="mainmenu__modal_content">
                    <img src="{{ asset('images/lomba/mainmenu/lagu-title.png') }}" alt="" class="mainmenu__modal_title">

                    <div class="mainmenu__modal_inner">
                        <p>Sobat Badak merasa hafal semua Lagu Nasional? Buktikan
                            kemampuanmu dengan menebak Lagu Nasional
                            Indonesia yang diucapkan dengan Google Voice
                            Jam Acara: 16:30 WIB</p>

                        <img src="{{ asset('images/lomba/mainmenu/cara.png') }}" alt="">

                        <div class="mainmenu__cara text-left">
                            <ol>
                                <li>Lagu akan diputarkan menggunakan suara Google
                                    Voice</li>
                                <li>Lagu yang diputarkan adalah Lagu Nasional

                                </li>
                                <li>Peserta harus menggunakan fitur “Raise Hand” di
                                    Zoom untuk menjawab
                                </li>
                                <li>Host akan memilih peserta tercepat yang
                                    menggunakan fitur “Raise Hand” untuk menjawab

                                </li>
                                <li>Peserta dilarang menjawab sebelum dipersilahkan
                                    oleh Host
                                </li>
                                <li>Peserta diwajibkan untuk menyalakan kamera</li>
                                <li>Jika ada peserta yang menjawab salah, maka akan
                                    dilempar ke peserta berikutnya
                                </li>
                            </ol>
                        </div>


                        @if ($tebakLagu == 0)
                            <button class="btn" data-toggle="modal" data-target="#modal_confirm_lagu"
                                onclick="daftarLomba('modal_lagu')">Ikutan!</button>
                        @else
                            <button class="btn" disabled style="cursor: default">Kamu berhasil Ikutan!</button>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>

    <script>
        function comingSoon() {
            // alert('Coming Soon!')
            Swal.fire("Coming Soon! :D", "", "info");
        }

        function daftarLomba(nama) {
            $(`#${nama}`).modal('toggle')
        }
    </script>

    @if (Session::has('lomba'))
        <script>
            Swal.fire("Berhasil Daftar!", "Ditunggu di Zoom yaa! :)", "success");
        </script>
    @endif
@endsection
