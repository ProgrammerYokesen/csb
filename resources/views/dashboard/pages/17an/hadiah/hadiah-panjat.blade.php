@section('title')
    Hadiah 17an
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">

                <div class="hadiah__header">
                    <h3>Daftar Hadiah Panjat Pinang</h3>
                </div>






                <div class="row justify-content-center mb-3">
                    <div class="col-lg-6 col-6 text-center">
                        <a href="{{ route('hadiahPanjat') }}">
                            <img src="{{ asset('images/lomba/mainmenu/panjat.png') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="col-lg-6 col-6 text-center">
                        <a href="{{ route('hadiahPompa') }}">
                            <img src="{{ asset('images/lomba/mainmenu/pompa.png') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>



                <div class="card card-custom gutter-b teman_sobat_card">
                    <div class="card-body">

                        <div class="row g-5">
                            <div class="col-lg-6">
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat1.jpg') }}" alt="">
                                    <h1>ECOHOME Stand Mixer ESM-999</h1>
                                    <div class="hadiah__number">
                                        <img src="{{ asset('images/lomba/hadiah/1.svg') }}" alt="">
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat2.jpg') }}" alt="">
                                    <h1>Gaming Chair Sapporo NORWICH</h1>
                                    <div class="hadiah__number">
                                        <img src="{{ asset('images/lomba/hadiah/2.svg') }}" alt="">
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat3.jpg') }}" alt="">
                                    <h1>Kurumi Home Mini Steam Oven Toaster KH 300</h1>
                                    <div class="hadiah__number">
                                        <img src="{{ asset('images/lomba/hadiah/3.svg') }}" alt="">
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat4.jpg') }}" alt="">
                                    <h1>Remington Catokan Mineral Glow</h1>
                                    <div class="hadiah__number">
                                        4
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat5.jpg') }}" alt="">
                                    <h1>Philips Handheld Steamer 3000 Series</h1>
                                    <div class="hadiah__number">
                                        5
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat6.jpg') }}" alt="">
                                    <h1>Russell Hobbs Compact Home Slow Cooker</h1>
                                    <div class="hadiah__number">
                                        6
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat7.jpg') }}" alt="">
                                    <h1>Gamemax HG3500 Pro USB 7.1 RGB Gaming Headset</h1>
                                    <div class="hadiah__number">
                                        7
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat8.jpg') }}" alt="">
                                    <h1>BOLDe Super Pan Set Beige</h1>
                                    <div class="hadiah__number">
                                        8
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat9.jpg') }}" alt="">
                                    <h1>Oxone Hand Blender & Chopper</h1>
                                    <div class="hadiah__number">
                                        9
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/panjat10.jpg') }}" alt="">
                                    <h1>Deerma Vacuum Cleaner UV-C Penyedot Debu Tungau</h1>
                                    <div class="hadiah__number">
                                        10
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/kaos.png') }}" alt="">
                                    <h1>Kaos CSB</h1>
                                    <div class="hadiah__number">
                                        11
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/kaos.png') }}" alt="">
                                    <h1>Kaos CSB</h1>
                                    <div class="hadiah__number">
                                        12
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/kaos.png') }}" alt="">
                                    <h1>Kaos CSB</h1>
                                    <div class="hadiah__number">
                                        13
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/kaos.png') }}" alt="">
                                    <h1>Kaos CSB</h1>
                                    <div class="hadiah__number">
                                        14
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/kaos.png') }}" alt="">
                                    <h1>Kaos CSB</h1>
                                    <div class="hadiah__number">
                                        15
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/tumblr.png') }}" alt="">
                                    <h1>Tumblr CSB (Warna Random)</h1>
                                    <div class="hadiah__number">
                                        16
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/tumblr.png') }}" alt="">
                                    <h1>Tumblr CSB (Warna Random)</h1>
                                    <div class="hadiah__number">
                                        17
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/tumblr.png') }}" alt="">
                                    <h1>Tumblr CSB (Warna Random)</h1>
                                    <div class="hadiah__number">
                                        18
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/tumblr.png') }}" alt="">
                                    <h1>Tumblr CSB (Warna Random)</h1>
                                    <div class="hadiah__number">
                                        19
                                    </div>
                                </div>
                                <div class="hadiah__card">
                                    <img src="{{ asset('images/lomba/hadiah/tumblr.png') }}" alt="">
                                    <h1>Tumblr CSB (Warna Random)</h1>
                                    <div class="hadiah__number">
                                        20
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->


    {{-- Modal Play --}}
    <div class="modal fade modal_panjat_play" id="modal_play" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <a href="{{ route('panjatPinangGame') }}">
                    <img src="{{ asset('images/lomba/main-btn.png') }}" alt="">
                </a>
                {{-- <img src="{{ asset('images/lomba/main-btn.png') }}" alt="" onclick="toPotong()"> --}}

            </div>
        </div>
    </div>

    <!-- Modal Potong Baper Poin-->
    <div class="modal fade modal_potong" id="modal_potong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal_ctn">
                    {{-- <h3>Kamu membutuhkan 1 Tiket 17an untuk bermain Panjat Pinang</h3>
                    <p>1 Tiket 17an = 1.000 Baper Poin. Baper Poin akan terpotong saat kamu klik tombol “MULAI”.</p> --}}
                    <h3>Hari ini Mimin kasih GRATIS untuk cobain Game Panjat Pinang</h3>
                    <p>Tapi mulai tanggal 17 nanti bayar pake baper poin yaa</p>
                </div>

                {{-- <a href="javascript:void(0)" class="mulai_btn" onclick="startGame('modal_potong')"> --}}
                <a href="{{ route('panjatPinangGame') }}" class="mulai_btn">
                    <img src="{{ asset('images/lomba/mulai-btn.png') }}" alt="">
                </a>

            </div>
        </div>
    </div>

    <!-- Modal Cara Bermain-->
    <div class="modal fade modal_potong" id="modal_bermain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">

                <img src="{{ asset('images/lomba/cara.png') }}" alt="" class="cara_bermain">

                <img src="{{ asset('images/lomba/close.png') }}" alt="" class="close__btn" data-dismiss="modal">

                <div class="modal_ctn">
                    <ol class="text-left">
                        <li>Tekan tombol hijau secara terus menerus</li>
                        <li>Apabila kamu berhenti menekan, karaktermu akan turun secara perlahan</li>
                        <li>Kamu diharuskan untuk terus menekan tombol hijau agar karaktermu bisa sampai ke puncak</li>
                        <li>Pastikan kamu mencapai puncak dengan waktu secepat mungkin</li>
                        <li>Pemenangnya ditentukan berdasarkan waktu tercepat mencapai puncak</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('pageJS')

@endsection
