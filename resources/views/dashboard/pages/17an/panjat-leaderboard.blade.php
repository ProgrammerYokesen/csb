@section('title')
    Lomba 17an
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom gutter-b teman_sobat_card">
                    <div class="card-body">

                        <div class="panjat_header">
                            <a href="{{ route('panjatPinang') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            
                            <a href="{{ route('hadiahPanjat') }}">
                                <button class="btn">Daftar Hadiah</button>
                            </a>
                        </div>

                        <div class="panjat__leaderboard">

                            <img src="{{ asset('images/lomba/peringkat.png') }}" alt="" class="leaderboard__label">
                            <img src="{{ asset('images/lomba/panjat-pinang.png') }}" alt="" class="leaderboard__logo">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panjat_klasemen">
                                        <table class="table table-striped table-borderless">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th style="border-top-left-radius: 10px">Posisi</th>
                                                    <th>Nama</th>
                                                    <th style="border-top-right-radius: 10px">Skor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @for ($i = 0; $i < 10; $i++)
                                                    <tr>
                                                        <td>{{ $i + 1 }}</td>
                                                        <td>{{ $leaderBoard[$i]->name }}</td>
                                                        <td>{{ $leaderBoard[$i]->time }}</td>
                                                    </tr>
                                                @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="panjat_klasemen">
                                        <table class="table table-striped table-borderless">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th style="border-top-left-radius: 10px">Posisi</th>
                                                    <th>Nama</th>
                                                    <th style="border-top-right-radius: 10px">Skor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @for ($i = 10; $i < 20; $i++)
                                                    <tr>
                                                        <td>{{ $i + 1 }}</td>
                                                        <td>{{ $leaderBoard[$i]->name }}</td>
                                                        <td>{{ $leaderBoard[$i]->time }}</td>
                                                    </tr>
                                                @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
