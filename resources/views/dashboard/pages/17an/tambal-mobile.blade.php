<!DOCTYPE html>
<html lang="en">

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak | Tambal Ban Online </title>
    @laravelPWA

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta property="og:title" content="Club Sobat Badak" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://sobatbadak.club" />
    <meta property="og:image" content="https://sobatbadak.club/images/share.png" />
    <meta property="og:description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />


    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, shrink-to-fit=no" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('user-dashboard/style.css') }}">
    <link rel="stylesheet" href="{{ asset('user-dashboard/tambal.css') }}">

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '806244510020586');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199518915-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199518915-2');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NVTPTBJ');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 695110688 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-695110688"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-695110688');
    </script>


</head>

<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVTPTBJ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=806244510020586&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <section id="tambal_mobile">
        {{-- <img class="bar__img" src="{{ asset('images/bar_challenge.png') }}" alt=""> --}}
        <canvas id="game_lay">
        </canvas>
        <img class="jamet__img" src="{{ asset('images/jamet_pompa.png') }}" alt="">
        <img class="ban__img" src="{{ asset('images/ban_1.png') }}" alt="">
        {{-- <img class="pointer__img" src="{{ asset('images/ban_pointer.png') }}" alt=""> --}}
        <img class="lifebar__img" src="{{ asset('images/lifebar.png') }}" alt="">
        <img class="bh1 bh__img" src="{{ asset('images/ban_health.png') }}" alt="">
        <img class="bh2 bh__img" src="{{ asset('images/ban_health.png') }}" alt="">
        <img class="bh3 bh__img" src="{{ asset('images/ban_health.png') }}" alt="">
        <div class="d-flex justify-content-between header__ban_mobile">
            <div class="d-flex">
                <div class="lifelove">
                    <img class="lovebar__img_mobile" src="{{ asset('images/lifebar_love.png') }}" alt="">
                    <img class="love1 love__img_img" src="{{ asset('images/love_bar.png') }}" alt="">
                    <img class="love2 love__img_img" src="{{ asset('images/love_bar.png') }}" alt="">
                    <img class="love3 love__img_img" src="{{ asset('images/love_bar.png') }}" alt="">
                </div>
                <img class="time__img_mobile" src="{{ asset('images/timebar_ban.png') }}" alt="">
                <div class="timer_text d-flex">
                    <div class="t_min">00:</div>
                    <div class="t_sec">00:</div>
                    <div class="t_ms">00</div>
                </div>
            </div>
            <img class="help__img_mobile" onclick="$('#caramain__modal').modal('toggle')"
                src="{{ asset('images/help__ban.png') }}" alt="" style="cursor: pointer">
        </div>
        <div class="d-flex justify-content-around btn__main_mobile">
            <img onclick="stop()" class="stop__img_mobile" src="{{ asset('images/ban_stop.png') }}" alt="">
            <img onclick="run()" class="go__img_mobile" src="{{ asset('images/ban_go.png') }}" alt="">
            <img onclick="startGame()" class="playagain__img_mobile" src="{{ asset('images/ban_go.png') }}" alt="">
        </div>
    </section>

    {{-- <section id="tambal_mobile_landscape">
        <h1>Tolong putar hp anda menjadi potrait!</h1>
    </section> --}}
    <div class="modal fade" id="caramain__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <ol>
                        <li>
                            Kamu diwajibkan untuk memompa 3 ban dengan waktu secepat mungkin
                        </li>
                        <li>
                            Tekan tombol hijau hingga mencapai garis berwarna hijau
                        </li>
                        <li>
                            Jika sudah mencapai garis hijau, tekan tombol merah untuk berhenti
                        </li>
                        <li>
                            Jika kamu berhenti di garis kuning atau garis merah, maka kesempatanmu akan berkurang satu
                        </li>
                        <li>
                            Di games ini, kamu memiliki tiga kesempatan dan bisa dilihat dari jumlah hati yang kamu
                            miliki
                        </li>
                        <li>
                            Pemenang ditentukan berdasarkan waktu tercepat dalam memompa 3 ban secara tepat
                        </li>
                    </ol>
                    <img class="modal__close" data-dismiss="modal"
                        onclick="$('#play__modal').modal('toggle');$('#caramain__modal').modal('toggle')"
                        src="{{ asset('images/modal__close.png') }}" alt="">
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/modal__label.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white">
                                    Cara Bermain
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal__bg_light fade" id="play__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content text-center">
                <img id="play__btn" onclick="initGame()" src="{{ asset('images/play__ban.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="modal modal__bg_light fade" id="warning__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <p class="modal__title_black">
                        Kamu membutuhkan 1 Tiket 17an untuk bermain Pompa Ban Online
                    </p>
                    <p class="modal__desc_gray">
                        1 Tiket 17an = 1.000 Baper Poin. Baper Poin akan terpotong saat kamu klik tombol “MULAI”.
                    </p>
                    <div class="btn__result d-flex justify-content-center">
                        <img onclick="$('#caramain__modal').modal('toggle');$('#warning__modal').modal('toggle')"
                            class="peringkat__btn mx-4" src="{{ asset('images/lomba/mobile/cara-btn.png') }}" alt="">
                        <a href="{{ route('goBanMobile') }}">
                            <img class="lanjut__btn mx-4" src="{{ asset('images/play_btn__modal.png') }}" alt="">
                        </a>
                    </div>
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/modal__label.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white">
                                    POMPA BAN
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="gagal__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <p class="modal__title_black">
                        Tiket yang kamu butuhkan tidak mencukupi
                    </p>
                    <p class="modal__desc_gray">
                        Ayo tambah baper poin kamu dengan melakukan tugas harian di menu dashboard!
                    </p>
                    <a href="{{ route('leaderboardTambalBan') }}">
                        <img class="modal__play" src="{{ asset('images/modal__close.png') }}" alt="">
                    </a>
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/modal__label.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white">
                                    GAGAL
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="result__modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="frame__board text-center">
                    <img class="jamet_result" src="{{ asset('images/fail_jamet.png') }}" alt="">
                    <div class="d-flex result__ban justify-content-center">
                        <img class="mx-1 res_ban rb1" src="{{ asset('images/ban_health.png') }}" alt="">
                        <img class="mx-1 res_ban rb2" src="{{ asset('images/ban_health.png') }}" alt="">
                        <img class="mx-1 res_ban rb3" src="{{ asset('images/empty_ban.png') }}" alt="">
                    </div>
                    <div class="res__score">
                        <img class="res__score__img" src="{{ asset('images/result_bar.png') }}" alt="">
                        <p class="modal__title_black txt_res_score">
                            00:30:68
                        </p>
                    </div>
                    <div class="btn__result d-flex justify-content-center">
                        <a href="{{ route('leaderboardTambalBan') }}">
                            <img class="peringkat__btn mx-4" src="{{ asset('images/peringkat__btn.png') }}" alt="">
                        </a>
                        <img class="lanjut__btn mx-4" src="{{ asset('images/lomba/mobile/main-lagi.png') }}" alt=""
                            onclick="window.location.reload()">
                    </div>
                    <div class="pos_modal__label">
                        <div class="modal_title__label">
                            <img src="{{ asset('images/modal__label.png') }}" alt="">
                            <div class="pos__modal_title">
                                <p class="modal__title_white title_of_modal">
                                    BERHASIL
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <audio src="{{ asset('sounds/panjat-win.wav') }}" id="winSound"></audio>
    <audio src="{{ asset('sounds/pompa.wav') }}" id="pompaSound"></audio>
    <audio src="{{ asset('sounds/crowd.wav') }}" id="crowdSound"></audio>
    <audio src="{{ asset('sounds/meledak.wav') }}" id="meledakSound"></audio>

    <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
    </script>
    <!--begin::Global Config(global config for global JS scripts)-->
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#6993FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1E9FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>
    {{-- Winwheel JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js"
        integrity="sha512-UxP+UhJaGRWuMG2YC6LPWYpFQnsSgnor0VUF3BHdD83PS/pOpN+FYbZmrYN+ISX8jnvgVUciqP/fILOXDjZSwg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    @if (Session::has('go'))
        <script>
            var myGamePiece = [];
            let score = []
            let totalScore = 0
            let pointer = new Image()
            pointer.src = "{{ asset('images/ban_pointer.png') }}"
            let imgIndex = 1
            var life = [$(".love3"), $(".love2"), $(".love1")]
            let idxLife, banCounter = 0
            var ban = [$(".bh3"), $(".bh2"), $(".bh1")]
            var ms = 0
            var sc = 0
            var mn = 0
            var clickable = true;
            var step = 10
            var stopp = false
            var batasAtas = innerWidth < 400 ? innerWidth - 60 : innerWidth - 90
            var batasBawah = innerWidth < 400 ? 30 : 50

            let lowGravity = 0.06
            let middleGravity = 0.01
            let highGravity = 0.0000009
            let vx = batasBawah
            let gravitySpeed = 0
            $(`.playagain__img_mobile`).hide()

            var myGameArea = {
                canvas: $("#game_lay"),
                start: function() {
                    this.canvas[0].width = innerWidth;
                    this.canvas[0].height = innerHeight;
                    this.context = this.canvas[0].getContext("2d");
                    this.frameNo = 0;
                    this.interval = setInterval(animate, 20)
                },
                clear: function() {
                    this.context.clearRect(0, 0, this.canvas[0].width, this.canvas[0].height)
                }
            };

            $(function() {
                let crowd = document.querySelector('#crowdSound')
                crowd.play()
                let pompa = document.querySelector('#pompaSound')
                pompa.play()

                $(".jamet__img").attr("src", "{{ asset('images/jamet_gif.gif') }}");
                if (innerHeight < 750) {
                    $('#game_lay').css("background", "url(../images/base_mobile_400.png)")
                    $('#game_lay').css("background-repeat", "no-repeat")
                    $('#game_lay').css("background-position", "center")
                    $('#game_lay').css("background-size", "cover")
                    $('#game_lay').css("object-fit", "cover")
                }
                startGame()
                stopWatch()
                $('.playagain__img_mobile').hide()
            })

            function sendScore(finalScore, totalBan) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('endTambalBan') }}",
                    type: "POST",
                    data: {
                        time: finalScore,
                        totalBan: totalBan
                    },
                    success: function(res) {
                        if (res.Status == "Success") {
                            // show modal
                            $("#result__modal").modal('toggle')
                        } else {
                            $("#result__modal").modal('toggle')
                        }
                    },
                    error: function(err) {
                        console.log(err)
                        // show modal
                        $("#result__modal").modal('toggle')
                    }
                })
            }

            function changeBan(uri) {
                var imgBan = $(".ban__img");
                imgBan.fadeOut('fast', function() {
                    imgBan.attr("src", uri);
                    imgBan.fadeIn('fast');
                })
            }

            function formatTime(val) {
                val = parseInt(val)
                return val >= 10 ? `${val}` : `0${val}`
            }

            function stopWatch() {
                var timerr = setInterval(function() {
                    if (stopp) {
                        clearInterval(timerr)
                    }

                    ms += 10
                    myGameArea.frameNo += 1
                    if (ms > 999) {
                        sc++
                        ms = 0
                    }
                    if (sc > 59) {
                        mn++
                        sc = 0
                    }

                    let minimized = ms > 100 ? ms / 10 : ms
                    $(".t_ms").text(formatTime(minimized))
                    $(".t_sec").text(`${formatTime(sc)}:`)
                    $(".t_min").text(`${formatTime(mn)}:`)
                }, 10)
            }

            function startGame() {

                clickable = true

                // make layout
                myGameArea.start()

                // draw pointer 
                if (innerHeight < 750) {
                    myGameArea.context.drawImage(pointer, vx, 505, 25, 25)
                } else {
                    myGameArea.context.drawImage(pointer, vx, 605, 25, 25)
                }

                // show run button 
                $('.go__img_mobile').show()
                $('.playagain__img_mobile').hide()

                // run stopwatch
                stopWatch()
            }

            function run() {
                vx += (1.6 * 20)
                myGameArea.clear()
                if (innerHeight < 750) {
                    myGameArea.context.drawImage(pointer, vx, 505, 25, 25)
                } else {
                    myGameArea.context.drawImage(pointer, vx, 605, 25, 25)
                }
            }

            function setLife() {
                if (life.length == 1) {
                    life[0].css('opacity', '0.2')
                    life.shift()
                } else {
                    let tempItem = life.shift()
                    tempItem.addClass('blink_me')
                    // blink the love for 2.5s 
                    setTimeout(function() {
                        tempItem.removeClass('blink_me')
                        tempItem.css('opacity', '0.2')
                    }, 2000)
                }
            }

            function setBan() {
                ban[0].css('opacity', '1')
                ban.shift()
                banCounter++
            }

            function setScore(val) {
                score.push(val * 20)
                totalScore += (val * 20)
            }

            function stop() {
                gravitySpeed = 0

                if (vx < 170 || vx > 310) {
                    // if hit yellow or red bar
                    setLife()
                    vx = batasBawah

                } else {
                    // hit the green area 
                    setBan()
                    vx = batasBawah

                }

                if (life.length == 0 || ban.length == 0) {
                    $('.go__img_mobile').hide()
                    $('.playagain__img_mobile').hide()
                    stopp = true

                    // set score
                    // life == 0
                    if (life.length == 0) {
                        setScore(0)
                        mn = 0
                        sc = 0
                        ms = 0
                        $('.jamet_result').attr("src", "{{ asset('images/fail_jamet.png') }}");
                        $('.title_of_modal').text("GAGAL")
                    }
                    // success
                    else {
                        setScore(myGameArea.frameNo)
                        $('.jamet_result').attr("src", "{{ asset('images/success_jamet.png') }}");
                        $('.title_of_modal').text("BERHASIL")
                        let winAudio = document.querySelector('#winSound')
                        winAudio.play()
                    }

                    // set image of tires in modal
                    for (let index = 1; index <= 3; index++) {
                        if (index <= banCounter) {
                            // succeded ban
                            $(`.rb${index}`).attr("src", "{{ asset('images/ban_health.png') }}");
                        } else {
                            // fail ban 
                            $(`.rb${index}`).attr("src", "{{ asset('images/empty_ban.png') }}");
                        }
                    }

                    // set text score
                    $('.txt_res_score').text(`${formatTime(mn)}:${formatTime(sc)}:${formatTime(ms/10)}`)

                    // should add code to end the game and show modal final score
                    sendScore(totalScore, banCounter)
                    // show modal
                    // $("#result__modal").modal('toggle')

                } else {
                    // change button
                    $('.playagain__img_mobile').show()
                    $('.go__img_mobile').hide()
                }

                // stop game and set score
                myGameArea.context = null

                // stop time
                clearInterval(myGameArea.interval)

                // prevent click stop 
                clickable = false
            }

            function animate() {
                if (isFinish()) {
                    vx = batasBawah
                    gravitySpeed = 0
                    let meledak = document.querySelector('#meledakSound')
                    meledak.play()
                    setTimeout(function() {
                        meledak.pause()
                        meledak.currentTime = 0
                    }, 2000)

                    // stop game and set score 
                    setScore(myGameArea.frameNo)
                    myGameArea.context = null

                    // stop time
                    clearInterval(myGameArea.interval)

                    // explosion of the tire 
                    imgIndex = 5
                    changeBan("{{ asset('images/boom.png') }}")

                    setLife()

                    //change button
                    $('.playagain__img_mobile').show()
                    $('.go__img_mobile').hide()

                    if (life.length == 0) {
                        $('.go__img_mobile').hide()
                        $('.playagain__img_mobile').hide()
                        stopp = true
                        setScore(0)
                        mn = 0
                        sc = 0
                        ms = 0
                        $('.jamet_result').attr("src", "{{ asset('images/fail_jamet.png') }}");
                        $('.title_of_modal').text("GAGAL")
                        $('.go__img').hide()
                        $('.playagain__img').hide()

                        // set image of tires in modal
                        for (let index = 1; index <= 3; index++) {
                            if (index <= banCounter) {
                                // succeded ban
                                $(`.rb${index}`).attr("src", "{{ asset('images/ban_health.png') }}");
                            } else {
                                // fail ban 
                                $(`.rb${index}`).attr("src", "{{ asset('images/empty_ban.png') }}");
                            }
                        }

                        // set text score
                        $('.txt_res_score').text(`${formatTime(mn)}:${formatTime(sc)}:${formatTime(ms/10)}`)

                        // should add code to end the game and show modal final score
                        sendScore(totalScore, banCounter)
                    }
                } else if (isBottom()) {
                    vx = batasBawah
                    gravitySpeed = 0
                } else {
                    if (vx <= 100) {
                        if (imgIndex != 1) {
                            imgIndex = 1
                            changeBan("{{ asset('images/ban_1.png') }}")
                        }
                        gravitySpeed += lowGravity
                    } else if (vx <= 200) {
                        if (imgIndex != 2) {
                            imgIndex = 2
                            changeBan("{{ asset('images/ban_2.png') }}")
                        }
                        gravitySpeed += lowGravity
                    } else if (vx <= 310) {
                        if (imgIndex != 3) {
                            imgIndex = 3
                            changeBan("{{ asset('images/ban_3.png') }}")
                        }
                        gravitySpeed += middleGravity
                    } else if (vx > 310) {
                        if (imgIndex != 4) {
                            imgIndex = 4
                            changeBan("{{ asset('images/ban_4.png') }}")
                        }
                        gravitySpeed += highGravity
                    }

                    vx -= gravitySpeed

                    myGameArea.clear()
                    myGameArea.frameNo += 1
                    if (innerHeight < 750) {
                        myGameArea.context.drawImage(pointer, vx, 505, 25, 25)
                    } else {
                        myGameArea.context.drawImage(pointer, vx, 605, 25, 25)
                    }
                }
            }

            function isFinish() {
                return vx > batasAtas
            }

            function isBottom() {
                return vx < batasBawah + 10
            }
        </script>
    @else
        <script>
            var myGamePiece = [];
            let score = []
            let totalScore = 0
            let pointer = new Image()
            pointer.src = "{{ asset('images/ban_pointer.png') }}"
            let lowGravity = 0.06
            let middleGravity = 0.0000000005 //0.018
            let highGravity = 0.000093 //0.002
            let vx = 2
            let gravitySpeed = 0
            let imgIndex = 1
            var life = [$(".love3"), $(".love2"), $(".love1")]
            let idxLife, banCounter = 0
            var ban = [$(".bh3"), $(".bh2"), $(".bh1")]
            var ms = 0
            var sc = 0
            var mn = 0
            var clickable = true;
            var step = 10
            var stopp = false

            var myGameArea = {
                canvas: $("#game_lay"),
                start: function() {
                    this.canvas[0].width = innerWidth;
                    this.canvas[0].height = innerHeight;
                    this.context = this.canvas[0].getContext("2d");
                    this.frameNo = 0;
                    // this.interval = setInterval(animate, 20)
                },
                clear: function() {
                    this.context.clearRect(0, 0, this.canvas[0].width, this.canvas[0].height)
                }
            };

            function initGame() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('joinTambalBan') }}",
                    type: "POST",
                    data: {},
                    beforeSend: function() {
                        $("#play__modal").modal('toggle')
                    },
                    success: function(res) {
                        if (res.Status == "Success") {
                            $("#warning__modal").modal('toggle')
                        } else {
                            $("#warning__modal").modal('toggle')
                            $("#gagal__modal").modal('toggle')
                        }
                    },
                    error: function(err) {
                        console.log(err)
                        // show modal
                        $("#warning__modal").modal('toggle')
                        $("#gagal__modal").modal('toggle')
                    }
                })
            }

            $(function() {
                if (innerHeight < 750) {
                    $('#game_lay').css("background", "url(../images/base_mobile_400.png)")
                    $('#game_lay').css("background-repeat", "no-repeat")
                    $('#game_lay').css("background-position", "center")
                    $('#game_lay').css("background-size", "cover")
                    $('#game_lay').css("object-fit", "cover")
                }
                myGameArea.start()
                $("#play__modal").modal('toggle')
            })
        </script>
    @endif
</body>

</html>
