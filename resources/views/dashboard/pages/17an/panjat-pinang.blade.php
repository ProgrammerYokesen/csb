@section('title')
    Lomba 17an
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom gutter-b teman_sobat_card">
                    <div class="card-body">

                        <div class="panjat_header">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <div>
                                <img src="{{ asset('images/lomba/help.png') }}" alt="" data-toggle="modal"
                                    data-target="#modal_bermain">
                            </div>
                            {{-- <button class="btn">Cara Bermain</button> --}}
                        </div>
                        <div class="panjat_content">

                            <img src="{{ asset('images/lomba/timer.png') }}" alt="" class="timer-img">

                            <div class="card">
                                <div class="card-body">
                                    <img src="{{ asset('images/lomba/panjat-pinang.png') }}" alt="" class="panjat_logo">
                                    <img src="{{ asset('images/lomba/panjat-bg.png') }}" alt="" class="panjat_bg">
                                    {{-- <canvas id="panjat_overlay_desk"></canvas> --}}
                                </div>
                            </div>

                            <div data-toggle="modal" data-target="#modal_play">
                                <img src="{{ asset('images/lomba/play-btn.png') }}" alt="" class="play-img">
                            </div>

                        </div>
                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->


    {{-- Modal Play --}}
    <div class="modal fade modal_panjat_play" id="modal_play" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <a href="{{ route('panjatPinangGame') }}">
                    <img src="{{ asset('images/lomba/main-btn.png') }}" alt="">
                </a>
                {{-- <img src="{{ asset('images/lomba/main-btn.png') }}" alt="" onclick="toPotong()"> --}}

            </div>
        </div>
    </div>

    <!-- Modal Potong Baper Poin-->
    <div class="modal fade modal_potong" id="modal_potong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal_ctn">
                    {{-- <h3>Kamu membutuhkan 1 Tiket 17an untuk bermain Panjat Pinang</h3>
                    <p>1 Tiket 17an = 1.000 Baper Poin. Baper Poin akan terpotong saat kamu klik tombol “MULAI”.</p> --}}
                    <h3>Hari ini Mimin kasih GRATIS untuk cobain Game Panjat Pinang</h3>
                    <p>Tapi mulai tanggal 17 nanti bayar pake baper poin yaa</p>
                </div>

                {{-- <a href="javascript:void(0)" class="mulai_btn" onclick="startGame('modal_potong')"> --}}
                <a href="{{ route('panjatPinangGame') }}" class="mulai_btn">
                    <img src="{{ asset('images/lomba/mulai-btn.png') }}" alt="">
                </a>

            </div>
        </div>
    </div>

    <!-- Modal Cara Bermain-->
    <div class="modal fade modal_potong" id="modal_bermain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">

                <img src="{{ asset('images/lomba/cara.png') }}" alt="" class="cara_bermain">

                <img src="{{ asset('images/lomba/close.png') }}" alt="" class="close__btn" data-dismiss="modal">

                <div class="modal_ctn">
                    <ol class="text-left">
                        <li>Tekan tombol hijau secara terus menerus</li>
                        <li>Apabila kamu berhenti menekan, karaktermu akan turun secara perlahan</li>
                        <li>Kamu diharuskan untuk terus menekan tombol hijau agar karaktermu bisa sampai ke puncak</li>
                        <li>Pastikan kamu mencapai puncak dengan waktu secepat mungkin</li>
                        <li>Pemenangnya ditentukan berdasarkan waktu tercepat mencapai puncak</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('pageJS')
    <script>
        function toPotong() {
            $('#modal_play').modal('toggle')
            $('#modal_potong').modal('toggle')
        }
    </script>

    <script>
        var screenHeight
        $(document).ready(function() {
            if (innerWidth <= 425 && innerWidth > 375) {
                screenHeight = 125
            } else if (innerWidth <= 375) {
                screenHeight = 110
            }
        });

        var myGamePiece;
        var myObstacles = [];
        var myScore;
        var myPrize;
        var score;
        var userScore;

        function startGame(nmModal) {
            $(`#${nmModal}`).modal('toggle')
            // $('#panjat_menu').addClass('d-none')
            // $('#panjat_game').removeClass('d-none')
            myGamePiece = new component(125, 125, "badak", screenHeight, 600,
                'img'); //SUBJECT --> ganti ini dengan badakbaper (w,h,color,start-w,start-h)
            myGamePiece.gravity = 0.03;
            myScore = new component("18px", "Consolas", "#A2600A", 100, 55, "text"); //KOMPONEN SKOR
            myPrize = new component(10, 10, "green", 180, 200);
            myGameArea.start();
        }

        var myGameArea = {
            canvas: $("#panjat_overlay_desk"),
            start: function() {
                this.canvas[0].width = 300;
                this.canvas[0].height = 700;
                this.context = this.canvas[0].getContext("2d");
                document.body.insertBefore(this.canvas[0], document.body.childNodes[0]);
                this.frameNo = 0;
                this.interval = setInterval(updateGameArea, 20);
            },
            clear: function() {
                this.context.clearRect(0, 0, this.canvas[0].width, this.canvas[0].height);
            },
            stop: function() {
                clearInterval(this.interval)
            }
        };

        function component(width, height, color, x, y, type) {
            this.type = type;
            if (type == "img") {
                this.image = new Image();
                this.image.src = "{{ asset('images/lomba/mobile/badak.gif') }}";
            }
            this.score = 0;
            this.width = width;
            this.height = height;
            this.speedX = 0;
            this.speedY = 0;
            this.x = x;
            this.y = y;
            this.gravity = 0;
            this.gravitySpeed = 0;
            this.update = function() {
                ctx = myGameArea.context;
                if (type == "img") {
                    ctx.drawImage(this.image,
                        this.x,
                        this.y,
                        this.width, this.height);
                } else if (this.type == "text") {
                    ctx.font = this.width + " " + this.height;
                    ctx.fillStyle = color;
                    ctx.fillText(this.text, this.x, this.y);
                } else {
                    ctx.fillStyle = color;
                    ctx.fillRect(this.x, this.y, this.width, this.height);
                }
            };
            this.newPos = function() {
                this.gravitySpeed += this.gravity;
                this.x += this.speedX;
                this.y += this.speedY + this.gravitySpeed;
                this.hitBottom();
                console.log('newPos')
            };
            this.hitBottom = function() {
                // var rockbottom = myGameArea.canvas[0].height - this.height;
                var rockbottom = myGameArea.canvas[0].height - 200;
                if (this.y > rockbottom) {
                    this.y = rockbottom;
                    this.gravitySpeed = 0;
                }

                console.log('hitbottom')
            };
            this.crashWith = function(otherobj) {
                var mytop = this.y + 20;
                var otherbottom = otherobj.y + otherobj.height;
                var crash = true;
                if (mytop > otherbottom) {
                    crash = false;
                }
                console.log('crash')
                return crash;


            };
        }

        function updateGameArea() {
            var x, height, gap, minHeight, maxHeight, minGap, maxGap;
            if (myGamePiece.crashWith(myPrize) || score < 1) {
                // WIN OR LOSE
                myGameArea.stop()
                if (score < 1) {
                    $('#modal_lose').modal('toggle')
                } else if (myGamePiece.crashWith(myPrize)) {
                    $('#modal_win').modal('toggle')
                    console.log(score)
                }

                return;
            }
            myGameArea.clear();
            myGameArea.frameNo += 1;
            score = 1000 - myGameArea.frameNo;

            // userScore = (((score * 20) % 60000) / 1000).toFixed(0);

            // myScore.text = '00' + ':' +
            //     userScore + '.' + score;
            myScore.text = score;
            myScore.update();
            myPrize.update();
            myGamePiece.newPos();
            myGamePiece.update();
        }

        function everyinterval(n) {
            if ((myGameArea.frameNo / n) % 1 == 0) {
                return true;
            }
            return false;
        }

        function accelerate(n) {
            myGamePiece.gravity = n;
        }

        function accelerateDesktop(n) {
            myGamePiece.y += n * 28;
        }

        function accelerateMobile(n) {
            myGamePiece.y += n * 2;
        }
    </script>

@endsection
