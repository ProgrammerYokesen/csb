@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                      <div class="card-title">
                          <a href="{{ route('homeDash') }}" class="btn">
                              <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                          </a>
                          <div class="text-right">
                            <a href="{{route('riwayatQuiz')}}" class="mb-2">
                              <button class="btn">
                                Riwayat Partisipasi
                              </button>
                            </a>
                            <button class="btn" data-toggle="modal" data-target="#rule-1212">
                              Cara Bermain
                            </button>
                          </div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="container text-center">
                        <h1 class="mb-5">Quiz Show Special Imlek</h1>
                        <img src="{{asset('images/event/imlek/games-imlek.png')}}" class="img-fluid mb-5">

                        <div class="text-center quiz__show_game">
                          <a href="{{route('registerQuizShow')}}">
                            <button class="btn prm">Daftar</button>
                          </a>
                          <a href="{{route('quizShowImlekDetail')}}">
                            <button class="btn sec">Peraturan & List Peserta</button>
                          </a>
                        </div>

                      </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal SOP -->
    <div class="modal fade" id="modal-sop" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>SOP/Peraturan Quiz Show Special Imlek</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <div data-scroll="true" data-height="500">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                              Yang dapat mengikuti games ini adalah Sobat Badak yang telah menyelesaikan transaksi sebesar Rp 61.000
                            </li>
                            <li>
                                Sobat Badak yang ingin mengikuti games ini juga WAJIB mengikuti WORKSHOP RESELLER pada 22 Januari 2022, pukul 16.00 WIB
                            </li>
                            <li>
                              Quiz Show Special Imlek 2022 akan berlangsung di ZOOM Club Sobat Badak dari tanggal 23 Januari - 31 Januari 2022
                            </li>
                            <li>
                              Peserta wajib menggunakan nama yang didaftarkan untuk Quiz Show Special Imlek 2022 saat berada di ZOOM
                            </li>
                            <li>
                              Sobat Badak wajib mendaftar setiap kali ingin mengikuti Quiz Show Imlek melalui website sobatbadak.club di menu "Quiz Show Imlek". Pendaftaran akan dibuka pada pukul 14.30 WIB
                            </li>
                            <li>
                              Peserta yang bermain adalah 60 orang pertama yang menekan tombol Join PADA HARI ITU
                            </li>
                            <li>
                              Nama-nama Sobat Badak yang telah menekan tombol join akan muncul di Leaderboard pada website Club Sobat Badak
                            </li>
                            <li>
                              Jika Sobat Badak tidak berada di Zoom saat dipanggil, maka Tim CSB akan memanggil nama di urutan selanjutnya
                            </li>
                            <li>
                              Jika Sobat Badak yang terpilih berada di Zoom, tetapi belum menghidupkan audio ataupun camera, Tim CSB akan memberikan kesempatan sebanyak 3 kali panggilan. Lewat dari itu, Tim CSB akan memanggil nama yang muncul selanjutnya
                            </li>
                            <li>
                              Jika Sobat Badak tidak terpanggil di Quiz Show hari pertama, Sobat Badak mendaftar lagi di hari selanjutnya
                            </li>
                            <li>
                              Sobat Badak hanya mendapatkan satu kali kesempatan untuk menang di Quiz Show Imlek
                            </li>
                        </ol>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="regis_success" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Selamat! kamu sudah terdaftar di Quiz Show ini.</h5>
                  <p>Silahkan langsung masuk zoom Club Sobat Badak ya! Semoga Beruntung</p>

                  <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                    <button class="btn">OK</button>
                  </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')
<script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>

@if (Session::get('response_code') == "001")
  <script>
    Swal.fire("Berhasil Daftar!", "Kamu berhasil Daftar", "success").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "002")
  <script>
    Swal.fire("Maaf", "Kamu belum register event imlek", "warning").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "003")
  <script>
    Swal.fire("Maaf", "Silahkan lakukan transaksi di event imlek sebelum daftar Quiz Show Special Imlek", "warning").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "004")
  <script>
    Swal.fire("Maaf", "Belum ada Quiz Show Aktif", "warning").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "005")
  <script>
    Swal.fire("", "Kamu sudah daftar di quiz show ini", "success").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "006")
  <script>
    Swal.fire("Maaf", "Kuota Quiz Show Special Imlek sudah penuh", "error").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "007")
  <script>
    Swal.fire("Maaf", "Kamu sudah pernah menang Quiz Show Imlek", "error").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif

<script>
  document.addEventListener('readystatechange', event => {
    if (event.target.readyState === "complete") {
        var clockdiv = document.getElementsByClassName("clockdiv");
        var countDownDate = new Array();
        for (var i = 0; i < clockdiv.length; i++) {
            countDownDate[i] = new Array();
            countDownDate[i]['el'] = clockdiv[i];
            countDownDate[i]['time'] = new Date(clockdiv[i].getAttribute('data-date')).getTime();
            // countDownDate[i]['days'] = 0;
            countDownDate[i]['hours'] = 0;
            countDownDate[i]['seconds'] = 0;
            countDownDate[i]['minutes'] = 0;
        }

        var countdownfunction = setInterval(function() {
          for (var i = 0; i < countDownDate.length; i++) {
                var now = new Date().getTime();
                var distance = countDownDate[i]['time'] - now;
                 // countDownDate[i]['days'] = Math.floor(distance / (1000 * 60 * 60 * 24));
                 countDownDate[i]['hours'] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                 countDownDate[i]['minutes'] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                 countDownDate[i]['seconds'] = Math.floor((distance % (1000 * 60)) / 1000);

                 if (distance < 0) {
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = 0;
                }else{
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = countDownDate[i]['days'];
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = (countDownDate[i]['hours'] < 10 ? '0' + countDownDate[i]['hours'] : countDownDate[i]['hours'] ) ;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = (countDownDate[i]['minutes'] < 10 ? '0' + countDownDate[i]['minutes'] : countDownDate[i]['minutes'] );
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = (countDownDate[i]['seconds'] < 10 ? '0' + countDownDate[i]['seconds'] : countDownDate[i]['seconds'] );
                }
           }
        }, 1000);
    }
  });
</script>
@endsection
