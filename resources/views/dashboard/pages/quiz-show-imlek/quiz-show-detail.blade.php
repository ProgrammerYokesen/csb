@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <h1>{{Session::get('response_code')}}</h1>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body quiz__show_rule">
                      <div class="container">

                        @if ($winner == 1)
                          <div class="text-center">
                            <h1 style="margin-bottom: 25px">Quiz Show Special Imlek</h1>
                            <img style="margin: 0 auto" src="{{asset('images/badak-baper.png')}}" alt="">
                            <h2 style="margin-top: 25px; font-weight: 700">Terima kasih sudah berpartisipasi dan menang di Quiz Show Special Imlek</h2>

                            <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                              <button class="btn" style="background: #009ef7; color: #fff">Nonton Quiz Show</button>
                            </a>
                          </div>
                        @else
                          <div class="row" style="margin-bottom: 25px">
                            <div class="col-lg-6">
                              <img src="{{asset('images/event/imlek/games-imlek.png')}}">
                            </div>
                            <div class="col-lg-6">
                              <div class="d-flex">
                                <div class="quiz__show_rule_countdown">

                                  @php
                                    $noww = date("Y-m-d H:i:s");
                                  @endphp

                                  @if (strtotime($noww) <  strtotime($startDate))
                                    <div class="clockdiv d-flex" data-date="{{ $startDate }}">
                                      <div class="d-flex">
                                        <span class="hours">00</span>
                                        <div class="smalltext">:</div>
                                      </div>
                                      <div class="d-flex">
                                        <span class="minutes">00</span>
                                        <div class="smalltext">:</div>
                                      </div>
                                      <div class="d-flex">
                                        <span class="seconds">00</span>
                                        <div class="smalltext"></div>
                                      </div>
                                    </div>
                                  @elseif(strtotime($noww) > strtotime($startDate) && strtotime($noww) < strtotime($endDate))
                                    Quiz Sedang Berlangsung
                                  @elseif (strtotime($noww) > strtotime($endDate))
                                    Quiz Sudah Berakhir
                                  @endif



                                </div>
                              </div>

                              <h1>Quiz Show Spesial Imlek</h1>
                              <!-- {{$noww}} -->
                              <!-- {{$startDate}} -->
                              @if($totalUser >= 25)
                              <h3>Kuota untuk hari ini sudah terpenuhi.</h3>
                              <h3>Silahkan mendaftar Quiz Show Imlek lagi besok ya!</h3>
                              @endif

                              <div class="quiz__show_game" style="text-align: left; margin-bottom: 0; margin-top: 15px">
                                <!-- <a href="#">
                                  <button class="btn prm">Riwayat Partisipasi</button>
                                </a> -->
                                <button class="btn prm" data-toggle="modal" data-target="#modal-sop">Peraturan</button>
                                <a href="{{route('listPesertaImlek', $quiz->id)}}">
                                  <button class="btn prm">Riwayat Peserta</button>
                                </a>
                                <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                                  <button class="btn" style="background: #009ef7; color: #fff">Nonton Quiz Show</button>
                                </a>
                              </div>

                            </div>
                          </div>

                          <h5>Cara Bermain:</h5>
                          <ol>
                              <li>Sobat Badak wajib hadir saat nama dipanggil dan tidak boleh diwakilkan</li>
                              <li>Quiz Show Special Imlek 2022 akan diadakan di Zoom Club Sobat Badak</li>
                              <li>Nama yang Sobat Badak gunakan saat di Zoom harus sesuai dengan nama saat mendaftar Quiz Show Spesial Imlek 2022 di website sobatbadak.club</li>
                              <li>Setiap Sobat Badak mendapatkan waktu 5 menit untuk bermain</li>
                              <li>Sobat Badak akan diberikan waktu 10 detik untuk memilih SATU ANGPAO dan mendapatkan hadiah yang berada di dalamnya</li>
                              <li>Hadiah maksimal akan dikirimkan H+20 (hari kerja) dari tanggal Sobat Badak main</li>
                              <li>Tidak ada penggantian kuota/pulsa dan kompensasi dalam bentuk apapun</li>
                          </ol>

                          @php
                            $now = date("Y-m-d H:i:s");
                          @endphp

                          <div class="text-center quiz__show_game">
                            <a href="{{route('registerQuizShow')}}">
                              <button class="btn prm">Daftar</button>
                            </a>
                          </div>
                        @endif



                      </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal SOP -->
    <div class="modal fade" id="modal-sop" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>SOP/Peraturan Quiz Show Special Imlek</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <div data-scroll="true" data-height="500">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                          <li>
                            Yang dapat mengikuti games ini adalah Sobat Badak yang telah menyelesaikan transaksi sebesar Rp 61.000
                          </li>
                          <li>
                              Sobat Badak yang ingin mengikuti games ini juga WAJIB mengikuti WORKSHOP RESELLER pada 22 Januari 2022, pukul 16.00 WIB
                          </li>
                          <li>
                            Quiz Show Special Imlek 2022 akan berlangsung di ZOOM Club Sobat Badak dari tanggal 23 Januari - 31 Januari 2022
                          </li>
                          <li>
                            Peserta wajib menggunakan nama yang didaftarkan untuk Quiz Show Special Imlek 2022 saat berada di ZOOM
                          </li>
                          <li>
                            Sobat Badak wajib mendaftar setiap kali ingin mengikuti Quiz Show Imlek melalui website sobatbadak.club di menu "Quiz Show Imlek". Pendaftaran akan dibuka pada pukul 14.30 WIB
                          </li>
                          <li>
                            Peserta yang bermain adalah 60 orang pertama yang menekan tombol Join PADA HARI ITU
                          </li>
                          <li>
                            Nama-nama Sobat Badak yang telah menekan tombol join akan muncul di Leaderboard pada website Club Sobat Badak
                          </li>
                          <li>
                            Jika Sobat Badak sudah terpilih namun tidak berada di zoom, Host akan memanggil sebanyak 3x. Setelah dipanggil masih tidak muncul, maka tim CSB akan memanggil nama di urutan selanjutnya
                          </li>
                          <li>
                            Jika Sobat Badak yang terpilih berada di Zoom, tetapi belum menghidupkan audio ataupun camera, Tim CSB akan memberikan kesempatan sebanyak 3 kali panggilan. Lewat dari itu, Tim CSB akan memanggil nama yang muncul selanjutnya
                          </li>
                          <li>
                            Jika Sobat Badak tidak terpanggil di Quiz Show hari pertama, Sobat Badak mendaftar lagi di hari selanjutnya
                          </li>
                          <li>
                            Sobat Badak hanya mendapatkan satu kali kesempatan untuk menang di Quiz Show Imlek
                          </li>
                        </ol>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <button data-toggle="modal" data-target="#modal_loading">asd</button> -->

    <div class="modal fade" id="regis_success" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Selamat! kamu sudah terdaftar di Quiz Show ini.</h5>
                  <p>Silahkan langsung masuk zoom Club Sobat Badak ya! Semoga Beruntung</p>

                  <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                    <button class="btn">OK</button>
                  </a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_loading" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <div class="spinner-border" role="status" style="margin-bottom: 25px; width: 50px; height: 50px">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <h5>Pendaftaran kamu sedang diproses</h5>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')

<script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>

@if (Session::get('response_code') == "001")
  <script>
    // Swal.fire("Selamat!", "Kamu berhasil Daftar", "success")
    $('#modal_loading').modal('show')


    $.ajax({
        type: 'GET',
        url: `/user-imlek-nomor-urut`,

        success: function(res) {
          // console.log(res)
          if (res.noUrut <= 25) {
            $('#modal_loading').modal('hide')
            Swal.fire("Selamat!", "Kamu berhasil Daftar", "success").then((result) => {
              window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
            })
          }else{
            Swal.fire("Maaf", "Kuota Quiz Show Special Imlek sudah penuh", "error").then((result) => {
              window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
            })
          }
        }
    });
  </script>
@endif
@if (Session::get('response_code') == "002")
  <script>
    Swal.fire("Maaf", "Kamu belum register event imlek", "warning")
  </script>
@endif
@if (Session::get('response_code') == "003")
  <script>
    Swal.fire("Maaf", "Silahkan lakukan transaksi di event imlek sebelum daftar Quiz Show Special Imlek", "warning")
  </script>
@endif
@if (Session::get('response_code') == "004")
  <script>
    Swal.fire("Maaf", "Belum ada Quiz Show Aktif", "warning").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "005")
  <script>
    Swal.fire("", "Kamu sudah daftar di quiz show ini", "success").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "006")
  <script>
    Swal.fire("Maaf", "Kuota Quiz Show Special Imlek sudah penuh", "error")
  </script>
@endif
@if (Session::get('response_code') == "007")
  <script>
    Swal.fire("Maaf", "Kamu sudah pernah menang Quiz Show Imlek", "error").then((result) => {
      window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
    })
  </script>
@endif
@if (Session::get('response_code') == "008")
  <script>
    Swal.fire("Maaf", "Kamu tidak memenuhi syarat untuk mengikuti event Quiz Show Special Imlek", "error")
  </script>
@endif

@if (Session::has('success_regist'))
    <script>
        $(`#regis_success`).modal('toggle')
    </script>
@endif

<script>
const getUserUrut = () => {
  // console.log('bzz')
  $.ajax({
      type: 'GET',
      url: `/user-imlek-nomor-urut`,

      success: function(res) {
        console.log(res)
        if (res.noUrut <= 25) {
          $('#modal_loading').modal('hide')
          Swal.fire("Selamat!", "Kamu berhasil Daftar", "success").then((result) => {
            window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
          })
        }else{
          Swal.fire("Maaf", "Kuota Quiz Show Special Imlek sudah penuh", "error").then((result) => {
            window.open('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09', '_blank');
          })
        }
      }
  });
}
</script>

<script>
  document.addEventListener('readystatechange', event => {
    if (event.target.readyState === "complete") {
        var clockdiv = document.getElementsByClassName("clockdiv");
        var countDownDate = new Array();
        for (var i = 0; i < clockdiv.length; i++) {
            countDownDate[i] = new Array();
            countDownDate[i]['el'] = clockdiv[i];
            countDownDate[i]['time'] = new Date(clockdiv[i].getAttribute('data-date')).getTime();
            // countDownDate[i]['days'] = 0;
            countDownDate[i]['hours'] = 0;
            countDownDate[i]['seconds'] = 0;
            countDownDate[i]['minutes'] = 0;
        }

        var countdownfunction = setInterval(function() {
          for (var i = 0; i < countDownDate.length; i++) {
                var now = new Date().getTime();
                var distance = countDownDate[i]['time'] - now;
                 // countDownDate[i]['days'] = Math.floor(distance / (1000 * 60 * 60 * 24));
                 countDownDate[i]['hours'] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                 countDownDate[i]['minutes'] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                 countDownDate[i]['seconds'] = Math.floor((distance % (1000 * 60)) / 1000);

                 if (distance < 0) {
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = 0;
                }else{
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = countDownDate[i]['days'];
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = (countDownDate[i]['hours'] < 10 ? '0' + countDownDate[i]['hours'] : countDownDate[i]['hours'] ) ;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = (countDownDate[i]['minutes'] < 10 ? '0' + countDownDate[i]['minutes'] : countDownDate[i]['minutes'] );
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = (countDownDate[i]['seconds'] < 10 ? '0' + countDownDate[i]['seconds'] : countDownDate[i]['seconds'] );
                }
           }
        }, 1000);
    }
  });
</script>
@endsection
