@section('title')
    Quiz Show
@endsection

@section('page_assets')
  <link rel="stylesheet" href="{{asset('user-dashboard/quiz-show-imlek.css')}}">
  <style>
    .quiz__show_imlek .card {
      width: 450px;
      margin: 15px auto;
      padding: 1rem;
      background: rgba(255, 133, 0, 0.1);
      border-radius: 6px;
      border: transparent;
    }

    .quiz__show_imlek .card button{
      background: #ffaa3a;
      color: #ffffff;
    }

    .quiz__show_imlek .card button.zoom{
      background: #539FFF;
    }

    .imlek__sop {
      margin: 25px auto;
    }

    .imlek__sop button {
      background: #ffaa3a;
      color: #ffffff;
      margin: 5px;
    }

    .imlek__sop button:hover {
      color: #fff;
    }

    .quiz__show_imlek .time__text h1{
      color: #03AB00;
    }

    @media screen and (max-width: 425px){
      .quiz__show_imlek .card {
        width: 100%;
      }
    }
  </style>
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <!-- <div class="text-right">
                              <a href="{{route('riwayatQuiz')}}" class="mb-2">
                                <button class="btn">
                                  Riwayat Partisipasi
                                </button>
                              </a>
                              <button class="btn" data-toggle="modal" data-target="#rule-1212">
                                Cara Bermain
                              </button>
                            </div> -->
                        </div>
                    </div>
                    <div class="card-body quiz__show_imlek">

                        <div class="text-center">
                          <h1 style="margin-bottom: 15px">Countdown menuju Quiz Show Special Imlek</h1>

                          <h3 style="margin-bottom: 25px">Quiz Show Special Imlek</h3>

                          <div class="time__text d-flex justify-content-center" style="margin-top: 1rem; margin-bottom: 2rem">
                              <div style="margin: 0 15px">
                                  <h1 id="time__day" class="fontw-9 fonts-64">26</h1>
                                  <h4>Hari</h4>
                              </div>
                              <div style="margin: 0 15px">
                                  <h1 id="time__hour" class="fontw-9 fonts-64">26</h1>
                                  <h4>Jam</h4>
                              </div>
                              <div style="margin: 0 15px">
                                  <h1 id="time__minute" class="fontw-9 fonts-64">26</h1>
                                  <h4>Menit</h4>
                              </div>
                              <div style="margin: 0 15px">
                                  <h1 id="time__second" class="fontw-9 fonts-64">26</h1>
                                  <h4>Detik</h4>
                              </div>
                          </div>

                          <p>Quiz Show Spesial Imlek 2022 merupakan acara kuis yang disponsori Warisan dan Warisan Gajahmada dalam rangka menyambut Hari Raya Imlek 2022. Dengan menyelesaikan transaksi Rp 61.000 maka Sobat Badak akan mendapatkan 1 Bundle Larutan Penyegar Cap Badak Rasa Jeruk dan Pilih Hadiah yang kamu inginkan!</p>

                          <p>Selain itu, Sobat Badak juga bisa mengikuti Workshop Reseller secara gratis dan juga kesempatan untuk mengikuti Quiz Show Spesial Imlek 2022! Sobat Badak bisa memilih angpao dengan total hadiah angpao senilai 50 juta rupiah!</p>


                          <h6 style="margin-top: 50px; color: #FF8500">Syarat mengikuti Quiz Show Special Imlek:</h6>

                          <div class="card">

                            @if($paidStatus == 1)
                              <h6 style="color: #00CC5D;">Beli Larutan Penyegar Cap Badak Rasa Jeruk <i class="fas fa-check-circle" style="color: #00CC5D;"></i></h6>
                            @else
                              <h6>Beli Larutan Penyegar Cap Badak Rasa Jeruk</h6>
                              <div class="d-flex justify-content-center" style="margin-top: 15px">
                                <a href="{{route('imlekLandingPage')}}">
                                  <button class="btn">Menuju Landing Page</button>
                                </a>
                              </div>
                            @endif


                          </div>

                          <div class="card">

                            @if($zoomStatus == 1)
                              <h6 style="color: #00CC5D;">Ikut Workshop Zoom <i class="fas fa-check-circle" style="color: #00CC5D;"></i> </h6>
                            @else
                              <h6>Ikut Workshop Zoom</h6>
                              <p style="margin: 0">Sabtu, 22 Januari 2022</p>
                              <p>16:00 WIB</p>

                            @php
                              $noww = date("Y-m-d H:i:s");
                            @endphp

                            @if (strtotime($noww) >=  strtotime("2022-01-22T15:45:00") && strtotime($noww) <= strtotime("2022-01-22T17:30:00"))
                              <div class="d-flex justify-content-center" style="margin-top: 15px">
                                <a href="{{route('joinZoomImlek')}}">
                                  <button class="btn zoom">Join Zoom</button>
                                </a>
                              </div>
                            @endif

                            @endif

                          </div>


                          <div class="imlek__sop">
                            <button class="btn" data-toggle="modal" data-target="#modal-sop">SOP/Peraturan</button>
                            <button class="btn" data-toggle="modal" data-target="#modal-cara-bermain">Cara Bermain</button>
                            <a href="{{route('imlekLandingPage')}}">
                              <button class="btn">Menuju Landing Page</button>
                            </a>
                          </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal SOP -->
    <div class="modal fade" id="modal-sop" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>SOP/Peraturan Quiz Show Special Imlek</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <div data-scroll="true" data-height="500">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                              Yang dapat mengikuti games ini adalah Sobat Badak yang telah menyelesaikan transaksi sebesar Rp 61.000
                            </li>
                            <li>
                                Sobat Badak yang ingin mengikuti games ini juga WAJIB mengikuti WORKSHOP RESELLER pada 22 Januari 2022, pukul 16.00 WIB
                            </li>
                            <li>
                              Quiz Show Special Imlek 2022 akan berlangsung di ZOOM Club Sobat Badak dari tanggal 23 Januari - 31 Januari 2022
                            </li>
                            <li>
                              Peserta wajib menggunakan nama yang didaftarkan untuk Quiz Show Special Imlek 2022 saat berada di ZOOM
                            </li>
                            <li>
                              Sobat Badak wajib mendaftar setiap kali ingin mengikuti Quiz Show Imlek melalui website sobatbadak.club di menu "Quiz Show Imlek". Pendaftaran akan dibuka pada pukul 14.30 WIB
                            </li>
                            <li>
                              Peserta yang bermain adalah 60 orang pertama yang menekan tombol Join PADA HARI ITU
                            </li>
                            <li>
                              Nama-nama Sobat Badak yang telah menekan tombol join akan muncul di Leaderboard pada website Club Sobat Badak
                            </li>
                            <li>
                              Jika Sobat Badak sudah terpilih namun tidak berada di zoom, Host akan memanggil sebanyak 3x. Setelah dipanggil masih tidak muncul, maka tim CSB akan memanggil nama di urutan selanjutnya
                            </li>
                            <li>
                              Jika Sobat Badak yang terpilih berada di Zoom, tetapi belum menghidupkan audio ataupun camera, Tim CSB akan memberikan kesempatan sebanyak 3 kali panggilan. Lewat dari itu, Tim CSB akan memanggil nama yang muncul selanjutnya
                            </li>
                            <li>
                              Jika Sobat Badak tidak terpanggil di Quiz Show hari pertama, Sobat Badak mendaftar lagi di hari selanjutnya
                            </li>
                            <li>
                              Sobat Badak hanya mendapatkan satu kali kesempatan untuk menang di Quiz Show Imlek
                            </li>
                        </ol>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Cara Bermain -->
    <div class="modal fade" id="modal-cara-bermain" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Cara Bermain Quiz Show Special Imlek</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <div data-scroll="true" data-height="500">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                              Sobat Badak wajib hadir saat nama dipanggil dan tidak boleh diwakilkan
                            </li>
                            <li>
                              Quiz Show Special Imlek 2022 akan diadakan di Zoom Club Sobat Badak
                            </li>
                            <li>
                              Nama yang Sobat Badak gunakan saat di Zoom harus sesuai dengan nama saat mendaftar Quiz Show Spesial Imlek 2022 di website sobatbadak.club
                            </li>
                            <li>
                              Setiap Sobat Badak mendapatkan waktu 5 menit untuk bermain
                            </li>
                            <li>
                              Sobat Badak akan diberikan waktu 10 detik untuk memilih SATU ANGPAO dan mendapatkan hadiah yang berada di dalamnya
                            </li>
                            <li>
                              Angpao yang didapatkan adalah angpao PILIHAN PERTAMA dari Sobat Badak
                            </li>
                            <li>
                              Hadiah maksimal akan dikirimkan H+20 (hari kerja) dari tanggal Sobat Badak main
                            </li>
                            <li>
                              Tidak ada penggantian kuota/pulsa dan kompensasi dalam bentuk apapun
                            </li>
                        </ol>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pageJS')
  <script>
  $(function() {

      var timer = setInterval(() => {
          var date1 = new Date('2022-01-23T14:15');
          var date2 = new Date();

          let distance = date1.getTime() - date2.getTime()

          if (distance < 0) {
              clearInterval(timer)
              window.location.href = "{{ route('quizShowImlekDetail') }}"
          }

          var diffInSeconds = Math.abs(date1 - date2) / 1000;
          var days = Math.floor(diffInSeconds / 60 / 60 / 24);
          var hours = Math.floor(diffInSeconds / 60 / 60 % 24);
          var minutes = Math.floor(diffInSeconds / 60 % 60);
          var seconds = Math.floor(diffInSeconds % 60);
          var milliseconds = Math.round((diffInSeconds - Math.floor(diffInSeconds)) * 1000);

          $('#time__day').text(days)
          $('#time__hour').text(('0' + hours).slice(-2))
          $('#time__minute').text(('0' + minutes).slice(-2))
          $('#time__second').text(('0' + seconds).slice(-2))


      }, 1000);
  })
  </script>
@endsection
