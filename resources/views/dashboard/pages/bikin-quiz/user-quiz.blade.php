@section('title')
    Bikin Quiz
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>

                            <div class="quiz_btn_group">
                                <button class="btn rule_btn" data-toggle="modal" data-target="#cara-tebak-kata">
                                    Peraturan
                                </button>
                                <button class="btn" data-toggle="modal" data-target="#modal_pertanyaan">
                                    Lihat <br> Pertanyaanmu
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">


                        @if ($qty > 0)
                            <h1>BIKIN QUIZ TEBAK KATA</h1>
                            <h5>Kuota pertanyaan yang bisa di submit: {{ $qty }}</h5>

                            <form class="input_quiz_form" action="{{ route('postTebakKata') }}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pertanyaan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="pertanyaan" id="input_soal"
                                            placeholder="Ketik disini" autocomplete="off" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jawaban</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="kata" id="input_jawaban"
                                            placeholder="Maksimal 10 Karakter" maxlength="10" autocomplete="off" required />
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn">SUBMIT</button>
                                </div>
                            </form>
                        @else

                            <div class="text-center mb-5">
                                <img src="{{ asset('images/boleng-getol.png') }}" alt="" class="img-fluid">
                            </div>
                            <h1>Maaf, Quota Pertanyaan kamu sudah habis</h1>
                            <h5>Silahkan input lagi besok ya!</h5>

                            <button class="btn" data-toggle="modal" data-target="#modal_pertanyaan">
                                Lihat <br> Pertanyaanmu
                            </button>
                        @endif




                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="cara-tebak-kata" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Bikin Quiz Tebak Kata</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Setiap peserta hanya bisa mengajukan <strong>maksimal 5 pertanyaan per hari</strong>
                            </li>
                            <li>
                                Jumlah Baper Poin yang akan didapatkan berdasarkan jumlah pertanyaan yang diterima
                            </li>
                            <li>
                                Satu pertanyaan yang diterima akan mendapatkan 1.000 Baper Poin
                            </li>
                            <li>
                                Apabila pertanyaan ditolak, maka tidak akan mendapatkan 1.000 Baper Poin
                            </li>
                            <li>
                                Pertanyaan yang diajukan tidak boleh mengandung SARA ataupun unsur pornografi
                            </li>
                            <li>
                                Keputusan panitia Club Sobat Badak tidak dapat diganggu gugat
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Riwayat Pertanyaan --}}
    <div class="modal fade" id="modal_pertanyaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_baper_poin">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_pertanyaan_header">
                        <h5>
                            List Pertanyaanmu
                        </h5>
                        <input type="text" class="form-control" id="kt_datepicker_1" readonly="readonly"
                            placeholder="Pilih Hari" />
                        {{-- <select name="month" id="selectMonth">

                        </select> --}}
                    </div>
                    <table class="table table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <td style="width:33%">Pertanyaan</td>
                                <td style="width:33%" class="text-center">Jawaban</td>
                                <td style="width:33%">Status</td>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll scroll-pull" data-scroll="true" data-height="300">
                        <table class="table table-borderless table-striped list_pertanyaan">
                            <tbody id="fullHistory">
                                @foreach ($lists as $list)
                                    <tr>
                                        <td style="width:33%">{{ $list->pertanyaan }}</td>
                                        <td style="width:33%">{{ $list->kata }}</td>
                                        @if ($list->acceptance === null)
                                            <td style="width:33%">
                                                <div class="waiting">Waiting</div>
                                            </td>
                                        @elseif ($list->acceptance === 0)
                                            <td style="width:33%">
                                                <div class="reject">Rejected</div>
                                            </td>
                                        @elseif($list->acceptance == 1)
                                            <td style="width:33%">
                                                <div class="approve">Approved</div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="border: transparent">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>

    <script>
        $('#kt_datepicker_1').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: "yyyy-mm-dd"
        });
    </script>
    <script>
        $('#kt_datepicker_1').change(function() {
            // You can access the value of your select field using the .val() method
            // alert('Select field value has changed to' + $('#selectMonth').val());

            // You can perform an ajax request using the .ajax() method
            $.ajax({
                type: 'GET',
                url: '{{ route('getRiwayatSoal') }}', // This is the url that will be requested
                data: {
                    date: $('#kt_datepicker_1').val()
                },

                // This is an object of values that will be passed as GET variables and 
                // available inside changeStatus.php as $_GET['selectFieldValue'] etc...

                // This is what to do once a successful request has been completed - if 
                // you want to do nothing then simply don't include it. But I suggest you 
                // add something so that your use knows the db has been updated{}
                success: function(data) {
                    var datas = data.lists;
                    console.log(datas);
                    if (datas.length == 0) {
                        deleteData();
                    } else {
                        $("#fullHistory").empty();
                        datas.forEach(element => {
                            var table = document.getElementById("fullHistory");
                            var row = table.insertRow(0);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);
                            cell1.innerHTML = `<td>${element.pertanyaan}</td>`;
                            cell2.innerHTML = `<td>${element.kata}</td>`;
                            if (element.acceptance === null) {
                                cell3.innerHTML = `<td><div class="waiting">Waiting</div></td>`;
                            } else if (element.acceptance === 0) {
                                cell3.innerHTML = `<td><div class="reject">Rejected</div></td>`;
                            } else if (element.acceptance === 1) {
                                cell3.innerHTML =
                                    `<td><div class="approve">Approved</div></td>`;
                            }
                        });
                    }
                    // alert('Select field value has changed to' + $('#selectMonth').val());
                },
            });

        });

        function deleteData() {
            $("#fullHistory").empty();
        }
    </script>

    <script>
        $('#input_jawaban').on('input', function() {
            $(this).val($(this).val().replace(/\s+/g, ''));
        });
    </script>
@endsection
