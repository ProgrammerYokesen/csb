@section('title')
    Bikin Quiz - Success
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('bikinQuiz') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="input_quiz_success">
                            <img src="{{ asset('images/boleng-getol.png') }}" alt="" class="img-fluid">

                            <h1>Pertanyaan buatan kamu sudah di submit!</h1>
                            <h5>Admin akan mengecek pertanyaanmu dan jika diterima, kamu akan mendapat 1.000 Baper Poin per
                                soal
                                yang diterima</h5>

                            <button class="btn">SELESAI</button>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Tebak Kata --}}
    <div class="modal fade" id="cara-tebak-kata" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Bikin Quiz Tebak Kata</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Tebak jawaban dari pertanyaan yang akan muncul
                            </li>
                            <li>
                                Ketik huruf yang terkandung dari jawaban yang kamu tebak didalam box, lalu klik enter
                            </li>
                            <li>
                                Kamu memiliki 3 kali kesempatan salah, jika berhasil menjawab kamu akan memperoleh 100 poin,
                                jika gagal kamu tidak memperoleh poin
                            </li>
                            <li>
                                Kumpulkan baper poin dalam sesi ini, selamat bermain!
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')

@endsection
