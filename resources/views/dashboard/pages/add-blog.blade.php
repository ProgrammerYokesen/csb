@section('title')
    Tambah Blog
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <form id="form__blog" action="{{ route('postBlog') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card card-custom gutter-b" style="padding-bottom: 3rem">
                        <div class="tebak_kata_card_head head-edit-profile justify-content-between d-flex">
                            {{-- <div class="card-title"> --}}
                            <a href="{{ route('userBlog') }}">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <div class="d-flex" id="button-in-desktop">
                                <button type="submit" class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_homepage" style="font-size: 14px">
                                    Submit
                                </button>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center" style="margin-bottom: 2rem">
                            <h1 style="margin: 0" class="bold-36-black">Majalah Dinding Teman Sobat</h1>
                        </div>
                        <div class="card-body card-edit-profile">
                            <div class="form-group">
                                <div>
                                    <input class="form-control title-input" placeholder="Judul" type="text" name="title"
                                        required autocomplete="off" />
                                </div>
                            </div>

                            {{-- Post Upload Image --}}
                            <div class="form-group post_upload">
                                <span id="file_remove" class="file_remove" style="">X</span>
                                <label class="custom-file-upload {{ Session::has('errImage') ? 'border-error' : '' }}">
                                    <div class="error_msg"></div>
                                    <input type="file" class="form-control-file" id="postUpload" name="blog_img">
                                    <div class="post_upload_text">
                                        <img src="{{ asset('images/upload.svg') }}" alt="upload">
                                        <h6 style="color: #000000;opacity: 0.3;">Masukan gambar disini</h6>
                                    </div>
                                    <div class="post-process" style="position: absolute;width:80%;border-radius:1rem">
                                        <div class="processing_bar" style="height: 10px"></div>
                                    </div>
                                    <div class="post-uploaded">
                                        
                                    </div>
                                </label>
                                @if (Session::has('errImage'))
                                    @component('components.text-error')
                                        @slot('message') {{ Session::get('errImage') }} @endslot
                                    @endcomponent
                                @endif
                            </div>

                            <div class="form-group">
                                {{-- Post Text Style --}}
                                <div id="medium-text">

                                </div>
                                <div class="error_desc"></div>
                            </div>
                            <input type="hidden" class="description" name="description">


                            <div id="button-in-mobile" class="d-flex justify-content-center pt-6">
                                <button type="submit" class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_homepage" style="font-size: 14px">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('pageJS')
    {{-- HTML EDITOR --}}
    <script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>
    <script src="{{ asset('user-dashboard/upload-image.js') }}"></script>
    <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire("Terjadi Kesalahan!", '{{ Session::get('error') }}', "error");
            })
        </script>
    @elseif(Session::has("success"))
        <script>
            $(function() {
                Swal.fire("Berhasil!", '{{ Session::get('success') }}', "success");
            })
        </script>
    @elseif($errors->get('title'))
        <script>
            $(function() {
                Swal.fire("Terjadi Kesalahan!", "Data judul tidak boleh kosong", "error");
            })
        </script>
    @elseif($errors->get('description'))
        <script>
            $(function() {
                Swal.fire("Terjadi Kesalahan!", "Data konten tidak boleh kosong", "error");
            })
        </script>
    @endif

    <script>
        let editorData;
        ClassicEditor
            .create(document.querySelector('#medium-text'), {
                placeholder: "Tulis ceritamu disini...",
                toolbar: {
                    items: ['heading', '|', 'bold', 'italic', 'underline', '|', 'bulletedList', '|',
                        'alignment', 'link', '|'
                    ]
                },
                heading: {
                    options: [{
                            model: 'paragraph',
                            title: 'Paragraph',
                            class: 'ck-heading_paragraph'
                        },
                        {
                            model: 'heading1',
                            view: 'h1',
                            title: 'Heading 1',
                            class: 'ck-heading_heading1'
                        },
                        {
                            model: 'heading2',
                            view: 'h2',
                            title: 'Heading 2',
                            class: 'ck-heading_heading2'
                        }
                    ]
                }
            })
            .then(data => {
                editorData = data;
            })
            .catch(error => {
                console.log(error);
            });

        $(function() {
            $('.error_desc').hide()
        })
        $(".submit_blog").on("click", function(e) {
            $('.description').val(editorData.getData())
            if (editorData.getData().length < 1050) {
                e.preventDefault()
                $('#form__blog .ck-content').css('border', 'solid 1px red')
                $('#form__blog .ck-content').css('border-radius', '0.5rem')
                $('.error_desc').show()
                $('.error_desc').text("minimal 1000 karakter")
            } else {
                $('.error_desc').hide()
            }
        });
    </script>
@endsection
