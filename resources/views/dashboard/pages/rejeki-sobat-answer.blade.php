@section('title')
    Quiz Rejeki Sobat
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">

                <div class="card card-custom gutter-b quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>

                    <div class="card-body">

                        <form action="{{ route('processQuiz') }}" method="POST">
                            <input type="hidden" name="quizId" value="{{ $datas[0]->quizId }}">
                            @csrf
                            <div class="quiz_rejeki">
                                @php $i = 1; @endphp
                                @foreach ($datas as $data)
                                    {{-- Question 1 --}}
                                    <div class="quiz_rejeki_quest">
                                        <h6>Pertanyaan {{ $i }}</h6>
                                        @php $i++; @endphp
                                        <h2>{{ $data->pertanyaan }}</h2>

                                        <div class="form-group">
                                            <div class="radio-inline">
                                                <label
                                                    class="radio radio-lg {{ $data->jawaban_user == 'A' && $data->jawaban_user != $data->jawaban_benar ? 'salah' : '' }}">
                                                    <input type="radio" value="A" name="{{ $data->pertanyaan_id }}"
                                                        {{ $data->jawaban_benar == 'A' ? 'checked' : '' }} disabled />
                                                    <span></span>
                                                    {{ $data->jawaban_a }}
                                                </label>
                                                <label
                                                    class="radio radio-lg {{ $data->jawaban_user == 'B' && $data->jawaban_user != $data->jawaban_benar ? 'salah' : '' }}">
                                                    <input type="radio" value="B" name="{{ $data->pertanyaan_id }}"
                                                        {{ $data->jawaban_benar == 'B' ? 'checked' : '' }} disabled />
                                                    <span></span>
                                                    {{ $data->jawaban_b }}
                                                </label>
                                                <label
                                                    class="radio radio-lg {{ $data->jawaban_user == 'C' && $data->jawaban_user != $data->jawaban_benar ? 'salah' : '' }}">
                                                    <input type="radio" value="C" name="{{ $data->pertanyaan_id }}"
                                                        {{ $data->jawaban_benar == 'C' ? 'checked' : '' }} disabled />
                                                    <span></span>
                                                    {{ $data->jawaban_c }}
                                                </label>
                                                <label
                                                    class="radio radio-lg {{ $data->jawaban_user == 'D' && $data->jawaban_user != $data->jawaban_benar ? 'salah' : '' }}">
                                                    <input type="radio" value="D" name="{{ $data->pertanyaan_id }}"
                                                        {{ $data->jawaban_benar == 'D' ? 'checked' : '' }} disabled />
                                                    <span></span>
                                                    {{ $data->jawaban_d }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- End question --}}
                                @endforeach

                                <div class="quiz_rejeki_btn">
                                    <button type="submit" class="btn">SELESAI</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
