@extends('dashboard.pages.home-coin')

@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container">
            <div class="card card-custom gutter-b dash_comingsoon">
                <div class="card-body">
                    <a href="{{ route('homeDash') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>
                    <div class="text-center">
                        <img src="{{ asset('images/baper.png') }}" alt="" class="img-fluid badak_comingsoon">
                        <h1>Coming Soon!</h1>

                        <a href="{{ route('homeDash') }}">
                            <button class="btn">Kembali</button>
                        </a>
                    </div>
                </div>
            </div>
            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>


@endsection
