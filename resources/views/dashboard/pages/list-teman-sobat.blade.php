@section('title')
    Teman Sobat
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom gutter-b teman_sobat_card">
                    <div class="card-header d-flex justify-content-between align-items-center" style="width: 100%">
                        <div class="card-title ">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>

                        </div>
                        <!-- <div>
                            <button class="btn all_teman_btn" data-toggle="modal" data-target="#modal_teman_sobat">Teman
                                Sobat</button>
                            <button class="btn all_teman_btn ml-1" data-toggle="modal"
                                data-target="#peraturan-event">Peraturan</button>
                        </div> -->

                    </div>
                    <div class="card-body">

                        <h1>Ajak Teman Sobatmu Sebanyak-banyaknya!</h1>

                        <h4>Kamu dapat mengundang teman-temanmu untuk ikutan keseruan bergabung di Club Sobat Badak dan
                            setiap mengundang satu orang dan orang tersebut <strong style="font-weight: 900; color: #e52c2c">memverifikasi
                                email dan nomor Whatsapp</strong>, Kamu
                            akan mendapatkan 1.000 Baper Poin!
                        </h4>
                        <div class="teman_sobat_share">
                            <div class="d-flex justify-content-center align-items-center mb-10">
                                <a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=facebook&utm_campaign=Bawa%20obat&utm_medium=share%20button&utm_content=dashboard') }}"
                                    target="_blank"
                                    onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=facebook&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}, 'newwindow', 'width=600,height=500'); return false;">
                                    <div class="sobat_fb">
                                        <img src="{{ asset('images/icons/fb.svg') }}" alt="fb">
                                    </div>
                                </a>

                                <a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=telegram&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}"
                                    target="_blank"
                                    onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=telegram&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}, 'newwindow', 'width=600,height=500'); return false;">
                                    <div class="sobat_tele">
                                        <img src="{{ asset('images/icons/telegram.svg') }}" alt="tele">
                                    </div>
                                </a>

                                <a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=twitter&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}"
                                    target="_blank"
                                    onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=twitter&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}, 'newwindow', 'width=600,height=500'); return false;">
                                    <div class="sobat_tw">
                                        <img src="{{ asset('images/icons/twitter.svg') }}" alt="fb">
                                    </div>
                                </a>
                            </div>

                            <div class="d-flex justify-content-center align-items-center">
                                <a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=line&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}"
                                    target="_blank"
                                    onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=line&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}, 'newwindow', 'width=600,height=500'); return false;">
                                    <div class="sobat_line">
                                        <img src="{{ asset('images/icons/line.svg') }}" alt="line">
                                    </div>
                                </a>

                                <a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=whatsapp&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}"
                                    onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{ urlencode('https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=whatsapp&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=dashboard') }}, 'newwindow', 'width=600,height=500'); return false;"
                                    target="_blank">
                                    <div class="sobat_wa">
                                        <img src="{{ asset('images/icons/whatsapp.svg') }}" alt="fb">
                                    </div>
                                </a>

                                <div class="sobat_link" data-toggle="tooltip" data-trigger="click"
                                    title="Berhasil Copy Link"
                                    data-clipboard-text="{{ 'https://sobatbadak.club/register?ref=' . Auth::user()->referral_code . '&utm_source=link&utm_campaign=Bawa%20Sobat&utm_medium=share%20button&utm_content=homepage' }}">
                                    <img src="{{ asset('images/icons/link.svg') }}" alt="fb">
                                </div>
                            </div>
                        </div>


                        <h5>Notes: Mohon untuk tidak melakukan spam daftar menggunakan URL sendiri karena akan
                            terdeteksi
                            spam dan akunmu dapat di suspend.</h5>

                        <h1 class="mb-10">Teman Sobat yang sudah kamu ajak: {{ $temanSobat }}</h1>
                        @if ($temanSobat > 0)
                            <div class="teman_sobat_table">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Waktu</th>
                                            <!-- <th>Kupon Undian</th>
                                            <th>Status Kupon Undian</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1; @endphp
                                        @foreach ($temanSobats as $tb)
                                            <tr>
                                                <td>
                                                  @if (Request::input('page'))
                                                    @if ($i % 10 == 0)
                                                      {{ $i * Request::input('page') }}
                                                    @else
                                                      {{ Request::input('page') - 1 != 0 ? Request::input('page') - 1 : ''  }}{{ $i }}
                                                    @endif
                                                  @else
                                                    {{ $i }}
                                                  @endif
                                                </td>
                                                <td>{{ $tb->name }} {!! badgeUser($tb->id) !!}</td>
                                                <td>{{ date('d-M-Y H:i:s', strtotime($tb->created_at)) }}</td>
                                                <!-- <td>{{ $tb->undian }}</td>
                                                <td>{{ $tb->created_at < date('Y-m-d H:i:s') ? 'expired' : 'active' }} -->
                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="text-center teman_sobat_paginate">
                                {{ $temanSobats->links() }}
                            </div>
                        @endif
                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->


    {{-- Modal Peraturan --}}
    <!-- <div class="modal fade" id="peraturan-event" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Teman Sobat</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Ajak sebanyak mungkin Teman Sobat untuk daftar ke Website Club Sobat Badak
                            </li>
                            <li>
                                Sobat Badak dapat melihat Leader Board untuk tahu tentang 10 Sobat Badak yang berpotensi
                                mendapatkan hadiah
                            </li>
                            <li>
                                Keputusan panitia tidak dapat diganggu gugat
                            </li>
                            <li>
                                Total Hadiah Rp14.000.000 untuk 10 orang pemenang
                                <span>Juara 1: Rp5.000.000</span><br>
                                <span>Juara 2: Rp3.000.000</span><br>
                                <span>Juara 3: Rp2.000.000</span><br>
                                <span>Juara 4: Rp1.000.000</span><br>
                                <span>Juara 5: Rp750.000</span><br>
                                <span>Juara 6 - 10: Rp500.000</span>
                            </li>
                            <li>
                              Apabila pada Website Club Sobat Badak ada "Simbol Kuning dan Tanda Seru", maka akun Anda terdeteksi spam
                            </li>
                            <li>
                              Apabila pada Website Club Sobat Badak ada "Simbol Hijau", maka akun Anda valid
                            </li>
                            <li>
                              Apabila Anda terdeteksi "Spam", maka Sobat Badak yang posisinya dibawah Anda akan secara otomatis Naik Posisi
                            </li>
                            <li>
                                Periode: 1 - 30 November 2021
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
@endsection

@section('pageJS')
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip({
                delay: {
                    hide: 100
                }
            })
        })

        $('.sobat_link').on('click', function() {
            // finds data-clipboard-test for content p class "click"
            value = $(this).data('clipboard-text');
            // Temporary input tag to store text
            var $temp = $("<input>");
            $("body").append($temp);
            // Selects text value
            $temp.val(value).select();
            // Copies text, removes temporary tag
            document.execCommand("copy");
            $temp.remove();

            // var el = $el.attr('title', 'My New Title');;
            // $(this).attr('title', 'Berhasil Copy Link')

        })
    </script>
@endsection
