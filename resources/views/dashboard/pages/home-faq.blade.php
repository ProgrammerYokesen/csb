@section('title')
    Dashboard
@endsection

@extends('dashboard.master')

@section('dash-content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-12 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row justify-content-center dash_home_navigation">

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('faqPoinPage') }}">
                            <div id="card-poin" class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/koinGK.svg') }}" alt="csb" class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        FAQ Point</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('faqAuctionPage') }}">
                            <div id="card-auction" class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/lelang.svg') }}" alt="csb" class="mb-3">

                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        FAQ Auction</div>

                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('faqQuizPage') }}">
                            <div id="card-quiz" class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/rejeki-sobat.svg') }}" alt="csb" class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        FAQ Quiz</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                {{-- BODY DISINI --}}
                <div id="faq-page">
                    @yield('inner-contents')


                    <div class="d-flex justify-content-center text-center">
                        <div class="my-6 header-text">Masih memiliki pertanyaan?</div>
                    </div>
                    <div class="d-flex justify-content-center text-center">
                        <div class="faq-content">
                            Jika kamu masih memiliki pertanyaan yang belum dapat dijawab, kamu dapat segera menghubungi
                            kami!
                        </div>
                    </div>
                    <div class="topbar-item d-flex justify-content-center text-center" style="margin-top: 2rem">
                        <a href="https://wa.me/6281287628068/?text=Halo saya ingin bertanya" target="_blank"
                            class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_homepage"
                            style="font-weight: bold!important">Hubungi
                            Kami</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(function() {
            let pathURL = window.location.pathname
            if (pathURL.includes("faq-poin")) {
                $("#card-poin").addClass('border-it')
                $("#card-quiz").removeClass('border-it')
                $("#card-auction").removeClass('border-it')
            } else if (pathURL.includes("faq-quiz")) {
                $("#card-quiz").addClass('border-it')
                $("#card-poin").removeClass('border-it')
                $("#card-auction").removeClass('border-it')
            } else if (pathURL.includes("faq-auction")) {
                $("#card-auction").addClass('border-it')
                $("#card-poin").removeClass('border-it')
                $("#card-quiz").removeClass('border-it')
            }
        })
    </script>
@endsection


@section('pageJS')

    <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>

@endsection
