@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('quizShow') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body quiz__show_rule">
                      <div class="container">
                        <div class="row" style="margin-bottom: 25px">
                          <div class="col-lg-6">
                            <img src="{{asset('images/event/1212/games/tys.png')}}" alt="">
                          </div>
                          <div class="col-lg-6">
                            <!-- <div class="quiz__show_rule_countdown">
                              01:30:00
                            </div> -->
                            <h1>Temukan Yang Sama!</h1>
                            <h6>Temukan sebanyak mungkin pasangan dari setiap kartu dan menangkan berbagai hadiah menarik!</h6>
                          </div>
                        </div>

                        <h5>Peraturan:</h5>
                        <ol>
                          <li>Sobat Badak wajib mendaftar melalui sobatbadak.club di menu “Quiz Show” pada bagian “Temukan yang Sama”</li>
                          <li>Penentuan peserta yang bermain akan dilakukan melalui spin the wheel</li>
                          <li>Sobat Badak wajib hadir saat nama dipanggil dan tidak boleh diwakilkan</li>
                          <li>Games “Temukan yang Sama” akan diadakan di Zoom Club Sobat Badak</li>
                          <li>Nama yang Sobat Badak gunakan saat di Zoom harus sesuai dengan nama saat mendaftar di Harbolnas 12.12</li>

                          <li>Sobat Badak akan diberikan waktu 5 menit untuk bermain</li>
                          <li>Akan ada 16 kartu yang Sobat Badak harus cari pasangannya</li>
                          <li>Hadiah yang Sobat Badak dapatkan berdasarkan berapa banyak pasang kartu yang ditemukan dalam 5 menit</li>

                          <li>Hadiah maksimal akan dikirimkan H+20 (hari kerja) dari tanggal Sobat Badak main</li>
                          <li>Tidak ada penggantian kuota/pulsa dan kompensasi dalam bentuk apapun</li>
                        </ol>
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
