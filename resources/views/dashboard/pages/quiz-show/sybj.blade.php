@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('quizShow') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body quiz__show_rule">
                      <div class="container">
                        <div class="row" style="margin-bottom: 25px">
                          <div class="col-lg-6">
                            <img src="{{asset('images/event/1212/games/sybj.png')}}" alt="">
                          </div>
                          <div class="col-lg-6">
                            <!-- <div class="quiz__show_rule_countdown">
                              01:30:00
                            </div> -->
                            <h1>Siapa Yang Bisa Jawab?</h1>
                            <h6>Siapa yang Bisa Jawab, nih? Semakin banyak pertanyaan yang bisa kamu jawab,  semakin besar juga hadiah yang akan kamu dapatkan, loh!</h6>
                          </div>
                        </div>

                        <h5>Peraturan:</h5>
                        <ol>
                          <li>Sobat Badak wajib mendaftar melalui sobatbadak.club di menu “Quiz Show” pada bagian “Siapa yang Bisa Jawab”</li>
                          <li>Penentuan peserta yang bermain akan dilakukan melalui spin the wheel</li>
                          <li>Sobat Badak wajib hadir saat nama dipanggil dan tidak boleh diwakilkan</li>
                          <li>Games “Siapa yang Bisa Jawab?” akan diadakan di Zoom Club Sobat Badak</li>
                          <li>Nama yang Sobat Badak gunakan saat di Zoom harus sesuai dengan nama saat mendaftar di Harbolnas 12.12</li>
                          <li>Sobat Badak akan dihadapkan dengan 5 level pertanyaan, masing-masing level terdiri atas satu pertanyaan</li>
                          <li>Pertanyaan akan berupa pilihan ganda dengan topik seputar pengetahuan umum</li>
                          <li>Apabila Sobat Badak berhasil menjawab pertanyaan dengan benar, maka bisa naik ke level selanjutnya</li>
                          <li>Apabila Sobat Badak salah menjawab, maka tidak bisa naik ke level selanjutnya</li>
                          <li>Hadiah yang didapatkan Sobat Badak berdasarkan level yang dicapainya</li>
                          <li>Hadiah maksimal akan dikirimkan H+20 (hari kerja) dari tanggal Sobat Badak main</li>
                          <li>Tidak ada penggantian kuota/pulsa dan kompensasi dalam bentuk apapun</li>
                        </ol>
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="cara-tebak-kata" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Baba Mencari Bakat</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Peserta yang sudah daftar harus hadir 15 menit sebelum Baba Mencari Bakat dimulai
                            </li>
                            <li>
                                Setiap peserta mendapatkan waktu 10 menit untuk menampilkan bakatnya
                            </li>
                            <li>
                                Setiap bakat yang ditunjukkan, <strong>tidak boleh</strong> mengandung SARA atau unsur
                                pornografi
                            </li>
                            <li>
                                Jumlah Baper Poin yang bisa didapatkan peserta berdasarkan keputusan Baba
                            </li>
                            <li>
                                Peserta harus datang sesuai jadwal yang sudah dipilih melalui sobatbadak.club
                            </li>
                            <li>
                                Silahkan daftar Baba Mencari Bakat melalui sobatbadak.club/
                            </li>
                            <li>
                                Keputusan panitia Club Sobat Badak tidak dapat diganggu gugat
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
