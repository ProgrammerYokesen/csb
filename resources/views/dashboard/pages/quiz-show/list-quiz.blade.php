@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <div class="text-right">
                              <!-- <a href="{{route('riwayatQuiz')}}" class="mb-2">
                                <button class="btn">
                                  Riwayat Partisipasi
                                </button>
                              </a> -->
                              <button class="btn" data-toggle="modal" data-target="#rule-1212">
                                Peraturan
                              </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body quiz__show">

                      <h1>Quiz Show</h1>
                      <!-- <h6 class="text-center">{{ date("d M Y") }}</h6> -->

                      <!-- <div class="text-center">
                        <a href="{{route('listPemenang')}}" class="mr-1">
                          <button class="btn" style="background: #ffaa3a; color: #ffffff">List Pemenang</button>
                        </a>
                        <a href="{{route('listBelumMain')}}" class="ml-1">
                          <button class="btn" style="background: #ffaa3a; color: #ffffff">Peserta Belum Menang</button>
                        </a>
                      </div> -->

                      <!-- <p>Halo Teman Sobat, ini saatnya kamu untuk memiliki kesempatan untuk memenangkan banyak hadiah dengan memainkan game yang ada di zoom Club Sobat Badak. Caranya gimana? tinggal daftar ikutan Event Spesial Harbolnas 12.12 dan klik daftar di game dibawah ini ya! <br> <span style="font-weight: 600; color: #ffaa3a">Kamu bisa daftar 30 menit sebelum acaranya dimulai</span>, jadi pasang alarmnya dan ikutan Quiz Shownya! Semoga beruntung!</p> -->

                      <div class="row justify-content-center align-items-center" style="margin-top: 25px;margin-bottom: 25px">

                        @php
                          $now = date("Y-m-d H:i:s");
                        @endphp

                        @foreach ($games as $key => $game)
                        <div class="col-lg-4 col-sm-6 quiz__show_game">
                            <img src="{{ $game->image_drive }}" alt="">
                            <div class="quiz__show_regis_btn">
                              <a href="{{route('gameDetail', $game->game_id )}}">
                                <button class="sec">Cara Bermain</button>
                              </a>
                            </div>
                        </div>
                        @endforeach
                      </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="rule-1212" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Quiz Show Club Sobat Badak</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Yang dapat mengikuti games ini adalah Sobat Badak yang telah memenangkan Tiket Quiz Show di Event Auction
                            </li>
                            <li>
                              Sobat Badak yang bermain di Quiz Show harus sudah melakukan konfirmasi KTP dan Data Diri pada Admin CSB
                            </li>
                            <li>
                                Sobat Badak yang bermain merupakan orang yang sama dengan KTP yang telah dikonfirmasi
                            </li>
                            <li>
                                Nama di Zoom HARUS sesuai dengan nama yang digunakan saat memenangkan Event Auction di sobatbadak.club (nama akun Club Sobat Badak)
                            </li>
                            <li>
                                Sobat Badak WAJIB hadir 10 menit sebelum jadwal yang di dapatkan
                            </li>
                            <li>
                              Jika Sobat Badak sudah dipanggil SEBANYAK 3 KALI, namun tidak hadir di Zoom. Maka kesempatan akan diberikan pada SOBAT BADAK LAINNYA YANG PERTAMA CHECK-OUT BARANG APAPUN di TOKOPEDIA WARISAN GAJAHMADA DI SAAT ITU JUGA
                            </li>
                            <li>
                                Games berlangsung di Zoom Live Club Sobat Badak setiap hari dari pukul 15.00 - 21.00 WIB
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')
@endsection
