@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('quizShow') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body quiz__show_rule">
                      <div class="container">
                        <div class="row" style="margin-bottom: 25px">
                          <div class="col-lg-6">
                            <img src="{{asset('images/event/1212/games/pyb.png')}}" alt="">
                          </div>
                          <div class="col-lg-6">
                            <!-- <div class="quiz__show_rule_countdown">
                              01:30:00
                            </div> -->
                            <h1>Pilih yang Benar</h1>
                            <h6>Pilih yang ini apa yang itu ya? Tentukan pilihanmu dan pastikan kamu memilih hadiah yang benar!</h6>
                          </div>
                        </div>

                        <h5>Peraturan:</h5>
                        <ol>
                          <li>Sobat Badak wajib mendaftar melalui sobatbadak.club di menu “Quiz Show” pada bagian “Pilih yang Benar”</li>
                          <li>Penentuan peserta yang bermain akan dilakukan melalui spin the wheel</li>
                          <li>Sobat Badak wajib hadir saat nama dipanggil dan tidak boleh diwakilkan</li>
                          <li>Games “Pilih yang Benar” akan diadakan di Zoom Club Sobat Badak</li>
                          <li>Nama yang Sobat Badak gunakan saat di Zoom harus sesuai dengan nama saat mendaftar di Harbolnas 12.12</li>

                          <li>Setiap Sobat Badak akan diberikan waktu 10 menit untuk menentukan pilihannya</li>
                          <li>Sobat Badak diperbolehkan untuk menukar pilihannya selama waktu belum habis</li>
                          <li>Apabila waktu habis, maka hadiah yang Sobat Badak dapat adalah hadiah yang terakhir dipilih</li>

                          <li>Hadiah maksimal akan dikirimkan H+20 (hari kerja) dari tanggal Sobat Badak main</li>
                          <li>Tidak ada penggantian kuota/pulsa dan kompensasi dalam bentuk apapun</li>
                        </ol>
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
