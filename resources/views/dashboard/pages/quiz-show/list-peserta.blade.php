@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('quizShow') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <button class="btn" data-toggle="modal" data-target="#rule-1212">
                              Peraturan
                            </button>
                        </div>
                    </div>
                    <div class="card-body quiz__show">

                      <h1>List Peserta - {{$event->name}}</h1>
                      <!-- <p style="margin:0">{{ date('d M Y - H:i', strtotime($event->date)) }}</p> -->

                      <div class="text-center mb-3">
                        <div class="dropdown">
                              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              {{ date('d M Y - H:i', strtotime($event->date)) }} WIB
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="max-height: 350px; overflow-y: auto">
                            @foreach ($events as $ev)
                            <a class="dropdown-item" href="{{route('listPeserta', $ev->id)}}">{{ date('d M Y - H:i', strtotime($ev->start_date)) }} WIB</a>
                            @endforeach
                          </div>
                        </div>
                      </div>

                      <p style="margin:0">Total Peserta: {{$totalData}}</p>

                      <div class="row justify-content-center align-items-center" style="margin-top: 25px">

                        @if ($user)
                        <div class="col-lg-4 mb-3">
                          <div class="list__peserta" style="background: rgba(255, 170, 58, 0.1);border-radius: 8px; border: 2px solid #ffaa3a; padding: 10px; font-weight: 500; font-size: 14px; line-height: 21px;
                          text-align: center; color: #000000;">
                            {{$user->name}} - {{$user->email}}
                          </div>
                        </div>
                        @endif

                        @foreach ($data as $key => $d)
                        <div class="col-lg-4 mb-3">
                          <div class="list__peserta" style="background: rgba(255, 170, 58, 0.1);border-radius: 8px; padding: 10px; font-weight: 500; font-size: 14px; line-height: 21px;
                          text-align: center; color: #000000;">
                            {{$d->name}} - {{$mailList[$key]}}
                          </div>
                        </div>
                        @endforeach



                      </div>

                      <div class="text-center teman_sobat_paginate">
                          {{ $data->links() }}
                      </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="rule-1212" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Yang dapat mengikuti games ini adalah Sobat Badak yang telah menyelesaikan transaksi Harbolnas 12.12
                            </li>
                            <li>
                                Sobat Badak yang bermain akan dipilih melalui Spin the Wheel
                            </li>
                            <li>
                                Untuk mencegah terhambatnya proses pemilihan nama peserta, Sobat Badak wajib mendaftar setiap kali ingin mengikuti games melalui website sobatbadak.club di menu "Quiz Show"
                            </li>
                            <li>
                                Games berlangsung di Zoom Live Club Sobat Badak dari tanggal 15-30 Desember 2021 pukul 15.00 - 21.00 WIB
                            </li>
                            <li>
                                Jika Sobat Badak tidak hadir saat nama terpilih melalui Spin the Wheel maka nama kalian akan hangus pada sesi tersebut
                            </li>
                            <li>
                                Sobat Badak dapat melakukan pendaftaran kembali di sobatbadak.club untuk games berikutnya
                            </li>
                            <li>
                                Sobat Badak hanya mendapatkan satu kali kesempatan untuk menang di games Harbolnas 12.12
                            </li>
                            <li>
                                Tanggal 25 Desember 2021 tidak akan ada sesi games dikarenakan hari libur nasional
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
