@section('title')
    Pendapat Sobat
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b">
                    <div class="card-header" style="border: transparent">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body csb__survey">

                        @php
                          $now = date("Y-m-d H:i:s");
                        @endphp

                        @if ( strtotime($now) > strtotime('2022-01-05T23:59:00'))
                        <div class="text-center">
                          <img src="{{asset('images/badak-baper.png')}}" alt="">
                          <h5>Survey sudah berakhir.</h5>
                          <p>Terima Kasih atas Partisipasi Sobat Badak. <br> Ayo dukung terus Club Sobat Badak untuk menjadi yang terbaik untuk Bawa Perubahan!.</p>
                        </div>
                        @elseif ($input_status == true || Session::get('success') == true)
                          <div class="text-center">
                            <img src="{{asset('images/badak-baper.png')}}" alt="">
                            <h5>Terima kasih untuk partisipasinya!</h5>
                            <p>Dengan partisipasi ini, kamu mendapatkan 100,000 Baper Poin. <br> Ayo dukung terus Club Sobat Badak untuk menjadi yang terbaik untuk Bawa Perubahan!</p>
                          </div>
                        @else
                          <h3>Pendapat Sobat Badak</h3>
                          <p>Menurut Sobat Badak apa aja sih jawaban dari pertanyaan-pertanyaan berikut? Segera jawab pertanyaannya dan dapatkan <span style="font-weight: 700;color: #ffaa3a">100.000 Baper Poin!</span></p>
                          <p>Psstt, <span style="font-weight: 700;color: #ffaa3a">Survey ini hanya berlangsung hingga 05 Januari pukul 23.59 WIB!</span> Yuk isi sekarang!!</p>

                          <form action="{{route('postSurvey')}}" method="post">
                            @csrf

                            @foreach ($question as $q)
                              <div class="form-group">
                                <label>{{$q->question}} <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="{{$q->id}}" placeholder="Jawaban Kamu" required autocomplete="off"/>
                              </div>
                            @endforeach

                            <div class="text-center">
                              <button type="submit" class="btn" style="margin: 25px auto; color:#fff; background: #ffaa3a; padding: 7px 20px">Submit</button>
                            </div>
                          </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
