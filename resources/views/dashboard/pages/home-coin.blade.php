@section('title')
    Dashboard
@endsection

@extends('dashboard.master')

@section('dash-content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-12 subheader-transparent dash_home_header" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-center flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex flex-column">
                        <h2 class="text-dark font-weight-bold my-2 mr-5"
                            style="background: #fff; padding: 1rem; border-radius: 5px">Hi
                            <span class="font-weight-bolder" style="color: #ffaa3a">{{ Auth::user()->name }}</span>,
                            Selamat Datang
                            di Club Sobat Badak
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row justify-content-center dash_home_navigation">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('tukarInvoice') }}">
                            <!--<a href="{{ route('comingSoonPage') }}">-->
                            <div id="card-inv" class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/invoice.svg') }}" alt="csb" class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Tiket Invoice</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('spinWheel') }}">
                            <!--<a href="{{ route('comingSoonPage') }}">-->
                            <div id="card-spin" class="card gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/spin.svg') }}" alt="csb" class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Spin It Yourself</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('coinPage') }}">
                            <!--<a href="{{ route('comingSoonPage') }}">-->
                            <div id="card-coin" class="card gutter-b card-stretch" style="position:relative">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/koinGK.svg') }}" alt="csb" class="mb-3">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Tukar Koin GK</div>
                                </div>
                                <div class="coin__hot blink" style="font-size: 14px; top: -10px; right: -10px">HOT</div>
                            </div>
                        </a>
                    </div>

                </div>
                {{-- BODY DISINI --}}
                @yield('inner-contents')
            </div>
        </div>
    </div>


    <script>
        $(function() {
            let pathURL = window.location.pathname
            if (pathURL.includes("coin")) {
                $("#card-coin").addClass('border-it')
            } else if (pathURL.includes("redeem")) {
                $("#card-redeem").addClass('border-it')
            } else if (pathURL.includes("invoice")) {
                $("#card-inv").addClass('border-it')
            } else if (pathURL.includes("spin")) {
                $("#card-spin").addClass('border-it')
            }
        })
    </script>
@endsection


@section('pageJS')

    <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
    @yield('jsFile')

@endsection
