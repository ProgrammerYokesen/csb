@section('title')
    Baba Mencari Bakat
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <button class="btn" data-toggle="modal" data-target="#cara-tebak-kata">
                                Peraturan
                            </button>
                        </div>
                    </div>
                    <div class="card-body">


                        <div class="baba_txt">
                            <div class="text-center">
                                <img src="{{ asset('images/kucing-baba.png') }}" alt="">
                            </div>

                            <h1>Audisi Baba Mencari Bakat</h1>

                            @if ($sisaSlot == 0)
                                <h5 class="red">Maaf, Slot Audisi Baba Mencari Bakat sudah habis :( <br>
                                    Ayo jangan menyerah dan ikutan Baba Mencari Bakat minggu depan!</h5>
                            @elseif ($joinBaba == 1)
                                <h5 class="red">Kamu sudah mendaftarkan diri untuk minggu ini. <br> Ayo ikutan lagi Baba
                                    Mencari
                                    Bakat minggu
                                    depan!</h5>

                                <div class="text-center">
                                    <a href="{{ route('babaSukses') }}">
                                        <button class="btn">Cek Status</button>
                                    </a>
                                </div>

                            @else
                                <h5>Sini ikutan Audisi Baba Mencari Bakat, <br> tampilkan bakat terbaikmu dan dapatkan
                                    hingga
                                    100.000
                                    Baper Poin.
                                    Ayo daftarkan dirimu sekarang!</h5>



                                <div class="input_quiz_form">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Pilih Hari</label>
                                        <div class="col-sm-9">
                                            {{-- <input type="text" class="form-control" id="kt_datepicker_1" readonly="readonly"
                                                placeholder="Pilih Hari" /> --}}
                                            <select class="form-control" id="pilih-hari" required>
                                                <option selected disabled value="">Pilih Hari</option>
                                                @php
                                                    $tudey = date('w');
                                                @endphp

                                                @foreach ($days as $day)
                                                    @if ($day['value'] <= $tudey - 1)
                                                        @php
                                                            continue;
                                                        @endphp
                                                    @endif
                                                    <option value="{{ $day['value'] }}"> {{ $day['hari'] }},
                                                        {{ $day['date'] }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Pilih Jam Tampil</label>
                                        <div class="col-sm-9">
                                            {{-- <input class="form-control" type="text" name="kata" id="input_jawaban"
                                                placeholder="Maksimal 10 Karakter" maxlength="10" autocomplete="off" required /> --}}
                                            <select class="form-control" id="pilih-jam" disabled required>
                                                <option selected disabled value="">Pilih Jam</option>
                                                {{-- <option value="18:30">18.30</option>
                                                <option value="19:00">19.00</option>
                                                <option value="19:30">19.30</option>
                                                <option value="20:00">20.00</option>
                                                <option value="20:30">20.30</option> --}}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button class="btn" data-toggle="modal" data-target="#modal_persetujuan">DAFTAR
                                            SEKARANG</button>
                                    </div>
                                </div>
                            @endif


                        </div>






                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="cara-tebak-kata" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Baba Mencari Bakat</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Peserta yang sudah daftar harus hadir 15 menit sebelum Baba Mencari Bakat dimulai
                            </li>
                            <li>
                                Setiap peserta mendapatkan waktu 10 menit untuk menampilkan bakatnya
                            </li>
                            <li>
                                Setiap bakat yang ditunjukkan, <strong>tidak boleh</strong> mengandung SARA atau unsur
                                pornografi
                            </li>
                            <li>
                                Jumlah Baper Poin yang bisa didapatkan peserta berdasarkan keputusan Baba
                            </li>
                            <li>
                                Peserta harus datang sesuai jadwal yang sudah dipilih melalui sobatbadak.club
                            </li>
                            <li>
                                Silahkan daftar Baba Mencari Bakat melalui sobatbadak.club/
                            </li>
                            <li>
                                Keputusan panitia Club Sobat Badak tidak dapat diganggu gugat
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Persetujuan --}}
    <div class="modal fade" id="modal_persetujuan" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_baba">
                {{-- <div class="modal-header" style="border: transparent">
                    <h5>Disclaimer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div> --}}
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                    <h4>Disclaimer</h4>
                    <h6>Dengan mendaftar Saya menyetujui bahwa bakat yang saya lakukan akan direkam dan dapat dipergunakan
                        untuk
                        seluruh platform CSB</h6>

                    <div class="modal_baba_btn">
                        <button class="btn batal" data-dismiss="modal">Batal</button>
                        <button class="btn daftar" onclick="daftarBaba()">Daftar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>

    <script>
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        let endTgl = new Date()
        endTgl.setDate(endTgl.getDate() + 6);

        $('#kt_datepicker_1').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: "DD, dd MM yyyy",
            startDate: new Date(),
            endDate: endTgl,
        });
    </script>

    <script>
        function daftarBaba() {

            // let jam = document.getElementById('pilih-jam').value
            // let tangg = document.getElementById('kt_datepicker_1').value

            // let spl = tangg.split(" ")

            // let tgl = spl[1]
            // var bulan = ""
            // let tahun = spl[3]
            // switch (spl[2]) {
            //     case "January":
            //         bulan = "1"
            //         break;
            //     case "February":
            //         bulan = "2"
            //         break;
            //     case "March":
            //         bulan = "3"
            //         break;
            //     case "April":
            //         bulan = "4"
            //         break;
            //     case "May":
            //         bulan = "5"
            //         break;
            //     case "June":
            //         bulan = "6"
            //         break;
            //     case "July":
            //         bulan = "7"
            //         break;
            //     case "August":
            //         bulan = "8"
            //         break;
            //     case "September":
            //         bulan = "9"
            //         break;
            //     case "October":
            //         bulan = "10"
            //         break;
            //     case "November":
            //         bulan = "11"
            //         break;
            //     case "December":
            //         bulan = "12"
            //         break;
            // }


            // let tanggalPost = `${tahun}-${bulan}-${tgl} ${jam}:00`

            // console.log(tanggalPost)

            let dataHari = document.getElementById('pilih-hari').value
            let dataJam = document.getElementById('pilih-jam').value

            // console.log(dataHari, dataJam)

            if (dataHari == "" || dataJam == "") {
                Swal.fire("Oops!", "Mohon Isi form terlebih dahulu", "error");
                return
            }



            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('postAudition') }}',
                data: {
                    data_hari: dataHari,
                    data_jam: dataJam
                },
                success: function(data) {
                    console.log(data)
                    if (data.Status == "Success") {
                        window.location.href = '{{ env('APP_URL') }}/baba-mencari-bakat/status'
                    }
                },
                error: function(data) {
                    Swal.fire("Ada kesalahan dalam server", "Silahkan refresh dan coba lagi", "error");
                }

            });
        }
    </script>

    <script>
        $("#pilih-hari").change(function() {
            var val = $(this).val(); //get the value
            $("#pilih-jam").attr('disabled', false);
            // console.log(val)

            $('#pilih-jam').find('option').remove().end()


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{ route('sisaSlotBaba') }}',
                data: {
                    day: val
                },
                success: function(data) {
                    if (data) {

                        let dataJam = data.slot
                        dataJam.forEach(e => {
                            $("#pilih-jam").append(
                                `<option value="${e.id}" ${e.slot == 0 ? 'disabled' : ""}  >${e.time_event} - ${e.slot} Slot Tersisa</option>`
                            );
                        });
                    }

                },
                error: function(data) {
                    Swal.fire("Ada kesalahan dalam server", "Silahkan refresh dan coba lagi", "error");
                }

            });
        });
    </script>
@endsection
