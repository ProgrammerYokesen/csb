@section('title')
    Quiz Tebak Kata
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>


        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row justify-content-center dash_home_navigation">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('tebakKata') }}" id="tebak_kata">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/tebak-kata.svg') }}" alt="csb">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Quiz Tebak Kata</div>
                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>


                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('rejekiSobat') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/rejeki-sobat.svg') }}" alt="csb">

                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Quiz Rejeki Sobat</div>

                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>

                </div>
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->


        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b tebak_kata_card">
                    <div class="tebak_kata_card_head">
                        <a href="{{ route('homeDash') }}" class="btn">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                        <div class="btn cara_bermain" data-toggle="modal" data-target="#cara-tebak-kata">
                            Cara Bermain
                        </div>
                    </div>

                    {{-- <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div> --}}
                    <div class="card-body">

                        @if ($quiz == 0)
                            <h1 class="mb-5">Sedang tidak ada quiz hari ini...</h1>
                            <a href="{{ route('homeDash') }}" class="mt-3">
                                <button class="btn">Kembali</button>
                            </a>


                            <div class="submit_tebak_kata">
                                <h4 class="ugc_tebak_kata">Kamu punya pertanyaan receh dan menarik ga?</h4>
                                <a href="{{ route('bikinQuiz') }}">
                                    <button class="btn">Submit Disini</button>
                                </a>
                            </div>

                        @elseif ($doneQuiz == true)

                            <h1 class="mb-3">Kamu sudah mengikuti QUIZ TEBAK KATA hari ini.</h1>
                            <h5 class="mb-5">Yuk kembali lagi besok dan dapatkan Baper Poin sebanyak-banyaknya!</h5>
                            <a href="{{ route('homeDash') }}" class="mt-3">
                                <button class="btn">Kembali</button>
                            </a>

                            <div class="submit_tebak_kata">
                                <h4 class="ugc_tebak_kata">Kamu punya pertanyaan receh dan menarik ga?</h4>
                                <a href="{{ route('bikinQuiz') }}">
                                    <button class="btn">Submit Disini</button>
                                </a>
                            </div>

                        @else
                            <h1>Quiz Tebak Kata</h1>
                            <h4>Ayo tebak kuis yang mimin Badak kasih hari ini
                                dan menangkan hingga
                                <span class="custom-text-primary">{{ $totalPoin }} Baper Poin</span> tiap harinya <br>
                                Psst.. Jawabannya tidak perlu tepat, yang penting harus out of the box ya!
                            </h4>
                            <a href="{{ route('handleTebakKata') }}">
                                <button class="btn">Mulai</button>
                            </a>

                            <div class="submit_tebak_kata">
                                <h4 class="ugc_tebak_kata">Kamu punya pertanyaan receh dan menarik ga?</h4>
                                <a href="{{ route('bikinQuiz') }}">
                                    <button class="btn">Submit Disini</button>
                                </a>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Tebak Kata --}}
    <div class="modal fade" id="cara-tebak-kata" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Cara Bermain Quiz Tebak Kata</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Tebak jawaban dari pertanyaan yang akan muncul
                            </li>
                            <li>
                                Ketik huruf yang terkandung dari jawaban yang kamu tebak didalam box, lalu klik enter
                            </li>
                            <li>
                                Kamu memiliki 3 kali kesempatan salah, jika berhasil menjawab kamu akan memperoleh 100 poin,
                                jika gagal kamu tidak memperoleh poin
                            </li>
                            <li>
                                Kumpulkan baper poin dalam sesi ini, selamat bermain!
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
