@section('title')
    Dashboard
@endsection

@extends("crudbooster::admin_template")

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-play"></i>Managemen Koin Gatotkaca</strong>
        </div>
        {{-- BODY DISINI --}}
        <div class="panel-body">
            <!--begin::Container-->
            <form class="form" id="redeem-form" action="{{ route('editTukarKoin') }}" method="POST">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Nama</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input readonly type="text" class="form-control" name="nama" value='{{$getData->name}}'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Alamat</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <textarea readonly class="form-control" id="exampleTextarea" name="alamat" rows="3">{{$getData->alamat}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>No Wa</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input readonly type="tel" class="form-control" name="no_wa" value='{{$getData->whatsapp}}'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Email</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input readonly type="email" class="form-control" name="email" value='{{$getData->email}}'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Kode</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input readonly type="text" class="form-control" name="kode" value='{{$getData->kode_penukaran}}'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Status Koin</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <div class="row">
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="{{asset('$getAllkoin[1]')}}" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[1], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[1]->qty}}" name="koin1">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin1">
                                @endif
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/kecil-merah.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[2], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[2]->qty}}" name="koin2">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin2">
                                @endif
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;" >
                                <img src="/images/kecil-hijau.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[0], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[0]->qty}}" name="koin3">
                                @else  
                                    <input type="text" class="form-control" readonly value="0" name="koin3">
                                @endif
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/gatot-head.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[3], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[3]->qty}}" name="koin4">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin4">
                                @endif
                            </div>

                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/gatot-body.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[4], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[4]->qty}}" name="koin5">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin5">
                                @endif
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/gatot-side.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[5], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[5]->qty}}" name="koin6">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin6">
                                @endif
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/dark-red-1.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[6], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[6]->qty}}" name="koin7">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin7">
                                @endif
                            </div> 
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/dark-red-2.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[7], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[7]->qty}}" name="koin8">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin8">
                                @endif
                            </div> 
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/dark-red-3.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[8], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[8]->qty}}" name="koin9">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin9">
                                @endif
                            </div> 
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/main-gatot.png" alt="" style="margin-right: 1rem">
                                @if (property_exists($getAllkoin[9], 'qty'))
                                    <input type="text" class="form-control" readonly value="{{$getAllkoin[9]->qty}}" name="koin10">
                                @else    
                                    <input type="text" class="form-control" readonly value="0" name="koin10">
                                @endif
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Jumlah Poin</label>
                    </div>
                    <div class="col-sm-9 col-12" style="display: flex; align-items: center">
                        <input readonly type="text" class="form-control" name="jumlah_poin" style="margin-right: 1rem" value="{{$totalPoin}}"/>
                        <h3 style="margin:0">BP</h3>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Status Koin</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <div class="radio-list" style="margin-left: 2rem" id='statusRadio' >
                            <!--<label class="radio">-->
                            <!--    <input disabled type="radio" name="radioes4" />-->
                            <!--    <span></span>-->
                            <!--    Belum Diterima-->
                            <!--</label>-->
                            <label class="radio">
                                <input type="radio" value="0" name="radioes4" required/>
                                <span></span>
                                Diterima Sesuai
                            </label>
                            <label class="radio">
                                <input type="radio" name="radioes4" value="1" />
                                <span></span>
                                Diterima Tidak Sesuai
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row hide" id="konsolidasiKoin">
                    <div class="col-sm-3 col-12">
                        <label>Konsolidasi Koin</label>
                    </div>
                    <div class="col-sm-9 col-12 ">
                        <div class="row">
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/kecil-hijau.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[0]->id}}" value="0">
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/kecil-biru.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[1]->id}}" value="0">
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/kecil-merah.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[2]->id}}" value="0">
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/gatot-head.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[3]->id}}" value="0">
                            </div>

                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/gatot-body.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[4]->id}}" value="0">
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/gatot-side.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[5]->id}}" value="0">
                            </div>
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/dark-red-1.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[6]->id}}" value="0">
                            </div> 
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/dark-red-2.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[7]->id}}" value="0">
                            </div> 
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/dark-red-3.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[8]->id}}" value="0">
                            </div> 
                            <div class="col-sm-2" style="margin-top: 2rem; display:flex; align-items: center;">
                                <img src="/images/main-gatot.png" alt="" style="margin-right: 1rem">
                                <input type="number" class="form-control" name="{{$getAllkoin[9]->id}}" value="0">
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="form-group row hide" id="konsolidasiPoin">
                    <div class="col-sm-3 col-12">
                        <label>Jumlah Poin yang diberikan</label>
                    </div>
                    <div class="col-sm-9 col-12" style="display: flex; align-items: center">
                        <input type="text" class="form-control" name="gived_poin" style="margin-right: 1rem" value=0 disabled/>
                        <h3 style="margin:0">BP</h3>
                    </div>
                </div> 
                    <div class="form-group"> 
                        <div style="display: flex; justify-content: center;margin-top: 1rem">
                            <input type="submit" name="submit" value="Confirm" class="btn btn-success" style="width: 150px">
                        </div>
                    </div> 
            </form>
        </div>
    </div>

@endsection


@section('jsPage')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#statusRadio input').on('change', function(){
                var value = $('input[name=radioes4]:checked', '#statusRadio').val()
                if(value == "1"){
                    $("#konsolidasiKoin").removeClass('hide');
                    $("#konsolidasiPoin").removeClass('hide');
                }else{
                    $("#konsolidasiKoin").addClass('hide');
                    $("#konsolidasiPoin").addClass('hide');
                }
            })
            $('input[name="1"], input[name="2"], input[name="3"], input[name="4"], input[name="5"], input[name="5"], input[name="6"], input[name="7"],input[name="8"], input[name="9"], input[name="10"]').on('keyup', function() {
               var value =  $(this).val();
               var id = $(this).attr('name')
               var bp = $('input[name="gived_poin"]').val()
               var url = '{{route('calculateBPTidakSesuai', [':id', ':value']) }}';
                  url = url.replace(':id', id);
                  url = url.replace(':value', value);
               if(value != ''){
                $.ajax({
                    type:"GET",
                    // url:'calculate-bp/tidak-sesuai/' +name+'/'+value,
                    url:url,
                    success:function(data){
                        console.log(data)
                        var totalPoin = bp + data
                        $('input[name="gived_poin"]').val(totalPoin)
                        console.log(value, 'kosong', name, bp);
                    }
                })
                   
               }else{
                console.log(value, 'kosong', name, bp);
                   
               }
            })
        })
    </script>

@endsection
