@extends('dashboard.pages.home-coin')

@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container">
            <div class="card card-custom gutter-b gacha_inv_card">
                <div class="card-body">

                    <div class="gacha_card_head">
                        <a href="{{ route('homeDash') }}">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>

                        <div class="d-flex flex-column justify-content-center">
                            <a href="{{ route('gachaTicket') }}" class="to_gacha text-center mb-3">
                                Gacha Tiket
                            </a>
                            <button class="btn" data-toggle="modal" data-target="#modal_riwayat_gacha">Riwayat
                                Gacha</button>
                        </div>
                    </div>

                    <h3>Beli Koin Gatotkaca di WGM dan copy paste invoicemu disini</h3>

                    <form class="form gacha_inv_form" action="{{ route('checkInvoice') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="invoice" class="form-control" autocomplete="off"
                                placeholder="Masukkan Nomor Invoice Disini" required />
                        </div>

                        <div class="d-flex justify-content-center mt-6">
                            <span>Notes: Perhatikan produk yang kamu beli harus benar. Silahkan <a
                                    href="https://www.tokopedia.com/warisangajahmada/paket-6pcs-edisi-gatotkaca-koin-larutan-penyegar-cap-badak-rasa-leci-leci"
                                    target="_blank">klik
                                    disini</a></span>
                        </div>

                        <div class="d-flex justify-content-center mt-6">
                            <button data-toggle="modal" data-target="#modal_inv_cek"
                                class="redeem-button btn btn-block dash_nav_homepage py-4">Redeem</button>
                        </div>
                    </form>
                    {{-- </form> --}}
                </div>
            </div>
            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>

    {{-- <button data-toggle="modal" data-target="#modal_inv_wrong">Click</button>
    <button data-toggle="modal" data-target="#modal_inv_used">Click</button>
    <button data-toggle="modal" data-target="#modal_inv_cek">Click</button> --}}

    @if (Session::has('Code'))
        {{-- Modal Invoice Salah --}}
        <div class="modal fade" id="modal_inv_wrong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body modal_intro">
                        <h6>Maaf, Nomor Invoicemu salah</h6>
                        <p>Silahkan cek lagi nomor invoice yang ada pada pembelianmu dan pastikan bahwa produk yang dibeli
                            benar.
                        </p>
                        <button class="btn" data-dismiss="modal">OK!</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- Modal Sudah Digunakan --}}
        <div class="modal fade" id="modal_inv_used" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body modal_intro">
                        <h6>Nomor Invoicemu telah digunakan</h6>
                        <p>Silahkan cek lagi nomor invoice yang ada pada pembelianmu dan pastikan bahwa produk yang dibeli
                            benar.
                        </p>
                        <button class="btn" data-dismiss="modal">OK!</button>
                    </div>
                </div>
            </div>
        </div>

        @if (Session::get('Code') == 1)
            {{-- Modal Cek Order --}}
            <div class="modal fade" id="modal_inv_cek" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body modal_intro">
                            <h6>Informasi Invoice Pesanan</h6>
                            <p>Invoice: {{ Session('Data')->invoice }}</p>
                            <p>Nama: {{ Session('Data')->nama }}</p>
                            <p>No. Telp: {{ Session('Data')->noHp }}</p>
                            <p>Platform: {{ Session('Data')->platform }}</p>
                            <p>Total Order: {{ Session('Data')->qtyTotal }}</p>
                            <form action="{{ route('PostGachaTiket') }}" method="POST">
                                @csrf
                                <input type="hidden" name="invoice" value="{{ Session('Data')->invoice }}">
                                <button type="submit" class="btn">Lanjut</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Modal Invoice Bukan STW --}}
        <div class="modal fade" id="modal_inv_csb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body modal_intro">
                        <h6>Maaf, Invoicemu tidak dapat ditukar</h6>
                        <p>Produk yang Kamu beli <b>hanya dapat</b> digunakan di Spin The Wheel Live Stream yang ada di Zoom
                            Club Sobat Badak.
                        </p>
                        <button class="btn" data-dismiss="modal">OK!</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    {{-- Modal Invoice Bukan STW --}}
    <div class="modal fade" id="modal_inv_csb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body modal_intro">
                    <h6>Maaf, Invoicemu tidak dapat ditukar</h6>
                    <p>Produk yang Kamu beli <b>hanya dapat</b> digunakan di Spin The Wheel Live Stream yang ada di Zoom
                        Club Sobat Badak.
                    </p>
                    <button class="btn" data-dismiss="modal">OK!</button>
                </div>
            </div>
        </div>
    </div>


    {{-- Jika user tidak punya quota gacha --}}
    <div class="modal fade" id="modal_no_gacha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body modal_intro">
                    <h6>Maaf, Kamu tidak memiliki kesempatan Gacha Tiket</h6>
                    <p>Silahkan beli Produk Khusus di Warisan Gajahmada Tokopedia </p>
                    <button class="btn" data-dismiss="modal">OK!</button>
                </div>
            </div>
        </div>
    </div>


    {{-- Modal Riwayat Gacha --}}
    <div class="modal fade" id="modal_riwayat_gacha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_baper_poin">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_baper_poin_header">
                        <h5>
                            Riwayat Gacha
                        </h5>
                        <select name="month" id="selectMonth">
                            <option value="01" {{ date('m') == 1 ? 'selected' : '' }}>January {{ date('Y') }}
                            </option>
                            <option value="02" {{ date('m') == 2 ? 'selected' : '' }}>Februari {{ date('Y') }}
                            </option>
                            <option value="03" {{ date('m') == 3 ? 'selected' : '' }}>Maret {{ date('Y') }}</option>
                            <option value="04" {{ date('m') == 4 ? 'selected' : '' }}>April {{ date('Y') }}</option>
                            <option value="05" {{ date('m') == 5 ? 'selected' : '' }}>Mei {{ date('Y') }}</option>
                            <option value="06" {{ date('m') == 6 ? 'selected' : '' }}>Juni {{ date('Y') }}</option>
                            <option value="07" {{ date('m') == 7 ? 'selected' : '' }}>Juli {{ date('Y') }}</option>
                            <option value="08" {{ date('m') == 8 ? 'selected' : '' }}>Agustus {{ date('Y') }}
                            </option>
                            <option value="09" {{ date('m') == 9 ? 'selected' : '' }}>September {{ date('Y') }}
                            </option>
                            <option value="10" {{ date('m') == 10 ? 'selected' : '' }}>Oktober {{ date('Y') }}
                            </option>
                            <option value="11" {{ date('m') == 11 ? 'selected' : '' }}>November {{ date('Y') }}
                            </option>
                            <option value="12" {{ date('m') == 12 ? 'selected' : '' }}>Desember {{ date('Y') }}
                            </option>
                        </select>
                    </div>
                    <table class="table table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <td>Waktu</td>
                                <td class="text-center">Tiket yang didapat</td>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll scroll-pull" data-scroll="true" data-height="300">
                        <table class="table table-borderless table-striped">
                            <tbody id="fullHistory">
                                @foreach ($riwayats as $riwayat)
                                    <tr>
                                        <td>{{ date('d M Y H:i:s', strtotime($riwayat->created_at)) }}</td>
                                        <td>Tiket {{ $riwayat->cat_name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="border: transparent">
                </div>
            </div>
        </div>
    </div>


@endsection

@section('jsFile')
    @if (Session::get('Code') == 1)
        <script>
            $('#modal_inv_cek').modal('show');;
        </script>
    @elseif (Session::get('Code') == 0)
        <script>
            $('#modal_inv_wrong').modal('show');;
        </script>
    @elseif (Session::get('Code') == 2)
        <script>
            $('#modal_inv_used').modal('show');;
        </script>
    @elseif (Session::get('Code') == 4)
        <script>
            $('#modal_inv_csb').modal('show');;
        </script>
    @endif

    @if (Session::has('error'))
        <script>
            $('#modal_no_gacha').modal('show');
        </script>
    @endif


    <script>
        $('#selectMonth').change(function() {

            // You can access the value of your select field using the .val() method
            // alert('Select field value has changed to' + $('#selectMonth').val());

            // You can perform an ajax request using the .ajax() method
            $.ajax({
                type: 'GET',
                url: '{{ route('ajaxRiwayatSitw') }}', // This is the url that will be requested
                data: {
                    month: $('#selectMonth').val()
                },

                // This is an object of values that will be passed as GET variables and 
                // available inside changeStatus.php as $_GET['selectFieldValue'] etc...

                // This is what to do once a successful request has been completed - if 
                // you want to do nothing then simply don't include it. But I suggest you 
                // add something so that your use knows the db has been updated{}
                success: function(data) {
                    var datas = data.riwayats;
                    if (datas.length == 0) {
                        deleteData();
                    } else {
                        $("#fullHistory").empty();
                        datas.forEach(element => {
                            var poin = element.poin;
                            var table = document.getElementById("fullHistory");
                            var row = table.insertRow(0);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var d = new Date(element.created_at);
                            cell1.innerHTML = `<td class="poin">${element.created_at}</td>`;
                            cell2.innerHTML = `<td>Tiket ${element.cat_name}</td>`;
                        });
                    }
                    // alert('Select field value has changed to' + $('#selectMonth').val());
                },
            });

        });

        function deleteData() {
            $("#fullHistory").empty();
        }
    </script>
@endSection
