@extends('dashboard.pages.home-coin')

@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container">
            <div class="card card-custom gutter-b gacha_inv_card">
                <div class="card-body">
                    <div class="gacha_header">
                        <a href="{{ route('tukarInvoice') }}">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                        <button class="btn" data-toggle="modal" data-target="#modal_riwayat_gacha">Riwayat Gacha</button>
                    </div>
                    <div class="text-center">
                        <h3 id="sisaTiket">Kesempatan Gacha Tersisa: <span class="oren">{{ $sisa }}</span></h3>
                        {{-- <img src="{{ asset('images/gacha.png') }}" alt="" class="gacha_img"> --}}
                        <img src="{{ asset('video/gacha.png') }}" alt="" class="gacha_img" id="gacha_machine">
                        {{-- <video src="{{ asset('video/gacha-vid.mp4') }}"></video> --}}
                        <div id="kt_nouislider_5"
                            class="gacha_slider nouislider nouislider-handle-warning nouislider-drag-danger">
                        </div>

                        <h5 id="gacha_caption">Geser untuk membuka bola tiket</h5>
                    </div>
                </div>
            </div>
            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>

    {{-- <button data-toggle="modal" data-target="#modal_menang">Click</button>
    <button type="button" class="btn btn-light font-weight-bold" id="kt_blockui_page_custom_spinner">Custom spinner</button> --}}

    {{-- Modal Invoice Salah --}}
    <div class="modal fade" id="modal_menang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="gacha_gif" id="gachagif">
            <img src="{{ asset('video/gacha.gif') }}" alt="" class="gacha_img" id="gif-gacha">
        </div>
        <div class="modal-dialog modal-dialog-centered d-none" role="document" id="modal_win">
            <div class="modal-content">
                <div class="modal-body modal_intro">
                    <img id="winningImage" src="{{ asset('images/premium.png') }}" alt=""
                        class="img-fluid gacha_ticket_img">
                    <h6 id="congrats"></h6>
                    <p>Kamu dapat menggunakannya di <strong>Spin It Yourself</strong> untuk mendapatkan Baper Poin. Semoga
                        Beruntung!
                    </p>
                    <button class="btn" data-dismiss="modal">OK!</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Riwayat Gacha --}}
    <div class="modal fade" id="modal_riwayat_gacha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_baper_poin">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_baper_poin_header">
                        <h5>
                            Riwayat Gacha
                        </h5>
                        <select name="month" id="selectMonth">
                            <option value="01" {{ date('m') == 1 ? 'selected' : '' }}>January {{ date('Y') }}
                            </option>
                            <option value="02" {{ date('m') == 2 ? 'selected' : '' }}>Februari {{ date('Y') }}
                            </option>
                            <option value="03" {{ date('m') == 3 ? 'selected' : '' }}>Maret {{ date('Y') }}</option>
                            <option value="04" {{ date('m') == 4 ? 'selected' : '' }}>April {{ date('Y') }}</option>
                            <option value="05" {{ date('m') == 5 ? 'selected' : '' }}>Mei {{ date('Y') }}</option>
                            <option value="06" {{ date('m') == 6 ? 'selected' : '' }}>Juni {{ date('Y') }}</option>
                            <option value="07" {{ date('m') == 7 ? 'selected' : '' }}>Juli {{ date('Y') }}</option>
                            <option value="08" {{ date('m') == 8 ? 'selected' : '' }}>Agustus {{ date('Y') }}
                            </option>
                            <option value="09" {{ date('m') == 9 ? 'selected' : '' }}>September {{ date('Y') }}
                            </option>
                            <option value="10" {{ date('m') == 10 ? 'selected' : '' }}>Oktober {{ date('Y') }}
                            </option>
                            <option value="11" {{ date('m') == 11 ? 'selected' : '' }}>November {{ date('Y') }}
                            </option>
                            <option value="12" {{ date('m') == 12 ? 'selected' : '' }}>Desember {{ date('Y') }}
                            </option>
                        </select>
                    </div>
                    <table class="table table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <td>Waktu</td>
                                <td class="text-center">Tiket yang didapat</td>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll scroll-pull" data-scroll="true" data-height="300">
                        <table class="table table-borderless table-striped">
                            <tbody id="fullHistory">
                                @foreach ($riwayats as $riwayat )                      
                                <tr>
                                    <td>{{ date('d M Y H:i:s', strtotime($riwayat->created_at)) }}</td>
                                    <td>Tiket {{ $riwayat->cat_name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="border: transparent">
                </div>
            </div>
        </div>
    </div>


@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/nouislider.js') }}"></script>
    <script src="{{ asset('assets/js/pages/features/miscellaneous/blockui.js') }}"></script>

    <script>
        var gachaSlider = function() {
            // init slider
            var slider = document.getElementById('kt_nouislider_5');

            noUiSlider.create(slider, {
                start: 0,
                connect: true,
                range: {
                    min: 0,
                    max: 100
                }
            });

            slider.noUiSlider.on('change', function(values, handle) {
                if (values[handle] < 100) {
                    slider.noUiSlider.set(0);
                }

                // Jika Slide Sampe Ujung
                if (values[handle] == 100) {


                    // Block Page saat loading Ajax
                    KTApp.blockPage({
                        overlayColor: '#000000',
                        state: 'warning', // a bootstrap color
                        size: 'lg' //available custom sizes: sm|lg
                    });

                    // Ajax here
                    $.ajax({ //create an ajax request to display.php
                        type: "GET",
                        url: "{{ route('postGacha') }}",
                        success: function(data) {
                            // console.log(data);
                            if (data.Code == 1) {
                                // $('#image-zoom img').attr("src",data.Image);
                                document.getElementById("winningImage").src = data.Image;
                                $("#congrats").html(
                                    `Selamat! Kamu mendapatkan tiket ${data.Ticket}`);
                                $("#sisaTiket").html(`Kesempatan Gacha Tersisa: ${data.sisaTiket}`);

                                $('#modal_menang').modal('show')

                                $("#kt_nouislider_5").addClass("d-none")
                                $("#gacha_caption").addClass("d-none")


                                document.getElementById("gif-gacha").src = ""

                                document.getElementById("gif-gacha").src =
                                    "{{ asset('video/gacha.gif') }}"

                                $("#modal_win").addClass("d-none")
                                $("#gachagif").removeClass("d-none")

                                KTApp.unblockPage();

                                setTimeout(function() {
                                    $("#gachagif").addClass("d-none")
                                    $("#modal_win").removeClass("d-none")
                                    $("#kt_nouislider_5").removeClass("d-none")
                                    $("#gacha_caption").removeClass("d-none")
                                }, 8500);

                                // setTimeout(function() {
                                //     $('#modal_menang').modal('toggle')
                                //     $("#kt_nouislider_5").removeClass("d-none")
                                //     $("#gacha_caption").removeClass("d-none")
                                // }, 9000);

                                // Set Slider to 0
                                slider.noUiSlider.set(0);
                            } else {
                                KTApp.unblockPage();
                                slider.noUiSlider.set(0);
                                alert("Kesempatan Gacha Telah Habis!!");
                            }
                        }
                    });

                    // Unblock Page Saat Ajax done
                    // setTimeout(function() {
                    //     KTApp.unblockPage();
                    //     $('#modal_menang').modal('toggle')
                    // }, 2000);

                }
            });
        }

        jQuery(document).ready(function() {
            gachaSlider()
        });


        $('#kt_blockui_page_custom_spinner').click(function() {
            KTApp.blockPage({
                overlayColor: '#000000',
                state: 'warning', // a bootstrap color
                size: 'lg' //available custom sizes: sm|lg
            });

            setTimeout(function() {
                KTApp.unblockPage();
            }, 2000);
        });
    </script>

<script>
    $('#selectMonth').change(function() {

        // You can access the value of your select field using the .val() method
        // alert('Select field value has changed to' + $('#selectMonth').val());

        // You can perform an ajax request using the .ajax() method
        $.ajax({
            type: 'GET',
            url: '{{ route('ajaxRiwayatSitw') }}', // This is the url that will be requested
            data: {
                month: $('#selectMonth').val()
            },

            // This is an object of values that will be passed as GET variables and 
            // available inside changeStatus.php as $_GET['selectFieldValue'] etc...

            // This is what to do once a successful request has been completed - if 
            // you want to do nothing then simply don't include it. But I suggest you 
            // add something so that your use knows the db has been updated{}
            success: function(data) {
                var datas = data.riwayats;
                if (datas.length == 0) {
                    deleteData();
                } else {
                    $("#fullHistory").empty();
                    datas.forEach(element => {
                        var poin = element.poin;
                        var table = document.getElementById("fullHistory");
                        var row = table.insertRow(0);
                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var d = new Date(element.created_at);
                        cell1.innerHTML = `<td class="poin">${element.created_at}</td>`;
                        cell2.innerHTML = `<td>Tiket ${element.cat_name}</td>`;
                    });
                }
                // alert('Select field value has changed to' + $('#selectMonth').val());
            },
        });

    });

    function deleteData() {
        $("#fullHistory").empty();
    }
</script>
@endsection
