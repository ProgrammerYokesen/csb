@section('title')
    Edit Profile
@endsection

@extends('dashboard.pages.home-coin')

@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">

                    <a href="{{ route('homeDash') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>

                    <div class="d-flex">
                        <div id="ticket__reg" style="align-items: center">
                            <img class="mr-2" src='/images/regular-ticket.png' alt="">
                            <div class="text-box-card">
                                {{ $ticketReguler }}
                            </div>
                        </div>
                        <div id="ticket__pre" style="align-items: center">
                            <img class="mr-2" src='/images/premium-ticket.png' alt="">
                            <div class="text-box-card">
                                {{ $ticketPremium }}
                            </div>
                        </div>

                        <button class="btn dash_nav_homepage ml-5" data-toggle="modal"
                            data-target="#modal_riwayat_spin">Riwayat
                            Spin</button>
                    </div>

                </div>
                <div class="d-flex justify-content-center text-center gacha-header">
                    <div class="redeem coin-header-text">Spin It Yourself!</div>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <div class="mr-5">
                            <div id="box-regular" class="box-card" onclick="chooseWheel('box-regular')">
                                <div class="text-box-card">
                                    Wheel Regular
                                </div>
                            </div>
                        </div>
                        <div class="ml-5">
                            <div id="box-premium" class="box-card" onclick="chooseWheel('box-premium')">
                                <div class="text-box-card">
                                    Wheel Premium
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <canvas id="canvas__reg" width="500" height="500">
                            <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                                try
                                another.</p>
                        </canvas>
                        <canvas id="canvas__pre" width="500" height="500">
                            <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                                try
                                another.</p>
                        </canvas>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button id="bigButton" onclick="spinProcess(); this.disabled=true" class="btn dash_nav_homepage"
                            style="font-size: 14px;padding:0.7rem 2rem;">
                            SPIN
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal-->
    <div class="modal fade" id="wheel-modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex justify-content-center" style="align-items: center">
                        <div class="circle-text">
                            BP
                        </div>
                        <div class="ml-2 big-text" style="margin:3rem 0">
                            x <span class="text-prize"></span>
                        </div>
                    </div>
                    <div class="">
                        <div class="title">
                            Selamat! Kamu mendapatkan <span class="text-prize"></span> Baper Poin
                        </div>
                        <div class="subtitle">
                            Yuk kumpulin terus dan menangkan lebih banyak <br>
                            lelangan di Club Sobat Badak!
                        </div>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <button onclick="elementID == 'canvas__pre' ? resetPremium() : resetRegular()"
                            class="redeem-button btn btn-block dash_nav_homepage py-4" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title" style="text-align: center;margin:1rem 0">
                        Yuk beli Koin Gatotkaca dan dapatkan Tiket Premium
                    </div>
                    <div class="subtitle">
                        Beli produk di Warisan Gajahmada dengan tanda khusus <a target="_blank"
                            style="color: #ffaa3a;text-decoration: underline"
                            href="https://www.tokopedia.com/warisangajahmada/paket-6pcs-edisi-gatotkaca-koin-larutan-penyegar-cap-badak-rasa-leci-leci">Spin
                            The Wheel</a> dan copy paste invoicemu
                        untuk mendapatkan Tiket Spin The Wheel
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <button class="redeem-button btn btn-block dash_nav_homepage py-4" data-dismiss="modal">OK!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="error-modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="">
                        <div class="title" style="text-align: center;margin:1rem 0">
                            Gagal Melakukan Spin!
                        </div>
                        <div class="subtitle">
                            Mohon maaf, sepertinya terjadi kesalahan saat melakukan spinwheel. Pastikan bahwa kamu memiliki
                            tiket yang cukup!
                        </div>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <button class="redeem-button btn btn-block dash_nav_homepage py-4" data-dismiss="modal">OK!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Riwayat Spin --}}
    <div class="modal fade" id="modal_riwayat_spin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_baper_poin">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_baper_poin_header">
                        <h5>
                            Riwayat Spin It Yourself
                        </h5>
                        <select name="month" id="selectMonth">
                            <option value="01" {{ date('m') == 1 ? 'selected' : '' }}>January {{ date('Y') }}
                            </option>
                            <option value="02" {{ date('m') == 2 ? 'selected' : '' }}>Februari {{ date('Y') }}
                            </option>
                            <option value="03" {{ date('m') == 3 ? 'selected' : '' }}>Maret {{ date('Y') }}</option>
                            <option value="04" {{ date('m') == 4 ? 'selected' : '' }}>April {{ date('Y') }}</option>
                            <option value="05" {{ date('m') == 5 ? 'selected' : '' }}>Mei {{ date('Y') }}</option>
                            <option value="06" {{ date('m') == 6 ? 'selected' : '' }}>Juni {{ date('Y') }}</option>
                            <option value="07" {{ date('m') == 7 ? 'selected' : '' }}>Juli {{ date('Y') }}</option>
                            <option value="08" {{ date('m') == 8 ? 'selected' : '' }}>Agustus {{ date('Y') }}
                            </option>
                            <option value="09" {{ date('m') == 9 ? 'selected' : '' }}>September {{ date('Y') }}
                            </option>
                            <option value="10" {{ date('m') == 10 ? 'selected' : '' }}>Oktober {{ date('Y') }}
                            </option>
                            <option value="11" {{ date('m') == 11 ? 'selected' : '' }}>November {{ date('Y') }}
                            </option>
                            <option value="12" {{ date('m') == 12 ? 'selected' : '' }}>Desember {{ date('Y') }}
                            </option>
                        </select>
                    </div>
                    <table class="table table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <td>Waktu</td>
                                <td>Tipe Wheel</td>
                                <td>Poin</td>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll scroll-pull" data-scroll="true" data-height="300">
                        <table class="table table-borderless table-striped">
                            <tbody id="fullHistory">
                                @foreach ($riwayats as $riwayat)
                                    <tr>
                                        <td>{{ date('d M Y H:i:s', strtotime($riwayat->created_at)) }}</td>
                                        <td>{{ $riwayat->from }}</td>
                                        <td>{{ number_format($riwayat->poin) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="border: transparent">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')
    <script>
        var listPremium = <?php echo $premiumPrize; ?>;
        var listRegular = <?php echo $regularPrize; ?>;
        var countPremium = listPremium.length
        var countRegular = listRegular.length
    </script>
    <script src="{{ asset('user-dashboard/spin-wheel.js') }}"></script>

    <script>
        $('#selectMonth').change(function() {

            // You can access the value of your select field using the .val() method
            // alert('Select field value has changed to' + $('#selectMonth').val());

            // You can perform an ajax request using the .ajax() method
            $.ajax({
                type: 'GET',
                url: '{{ route('ajaxRiwayatSpin') }}', // This is the url that will be requested
                data: {
                    month: $('#selectMonth').val()
                },

                // This is an object of values that will be passed as GET variables and 
                // available inside changeStatus.php as $_GET['selectFieldValue'] etc...

                // This is what to do once a successful request has been completed - if 
                // you want to do nothing then simply don't include it. But I suggest you 
                // add something so that your use knows the db has been updated{}
                success: function(data) {
                    var datas = data.riwayats;
                    if (datas.length == 0) {
                        deleteData();
                    } else {
                        $("#fullHistory").empty();
                        datas.forEach(element => {
                            var poin = element.poin;
                            var table = document.getElementById("fullHistory");
                            var row = table.insertRow(0);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);
                            var d = new Date(element.created_at);
                            cell1.innerHTML = `<td>${element.created_at}</td>`;
                            cell2.innerHTML = `<td>${element.from}</td>`;
                            cell3.innerHTML = `<td>${element.poin}</td>`;
                        });
                    }
                    // alert('Select field value has changed to' + $('#selectMonth').val());
                },
            });

        });

        function deleteData() {
            $("#fullHistory").empty();
        }
    </script>
@endsection
