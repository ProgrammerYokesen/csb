@section('title')
    Edit Profile
@endsection

@extends('dashboard.pages.home-coin')

{{-- TODO 
make alogirthm to get number position
from backend using AJAX 
and loading while fetching from backend --}}

@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">

                    <a href="{{ route('homeDash') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>

                </div>
                <div class="d-flex justify-content-center text-center">
                    <div class="redeem coin-header-text">Pilih <i>Spin The Wheel</i></div>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <div class="mr-5">
                            <div id="box-regular" class="box-card" onclick="chooseWheel('box-regular')">
                                <div class="text-box-card">
                                    Wheel Regular
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mt-4">
                                <img class="mr-2" src="/images/regular-ticket.png" alt="">
                                <div class="text-box-card">
                                    3
                                </div>
                            </div>
                        </div>
                        <div class="ml-5">
                            <div id="box-premium" class="box-card" onclick="chooseWheel('box-premium')">
                                <div class="text-box-card">
                                    Wheel Premium
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mt-4">
                                <img class="mr-2" src="/images/premium-ticket.png" alt="">
                                <div class="text-box-card">
                                    1
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gacha-text" style="margin-top: 2rem">
                        Belum punya tiket? Beli <a href="#" class="gacha-link">Koin Gatotkaca</a> di WGM dan copy paste
                        invoicemu disini
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 3rem">
                        <a href="{{ route('spinWheel', "type=premium") }}"
                            class="redeem-button btn btn-block dash_nav_homepage py-4">Lanjut</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')
    <script>
        function chooseWheel(choosed) {
            if (choosed.includes("regular")) {
                $("#box-regular").addClass('border-it')
                $("#box-premium").removeClass('border-it')
            } else if (choosed.includes("premium")) {
                $("#box-premium").addClass('border-it')
                $("#box-regular").removeClass('border-it')
            }
        }
    </script>
@endsection
