@section('title')
    Edit Profile
@endsection

@extends('dashboard.pages.home-coin')

@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">

                    <a href="{{ route('homeDash') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>

                </div>
                <div class="d-flex justify-content-center text-center gacha-header">
                    <div class="redeem coin-header-text">Spin It Yourself!</div>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <div class="mr-5">
                            <div id="box-regular" class="box-card" onclick="chooseWheel('box-regular')">
                                <div class="text-box-card">
                                    Wheel Regular
                                </div>
                            </div>
                        </div>
                        <div class="ml-5">
                            <div id="box-premium" class="box-card" onclick="chooseWheel('box-premium')">
                                <div class="text-box-card">
                                    Wheel Premium
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <canvas id="canvas__reg" width="500" height="500">
                            <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                                try
                                another.</p>
                        </canvas>
                        <canvas id="canvas__pre" width="500" height="500">
                            <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                                try
                                another.</p>
                        </canvas>
                    </div>
                    {{-- <div class="d-flex justify-content-center">
                        <button id="bigButton" onclick="spinProcess(); this.disabled=true" class="btn dash_nav_homepage"
                            style="font-size: 14px;padding:0.7rem 2rem;">
                            SPIN
                        </button>
                    </div> --}}
                    <div class="d-flex justify-content-center" style="margin-top: 4rem">
                        <div class="form-group row" style="width: 500px">
                            <div class="col-md-9 col-12 mt-2">
                                <input id="userEmail" type="email" class="form-control" placeholder="Masukan email user" required />
                            </div>
                            <div class="col-md-3 col-12 mt-2 text-right">
                                <button type="submit" onclick="spinProcess();" id="submit__btn" class="btn dash_nav_homepage"
                                    style="font-size: 14px;padding:0.7rem 2rem;">
                                    Spin
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal-->
    <div class="modal fade" id="wheel-modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex justify-content-center" style="align-items: center">
                        <div class="circle-text">
                            BP
                        </div>
                        <div class="ml-2 big-text" style="margin:3rem 0">
                            x <span class="text-prize"></span>
                        </div>
                    </div>
                    <div class="">
                        <div class="title">
                            Selamat! Kamu mendapatkan <span class="text-prize"></span> Baper Poin
                        </div>
                        <div class="subtitle">
                            Yuk kumpulin terus dan menangkan lebih banyak <br>
                            lelangan di Club Sobat Badak!
                        </div>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <button onclick="elementID == 'canvas__pre' ? resetPremium() : resetRegular()"
                            class="redeem-button btn btn-block dash_nav_homepage py-4" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title" style="text-align: center;margin:1rem 0">
                        Yuk beli Koin Gatotkaca dan dapatkan Tiket Premium
                    </div>
                    <div class="subtitle">
                        Beli produk di Warisan Gajahmada dengan tanda khusus <a target="_blank"
                            style="color: #ffaa3a;text-decoration: underline"
                            href="https://www.tokopedia.com/warisangajahmada/paket-6pcs-edisi-gatotkaca-koin-larutan-penyegar-cap-badak-rasa-leci-leci">Spin
                            The Wheel</a> dan copy paste invoicemu
                        untuk mendapatkan Tiket Spin The Wheel
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <button class="redeem-button btn btn-block dash_nav_homepage py-4" data-dismiss="modal">OK!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="error-modal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="">
                        <div class="title" style="text-align: center;margin:1rem 0">
                            Gagal Melakukan Spin!
                        </div>
                        <div class="subtitle">
                            Mohon maaf, sepertinya terjadi kesalahan saat melakukan spinwheel. Pastikan bahwa kamu memiliki
                            tiket yang cukup!
                        </div>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 2rem">
                        <button class="redeem-button btn btn-block dash_nav_homepage py-4" data-dismiss="modal">OK!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Riwayat Spin --}}
    {{-- <div class="modal fade" id="modal_riwayat_spin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_baper_poin">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_baper_poin_header">
                        <h5>
                            Riwayat Spin It Yourself
                        </h5>
                        <select name="month" id="selectMonth">
                            <option value="01" {{ date('m') == 1 ? 'selected' : '' }}>January {{ date('Y') }}
                            </option>
                            <option value="02" {{ date('m') == 2 ? 'selected' : '' }}>Februari {{ date('Y') }}
                            </option>
                            <option value="03" {{ date('m') == 3 ? 'selected' : '' }}>Maret {{ date('Y') }}</option>
                            <option value="04" {{ date('m') == 4 ? 'selected' : '' }}>April {{ date('Y') }}</option>
                            <option value="05" {{ date('m') == 5 ? 'selected' : '' }}>Mei {{ date('Y') }}</option>
                            <option value="06" {{ date('m') == 6 ? 'selected' : '' }}>Juni {{ date('Y') }}</option>
                            <option value="07" {{ date('m') == 7 ? 'selected' : '' }}>Juli {{ date('Y') }}</option>
                            <option value="08" {{ date('m') == 8 ? 'selected' : '' }}>Agustus {{ date('Y') }}
                            </option>
                            <option value="09" {{ date('m') == 9 ? 'selected' : '' }}>September {{ date('Y') }}
                            </option>
                            <option value="10" {{ date('m') == 10 ? 'selected' : '' }}>Oktober {{ date('Y') }}
                            </option>
                            <option value="11" {{ date('m') == 11 ? 'selected' : '' }}>November {{ date('Y') }}
                            </option>
                            <option value="12" {{ date('m') == 12 ? 'selected' : '' }}>Desember {{ date('Y') }}
                            </option>
                        </select>
                    </div>
                    <table class="table table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <td>Waktu</td>
                                <td>Tipe Wheel</td>
                                <td>Poin</td>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll scroll-pull" data-scroll="true" data-height="300">
                        <table class="table table-borderless table-striped">
                            <tbody id="fullHistory">
                                @foreach ($riwayats as $riwayat)
                                    <tr>
                                        <td>{{ date('d M Y H:i:s', strtotime($riwayat->created_at)) }}</td>
                                        <td>{{ $riwayat->from }}</td>
                                        <td>{{ number_format($riwayat->poin) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="border: transparent">
                </div>
            </div>
        </div>
    </div> --}}
@endsection

@section('pageJS')
    <script>
        var listPremium = <?php echo $premiumPrize; ?>;
        var listRegular = <?php echo $regularPrize; ?>;
        var countPremium = listPremium.length
        var countRegular = listRegular.length

        var elementID = "canvas__reg"
        var clickable = true;

        $(function() {
            $('#canvas__pre').css("display", "none")
            $('#ticket__pre').css("display", "none")
            $("#box-regular").addClass('border__active')
            $('#ticket__reg').addClass("d-flex")
        })

        function chooseWheel(choosed) {
            if (clickable) {
                if (choosed.includes("regular")) {
                    $("#box-regular").addClass('border__active')
                    $("#box-premium").removeClass('border__active')
                    $('#canvas__pre').css("display", "none")
                    $('#canvas__reg').css("display", "block")

                    $('#ticket__pre').css("display", "none")
                    $('#ticket__reg').css("display", "block")
                    $('#ticket__reg').addClass("d-flex")
                    $('#ticket__pre').removeClass("d-flex")
                    elementID = "canvas__reg"
                } else if (choosed.includes("premium")) {
                    $("#box-premium").addClass('border__active')
                    $("#box-regular").removeClass('border__active')
                    $('#canvas__reg').css("display", "none")
                    $('#canvas__pre').css("display", "block")

                    $('#ticket__reg').css("display", "none")
                    $('#ticket__pre').css("display", "block")
                    $('#ticket__pre').addClass("d-flex")
                    $('#ticket__reg').removeClass("d-flex")
                    elementID = "canvas__pre"
                }
            }
        }

        // Make Color Generator
        function generateColor(idx) {
            let colors = ['#249924', '#3369e8', '#d50f26', '#eeb211']
            return colors[idx]
        }

        function triggerModal(res) {
            $(".text-prize").text(res.text)
            $("#wheel-modal").modal('toggle')
            clickable = true
        }

        function confirmModal() {
            $("#confirm-modal").modal('toggle');
            clickable = true;
            bigButton.disabled = false;
        }

        function errorModal() {
            $("#error-modal").modal('toggle');
            clickable = true;
            bigButton.disabled = false;
        }

        function preparingWheel() {
            KTApp.blockPage({
                overlayColor: 'black',
                opacity: 0.1,
                state: 'primary',
                message: 'Preparing...'
            });
        }

        // AJAX handle bookmark and unbookmark 
        function spinProcess() {
            var email = document.getElementById("userEmail").value;
            console.log(email);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('spinWheelAdmin') }}",
                type: "POST",
                data: {
                    type: elementID == "canvas__reg" ? 2 : 1,
                    email: email
                },
                beforeSend: function() {
                    preparingWheel()
                    clickable = false
                },
                success: function(res) {
                    console.log(res)
                    if (res.code == 0) {
                        KTApp.unblockPage();
                        confirmModal()
                    } else {
                        setTimeout(function() {
                            KTApp.unblockPage();
                            if (elementID === "canvas__reg") {
                                $("#ticket__reg .text-box-card").text(res.ticketReguler)
                                calculateRegular(res.sudut)
                            } else {
                                $("#ticket__pre .text-box-card").text(res.ticketPremium)
                                calculatePremium(res.sudut)
                            }
                        }, 2000);
                    }
                },
                error: function(err) {
                    console.log("ERROR")
                    console.log(err)
                    KTApp.unblockPage();
                    errorModal()
                }
            })
        }

        //the wheel
        var i = 0
        let wheelRegular = new Winwheel({
            'canvasId': 'canvas__reg',
            'numSegments': countRegular,
            'outerRadius': 200,
            'segments': listRegular.map((val) => {
                if (i == 4) i = 0
                return {
                    'fillStyle': generateColor(i++),
                    'text': `${val.point}`
                }
            }),
            'animation': {
                'type': 'spinToStop',
                'duration': 5,
                'spins': countRegular,
                'callbackAfter': 'regularTriangle()',
                'callbackFinished': triggerModal
            }
        });
        i = 0
        let wheelPremium = new Winwheel({
            'canvasId': 'canvas__pre',
            'numSegments': countPremium,
            'outerRadius': 200,
            'segments': listPremium.map((val) => {
                if (i == 4) i = 0
                return {
                    'fillStyle': generateColor(i++),
                    'text': `${val.point}`
                }
            }),
            'animation': {
                'type': 'spinToStop',
                'duration': 5,
                'spins': countPremium,
                'callbackAfter': 'premiumTriangle()',
                'callbackFinished': triggerModal
            }
        });

        // Function with formula to work out stopAngle before spinning animation.
        // Called from Click of the Spin button.
        function calculateRegular(cons_position) {
            let stopAt = cons_position;
            wheelRegular.animation.stopAngle = stopAt;
            wheelRegular.startAnimation();
        }

        function calculatePremium(cons_position) {
            let stopAt = cons_position;
            wheelPremium.animation.stopAngle = stopAt;
            wheelPremium.startAnimation();
        }

        // Usual pointer drawing code.
        premiumTriangle();
        regularTriangle();

        function premiumTriangle() {
            // Get the canvas context the wheel uses.
            let ctx = wheelPremium.ctx;
            ctx.strokeStyle = 'navy'; // Set line colour.
            ctx.fillStyle = 'black'; // Set fill colour. 
            ctx.beginPath(); // Begin path.
            ctx.moveTo(170 + 50, 5 + 20); // Move to initial position.
            ctx.lineTo(230 + 50, 5 + 20); // Draw lines to make the shape.
            ctx.lineTo(200 + 50, 40 + 20);
            ctx.lineTo(171 + 50, 5 + 20);
            ctx.stroke(); // Complete the path by stroking (draw lines).
            ctx.fill(); // Then fill.
        }

        function regularTriangle() {
            // Get the canvas context the wheel uses.
            let ctx = wheelRegular.ctx;
            ctx.strokeStyle = 'navy'; // Set line colour.
            ctx.fillStyle = 'gray'; // Set fill colour. 
            ctx.beginPath(); // Begin path.
            ctx.moveTo(170 + 50, 5 + 20); // Move to initial position.
            ctx.lineTo(230 + 50, 5 + 20); // Draw lines to make the shape.
            ctx.lineTo(200 + 50, 40 + 20);
            ctx.lineTo(171 + 50, 5 + 20);
            ctx.stroke(); // Complete the path by stroking (draw lines).
            ctx.fill(); // Then fill.
        }

        // Function to reset the wheel
        function resetRegular() {
            wheelRegular.stopAnimation(false);
            wheelRegular.rotationAngle = 0;
            wheelRegular.draw();
            regularTriangle();
            bigButton.disabled = false;
        }

        function resetPremium() {
            wheelPremium.stopAnimation(false);
            wheelPremium.rotationAngle = 0;
            wheelPremium.draw();
            premiumTriangle();
            bigButton.disabled = false;
        }
    </script>

    <script>
        $('#selectMonth').change(function() {

            // You can access the value of your select field using the .val() method
            // alert('Select field value has changed to' + $('#selectMonth').val());

            // You can perform an ajax request using the .ajax() method
            $.ajax({
                type: 'GET',
                url: '{{ route('ajaxRiwayatSpin') }}', // This is the url that will be requested
                data: {
                    month: $('#selectMonth').val()
                },

                // This is an object of values that will be passed as GET variables and 
                // available inside changeStatus.php as $_GET['selectFieldValue'] etc...

                // This is what to do once a successful request has been completed - if 
                // you want to do nothing then simply don't include it. But I suggest you 
                // add something so that your use knows the db has been updated{}
                success: function(data) {
                    var datas = data.riwayats;
                    if (datas.length == 0) {
                        deleteData();
                    } else {
                        $("#fullHistory").empty();
                        datas.forEach(element => {
                            var poin = element.poin;
                            var table = document.getElementById("fullHistory");
                            var row = table.insertRow(0);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);
                            var d = new Date(element.created_at);
                            cell1.innerHTML = `<td>${element.created_at}</td>`;
                            cell2.innerHTML = `<td>${element.from}</td>`;
                            cell3.innerHTML = `<td>${element.poin}</td>`;
                        });
                    }
                    // alert('Select field value has changed to' + $('#selectMonth').val());
                },
            });

        });

        function deleteData() {
            $("#fullHistory").empty();
        }
    </script>
@endsection
