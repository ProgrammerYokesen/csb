@section('title')
    Ganti Password
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <form id="form-edit-profile" action="{{ route('handleChangePassword') }}" method="post">
                    @csrf
                    <div class="card card-custom gutter-b" style="padding-bottom: 3rem">
                        <div class="tebak_kata_card_head head-edit-profile justify-content-between d-flex">
                            {{-- <div class="card-title"> --}}
                            <a href="{{ route('editProfile') }}">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <div class="d-flex" id="button-in-desktop">
                                {{-- <a href="#" class="btn custom-white-btn py-2 px-6 mr-2" style="font-size: 14px;">
                                    Ganti Passsword
                                </a> --}}
                                <button type="submit" class="btn dash_nav_homepage py-2 px-6" style="font-size: 14px">
                                    Simpan
                                </button>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center" style="margin-bottom: 2rem">
                            <h1 style="margin: 0" class="bold-36-black">Ganti Password</h1>
                        </div>
                        <div class="card-body card-ganti-pass">
                            <div class="form-group row" style="align-items: center">
                                <label class="col-md-4 col-sm-12 col-form-label" style="text-align: left">Password
                                    Lama</label>
                                <div class="col-md-8 col-sm-12" style="position: relative">
                                    <input id="old_password" class="form-control" type="password" name="old_password" />
                                    <i class="fas fa-eye old_password" id="togglePassword"
                                        onclick="togglePassword('old_password')"
                                        style="position: absolute; right:2rem;top:0.9rem; cursor: pointer;"></i>
                                    <div id="error-old_password" class="custom-text-error"></div>
                                </div>
                            </div>
                            <div class="form-group row" style="align-items: center">
                                <label class="col-md-4 col-sm-12 col-form-label" style="text-align: left">Password
                                    Baru</label>
                                <div class="col-md-8 col-sm-12" style="position: relative">
                                    <input id="new_password" class="form-control" type="password" name="new_password" />
                                    <i class="fas fa-eye new_password" id="togglePassword"
                                        onclick="togglePassword('new_password')"
                                        style="position: absolute; right:2rem;top:0.9rem; cursor: pointer;"></i>
                                    <div id="error-new_password" class="custom-text-error"></div>
                                </div>
                            </div>
                            <div class="form-group row" style="align-items: center">
                                <label class="col-md-4 col-sm-12 col-form-label" style="text-align: left">Konfirmasi
                                    Password Baru</label>
                                <div class="col-md-8 col-sm-12" style="position: relative">
                                    <input id="confirm_new_password" class="form-control" type="password"
                                        name="confirm_new_password" />
                                    <i class="fas fa-eye confirm_new_password" id="togglePassword"
                                        onclick="togglePassword('confirm_new_password')"
                                        style="position: absolute; right:2rem;top:0.9rem; cursor: pointer;"></i>
                                    <div id="error-confirm_new_password" class="custom-text-error"></div>
                                </div>
                            </div>

                            <div id="button-in-mobile" class="d-flex justify-content-center pt-6">
                                {{-- <a href="#" class="btn custom-white-btn py-2 px-6 mr-2" style="font-size: 14px;">
                                    Ganti Passsword
                                </a> --}}
                                <button type="submit" class="btn dash_nav_homepage py-2 px-6" style="font-size: 14px">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    @if (Session::has('beda'))
        <script>
            $(function() {
                Swal.fire("Gagal!", '{{ Session::get('beda') }}', "error");
            })
        </script>
    @elseif(Session::has("success"))
        <script>
            $(function() {
                Swal.fire("Berhasil!", '{{ Session::get('success') }}', "success");
            })
        </script>
    @endif

    <script>
        function togglePassword(elmID) {
            let password = document.getElementById(elmID);
            if (password.type === "password") {
                password.type = "text";
                $(`.${elmID}`).removeClass("fa-eye")
                $(`.${elmID}`).addClass("fa-eye-slash")
            } else {
                password.type = "password";
                $(`.${elmID}`).removeClass("fa-eye-slash")
                $(`.${elmID}`).addClass("fa-eye")
            }
        }
        $(function() {
            $("#form-edit-profile").on('submit', function(e) {
                let new_password = $('#new_password').val();
                let confirm_new_password = $('#confirm_new_password').val();
                let old_password = $('#old_password').val();

                let isValid = new_password.length >= 6 &&
                    confirm_new_password.length >= 6 &&
                    old_password.length >= 6 &&
                    new_password === confirm_new_password;

                if (!isValid) {
                    e.preventDefault();
                    // password lama 
                    if (old_password.length < 1) {
                        $("#error-old_password").text("field tidak boleh kosong!")
                        $("#error-old_password").show()
                    } else if (old_password.length < 6) {
                        $("#error-old_password").text("minimal 6 karakter!")
                        $("#error-old_password").show()
                    } else {
                        $("#error-old_password").hide()
                    }

                    // password baru 
                    if (new_password.length < 1) {
                        $("#error-new_password").text("field tidak boleh kosong!")
                        $("#error-new_password").show()
                    } else if (new_password.length < 6) {
                        $("#error-new_password").text("minimal 6 karakter!")
                        $("#error-new_password").show()
                    } else {
                        $("#error-new_password").hide()
                    }

                    // confirm password
                    if (confirm_new_password.length < 1) {
                        $("#error-confirm_new_password").text("field tidak boleh kosong!")
                        $("#error-confirm_new_password").show()
                    } else if (confirm_new_password.length < 6) {
                        $("#error-confirm_new_password").text("minimal 6 karakter!")
                        $("#error-confirm_new_password").show()
                    } else {
                        $("#error-confirm_new_password").hide()
                    }

                    // password beda
                    if (new_password != confirm_new_password) {
                        $("#error-new_password").text("password tidak sama!")
                        $("#error-new_password").show()
                        $("#error-confirm_new_password").text("password tidak sama!")
                        $("#error-confirm_new_password").show()
                    }

                } else {

                    $("#error-confirm_new_password").hide()
                    $("#error-new_password").hide()
                    $("#error-old_password").hide()

                    $("#error-confirm_new_password").text("")
                    $("#error-new_password").text("")
                    $("#error-old_password").text("")
                }
            })

        })
    </script>
@endsection
