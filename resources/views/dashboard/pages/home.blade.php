@section('title')
    Dashboard
@endsection

@extends('dashboard.master')

@section('page_assets')
<style>
  .regional__banner .banner__mobile {
    display: none;
  }

  @media screen and (max-width: 425px){
    .regional__banner .banner__mobile {
      display: block;
    }

    .regional__banner .banner__desktop {
      display: none;
    }
  }

</style>
@endsection

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-12 subheader-transparent dash_home_header" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-center flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Heading-->
                    <div class="d-flex flex-column">
                        <!--begin::Title-->
                        <h2 class="text-dark font-weight-bold my-2 mr-5"
                            style="background: #fff; padding: 1rem; border-radius: 5px">Hi
                            <span class="font-weight-bolder" style="color: #ffaa3a">{{ Auth::user()->name }} {!! badgeUser(Auth::id()) !!}</span>,
                            Selamat Datang
                            di Club Sobat Badak
                        </h2>
                        <!--end::Title-->
                        <!--begin::Breadcrumb-->
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row justify-content-center dash_home_navigation">

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('quizShow') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/quiz-show.svg') }}" alt="csb">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Quiz Show</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('tebakKata') }}" id="tebak_kata">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/gamezone.svg') }}" alt="csb">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Game Zone</div>
                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('auctionPage') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/lelang.svg') }}" alt="csb">

                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Event Auction</div>
                                    @if ($ongoingLelang)
                                        <div
                                            class="text-dark font-weight-bolder font-size-h6 mt-1 d-flex justify-content-center align-items-center">
                                            <div class="live-icon blink"></div> LIVE NOW!
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>


                    <!-- <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('jadwalPage') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/jadwal.svg') }}" alt="csb">

                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Jadwal Minggu Ini</div>

                                </div>
                            </div>
                        </a>
                    </div> -->

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('tukarInvoice') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center" style="position: relative">
                                    <img src="{{ asset('images/icons/tambang.svg') }}" alt="csb">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Tambang Baper</div>
                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('temanSobat') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center" style="position: relative">
                                    <img src="{{ asset('images/icons/bawa-sobat.svg') }}" alt="csb">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Teman Sobat Leaderboard</div>
                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>

                    <!-- <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{ route('survey') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/survey.svg') }}" alt="csb">
                                    <div class="dash_ribbon dash_ribbon-top-right"><span>HOT</span></div>
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Form Survey</div>

                                </div>
                            </div>
                        </a>
                    </div> -->

                    <!-- <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <a href="{{route('quizShowImlekDetail')}}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center position-relative">
                                    <div class="dash_ribbon dash_ribbon-top-right"><span>HOT</span></div>
                                    <img src="{{ asset('images/icons/imlek-new.svg') }}" alt="csb">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Quiz Show Imlek</div>

                                </div>
                            </div>
                        </a>
                    </div> -->

                </div>
                <!--end::Row-->

                <!-- <div class="row sppa__banner" style="margin: 0 auto;margin-bottom: 15px">
                    <a href="{{ route('sppaWelcomePage') }}">
                        <img class="banner__desktop" src="{{ asset('images/event/sppa-desk-slim.png') }}" alt="" style="width: 100%; border-radius: 10px">
                        <img class="banner__mobile" src="{{ asset('images/event/sppa.jpg') }}" alt="" style="width: 100%; border-radius: 10px">
                    </a>
                </div> -->

                <div class="row regional__banner" style="margin: 0 auto;margin-bottom: 15px">
                    <a href="{{route('listUserReg')}}">
                        <img class="banner__desktop" src="{{ asset('images/regional/banner-menu.jpg') }}" alt="" style="width: 100%; border-radius: 10px">
                        <img class="banner__mobile" src="{{ asset('images/regional/banner-menu-mobile.jpg') }}" alt="" style="width: 100%; border-radius: 10px">
                    </a>
                </div>

                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card card-custom gutter-b dash_home_card_header">
                            <div class="card-header dash_header_red">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Jumlah Baper Poinmu
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                {{ number_format($poin, 0, ',', '.') }}
                            </div>
                        </div>

                        {{-- Begin Riwayat Baper Poin --}}
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header align-items-center border-0 mt-4">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="font-weight-bolder text-dark">Riwayat Baper Poin</span>
                                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@php echo date('M Y') @endphp</span>
                                </h3>

                                {{-- DETAIL RIWAYAT BAPER POIN --}}
                                <div class="card-toolbar">
                                    <div class="dropdown dropdown-inline">
                                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ki ki-bold-more-hor"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                                            <!--begin::Navigation-->
                                            <ul class="navi navi-hover">


                                                <li class="navi-item" data-toggle="modal"
                                                    data-target="#kt_scrollable_modal_1">
                                                    <a href="javascript:void(0)" class="navi-link">
                                                        <span class="navi-text">
                                                            <span>Lihat Detail</span>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--end::Navigation-->
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-4">
                                <!--begin::Timeline-->
                                <div class="timeline timeline-6 mt-3">

                                    @if (count($riwayats) > 0)
                                        @foreach ($riwayats as $riwayat)

                                            <!--begin::Item-->
                                            <div class="timeline-item align-items-start">
                                                <!--begin::Label-->
                                                <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">
                                                    {{ date('d/m', strtotime($riwayat->created_at)) }}</div>
                                                <!--end::Label-->
                                                <!--begin::Badge-->
                                                <div class="timeline-badge">
                                                    <i
                                                        class="fa fa-genderless {{ $riwayat->poin > 0 ? 'text-success' : 'text-danger' }} icon-xl"></i>
                                                </div>
                                                <!--end::Badge-->
                                                <!--begin::Content-->
                                                <div class="timeline-content d-flex flex-column">
                                                    @if ($riwayat->status == 1)
                                                        @if ($riwayat->poin > 0)
                                                            <span
                                                                class="font-weight-bolder text-dark-75 pl-3 font-size-lg">+{{ number_format($riwayat->poin, 0, ',', '.') }}</span>
                                                        @else
                                                            <span
                                                                class="font-weight-bolder text-dark-75 pl-3 font-size-lg">{{ number_format($riwayat->poin, 0, ',', '.') }}</span>
                                                        @endif
                                                    @else
                                                        <span
                                                            class="font-weight-bolder text-dark-75 pl-3 font-size-lg">+0</span>
                                                    @endif
                                                    <span class="font-weight-bold text-dark-75 pl-3 font-size-lg">
                                                        {{-- {{ $riwayat->from }} --}}
                                                        @if ($riwayat->from == 'tebak-kata')
                                                            {{ 'Quiz Tebak Kata' }}
                                                        @elseif ($riwayat->from == 'quiz rejeki sobat')
                                                            {{ 'Quiz Rejeki Sobat' }}
                                                        @elseif ($riwayat->from == 'Bawa teman sobat')
                                                            {{ 'Bawa Teman Sobat' }}
                                                        @elseif ($riwayat->from == 'Redeem Voucher')
                                                            {{ 'Redeem Voucher' }}
                                                        @else
                                                            {{ $riwayat->from }}
                                                        @endif
                                                    </span>
                                                </div>
                                                <!--end::Content-->
                                            </div>
                                            <!--end::Item-->
                                        @endforeach
                                    @else
                                        {{-- If Empty --}}

                                        @php
                                            $bulan = date('M');
                                        @endphp

                                        <div class="dash_home_empty">
                                            <p>Tidak ada riwayat Baper Poin
                                                {{ $bulan }} 2021</p>
                                        </div>
                                    @endif

                                </div>
                                <!--end::Timeline-->




                            </div>
                            <!--end: Card Body-->
                        </div>
                        {{-- End Riwayat Baper Poin --}}

                    </div>

                    <div class="col-xl-4">
                        <div class="card card-custom gutter-b dash_home_card_header">
                            <div class="card-header dash_header_blue">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Baper Poin yang bisa <br> kamu dapatkan hari ini
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                {{ number_format($todayPoin, 0, ',', '.') }}
                            </div>
                        </div>

                        {{-- Begin Misi Hari Ini --}}
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header align-items-center border-0 mt-4">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="font-weight-bolder text-dark">Misi Hari Ini</span>
                                    <span
                                        class="text-muted mt-3 font-weight-bold font-size-sm">{{ date('d M Y') }}</span>
                                </h3>

                                {{-- Jika Sudah Selesai Semua Misi --}}
                                @if ($centangTebakKata > 0 && $centangRejekiSobat > 0)
                                    <div class="mt-3 mission_done">
                                        <span class="font-weight-bold">Kamu sudah menyelesaikan semua misi hari
                                            ini.</span>
                                    </div>
                                @endif
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">
                                {{-- @foreach ($todayMissions as $mission) --}}
                                <!--begin::Item-->
                                {{-- <div class="d-flex align-items-center dash_mission_item"> --}}
                                <!--begin::Bullet-->
                                {{-- <span class="bullet bullet-bar bg-primary align-self-stretch"></span> --}}
                                <!--end::Bullet-->
                                <!--begin::Checkbox-->
                                {{-- <label
                                        class="checkbox checkbox-lg checkbox-light-secondary checkbox-inline flex-shrink-0 m-0 mx-4">
                                        <input type="checkbox" value="1" disabled />
                                        <span></span>
                                    </label> --}}
                                <!--end::Checkbox-->
                                <!--begin::Text-->
                                {{-- <div class="d-flex flex-column flex-grow-1"> --}}
                                {{-- <a href="#" class="text-dark-75 text-blue font-weight-bolder font-size-lg mb-1">{{ $mission->mission_name }}</a>
                                            <span class="text-muted font-weight-bold">+ {{ $mission->poin }}</span> --}}
                                {{-- <a href="#" class="text-dark-75 text-blue font-weight-bolder font-size-lg mb-1">Join
                                            Zoom</a>
                                        <span class="text-muted font-weight-bold">+ 2,500 Poin</span>
                                    </div> --}}
                                <!--end::Text-->
                                {{-- </div> --}}
                                <!--end::Item-->
                                {{-- @endforeach --}}

                                @if ($tebakKataPoin > 0)
                                    <!--begin::Item-->
                                    <div class="d-flex align-items-center mt-7 dash_mission_item">
                                        <!--begin::Bullet-->
                                        <span class="bullet bullet-bar bg-danger align-self-stretch"></span>
                                        <!--end::Bullet-->
                                        <!--begin::Checkbox-->
                                        <label
                                            class="checkbox checkbox-lg checkbox-light-secondary checkbox-inline flex-shrink-0 m-0 mx-4">
                                            <input type="checkbox" value="1" disabled
                                                {{ $centangTebakKata > 0 ? 'checked' : '' }} />
                                            <span></span>
                                        </label>
                                        <!--end::Checkbox-->
                                        <!--begin::Text-->
                                        <div class="d-flex flex-column flex-grow-1">
                                            <a href="{{ route('tebakKata') }}"
                                                class="text-dark-75 text-red font-size-sm font-weight-bolder font-size-lg mb-1">Quiz
                                                Tebak Kata</a>
                                            <span class="text-muted font-weight-bold">+ {{ $tebakKataPoin }}
                                                poin</span>
                                        </div>
                                        <!--end::Text-->
                                    </div>
                                    <!--end::Item-->
                                @endif

                                @if ($rejekiSobatPoin > 0)
                                    <!--begin::Item-->
                                    <div class="d-flex align-items-center mt-7 dash_mission_item">
                                        <!--begin::Bullet-->
                                        <span class="bullet bullet-bar bg-danger align-self-stretch"></span>
                                        <!--end::Bullet-->
                                        <!--begin::Checkbox-->
                                        <label
                                            class="checkbox checkbox-lg checkbox-light-secondary checkbox-inline flex-shrink-0 m-0 mx-4">
                                            <input type="checkbox" value="1" disabled
                                                {{ $centangRejekiSobat > 0 ? 'checked' : '' }} />
                                            <span></span>
                                        </label>
                                        <!--end::Checkbox-->
                                        <!--begin::Text-->
                                        <div class="d-flex flex-column flex-grow-1">
                                            <a href="{{ route('rejekiSobat') }}"
                                                class="text-dark-75 text-red font-size-sm font-weight-bolder font-size-lg mb-1">Quiz
                                                Rejeki Sobat</a>
                                            <span class="text-muted font-weight-bold">+
                                                {{ number_format($rejekiSobatPoin, 0, ',', '.') }}
                                                poin</span>

                                        </div>
                                        <!--end::Text-->
                                    </div>
                                    <!--end::Item-->
                                @endif

                                <div class="accordion accordion-light accordion-toggle-arrow mt-5 dash_home_accordion"
                                    id="accordionExample2">
                                    <div class="card">
                                        <div class="card-header" id="headingOne2">
                                            <div class="card-title" data-toggle="collapse" data-target="#collapseOne2">
                                                Baper Poinmu Kurang?
                                            </div>
                                        </div>
                                        <div id="collapseOne2" class="collapse" data-parent="#accordionExample2">
                                            <div class="card-body">
                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center dash_mission_item">
                                                    <!--begin::Bullet-->
                                                    <span class="bullet bullet-bar bg-warning align-self-stretch"></span>
                                                    <!--end::Bullet-->

                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column flex-grow-1 mx-4">
                                                        <a href="{{ route('temanSobat') }}"
                                                            class="text-dark-75 text-yellow font-size-sm font-weight-bolder font-size-lg mb-1">Bawa
                                                            Teman Sobat</a>
                                                        <span class="text-muted font-weight-bold">+ 1.000 poin/teman</span>
                                                    </div>
                                                    <!--end::Text-->
                                                </div>
                                                <!--end::Item-->

                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center dash_mission_item mt-5">
                                                    <!--begin::Bullet-->
                                                    <span class="bullet bullet-bar bg-warning align-self-stretch"></span>
                                                    <!--end::Bullet-->

                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column flex-grow-1 mx-4">
                                                        <a href="{{ route('joinZoom') }}" target="_blank"
                                                            class="text-dark-75 text-yellow font-size-sm font-weight-bolder font-size-lg mb-1">Boost
                                                            dengan Spin the Wheel</a>
                                                        <span class="text-muted font-weight-bold">Klik untuk Join
                                                            Zoom</span>
                                                    </div>
                                                    <!--end::Text-->
                                                </div>
                                                <!--end::Item-->

                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center dash_mission_item mt-5">
                                                    <!--begin::Bullet-->
                                                    <span class="bullet bullet-bar bg-warning align-self-stretch"></span>
                                                    <!--end::Bullet-->

                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column flex-grow-1 mx-4">
                                                        <a href="{{ route('bikinQuiz') }}" target="_blank"
                                                            class="text-dark-75 text-yellow font-size-sm font-weight-bolder font-size-lg mb-1">Bikin
                                                            Soal Tebak Kata</a>
                                                        <span class="text-muted font-weight-bold">+ 1.000 Poin/Soal
                                                            Disetujui</span>
                                                    </div>
                                                    <!--end::Text-->
                                                </div>
                                                <!--end::Item-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        {{-- End Misi Hari Ini --}}
                    </div>

                    <div class="col-xl-4">
                        <div class="card card-custom gutter-b dash_home_card_header">
                            <div class="card-header dash_header_yellow">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Jumlah Teman Sobat
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                {{ $temanSobat }}
                            </div>
                        </div>

                        {{-- Begin Teman Sobatmu --}}
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0">
                                <h3 class="card-title font-weight-bolder text-dark">Teman Sobatmu</h3>
                                <div class="card-toolbar">
                                    <div class="dropdown dropdown-inline">
                                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ki ki-bold-more-hor"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                                            <!--begin::Navigation-->
                                            <ul class="navi navi-hover">
                                                <li class="navi-item">
                                                    <a href="{{ route('temanSobat') }}" class="navi-link">
                                                        <span class="navi-text">
                                                            Lihat Semua
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--end::Navigation-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">

                                @if (count($temanSobats) > 0)
                                    {{-- Jika data Riwayat ada --}}
                                    @foreach ($temanSobats as $tb)
                                        <!--begin::Item-->
                                        <div class="d-flex align-items-center mb-8">
                                            <!--begin::Symbol-->
                                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                                <span class="symbol-label">
                                                    <img src="{{ $tb->avatars != null ? $tb->avatars : 'assets/media/svg/avatars/009-boy-4.svg' }}"
                                                        class="h-75 align-self-end" alt="" />
                                                </span>
                                            </div>
                                            <!--end::Symbol-->
                                            <!--begin::Text-->
                                            <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                                                <a href="#"
                                                    class="text-dark text-hover-primary mb-1 font-size-lg">{{ $tb->name }} {!! badgeUser($tb->id) !!}</a>
                                            </div>
                                            <!--end::Text-->
                                        </div>
                                        <!--end::Item-->
                                    @endforeach
                                @else
                                    {{-- Jika Data Riwayat kosong --}}
                                    <div class="dash_home_empty">
                                        <p>Belum ada teman sobat yang kamu undang. Yuk undang sekarang!</p>
                                        <a href="{{ route('temanSobat') }}">
                                            <button class="btn">Undang Teman Sobat</button>
                                        </a>
                                    </div>
                                @endif
                            </div>
                            <!--end::Body-->
                        </div>
                        {{-- End Teman Sobatmu --}}

                    </div>
                </div>
                <!--end::Row-->
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    {{-- @include('dashboard.components.footer') --}}

    {{-- Modal Detail Riwayat --}}
    <div class="modal fade" id="kt_scrollable_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_baper_poin">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_baper_poin_header">
                        <h5>
                            Riwayat Baper Poinmu
                        </h5>
                        <select name="month" id="selectMonth">
                            <option value="01" {{ date('m') == 1 ? 'selected' : '' }}>January {{ date('Y') }}
                            </option>
                            <option value="02" {{ date('m') == 2 ? 'selected' : '' }}>Februari {{ date('Y') }}
                            </option>
                            <option value="03" {{ date('m') == 3 ? 'selected' : '' }}>Maret {{ date('Y') }}</option>
                            <option value="04" {{ date('m') == 4 ? 'selected' : '' }}>April {{ date('Y') }}</option>
                            <option value="05" {{ date('m') == 5 ? 'selected' : '' }}>Mei {{ date('Y') }}</option>
                            <option value="06" {{ date('m') == 6 ? 'selected' : '' }}>Juni {{ date('Y') }}</option>
                            <option value="07" {{ date('m') == 7 ? 'selected' : '' }}>Juli {{ date('Y') }}</option>
                            <option value="08" {{ date('m') == 8 ? 'selected' : '' }}>Agustus {{ date('Y') }}
                            </option>
                            <option value="09" {{ date('m') == 9 ? 'selected' : '' }}>September {{ date('Y') }}
                            </option>
                            <option value="10" {{ date('m') == 10 ? 'selected' : '' }}>Oktober {{ date('Y') }}
                            </option>
                            <option value="11" {{ date('m') == 11 ? 'selected' : '' }}>November {{ date('Y') }}
                            </option>
                            <option value="12" {{ date('m') == 12 ? 'selected' : '' }}>Desember {{ date('Y') }}
                            </option>
                        </select>
                    </div>
                    <table class="table table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <td>Aktifitas</td>
                                <td>Sumber</td>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll scroll-pull" data-scroll="true" data-height="300">
                        <table class="table table-borderless table-striped">
                            {{-- <thead class="thead-light">
                                <tr>
                                    <td>Aktifitas</td>
                                    <td>Sumber</td>
                                </tr>
                            </thead> --}}
                            <tbody id="fullHistory">

                                @foreach ($fullRiwayats as $fr)
                                    <tr>
                                        @if ($fr->status == 1)
                                            @if ($fr->poin > 0)
                                                <td class="poin"><span
                                                        class="gain">+{{ $fr->poin }}</span>
                                                @else
                                                <td class="poin"><span
                                                        class="loss">{{ $fr->poin }}</span>
                                            @endif
                                        @else
                                            <td class="poin"><span class="loss">0</span>
                                        @endif
                                        <br> @php echo date('d/m', strtotime($fr->created_at)) @endphp</td>
                                        {{-- <td class="text-center" style="vertical-align: middle">{{ $fr->from }}</td> --}}
                                        <td class="text-center" style="vertical-align: middle">
                                            @if ($fr->from == 'tebak-kata')
                                                {{ 'Quiz Tebak Kata' }}
                                            @elseif ($fr->from == 'quiz rejeki sobat')
                                                {{ 'Quiz Rejeki Sobat' }}
                                            @elseif ($fr->from == 'Bawa teman sobat')
                                                {{ 'Bawa Teman Sobat' }}
                                            @elseif ($fr->from == 'Redeem Voucher')
                                                {{ 'Redeem Voucher' }}
                                            @else
                                                {{ $fr->from }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="border: transparent">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="kt_blockui_4_1">Submit</button> --}}
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Alamat --}}
    <div class="modal fade" id="modal_alamat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel">Isi Alamat, KTP dan Verifikasi <br> No. Whatsapp mu untuk mengikuti Auction
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_alamat">

                        <div class="row justify-content-center">
                          <div class="col-lg-3">
                            KTP
                          </div>
                          <div class="col-lg-5">
                            :
                            @if (Auth::user()->ktp_verification == 1)
                              Terverifikasi
                            @elseif (Auth::user()->ktp_verification == 0)
                              Menunggu Verifikasi
                            @else
                              Belum Upload KTP
                            @endif
                            {{-- {{ Auth::user()->ktp_verification == 1 ? 'Terverifikasi' : Auth::user()->ktp_verification == 0 ? 'Menunggu Verifikasi' : 'Belum Upload KTP' }} --}}
                          </div>
                        </div>

                        <div class="row justify-content-center">
                          <div class="col-lg-3">
                            Whatsapp
                          </div>
                          <div class="col-lg-5">
                            :
                            @if (Auth::user()->whatsapp_verification == 1)
                              Terverifikasi
                            @elseif (Auth::user()->whatsapp_verification == 0)
                              Belum Verifikasi
                            @endif
                          </div>
                        </div>

                        <div class="row justify-content-center">
                          <div class="col-lg-3">
                            Alamat
                          </div>
                          <div class="col-lg-5">
                            :
                            @if (Auth::user()->alamat_rumah != null)
                              Sudah ada alamat
                            @elseif (Auth::user()->alamat_rumah == null)
                              Belum ada alamat
                            @endif
                          </div>
                        </div>

                        <div class="text-center mt-5">
                          <a href="{{route('editProfile')}}">
                            <button type="submit" class="btn" style="width: 200px">Menuju Profile</button>
                          </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Intro --}}
    <div class="modal fade" id="modal_intro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body modal_intro">
                    <img src="{{ asset('images/baper-coin.png') }}" alt="">
                    <h6>Selamat Datang di Club Sobat Badak!</h6>
                    <p>Tempat nongki online yang cocok banget untuk kalian para kaum gabut! Dapetin juga kesempatan buat
                        menangin berbagai hadiah total jutaan rupiah!
                    </p>
                    <p class="green">Yuk cek inbox email kamu untuk memverifikasi email.</p>
                    <button class="btn" data-dismiss="modal">OK!</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Banner-->
    <div class="modal" id="modal_banner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true" style="padding-right: 0">
        <div class="modal_banner">
            <div class="banner_wrapper">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i> Close
                </button>
                <div id="modal_banner_carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                        @foreach ($banners as $key=>$banner)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <a href="{{ $banner->link }}" target="_blank">
                                    <img src="{{ 'https://data.sobatbadak.club/' . $banner->photo }}" alt=""
                                        class="img-fluid banner_desktop">
                                    <img src="{{ 'https://data.sobatbadak.club/' . $banner->photo_mobile }}" alt=""
                                        class="img-fluid banner_mobile">
                                </a>
                            </div>
                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href="#modal_banner_carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#modal_banner_carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    {{-- <button class="btn" data-toggle="modal" data-target="#modal_banner">Click</button> --}}
    @include('dashboard.components.zoom-popup')


@endsection



@section('pageJS')

    {{-- <script>
        $("#redeem_btn").click(function(e) {
            $('#modal_redeem').modal('toggle');
            Swal.fire("Selamat!",
                "Kamu mendapatkan 30,000 Baper Poin. Kupon Redeem akan langsung masuk ke akunmu. ", "success");
        });
    </script> --}}

    @if (Session::has('success_regis'))
        <script>
            $('#modal_intro').modal('toggle');
        </script>

        <!-- Event snippet for [CSB] Register Page - Daftar conversion page -->
        <script>
            gtag('event', 'conversion', {
                'send_to': 'AW-695110688/iUJ9CPv2weICEKCYussC'
            });
        </script>
    @endif

    @if (Session::has('login_aug'))
        <script>
            $('#modal_banner').modal('toggle');
        </script>
    @endif

    @if (Session::has('Voucher'))
        @if (Session::get('Voucher') > 0)
            <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
            <script>
                var poin = {{ Session::get('Voucher') }};
                Swal.fire(
                    "Selamat!",
                    `Kamu mendapatkan ${poin} Baper Poin. Kupon Redeem akan langsung masuk ke akunmu. `,
                    "success");
            </script>
        @endif
        @if (Session::get('Voucher') == 0)
            <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
            <script>
                Swal.fire("Gagal!", `Kode voucher tidak valid`, "error");
            </script>
        @endif
    @endif
    <script>
        $('#selectMonth').change(function() {

            // You can access the value of your select field using the .val() method
            // alert('Select field value has changed to' + $('#selectMonth').val());

            // You can perform an ajax request using the .ajax() method
            $.ajax({
                type: 'GET',
                url: '{{ route('ajaxRiwayat') }}', // This is the url that will be requested
                data: {
                    month: $('#selectMonth').val()
                },

                // This is an object of values that will be passed as GET variables and
                // available inside changeStatus.php as $_GET['selectFieldValue'] etc...

                // This is what to do once a successful request has been completed - if
                // you want to do nothing then simply don't include it. But I suggest you
                // add something so that your use knows the db has been updated{}
                success: function(data) {
                    var datas = data.riwayats;
                    if (datas.length == 0) {
                        deleteData();
                    } else {
                        $("#fullHistory").empty();
                        datas.forEach(element => {
                            // console.log(element);
                            var poin = element.poin;
                            var table = document.getElementById("fullHistory");
                            var row = table.insertRow(0);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var d = new Date(element.created_at);

                            if (element.from == "tebak-kata") {
                                var poinFrom = 'Quiz Tebak Kata'
                            } else if (element.from == "quiz rejeki sobat") {
                                var poinFrom = 'Quiz Rejeki Sobat'
                            } else if (element.from == "Bawa teman sobat") {
                                var poinFrom = 'Bawa Teman Sobat'
                            } else if (element.from == "Redeem Voucher") {
                                var poinFrom = 'Redeem Voucher'
                            } else {
                                var poinFrom = element.from
                            }
                            if (element.status == 1) {
                                if (poin > 0) {
                                    cell1.innerHTML = `<td class="poin"><span class="gain">+${poin}</span>
                                <br> ${d.getDate()}/${d.getMonth()}</td>`;
                                } else {
                                    cell1.innerHTML = `<td><div class="poin"><span class="loss">${poin}</span>
                                <br> ${d.getDate()}/${d.getMonth()}</div></td>`;
                                }
                            } else {
                                cell1.innerHTML = `<td><div class="poin"><span class="loss">0</span>
                                <br> ${d.getDate()}/${d.getMonth()}</div></td>`;
                            }
                            cell2.innerHTML =
                                `<td><div class="text-center" style="vertical-align: middle">${poinFrom}</div></td>`;
                        });
                    }
                    // alert('Select field value has changed to' + $('#selectMonth').val());
                },
            });

        });

        function deleteData() {
            $("#fullHistory").empty();
        }
    </script>
    @if (Session::has('verifWhatsapp'))
        <script>
            $('#modal_alamat').modal('toggle');
        </script>
    @endif
@endsection
