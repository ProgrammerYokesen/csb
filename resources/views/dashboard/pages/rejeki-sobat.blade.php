@section('title')
    Quiz Rejeki Sobat
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>

        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row justify-content-center dash_home_navigation">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('tebakKata') }}" id="tebak_kata">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/tebak-kata.svg') }}" alt="csb">
                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Quiz Tebak Kata</div>
                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>


                    <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                        <!--begin::Tiles Widget 12-->
                        <a href="{{ route('rejekiSobat') }}">
                            <div class="card card-custom gutter-b card-stretch">
                                <div class="card-body text-center">
                                    <img src="{{ asset('images/icons/rejeki-sobat.svg') }}" alt="csb">

                                    <div class="text-dark font-weight-bolder font-size-h4">
                                        Quiz Rejeki Sobat</div>

                                </div>
                            </div>
                        </a>
                        <!--end::Tiles Widget 12-->
                    </div>

                </div>
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->

        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b tebak_kata_card">
                    <div class="tebak_kata_card_head">
                        {{-- <div class="card-title"> --}}
                        <a href="{{ route('homeDash') }}" class="btn">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                        {{-- </div> --}}
                        <div class="btn cara_bermain" data-toggle="modal" data-target="#cara-quiz">
                            Cara Bermain
                        </div>
                    </div>
                    <div class="card-body">

                        @if ($quiz == 0)
                            {{-- Jika Tidak ada Quiz --}}
                            <h1 class="mb-5">Sedang tidak ada quiz hari ini...</h1>
                            <a href="{{ route('homeDash') }}" class="mt-3">
                                <button class="btn">Kembali</button>
                            </a>
                        @elseif ($doneQuiz == true)
                            <h1 class="mb-3">Kamu sudah mengikuti QUIZ REJEKI SOBAT hari ini.</h1>
                            <h5 class="mb-5">Yuk kembali lagi besok dan dapatkan Baper Poin sebanyak-banyaknya!</h5>
                            <a href="{{ route('homeDash') }}" class="mt-3">
                                <button class="btn">Kembali</button>
                            </a>
                        @else
                            <h1>Quiz Rejeki Sobat</h1>
                            <h4>Ayo main kuis Rejeki Sobat dari mimin Badak hari ini! <br>
                                Temukan jawabannya melalui link Youtube pada bagian hint <br>
                                dan menangkan hingga ini dan menangkan hingga
                                <span class="custom-text-primary">{{ $rejekiSobatPoin }}
                                    Baper Poin</span>!
                            </h4>
                            <a href="{{ route('quizRejekiSobat') }}">
                                <button class="btn">Mulai</button>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Quiz --}}
    <div class="modal fade" id="cara-quiz" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Cara Bermain Quiz Rejeki Sobat</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Pilih jawaban yang paling tepat dari setiap pertanyaan yang ada
                            </li>
                            <li>
                                Kamu hanya dapat memilih 1 jawaban pada setiap pertanyaan
                            </li>
                            <li>
                                Kamu dapat melihat petunjuk dari link yang tercantum dalam HINT
                            </li>
                            <li>
                                Jika kamu sudah yakin dengan jawabanmu, klik selesai
                            </li>
                            <li>
                                Kumpulkan baper poin dalam sesi ini, selamat bermain!
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
