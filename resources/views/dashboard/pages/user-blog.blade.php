@section('title')
    Edit Profile
@endsection

@section('page_assets')
    <link rel="stylesheet" href="{{ asset('blog/blog.css') }}">
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b px-6" style="padding-bottom: 3rem">
                    <div class="tebak_kata_card_head head-edit-profile justify-content-between d-flex">
                        {{-- <div class="card-title"> --}}
                        <a href="{{ route('homeDash') }}">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                        <div class="d-flex" id="button-in-desktop">
                            <a href="{{ route('postBlog') }}"
                                class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_homepage" style="font-size: 14px">
                                Buat Baru
                            </a>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-bottom: 2rem">
                        <h1 style="margin: 0" class="bold-36-black">Majalah Dinding Teman Sobat</h1>
                    </div>
                    @if ($blogs)
                        <div class="list__blogs row">
                            @foreach ($blogs as $item)
                                @component('blogs.components.blog__card')
                                    @slot('blogID') {{ $item->id }} @endslot
                                    @slot('image') {{ $item->image }} @endslot
                                    @slot('title') {{ $item->title }} @endslot 
                                    @slot('likes') {{ $item->likes }} @endslot
                                    @slot('likeStatus') {{ $item->likeStatus }} @endslot
                                    @slot('date') {{ $item->created_at }} @endslot
                                @endcomponent
                            @endforeach
                        </div>
                    @else
                        <div class="text-center">
                            <img src="/images/baper_kosong.png" alt="">
                            <p class="gray-normal-18">
                                Kamu belum punya artikel nih di Majalah Dinding. <br>
                                Ayo buat artikel dan dapatkan poin hingga <b>50.000!</b>
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection

@section('pageJS')

@endsection
