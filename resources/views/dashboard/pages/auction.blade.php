@section('title')
    Auction
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->

        @if ($poin <= $setting->baper_poin_maksimal )
              <div class="d-flex flex-column-fluid">
                  <!--begin::Container-->
                  <div class="container">
                      <!--begin::Dashboard-->
                      <!--begin::Row-->
                      <div class="row justify-content-center dash_home_navigation">
                          <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                              <!--begin::Tiles Widget 12-->
                              <a href="{{ route('auctionPage') }}">
                                  <div class="card card-custom gutter-b card-stretch">
                                      <div class="card-body text-center">
                                          <img src="{{ asset('images/icons/lelang.svg') }}" alt="csb">
                                          <div class="text-dark font-weight-bolder font-size-h4">
                                              Event Auction</div>
                                      </div>
                                  </div>
                              </a>
                              <!--end::Tiles Widget 12-->
                          </div>


                          <div class="col-xl-2 col-lg-2 col-md-2 col-4 dash_home_menu">
                              <!--begin::Tiles Widget 12-->
                              <a href="{{ route('specialAuction') }}">
                                  <div class="card card-custom gutter-b card-stretch">
                                      <div class="card-body text-center">
                                          <img src="{{ asset('images/icons/lelang.svg') }}" alt="csb">

                                          <div class="text-dark font-weight-bolder font-size-h4">
                                              Mini Auction</div>

                                      </div>
                                  </div>
                              </a>
                              <!--end::Tiles Widget 12-->
                          </div>

                      </div>
                      <!--end::Dashboard-->
                  </div>
                  <!--end::Container-->
              </div>
          @endif

        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">

                <div class="card card-custom gutter-b dash_auction_card">
                    <div class="card-body">
                        <a href="{{ route('homeDash') }}" class="btn back_btn">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>

                        <div class="dash_auction">
                            <h2>BID AUCTION</h2>

                            {{-- If tidak ada lelang --}}
                            @if (!$data)
                                <h4 class="no_auction">Saat ini sedang tidak ada auction. <br>
                                    Silahkan pantau jadwal auction melalui Zoom Club Sobat Badak ya!</h4>
                            @endif
                            @if ($data)
                                {{-- If Ada Lelang Start --}}
                                <div class="auction_countdown_wrapper">
                                    <h2 id="auction_head" style="color: #FF0000">Remaining Time:</h2>

                                    <div class="auction_countdown">
                                        <ul>
                                            {{-- <li><span id="days"></span>:</li> --}}
                                            <li><span id="hours"></span></li>
                                            <li><span id="minutes"></span></li>
                                            <li><span id="seconds"></span></li>
                                        </ul>
                                        <h3 id="auction_done"></h3>
                                        <h2 id="auction_winner" class="mb-5 text-center" style="color: #00CC5D"></h2>
                                    </div>
                                </div>


                                @if (Auth::user()->whatsapp_verification == 0 || Auth::user()->alamat_rumah == null || Auth::user()->ktp_verification !== 1)
                                  <button class="btn" data-toggle="modal" data-target="#modal_alamat"
                                  id="bit_btn">Pasang
                                  Bid</button>
                                @else
                                  <button class="btn" data-toggle="modal" data-target="#bid_modal"
                                  id="bit_btn">Pasang
                                  Bid</button>
                                @endif

                                <div class="row justify-content-center align-items-center">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-6">
                                        <img class="gutter-b" src="https://data.sobatbadak.club/{{ $data->photo }}"
                                            alt="img">
                                    </div>
                                    @foreach ($photos as $photo)
                                        <div class="col-xl-3 col-lg-3 col-md-3 col-6">
                                            <img class="gutter-b"
                                                src="https://data.sobatbadak.club/{{ $photo->auction_photo }}" alt="img">

                                        </div>
                                    @endforeach

                                </div>

                                <h4>{{ $data->name }}</h4>
                                <h6>{{ $data->detaul }}</h6>

                                <h4 class="auction_table_head">Leaderboard</h4>

                                <table class="table table-borderless table-hover">
                                    <thead>
                                        <tr>
                                            <th>Peringkat</th>
                                            <th>Nama</th>
                                            <th>Bid</th>
                                            <th>Waktu Bid</th>
                                        </tr>
                                    </thead>
                                    <tbody id="leaderBoardAuction">
                                        @php $i = 1; @endphp
                                        @foreach ($leaderBoards as $lb)
                                            <tr>
                                                <td
                                                    class="number {{ $i == 1 ? 'first' : ($i == 2 ? 'second' : ($i == 3 ? 'third' : '')) }}">
                                                    {{ $i }}</td>
                                                <td>{{ $lb->name }} {!! badgeUser($lb->user_id) !!}</td>
                                                <td>{{ number_format($lb->bid_price, 0, ',', '.') }}</td>
                                                <td>{{ $lb->created_at }}</td>
                                            </tr>
                                            @php $i++; @endphp
                                        @endforeach

                                    </tbody>
                                </table>

                                <div class="text-center">
                                    <span class="dash_caution">Harap menggunakan nama sesuai dengan KTP. Jika nama yang
                                        digunakan tidak sesuai, barang lelang akan hangus</span>
                                </div>

                                {{-- =================================== If Ada Lelang End =================================== --}}
                            @endif

                        </div>

                    </div>
                </div>

                {{-- <div class="auction_heading">
                    <h4>Barang Auction Mendatang</h4>
                </div> --}}

                <!--begin::Entry-->
                {{-- <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container auction_upcoming">
                        <!--begin::Row-->
                        <div class="row justify-content-center">
                            @forelse($lelangMendatang as $lm)
                                <!--begin::Col-->
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                                    <!--begin::Card-->
                                    <div class="card card-custom gutter-b auction_upcoming_card">
                                        <!--begin::Body-->
                                        <div class="card-body">

                                            <img src="{{ asset($lm->photo) }}" alt="img">

                                            <!--begin::User-->
                                            <div class="auction_upcoming_desc">
                                                <!--begin::Pic-->
                                                <div class="d-flex align-items-center justify-content-center mb-3">
                                                    <!--begin::Title-->
                                                    <div class="d-flex text-center">
                                                        <h5 class="mb-0">{{ $lm->name }}</h5>
                                                    </div>
                                                    <!--end::Title-->
                                                </div>
                                                <!--end::User-->

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Card-->
                                </div>
                                <!--end::Col-->
                            @empty
                                <h4 class="my-10">Belum ada barang auction</h4>
                            @endforelse
                        </div>
                        <!--end::Row-->
                        @if (count($lelangMendatang) > 4)
                            <div class="text-center">
                                <button class="btn">TAMPILKAN SELEBIHNYA</button>
                            </div>
                        @endif
                    </div>
                    <!--end::Container-->
                </div> --}}
                <!--end::Entry-->


                <div class="auction_heading history">
                    <h4>Riwayat Auction</h4>
                    <div class="quick-search">
                        <!--begin:Form-->
                        <div class="quick-search-form">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="svg-icon svg-icon-lg">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path
                                                        d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path
                                                        d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                        fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                </div>
                                <input id="cari" type="text" class="form-control" placeholder="Cari..." />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="quick-search-close ki ki-close icon-sm text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                        <!--begin::Scroll-->
                        <div class="quick-search-wrapper scroll" data-scroll="true" data-height="325"
                            data-mobile-height="200"></div>
                        <!--end::Scroll-->
                    </div>
                </div>

                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid" style="margin-bottom: 50px">
                    <!--begin::Container-->
                    <div class="container auction_upcoming" id="searchLelang">
                        <!--begin::Row-->
                        <div class="row justify-content-center" id="infinite-data">
                            @forelse($riwayatLelang as $rl)
                                <!--begin::Col-->
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                                    <!--begin::Card-->
                                    <div class="card card-custom gutter-b auction_upcoming_card">
                                        <!--begin::Body-->
                                        <div class="card-body">

                                            <!--<img src="{{ asset($rl->photo) }}" alt="img">-->
                                            <img loading=”lazy” src="https://data.sobatbadak.club/{{ $rl->photo }}"
                                                alt="img">

                                            <!--begin::User-->
                                            <div class="auction_upcoming_desc">
                                                <!--begin::Pic-->
                                                <div class="d-flex align-items-center mb-3">
                                                    <!--begin::Title-->
                                                    <div class="d-flex flex-column">
                                                        <h5 class="mb-0">{{ $rl->name }}</h5>
                                                    </div>
                                                    <!--end::Title-->
                                                </div>
                                                <!--end::User-->
                                                <!--begin::Desc-->
                                                <div class="d-flex align-items-center mb-3">
                                                    <div class="mr-10">
                                                        <span>Bid Tertinggi</span>
                                                        <h5>{{ number_format($rl->bidTertinggi, 0, ',', '.') }}</h5>
                                                    </div>
                                                    <div>
                                                        <span>Pemenang</span>
                                                        <h5>{{ $rl->username }}</h5>
                                                    </div>
                                                </div>

                                                <div class="d-flex align-items-center">
                                                    <div class="mr-10">
                                                        <span>Tanggal dan waktu</span>
                                                        <h5>{{ date('d M Y', strtotime($rl->starttime)) }},
                                                            {{ date('H:i', strtotime($rl->starttime)) }}</h5>
                                                    </div>
                                                </div>
                                                <!--end::Desc-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Card-->
                                </div>
                                <!--end::Col-->
                            @empty
                                {{-- Belum ada barang lelang --}}
                                <h4 class="my-10">Belum ada barang auction</h4>
                            @endforelse
                        </div>
                        <div class="ajax-load text-center" style="display:none;align-content: center;">
                            <img src="{{ asset('images/loader.gif') }}">
                        </div>
                        <!--end::Row-->
                        @if (count($riwayatLelang) == 4)
                            <div class="text-center">
                                {{-- Trigger Show More --}}
                                <button class="btn lelang_all" id="showMore">Lihat Selengkapnya</button>
                            </div>
                        @endif
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->


            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    {{-- Modal Bid Auction --}}
    <div class="modal fade" id="bid_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel">Input Bid Auction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="d-flex justify-content-between align-items-center mb-5">
                        <h5 style="color: #E52020;">Bid paling tinggi saat ini</h5>
                        <h5 style="color: #E52020;" id="highestBid">{{ number_format($highestBid, 0, ',', '.') }}</h5>
                    </div>

                    <div class="d-flex justify-content-between align-items-center mb-5">
                        <h5 style="color: #00CC5D;">Baper Poin Kamu</h5>
                        <h5 style="color: #00CC5D;">{{ number_format($poin, 0, ',', '.') }}</h5>
                    </div>
                    <h5>Masukkan penawaranmu disini</h5>
                    <span>Silahkan masukkan penawaranmu dengan kelipatan {{ $data->kelipatan }}</span>



                    <div class="form-group row mt-5 mb-7">
                        <div class="col-lg-12 col-md-12 col-sm-12">

                            <input id="kt_touchspin_1" type="text" class="form-control" value="55" name="price"
                                placeholder="Select time" style="text-align: center" />
                            {{-- <input type="number" step="5000" value="0"> --}}
                        </div>
                    </div>

                    <div class="text-center">
                        <button class="btn bid_btn" id="trigger-tnc" data-toggle="modal" data-target="#tncModal">Pasang
                            Bid</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{-- Modal T&C --}}
    <div class="modal fade" id="tncModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Syarat dan Ketentuan Auction</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div data-scroll="true" data-height="300" class="auction-text-tnc">
                        Berikut adalah syarat dan ketentuan yang perlu diketahui sebelum melakukan auction.
                        <ol class="mt-4">
                            <li>
                                Kegiatan Auction akan dilakukan sewaktu-waktu (tidak terjadwal) di breakout room Zoom WGM
                                Live.
                            </li>
                            <li>
                                Kegiatan penawaran/bidding bukan dilakukan melalui Zoom, tapi melalui website
                                sobatbadak.club.
                            </li>
                            <li>
                                Barang Auction hanya bisa dimenangkan atau di-redeem dengan menukar Baper Poin.
                            </li>
                            <li>
                                Peserta dapat menawar/bid barang Auction dimolai dari 100 Baper Poin berlaku kelipatan.
                            </li>
                            <li>
                                Peserta bisa me-redeem langsung barang Auction dengan harga “Redeem Now” yang tertera di
                                katalog auction.
                            </li>
                            <li>
                                Peserta yang memberikan penawaran tertinggi dan melampaui nilai limit akan disahkan oleh
                                Club Sobat Badak sebagai pemenang Auction. Jika ada penawaran tertinggi yang sama, maka
                                tawaran yang masuk terlebih dahulu oleh sistem akan disahkan oleh pemenang.
                            </li>
                            <li>
                                Jika peserta telah dinyatakan menang oleh Club Sobat Badak, maka Baper Poin pemenang akan
                                dipotong secara otomatis sesuai dengan nominal penawaran yang disepakati.
                            </li>
                            <li>
                                Baper Poin tidak akan terpotong saat peserta menawar/bid tidak menang.
                            </li>
                            <li>
                                Pemenang Auction wajib mengisi data pengiriman di dalam page auction maksimal 1x24 jam untuk
                                konfirmasi data diri.
                            </li>
                            <li>
                                Barang Auction yang berhasil dimenangkan oleh pemenang akan dikirimkan ke alamat pemenang
                                dengan ketentuan ongkos kirim ditanggung oleh Club Sobat Badak.
                            </li>
                            <li>
                                Jika peserta terbukti melakukan kecurangan terhadap sistem yang ada di Club Sobat Badak,
                                panitia berhak untuk membatalkan transaksi auction tersebut.
                            </li>
                            <li>
                                Keputusan panitia Club Sobat Badak mutlak dan tidak bisa diganggu gugat.
                            </li>
                        </ol>
                    </div>
                    <div class="text-center mt-2">
                        <button class="btn back_btn" data-dismiss="modal">Kembali</button>


                        @if (Auth::user()->whatsapp_verification == 0 || Auth::user()->alamat_rumah == null || Auth::user()->ktp_verification !== 1)
                          <button class="btn bid_btn" onclick="notVerified()">Setuju</button>
                        @else
                          <button class="btn bid_btn" id="bid_btn">Setuju</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Alamat --}}
    <div class="modal fade" id="modal_alamat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: transparent">
                    <h5 class="modal-title" id="exampleModalLabel">Isi Alamat, KTP dan Verifikasi <br> No. Whatsapp mu untuk mengikuti Auction
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_alamat">

                        <div class="row justify-content-center">
                          <div class="col-lg-3">
                            KTP
                          </div>
                          <div class="col-lg-5">
                            :
                            @if (Auth::user()->ktp_verification == 1)
                              Terverifikasi
                            @elseif (Auth::user()->ktp_verification == 0)
                              Menunggu Verifikasi
                            @else
                              Belum Upload KTP
                            @endif
                            <!-- {{ Auth::user()->ktp_verification == 1 ? 'Terverifikasi' : Auth::user()->ktp_verification == 0 ? 'Menunggu Verifikasi' : 'Belum Upload KTP' }} -->
                          </div>
                        </div>

                        <div class="row justify-content-center">
                          <div class="col-lg-3">
                            Whatsapp
                          </div>
                          <div class="col-lg-5">
                            :
                            @if (Auth::user()->whatsapp_verification == 1)
                              Terverifikasi
                            @elseif (Auth::user()->whatsapp_verification == 0)
                              Belum Verifikasi
                            @endif
                          </div>
                        </div>

                        <div class="row justify-content-center">
                          <div class="col-lg-3">
                            Alamat
                          </div>
                          <div class="col-lg-5">
                            :
                            @if (Auth::user()->alamat_rumah != null)
                              Sudah ada alamat
                            @elseif (Auth::user()->alamat_rumah == null)
                              Belum ada alamat
                            @endif
                          </div>
                        </div>

                        <div class="text-center mt-5">
                          <a href="{{route('editProfile')}}">
                            <button type="submit" class="btn" style="width: 200px">Menuju Profile</button>
                          </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')

  <script>
    const notVerified = () => {
      $(`#tncModal`).modal('hide')
      $(`#modal_alamat`).modal('toggle')
    }
  </script>

    <script>
        document.getElementById("cari").oninput = function() {
            var data = $(this).val();
            // console.log(data);
            if (data.length >= 2) {
                // Ajax get data
                // console.log('masuk');
                $.ajax({
                    type: 'GET',
                    url: '{{ route('searchAuction') }}', // This is the url that will be requested
                    data: {
                        q: data
                    },
                    success: function(data) {
                        // console.log('masuk sucess');
                        document.getElementById('searchLelang').innerHTML = data.html;
                    }
                });
            } else if (data.length <= 1) {
                $.ajax({
                    type: 'GET',
                    url: '{{ route('searchAuction') }}', // This is the url that will be requested
                    success: function(data) {
                        // console.log('masuk');
                        document.getElementById('searchLelang').innerHTML = data.html;
                    }
                });
            }
        };
    </script>


    @if ($data)
        <script>
            // document.getElementById("cari").onkeydown(function() {
            //     var data = $(this).value();
            //     console.log(data);
            //     if(data.length == 3){
            //         // Ajax get data
            //         $.ajax({
            //         type: 'GET',
            //         url: '{{ route('searchAuction') }}', // This is the url that will be requested
            //         data : {q : data}
            //         success: function(data) {
            //             console.log('masuk');
            //             document.getElementById('searchLelang').innerHTML = data.html;
            //         }
            //     });
            //     }
            // });
            // trigger for open TNC modal and close bid modal
            $(function() {
                $("#trigger-tnc").click(function(e) {
                    $('#bid_modal').modal('toggle');
                })
            })

            var highestPoin = 0;
            var temp = "{{ $highestBid }}";
            var startPoin = {{ $data->start_poin }};
            if (temp !== "") {
                var highestPoin = "{{ $highestBid }}";
            }
            $('#kt_touchspin_1').TouchSpin({
                buttondown_class: 'btn btn-danger text-white',
                buttonup_class: 'btn btn-success text-white',

                min: (startPoin > highestPoin) ? startPoin : highestPoin,
                max: 100000000000000,
                step: {{ $data->kelipatan }},
                // decimals: 2,
                // boostat: 5,
                // maxboostedstep: 10,
            });
        </script>

        <script src="/assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
        <script>
            // $("#bid_btn").click(function(e) {
            //     $('#bid_modal').modal('toggle');
            //     Swal.fire("Bid Berhasil", "", "success");
            // });
        </script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': `{{ csrf_token() }}`
                }
            });

            $("#bid_btn").click(function(e) {

                e.preventDefault();

                var price = $("#kt_touchspin_1").val();

                $.ajax({
                    type: 'POST',
                    url: "{{ route('bid') }}",
                    data: {
                        auction_id: `{{ $data->uuid }}`,
                        price: price
                    },
                    success: function(data) {
                        $('#tncModal').modal('toggle');
                        if (data.Code == 1) {
                            //   Bid berhasil
                            Swal.fire("Kamu Berhasil Pasang Bid", "", "success");
                        } else if (data.Code == 0) {
                            //   Kelipatan Bid salah
                            Swal.fire(
                                "Maaf terjadi kesalahan di nominal kelipatan bid mu !",
                                "Note: Bidmu tidak berkelipatan {{ $data->kelipatan }}", "error");
                        } else if (data.Code == 2) {
                            //   Bid lebih rendah dari bid tertinggi
                            Swal.fire(
                                "Maaf nominal bid kamu terlalu rendah!",
                                "Note: Bid harus lebih besar dari bid sekarang", "error");
                        } else if (data.Code == 3) {
                            Swal.fire("Maaf Baper Poinmu kurang!",
                                "Note: Tambah baper poinmu melalui misi harian!", "error");
                        } else if (data.Code == 4) {
                            Swal.fire("Mohon Maaf, sesi auction telah berakhir!", "", "error");
                        } else if(data.Code == 5){
                            Swal.fire("Mohon Maaf, Kamu sudah pasang bid di Mini Auction dan tidak bisa bid di Event Auction sekaligus. Coba lagi nanti ya!", "",
                                    "error");
                        } else if(data.Code == 400){
                            Swal.fire("Mohon Maaf, Kamu belum verifikasi data diri kamu. Silahkan lengkapi data diri kamu di halaman Profil untuk mengikuti auction", "",
                                    "error");
                        } else {
                            alert('Server Error');
                        }
                    }
                });

            });
        </script>

        <script>
            // Coundown
            const second = 1000,
                minute = second * 60,
                hour = minute * 60,
                day = hour * 24;

            let countDown = new Date(`{{ $data->end_date }}`).getTime(),
                x = setInterval(function() {
                    let userDate = new Date();
                    let noww = convertTZ(userDate, "Asia/Jakarta");
                    let now = noww.getTime();
                    // let now = new Date().getTime();
                    let distance = countDown - now;

                    // console.log(countDown)

                    if (distance > 0) {
                        if (Math.floor((distance % (day)) / (hour)) < 10) {
                            document.getElementById('hours').innerText = "0" + Math.floor((distance % (day)) / (hour)) +
                                ':'
                        } else {
                            document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)) + ':'
                        }

                        if (Math.floor((distance % (hour)) / (minute)) < 10) {
                            document.getElementById('minutes').innerText = "0" + Math.floor((distance % (hour)) / (
                                minute)) + ':'
                        } else {
                            document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)) + ':'
                        }

                        if (Math.floor((distance % (minute)) / second) < 10) {
                            document.getElementById('seconds').innerText = "0" + Math.floor((distance % (minute)) / second)
                        } else {
                            document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second)
                        }
                    } else {
                        document.getElementById('auction_done').innerText = 'Auction Telah Selesai';
                        document.getElementById('auction_head').innerText = '';

                        // Show Nama Pemenang Lelang
                        getWinner();

                        clearInterval(x)
                        $("#bit_btn").addClass("d-none")
                    }

                }, second)

            function deleteData() {
                $("#leaderBoardAuction").empty();
            }

            function getData() {
                $.ajax({
                    type: 'GET',
                    url: '{{ route('ajaxLeaderboard') }}', // This is the url that will be requested
                    data: {
                        id: {{ $data->id }}
                    },

                    // This is an object of values that will be passed as GET variables and
                    // available inside changeStatus.php as $_GET['selectFieldValue'] etc...

                    // This is what to do once a successful request has been completed - if
                    // you want to do nothing then simply don't include it. But I suggest you
                    // add something so that your use knows the db has been updated{}
                    success: function(data) {
                        var datas = data.leaderBoards;
                        var highestBid = data.highestBid.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        if (datas.length != 0) {
                            deleteData();
                            document.getElementById("highestBid").innerHTML = highestBid;
                            var i = 1;
                            datas.forEach(element => {
                                // var poin = element.poin;
                                var table = document.getElementById("leaderBoardAuction");
                                var row = table.insertRow(i - 1);
                                var cell1 = row.insertCell(0);
                                var cell2 = row.insertCell(1);
                                var cell3 = row.insertCell(2);
                                var cell4 = row.insertCell(3);
                                if (i == 1) {
                                    cell1.className = 'first number';
                                } else if (i == 2) {
                                    cell1.className = 'second number';
                                } else if (i == 3) {
                                    cell1.className = 'third number';
                                } else {
                                    cell1.className = 'number';
                                }
                                cell1.innerHTML = i;
                                cell2.innerHTML = `${element.name} ${element.badge}`;
                                // cell2.innerHTML = element.name;
                                cell3.innerHTML = element.bid_price;
                                cell4.innerHTML = element.created_at;
                                i++;
                            });
                        }
                        // alert('Select field value has changed to' + $('#selectMonth').val());
                    },
                });
            }

            function getWinner() {
                $.ajax({
                    type: 'GET',
                    url: '{{ route('lastWinner') }}', // This is the url that will be requested
                    success: function(data) {
                        // console.log(data);
                        var auction = data.auction;
                        // console.log(auction);
                        document.getElementById('auction_winner').innerText =
                            `${auction.name} - ${auction.bid_price} Baper Poin`;
                    }
                });
            }

            function convertTZ(date, tzString) {
                return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));
            }

            setInterval(function() {
                getData();
            }, 10000); //5 detik get data baru
        </script>
    @endif
    @include('dashboard.pages.showmore.showmore')

@endsection
