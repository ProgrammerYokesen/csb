@section('title')
  Sang Pembawa Perubahan Awards
@endsection

@extends('dashboard.master')

@section('page_assets')
    <link rel="stylesheet" href="{{ asset('user-dashboard/sppa.css') }}">
@endsection

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-12 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container white-container ">
                <div class="d-flex justify-content-between align-items-center">
                    <a href="{{route('sppaWelcomePage')}}">
                        <i class="fas fa-arrow-left back-icon"></i>

                    </a>
                    <h2 class="title-part mb-0">Referensi Untukmu</h2>
                    <div class="">

                    </div>
                </div>
                <div class="row justify-content-center mt-7">
                    <div class= "col-md-4 mb-5">
                        <iframe src="https://drive.google.com/file/d/1QR0Fhp9Q46lAw8kVvGEtp0PniVoGXLst/preview" class="modal-video" frameBorder="0"></iframe>
                        <p class="exp-part left" style="font-weight: 600">Kategori: Pahlawan Lingkungan (Menjaga Kebersihan Lingkungan)</p>
                    </div>
                    <div class= "col-md-4 mb-5">
                        <iframe src="https://drive.google.com/file/d/1QJnltlV6IBTF8IhsRFqw69V-lhZG1bNA/preview" class="modal-video" frameBorder="0"></iframe>
                        <p class="exp-part left" style="font-weight: 600">Kategori: Melestarikan Budaya (Melestarikan Tari, Pakaian, atau Lagu Tradisional)</p>
                    </div>
                    <div class= "col-md-4 mb-5">
                        <iframe src="https://drive.google.com/file/d/1smWnPDeywm3audqir37XSJmxm8pKPnpW/preview" class="modal-video" frameBorder="0"></iframe>
                        <p class="exp-part left" style="font-weight: 600">Kategori: Pahlawan tanpa Tanda Jasa (Membantu/Mengajarkan Sesama)</p>
                    </div>
                    <div class= "col-md-4 mb-5">
                        <iframe src="https://drive.google.com/file/d/1pPIFuQlKEqZ6yxhRpN54xvLOHRGonCEF/preview" class="modal-video" frameBorder="0"></iframe>
                        <p class="exp-part left" style="font-weight: 600">Kategori: Sobat Badak Terbugar (Menjaga Kebugaran Tubuh/Olahraga)</p>
                    </div>
                    <div class= "col-md-4 mb-5">
                        <iframe src="https://drive.google.com/file/d/1ab1xOCvSePiXZzEryeFdEAvqAW2PbVgQ/preview" class="modal-video" frameBorder="0"></iframe>
                        <p class="exp-part left" style="font-weight: 600">Kategori: Sobat Badak Terkreatif (Membuat Kerajinan Tangan)</p>
                    </div>
                </div>
                <div class="d-flex justify-content-center mb-9 mt-8">
                  <a href="{{route('sppaFormPage')}}" class="btn btn-yellow">Daftar Sekarang</a>
                </div>



            </div>

        </div>
    </div>



@endsection

@section('pageJS')

@endsection
