@section('title')
  Sang Pembawa Perubahan Awards
@endsection

@extends('dashboard.master')

@section('page_assets')
    <link rel="stylesheet" href="{{ asset('user-dashboard/sppa.css') }}">
@endsection

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-12 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container white-container ">
                <div class="d-flex justify-content-between">
                    <a href="{{route('homeDash')}}">
                        <i class="fas fa-arrow-left back-icon"></i>
                    </a>
                    <div class="d-flex justify-content-end align-items-center">

                        <button class="btn btn-yellow small ml-3" data-toggle="modal"
                            data-target="#modalPeraturan">Peraturan</button>
                        @if($daftar != null)
                            <button class="btn btn-yellow small ml-3" data-toggle="modal" data-target="#modalStatus">Cek
                                Status</button>
                        @endif
                        <a href="{{route('sppaReferensiPage')}}" class="btn btn-yellow small ml-3" >Referensi</a>

                    </div>
                </div>
                <div class="d-flex justify-content-center align-items-center mt-4">
                    <div class="box-title">
                        <div class="d-flex justify-content-center ">
                            <div class="circle-img">
                                <img src="{{ asset('images/sppa-form/Asset 1.svg') }}" alt="">

                            </div>
                        </div>

                        <div class="d-flex justify-content-center mt-5">
                            <h1 class="title-part">Sang Pembawa Perubahan Awards</h1>
                        </div>
                        <div class="d-flex justify-content-center mt-3">
                            <p class="exp-part">Club Sobat Badak mempersembahkan <strong>Sang Pembawa Perubahan Awards!</strong> Mengundang Sobat Badak yang sudah <strong>#bawaperubahan</strong> untuk berpartisipasi dalam Sang Pembawa Perubahan Awards dan mendapatkan predikat sebagai Sang Pembawa Perubahan!</p>
                        </div>
                        <div class="d-flex justify-content-center mt-5">
                        {{--  @if($daftar == null )
                                <a href="{{route('sppaFormPage')}}" class="btn btn-yellow">DAFTAR</a>
                          @elseif($daftar->status == 'rejected')
                                <a href="{{route('sppaFormPage')}}" class="btn btn-yellow">DAFTAR</a>
                          @endif --}}
                               <a href="{{route('sppaList')}}" class="btn btn-yellow">VOTING SEKARANG</a>

                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    {{-- MODAL REFERENSI --}}
    <!-- <div class="modal fade" id="modalReferensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content position-relative">
                {{-- <div class="d-flex justify-content-end">

                    <button class="close btn-close" onclick="closeModal()">
                        <i class="fas fa-times"></i>
                    </button>


                </div> --}}
                <div class="modal-body">
                    <div class="d-flex justify-content-center mb-3">
                        <iframe src="" frameborder="0"
                            class="modal-video"></iframe>
                    </div>

                    <h4 class="title-part">
                        Nih contoh videonya dari kami. Bikin sekreatif mungkin ya, kalo bisa lebih bagus dari video ini!
                    </h4>

                    <div class="d-flex justify-content-center mt-5">

                        <button class="btn btn-yellow" id="closeReferensi">OK!</button>
                    </div>

                </div>
                <div class="position-absolute" style=""></div>
            </div>
        </div>
    </div> -->
    <!-- MODAL Peraturan -->
    <div class="modal fade" id="modalPeraturan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content position-relative">

                <div class="modal-body">

                    <h3 class="title-part " style="margin-bottom: 30px">
                        Peraturan Sang Pembawa Perubahan Awards
                    </h3>
                    <ol >
                        <li class="exp-part">
                            <h6 class="text-left" style="font-weight: 300">Peserta wajib terdaftar di Website Club Sobat Badak.</h6>
                        </li>

                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Peserta hanya boleh mengikuti <strong>“satu”</strong>  kategori saja</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left"  style="font-weight: 300">Buat video berisikan Sobat Badak melakukan kegiatan #bawaperubahan sesuai kategori-kategori yang dipilih sekreatif mungkin dengan durasi maksimal 3 menit dan dikumpulkan melalui TikTok Sobat Badak</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Upload videonya di TikTok kalian dan sertakan #yangasliyangadabadaknya</h6>
                        </li>


                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Kategori #SangPembawaPerubahanAwards:</h6>
                            <ul class="mb-3">
                              <li class="text-left">Pahlawan Lingkungan (Menjaga Kebersihan Lingkungan)</li>
                              <li class="text-left">Sobat Badak Terkreatif (Membuat Kerajinan Tangan)</li>
                              <li class="text-left">Pahlawan tanpa Tanda Jasa (Membantu/Mengajarkan Sesama)</li>
                              <li class="text-left">Melestarikan Budaya (Melestarikan Tari, Pakaian, atau Lagu Tradisional)</li>
                              <li class="text-left">Sobat Badak Terbugar (Menjaga Kebugaran Tubuh/Olahraga)</li>
                            </ul>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Peserta hanya bisa <strong>“satu”</strong> kali upload video</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Peserta yang diketahui berbuat curang, akan secara otomatis tereliminasi</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Peserta yang mendapatkan Anugerah SPPA adalah peserta yang dipilih oleh Tim Club Sobat Badak dan peserta dengan jumlah vote terbanyak</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Urutan video berdasarkan waktu upload</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Video kamu akan dikonfirmasi H+3 dari waktu upload</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Waktu pengumpulan: 16 Desember 2021 - 21 Januari 2022</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Waktu voting: 22 Januari 2022 - 29 Januari 2022</h6>
                        </li>
                        <li class="exp-part">
                            <h6 class="text-left "  style="font-weight: 300">Keputusan panitia Club Sobat Badak tidak dapat diganggu gugat</h6>
                        </li>
                    </ol>
                    <div class="d-flex justify-content-center" style="margin-top: 30px">

                        <button class="btn btn-yellow" id="closePeraturan">OK!</button>
                    </div>

                </div>
                <div class="position-absolute" style=""></div>
            </div>
        </div>
    </div>
    {{-- MODAL STATUS --}}
    <div class="modal fade" id="modalStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content position-relative">
                <div class="d-flex justify-content-end">

                    <button class="close btn-close" onclick="closeModal()">
                        <i class="fas fa-times"></i>
                    </button>


                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center mb-3" >
                        {!!$daftar->embed_url!!}
                    </div>
                    <div class="d-flex justify-content-center">
                        <p class="exp-part left" style="font-weight: 600">Kategori: {{$daftar->category_name}}</p>
                    </div>

                    <h3 class="title-part">
                        Status:
                        @if($daftar != null)
                            @if($daftar->status == 'new')
                                <span class="waiting-stat">Dalam Pengecekan</span>
                            @elseif($daftar->status == 'accepted' )
                                <span class="accepted-stat">Telah Diterima</span>
                            @elseif($daftar->status == 'rejected' )
                                <span class="accepted-stat">Ditolak</span>
                            @endif
                        @endif

                    </h3>
                    @if($daftar != null)
                        @if($daftar->status == 'rejected')
                          <h5 class="title-part">
                              Alasan: {{$daftar->reason}}
                          </h5>
                        @endif
                    @endif

                    <div class="d-flex justify-content-center mt-5">
                        <button class="btn btn-yellow" onclick="closeModal()">Selesai</button>
                    </div>

                </div>
                <div class="position-absolute" style=""></div>
            </div>
        </div>
    </div>

    {{-- MODAL APPROVAL DAFTAR --}}
    <div class="modal fade" id="modalApproval" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="d-flex justify-content-center mb-6">
                        <img src="{{ asset('images/sppa-form/asset_check.svg') }}" alt="" class="check-pic">
                    </div>
                    <h3 class="title-part mb-5">Selamat! Anda berhasil mengupload</h3>
                    <p class="exp-part" style="font-weight: 600">Video atau foto Anda akan diverifikasi oleh tim Club
                        Sobat Badak. Mohon untuk mengecek berkala mengenai status video atau foto Anda</p>
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-yellow" onclick="closeModalApproval()">Selesai</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- <div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="d-flex justify-content-center mb-6">
                        <img src="https://www.clipartmax.com/png/full/89-891075_red-wrong-cross-clip-art-at-red-cross-icon-png.png" alt="" class="check-pic">
                    </div>
                    <h3 class="title-part mb-5">Kamu sudah pernah isi</h3>
                    <p class="exp-part" style="font-weight: 600">Satu akun hanya boleh mengisi satu kali saja</p>
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-yellow" onclick="closeModalError()">Selesai</button>
                    </div>

                </div>

            </div>
        </div>
    </div> -->
@endsection

@section('pageJS')
    {{-- HTML EDITOR --}}
    @if (Session::has('successInput'))
        <script>
            $('#modalApproval').modal('show');
        </script>
    @endif
    @if (Session::has('errorMsg'))
        <script>
            Swal.fire('Form hanya boleh diisi sekali', 'Kamu hanya boleh submit satu video ya sobat' ,'error')
            //$('#modalError').modal('show');
        </script>
    @endif
    <script>
        function closeModal() {
            $('#modalStatus').modal('hide');
        }
        // function closeModalError() {
        //     $('#modalError').modal('hide');
        // }
    </script>
    <script>
        function closeModalApproval() {
            $('#modalApproval').modal('hide');
        }
        $(function() {

            $('#closeReferensi').click(function() {
                $('#modalReferensi').modal('hide');
            });
            $('#closePeraturan').click(function() {
                $('#modalPeraturan').modal('hide');
            });
        });
    </script>
@endsection
