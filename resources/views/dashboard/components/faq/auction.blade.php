@extends('dashboard.pages.home-faq')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container">
            <div class="d-flex justify-content-center text-center">
                <div class="my-6 header-text">FAQ AUCTION</div>
            </div>
            <section class="container mb-8 section-content">
                <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                    id="accordionExample8">
                    <div class="card">
                        <div class="card-header" id="headingOne8">
                            <div class="card-title" data-toggle="collapse" data-target="#collapseOne8">
                                <div class="card-label faq-title">Apa itu Auction?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseOne8" class="collapse show" data-parent="#accordionExample8">
                            <div class="card-body faq-content faq-content">
                                <ul>
                                    <li>
                                        Menanggapi maraknya auction/lelang di Instagram, Club Sobat Badak
                                        membuat Auction yang jujur dan adil, kalian bisa memenangkan
                                        barang impian menggunakan Baper Poin
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo8">
                                <div class="card-label faq-title">Kapan dan dimana Auction akan diadakan?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseTwo8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Kegiatan Auction akan dilakukan sewaktu-waktu (tidak terjadwal) di
                                        breakout room Zoom WGM Live
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree8">
                                <div class="card-label faq-title">Apa saja barang yang akan dilelang di room WGM Live?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseThree8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Setiap harinya akan ada 4 Hadiah yang akan dilelang di room WGM
                                        Live.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour8">
                                <div class="card-label faq-title">Bagaimana cara mengikuti Auction?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseFour8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Pastikan kalian memiliki baper poin
                                    </li>
                                    <li>
                                        Host akan mengumumkan waktu lelang di room WGM Live
                                    </li>
                                    <li>
                                        Sobat Badak dapat melakukan penawaran pada website
                                        sobatbadak.club
                                    </li>
                                    <li>
                                        Ikuti penawaran sesuai dengan peraturan
                                    </li>
                                    <li>
                                        Host akan mengumumkan pemenang lelang
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFive8">
                                <div class="card-label faq-title">Bagaimana cara membeli barang Auction?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseFive8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Kita gak ada sistem redeem tapi “Redeem Now”
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSix8">
                                <div class="card-label faq-title">Apakah ada jumlah bid maksimal dalam sehari?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseSix8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Tidak ada. Kamu bebas melakukan bid selama tidak melebihi jumlah
                                        Baper Poin kamu.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSeven8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSeven8">
                                <div class="card-label faq-title">Berapa jumlah minimal Baper Poin untuk bisa ikut bid?
                                </div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseSeven8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Kamu bisa bid barang Auction mulai dari 100 Baper Poin
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingEight8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseEight8">
                                <div class="card-label faq-title">Apa yang harus saya lakukan jika menang Auction?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseEight8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Pemenang Auction harus mengkonfirmasikan data diri dan menunjukkan foto
                                        KTP/Paspor/SIM yang sesuai dengan data diri yang didaftarkan, ke Whatsapp Official
                                        Club Sobat Badak maksimal 1x24 jam. Jika data diri yang didaftarkan tidak sesuai
                                        dengan yang tertera pada KTP/Paspor/SIM, maka kemenangan tidak valid dan paket tidak
                                        dapat dikirimkan ke pemenang.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingNine">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseNine">
                                <div class="card-label faq-title">Kapan barang Auction yang sudah berhasil dimenangkan
                                    dikirim kepada
                                    pemenang?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseNine" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        H+7 maksimal barang akan diterima
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTen">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTen">
                                <div class="card-label faq-title">Apakah boleh mengikuti Auction lebih dari 1 kali?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseTen" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Sangat diperbolehkan.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
