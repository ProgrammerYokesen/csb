@extends('dashboard.pages.home-faq')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container">
            <div class="d-flex justify-content-center text-center">
                <div class="my-6 header-text">FAQ QUIZ</div>
            </div>
            <section class="container mb-8 section-content">
                <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                    id="accordionExample8">
                    <div class="card">
                        <div class="card-header" id="headingOne8">
                            <div class="card-title" data-toggle="collapse" data-target="#collapseOne8">
                                <div class="card-label faq-title">Apakah setiap hari akan ada Quiz?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseOne8" class="collapse show" data-parent="#accordionExample8">
                            <div class="card-body faq-content faq-content">
                                <ul>
                                    <li>
                                        Ya, benar. Setiap hari akan ada Quiz Tebak Kata dan Quiz Rejeki
                                        Sobat.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo8">
                                <div class="card-label faq-title">Apakah soal quiz akan selalu berubah?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseTwo8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Ya, soal quiz akan berbeda setiap harinya.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree8">
                                <div class="card-label faq-title"> Berapa kali saya bisa mainkan quiz per hari?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseThree8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Quiz Tebak Kata dan Quiz Rejeki Sobat bisa dimainkan masing-masing
                                        1x per hari.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour8">
                                <div class="card-label faq-title">Berapa Baper Poin yang bisa didapatkan dari quiz?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseFour8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        100 Baper Poin untuk setiap jawaban benar Quiz Tebak Kata. 200
                                        Baper Poin untuk setiap jawaban benar Quiz Rejeki Sobat.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFive8">
                                <div class="card-label faq-title">Jika hanya benar satu atau dua pertanyaan, apakah tetap
                                    dapat Baper Poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseFive8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Setiap 1 soal yang benar akan mendapatkan Baper Poin.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card">
                        <div class="card-header" id="headingSix8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSix8">
                                <div class="card-label faq-title">Bagaimana jika panitia menemukan kecurangan dalam perolehan Baper Poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseSix8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Panitia berhak menarik Baper Poin yang terbukti dicurangi oleh peserta tanpa pemberitahuan
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
        </div>
    </div>
@endsection
