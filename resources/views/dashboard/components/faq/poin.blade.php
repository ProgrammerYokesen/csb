@extends('dashboard.pages.home-faq')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container">
            <div class="d-flex justify-content-center text-center">
                <div class="my-6 header-text">FAQ POINT </div>
            </div>
            <section class="container mb-8 section-content">
                <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                    id="accordionExample8">
                    <div class="card">
                        <div class="card-header" id="headingOne8">
                            <div class="card-title" data-toggle="collapse" data-target="#collapseOne8">
                                <div class="card-label faq-title">Apa itu Baper Poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseOne8" class="collapse show" data-parent="#accordionExample8">
                            <div class="card-body faq-content faq-content">
                                <ul>
                                    <li>
                                        Baper poin adalah reward bagi kalian yang join club sobat badak dan
                                        dapat mengikuti Lelang barang impian kamu.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo8">
                                <div class="card-label faq-title"> Bagaimana cara mendapatkan baper poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseTwo8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Mengikuti Zoom
                                    </li>
                                    <li>
                                        Kuis di Zoom
                                    </li>
                                    <li>
                                        Tukar dengan Koin Gatotkaca
                                    </li>
                                    <li>
                                        Permainan di Website
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree8">
                                <div class="card-label faq-title">Bagaimana cara melihat Baper Poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseThree8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Baper poin dapat dilihat di profil pada saat login sobatbadak.club
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour8">
                                <div class="card-label faq-title">Baper Poin bisa digunakan untuk apa saja?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseFour8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Bisa digunakan untuk Auction yang akan diadakan setiap harinya
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFive8">
                                <div class="card-label faq-title">Bagaimana cara menukar Baper Poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseFive8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Baper Poin akan otomatis terpotong saat memenangkan barang
                                        Auction, dan tidak akan terpotong jika kalah dalam Auction.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSix8">
                                <div class="card-label faq-title">Apakah ada jangka waktu kadaluarsa Baper Poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseSix8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Tidak ada, Baper Poin tidak akan hangus
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSeven8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSeven8">
                                <div class="card-label faq-title">Apakah Baper Poin bisa diuangkan?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseSeven8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Baper Poin tidak bisa diuangkan
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingEight8">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseEight8">
                                <div class="card-label faq-title">Bagaimana jika panitia menemukan kecurangan dalam perolehan Baper Poin?</div>
                                <span class="svg-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                        <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div id="collapseEight8" class="collapse" data-parent="#accordionExample8">
                            <div class="card-body faq-content">
                                <ul>
                                    <li>
                                        Panitia berhak menarik Baper Poin yang terbukti dicurangi oleh peserta tanpa pemberitahuan
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
