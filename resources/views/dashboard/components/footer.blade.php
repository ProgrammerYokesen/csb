<!--begin::Footer-->
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2">{{ now()->year }}©</span>
            <a href="{{ route('homeDash') }}" class="text-dark-75 text-hover-primary">Club Sobat
                Badak</a>
        </div>
        <!--end::Copyright-->
        <!--begin::Nav-->
        <div class="nav nav-dark order-1 order-md-2">
            <p style="margin: 0">Disponsori oleh <a href="https://warisangajahmada.com/" target="_blank">Warisan
                    Gajahmada</a></p>
        </div>
        <!--end::Nav-->
    </div>
    <!--end::Container-->
</div>
<!--end::Footer-->
