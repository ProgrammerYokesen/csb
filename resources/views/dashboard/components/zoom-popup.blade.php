<div class="livechat">
    <div class="popup_close" onclick="closeChatFunc()"> <small>close</small> x</div>
    <a href="https://t.me/joinchat/dIVXmX2tEOJlZjZl" target="_blank">
        <div class="logo-chat">
            <img src="{{ asset('images/badak-baper.png') }}" class="img-logo-chat img-chat-sales-bounce" alt="">
            <img src="{{ asset('images/icons/tele-border.svg') }}" class="logo-chat-wab img-chat-logo-bounce" alt="">
        </div>
        <div class="box-chat">
            <p>Sini ngobrol sama Karakter Club Sobat Badak di Telegram CSB! Yuk Join sekarang</p>
        </div>
    </a>
</div>
