<!--begin::Header Mobile-->
<div id="kt_header_mobile" class="header-mobile">
    <!--begin::Logo-->
    <a href="{{ route('homeDash') }}">
        <img alt="Logo" src="{{ asset('images/csb-logo.png') }}" class="logo-default max-h-40px" />
    </a>
    <!--end::Logo-->
    <!--begin::Toolbar-->
    <div class="d-flex align-items-center">
        <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
            <span></span>
        </button>

        <!--begin::User Avatar-->
        <div class="topbar-item ml-3">
            <a class="btn btn-icon btn-hover-transparent-white btn-dropdown btn-lg ml-3" style="background: #fff; "
                href="{{ route('editProfile') }}">
                <img class="h-40px w-40px rounded-sm"
                    src="{{ asset(Auth::user()->avatars == null ? 'images/badak-baper.png' : Auth::user()->avatars) }}"
                    alt="" />
            </a>
            @if(Auth::user()->whatsapp_verification == 1 && Auth::user()->emailValidation == 'validated' && Auth::user()->ktp_verification == 1)
            <span class="ava_badge_verified"><i class="fas fa-check"></i></span>
            @else
            <span class="ava_badge"><i class="fas fa-exclamation"></i></span>
            @endif
        </div>

        <!--end::User Avatar-->
    </div>
    <!--end::Toolbar-->
</div>
<!--end::Header Mobile-->


<div id="kt_header" class="header header-fixed">
    <!--begin::Container-->
    <div class="container d-flex align-items-stretch justify-content-between">
        <!--begin::Left-->
        <div class="d-flex align-items-stretch mr-3">
            <!--begin::Header Logo-->
            <div class="header-logo">
                <a href="{{ route('homeDash') }}">
                    <img alt="Logo" src="{{ asset('images/csb-logo.png') }}" class="logo-default max-h-100px" />
                    <img alt="Logo" src="{{ asset('images/csb-logo.png') }}" class="logo-sticky max-h-100px" />
                </a>
            </div>
            <!--end::Header Logo-->
            <!--begin::Header Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <!--begin::Header Menu-->
                <div id="kt_header_menu"
                    class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
                    <!--begin::Header Nav-->

                    <ul class="menu-nav">

                        <li class="menu-item menu-item-rel">
                            <div class="menu-link topbar-item dash_navbar_btn">
                                <a href="https://wa.me/6281287628068/?text=verify {{ Auth::user()->referral_code }}"
                                    class="btn btn-block font-weight-bold py-3 px-6" target="_blank">Verifikasi Nomor
                                    Whatsapp</a>
                            </div>
                        </li>

                        <li class="menu-item menu-item-rel">
                            <div class="menu-link topbar-item">
                                <a href="{{ route('joinZoom') }}"
                                    class="btn btn-block font-weight-bold py-3 px-6 dash_nav_join" target="_blank">Join
                                    Zoom</a>
                            </div>
                        </li>

                        <li class="menu-item menu-item-rel">
                            <div class="menu-link topbar-item">
                                <a href="https://t.me/joinchat/dIVXmX2tEOJlZjZl"
                                    class="btn btn-block font-weight-bold py-3 px-6 dash_nav_join" target="_blank">Join
                                    Telegram</a>
                            </div>
                        </li>

                        <li class="menu-item menu-item-rel">

                            <div class="menu-link topbar-item">
                                <a href="{{ route('defaultPage') }}"
                                    class="btn btn-block font-weight-bold py-3 px-6 dash_nav_homepage">Homepage</a>
                            </div>

                        </li>

                        <li class="menu-item menu-item-rel">

                            <div class="menu-link topbar-item">
                                <a href="{{ route('faqPoinPage') }}"
                                    class="btn btn-block font-weight-bold py-3 px-6 dash_nav_homepage">FAQ</a>
                            </div>

                        </li>

                        <li class="menu-item menu-item-rel">

                            <div class="menu-link topbar-item">
                                <a href="{{ route('logout') }}"
                                    class="btn btn-block font-weight-bold py-3 px-6 dash_nav_logout" target="_blank">
                                    <i class="fas fa-sign-out-alt mr-2"></i>
                                    Keluar
                                </a>

                            </div>

                        </li>
                    </ul>

                    <!--end::Header Nav-->
                </div>
                <!--end::Header Menu-->
            </div>
            <!--end::Header Menu Wrapper-->
        </div>
        <!--end::Left-->
        <!--begin::Topbar-->
        <div class="topbar dash_nav_desktop">

            <div class="topbar-item">
                <a href="{{ route('faqPoinPage') }}"
                    class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_homepage">FAQ</a>
            </div>

            <div class="topbar-item">
                <a href="{{ route('defaultPage') }}"
                    class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_homepage">Homepage</a>
            </div>

            <div class="topbar-item">
                <a href="https://t.me/joinchat/dIVXmX2tEOJlZjZl" target="_blank"
                    class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_join">Join
                    Telegram</a>
            </div>

            <div class="topbar-item">
                <a href="{{ route('joinZoom') }}" target="_blank"
                    class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_join">Join
                    Zoom</a>
            </div>

            @if (Auth::user()->whatsapp_verification == 0)
                <div class="topbar-item dash_navbar_btn">
                    <a href="https://wa.me/6281287628068/?text=verify {{ Auth::user()->referral_code }}"
                        class="btn font-weight-bold py-3 px-6 mr-2" target="_blank">Verifikasi Nomor Whatsapp</a>
                </div>
            @endif

            <div class="topbar-item">

                <a href="{{ route('logout') }}" class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_logout">
                    <i class="fas fa-sign-out-alt mr-2"></i>
                    Keluar
                </a>

            </div>

            <div class="topbar-item" style="position: relative">
                <a class="btn btn-icon btn-hover-transparent-white btn-dropdown btn-lg ml-3" style="background: #fff; "
                    href="{{ route('editProfile') }}">
                    <img class="h-40px w-40px rounded-sm"
                        src="{{ asset(Auth::user()->avatars == null ? 'images/badak-baper.png' : Auth::user()->avatars) }}"
                        alt="" />
                </a>
                {{-- <div class="btn btn-icon btn-hover-transparent-white btn-dropdown btn-lg ml-3"
                    style="background: #fff; " data-toggle="modal" data-target="#modal_avatar">
                    <img class="h-40px w-40px rounded-sm"
                        src="{{ asset(Auth::user()->avatars == null ? 'images/badak-baper.png' : Auth::user()->avatars) }}"
                        alt="" />
                </div> --}}
                @if(Auth::user()->whatsapp_verification == 1 && Auth::user()->emailValidation == 'validated' && Auth::user()->ktp_verification == 1)
                <span class="ava_badge_verified"><i class="fas fa-check"></i></span>
                @else
                <span class="ava_badge"><i class="fas fa-exclamation"></i></span>
                @endif
            </div>

        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Container-->
</div>


{{-- Modal Avatar --}}
<div class="modal fade" id="modal_avatar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal_baper_poin">
            <div class="modal-header" style="border: transparent">
                <h5 class="modal-title" id="exampleModalLabel">Pilih Avatarmu di Club Sobat Badak</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" method="GET" action="{{ route('avatar') }}">
                    <div class="form-group m-0">
                        <div class="row">

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/badak-baper.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/badak-baper.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/badak-mama.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/badak-mama.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/jamet-kuproy.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/jamet-kuproy.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/bang-jago.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/bang-jago.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/boleng-getol.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/boleng-getol.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/c-adodo.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/c-adodo.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/kucing-baba.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/kucing-baba.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>

                            <div class="col-lg-4 col-6 p-2">
                                <label class="radio-img">
                                    <input type="radio" name="avatar" value="images/bos-tudung.png" />
                                    <div class="ava_img">
                                        <img src="{{ asset('images/bos-tudung.png') }}" alt="" class="img-fluid">
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="text-center ava_btn_sumbit mt-3">
                        <button type="submit">Simpan</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="border: transparent">
            </div>
        </div>
    </div>
</div>
