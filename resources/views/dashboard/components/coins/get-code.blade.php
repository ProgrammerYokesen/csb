@extends('dashboard.pages.home-coin')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header coin-header d-flex justify-content-between item-v-center">
                    {{-- <div class="card-title "> --}}
                    <div>
                        <a href="{{ route('homeDash') }}">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="normal-red-500 notes-coin" style="font-size:16px;margin-bottom: 2rem; margin-top:0">
                        Tulis Kode Penukaran Koin di Paket yang akan Kamu kirim ya!
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="coin-card-outline">
                            <div class="black-normal-18">
                                Kode Penukaran Koin
                            </div>
                            <div class="black-bold-36">
                                {{ Session::get('code') }}
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="address-box">
                            <div class="black-normal-18">
                                Silahkan mengirimkan koin tersebut ke alamat di bawah ini :
                            </div>
                            <div class="black-bold-18">
                                Warisan Gajahmada
                                Ruko Crystal 8 No. 18
                                Alam Sutera,
                                Pakualam, Kec. Serpong Utara,
                                Kota Tangerang Selatan, Banten 15320
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 4rem">
                        <a href="{{ route('doneCoinPage') }}"
                                class="redeem-button btn btn-block font-weight-bold py-3 px-6 dash_nav_homepage"
                                >Mengerti</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')

    <script>
        for (let index = 1; index <= 10; index++) {
            $(`#kt_touchspin_${index}`).TouchSpin({
                buttondown_class: 'btn btn-danger text-white',
                buttonup_class: 'btn btn-success text-white',

                min: 0,
                max: 100000000000000,
                step: 1,
            });
        }
    </script>
@endsection
