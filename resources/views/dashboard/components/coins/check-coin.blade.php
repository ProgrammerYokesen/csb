@extends('dashboard.pages.home-coin')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">

                    <a href="{{ route('coinPage') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>

                </div>
                <div class="d-flex justify-content-center text-center">
                    <div class="redeem coin-header-text">Status Penukaran Koin</div>
                </div>
                <div class="card-body">
                    <div class="coin-form">
                        <div class="teman_sobat_table">
                            <table class="table">
                                <tbody>

                                    @foreach ($getData as $data)
                                        <tr>
                                            <td class="td__center__vertical">{{ $data->kode_penukaran }}</td>
                                            @if ($data->status_baper_poin == 'sudah diproses' && $data->status_koin == 'diterima sesuai')
                                                <td class="td__center__vertical">Baper Poin Telah Dikirim - Sesuai</td>
                                            @elseif ($data->status_baper_poin == 'sudah diproses' && $data->status_koin
                                                == 'diterima tidak sesuai')
                                                <td class="td__center__vertical">Baper Poin Telah Dikirim - Tidak Sesuai</td>
                                            @else
                                                <td class="td__center__vertical">Request Diterima</td>
                                            @endif
                                            <td class="td__center__vertical">
                                                <a href="{{ route('trackCoinPage', ['kode' => $data->kode_penukaran]) }}"
                                                    class="btn font-weight-bold py-3 px-6 mr-2 dash_nav_homepage">Detail</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center teman_sobat_paginate">
                            {{ $getData->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')

    <script>
        for (let index = 1; index <= 10; index++) {
            $(`#kt_touchspin_${index}`).TouchSpin({
                buttondown_class: 'btn btn-danger text-white',
                buttonup_class: 'btn btn-success text-white',

                min: 0,
                max: 100000000000000,
                step: 1,
            });
        }
    </script>
    @if (Session::has('Voucher'))
        @if (Session::get('Voucher') > 0)
            <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
            <script>
                var poin = {{ Session::get('Voucher') }};
                Swal.fire(
                    "Selamat!",
                    `Kamu mendapatkan ${poin} Baper Poin. Kupon Redeem akan langsung masuk ke akunmu. `,
                    "success");
            </script>
        @endif
        @if (Session::get('Voucher') == 0)
            <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
            <script>
                Swal.fire("Gagal!", `Kode voucher tidak valid`, "error");
            </script>
        @endif
    @endif
@endsection
