@extends('dashboard.pages.home-coin')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">
                    <div>
                        <a href="{{ route('homeDash') }}">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                    </div>
                </div>
                <div class="d-flex justify-content-center text-center">
                    <div class="redeem coin-header-text">Isi Data Berikut</div>
                </div>
                <div class="card-body">

                    <form class="form coin-form" action="{{ route('storeTukarKoin') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" required />
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" name="whatsapp" placeholder="No Whatsapp" required />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Alamat Email" required />
                        </div>
                        <div class="form-group mb-1">
                            <textarea class="form-control" id="exampleTextarea" rows="3"
                                placeholder="Alamat Lengkap" name="alamat"></textarea>
                        </div>
                        <div class="d-flex justify-content-center mt-6">
                            <button type="submit"
                                class="redeem-button btn btn-block font-weight-bold py-3 px-6 dash_nav_homepage"
                                target="_blank">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
@endsection


@section('pageJS')

    <script>
        for (let index = 1; index <= 10; index++) {
            $(`#kt_touchspin_${index}`).TouchSpin({
                buttondown_class: 'btn btn-danger text-white',
                buttonup_class: 'btn btn-success text-white',

                min: 0,
                max: 100000000000000,
                step: 1,
            });
        }
    </script>
@endsection
