@extends('dashboard.pages.home-coin')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header coin-header d-flex justify-content-between item-v-center">
                    {{-- <div class="card-title "> --}}
                    <div>
                        <a href="{{ route('homeDash') }}">
                            <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-center">
                        <img src="/images/baper-coin.png" alt="">
                    </div>
                    <div class="d-flex justify-content-center mt-6">
                        <div class="black-bold-18">
                            Terima kasih sudah menukar Koin Gatotkacamu!
                            Kamu bisa mengecek status penukaran koinmu disini
                        </div>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 4rem">
                        <a href="{{ route('homeDash') }}"
                            class="mr-4 redeem-button-outline btn btn-block font-weight-bold py-3 px-6 dash_nav_homepage">Selesai</a>

                        <a href="{{ route('checkCoinPage') }}"
                            class="redeem-button btn font-weight-bold py-3 px-6 dash_nav_homepage">Cek Status</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')

    <script>
        for (let index = 1; index <= 10; index++) {
            $(`#kt_touchspin_${index}`).TouchSpin({
                buttondown_class: 'btn btn-danger text-white',
                buttonup_class: 'btn btn-success text-white',

                min: 0,
                max: 100000000000000,
                step: 1,
            });
        }
    </script>
@endsection
