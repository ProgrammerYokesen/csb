@extends('dashboard.pages.home-coin')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header d-flex justify-content-between">

                    <a href="{{ route('homeDash') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>
                    <a href="{{ route('checkCoinPage') }}" class="btn font-weight-bold py-3 px-6 dash_nav_homepage">
                        Check Status</a>
                </div>
                <div class="d-flex justify-content-center text-center">
                    <div class="redeem coin-header-text">Penukaran Koin Gatotkaca</div>
                </div>

                <div class="card-body">

                    <form id="exchange-coin" class="form" action="{{ route('storeTukarKoin') }}" method="post">
                        @csrf
                        <div class="form-group row justify-content-center">
                            @foreach ($datas as $data)
                                @if ($data->name != 'Ultra Premium')
                                    <div class="col-md-4 col-sm-6 col-12 row coin-column">
                                        @if ($data->koin_name == 'Reguler Hijau' || $data->koin_name == 'GK Helmet' || $data->koin_name == 'Korawa Armor' || $data->koin_name == 'Ultra Premium')
                                            <div style="align-self: center; position: relative"
                                                class="d-flex justify-content-end col-lg-4 col-md-12">
                                                <img src="{{ $data->photo }}" alt="">
                                                <div class="coin__hot blink">HOT</div>
                                            </div>
                                        @else
                                            <div style="align-self: center;"
                                                class="d-flex justify-content-end col-lg-4 col-md-12">
                                                <img src="{{ $data->photo }}" alt="">
                                            </div>
                                        @endif
                                        <div class="col-lg-6 col-md-12">
                                            <div class="coin-name-text" id="{{ $data->id }}"
                                                data-name="{{ $data->koin_name }}">
                                                {{ $data->koin_name }}<br>{{ number_format($data->poin) }} BP
                                            </div>
                                            <input id="kt_touchspin_{{ $data->id }}" type="text" class="form-control"
                                                value="0" name="{{ $data->id }}" placeholder="Select time" readonly />
                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-4 col-sm-6 col-12 row coin-column will-hide">
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-12 row coin-column">
                                        <div style="align-self: center;" class="ex-coin d-flex col-lg-4 col-md-12">
                                            <img src="/images/main-gatot.png" alt="">
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="coin-name-text" id="{{ $data->id }}"
                                                data-name="{{ $data->koin_name }}">
                                                Reguler Hijau 1.000.000 BP
                                            </div>
                                            <input id="kt_touchspin_{{ $data->id }}" type="text" class="form-control"
                                                value="0" name="{{ $data->id }}" placeholder="Select time" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-12 row coin-column will-hide">
                                    </div>
                                @endif

                            @endforeach
                        </div>
                        <div class="normal-red-500 notes-coin">
                            Notes: Pengajuan Penukaran Koin hanya dapat dilakukan sehari sekali
                        </div>
                    </form>
                    <div class="d-flex justify-content-center">
                        <button data-toggle="modal" @if (Auth::user()->emailValidation == 'new' || Auth::user()->whatsapp_verification == 0) data-target="#verifModal" @else data-target="#confirmModal" @endif
                            class="redeem-button btn btn-block font-weight-bold py-3 px-6 dash_nav_homepage"
                            id="submit-form">Redeem</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="modal-coin-header modal__mt_1">Apakah Kamu sudah yakin?</div>
                    <div class="modal-coin-content">Koin yang mau kamu tukarkan:</div>
                    <ol class="modal-coin-content text-left" id="list-koin">
                    </ol>
                    <div class="normal-red-500 modal__mt_1">
                        Kamu hanya bisa menukar koin sekali dalam sehari. Jadi perhatikan baik-baik ya!
                    </div>
                    <div class="d-flex justify-content-center">
                        <button data-dismiss="modal" class="btn custom-white-btn py-2 px-6 mr-2" style="font-size: 14px;">
                            Batalkan
                        </button>
                        <button onclick="submitForm()" class="btn font-weight-bold py-3 px-6 dash_nav_homepage"
                            id='lanjut-submit'>Lanjutkan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="verifModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="modal-coin-header modal__mt_1">HARAP KONFIRMASI DATA ANDA TERLEBIH DAHULU</div>
                    @if (Auth::user()->emailValidation == 'new' || Auth::user()->whatsapp_verification == 0 || Auth::user()->alamat_rumah == '')
                        <div class="modal-coin-content">
                            Verifikasi dengan cara melengkapi data anda melalui <br>
                            <a href="{{ route('editProfile') }}">
                                <strong style="color:#FFAA3A">
                                    Menu Edit Profil
                                </strong>
                            </a>
                        </div>
                    @endif
                    {{-- <div class="d-flex justify-content-center mt-5">
                        @if (Auth::user()->whatsapp_verification == 0)
                            <a type='button'
                                href="https://wa.me/6281287628068/?text=verify {{ Auth::user()->referral_code }}"
                                target="_blank" class="btn font-weight-bold custom-success-btn py-2 px-4 mr-2">Whatsapp</a>
                        @elseif (Auth::user()->whatsapp_verification == 1)
                            <button class="btn font-weight-bold custom-success-btn py-2 px-4 mr-2">Terverifikasi</button>
                        @endif
                        @if (Auth::user()->emailValidation == 'new')
                            <a type="button" href="{{ route('sendVerifikasiEmail') }}"
                                class="btn font-weight-bold custom-success-btn py-2 px-4 mr-2">Email</a>
                        @endif
                    </div> --}}
                    <div class="d-flex justify-content-center" style="font-size: 14px;margin-top:2rem">
                        <button data-dismiss="modal" class="btn custom-white-btn py-2 px-6 mr-2">
                            Batalkan
                        </button>
                        <a href="{{ route('editProfile') }}"
                            class="btn font-weight-bold py-2 px-6 mr-2 dash_nav_homepage">Meluncur</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')
    <script>
        function submitForm() {
            $("#exchange-coin").submit();
        }

        for (let index = 1; index <= 10; index++) {
            $(`#kt_touchspin_${index}`).TouchSpin({
                buttondown_class: 'btn btn-danger text-white',
                buttonup_class: 'btn btn-success text-white',

                min: 0,
                max: 100000000000000,
                step: 1,
            });
        }
        $(function() {
            @if (Session::has('error'))
                Swal.fire("Gagal!", "Notes: Pengajuan Penukaran Koin hanya dapat dilakukan sehari sekali!", "error")
            @endif
        })

        $(document).ready(function() {
            $('#submit-form').on('click', function() {
                $('#list-koin').empty();
                var kosong = true;
                for (let i = 1; i < 11; i++) {
                    var value = parseInt($('input[name=' + i + ']').val());
                    var name = $(`#${i}`).attr('data-name');
                    // console.log(value, name)
                    if (value > 0) {
                        $('#list-koin').append(`
                             <div class="row" style="list-style: none;">
                                 <div class="col-5" style="text-align: left">${name}</div>
                                 <div class="col-1" style="text-align: center">:</div>
                             <div class="col-5" style="text-align: left">${value}</div>
                             </div>
                        `)

                        kosong = false;
                    }
                }

                if (kosong) {
                    $('#lanjut-submit').prop('disabled', true)
                } else {
                    $('#lanjut-submit').prop('disabled', false)
                }
            })
        })
    </script>

@endsection
