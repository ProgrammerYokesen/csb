@extends('dashboard.pages.home-coin')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">

                    <a href="{{ route('homeDash') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>

                </div>
                <div class="d-flex justify-content-center text-center">
                    <div class="redeem coin-header-text">Silahkan masukkan kode untuk redeem kupon </div>
                </div>
                <div class="card-body">
                    <form class="form coin-form" id="redeem-form" action="{{ route('redeemVoucher') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control redeem_input" name="voucher"
                                placeholder="Contoh: F00315911201" />
                        </div>
                        <div class="d-flex justify-content-center mt-6">
                            <button type="submit" class="redeem-button btn btn-block dash_nav_homepage py-4">Redeem</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--begin::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
    <script>
        // $(function() {

        //     var $form = $("#redeem-form");
        //     var $input = $form.find("input");

        //     $input.on("keyup", function(event) {

        //         var $this = $(this);
        //         var input = $this.val();
        //         $this.val(function() {
        //             return input.toUpperCase();
        //         });

        //     });
        // });
    </script>
@endsection


@section('pageJS')

    <script>
        for (let index = 1; index <= 10; index++) {
            $(`#kt_touchspin_${index}`).TouchSpin({
                buttondown_class: 'btn btn-danger text-white',
                buttonup_class: 'btn btn-success text-white',

                min: 0,
                max: 100000000000000,
                step: 1,
            });
        }
    </script>
    @if (Session::has('Voucher'))
        @if (Session::get('Voucher') > 0)
            <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
            <script>
                var poin = {{ Session::get('Voucher') }};
                Swal.fire(
                    "Selamat!",
                    `Kamu mendapatkan ${poin} Baper Poin. Kupon Redeem akan langsung masuk ke akunmu. `,
                    "success");
            </script>
        @endif
        @if (Session::get('Voucher') == 0)
            <script src="assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
            <script>
                Swal.fire("Gagal!", `Kode voucher tidak valid`, "error");
            </script>
        @endif
    @endif
@endsection
