@extends('dashboard.pages.home-coin')
@section('inner-contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <div class="container coin">
            <div class="card card-custom gutter-b coin-card">
                <div class="card-header">
                    <a href="{{ route('checkCoinPage') }}">
                        <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                    </a>

                </div>
                <div class="d-flex justify-content-center text-center">
                    <div class="redeem coin-header-text">Silahkan Masukkan Kode Tukar Koinmu </div>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-center">
                        <div class="coin-card-outline">
                            <div class="black-normal-18">
                                Kode Penukaran Koin
                            </div>
                            <div class="black-bold-36">
                                {{$data->kode_penukaran}}
                            </div>
                        </div>
                    </div>

                    {{-- UNTUK ITEM
                    - active : /images/icons/circle-check.svg
                    - not active : /images/icons/circle-deactive.svg 
                    
                    UNTUK LINE
                    - active : class -> line-timeline active
                    - not active : class -> line-timeline --}}
                    <div class="d-flex justify-content-center timeline-coin" style="align-items: center">
                        @if ($data->status_baper_poin == 'sudah diproses')
                            <div class="item-timeline" id="item-req">
                                <img src="/images/icons/circle-check.svg" alt="">
                            </div>
                            <div class="line-timeline active" id="line-1">
                            </div>
                            
                            <div class="item-timeline status-koin-image" id="item-rcv">
                                <img src="/images/icons/circle-check.svg" alt="">
                            </div>
                            <div class="line-timeline active" id="line-2">
                            </div>
                            
                            <div class="item-timeline" id="item-rcv">
                                <img src="/images/icons/circle-check.svg" alt="">
                            </div>
                        @elseif ($data->status_redeem == 'koin diterima')
                            <div class="item-timeline" id="item-req">
                                <img src="/images/icons/circle-check.svg" alt="">
                            </div>
                            <div class="line-timeline" id="line-1">
                            </div>
                            
                            <div class="item-timeline" id="item-rcv">
                                <img src="/images/icons/circle-deactive.svg" alt="">
                            </div>
                            <div class="line-timeline" id="line-2">
                            </div>
                            
                            <div class="item-timeline" id="item-rcv">
                                <img src="/images/icons/circle-deactive.svg" alt="">
                            </div>
                        @else
                            <div class="item-timeline" id="item-req">
                                <img src="/images/icons/circle-check.svg" alt="">
                            </div>
                            <div class="line-timeline" id="line-1">
                            </div>
                            
                            <div class="item-timeline" id="item-rcv">
                                <img src="/images/icons/circle-deactive.svg" alt="">
                            </div>
                            <div class="line-timeline" id="line-2">
                            </div>
                            
                            <div class="item-timeline" id="item-rcv">
                                <img src="/images/icons/circle-deactive.svg" alt="">
                            </div>
                        @endif
                        
                        <!--@if ($data->status_koin == 'belum diterima')-->
                        <!--    <div class="item-timeline" id="item-rcv">-->
                        <!--        <img src="/images/icons/circle-deactive.svg" alt="">-->
                        <!--    </div>-->
                        <!--    <div class="line-timeline" id="line-2">-->
                        <!--    </div>-->
                        <!--@else-->
                        <!--    <div class="item-timeline status-koin-image" id="item-rcv">-->
                        <!--        <img src="/images/icons/circle-check.svg" alt="">-->
                        <!--    </div>-->
                        <!--    <div class="line-timeline active" id="line-2">-->
                        <!--    </div>-->
                        <!--@endif-->
                        <!--@if ($data->status_baper_poin == 'belum diberikan')-->
                        <!--    <div class="item-timeline" id="item-rcv">-->
                        <!--        <img src="/images/icons/circle-deactive.svg" alt="">-->
                        <!--    </div>-->
                        <!--@else-->
                        <!--    <div class="item-timeline" id="item-rcv">-->
                        <!--        <img src="/images/icons/circle-check.svg" alt="">-->
                        <!--    </div>-->
                        <!--@endif-->
                    </div>

                    {{-- UNTUK TEXT, ganti class
                    - active : black-normal-18
                    - not active : gray-normal-18 --}}
                    <div class="d-flex justify-content-center mt-2">
                         @if ($data->status_redeem == 'menunggu penerimaan koin')
                            <div id="text-tl-req" class="text-timeline black-normal-18">
                                Request diterima
                            </div>
                        @endif
                        @if ($data->status_koin == 'belum diterima')
                        <div id="text-tl-rcv" class="text-timeline gray-normal-18 tl-center">
                            Koin Diterima
                        </div>
                        @else
                        <div id="text-tl-rcv" class="text-timeline black-normal-18 tl-center">
                            Koin Diterima
                        </div>
                        @endif
                        @if ($data->status_baper_poin == 'belum diberikan')
                            <div id="text-tl-send" class="text-timeline gray-normal-18">
                                Baper Poin Dikirim
                            </div>
                        @else
                            <div id="text-tl-send" class="text-timeline black-normal-18">
                                Baper Poin Dikirim
                            </div>
                        @endif
                    </div>

                    {{-- WORDING 
                        - request : Request kamu telah kami terima. <br>
                                Silahkan mengirimkan koinmu ke alamat yang tertera.
                        - diterima : Koin yang kamu kirim telah kami terima dan sesuai. <br> 
                                Admin akan segera mengirimkan Baper Poin senilai xx.xxx ke akun Club Sobat Badakmu
                        - dikirim : Selamat! Baper Poin telah dikirimkan ke dalam akun <br> 
                                Club Sobat Badakmu. Yuk terus kumpulin Baper Poinnya lagi! --}}
                    <div class="d-flex justify-content-center">
                        <div class="address-box">
                            <div id="request" class="gray-normal-18">
                            
                            @if ( $data->status_baper_poin == 'sudah diproses')
                                Selamat! Baper Poin telah dikirimkan ke dalam akun 
                                Club Sobat Badakmu senilai {{$baperPoin->poin}}. Yuk terus kumpulin Baper Poinnya lagi!
                            @elseif ($data->status_koin == 'diterima sesuai')
                                Koin yang kamu kirim telah kami terima dan sesuai. <br> 
                                Admin akan segera mengirimkan Baper Poin senilai {{$baperPoin->poin}} ke akun Club Sobat Badakmu
                            @elseif ($data->status_redeem == 'menunggu penerimaan koin')
                            Request kamu telah kami terima. <br>
                                Silahkan mengirimkan koinmu ke alamat yang tertera.
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')
    <script>
        $(document).ready(function(){
            $('.status-koin-image').on('click', function(){
                var kode = '<?php echo $data->kode_penukaran; ?>'
                console.log(kode)
                $.ajax({
                    url:'/check-status-koin/'+kode,
                    type:"GET"
                })
                .done(function(data){
                    console.log(data);
                    var poin = data[1].poin
                    if(data[0].status_koin == 'diterima sesuai'){
                        $('#request').html("Koin yang kamu kirim telah kami terima dan sesuai. <br/> Admin akan segera mengirimkan Baper Poin senilai "+poin+" ke akun Club Sobat Badakmu");
                    }else if(data[0].status_koin == 'diterima tidak sesuai'){
                        $('#request').html("Koin yang kamu kirim telah kami terima namun tidak sesuai. <br/> Admin akan mengirimkan Baper Poin senilai "+poin+" sesuai koin yang kamu kirimkan.");
                    }
                })
            })
        })
    </script>       

@endsection


