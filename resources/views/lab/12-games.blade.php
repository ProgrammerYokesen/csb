@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <div class="text-right">
                              <!-- <a href="{{route('riwayatQuiz')}}" class="mb-2">
                                <button class="btn">
                                  Riwayat Partisipasi
                                </button>
                              </a>
                              <button class="btn" data-toggle="modal" data-target="#rule-1212">
                                Cara Bermain
                              </button> -->
                            </div>
                        </div>
                    </div>
                    <div class="card-body quiz__show">

                      @if ($winner == true)
                        <div class="text-center">
                          <h1 style="margin-bottom: 25px">Quiz Show</h1>
                          <img style="margin: 0 auto" src="{{asset('images/badak-baper.png')}}" alt="">
                          <h2 style="margin-top: 25px; font-weight: 700">Terima kasih sudah berpartisipasi dan menang di Event Spesial Harbolnas 12.12</h2>

                          <!-- <a href="https://us02web.zoom.us/j/87489995180?pwd=eCtVSGt0cCthUEZlQjk4eEVUZW80UT09" target="_blank"> -->
                          <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                            <button class="btn" style="background: #ffaa3a; color: #ffffff">Join Zoom</button>
                          </a>
                        </div>
                      @else
                        <h1>Quiz Show</h1>
                        <!-- <h6 class="text-center">{{ date("d M Y") }}</h6> -->

                        <!-- <div class="text-center">
                          <a href="{{route('listPemenang')}}" class="mr-1">
                            <button class="btn" style="background: #ffaa3a; color: #ffffff">List Pemenang</button>
                          </a>
                          <a href="{{route('listBelumMain')}}" class="ml-1">
                            <button class="btn" style="background: #ffaa3a; color: #ffffff">Peserta Belum Menang</button>
                          </a>
                        </div> -->

                        <!-- <p>Halo Teman Sobat, ini saatnya kamu untuk memiliki kesempatan untuk memenangkan banyak hadiah dengan memainkan game yang ada di zoom Club Sobat Badak. Caranya gimana? tinggal daftar ikutan Event Spesial Harbolnas 12.12 dan klik daftar di game dibawah ini ya! <br> <span style="font-weight: 600; color: #ffaa3a">Kamu bisa daftar 30 menit sebelum acaranya dimulai</span>, jadi pasang alarmnya dan ikutan Quiz Shownya! Semoga beruntung!</p> -->

                        <div class="row justify-content-center align-items-center" style="margin-top: 25px;margin-bottom: 25px">

                          @php
                            $now = date("Y-m-d H:i:s");
                          @endphp

                          @foreach ($games as $key => $game)
                          <div class="col-lg-4 col-sm-6 quiz__show_game">

                            @if (strtotime($now) <=  strtotime($game->start_date))
                              <div class="quiz__show_countdown">

                                <div class="clockdiv d-flex" data-date="{{ $game->start_date }}">
                                  <div class="d-flex">
                                    <span class="hours">00</span>
                                    <div class="smalltext">:</div>
                                  </div>
                                  <div class="d-flex">
                                    <span class="minutes">00</span>
                                    <div class="smalltext">:</div>
                                  </div>
                                  <div class="d-flex">
                                    <span class="seconds">00</span>
                                    <div class="smalltext"></div>
                                  </div>
                                </div>

                              </div>
                            @elseif (strtotime($now) >=  strtotime($game->start_date) && strtotime($now) <= strtotime($game->end_date))
                              <div class="quiz__show_countdown" id="quizShowStatus">
                                <div
                                    class="text-dark mt-1 d-flex justify-content-center align-items-center" style="font-size: 14px">
                                    <div class="live-icon blink"></div> Game Sedang Berlangsung!
                                </div>
                              </div>
                            @else
                              <div class="quiz__show_countdown" id="quizShowStatus">
                                <div
                                    class="text-dark mt-1 d-flex justify-content-center align-items-center" style="font-size: 14px">
                                    Pendaftaran ditutup!
                                </div>
                              </div>
                            @endif


                              <div class="quiz__show_countdown d-none" id="quizShowStatus">
                                <div
                                    class="text-dark mt-1 d-flex justify-content-center align-items-center" style="font-size: 14px">
                                    <div class="live-icon blink"></div> Game Sedang Berlangsung!
                                </div>
                              </div>

                              <!-- @if (strtotime($now) >=  strtotime($game->start_date) && strtotime($now) <= strtotime($game->end_date))

                              @endif -->

                              <img src="{{ $game->image_drive }}" alt="">
                              <div class="quiz__show_regis_btn">
                                @if (strtotime($now) >=  strtotime($game->start_register) && strtotime($now) <= strtotime($game->end_register))

                                            @if ($register == false)
                                            <!-- <a href="https://us02web.zoom.us/j/87489995180?pwd=eCtVSGt0cCthUEZlQjk4eEVUZW80UT09" target="_blank"> -->
                                            <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                                              <button class="btn prm">Nonton</button>
                                            </a>
                                            @elseif ($paid == false)
                                              <!-- <button class="btn prm" data-toggle="modal" data-target="#not_paid">Daftar</button> -->
                                              <!-- <a href="https://us02web.zoom.us/j/87489995180?pwd=eCtVSGt0cCthUEZlQjk4eEVUZW80UT09" target="_blank"> -->
                                              <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                                                <button class="btn prm">Nonton</button>
                                              </a>
                                            @else
                                              <a href="{{route('joinGame', [ 'eventId' => $game->event_id ])}}">
                                                <button class="btn prm" id="daftarBtn{{ $key }}">Daftar</button>
                                              </a>
                                            @endif
                                @elseif (strtotime($now) >=  strtotime($game->start_date) && strtotime($now) <= strtotime($game->end_date))
                                  <!-- <a href="https://us02web.zoom.us/j/87489995180?pwd=eCtVSGt0cCthUEZlQjk4eEVUZW80UT09" target="_blank"> -->
                                  <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                                    <button class="btn prm">Nonton</button>
                                  </a>
                                @endif
                                <a href="{{route('gameDetail', $game->event_id )}}">
                                  <button class="sec">Cara Bermain & List Peserta</button>
                                </a>
                              </div>
                          </div>

                          @endforeach

                        </div>
                      @endif


                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="rule-1212" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Yang dapat mengikuti games ini adalah Sobat Badak yang telah menyelesaikan transaksi Harbolnas 12.12
                            </li>
                            <li>
                                Sobat Badak yang bermain adalah mereka yang belum bermain di Quiz Show Harbolnas 12.12
                            </li>
                            <li>
                                Sobat badak yang bermain akan dipilih melalui Spin the Wheel dan akan bermain pada saat itu juga
                            </li>
                            <li>
                                Untuk mencegah terhambatnya proses pemilihan nama peserta, Sobat Badak wajib mendaftar setiap kali ingin mengikuti games melalui website sobatbadak.club di menu "Quiz Show"
                            </li>
                            <li>
                                Games berlangsung di Zoom Live Club Sobat Badak dari tanggal 7 Januari - 31 Januari 2022 pukul 15.00 - 21.00 WIB
                            </li>
                            <li>
                                Peserta wajib menggunakan nama yang didaftarkan pada Harbolnas 12.12 di Website Club Sobat Badak saat berada di ZOOM
                            </li>
                            <li>
                                Host akan memanggil nama Sobat Badak yang terpilih. Jika Sobat Badak tidak berada di Zoom saat dipanggil, maka Tim CSB akan langsung melakukan Spin the Wheel kembali.
                            </li>
                            <li>
                                Jika Sobat Badak yang terpilih berada di Zoom, tetapi belum menghidupkan audio ataupun camera, Tim CSB akan memberikan kesempatan sebanyak 3 kali panggilan. Lewat dari itu, Tim CSB akan langsung melakukan Spin the Wheel kembali.
                            </li>
                            <li>
                                Jika Sobat Badak tidak terpanggil di Quiz Show yang dipilih pertama kali, Sobat Badak dapat melakukan pendaftaran kembali di sobatbadak.club untuk games selanjutnya
                            </li>
                            <li>
                                Sobat Badak hanya mendapatkan satu kali kesempatan untuk menang di Quiz Show
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="not_register" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Kamu belum daftar Event 12.12</h5>
                  <p>Klik disini untuk daftar Event 12.12</p>
                  <a href="{{route('harbolnas1212')}}">
                    <button class="btn">Menuju Halaman 12.12</button>
                  </a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="not_paid" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Mohon konfirmasi pembayaran Mystery Box kamu ke admin ya</h5>
                  <p>Sebelum mendaftar silahkan selesaikan pembayaran dengan admin kami di Whatsapp.</p>
                  <p>Order ID kamu: <strong>{{$users->orderId}}</strong></p>

                  <button class="btn" data-dismiss="modal">Tutup</button>
                  <button class="btn" onclick="proceedWA({{$users->whatsapp}},'{{$users->orderId}}')">Chat Admin</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="regis_success" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Selamat! kamu sudah terdaftar di Quiz Show ini.</h5>
                  <p>Silahkan langsung masuk zoom Club Sobat Badak ya! Semoga Beruntung</p>

                  <!-- <a href="https://us02web.zoom.us/j/87489995180?pwd=eCtVSGt0cCthUEZlQjk4eEVUZW80UT09" target="_blank"> -->
                  <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                    <button class="btn">OK</button>
                  </a>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')

@if (Session::has('success_regist'))
    <script>
        $(`#regis_success`).modal('toggle')
    </script>
@endif


<script>
  document.addEventListener('readystatechange', event => {
    if (event.target.readyState === "complete") {
        var clockdiv = document.getElementsByClassName("clockdiv");
        var countDownDate = new Array();
        for (var i = 0; i < clockdiv.length; i++) {
            countDownDate[i] = new Array();
            countDownDate[i]['el'] = clockdiv[i];
            countDownDate[i]['time'] = new Date(clockdiv[i].getAttribute('data-date')).getTime();
            // countDownDate[i]['days'] = 0;
            countDownDate[i]['hours'] = 0;
            countDownDate[i]['seconds'] = 0;
            countDownDate[i]['minutes'] = 0;
        }

        var countdownfunction = setInterval(function() {
          for (var i = 0; i < countDownDate.length; i++) {
                var now = new Date().getTime();
                var distance = countDownDate[i]['time'] - now;
                 // countDownDate[i]['days'] = Math.floor(distance / (1000 * 60 * 60 * 24));
                 countDownDate[i]['hours'] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                 countDownDate[i]['minutes'] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                 countDownDate[i]['seconds'] = Math.floor((distance % (1000 * 60)) / 1000);

                 if (distance < 0) {
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = 0;
                  $(`#daftarBtn${i}`).prop('disabled', true);
                }else{
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = countDownDate[i]['days'];
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = (countDownDate[i]['hours'] < 10 ? '0' + countDownDate[i]['hours'] : countDownDate[i]['hours'] ) ;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = (countDownDate[i]['minutes'] < 10 ? '0' + countDownDate[i]['minutes'] : countDownDate[i]['minutes'] );
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = (countDownDate[i]['seconds'] < 10 ? '0' + countDownDate[i]['seconds'] : countDownDate[i]['seconds'] );
                }
           }
        }, 1000);
    }
  });
</script>

<script>
  const proceedWA = (nomor, order) => {
      window.location =
          `https://api.whatsapp.com/send/?phone=${nomor}&text=Halo+Admin%2C+saya+mau+konfirmasi+pembayaran+orderan+Mystery+Box+12.12.%0D%0A%0D%0AOrder+ID%3A+${order}`;
  }
</script>
@endsection
