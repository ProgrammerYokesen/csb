@section('title')
    Pendapat Sobat
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body csb__survey">
                        <!-- <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSemslfT2YECGA0JP4-r7LrnCqNhjD-eJ2BbbGDp2iVIQ3gxew/viewform?embedded=true" style="width: 100%; height: 500px" frameborder="0" marginheight="0" marginwidth="0"></iframe> -->
                        <!-- <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSd29WZ8JAWAEEbDWDLUzIGadMFqDFcWEklAXwx7wviLyo97iA/viewform?embedded=true" style="width: 100%; height: 500px" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe> -->

                        <form action="index.html" method="post">
                          <div class="form-group">
                            <label>Pertanyaan <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="survey1" placeholder="Jawaban Kamu" required autocomplete="off"/>
                          </div>

                          <div class="text-center">
                            <button type="submit" class="btn" style="margin: 25px auto; color:#fff; background: #ffaa3a; padding: 7px 20px">Lanjut</button>
                          </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
