<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak | @yield('title') </title>
    @laravelPWA

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta property="og:title" content="Club Sobat Badak" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://sobatbadak.club" />
    <meta property="og:image" content="https://sobatbadak.club/images/share.png" />
    <meta property="og:description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, shrink-to-fit=no" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('regional/style.css') }}?v=3.1.3">
    @yield('page_assets')

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '806244510020586');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199518915-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199518915-2');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NVTPTBJ');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 695110688 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-695110688"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-695110688');
    </script>

    <link
      rel="stylesheet"
      href="https://cdn.rawgit.com/afeld/bootstrap-toc/v1.0.1/dist/bootstrap-toc.min.css"
    />
    <style>
      .header {
        background: #FFFFFF;
        border: 1px solid rgba(0, 0, 0, 0.1);
      }
    </style>
</head>

<body id="kt_body"
    class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading" data-spy="scroll" data-target="#toc" style="background: none">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVTPTBJ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-row flex-column-fluid page">
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper" style="position: relative;">

                @include('dashboard.components.navbar')
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                  <div class="d-flex flex-column-fluid">
                      <div class="container">
                        <h1>Koordinator Cabang</h1>
                        <p>Club Sobat Badak mempersembahkan CSB Regional untuk meningkatkan semangat dan antusias dari Sobat Badak untuk terus berkarya dan bergembira dalam komunitas ini dengan bercengkrama dengan Sobat Badak sekitarnya. Oleh karena itu, koordinasi cabang diperlukan untuk memaksimalkan pengalaman ini!</p>
                      <div class="row">
                        <div class="col-lg-3">
                          <nav id="toc"></nav>
                        </div>
                        <div class="col-lg-9">
                          <h2>Cara Mendaftar</h2>
                          <h3>asdadsd</h3>
                          <p>baskabdladkasbdkl</p>
                          <h2>Title B</h2>
                          <p>Aksjdlaksdjlaksdjk</p>
                          <h2>Title C</h2>
                          <p>asldjkasdaldsj</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @include('dashboard.components.footer')

            </div>
        </div>
    </div>

    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=806244510020586&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
    </script>
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#6993FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1E9FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>

    <script src="https://cdn.rawgit.com/afeld/bootstrap-toc/v1.0.1/dist/bootstrap-toc.min.js"></script>
    <script>
    $(function() {
      var navSelector = "#toc";
      var $myNav = $(navSelector);
      Toc.init({
        $nav: $myNav,
        $scope: $("h2, h3"),
      });
      $("body").scrollspy({
        target: navSelector
      });
    });
    </script>
</body>
</html>
