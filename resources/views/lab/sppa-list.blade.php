@section('title')
    Sang Pembawa Perubahan Awards
@endsection

@extends('dashboard.master')

@section('page_assets')
    <link rel="stylesheet" href="{{ asset('user-dashboard/sppa.css') }}?v=1">
@endsection

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div id="sppa" class="d-flex flex-column-fluid justify-content-center align-items-center">
          <form class="" action="{{ route('voteVideoSppa') }}" method="post">
            @csrf

            <input type="hidden" name="video_id" value="">
            <div class="container">
              <div class="card card-custom gutter-b" style="padding-bottom: 3rem">
                  <div class="tebak_kata_card_head head-edit-profile justify-content-between d-flex">
                      <a href="{{ route('homeDash') }}">
                          <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                      </a>
                      @if($vote)
                        <button class="btn dash_nav_homepage py-2 px-6" type="button" style="font-size: 14px" onclick="confirmVote()">
                            VOTING SEKARANG!
                        </button>
                      @else
                        <button class="btn dash_nav_homepage py-2 px-6" type="button"  style="font-size: 14px" data-toggle="modal" data-target="#modal_syarat">
                            VOTING VIDEO!
                        </button>
                      @endif
                  </div>
                  <div class="d-flex justify-content-center">
                      <h1 style="margin: 0" class="bold-36-black">
                        Nominasi Sang Pembawa
                        <br>
                        Perubahan Awards {{ $vote }}
                      </h1>
                  </div>

                  <div class="card-body card-edit-profile">
                    <div class="categories d-flex">
                      <div class="category active cat_0" value="0">
                        Semua
                      </div>
                      @foreach($categories as $value)
                        <div class="category cat_{{ $value->id }}" value="{{ $value->id }}">
                          {{ $value->category_name }}
                        </div>
                      @endforeach
                    </div>

                    <div class="row" style="margin-top:3rem">
                      @foreach ($videos as $value)
                      <div class="col-md-4 col-12 sppa__card">
                        <div class="position-relative">
                          <div class="image__card" onclick="openVideo('{{ $value->embed_url }}')">
                             <img src="{{ $value->thumbnail }}" alt="sobatbadak empty">
                          </div>
                          <div class="info__card">
                            <p>Karya dari: {{$value->name}}</p>
                            <p>Kategori: {{$value->category_name}}</p>
                          </div>
                          <div class="vote__badge">
                            <div class="d-flex">
                              <h6 class="mb-0">
                                <i class="fas fa-check-circle" style="color:#FF8500"></i>
                                <b>
                                  {{ $value->totalLike }}
                                </b>
                              </h6>
                            </div>
                          </div>
                          @if($vote)
                          <div class="box__vote text-center">
                            <div class="vote__box mx-auto vid_{{ $value->id }}" onclick="vote({{ $value->id }})">
                            </div>
                            <i class="mx-auto fas fa-check-circle d-none vid_{{ $value->id }}" style="color:#FF8500;font-size:34px" onclick="vote({{ $value->id }})"></i>
                          </div>
                          @endif
                        </div>
                      </div>
                      @endforeach

                    </div>

                    {{ $videos->links() }}

                  </div>
              </div>
            </div>

          </form>
        </div>
    </div>

    <div class="modal fade" id="modal_syarat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content position-relative">
                <div class="position-absolute" style="top:20px; right:20px; z-index:1">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <i aria-hidden="true" class="ki ki-close"></i>
                  </button>
                </div>
                <div class="modal-body">
                  <h5 class="title-part mb-5" style="padding-top:2rem">
                      Peraturan Voting Nominasi Sang Pembawa Perubahan Awards
                  </h5>
                  <ol >
                      <li class="exp-part">
                          <h6 class="text-left ">Urutan video berdasarkan waktu upload</h6>
                      </li>
                      <li class="exp-part">
                          <h6 class="text-left ">Video kamu akan dikonfirmasi H+3 dari waktu upload</h6>
                      </li>
                      <li class="exp-part">
                          <h6 class="text-left ">Waktu pengumpulan: 15 - 26 Desember 2021</h6>
                      </li>
                      <li class="exp-part">
                          <h6 class="text-left ">Waktu voting: 15 - 26 Desember 2021</h6>
                      </li>
                      <li class="exp-part">
                          <h6 class="text-left ">Keputusan panitia Club Sobat Badak tidak dapat diganggu gugat</h6>
                      </li>
                  </ol>

                  <div class="d-flex justify-content-center mt-5">
                    <a class="btn dash_nav_homepage" href="{{ route('sppaList',['vote'=>true]) }}">Setuju dan lanjutkan</a>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Video -->
    <div class="modal fade" id="modal_video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body modal_intro">
                    <div class="modal__video_player">
                      <!-- <iframe src="https://www.instagram.com/tv/CXei7CTDVU4/embed/" width="50%" height="100%"></iframe> -->
                      <div class="iframe_video">

                      </div>
                    </div>
                    <button class="btn" data-dismiss="modal">Selesai</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')
    @if (Session::has('emailVerification'))
        <script>
            $('#modal_email').modal('toggle');
        </script>
    @endif

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>
    <!-- <script src="https://www.tiktok.com/embed.js"></script> -->

    <script>
      var url = 'https://www.tiktok.com/@pepsi.121/video/7034315492500278555'
      var q = []
      const searchs = new URLSearchParams(window.location.search);
      var queries = searchs.get('q')

      if(searchs.has('q')){
        queries = queries.replace("[","")
        queries = queries.replace("]","")
      }


      $(function(){
        // mapping query params
        if(queries != undefined){
          $(`.cat_0`).removeClass('active')
          queries.split`,`.map(val => {
            q.push(val)
            $(`.cat_${val}`).addClass('active')
          })
        }else{
          $('.cat_0').addClass('active')
        }

        // handle choose filter
        $('.category').on('click', function(){
          let value = $(this).attr( "value" )

          if(value != 0){
            $(`.cat_0`).removeClass('active')

            if($(`.cat_${value}`).hasClass('active')){
              $(`.cat_${value}`).removeClass('active')

              const index = q.indexOf(value);
              if (index > -1) {
                q.splice(index, 1);
              }
            }else{
              $(`.cat_${value}`).addClass('active')
              q.push(value)
            }

            // function execute after 800ms user not click anything
            if(this.timeout) clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
              if(q.length != 0){
                window.location.href = "{{ route('sppaList') }}?q=["+q+"]"
              }else{
                window.location.href = "{{ route('sppaList') }}"
              }
            }, 800);
          }else{
            q = []
            $('.category').removeClass('active')
            $('.cat_0').addClass('active')
            window.location.href = "{{ route('sppaList') }}"
          }
        })
      })

      function openVideo(url){
        $(".iframe_video").empty()
        $(".iframe_video").append(url)
        $("#modal_video").modal('toggle')
      }

      function vote(id){
        $('.vote__box.vid_'+id).addClass('d-none')

        if($('i.vid_'+id).hasClass('d-none')){
          $('i.vid_'+id).removeClass('d-none')
          $('input[name="video_id"]').val(id)
        }else{
          $('i.vid_'+id).addClass('d-none')
          $('.vote__box.vid_'+id).removeClass('d-none')
        }
      }

      function confirmVote(){
        Swal.fire({
          title: 'Apa kamu yakin ingin memilih video ini?',
          text: "Karena kamu hanya bisa memilih satu video untuk di like/vote.",
          showDenyButton: true,
          confirmButtonText: 'Ya',
          denyButtonText: `Tidak`,
        }).then((result) => {
          if (result.isConfirmed) {
            const data = $('input[name="video_id"]').val()
            if(data != "" && data != null && data != undefined){
              $('form').submit()
            }else{
              Swal.fire("GAGAL!", "Harap pilih video terlebih dahulu!", "error");
            }
          }
        })
      }

    </script>
    @if (Session::has('fail'))
        <script>
            $(function() {
                Swal.fire("Kesalahan Server!", '{{ Session::get('fail') }}', "error");
            })
        </script>
    @elseif(Session::has("errorMsg"))
        <script>
            $(function() {
                Swal.fire("Terjadi Kesalahan!", '{{ Session::get('errorMsg') }}', "error");
            })
        </script>
    @elseif(Session::has("success"))
        <script>
            $(function() {
                Swal.fire("Berhasil!", '{{ Session::get('success') }}', "success");
            })
        </script>
    @elseif(Session::has("city"))
        <script>
            $(function() {
                Swal.fire("Gagal!", 'Kota tidak boleh kosong!', "error");
            })
        </script>
    @elseif(Session::has("name"))
        <script>
            $(function() {
                Swal.fire("Gagal!", 'Nama tidak boleh kosong!', "error");
            })
        </script>
    @elseif(Session::has("kode_pos"))
        <script>
            $(function() {
                Swal.fire("Gagal!", 'Kode pos tidak boleh kosong!', "error");
            })
        </script>
    @elseif(Session::has("no_wa"))
        <script>
            $(function() {
                Swal.fire("Gagal!", '{{ Session::get('no_wa') }}', "error");
            })
        </script>
    @endif


    @if (Session::has('Voucher'))
        @if (Session::get('Voucher') > 0)
            <script>
                var poin = {{ Session::get('Voucher') }};
                let formattedPoin = new Intl.NumberFormat('id-ID').format(poin)
                Swal.fire(
                    "Selamat!",
                    `Kamu mendapatkan ${formattedPoin} Baper Poin. Kupon Redeem akan langsung masuk ke akunmu. `,
                    "success");
            </script>
        @endif
        @if (Session::get('Voucher') == 0)
            <script>
                Swal.fire("Gagal!", `Kode voucher tidak valid`, "error");
            </script>
        @endif
    @endif
@endsection
