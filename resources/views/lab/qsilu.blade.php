@section('title')
    Quiz Show
@endsection

@section('page_assets')
<style>
.list__peserta {
  background: rgba(255, 170, 58, 0.1);
  border-radius: 8px;
  padding: 10px;

  font-weight: 500;
  font-size: 14px;
  line-height: 21px;
  text-align: center !important;
  color: #000000;
}

.list__peserta.skipped {
  background: rgba(0, 0, 0, 0.1);
}
.list__peserta.win {
  background: #8200E8;
  color: #fff;
}
.list__peserta.playing {
  background: #00CC5D;
}
.list__peserta.waiting {
  background: #0078A7;
  color: #ffffff;
}

.dropdown-menu {
    max-height: 350px;
    overflow-y: auto;
}

</style>
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('quizShowImlekDetail') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <button class="btn" data-toggle="modal" data-target="#modal-sop">
                              Peraturan
                            </button>
                        </div>
                    </div>
                    <div class="card-body quiz__show">

                      <h1>List Peserta - Quiz Show Special Imlek</h1>

                      @php
                        $now = date("Y-m-d H:i:s");
                      @endphp

                      <!-- <p style="margin:0">{{ date('d M Y', strtotime($now)) }}</p> -->

                      <div class="text-center mb-3">
                        <div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ date('d M Y', strtotime($currentEvent->start_date)) }}
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach ($events as $ev)
                            <a class="dropdown-item" href="{{route('labQsiLu', $ev->id)}}">{{ date('d M Y', strtotime($ev->start_date)) }}</a>
                            @endforeach
                          </div>
                        </div>
                      </div>

                      <div class="row justify-content-center align-items-center" style="margin-top: 25px">

                        @foreach ($data as $key => $d)
                          <div class="col-lg-4 mb-3">
                            <div class="list__peserta {{$d->status == 'new' ? 'waiting' : $d->status}}">
                              {{$d->name}}  {{$d->prize != null ? '- Rp ' . number_format($d->prize, 0, ',', '.') : ''}}
                            </div>
                          </div>
                        @endforeach



                      </div>

                      <div class="text-center teman_sobat_paginate">
                          {{ $data->links() }}
                      </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="rule-1212" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Yang dapat mengikuti games ini adalah Sobat Badak yang telah menyelesaikan transaksi Harbolnas 12.12
                            </li>
                            <li>
                                Sobat Badak yang bermain akan dipilih melalui Spin the Wheel
                            </li>
                            <li>
                                Untuk mencegah terhambatnya proses pemilihan nama peserta, Sobat Badak wajib mendaftar setiap kali ingin mengikuti games melalui website sobatbadak.club di menu "Quiz Show"
                            </li>
                            <li>
                                Games berlangsung di Zoom Live Club Sobat Badak dari tanggal 15-30 Desember 2021 pukul 15.00 - 21.00 WIB
                            </li>
                            <li>
                                Jika Sobat Badak tidak hadir saat nama terpilih melalui Spin the Wheel maka nama kalian akan hangus pada sesi tersebut
                            </li>
                            <li>
                                Sobat Badak dapat melakukan pendaftaran kembali di sobatbadak.club untuk games berikutnya
                            </li>
                            <li>
                                Sobat Badak hanya mendapatkan satu kali kesempatan untuk menang di games Harbolnas 12.12
                            </li>
                            <li>
                                Tanggal 25 Desember 2021 tidak akan ada sesi games dikarenakan hari libur nasional
                            </li>9
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal SOP -->
    <div class="modal fade" id="modal-sop" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>SOP/Peraturan Quiz Show Special Imlek</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <div data-scroll="true" data-height="500">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                          <li>
                            Yang dapat mengikuti games ini adalah Sobat Badak yang telah menyelesaikan transaksi sebesar Rp 61.000
                          </li>
                          <li>
                              Sobat Badak yang ingin mengikuti games ini juga WAJIB mengikuti WORKSHOP RESELLER pada 22 Januari 2022, pukul 16.00 WIB
                          </li>
                          <li>
                            Quiz Show Special Imlek 2022 akan berlangsung di ZOOM Club Sobat Badak dari tanggal 23 Januari - 31 Januari 2022
                          </li>
                          <li>
                            Peserta wajib menggunakan nama yang didaftarkan untuk Quiz Show Special Imlek 2022 saat berada di ZOOM
                          </li>
                          <li>
                            Sobat Badak wajib mendaftar setiap kali ingin mengikuti Quiz Show Imlek melalui website sobatbadak.club di menu "Quiz Show Imlek". Pendaftaran akan dibuka pada pukul 14.30 WIB
                          </li>
                          <li>
                            Peserta yang bermain adalah 60 orang pertama yang menekan tombol Join PADA HARI ITU
                          </li>
                          <li>
                            Nama-nama Sobat Badak yang telah menekan tombol join akan muncul di Leaderboard pada website Club Sobat Badak
                          </li>
                          <li>
                            Jika Sobat Badak sudah terpilih namun tidak berada di zoom, Host akan memanggil sebanyak 3x. Setelah dipanggil masih tidak muncul, maka tim CSB akan memanggil nama di urutan selanjutnya
                          </li>
                          <li>
                            Jika Sobat Badak yang terpilih berada di Zoom, tetapi belum menghidupkan audio ataupun camera, Tim CSB akan memberikan kesempatan sebanyak 3 kali panggilan. Lewat dari itu, Tim CSB akan memanggil nama yang muncul selanjutnya
                          </li>
                          <li>
                            Jika Sobat Badak tidak terpanggil di Quiz Show hari pertama, Sobat Badak mendaftar lagi di hari selanjutnya
                          </li>
                          <li>
                            Sobat Badak hanya mendapatkan satu kali kesempatan untuk menang di Quiz Show Imlek
                          </li>
                        </ol>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection
