<div class="col-md-3 col-lg-2 col-sm-4  col-6 mb-3">
    <div class="kategori-box card h-100" id="{{$id}}" onclick="clickKategori('{{$id}}')">
        <div class="card-body ">
            <img src="{{$asset}}" alt="{{$title}}" class="kategori-box-img mb-2">
            <p class="exp-part" style="font-weight: 600;">{{$title}}</p>
        </div>
    </div>
</div>
