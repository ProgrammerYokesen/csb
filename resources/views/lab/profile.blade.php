@section('title')
    Edit Profile
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <form enctype="multipart/form-data" id="form-edit-profile" action="{{ route('labhandleEditProfile') }}" method="post">
                    @csrf
                    <div class="card card-custom gutter-b" style="padding-bottom: 3rem">
                        <div class="tebak_kata_card_head head-edit-profile justify-content-between d-flex">
                            {{-- <div class="card-title"> --}}
                            <a href="{{ route('homeDash') }}">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                            <div class="d-flex" id="button-in-desktop">
                                <a href="{{ route('changePassword') }}" class="btn custom-white-btn py-2 px-6 mr-2"
                                    style="font-size: 14px;">
                                    Ganti Passsword
                                </a>
                                <button type="submit" class="btn dash_nav_homepage py-2 px-6" style="font-size: 14px">
                                    Simpan
                                </button>
                            </div>
                            <button class="btn redeem_vouch" data-toggle="modal" data-target="#modal_redeem">Redeem
                                Code</button>
                        </div>
                        <div class="d-flex justify-content-center">
                            <h1 style="margin: 0" class="bold-36-black">Edit Profile</h1>
                            <br>
                            <!--<label class="col-md-2 col-sm-12 col-form-label text-center">Baper Poin : {{ $poin }}</label>-->
                        </div>
                        <div class="d-flex justify-content-center" style="margin-bottom: 2rem">
                            <h5 style="margin: 0" class="bold-18-black">Baper Poin : {{ number_format($poin, 0, ',', '.') }}</h5>
                        </div>
                        <div class="card-body card-edit-profile">
                            <div class="text-center" style="margin-bottom:3rem">
                                <div class="avatar-box-profile" style="display: inline-block">
                                    <img src="{{ asset(Auth::user()->avatars == null ? 'images/badak-baper.png' : Auth::user()->avatars) }}"
                                        width="100%" alt="">
                                </div>
                                <div class="text-change-img mt-2 btn-change-ava" data-toggle="modal"
                                    data-target="#modal_avatar">
                                    Ganti Karakter
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label" style="text-align: left">Nama</label>
                                <div class="col-md-8 col-sm-12">
                                    <input class="form-control" type="text" name="name" value="{{ Auth::user()->name }}"
                                         />
                                    <i style="font-size:12px; color:#e52c2c">*Hanya bisa mengganti nama sekali. Pastikan
                                        nama yang kamu pakai sesuai dengan Kartu Identitas.</i>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label" style="text-align: left">Email</label>
                                <div class="col-md-8 col-sm-8 col-8">
                                    <input class="form-control" type="email" name="email"
                                        value="{{ Auth::user()->email }}" disabled />
                                </div>
                                @if (Auth::user()->emailValidation == 'new')
                                    <div class="col-md-2 col-sm-4 col-4" style="align-items: center">
                                        <a href="{{ route('sendVerifikasiEmail') }}"
                                            class="btn font-weight-bold custom-success-btn py-2 px-4 mr-2">Verifikasi</a>
                                    </div>
                                @elseif(Auth::user()->emailValidation == "validated")
                                    <div class="col-md-2 col-sm-4 col-4 d-flex" style="align-items: center">
                                        <img class="img-wa-verified" src="/images/icons/circle-check.svg" alt="">
                                        <div class="wa-verified ml-1">
                                            Terverifikasi
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label" style="text-align: left">No
                                    Whatsapp</label>
                                <div class="col-md-8 col-sm-8 col-8 d-flex" style="align-items: center">
                                    <input class="form-control" type="tel" name="no_wa"
                                        value="{{ Auth::user()->whatsapp }}" required {{Auth::user()->whatsapp_verification == 1 ? 'readonly' : ''}} />
                                </div>
                                @if (Auth::user()->whatsapp_verification == 0)
                                    <div class="col-md-2 col-sm-4 col-4" style="align-items: center">
                                        <a href="https://wa.me/6281287628068/?text=verify {{ Auth::user()->referral_code }}"
                                            class="btn font-weight-bold custom-success-btn py-2 px-4 mr-2">Verifikasi</a>
                                    </div>
                                @else
                                    <div class="col-md-2 col-sm-4 col-4 d-flex" style="align-items: center">
                                        <img class="img-wa-verified" src="/images/icons/circle-check.svg" alt="">
                                        <div class="wa-verified ml-1">
                                            Terverifikasi
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label" style="text-align: left">Alamat
                                    Lengkap</label>
                                <div class="col-md-8 col-sm-12">
                                    <textarea class="form-control" name="alamat" rows="3"
                                        required>{{ Auth::user()->alamat_rumah }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label"
                                    style="text-align: left">Kabupaten/Kota</label>
                                <div class="col-md-8 col-sm-12">
                                    <select class="form-control select2" id="select2_lib" name="city">
                                        @foreach ($city as $item)
                                            <option {{ $item->city_name == Auth::user()->kota ? 'selected' : '' }}
                                                value="{{ $item->city_name }}">
                                                {{ $item->city_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label" style="text-align: left">Kode Pos</label>
                                <div class="col-md-8 col-sm-12">
                                    <input class="form-control" type="number" name="kode_pos"
                                        value="{{ Auth::user()->kode_pos }}" required />
                                </div>
                            </div>

                            <div id="button-in-mobile" class="d-flex justify-content-center pt-6">
                                <a href="{{ route('changePassword') }}" class="btn custom-white-btn py-2 px-6 mr-2"
                                    style="font-size: 14px;">
                                    Ganti Passsword
                                </a>
                                <button type="submit" class="btn dash_nav_homepage py-2 px-6" style="font-size: 14px">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card card-custom gutter-b">
                      <div class="card-body">
                        <div class="row mt-5 align-items-center">
                            <div class="col-md-2">
                                <label class="fonts-16 fc-black opac-50" for="">NIK</label>
                            </div>
                            <div class="col-md-10">
                                <div class="position-relative">
                                    <input value="{{ Auth::user()->nik }}" autocomplete="off" name="nik" placeholder="Masukan NIK" type="text"
                                        class="form-control fontw-6">
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-md-2">
                                <label class="fonts-16 fc-black opac-50" for="">Upload KTP</label>
                            </div>
                            <div class="col-md-10">
                                <div class="d-sm-flex d-block">
                                    <div class="d-flex align-items-center">
                                      @if (Auth::user()->ktp_verification == 2 || Auth::user()->ktp == null)
                                          <input autocomplete="off" type="file" id="upload_ktp" name="ktp"
                                              accept="image/jpeg, image/png" hidden />
                                          <label for="upload_ktp" class="file__input btn__success1 mb-0 label__ktp"
                                              style="color: #098b39">
                                              Klik
                                              di
                                              sini
                                              untuk
                                              upload
                                          </label>
                                      @endif

                                        <span id="ktp_name">{{ Auth::user()->ktp }}</span>
                                    </div>


                                    @if (Auth::user()->ktp)
                                        @if (Auth::user()->ktp_verification == 0)
                                            <div
                                                class="d-flex align-items-center text-warning p-3 bg-warning-o-20 rounded-sm profile__warning">
                                                Menunggu Verifikasi
                                            </div>
                                        @elseif(Auth::user()->ktp_verification == 1)
                                            <div class="btn terverifikasi" disabled style="width: unset!important">
                                                <span>
                                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M7.99992 14.4173C11.5437 14.4173 14.4166 11.5445 14.4166 8.00065C14.4166 4.45682 11.5437 1.58398 7.99992 1.58398C4.45609 1.58398 1.58325 4.45682 1.58325 8.00065C1.58325 11.5445 4.45609 14.4173 7.99992 14.4173Z"
                                                            stroke="#1AD598" stroke-width="2" stroke-linejoin="round" />
                                                        <path
                                                            d="M10.8188 5.08418C10.7321 5.08745 10.647 5.10832 10.5686 5.14542C10.4902 5.18251 10.4201 5.23514 10.3626 5.30008C9.31261 6.4335 8.35478 7.54532 7.34678 8.65482L6.16203 7.63113C6.09538 7.57267 6.01717 7.52865 5.93249 7.50239C5.84781 7.47613 5.75855 7.46824 5.67052 7.47875C5.58249 7.48926 5.49765 7.51818 5.42152 7.56363C5.3454 7.60908 5.2797 7.66999 5.22869 7.7425C5.11839 7.89308 5.06788 8.0792 5.08696 8.26488C5.10603 8.45056 5.19333 8.62238 5.33195 8.74739L6.99037 10.1772C7.11556 10.2864 7.27885 10.3414 7.44457 10.3305C7.61029 10.3195 7.76494 10.2435 7.8747 10.1188C9.06762 8.83085 10.1363 7.5658 11.3018 6.30639C11.428 6.1688 11.4988 5.98923 11.5004 5.80252C11.502 5.61581 11.4344 5.43498 11.3105 5.29524C11.2486 5.22672 11.1726 5.17246 11.0877 5.13602C11.0028 5.09957 10.9111 5.08191 10.8188 5.08418Z"
                                                            fill="#1AD598" />
                                                    </svg>
                                                </span> Terverifikasi
                                            </div>
                                        @else
                                            <div id="not_verify" class="d-flex align-items-center p-3 bg-danger-o-50 rounded-sm profile__warning">
                                                <span>
                                                    <svg width="26" height="24" viewBox="0 0 26 24" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M14.6487 1.98877L24.7332 19.9888C25.5054 21.367 24.5106 23 23.0845 23H2.91549C1.48937 23 0.494608 21.367 1.26678 19.9888L11.3513 1.98877C12.0899 0.67041 13.9101 0.67041 14.6487 1.98877Z"
                                                            stroke="#DF0000" stroke-width="2" />
                                                        <path
                                                            d="M13.5 19.7494C14.0799 19.7494 14.55 19.2793 14.55 18.6994C14.55 18.1195 14.0799 17.6494 13.5 17.6494C12.9201 17.6494 12.45 18.1195 12.45 18.6994C12.45 19.2793 12.9201 19.7494 13.5 19.7494Z"
                                                            fill="#DF0000" />
                                                        <path
                                                            d="M13.5 9.25C13.2215 9.25 12.9545 9.36062 12.7576 9.55754C12.5606 9.75445 12.45 10.0215 12.45 10.3V15.55C12.45 15.8285 12.5606 16.0955 12.7576 16.2925C12.9545 16.4894 13.2215 16.6 13.5 16.6C13.7785 16.6 14.0456 16.4894 14.2425 16.2925C14.4394 16.0955 14.55 15.8285 14.55 15.55V10.3C14.55 10.0215 14.4394 9.75445 14.2425 9.55754C14.0456 9.36062 13.7785 9.25 13.5 9.25Z"
                                                            fill="#DF0000" />
                                                    </svg>
                                                </span>
                                                <p class="mb-0 ml-2" style="color: #DF0000">
                                                    KTP anda tidak terverifikasi. Silahkan upload ulang.
                                                </p>
                                            </div>
                                            <button class="btn btn__success1 d-none ktp__button" style="border: none">
                                                <div class="d-none spinner spinner-track spinner-light py-3"
                                                    style="margin: 0 3rem 0 2rem;"></div>
                                                <div class="text__btn">Konfirmasi Sekarang</div>
                                            </button>
                                        @endif
                                    @else
                                        <button class="btn btn__success1 d-none ktp__button" style="border: none">
                                            <div class="d-none spinner spinner-track spinner-light py-3"
                                                style="margin: 0 3rem 0 2rem;"></div>
                                            <div class="text__btn">Konfirmasi Sekarang</div>
                                        </button>
                                    @endif
                                </div>
                                <small id="error__ktp" class="d-none text-danger"><strong>File harus berekstensi jpg, jpeg, atau
                                        png!</strong><br></small>

                            </div>
                        </div>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{-- Modal Email --}}
    <div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body modal_intro">
                    <img src="{{ asset('images/baper-coin.png') }}" alt="">
                    <h6>Email verifikasi sudah dikirim!</h6>
                    <p>Yuk cek inbox email kamu dan klik linknya untuk verifikasi email</p>
                    <button class="btn" data-dismiss="modal">OK!</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Redeem --}}
    <div class="modal fade" id="modal_redeem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body modal_redeemvoch">
                    <div class="text-center">
                        <h5>Silahkan masukkan kode untuk redeem kupon</h5>
                    </div>

                    <form class="form coin-form" id="redeem-form" action="{{ route('redeemVoucher') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control redeem_input" name="voucher"
                                placeholder="Contoh: F00315911201" required autocomplete="off" />
                        </div>
                        <div class="d-flex justify-content-center mt-6">
                            <button type="submit" class="redeem-button btn btn-block dash_nav_homepage py-4">Redeem</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('pageJS')
    @if (Session::has('emailVerification'))
        <script>
            $('#modal_email').modal('toggle');
        </script>
    @endif

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#select2_lib').select2();
        });

        function regexImage(type) {
          return type.includes('jpg') || type.includes('jpeg') || type.includes('png')
        }

        $(function(){
          // handle change image
          $('#upload_ktp').on('change', function(event) {
              if (!$('.ktp__button').hasClass('d-none')) {
                  $('.ktp__button').addClass('d-none')
              }

              if (!regexImage(event.target.files[0].type)) {
                  $('#error__ktp').removeClass('d-none')
              } else {
                  if (!$('#error__ktp').hasClass('d-none')) {
                      $('#error__ktp').addClass('d-none')
                  }
                  $('.ktp__button').removeClass('d-none')
                  $("#not_verify").addClass('d-none')

                  $('#ktp_name').text(event.target.files[0].name)
              }
          })

          $('form').on('submit', function(){
             KTApp.blockPage({
              overlayColor: '#000000',
              state: 'warning', // a bootstrap color
              size: 'lg' //available custom sizes: sm|lg
             });
          })
        })
    </script>
    @if (Session::has('fail'))
        <script>
            $(function() {
                Swal.fire("Kesalahan Server!", '{{ Session::get('fail') }}', "error");
            })
        </script>
    @elseif(Session::has("errorMsg"))
        <script>
            $(function() {
                Swal.fire("Terjadi Kesalahan!", '{{ Session::get('errorMsg') }}', "error");
            })
        </script>
    @elseif(Session::has("success"))
        <script>
            $(function() {
                Swal.fire("Berhasil!", '{{ Session::get('success') }}', "success");
            })
        </script>
    @elseif(Session::has("city"))
        <script>
            $(function() {
                Swal.fire("Gagal!", 'Kota tidak boleh kosong!', "error");
            })
        </script>
    @elseif(Session::has("name"))
        <script>
            $(function() {
                Swal.fire("Gagal!", 'Nama tidak boleh kosong!', "error");
            })
        </script>
    @elseif(Session::has("kode_pos"))
        <script>
            $(function() {
                Swal.fire("Gagal!", 'Kode pos tidak boleh kosong!', "error");
            })
        </script>
    @elseif(Session::has("no_wa"))
        <script>
            $(function() {
                Swal.fire("Gagal!", '{{ Session::get('no_wa') }}', "error");
            })
        </script>
    @endif


    @if (Session::has('Voucher'))
        @if (Session::get('Voucher') > 0)
            <script>
                var poin = {{ Session::get('Voucher') }};
                let formattedPoin = new Intl.NumberFormat('id-ID').format(poin)
                Swal.fire(
                    "Selamat!",
                    `Kamu mendapatkan ${formattedPoin} Baper Poin. Kupon Redeem akan langsung masuk ke akunmu. `,
                    "success");
            </script>
        @endif
        @if (Session::get('Voucher') == 0)
            <script>
                Swal.fire("Gagal!", `Kode voucher tidak valid`, "error");
            </script>
        @endif
    @endif
@endsection
