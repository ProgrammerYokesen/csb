@section('title')
    Quiz Show
@endsection

@extends('dashboard.master')

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <div class="container">
                <div class="card card-custom gutter-b input_quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('quizShow') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="card-body quiz__show_rule">
                      <div class="container">
                        <div class="row" style="margin-bottom: 25px">
                          <div class="col-lg-6">
                            <img src="{{ $data['game']->image_drive }}" alt="">
                          </div>
                          <div class="col-lg-6">
                            <div class="d-flex">
                              <div class="quiz__show_rule_countdown">
                                <div class="clockdiv d-flex" data-date="{{ $data['game']->end_register }}">
                                  <!-- <div class="d-flex">
                                    <span class="days"></span>
                                    <div class="smalltext">:</div>
                                  </div> -->
                                  <div class="d-flex">
                                    <span class="hours">00</span>
                                    <div class="smalltext">:</div>
                                  </div>
                                  <div class="d-flex">
                                    <span class="minutes">00</span>
                                    <div class="smalltext">:</div>
                                  </div>
                                  <div class="d-flex">
                                    <span class="seconds">00</span>
                                    <div class="smalltext"></div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <h1>{{ $data['game']->game_name }}</h1>
                            <h6>{{ $data['game']->description }}</h6>

                            <div class="quiz__show_game" style="text-align: left; margin-bottom: 0; margin-top: 15px">
                              <a href="{{route('listPeserta', $event_id)}}">
                                <button class="btn prm">List Peserta</button>
                              </a>
                            </div>

                          </div>
                        </div>

                        <h5>Peraturan:</h5>
                        <ol>
                          @foreach ($data['rules'] as $item)
                            <li>{{ $item->rule }}</li>
                          @endforeach
                        </ol>

                        @php
                          $now = date("Y-m-d H:i:s");
                        @endphp

                        <div class="text-center quiz__show_game">
                          @if (strtotime($now) >= strtotime($data['game']->start_register) && strtotime($now) <= strtotime($data['game']->end_register))

                            @if ($register == false)
                              <button class="btn prm" data-toggle="modal" data-target="#not_register">Daftar</button>
                            @elseif ($paid == false)
                              <button class="btn prm" data-toggle="modal" data-target="#not_paid">Daftar</button>
                            @else
                              <a href="{{route('joinGame', [ 'eventId' => $event_id ])}}">
                                <button class="btn prm">Daftar</button>
                              </a>
                            @endif


                          @endif
                        </div>

                      </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Peraturan --}}
    <div class="modal fade" id="cara-tebak-kata" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal_auction">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Baba Mencari Bakat</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                    <div class="auction-text-tnc">
                        <ol class="mt-1">
                            <li>
                                Peserta yang sudah daftar harus hadir 15 menit sebelum Baba Mencari Bakat dimulai
                            </li>
                            <li>
                                Setiap peserta mendapatkan waktu 10 menit untuk menampilkan bakatnya
                            </li>
                            <li>
                                Setiap bakat yang ditunjukkan, <strong>tidak boleh</strong> mengandung SARA atau unsur
                                pornografi
                            </li>
                            <li>
                                Jumlah Baper Poin yang bisa didapatkan peserta berdasarkan keputusan Baba
                            </li>
                            <li>
                                Peserta harus datang sesuai jadwal yang sudah dipilih melalui sobatbadak.club
                            </li>
                            <li>
                                Silahkan daftar Baba Mencari Bakat melalui sobatbadak.club/
                            </li>
                            <li>
                                Keputusan panitia Club Sobat Badak tidak dapat diganggu gugat
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="not_register" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Kamu belum daftar Event 12.12</h5>
                  <p>Klik disini untuk daftar Event 12.12</p>
                  <a href="{{route('harbolnas1212')}}">
                    <button class="btn">Menuju Halaman 12.12</button>
                  </a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="not_paid" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Mohon konfirmasi pembayaran Mystery Box kamu ke admin ya</h5>
                  <p>Sebelum mendaftar silahkan selesaikan pembayaran dengan admin kami di Whatsapp.</p>
                  <p>Order ID kamu: <strong>{{$user->orderId}}</strong></p>

                  <button class="btn" data-dismiss="modal">Tutup</button>
                  <button class="btn" onclick="proceedWA({{$user->whatsapp}},'{{$user->orderId}}')">Chat Admin</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="regis_success" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal__quizshow">
                <div class="modal-header" style="border: transparent">
                    <div></div>
                    <!-- <h5 class="modal-title" id="exampleModalLabel"><b>Peraturan Games Harbolnas 12.12</b></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0">
                  <img src="{{asset('images/badak-baper.png')}}" alt="">
                  <h5>Selamat! kamu sudah terdaftar di Quiz Show ini.</h5>
                  <p>Silahkan langsung masuk zoom Club Sobat Badak ya! Semoga Beruntung</p>

                  <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank">
                    <button class="btn">OK</button>
                  </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJS')

@if (Session::has('success_regist'))
    <script>
        $(`#regis_success`).modal('toggle')
    </script>
@endif

<script>
  document.addEventListener('readystatechange', event => {
    if (event.target.readyState === "complete") {
        var clockdiv = document.getElementsByClassName("clockdiv");
        var countDownDate = new Array();
        for (var i = 0; i < clockdiv.length; i++) {
            countDownDate[i] = new Array();
            countDownDate[i]['el'] = clockdiv[i];
            countDownDate[i]['time'] = new Date(clockdiv[i].getAttribute('data-date')).getTime();
            // countDownDate[i]['days'] = 0;
            countDownDate[i]['hours'] = 0;
            countDownDate[i]['seconds'] = 0;
            countDownDate[i]['minutes'] = 0;
        }

        var countdownfunction = setInterval(function() {
          for (var i = 0; i < countDownDate.length; i++) {
                var now = new Date().getTime();
                var distance = countDownDate[i]['time'] - now;
                 // countDownDate[i]['days'] = Math.floor(distance / (1000 * 60 * 60 * 24));
                 countDownDate[i]['hours'] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                 countDownDate[i]['minutes'] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                 countDownDate[i]['seconds'] = Math.floor((distance % (1000 * 60)) / 1000);

                 if (distance < 0) {
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = 0;
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = 0;
                }else{
                  // countDownDate[i]['el'].querySelector('.days').innerHTML = countDownDate[i]['days'];
                  countDownDate[i]['el'].querySelector('.hours').innerHTML = (countDownDate[i]['hours'] < 10 ? '0' + countDownDate[i]['hours'] : countDownDate[i]['hours'] ) ;
                  countDownDate[i]['el'].querySelector('.minutes').innerHTML = (countDownDate[i]['minutes'] < 10 ? '0' + countDownDate[i]['minutes'] : countDownDate[i]['minutes'] );
                  countDownDate[i]['el'].querySelector('.seconds').innerHTML = (countDownDate[i]['seconds'] < 10 ? '0' + countDownDate[i]['seconds'] : countDownDate[i]['seconds'] );
                }
           }
        }, 1000);
    }
  });
</script>

<script>
  const proceedWA = (nomor, order) => {
      window.location =
          `https://api.whatsapp.com/send/?phone=${nomor}&text=Halo+Admin%2C+saya+mau+konfirmasi+pembayaran+orderan+Mystery+Box+12.12.%0D%0A%0D%0AOrder+ID%3A+${order}`;
  }
</script>
@endsection
