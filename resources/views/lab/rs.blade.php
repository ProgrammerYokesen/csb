@section('title')
    Quiz Rejeki Sobat
@endsection

@extends('dashboard.master')

@section('dash-content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-2 subheader-transparent dash_home_header" id="kt_subheader">
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid justify-content-center align-items-center">
            <!--begin::Container-->
            <div class="container">

                <div class="card card-custom gutter-b quiz_card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('homeDash') }}" class="btn">
                                <img src="{{ asset('images/icons/arrow-left.svg') }}" alt="back">
                            </a>
                        </div>
                    </div>

                    <div class="card-body">

                        <form action="{{ route('labProcessQuiz') }}" method="POST" onsubmit="disableBtn()">
                            <input type="hidden" name="quizId" value="{{ $datas[0]->quizId }}">
                            @csrf
                            <div class="quiz_rejeki">
                                @php $i = 1; @endphp
                                @foreach ($datas as $data)
                                    {{-- Question 1 --}}
                                    <div class="quiz_rejeki_quest">
                                        <h6>Pertanyaan {{ $i }}</h6>
                                        @php $i++; @endphp
                                        <h2>{{ $data->pertanyaan }}</h2>

                                        {{-- Hint Quiz --}}
                                        <p class="mb-5">Hint: {{ $data->link }}</p>

                                        <div class="form-group">
                                            <div class="radio-inline">
                                                <label class="radio radio-lg">
                                                    <input type="radio" value="A" name="{{ $data->pertanyaan_id }}" />
                                                    <span></span>
                                                    {{ $data->jawaban_a }}
                                                </label>
                                                <label class="radio radio-lg">
                                                    <input type="radio" value="B" name="{{ $data->pertanyaan_id }}" />
                                                    <span></span>
                                                    {{ $data->jawaban_b }}
                                                </label>
                                                <label class="radio radio-lg">
                                                    <input type="radio" value="C" name="{{ $data->pertanyaan_id }}" />
                                                    <span></span>
                                                    {{ $data->jawaban_c }}
                                                </label>
                                                <label class="radio radio-lg">
                                                    <input type="radio" value="D" name="{{ $data->pertanyaan_id }}" />
                                                    <span></span>
                                                    {{ $data->jawaban_d }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- End question --}}
                                @endforeach

                                <div class="quiz_rejeki_btn">
                                    <button type="submit" class="btn" id="btn_selesai">SELESAI</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <!--begin::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

@section('pageJS')
    <script>
        $(document).bind("contextmenu", function(e) {
            e.preventDefault();
        });

        $(document).keydown(function(e) {
            if (e.which === 123) {
                return false;
            } else if ((event.ctrlKey && event.shiftKey && event.keyCode == 73) || (event.ctrlKey && event
                    .shiftKey && event.keyCode == 74)) {
                return false;
            }
        });
    </script>

    <script>
        function disableBtn() {
            document.getElementById("btn_selesai").disabled = true;
        }
    </script>
@endsection
