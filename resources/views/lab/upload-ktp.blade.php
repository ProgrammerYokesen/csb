@extends('landing-page.master')

@section('v2_assets')
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('lab/ktp.css') }}">
@endsection

@section('content')
  <section id="form-ktp">
    <div class="container py-5">
      <form action="{{route('processKTP')}}" method="post" enctype='multipart/form-data'>
        @csrf
        <div class="row mt-5 align-items-center">
            <div class="col-md-2">
                <label class="fonts-16 fc-black opac-50" for="">NIK</label>
            </div>
            <div class="col-md-10">
                <div class="position-relative">
                    <input autocomplete="off" name="nik" placeholder="Masukan NIK" type="number"
                        class="form-control fontw-6">
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-2">
                <label class="fonts-16 fc-black opac-50" for="">Upload KTP</label>
            </div>
            <div class="col-md-10">
                <div class="d-sm-flex d-block">
                    <div class="d-flex align-items-center">
                        <input autocomplete="off" type="file" id="upload_ktp" name="ktp"
                            accept="image/jpeg, image/png" hidden />
                        <label for="upload_ktp" class="file__input btn__success1 mb-0 label__ktp"
                            style="color: #098b39">
                            Klik
                            di
                            sini
                            untuk
                            upload
                        </label>

                        <span id="ktp_name"></span>
                    </div>

                    <button class="btn btn__success1 ktp__button" style="border: none">
                        <div class="d-none spinner spinner-track spinner-light py-3"
                            style="margin: 0 3rem 0 2rem;"></div>
                        <div class="text__btn">Konfirmasi Sekarang</div>
                    </button>

                    <!-- @if ($profile->ktp)
                        @if ($profile->ktp_verification == 0)
                            <div
                                class="d-flex align-items-center fc-warning p-3 bg-warning-o-20 border-xs profile__warning">
                                Menunggu Verifikasi
                            </div>
                        @elseif($profile->ktp_verification == 1)
                            <div class="btn terverifikasi" disabled style="width: unset!important">
                                <span>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.99992 14.4173C11.5437 14.4173 14.4166 11.5445 14.4166 8.00065C14.4166 4.45682 11.5437 1.58398 7.99992 1.58398C4.45609 1.58398 1.58325 4.45682 1.58325 8.00065C1.58325 11.5445 4.45609 14.4173 7.99992 14.4173Z"
                                            stroke="#1AD598" stroke-width="2" stroke-linejoin="round" />
                                        <path
                                            d="M10.8188 5.08418C10.7321 5.08745 10.647 5.10832 10.5686 5.14542C10.4902 5.18251 10.4201 5.23514 10.3626 5.30008C9.31261 6.4335 8.35478 7.54532 7.34678 8.65482L6.16203 7.63113C6.09538 7.57267 6.01717 7.52865 5.93249 7.50239C5.84781 7.47613 5.75855 7.46824 5.67052 7.47875C5.58249 7.48926 5.49765 7.51818 5.42152 7.56363C5.3454 7.60908 5.2797 7.66999 5.22869 7.7425C5.11839 7.89308 5.06788 8.0792 5.08696 8.26488C5.10603 8.45056 5.19333 8.62238 5.33195 8.74739L6.99037 10.1772C7.11556 10.2864 7.27885 10.3414 7.44457 10.3305C7.61029 10.3195 7.76494 10.2435 7.8747 10.1188C9.06762 8.83085 10.1363 7.5658 11.3018 6.30639C11.428 6.1688 11.4988 5.98923 11.5004 5.80252C11.502 5.61581 11.4344 5.43498 11.3105 5.29524C11.2486 5.22672 11.1726 5.17246 11.0877 5.13602C11.0028 5.09957 10.9111 5.08191 10.8188 5.08418Z"
                                            fill="#1AD598" />
                                    </svg>
                                </span> Terverifikasi
                            </div>
                        @else
                            <div class="d-flex align-items-center p-3 bg-danger-o-50 border-xs profile__warning">
                                <span>
                                    <svg width="26" height="24" viewBox="0 0 26 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M14.6487 1.98877L24.7332 19.9888C25.5054 21.367 24.5106 23 23.0845 23H2.91549C1.48937 23 0.494608 21.367 1.26678 19.9888L11.3513 1.98877C12.0899 0.67041 13.9101 0.67041 14.6487 1.98877Z"
                                            stroke="#DF0000" stroke-width="2" />
                                        <path
                                            d="M13.5 19.7494C14.0799 19.7494 14.55 19.2793 14.55 18.6994C14.55 18.1195 14.0799 17.6494 13.5 17.6494C12.9201 17.6494 12.45 18.1195 12.45 18.6994C12.45 19.2793 12.9201 19.7494 13.5 19.7494Z"
                                            fill="#DF0000" />
                                        <path
                                            d="M13.5 9.25C13.2215 9.25 12.9545 9.36062 12.7576 9.55754C12.5606 9.75445 12.45 10.0215 12.45 10.3V15.55C12.45 15.8285 12.5606 16.0955 12.7576 16.2925C12.9545 16.4894 13.2215 16.6 13.5 16.6C13.7785 16.6 14.0456 16.4894 14.2425 16.2925C14.4394 16.0955 14.55 15.8285 14.55 15.55V10.3C14.55 10.0215 14.4394 9.75445 14.2425 9.55754C14.0456 9.36062 13.7785 9.25 13.5 9.25Z"
                                            fill="#DF0000" />
                                    </svg>
                                </span>
                                <p class="mb-0 ml-2" style="color: #DF0000">
                                    KTP anda tidak terverifikasi. Silahkan upload ulang.
                                </p>
                            </div>
                        @endif
                    @else
                        <button class="btn btn__success1 ktp__button" style="border: none">
                            <div class="d-none spinner spinner-track spinner-light py-3"
                                style="margin: 0 3rem 0 2rem;"></div>
                            <div class="text__btn">Konfirmasi Sekarang</div>
                        </button>
                    @endif -->
                </div>
                <small id="error__ktp" class="d-none text-danger"><strong>File harus berekstensi jpg, jpeg, atau
                        png!</strong><br></small>

            </div>
        </div>
      </form>
    </div>
  </section>
@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    @if (Session::has('errorMsg'))
        <script>
            Swal.fire("Gagal!", "{{ Session::get('errorMsg') }}", "error");
        </script>
    @endif
    @if (Session::has('successMsg'))
        <script>
            Swal.fire("{{ Session::get('successMsg') }}", "", "success");
        </script>
    @endif

    <script>
      AOS.init();

      function downloadPDF() {
        let filePDF = 'https://sobatbadak.club/uploads/download/csb_mewarnai.pdf'
        window.open(filePDF, '_blank');
      }

      function regexImage(type) {
        return type.includes('jpg') || type.includes('jpeg') || type.includes('png')
      }

      $(function(){
        // handle change image
        $('#upload_ktp').on('change', function(event) {
            if (!regexImage(event.target.files[0].type)) {
                $('#error__ktp').removeClass('d-none')
            } else {
                if (!$('#error__ktp').hasClass('d-none')) {
                    $('#error__ktp').addClass('d-none')
                }

                $('#ktp_name').text(event.target.files[0].name)
            }
        })
      })
    </script>
@endsection
