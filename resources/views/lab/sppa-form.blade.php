@section('title')
    Pilih Kategori
@endsection

@extends('dashboard.master')

@section('page_assets')
    <link rel="stylesheet" href="{{ asset('user-dashboard/sppa.css') }}">

@endsection

@section('dash-content')
    <div class="content d-flex flex-column flex-column-fluid " id="kt_content">
        <div class="subheader py-2 py-lg-12 subheader-transparent dash_home_header" id="kt_subheader">
        </div>

        <div class="d-flex flex-column-fluid">
            <div class="container white-container">
                <div class="d-flex justify-content-start">
                    <a href="javascript:history.back()">
                        <i class="fas fa-arrow-left back-icon"></i>
                    </a>

                </div>
                <div class="d-flex align-items-center mtop-50px">

                    <form class="w-100" id="form_daftar" action="{{route('postSppa')}}" method="POST">
                        @csrf
                        <h3 class="title-part left">Silahkan masukkan link video kamu disini</h3>
                        <p class="exp-part left mb-5">Video harus di upload di tiktok dan akun tidak boleh di private</p>
                        <input type="text" class="form-control" name="link" id="linkInput" placeholder="Masukan link anda disini!">
                        <div class="d-none" style="font-size: 12px; font-weight: 500;color: red" id="error_link_tiktok">
                          Mohon mengisi link tiktok yang valid sebelum mengirim!
                        </div>

                        <input type="text" hidden name="cat_id" id="kategoriInput">
                    </form>



                </div>

                <div class="mtop-50px w-100 mb-8 ">
                    <h3 class="title-part left">Pilih Kategori</h3>
                    <p class="exp-part left mb-5">Silahkan memilih salah satu kategori di bawah ini</p>

                    <div class="row justify-content-center ">

                      @foreach($categories as $category)
                        @component('lab.components.kategori-box')
                            @slot('id')
                                {{$category->id}}
                            @endslot
                            @slot('asset')
                                https://data.sobatbadak.club/{{$category->photo}}
                            @endslot
                            @slot('title')
                                {{$category->category_name}}
                            @endslot
                        @endcomponent
                      @endforeach

                    </div>

                    <div class="d-flex justify-content-center mt-10">
                        <button class="btn btn-yellow" onclick="daftarValidation()" >DAFTAR</button>
                    </div>
                </div>
                {{-- BODY DISINI --}}
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUploadWarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <h3 class="title-part mb-5">Konfirmasi Upload</h3>
                    <p class="exp-part" style="font-weight: 600"><span style="color: red">Peserta yang telah
                            mengupload tidak dapat melakukan upload ulang. </span>Pastikan video atau foto yang Anda upload
                        sudah sesuai dengan Syarat dan Ketentuan.</p>
                    <div class="row mt-7">
                        <div class="col-6">
                            <button class="btn btn-yellow-outline w-100" data-dismiss="modal">Batalkan</button>


                        </div>
                        <div class="col-6">
                            <button class="btn btn-yellow w-100"  id="btnWarningOK"
                                onclick="clickSubmit()">OK!</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('pageJS')

    {{-- HTML EDITOR --}}
    <script>
        $(function() {
            $('#btnWarningOK').click(function() {
                $('#modalApproval').modal('show');
            });
        })
    </script>
    <script>
      //VALIDATION PROCESS HERE
        function daftarValidation(){
          var link = $('#linkInput').val();

          var kategori = $('#kategoriInput').val();

          // If link is not tiktok
          if (link.length < 5 || !(link.includes('tiktok')) ) {
              errorLink = true;
              //Swal.fire('Link Tidak Valid', 'Mohon mengisi link tiktok yang valid sebelum mengirim!', 'error' );
              $('#error_link_tiktok').removeClass('d-none');
          } else {
              errorLink = false;
              $('#error_link_tiktok').addClass('d-none');
          }

          // If kategori has not been choosen
          if(kategori === undefined || kategori == null ||kategori  == ""){
              errorKategori = true;
              Swal.fire('Kategori belum dipilih', 'Mohon memilih kategori sebelum mengirim data', 'error' );

          } else {
              errorKategori = false;
          }

          if(!errorKategori && !errorLink){
            $('#modalUploadWarning').modal('show');
          }
        }
    </script>
    <script>
        var errorLink = false;
        var errorKategori = false;

        function clickKategori(id) {
            $('.kategori-box').removeClass('active');

            $('#' + id).addClass('active');
            $('#kategoriInput').val(id);

        }
        function clickSubmit(){

            $('#modalUploadWarning').modal('hide');

            $('#form_daftar').submit();

        }
        // $(document).ready(function() {
        //     $('#form_daftar').on('submit', function(e) {
        //     });
        // });
    </script>
@endsection
