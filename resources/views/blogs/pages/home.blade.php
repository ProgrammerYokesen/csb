@section('title')
    Auction
@endsection

@extends('blogs.master')

@section('content')
    <section class="container">
        <div class="title_black section-title">
            Majalah Dinding Sobat Badak
        </div>

        @if ($blogs)
            <div class="list__blogs row">
                @foreach ($blogs as $item)
                    @component('blogs.components.blog__card')
                        @slot('blogID') {{ $item->id }} @endslot
                        @slot('image') {{ $item->image }} @endslot
                        @slot('title') {{ fillter($item->title) }} @endslot
                        @slot('body') {!! fillter($item->body) !!} @endslot
                        @slot('likes') {{ $item->likes }} @endslot
                        @slot('likeStatus') {{ $item->likeStatus }} @endslot
                        @slot('date') {{ $item->created_at }} @endslot
                    @endcomponent
                @endforeach
            </div>
        @else
            <div class="text-center">
                <img src="/images/baper_kosong.png" alt="">
                <p class="black75-normal-18">
                    Masih belum ada artikel nih di Majalah Dinding. <br>
                    Ayo buat artikel melalui <b>menu dashboard</b> <br>
                    dan dapatkan baper poin hingga 50.000!
                </p>
            </div>
        @endif
        <div class="title_inner_shadow" style="margin: 6rem 0 2rem 0">
            Ayo ikutan nulis juga bareng <br> Teman Sobat lainnya!
        </div>
        <div class="d-flex justify-content-center" style="margin-bottom: 10rem">
            <a href="{{ route('registerPage') }}" class="btn btn-warning custom-btn-primary">Bergabung Sekarang</a>
        </div>
    </section>
@endsection


@section('pageJS')
    <script>
        // AJAX handle like signal
        function handleLike(blogID) {
            var like = parseInt($("#total-like-" + blogID).text())
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                url: "{{ route('blogLike') }}",
                type: "POST",
                data: {
                    blog_id: blogID
                },
                success: function(res) {
                    console.log(res)

                    if (res.action.includes("dislike")) {
                        like = like - 1
                        $("#like_icon" + blogID).empty();
                        $("#like_icon" + blogID).append(`
                        <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.5"
                                    d="M17.0761 2.45098C16.6504 2.02472 16.1451 1.68659 15.5889 1.45589C15.0326 1.22519 14.4365 1.10645 13.8344 1.10645C13.2323 1.10645 12.6362 1.22519 12.0799 1.45589C11.5237 1.68659 11.0184 2.02472 10.5927 2.45098L9.7094 3.3352L8.82607 2.45098C7.96633 1.59038 6.80027 1.10689 5.58441 1.10689C4.36855 1.10689 3.20248 1.59038 2.34274 2.45098C1.483 3.31159 1 4.47882 1 5.6959C1 6.91298 1.483 8.08021 2.34274 8.94082L3.22607 9.82504L9.7094 16.3149L16.1927 9.82504L17.0761 8.94082C17.5019 8.51476 17.8397 8.0089 18.0702 7.45212C18.3006 6.89535 18.4193 6.29858 18.4193 5.6959C18.4193 5.09322 18.3006 4.49645 18.0702 3.93968C17.8397 3.3829 17.5019 2.87704 17.0761 2.45098V2.45098Z"
                                    stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        `);
                    } else {
                        like = like + 1
                        $("#like_icon" + blogID).empty();
                        $("#like_icon" + blogID).append(`
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9 1.40165C13.9929 -3.46447 26.4762 5.0507 9 16C-8.47622 5.05177 4.00711 -3.46447 9 1.40165Z" fill="#FFAA3A"/>
                        </svg>
                        `);
                    }

                    $("#total-like-" + blogID).text(like)
                },
                error: function(err) {
                    console.log("ERROR")
                    console.log(err)
                }
            })
        }
    </script>
@endsection
