@section('title')
    Auction
@endsection

@extends('blogs.master')

@section('name')
    <link rel="stylesheet" href="{{ asset('assets/gs/good-share.min.css') }}">
@endsection

@section('content')
    <div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
        <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5" kt-hidden-height="40" style="">
            <h3 class="font-weight-bold m-0">Comment
            </h3>
            <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-warning" id="kt_quick_user_close">
                <i class="ki ki-close icon-xs text-muted"></i>
            </a>
        </div>
        <div class="offcanvas-content pr-5 mr-n5 scroll ps ps--active-y" style="height: 766px; overflow: hidden;">
            @if (Auth::check())
                <textarea placeholder="Apa pendapatmu?" id="autoresizing" name="comment"
                    class="form-control reply-comment-section"></textarea>
            @else
                <a href="{{ route('loginPage') }}" class="btn primary__btn_blog w-100 mt-4">Silahkan login untuk
                    memberikan
                    komentar</a>
            @endif
            <div class="comment__list mt-3">
                @foreach ($comments as $item)
                    <div class="comment__item">
                        <div class="d-flex" style="align-items: center">
                            <img src="{{ asset($item->avatars) }}" alt="" class="avatar__user">
                            <p class="user__name">{{ $item->name }}</p>
                        </div>
                        <div class="user__comment" style="margin-top: .5rem">
                            {{ $item->comment }}
                        </div>
                    </div>
                @endforeach
                <center id="empty_comment" style="margin-top: 3rem">
                    Belum ada komentar, <br> jadilah yang pertama!
                </center>
            </div>
            <a id="load_more" onclick="loadComment({{ $blog->id }})" class="btn primary__btn_blog w-100 mt-4">Muat
                selebihnya</a>
        </div>
    </div>
    <section class="container">
        <div class="blog_detail__title black-normal-48 text-center">
            {{ fillter($blog->title) }}
        </div>
        <div class="blog__head_img">
            <img width="100%" src="{{ asset($blog->image) }}" alt="unknown">
        </div>
        <div class="blog__content black-normal-16">
            {!! fillter($blog->body) !!}
            <div class="blog__author d-flex justify-content-between" style="align-items: center">
                <div class="black-bold-16">
                    Karya: {{ $blog->name }}
                </div>
                <div class="d-flex">
                    <div id="like_icon{{ $blog->id }}" onclick="handleLike({{ $blog->id }})">
                        @if ($blog->likeStatus == 1)
                            <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M9 1.40165C13.9929 -3.46447 26.4762 5.0507 9 16C-8.47622 5.05177 4.00711 -3.46447 9 1.40165Z"
                                    fill="#FFAA3A" />
                            </svg>
                        @else
                            <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.5"
                                    d="M17.0761 2.45098C16.6504 2.02472 16.1451 1.68659 15.5889 1.45589C15.0326 1.22519 14.4365 1.10645 13.8344 1.10645C13.2323 1.10645 12.6362 1.22519 12.0799 1.45589C11.5237 1.68659 11.0184 2.02472 10.5927 2.45098L9.7094 3.3352L8.82607 2.45098C7.96633 1.59038 6.80027 1.10689 5.58441 1.10689C4.36855 1.10689 3.20248 1.59038 2.34274 2.45098C1.483 3.31159 1 4.47882 1 5.6959C1 6.91298 1.483 8.08021 2.34274 8.94082L3.22607 9.82504L9.7094 16.3149L16.1927 9.82504L17.0761 8.94082C17.5019 8.51476 17.8397 8.0089 18.0702 7.45212C18.3006 6.89535 18.4193 6.29858 18.4193 5.6959C18.4193 5.09322 18.3006 4.49645 18.0702 3.93968C17.8397 3.3829 17.5019 2.87704 17.0761 2.45098V2.45098Z"
                                    stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        @endif
                    </div>
                    <span id="total-like-{{ $blog->id }}" class="ml-1 mr-4">{{ $blog->likes }}</span>
                    <div style="cursor: pointer" class="" id="kt_quick_user_toggle" {{-- data-toggle="modal" data-target="#commentModal" --}}>
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g opacity="0.5">
                                <path
                                    d="M17.5 12.5C17.5 12.942 17.3244 13.366 17.0118 13.6785C16.6993 13.9911 16.2754 14.1667 15.8333 14.1667H5.83333L2.5 17.5V4.16667C2.5 3.72464 2.67559 3.30072 2.98816 2.98816C3.30072 2.67559 3.72464 2.5 4.16667 2.5H15.8333C16.2754 2.5 16.6993 2.67559 17.0118 2.98816C17.3244 3.30072 17.5 3.72464 17.5 4.16667V12.5Z"
                                    stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </g>
                        </svg>
                    </div>
                    <span id="total-comment-{{ $blog->id }}" class="ml-1 mr-4">{{ $totalComments }}</span>
                    {{-- <button class="good-share" data-share-title="{{ $blog->title }}" data-share-url="{{route('detailBlogPage', $blog->id)}}">Bagikan</button> --}}
                    <div class="dropdown dropdown-inline mr-4">
                        <i class="fas fa-share good-share" data-share-text="" data-share-title="{{ $blog->title }}"
                            data-share-url="{{ route('detailBlogPage', $blog->id) }}" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false" style="cursor: pointer"></i>
                        <div class="dropdown-menu">
                            <a class="dropdown-item btn twitter-btn">Twitter</a>
                            <a class="dropdown-item btn facebook-btn">Facebook</a>
                            <a class="dropdown-item" tabindex="0" role="button" data-toggle="tooltip" data-trigger="click"
                                title="Copied!" onclick="copyUrl()">Link</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="title_inner_shadow" style="margin: 6rem 0 2rem 0">
            Lihat Karya Lainnya
        </div>
        <div class="list__blogs row">
            @foreach ($blogs as $item)
                @component('blogs.components.blog__card')
                    @slot('blogID') {{ $item->id }} @endslot
                    @slot('image') {{ $item->image }} @endslot
                    @slot('title') {{ $item->title }} @endslot
                    @slot('body') {!! $item->body !!} @endslot
                    @slot('likes') {{ $item->likes }} @endslot
                    @slot('likeStatus') {{ $item->likeStatus }} @endslot
                    @slot('date') {{ $item->created_at }} @endslot
                @endcomponent
            @endforeach
        </div>
        <div class="title_inner_shadow" style="margin: 6rem 0 2rem 0">
            Ayo ikutan nulis juga bareng <br> Teman Sobat lainnya!
        </div>
    </section>
@endsection


@section('pageJS')
    <script src="{{ asset('assets/gs/good-share.min.js') }}"></script>
    @if (Auth::check())
        <script>
            var base_url = window.location.origin;
            // handle to auto resize text area
            document.querySelector("#autoresizing").addEventListener(
                'input', autoResize, false);

            function autoResize() {
                this.style.height = 'auto';
                this.style.height = this.scrollHeight + 'px';
            }

            // handle when user press enter
            $("#autoresizing").keydown(function(e) {
                if (e.which == 13 && !e.shiftKey) {
                    handleComment({{ $blog->id }})
                    console.log("submit")
                }
            });

            // handle comment
            function handleComment(blogID) {
                let comment = $("#autoresizing").val()
                let totalComment = parseInt($("#total-comment-" + blogID).text())
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                $.ajax({
                    url: "{{ route('blogComment') }}",
                    type: "POST",
                    data: {
                        blog_id: blogID,
                        user_id: {{ Auth::user()->id }},
                        comment: comment,
                    },
                    success: function(res) {
                        console.log(res)

                        if (res.status.includes("success")) {
                            $('#empty_comment').hide()

                            let ava = `${base_url}/${res.avatar}`

                            $(".comment__list").append(`
                            <div class="comment__item">
                                <div class="d-flex" style="align-items: center">
                                    <img src="${ava}"
                                        alt="" class="avatar__user">
                                    <p class="user__name">${res.name}</p>
                                </div>
                                <div class="user__comment" style="margin-top: .5rem">
                                    ${res.comment}
                                </div>
                            </div>
                            `);

                            totalComment++

                            $("#autoresizing").val('');
                        }

                        $("#total-comment-" + blogID).text(totalComment)
                    },
                    error: function(err) {
                        console.log("ERROR")
                        console.log(err)
                    }
                })
            }
        </script>
    @endif
    
    {{-- show empty comment components if comments = 0 --}}
    @if ($totalComments == 0)
        <script>
            $('#empty_comment').show()
        </script>
    {{-- hide empty comment components if comments > 0 --}}
    @else
        <script>
            $('#empty_comment').hide()
        </script>
    @endif
    
    {{-- hide button load more if comments < 7 --}}
    @if ($totalComments < 7)
        <script>
            $('#load_more').hide()
        </script>
    @endif

    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'focus'
            })
        });

        var base_url = window.location.origin;
        var load_comment_url = base_url + "/load-comment?page=" + 2

        //copy link
        function copyUrl() {
            let url = window.location.href;
            navigator.clipboard.writeText(url);
        }

        function handleLike(blogID) {
            var like = parseInt($("#total-like-" + blogID).text())
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                url: "{{ route('blogLike') }}",
                type: "POST",
                data: {
                    blog_id: blogID
                },
                success: function(res) { 

                    if (res.action.includes("dislike")) {
                        like = like - 1
                        $("#like_icon" + blogID).empty();
                        $("#like_icon" + blogID).append(`
                        <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.5"
                                    d="M17.0761 2.45098C16.6504 2.02472 16.1451 1.68659 15.5889 1.45589C15.0326 1.22519 14.4365 1.10645 13.8344 1.10645C13.2323 1.10645 12.6362 1.22519 12.0799 1.45589C11.5237 1.68659 11.0184 2.02472 10.5927 2.45098L9.7094 3.3352L8.82607 2.45098C7.96633 1.59038 6.80027 1.10689 5.58441 1.10689C4.36855 1.10689 3.20248 1.59038 2.34274 2.45098C1.483 3.31159 1 4.47882 1 5.6959C1 6.91298 1.483 8.08021 2.34274 8.94082L3.22607 9.82504L9.7094 16.3149L16.1927 9.82504L17.0761 8.94082C17.5019 8.51476 17.8397 8.0089 18.0702 7.45212C18.3006 6.89535 18.4193 6.29858 18.4193 5.6959C18.4193 5.09322 18.3006 4.49645 18.0702 3.93968C17.8397 3.3829 17.5019 2.87704 17.0761 2.45098V2.45098Z"
                                    stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        `);
                    } else {
                        like = like + 1
                        $("#like_icon" + blogID).empty();
                        $("#like_icon" + blogID).append(`
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9 1.40165C13.9929 -3.46447 26.4762 5.0507 9 16C-8.47622 5.05177 4.00711 -3.46447 9 1.40165Z" fill="#FFAA3A"/>
                        </svg>
                        `);
                    }

                    $("#total-like-" + blogID).text(like)
                },
                error: function(err) {
                    console.log("ERROR")
                    console.log(err)
                }
            })
        }

        function loadComment(blogID) {
            let comment = $("#autoresizing").val()
            let totalComment = parseInt($("#total-comment-" + blogID).text())
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                url: load_comment_url,
                type: "POST",
                data: {
                    blog_id: blogID,
                },
                success: function(res) {

                    if (res.status.includes("success")) {
                        // if the last page, hide the load more button
                        if (res.data.next_page_url == null) {
                            $("#load_more").hide()
                        }

                        // if note change the url into the next page
                        load_comment_url = res.data.next_page_url

                        $(".comment__list").append(
                            res.data.data.map((item) => {
                                let ava = `${base_url}/${item.avatars}`
                                return `
                            <div class="comment__item">
                                <div class="d-flex" style="align-items: center">
                                    <img src="${ava}"
                                        alt="" class="avatar__user">
                                    <p class="user__name">${item.name}</p>
                                </div>
                                <div class="user__comment" style="margin-top: .5rem">
                                    ${item.comment}
                                </div>
                            </div>
                            `
                            })
                        )
                    }
                },
                error: function(err) {
                    console.log("ERROR")
                    console.log(err)
                }
            })
        }
    </script>
@endsection
