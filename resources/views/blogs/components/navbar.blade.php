<div id="kt_header" class="header header-fixed">
    <div class="container d-flex align-items-stretch justify-content-between">
        <div class="d-flex align-items-stretch mr-3">
            <div class="header-logo">
                <a href="/">
                    <img alt="Logo" src="{{ asset('images/csb-logo.png') }}" class="logo-default max-h-50px" />
                    <img alt="Logo" src="{{ asset('images/csb-logo.png') }}" class="logo-sticky max-h-50px">
                </a>
            </div>
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <div id="kt_header_menu"
                    class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
                    <ul class="nav nav-pills custom-list-navbar" id="custom-navbar" role="tablist">
                        <li class="nav-item d-flex list-hidden-nav">
                            <div class="black-bold-32">Club Sobat Badak</div>
                        </li>
                        <li class="nav-item d-flex list-hidden-nav">
                            <a href="{{ route('omaru') }}" class="btn omaru-btn"
                                style="display: inline-flex;align-items:center">Om Badak</a>
                        </li>
                        @if (Auth::user())
                            <li class="nav-item d-flex list-hidden-nav">
                                <a href="{{ route('homeDash') }}" class="btn custom-btn-primary d-flex"
                                    style="align-items: center; padding:6.5px">
                                    <img class="avatar-login"
                                        src="{{ asset(Auth::user()->avatars == null ? 'images/badak-baper.png' : Auth::user()->avatars) }}"
                                        alt="" srcset="">
                                        @php 
                                        $arr = explode(' ',trim(Auth::user()->name));
                                        @endphp
                                    <div class="ml-2">{{ $arr[0] }}</div>
                                </a>
                            </li>
                            <li class="nav-item d-flex list-hidden-nav">
                                <a href="{{ route('logout') }}" class="btn logout-button">
                                    <img src="/images/icons/log-out.svg" alt="">
                                    <div class="ml-2">Keluar</div>
                                </a>
                            </li>
                        @else
                            <li class="nav-item d-flex list-hidden-nav">
                                <div class="custom-navbar-button">
                                    <a href="{{ route('loginPage') }}"
                                        class="btn btn-warning custom-btn-primary">Masuk</a>
                                </div>

                            </li>
                            <li class="nav-item d-flex list-hidden-nav">

                                <div class="custom-navbar-button">
                                    <a href="{{ route('registerPage') }}"
                                        class="btn custom-btn-primary-outline">Daftar</a>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="custom-navbar-button" id="custom-nav-btn">
            <a href="{{ route('omaru') }}" class="btn omaru-btn mr-4 d-flex ">
                <div>Om Badak</div>
            </a>
            @if (Auth::user())
                <a href="{{ route('homeDash') }}" class="btn custom-btn-primary d-flex justify-content-center"
                    style="align-items: center; padding:6.5px">
                    <img class="avatar-login mr-2"
                        src="{{ asset(Auth::user()->avatars == null ? 'images/badak-baper.png' : Auth::user()->avatars) }}"
                        alt="">
                    @php 
                    $arr = explode(' ',trim(Auth::user()->name));
                    @endphp
                    <div class="ml-2">{{ $arr[0] }}</div>
                </a>
                <a href="{{ route('logout') }}" class="ml-3 btn logout-button">
                    <img src="/images/icons/log-out.svg" alt="">
                    <div class="ml-2">Keluar</div>
                </a>
            @else
                <a href="/login" class="btn btn-warning custom-btn-primary">Masuk</a>
                <a href="/register" class="ml-4 btn custom-btn-primary-outline">Daftar</a>
            @endif

        </div>
    </div>
</div>

<script>
    function moveTo(url) {
        window.location.href = url;
    }
</script>
