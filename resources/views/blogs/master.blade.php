<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak</title>
    @laravelPWA
    <meta name="description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta property="og:title" content="Club Sobat Badak" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://sobatbadak.club" />
    <meta property="og:image" content="https://sobatbadak.club/images/share.png" />
    <meta property="og:description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('blog/style.css') }}"> 
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '806244510020586');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199518915-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199518915-2');
    </script>


</head>

<body id="kt_body" style="background-image: url(assets/media/bg/bg-10.jpg)"
    class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">
    <div id="kt_header_mobile" class="header-mobile">
        <a href="/">
            <img alt="Logo" src="{{ asset('images/csb-logo.png') }}" class="logo-default max-h-40px" />
        </a>
        <div class="d-flex align-items-center">
            <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
                <img src="/images/icons/burger-btn.svg" alt="">
            </button>
        </div>
    </div>
    <div id="header-desktop" class="flex-column flex-root">
        <div class="d-flex flex-row flex-column-fluid page">
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                @include('blogs.components.navbar')
                @yield('content')
                @include('blogs.components.footer')
            </div>
        </div>
    </div>

    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=806244510020586&ev=PageView&noscript=1" /></noscript>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script> 
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script> 

    @yield('pageJS')

</body> 

</html>
