@extends('dashboard.master')

@section('dash-content')
    <nav id="nav-tool" class="navbar" style="border: 1px solid red;">
        <div class="container">
            {{-- <div class="navbar-header">
            <a class="navbar-brand" href="#">Test</a>
        </div> --}}
            <div id="navbar" class="websdktest">
                <form class="form" id="meeting_form">
                    <div class="form-group">
                        <input type="text" name="display_name" id="display_name" value="Test-Embed" maxLength="100"
                            placeholder="Name" class="form-control d-none" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="meeting_number" id="meeting_number" value="8013581311" maxLength="200"
                            style="width:150px" placeholder="Meeting Number" class="form-control d-none">
                    </div>
                    <div class="form-group">
                        <input type="text" name="meeting_pwd" id="meeting_pwd" value="dTlaYXcwYUxYa085eG1NUDZzK1RyQT09"
                            style="width:150px" maxLength="32" placeholder="Meeting Password" class="form-control d-none">
                    </div>
                    <div class="form-group">
                        <input type="text" name="meeting_email" id="meeting_email" value="{{ $data['email'] }}"
                            style="width:150px" maxLength="32" placeholder="Email option" class="form-control d-none">
                    </div>
                    <div class="form-group">
                        <select id="meeting_role" class="sdk-select text d-none">
                            <option value=0>Attendee</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="meeting_china" class="sdk-select text d-none">
                            <option value=0>Global</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="meeting_lang" class="sdk-select text d-none">
                            <option value="en-US">English</option>
                        </select>
                    </div>

                    {{-- <input type="text" value="" id="copy_link_value" /> --}}
                    @csrf
                    <button type="submit" class="btn btn-primary" id="join_meeting">Join</button>
                    <button type="submit" class="btn btn-primary d-none" id="clear_all">Clear</button>
                    <button type="button" link="" onclick="window.copyJoinLink('#copy_join_link')"
                        class="btn btn-primary d-none" id="copy_join_link">Copy Direct join link</button>
                </form>
            </div>
        </div>
    </nav>
@endsection

@section('pageJS')
    <script src="https://source.zoom.us/1.9.6/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/1.9.6/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/1.9.6/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/1.9.6/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/1.9.6/lib/vendor/lodash.min.js"></script>
    <script src="https://source.zoom.us/zoom-meeting-1.9.6.min.js"></script>

    <script src="{{ url('/') }}/zoom/js/tool.js"></script>
    <script src="{{ url('/') }}/zoom/js/vconsole.min.js"></script>
    <script src="{{ url('/') }}/zoom/js/index.js"></script>
@endsection
