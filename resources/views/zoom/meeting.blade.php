<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.9.6/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.9.6/css/react-select.css" />
    <meta http-equiv="origin-trial" content="">
    <style media="screen">
        #zmmtg-root {
            width: 40% !important;
            height: 50% !important;
        }

    </style>
</head>

<body>

</body>

<script src="https://source.zoom.us/1.9.6/lib/vendor/react.min.js"></script>
<script src="https://source.zoom.us/1.9.6/lib/vendor/react-dom.min.js"></script>
<script src="https://source.zoom.us/1.9.6/lib/vendor/redux.min.js"></script>
<script src="https://source.zoom.us/1.9.6/lib/vendor/redux-thunk.min.js"></script>
<script src="https://source.zoom.us/1.9.6/lib/vendor/lodash.min.js"></script>
<script src="https://source.zoom.us/zoom-meeting-1.9.6.min.js"></script>
<script src="{{ url('/') }}/zoom/js/tool.js"></script>
<script src="{{ url('/') }}/zoom/js/vconsole.min.js"></script>
<script src="{{ url('/') }}/zoom/js/meeting.js"></script>

</html>
