@extends('errors.main')

@section('content')
    <section class="body d-flex justify-content-center">
        <div class="text-center custom-card">
            <img src="/images/kucing-baba.png" alt="">
            <h1 class="error-code">405</h1>
            <h1 class="error-description">Coba cek lagi, kebalik ini method requestnya</h1>
            <h1 class="error-description">Benerin tuh, babe capek!</h1>
        </div>
    </section>
@endsection
