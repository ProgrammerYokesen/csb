@extends('errors.main')

@section('content')
    <section class="body d-flex justify-content-center">
        <div class="text-center custom-card">
            <img src="/images/badak-baper.png" alt="">
            <h1 class="error-code">419</h1>
            <h1 class="error-description">Sorry, coba cek tokennya lagi soalnya</h1>
            <h1 class="error-description">tokennya invalid</h1>
        </div>
    </section>
@endsection
