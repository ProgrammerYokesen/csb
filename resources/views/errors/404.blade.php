@extends('errors.main')

@section('content')
    <section class="body d-flex justify-content-center">
        <div class="text-center custom-card">
            <img src="/images/badak-mama.png" alt="">
            <h1 class="error-code">404</h1>
            <h1 class="error-description">Oops, halaman yang kamu maksud tidak ada sayang!</h1>
            <h1 class="error-description">sini <a class="error-link" href="/">kembali</a></h1>
        </div>
    </section>
@endsection
