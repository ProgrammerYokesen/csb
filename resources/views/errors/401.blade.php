@extends('errors.main')

@section('content')
    <section class="body d-flex justify-content-center">
        <div class="text-center custom-card">
            <img src="/images/bang-jago.png" alt="">
            <h1 class="error-code">401</h1>
            <h1 class="error-description">Oi lau sape ? gaada wewenang nih buat akses.</h1>
            <h1 class="error-description text-primary">(Unauthorized)</h1>
        </div>
    </section>
@endsection
