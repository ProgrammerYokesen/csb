@extends('errors.main')

@section('content')
    <section class="body d-flex justify-content-center">
        <div class="text-center custom-card">
            <img src="/images/jamet-kuproy.png" alt="">
            <h1 class="error-code">429</h1>
            <h1 class="error-description">Requestnya kebanyakan woi, sabar atuh </h1>
            <h1 class="error-description">Sambil nunggu, joget dulu ama saya</h1>
        </div>
    </section>
@endsection
