@extends('landing-page.quiz-bunda.module')

@section('v2_assets')
    <link rel="stylesheet" href="{{ asset('landing-page/quiz.css') }}">
@endsection

@section('content')

    <div class="d-flex flex-column-fluid justify-content-center align-items-center my-10">
        <!--begin::Container-->
        <div class="container">

            <div class="modul_fail">
                <div class="quiz_csb__logo">
                    <img src="{{ asset('images/csb-logo-big.png') }}" alt="" class="csb__logo">
                </div>
                <div class="card">
                    <div class="card-body text-center" style="position: relative">
                        <img src="{{ asset('images/badak-mama.png') }}" alt="" class="img-fluid">
                        <h3>Nilai Bunda: <span>{{ $hasil->nilai }}</span></h3>
                        <h6>Wah, Bunda sudah menyelesaikan kuis hari ini! <br> Semoga jawabannya membawa berkah ya, Bunda!
                            Jangan
                            lupa ikuti kuis besok! Semoga beruntung, bunda~</h6>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>


@endsection
