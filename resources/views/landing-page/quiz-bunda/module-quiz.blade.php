@extends('landing-page.quiz-bunda.module')

@section('v2_assets')
    <link rel="stylesheet" href="{{ asset('landing-page/quiz.css') }}">
@endsection

@section('content')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center my-10">
        <!--begin::Container-->
        <div class="container">

            {{-- <div class="text-center"> --}}
            <div class="quiz_csb__logo">
                <img src="{{ asset('images/csb-logo-big.png') }}" alt="">
            </div>
            {{-- </div> --}}

            <div class="quiz__bunda">
                {{-- foreach --}}
                <form action="{{ route('processQuizBunda') }}" method="post" id="quiz_bunda_hebat">
                    @csrf
                    {{-- @foreach ($data->soals as $key => $dt) --}}
                    @foreach ($soals as $key => $soal)
                        @php
                            $index = ($key + 1) / count($soals);
                            $progress = $index * 100;
                        @endphp
                        <div class="card d-none" id="soal{{ $key + 1 }}">
                            <div class="card-body container-fluid">

                                <div class="text__black1_28 mt-2">
                                    Quiz Bunda Hebat - {{ $quiz->name }}
                                </div>

                                <div class="academy__quiz">
                                    <span>Pertanyaan {{ $key + 1 }}/{{ count($soals) }}</span>

                                    <div class="academy__progress">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar"
                                                style="width: {{ $progress }}%" aria-valuenow="25" aria-valuemin="0"
                                                aria-valuemax="100"></div>
                                        </div>
                                    </div>


                                    <h3 class="text__black1_24">{{ $soal->pertanyaan }}</h3>

                                    <div class="academy__ans">

                                        <label style="width: 100%" class="ans__label">
                                            <input type="radio" name="{{ $soal->id }}" value="A"
                                                class="card-input-element check__quiz"
                                                onclick="clickJawab({{ $key + 1 }}, {{ $soal->id }})" />
                                            <div class="card card-input">
                                                <div class="card-body text__black1_16">
                                                    <div class="mr-2">A.</div>
                                                    <div class="">
                                                        {{ $soal->jawaban_a }}
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                        <label style="width: 100%" class="ans__label">
                                            <input type="radio" name="{{ $soal->id }}" value="B"
                                                class="card-input-element check__quiz"
                                                onclick="clickJawab({{ $key + 1 }}, {{ $soal->id }})" />
                                            <div class="card card-input">
                                                <div class="card-body text__black1_16">
                                                    <div class="mr-2">B.</div>
                                                    <div class="">
                                                        {{ $soal->jawaban_b }}
                                                    </div>
                                                </div>
                                            </div>
                                        </label>

                                    </div>
                                    <div class="academy__quiz_button">
                                        @if ($key + 1 == count($soals))
                                            <button type="submit" class="btn" id="button{{ $key + 1 }}"
                                                disabled>Finish</button>
                                        @else
                                            <button type="button" class="btn btn__yellow" id="button{{ $key + 1 }}"
                                                onclick="nextQuestion({{ $key + 1 }})" disabled>Next</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{-- End foreach --}}
                </form>
            </div>
        </div>
        <!--end::Container-->
    </div>


@endsection


@section('pageJS')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        function nextQuestion(num) {
            // console.log(num)
            $(`#soal${num}`).addClass('d-none')
            $(`#soal${num + 1}`).removeClass('d-none')
        }

        function clickJawab(idnum, dtId) {
            $(`#button${idnum}`).prop('disabled', false);
        }
    </script>

    <script>
        // let currentIdx = {{ $currentIndex }}
        // console.log(currentIdx)
        $(`#soal1`).removeClass('d-none')
    </script>
@endsection
