@extends('errors.main')

@section('content')
    <section class="body d-flex justify-content-center">
        <div class="text-center custom-card">
            <img src="/images/badak-mama.png" alt="">
            <h1 class="error-description">Quiz sudah berakhir!</h1>
            {{-- <h1 class="error-description">sini <a class="error-link" href="/">kembali</a></h1> --}}
        </div>
    </section>
@endsection
