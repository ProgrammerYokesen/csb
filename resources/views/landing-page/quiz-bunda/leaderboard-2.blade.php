@extends('landing-page.quiz-bunda.module')

@section('v2_assets')
    <link rel="stylesheet" href="{{ asset('user-dashboard/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/quiz.css') }}">
@endsection

@section('content')

    <div class="d-flex flex-column-fluid justify-content-center align-items-center my-10">
        <!--begin::Container-->
        <div class="container">
            <div class="quiz_csb__logo">
                <img src="{{ asset('images/csb-logo-big.png') }}" alt="">
            </div>
            <h1 class="mb-10 text-center">Leaderboard Bunda Hebat</h1>

            <div class="teman__sobat_leaderboard">
                <div class="panjat_klasemen">
                    <table class="table table-striped table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <th style="border-top-left-radius: 10px">Posisi</th>
                                <th>Nama</th>
                                <th style="border-top-right-radius: 10px">Total Poin</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for ($i = 0; $i < 20; $i++)
                                <tr>
                                    <td style="font-weight: 700; padding-left: 25px">{{ $i + 1 }}</td>
                                    <td>{{ $dataLeaderboard[$i]->nama }}<sup>
                                        </sup></td>
                                    <td style="font-weight: 700">{{ $dataLeaderboard[$i]->poin }}</td>
                                    <td>{{ $dataLeaderboard[$i]->time }}</td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>


@endsection