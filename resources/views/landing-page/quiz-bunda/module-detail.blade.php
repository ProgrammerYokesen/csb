@extends('master')

@section('contents')
    <div class="d-flex flex-column-fluid justify-content-center align-items-center">
        <!--begin::Container-->
        <div class="container">

            <div class="modul_detail">
                <div class="back__btn">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('images/icon/arrow-left.svg') }}" alt=""> Back
                    </a>
                </div>

                <div class="row">
                    <div class="col-lg-8">

                        <div id="myElement" style="border-radius: 10px"></div>

                    </div>
                    <div class="col-lg-4 modul_desc">
                        <div class="card" style="height: 100%">
                            <div class="card-body">
                                <div>
                                    <h5>{{ $data->video_name }}.</h5>
                                    <p>{{ $data->description }}</p>
                                </div>

                                <div>
                                    <a href="{{ route('quiz', $data->uuid . '?marketId=' . $marketId) }}">
                                        @if ($lastPlayed != null)
                                            @if ($lastPlayed->end_status == 1)
                                                <button id="postQuizBtn" class="btn">Post Quiz</button>
                                            @else
                                                <button id="postQuizBtn" class="btn" disabled>Post Quiz</button>
                                            @endif
                                        @else
                                            <button id="postQuizBtn" class="btn" disabled>Post Quiz</button>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>

@endsection

@section('js')
    <script type="text/javascript">
        jwplayer("myElement").setup({
            file: "{{ $data->link_video }}",
            type: "video/mp4",
            autostart: false,
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>',
                'Authorization': "Bearer {{ Session::get('token') }}"
            }
        });

        function sendHistory() {
            var elapsed = jwplayer("myElement").getPosition();

            $.ajax({
                type: 'POST',
                url: '{{ route('sendVideoMinute') }}',
                data: {
                    videoId: '{{ $data->uuid }}',
                    time: elapsed
                },
                success: function(data) {
                    return
                },
                error: function(data) {
                    return
                }
            });
        }

        jwplayer().on('play', function() {
            sendHistory()
        })

        jwplayer().on('complete', function() {
            var elapsed = jwplayer("myElement").getPosition();

            $.ajax({
                type: 'POST',
                url: '{{ route('sendVideoMinute') }}',
                data: {
                    videoId: '{{ $data->uuid }}',
                    time: elapsed,
                    status_end: 1
                },
                success: function(data) {
                    return
                },
                error: function(data) {
                    return
                }
            });

            $('#postQuizBtn').prop('disabled', false);
        })

        window.onbeforeunload = function() {
            sendHistory()
        };
    </script>

    @if ($lastPlayed != null)
        <script>
            var minute = {{ $lastPlayed->minute }};
            jwplayer('myElement').seek(minute);
        </script>
    @endif

@endsection
