<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak</title>
    @laravelPWA
    <meta name="description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta property="og:title" content="Club Sobat Badak" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://sobatbadak.club" />
    <meta property="og:image" content="https://sobatbadak.club/images/share.png" />
    <meta property="og:description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />

    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, shrink-to-fit=no" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    {{-- CSRF TOKEN --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    {{-- Slick Carousel --}}
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />


    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('landing-page/style.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('landing-page/slider.css') }}">

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('landing-page/12.css') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '806244510020586');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199518915-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199518915-2');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NVTPTBJ');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 695110688 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-695110688"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-695110688');
    </script>

    <style>
      .status__imlek_container {
        padding-top: 100px;
        padding-bottom: 50px;
        position: relative;
        text-align: center;
      }

      .imlek__back_btn {
        position: absolute;
        top: 15px;
        left: 15px;
      }

      .imlek__back_btn a,
      .imlek__back_btn i {
        font-weight: 600;
        font-size: 18px;
        line-height: 120%;
        color: #600000;
      }

      .status__imlek_container h1 {
        font-weight: 600;
        font-size: 40px;
        line-height: 56px;
        text-align: center;
        color: #600000;
      }

      .status__imlek_container h6 {
        font-weight: 600;
        font-size: 18px;
        line-height: 27px;
        color: rgba(96, 0, 0, 0.7);
      }

      .status__imlek_container p {
        font-weight: 500;
        font-size: 18px;
        line-height: 27px;
        color: rgba(0, 0, 0, 0.3);
      }

      .status__imlek_container .card.outer-card {
        background: #FFFFFF;
        box-shadow: 0px 0px 12px rgba(240, 238, 227, 0.82);
        border-radius: 12px;
        padding: 2rem;
        width: 60%;
        margin: 15px auto;
        text-align: left;
      }

      .status__imlek_container .card.inner-card {
        padding: 1.5rem 1rem;
        border-radius: 0;
      }

      .status__imlek_container .status__img {
        margin-bottom: 25px;
      }

      .status__imlek_container button {
        background: #FFAA3B;
        border-radius: 8px 8px 8px 0px;
        font-weight: bold;
        font-size: 16px;
        line-height: 120%;
        color: #FFFFFF;
        padding: 12px 35px;
      }

      .status__imlek_container button:hover {
        color: #FFFFFF;
      }

      .imlek__qr {
        width: 300px;
      }

      @media screen and (max-width: 425px){
        .status__imlek_container .card.outer-card {
          background: #FFFFFF;
          box-shadow: 0px 0px 12px rgba(240, 238, 227, 0.82);
          border-radius: 12px;
          padding: 2rem;
          width: 100%;
          margin: 15px auto;
          text-align: left;
        }

        .status__imlek_container .card.inner-card {
          padding: 1rem 0.5rem;
          border-radius: 0;
        }

        .imlek__qr {
          width: 200px;
        }
      }


    </style>


</head>

<body id="kt_body" style="background: #F5EFE6;"
    class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVTPTBJ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="status__imlek_container">
      <!-- <div class="imlek__back_btn">
        <a href="{{route('imlekOrderPage')}}">
          <i class="fas fa-chevron-left"></i> Kembali
        </a>
      </div> -->
      <div class="container">
        <h1>Status Pembayaran</h1>

        @if($order->payment_status == 0)
          <img src="{{asset('images/event/imlek/order-success.png')}}" alt="" class="status__img">
          <h6>Pemesanan berhasil!</h6>
          <p>Terima kasih, pesananmu telah berhasil dibuat!</p>
        @elseif($order->payment_status == 1)
          <img src="{{asset('images/event/imlek/payment-success.png')}}" alt="" class="status__img">
          <h6>Pembayaran berhasil!</h6>
          <p>Terima kasih, pembayaranmu telah berhasil dan terverifikasi!</p>
        @elseif($order->payment_status == 2)
          <img src="{{asset('images/event/imlek/payment-failed.png')}}" alt="" class="status__img">
          <h6>Pembayaran Gagal!</h6>
          <p>Maaf, transaksimu gagal diproses. Silahkan lakukan transaksi ulang!</p>
        @endif

        <div class="card outer-card">
          <h5>Rincian Pesanan</h5>
          <div class="card inner-card text-left">
            <div class="row">
              <div class="col-4">
                Nomor Invoice
              </div>
              <div class="col-8">
                : {{$order->invoice}}
              </div>
            </div>
          </div>

          <div class="card inner-card text-left">
            <div class="row">
              <div class="col-4">
                Total Harga
              </div>
              <div class="col-8">
                : Rp {{$order->total_price}}
              </div>
            </div>
          </div>

          <div class="card inner-card text-left">
            <div class="row">
              <div class="col-4">
                Tanggal Pembelian
              </div>
              <div class="col-8">
                : {{$order->created_at}}
              </div>
            </div>
          </div>

          <div class="card inner-card text-left">
            <div class="row">
              <div class="col-4">
                Status Pembayaran
              </div>
              <div class="col-8">
                :
                @if($order->payment_status == 0)
                  <span style="color: #E50000;font-weight: 600">Menunggu Pembayaran</span>
                @elseif($order->payment_status == 1)
                  <span style="color: #00A92F;font-weight: 600">Pembayaran Berhasil</span>
                @elseif($order->payment_status == 2)
                  <span style="color: #E50000;font-weight: 600">Pembayaran Gagal</span>
                @endif
              </div>
            </div>
          </div>

          <div class="card inner-card text-left">
            <div class="row">
              <div class="col-4">
                Waktu Pembayaran
              </div>
              <div class="col-8">
                : {{$order->expired_at}}
              </div>
            </div>
          </div>

          <div class="card inner-card text-left">
            <div class="row">
              <div class="col-4">
                Metode Pembayaran
              </div>
              <div class="col-8">
                : <span style="font-weight: 700">{{$order->payment_channel}}</span>
              </div>
            </div>
          </div>

          @if($order->payment_channel == "SHOPEEPAY")
          <div class="text-center my-5">
            <a href="{{$order->invoice_link}}">
              Klik disini untuk Bayar
            </a>
          </div>
          @elseif($order->payment_channel == "QRIS")
            <div class="text-center my-5">
              <img class="imlek__qr" src="{{$order->invoice_link}}" alt="">
            </div>
          @elseif($order->payment_channel == "OVO" && $order->payment_status == 2)
            <div class="text-center my-5">
              <a href="{{route('recreateInvoiceImlek')}}">
                Buat Pembayaran Baru
              </a>
            </div>
          @endif




        </div>

        @if($order->payment_status == 2)
        <div class="text-center">
          <a href="{{route('resetInvoiceImlek')}}">
            <button class="btn">Buat Pesanan Baru</button>
          </a>
        </div>
        @endif

      </div>
    </div>


    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=806244510020586&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
    </script>
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#6993FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1E9FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>

    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>

</body>
<!--end::Body-->

</html>
