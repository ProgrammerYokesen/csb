@extends('landing-page.master')

@section('v2_assets')
  <link rel="stylesheet" href="{{ asset('landing-page/imlek.css') }}?v=0.1.6">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <style media="screen">

  </style>
@endsection

@section('content')

    <!-- === -->
    <div class="hero__imlek">
      <div class="container">
        <img class="desktop" src="{{asset('images/event/imlek/hero.png')}}" alt="">
        <img class="mobile" src="{{asset('images/event/imlek/hero-m.png')}}" alt="">
      </div>
    </div>

    <div class="imlek__container">

      <section class="section__imlek_1 imlek">
        <div class="container">
          <div class="row justofy-content-center align-items-center">
            <div class="col-sm-6">
              <img src="{{asset('images/event/imlek/section1-a.png')}}" alt="">
            </div>
            <div class="col-sm-6">
              <h2>Belanja LPCB Jeruk Panen Hadiah!</h2>
              <h6>Belanja Rp 61.000 bisa dapat 1 Bundle Larutan Penyegar Cap Badak Rasa Jeruk, plus bisa pilih Hadiah yang Sobat Badak mau dan mengikuti Workshop Reseller GRATIS!!!</h6>
              <div class="btn_container">
                <button class="btn cta__btn">Beli Sekarang</button>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section__imlek_2 imlek">
        <div class="container">
          <h2>Jadi Reseller Warisan <br> Gajahmada Auto Cuan!</h2>

          <div class="row">
            <div class="col-sm-4">
              <div class="card">
                <div class="d-flex justify-content-between">
                  <h3>01</h3>
                  <img src="{{asset('images/event/imlek/section2-a.png')}}" alt="">
                </div>
                <h5>Bisa mengikuti workshop menjadi Reseller Gratis</h5>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="card">
                <div class="d-flex justify-content-between">
                  <h3>02</h3>
                  <img src="{{asset('images/event/imlek/section2-b.png')}}" alt="">
                </div>
                <h5>Berkesempatan mengikuti Quiz Show Spesial Imlek 2022 di Club Sobat Badak</h5>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="card">
                <div class="d-flex justify-content-between">
                  <h3>03</h3>
                  <img src="{{asset('images/event/imlek/section2-c.png')}}" alt="">
                </div>
                <h5>Berhak mendapatkan harga spesial Reseller di Warisan Gajahmada</h5>
              </div>
            </div>
          </div>


        </div>
      </section>

      <section class="section__imlek_3 imlek">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 text-center">
              <img src="{{asset('images/event/imlek/section3.png')}}" alt="">
            </div>
            <div class="col-sm-6">
              <h2>Quiz Show Spesial Imlek 2022</h2>
              <h6>Quiz Show Spesial Imlek 2022 merupakan acara quiz yang disponsori oleh Warisan dan Warisan Gajahmada dalam rangka menyambut Hari Raya Imlek 2022.
                Acara ini memiliki hadiah dengan total hingga 50 juta rupiah!</h6>
              <div class="btn_container">
                  <button class="btn cta__btn">Ikuti Quiznya</button>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section__imlek_4 imlek">
        <div class="container">
            <h2>Daftar Sekarang</h2>
            <h2>Jangan sampai kehabisan slot ya!</h2>
            <div class="d-flex flex-column" style="margin: 25px auto">

              @php
                $noww = date("Y-m-d H:i:s");
              @endphp

              @if (strtotime($noww) <=  strtotime("2022-01-22T15:00:00"))
              <a href="{{ route('login.process-oAuth', 'google') }}" style="margin-bottom: 15px">
                <button class="btn imlek__reg_csb"><img class="mr-3" src="{{ asset('images/icons/google.svg') }}" alt="" style="background: #fff;padding: 5px; border-radius: 50%"> Daftar Sekarang Menggunakan Google</button>
              </a>
              <!-- <a href="{{route('registerPage')}}" style="margin-bottom: 15px">
                <button class="btn imlek__reg_csb"><img src="{{asset('images/csb-logo.png')}}" alt="" style="width: 35px; height: 35px; margin-right: 10px"> Daftar dengan akun CSB</button>
              </a> -->
              <h3 style="color: #600000;font-weight: 600; margin: 15px auto">Atau kamu sudah punya akun CSB? Klik disini</h3>
              <button class="btn imlek__reg_csb" data-toggle="modal" data-target="#modal_login" style="background: #ffaa3a"><img src="{{asset('images/csb-logo.png')}}" alt="" style="width: 35px; height: 35px; margin-right: 10px"> Masuk dengan akun CSB</button>
              @else
              <button class="btn imlek__reg_csb" style="background: #ffaa3a">Sold Out</button>
              @endif
            </div>
        </div>
      </section>

      <section class="section__imlek_5 imlek">
        <div class="container">
          <!-- <img src="{{asset('images/event/imlek/laptop.png')}}" alt=""> -->

          <div class="card">
            <!-- <img src="{{asset('images/event/imlek/laptop.png')}}" alt=""> -->
            <div class="isilaptop__imlek">
              <p style="font-weight: 700">Ketentuan:</p>
              <ol>
                <li>Pengiriman akan dilakukan mulai tanggal 31 Januari 2022 secara bertahap</li>
                <li>Kuota/pulsa yang digunakan selama acara ditanggung oleh peserta acara</li>
                <li>Yang berhak mengikuti kegiatan ini adalah Sobat Badak yang telah menyelesaikan transaksi</li>
              </ol>

              <div class="text-center">
                <button class="btn cta__btn">Daftar Sekarang</button>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>


    <!-- Modal Success -->
    <div class="modal fade harbolnas__modal" id="modal_login" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <h2 style="margin-bottom: 10px">Daftar dengan Akun CSB</h2>

                <div class="text-center" style="margin-bottom: 25px">
                  <p>Belum punya akun CSB? Daftar <a href="{{route('registerPage')}}">disini</a> </p>
                </div>

                <div class="form-auth">
                    <form class="form" id="form-auth" action="{{ route('processLogin') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input id="input-email" type="email" name="email" class="form-control form-control-solid"
                                placeholder="Email" required/>
                            <div class="input-text-error" id="error-email"></div>
                        </div>
                        <div class="form-group">
                            <div style="position: relative">
                                <input id="input-password" type="password" name="password"
                                    class="form-control form-control-solid" placeholder="Password" required />
                                <i class="fas fa-eye" id="togglePassword" onclick="togglePassword('input-password')"
                                    style="position: absolute; right:0.5rem;top:0.9rem; cursor: pointer;"></i>
                            </div>
                            <div class="input-text-error" id="error-password">
                            </div>
                        </div>

                        <div class="form-group text-left">
                            {!! NoCaptcha::display() !!}
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group d-flex justify-content-end">
                            <div>
                                <a href="{{ route('forgotPage') }}" class="forgot-pass-text">
                                    Lupa password?
                                </a>
                            </div>
                        </div>

                        <div class="form-button text-center">
                            <button type="submit" class="btn custom-btn-primary-rounded" style="width: unset;">Masuk</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="imlek__flying">
      <a href="https://api.whatsapp.com/send?phone=6281287628068&text=Punten+min%2C+mau+tanya-tanya+tentang+Quiz+Show+Special+Imlek+nih" class="flying" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
      </a>
    </div>
    <!-- === -->

@endsection

@section('pageJS')
<script src="//www.google.com/recaptcha/api.js"></script>

@if (Session::has('error'))
<script>
  Swal.fire("Gagal!", "Email atau password salah!", "error")
</script>
@endif

<script>
  $(".cta__btn").click(function() {
     $('html, body').animate({
         scrollTop: $(".section__imlek_4").offset().top - 200
     }, 1500);
  });
</script>
@endsection
