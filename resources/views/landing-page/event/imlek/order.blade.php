<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak</title>
    @laravelPWA
    <meta name="description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta property="og:title" content="Club Sobat Badak" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://sobatbadak.club" />
    <meta property="og:image" content="https://sobatbadak.club/images/share.png" />
    <meta property="og:description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />

    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, shrink-to-fit=no" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    {{-- CSRF TOKEN --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    {{-- Slick Carousel --}}
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />


    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('landing-page/style.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('landing-page/slider.css') }}">

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('landing-page/12.css') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '806244510020586');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199518915-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199518915-2');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NVTPTBJ');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 695110688 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-695110688"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-695110688');
    </script>

    <style>
      .order__imlek_container {
        padding-top: 50px;
        padding-bottom: 50px;
      }

      .order__imlek_container h1 {
        font-weight: 600;
        font-size: 40px;
        line-height: 56px;
        text-align: center;
        color: #600000;
      }

      .order__imlek_container h6 {
        font-weight: 500;
        font-size: 18px;
        line-height: 27px;
        text-align: center;
        color: rgba(96, 0, 0, 0.7);
      }

      .order__imlek_card .card.active {
        background: #FFFFFF;
        border: 4px solid #FFAA3B;
        box-sizing: border-box;
        border-radius: 16px;
      }

      .order__imlek_card label {
          width: 100%;
      }

      .order__imlek_card .card-input-element {
          display: none;
      }

      .order__imlek_card .card-input {
          margin: 10px;
          padding: 00px;
      }

      .order__imlek_card .card-input:hover {
          cursor: pointer;
      }

      .order__imlek_card .card-input-element:checked + .card-input {
           border: 4px solid #FFAA3B;
       }

       .order__imlek_card button {
         background: #FFAA3B;
         border-radius: 8px 8px 8px 0px;
         font-weight: bold;
         font-size: 16px;
         line-height: 120%;
         color: #FFFFFF;
         padding: 12px 35px;
       }

       .order__imlek_card .card {
         min-height: 400px;
       }

       .order__imlek_card .card-body {
         display: flex;
         flex-direction: column;
         justify-content: center;
         align-items: center;
       }

       .order__imlek_card .card-body img{
         width: 300px;
         min-height: 350px;
         object-fit: contain;
       }

       @media screen and (max-width: 425px){
         .order__imlek_card .card {
           min-height: 200px;
         }

         .order__imlek_card .card-body {
           padding: 1rem;
         }

         .order__imlek_card .card.lpcb img{
           width: 250px

         }

         .order__imlek_card .card-body img{
           width: 100%;
           min-height: 175px;
           object-fit: contain;
         }
       }
    </style>


</head>

<body id="kt_body" style="background: #F5EFE6;"
    class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVTPTBJ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="order__imlek_container">
      <div class="container">
        <h1>Pilih Barangmu</h1>
        <h6>Yuk pilih hadiah yang kamu mau. Kamu bebas untuk memilih salah satu dari ke 9 hadiah yang tersedia!</h6>

        <div class="order__imlek_card" style="margin-top: 50px">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <div class="card active lpcb">
                <div class="card-body">
                  <img src="{{asset('images/event/imlek/lpcb-new.png')}}" alt="">
                  <h6>Larutan Penyegar Cap Badak Bundle 6 Rasa Jeruk</h6>
                </div>
              </div>
            </div>
          </div>

          <div class="text-center">
            <h1 style="font-size: 96px;line-height: 144px;">+</h1>
          </div>

          <form class="" action="{{route('addToCartImlek')}}" method="post" id="productForm">
            @csrf
            <div class="row justify-content-center">
              @foreach($products as $product)
              @php
                $namaProduk = explode("+",$product->product_name);
              @endphp
              <div class="col-lg-4 col-6" style="padding: 0">
                <label>
                   <input type="radio" name="vendor_product_id" class="card-input-element" id="{{$product->id}}" value="{{$product->id}}" />
                     <div class="card card-input">
                       <div class="card-body text-center">
                         <img src="https://api.warisan.co.id/{{ $product->img }}" alt="">
                         <h6>{{ $namaProduk[1] }}</h6>
                       </div>
                     </div>
                 </label>
              </div>
              @endforeach
            </div>

          </form>

          <div class="text-center">
            <button class="btn" onclick="submitProduct()">Lanjut</button>
          </div>

        </div>
      </div>
    </div>


    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=806244510020586&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
    </script>
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#6993FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1E9FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>

    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>

    <script>
      function submitProduct() {
        var checkRadio = document.querySelector('input[name="vendor_product_id"]:checked');

        if (checkRadio) {
          $('#productForm').submit()
        }else{
          Swal.fire("Oops!", "Silahkan pilih hadiah terlebih dahulu sebelum lanjut", "warning")
        }
      }
    </script>

</body>
<!--end::Body-->

</html>
