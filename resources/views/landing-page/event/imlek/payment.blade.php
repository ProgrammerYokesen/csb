<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak</title>
    @laravelPWA
    <meta name="description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />
    <meta property="og:title" content="Club Sobat Badak" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://sobatbadak.club" />
    <meta property="og:image" content="https://sobatbadak.club/images/share.png" />
    <meta property="og:description"
        content="Yuk gabung di Club Sobat Badak! banyak keseruan games berhadiah & menangkan lelang barang-barang impianmu! Bisa juga Ngobrol bareng, belajar bareng, sharing, dan nambah teman" />

    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, shrink-to-fit=no" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    {{-- CSRF TOKEN --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    {{-- Slick Carousel --}}
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />


    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('landing-page/style.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('landing-page/slider.css') }}">

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('landing-page/12.css') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '806244510020586');
        fbq('track', 'PageView');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199518915-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199518915-2');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NVTPTBJ');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 695110688 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-695110688"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-695110688');
    </script>

    <style>
      .order__imlek_container {
        padding-top: 100px;
        padding-bottom: 50px;
        position: relative;
        width: 60%;
        margin: 0 auto;
      }

      .imlek__back_btn {
        position: absolute;
        top: 15px;
        left: 15px;
      }

      .imlek__back_btn a,
      .imlek__back_btn i {
        font-weight: 600;
        font-size: 18px;
        line-height: 120%;
        color: #600000;
      }

      .order__imlek_container h1 {
        font-weight: 600;
        font-size: 40px;
        line-height: 56px;
        text-align: center;
        color: #600000;
      }

      .order__imlek_container h6 {
        font-weight: 500;
        font-size: 18px;
        line-height: 27px;
        text-align: center;
        color: rgba(96, 0, 0, 0.7);
      }

      .payment__imlek .card.selectedproduct {
        padding: 1rem;
        width: 70%;
        margin-bottom: 25px;
      }

      .payment__imlek .card.selectedproduct img {
        width: 100%;
      }

      .payment__imlek h2 {
        font-weight: 600;
        font-size: 24px;
        line-height: 36px;
        color: #000000;
      }

      .payment__imlek h5 {
        font-weight: 600;
        font-size: 24px;
        line-height: 36px;
        color: #BD201C;
      }

      .payment__imlek ul li {
        font-weight: 500;
        font-size: 18px;
        line-height: 27px;
        color: rgba(96, 0, 0, 0.7);
      }

      .payment__imlek img {
        width: 200px;
      }

      .order__imlek_card .card.active {
        background: #FFFFFF;
        border: 4px solid #FFAA3B;
        box-sizing: border-box;
        border-radius: 16px;
      }

      .payment__imlek label {
          width: 100%;
      }

      .payment__imlek .card-input-element {
          display: none;
      }

      .payment__imlek .card-input {
          margin: 10px;
          padding: 00px;
      }

      .payment__imlek .card-input:hover {
          cursor: pointer;
      }

      .payment__imlek .card-input-element:checked + .card-input {
           border: 4px solid #FFAA3B;
       }

       .payment__imlek.method .card {
         min-height: 250px;
       }

       .payment__imlek.method .card-body {
         display: flex;
         justify-content: center;
         align-items: center;
       }

       .payment__imlek.method .card-body img {
         width: 200px;
       }

       .payment__imlek.method button {
         background: #FFAA3B;
         border-radius: 8px 8px 8px 0px;
         font-weight: bold;
         font-size: 16px;
         line-height: 120%;
         color: #FFFFFF;
         padding: 12px 35px;
       }

       @media screen and (max-width: 425px){
         .order__imlek_container {
           width: 100%;
           margin: 0 auto;
         }

         .payment__imlek img {
           width: 125px;
         }

         .payment__imlek.method .card {
           min-height: 100px;
         }

         .payment__imlek.method .col-4 {
           padding: 0;
         }

         .payment__imlek h5 {
           font-size: 18px !important;
           line-height: 36px;
         }

         .payment__imlek ul li {
           font-size: 13px;
           line-height: 20px;
         }

         .payment__imlek.method .card-body {
           padding: 1rem;
         }

         .payment__imlek.method .card-body img {
           width: 100%;
         }

         .payment__imlek .card.selectedproduct {
           width: 100%;
         }
       }
    </style>


</head>

<body id="kt_body" style="background: #F5EFE6;"
    class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVTPTBJ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="order__imlek_container">
      <div class="imlek__back_btn">
        <a href="{{route('imlekOrderPage')}}">
          <i class="fas fa-chevron-left"></i> Kembali
        </a>
      </div>
      <div class="container">
        <h1>Pembayaran</h1>
        <h6>Silahkan melakukan pembayaran sesuai dengan metode yang kamu inginkan.</h6>

        <div class="payment__imlek" style="margin-top: 50px; margin-bottom: 50px">
          <h2 class="text-left mb-3">Detail Produk</h2>
          <!-- <div class="row"> -->

              <!-- <div class="col-lg-4"> -->
                <div class="card selectedproduct">
                  <div class="container-fluid">
                    <div class="row justify-content-center align-items-center">
                      <div class="col-5">
                        <img src="{{asset('images/event/imlek/lpcb-new.png')}}" alt="">

                      </div>
                      <div class="col-2">
                        <h1>+</h1>

                      </div>
                      <div class="col-5">
                        <img src="https://api.warisan.co.id/{{ $product->img }}" alt="">

                      </div>
                    </div>
                  </div>
                </div>
              <!-- </div> -->

              <!-- <div class="col-lg-8"> -->
                <h5 class="text-left">{{$product->product_name}}</h5>
                <p>{!! $product->description !!}</p>
                <div class="text-right">
                  <h5 class="text-right">Rp61.000,-</h5>
                </div>
              <!-- </div> -->
          <!-- </div> -->
        </div>


        <form class="" action="{{route('createInvoiceImlek')}}" method="post" id="createInvoice">
        @csrf

        <div class="payment__imlek">
          <h2 class="text-left mb-5">Detail Pengiriman</h2>


            <div class="form-group row">
              <div class="col-lg-2">
                <label>Nama</label>
              </div>
              <div class="col-lg-10">
                <input class="form-control" type="text" id="userName" name="first_name" autocomplete="off" required
                placeholder="Masukkan nama kamu" />
              </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2">
                <label>Nomor Telepon</label>
              </div>
              <div class="col-lg-10 input-group">
                  <div class="input-group-prepend"><span class="input-group-text">+62</span></div>
                  <input id="whatsapp" type="text" class="form-control" name="whatsapp"
                      placeholder="Silahkan masukkan Nomor Whatsapp Anda" />
              </div>
              <!-- <div class="col-lg-10">
                <input class="form-control" type="text" id="whatsapp" name="whatsapp" autocomplete="off" required
                placeholder="" />
              </div> -->
            </div>

            <div class="form-group row">
              <div class="col-lg-2">
                <label>Alamat Lengkap</label>
              </div>
              <div class="col-lg-10">
                <textarea class="form-control" name="address" id="userAlamat" cols="10" rows="5"
                placeholder="Masukkan Alamat Lengkap kamu"></textarea>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2">
                <label>Provinsi</label>
              </div>
              <div class="col-lg-10">
                <!-- <select class="form-control select2" id="kt_select2_1" name="kota">
                  <option></option>
                  <option value="asd">asd</option>
                </select> -->

                <select class="select2-data-array browser-default" id="select2-provinsi" name="select2_province"></select>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2">
                <label>Kota/Kabupaten</label>
              </div>
              <div class="col-lg-10">
                <select class="select2-data-array browser-default" id="select2-kabupaten" name="select2_city"></select>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2">
                <label>Kecamatan</label>
              </div>
              <div class="col-lg-10">
                <select class="select2-data-array browser-default" id="select2-kecamatan" name="select2_subdistrict"></select>
              </div>
            </div>

            <input type="hidden" id="province" name="province" value="">
            <input type="hidden" id="city" name="city" value="">
            <input type="hidden" id="subdistrict" name="subdistrict" value="">

            <div class="form-group row">
              <div class="col-lg-2">
                <label>Info Tambahan</label>
              </div>
              <div class="col-lg-10">
                <textarea class="form-control" id="note" name="note" cols="10" rows="5"
                placeholder=""></textarea>
              </div>
            </div>


        </div>

        <div class="payment__imlek method" style="margin-top: 50px">
          <h2 class="text-left">Metode Pembayaran</h2>
          <p>Silahkan pilih metode pembayaran</p>

          <div class="row">

            <div class="col-lg-4 col-4">
              <label>
                 <input type="radio" name="payment_channel" class="card-input-element" value="OVO" onclick="showOVO()" />
                   <div class="card card-input">
                     <div class="card-body">
                       <img src="{{asset('images/event/imlek/ovo.png')}}" alt="">
                     </div>
                   </div>
               </label>
            </div>
            <div class="col-lg-4 col-4">
              <label>
                 <input type="radio" name="payment_channel" class="card-input-element" value="SHOPEEPAY" onclick="hideOVO()" />
                   <div class="card card-input">
                     <div class="card-body">
                       <img src="{{asset('images/event/imlek/sp.png')}}" alt="">
                     </div>
                   </div>
               </label>
            </div>
            <div class="col-lg-4 col-4">
              <label>
                 <input type="radio" name="payment_channel" class="card-input-element" value="QRIS" onclick="hideOVO()" />
                   <div class="card card-input">
                     <div class="card-body">
                       <img src="{{asset('images/event/imlek/qr.png')}}" alt="">
                     </div>
                   </div>
               </label>
            </div>

          </div>

          <div id="input__ovo" class="form-group d-none">
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text">+62</span></div>
                <input id="ovo" type="text" class="form-control" name="ovo_number"
                    placeholder="Silahkan masukkan Nomor OVO Anda" />
            </div>
          </div>


        </div>
        <!-- == -->
      </form>

      <div class="payment__imlek method text-right">
        <button class="btn" onclick="submitPayment()">Beli Sekarang</button>
      </div>

      </div>
    </div>


    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=806244510020586&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
    </script>
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#6993FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1E9FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>

    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
        integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
      var urlProvinsi = "https://ibnux.github.io/data-indonesia/provinsi.json";
      var urlKabupaten = "https://ibnux.github.io/data-indonesia/kabupaten/";
      var urlKecamatan = "https://ibnux.github.io/data-indonesia/kecamatan/";
      var urlKelurahan = "https://ibnux.github.io/data-indonesia/kelurahan/";

      $(document).ready(function() {
        $('#whatsapp').mask('000-0000-00000');
        $('#ovo').mask('000-0000-00000');
      });

      const showOVO = () => {
        $('#input__ovo').removeClass('d-none')
      }

      const hideOVO = () => {
        $('#input__ovo').addClass('d-none')
      }

      const submitPayment = () => {
        $('#whatsapp').unmask();
        $('#ovo').unmask();

        let payment_channel = $('input[name="payment_channel"]:checked').val();

        let username = document.getElementById('userName').value
        let whatsapp = document.getElementById('whatsapp').value
        let userAlamat = document.getElementById('userAlamat').value
        let province = document.getElementById('province').value
        let city = document.getElementById('city').value
        let subdistrict = document.getElementById('subdistrict').value
        let ovo = document.getElementById('ovo').value



        if (username && whatsapp && userAlamat && province && city && subdistrict && payment_channel ) {
          if (payment_channel == "OVO" && ovo == "") {
            return Swal.fire("Oops!", "Mohon isi nomor OVO terlebih dahulu!", "warning")
          }else {
            $('#createInvoice').submit()
          }
        }else{
          return Swal.fire("Oops!", "Mohon lengkapi semua data terlebih dahulu!", "warning")
        }

        // $('#createInvoice').submit()
      }

      function clearOptions(id) {
            // console.log("on clearOptions :" + id);

            //$('#' + id).val(null);
            $('#' + id).empty().trigger('change');
        }



      $.getJSON(urlProvinsi, function (res) {

            res = $.map(res, function (obj) {
                obj.text = obj.nama
                return obj;
            });

            data = [{
                id: "",
                nama: "- Pilih Provinsi -",
                text: "- Pilih Provinsi -"
            }].concat(res);
            // console.log(res)

            //implemen data ke select provinsi
            $("#select2-provinsi").select2({
                dropdownAutoWidth: true,
                width: '100%',
                data: data
            })

            $("#select2-kabupaten").select2({
                width: '100%',
            })

            $("#select2-kecamatan").select2({
                width: '100%'
            })
        });

        var selectProv = $('#select2-provinsi');
        $(selectProv).change(function () {

            // var nmProv = $('#select2-provinsi').select2('data')
            // $('#province').val(nmProv[0].nama);

            var value = $(selectProv).val();
            clearOptions('select2-kabupaten');


            if (value) {
                // console.log("on change selectProv");

                var text = $('#select2-provinsi :selected').text();
                $('#province').val(text);
                // console.log("value = " + value + " / " + "text = " + text);

                // console.log('Load Kabupaten di '+text+'...')
                $.getJSON(urlKabupaten + value + ".json", function(res) {

                    res = $.map(res, function (obj) {
                        obj.text = obj.nama
                        return obj;
                    });

                    data = [{
                        id: "",
                        nama: "- Pilih Kota / Kabupaten -",
                        text: "- Pilih Kota / Kabupaten -"
                    }].concat(res);

                    //implemen data ke select provinsi
                    $("#select2-kabupaten").select2({
                        dropdownAutoWidth: true,
                        width: '100%',
                        data: data
                    })
                })
            }
        });

        var selectKab = $('#select2-kabupaten');
        $(selectKab).change(function () {

            // var nmKab = $('#select2-kabupaten').select2('data')
            // $('#city').val(nmKab[0].nama);

            var value = $(selectKab).val();
            clearOptions('select2-kecamatan');

            if (value) {
                // console.log("on change selectKab");

                var text = $('#select2-kabupaten :selected').text();
                $('#city').val(text);

                // console.log("value = " + value + " / " + "text = " + text);

                // console.log('Load Kecamatan di '+text+'...')
                $.getJSON(urlKecamatan + value + ".json", function(res) {

                    res = $.map(res, function (obj) {
                        obj.text = obj.nama
                        return obj;
                    });

                    data = [{
                        id: "",
                        nama: "- Pilih Kecamatan -",
                        text: "- Pilih Kecamatan -"
                    }].concat(res);

                    //implemen data ke select provinsi
                    $("#select2-kecamatan").select2({
                        dropdownAutoWidth: true,
                        width: '100%',
                        data: data
                    })
                })
            }
        });

        var selectKec = $('#select2-kecamatan');
        $(selectKec).change(function () {

            // var nmKec = $('#select2-kecamatan').select2('data')
            // $('#subdistrict').val(nmKab[0].nama);

            var value = $(selectKec).val();
            clearOptions('select2-kelurahan');

            if (value) {
                // console.log("on change selectKec");

                var text = $('#select2-kecamatan :selected').text();
                $('#subdistrict').val(text);

                // console.log("value = " + value + " / " + "text = " + text);

                // console.log('Load Kelurahan di '+text+'...')
                $.getJSON(urlKelurahan + value + ".json", function(res) {

                    res = $.map(res, function (obj) {
                        obj.text = obj.nama
                        return obj;
                    });

                    data = [{
                        id: "",
                        nama: "- Pilih Kelurahan -",
                        text: "- Pilih Kelurahan -"
                    }].concat(res);

                    //implemen data ke select provinsi
                    $("#select2-kelurahan").select2({
                        dropdownAutoWidth: true,
                        width: '100%',
                        data: data
                    })
                })
            }
        });

        var selectKel = $('#select2-kelurahan');
        $(selectKel).change(function () {
            var value = $(selectKel).val();

            if (value) {
                // console.log("on change selectKel");

                var text = $('#select2-kelurahan :selected').text();
                // console.log("value = " + value + " / " + "text = " + text);
            }
        });
    </script>

</body>
<!--end::Body-->

</html>
