@extends('landing-page.master')

@section('content')
    <section class="event__makeup">
        <div class="container" style="position: relative; height: 100%">
            <div class="event__hero_wrapper">
                <h2>Modal Make-up #dirumahaja <br>
                    Bisa dapat</h2>
                <h1>TOTAL 10 JUTA RUPIAH?!</h1>
                <button class="btn" id="event__hero_btnn">Daftar Sekarang</button>
            </div>


            <img src="{{ asset('images/event/1010/bunda-makeup.png') }}" alt="" class="bunda__img">
        </div>
    </section>

    <section class="event__desc">
        <div class="container">
            <h2 style="color: #eb5a85">10 HADIAH UNTUK 10 BUNDA HEBAT!</h2>
            <p>10.10 Lomba Make-Up hadir untuk kamu! Daftarkan diri kamu segera dan dapatkan kesempatan untuk memenangkan 10
                hadiah berupa alat make up dengan total hingga 10 juta rupiah!</p>

            <p>Lomba Make-Up ini diadakan pada:</p>

            <div class="row text-left">
                <div class="col-6">Hari, Tanggal</div>
                <div class="col-6">: 8, 9, dan 10 Oktober 2021</div>
            </div>
            <div class="row text-left">
                <div class="col-6">Pukul</div>
                <div class="col-6">: 15.00 WIB - Selesai</div>
            </div>
            <div class="row text-left">
                <div class="col-12">Zoom Club Sobat Badak</div>
            </div>


            <p style="margin-top: 50px">Lomba ini akan diadakan selama 3 hari berturut-turut dengan sistem gugur dan
                final-nya akan diadakan ada pada
                10 Oktober 2021! Segera daftarkan dirimu dan jangan lupa juga ikuti workshop Make-Up pada 10 Oktober 2021!
                Kami tunggu kehadiran kalian!</p>
        </div>
    </section>

    <section class="event__form">
        <div class="container">
            <div class="event__form_container">
                <h3 class="mb-5" style="color: #eb5a85">Yuk Daftar!</h3>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Nama</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" id="userNama" autocomplete="off" required
                            placeholder="Isi Nama Disini" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">No. Whatsapp</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="number" id="userNo" autocomplete="off" required
                            placeholder="Isi No. Whatsapp Disini" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Email</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="email" id="userEmail" autocomplete="off" required
                            placeholder="Isi Email Disini" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Alamat Lengkap</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" name="" id="userAlamat" cols="10" rows="5"
                            placeholder="Isi Alamat Lengkap Disini"></textarea>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn mt-5" onclick="makeupSubmit()" id="btn_submit"
                        style="background: #eb5a85">Daftar</button>
                </div>
            </div>
        </div>
    </section>


    <!-- Modal-->
    <div class="modal fade event__makeup_modal" id="modal_conf" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h6>Terima kasih telah mengisi form. Selanjutnya Bunda akan mengirimkan pesan kepada kami melalui WA ya!
                </h6>
                <p>*Notes: Jangan ubah kata kata yang akan dikirim ke WA ya bund</p>

                <div class="row my-3">
                    <div class="col-12">
                        <button class="btn" onclick="sendWa()">Selesai</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>

    <script>
        $(document).ready(function() {
            Swal.fire("Halo Bunda, Kuota Lomba Makeup sudah penuh ya", "Semangat terus Bunda!", "warning");
        });
    </script>

    <script>
        $("#event__hero_btnn").click(function() {
            $('html, body').animate({
                scrollTop: $(".event__desc").offset().top - 150
            }, 1500);
        });
    </script>

    <script>
        let user_regis

        const makeupSubmit = () => {

            return Swal.fire("Halo Bunda, Kuota Lomba Makeup sudah penuh ya", "Semangat terus Bunda!", "warning");

            let namaUser = document.getElementById('userNama').value
            let noWa = document.getElementById('userNo').value
            let email = document.getElementById('userEmail').value
            let alamat = document.getElementById('userAlamat').value


            if (namaUser !== "" && noWa !== "" && email !== "" && alamat !== "") {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user-makeup.post') }}',
                    data: {
                        nama: namaUser,
                        no_wa: noWa,
                        email: email,
                        alamat: alamat
                    },
                    success: function(data) {
                        user_regis = data
                        if (data.success == false) {
                            return Swal.fire("Oops", "Email yang anda gunakan sudah terdaftar", "error");
                        }
                        $(`#modal_conf`).modal('toggle')
                        // window.location =
                        //     `{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`;
                        // location.replace(`{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`)
                        // windows.location(`{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`)
                        // Swal.fire("Berhasil Daftar!", "Jangan lupa join yaa!", "success");

                    },
                    error: function(data) {
                        Swal.fire("Ada kesalahan dalam server", "Silahkan refresh dan coba lagi", "error");
                    }

                });
            } else {
                return Swal.fire("Oops!", "Mohon isi semua Form terlebih dahulu", "warning");
            }

            // console.log(namaUser, noWa, email, alamat)
            // $('#modal_conf').modal('toggle')
        }

        const sendWa = () => {
            window.location =
                `https://api.whatsapp.com/send/?phone=6281287628068&text=Halo+Mimin%2C+Bunda+mau+daftar+Lomba+Make-Up%2C+nih%21+Ini+data+diri+Bunda+ya%2C+Min%3A%0D%0ANama%3A+${user_regis.user.nama}%0D%0ANo.+WA%3A+${user_regis.user.no_wa}%0D%0AAlamat%3A+${user_regis.user.alamat}%0D%0ATerima+kasih%2C+Mimin%21&app_absent=0`;
        }
    </script>
@endsection
