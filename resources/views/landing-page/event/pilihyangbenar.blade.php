@extends('landing-page.master')

@section('v2_assets')
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
@endsection

@section('content')
    <section class="harbolnas__hero2">
    </section>

    <section class="harbolnas__desc">
      <div class="container" data-aos="fade-up">
        <div class="row align-items-center justify-contents-center">
          <div class="col-lg-6 harbolnas__desc_img">
            <img src="{{asset('images/event/1111/pilihyangbenar.png')}}" alt="">
          </div>
          <div class="col-lg-6 harbolnas__desc_txt">
            <h2>"Pilih Yang Benar" game apa sih?</h2>
            <p style="margin-bottom: 25px">Pilih yang ini atau yang itu ya? PILIH YANG BENAR dong!
            Yuk sama-sama mainkan games "Pilih yang Benar"! Bunda akan diberikan batas waktu tertentu untuk menentukan pilihan hadiah yang Bunda bisa dapatkan!
            Pokoknya dijamin seru deh, Bund! Jangan sampai ketinggalan ya!
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="harbolnas__cara">
      <div class="container" data-aos="fade-up">
        <div class="text-center">
          <h2 style="margin-bottom: 25px">Cara Bermain</h2>
          <img src="{{asset('images/event/1111/cara-bermain.png')}}" alt="" class="img-fluid cara-desk" style="width: 80%">
          <img src="{{asset('images/event/1111/cara-bermainm.png')}}" alt="" class="img-fluid cara-mob" style="width: 100%">
        </div>
      </div>
    </section>

    <section class="harbolnas__footer">
      <div class="container" data-aos="fade-up">
        <div class="text-center">
          <h2 class="ketentuan">Syarat & Ketentuan</h2>
          <div class="sk_wrapper">
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">1</div>
              <div class="harbolnas__benefit_txt">Bunda wajib mengikuti event Harbolnas 11.11 Warisan Gajahmada (menyelesaikan transaksi dan mengikuti workshop reseller tanggal 12 November 2021)</div>
            </div>
            <div class="text-center mb-5">
              <a href="{{route('event11')}}">
                <button class="btn">Ikut Event Harbolnas 11.11 Warisan Gajahmada</button>
              </a>
            </div>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">2</div>
              <div class="harbolnas__benefit_txt">Periode Event Pilih yang benar: 15 November 2021 - 11 Desember 2021</div>
            </div>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">3</div>
              <div class="harbolnas__benefit_txt">Setiap harinya akan ada 20 Bunda yang terpilih untuk bermain</div>
            </div>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">4</div>
              <div class="harbolnas__benefit_txt">Kuota/pulsa yang digunakan selama acara ditanggung oleh peserta acara</div>
            </div>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">5</div>
              <div class="harbolnas__benefit_txt">Yang berhak mengikuti kegiatan ini adalah Bunda yang telah menyelesaikan transaksi</div>
            </div>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">6</div>
              <div class="harbolnas__benefit_txt">Keputusan Club Sobat Badak adalah mutlak dan tidak bisa diganggu gugat</div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="harbolnas__footer">
      <div class="container" data-aos="fade-up">
        <div class="text-center">
          <h2 class="ketentuan">Daftar sekarang ke Mimin yuk!<br>Kuotanya terbatas ya, Bund!<br>Jangan sampai kehabisan slot!</h2>
          <a href="{{route('event11')}}">
            <button class="btn">Daftar Sekarang</button>
          </a>
        </div>
      </div>
    </section>

    <!-- Modal-->
    <div class="modal fade event__makeup_modal" id="modal_conf" tabindex="-1" role="dialog" data-backdrop="static"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h6>Terima kasih telah mengisi form. Selanjutnya Bunda akan mengirimkan pesan kepada kami melalui WA ya!
                </h6>
                <p>*Notes: Jangan ubah kata kata yang akan dikirim ke WA ya bund</p>

                <div class="row my-3">
                    <div class="col-12">
                        <button class="btn" onclick="cekRegis()">Selesai</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Form-->
    <div class="modal fade event__modal_form" id="event__modal_form" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h6 class="mb-5">Yuk Bunda di isi dulu data dirinya!</h6>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Nama</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" id="userNama" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">No. Whatsapp</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="number" id="userNo" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Email</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="email" id="userEmail" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Alamat Lengkap</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" name="" id="userAlamat" cols="10" rows="5"
                            placeholder="Isi Alamat Lengkap Disini"></textarea>
                    </div>
                </div>

                <label class="checkbox">
                    <input type="checkbox" id="cek_syarat" name="remember" onclick="cekSyarat()" />
                    <span class="mr-2"></span>
                    Saya menyetujui Syarat dan Ketentuan yang berlaku
                </label>

                <button class="btn mt-5" id="btn_submit" onclick="sumbitFormNeeds()">Daftar</button>
            </div>
        </div>
    </div>

@endsection

@section('pageJS')
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
@endsection
