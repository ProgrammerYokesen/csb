@extends('landing-page.master')

@section('content')
    {{-- <section class="event__hero">
        <div id="marquee"></div>
        <div class="container d-flex justify-content-center align-items-center" style="position: relative">
            <div style="position: relative; z-index: 2">
                <h2>Belanja Bulanan Tanggal 9 Bulan 9 cuma</h2>
                <div class="event__title">
                    <h1>Rp 3.000?!</h1>
                    <div class="event__title_bg"></div>
                </div>

                <h3>Khusus untuk <span>Bunda Bunda Kece</span></h3>
                <h3>Hanya di Warisan Gajahmada x Club Sobat Badak!</h3>

                <button class="btn" id="event__btn">BELI SEKARANG</button>
            </div>

            <img src="{{ asset('images/event/hero-bg.png') }}" alt="" class="event__hero_bg">
        </div>
        <div id="marquee_bot"></div>
    </section> --}}

    <section class="event__banner">

        <div class="container">
            <!--<button class="btn event__banner_btn blink">BELANJA SEKARANG</button>-->
        </div>

    </section>



    <!--<section class="event__syarat">-->
    <!--    <div class="container">-->
    <!--        <div class="event__syarat_title">-->
    <!--            SYARAT DAN KETENTUAN-->
    <!--        </div>-->

    <!--        <h6 class="mb-5">Dengan menyelesaikan transaksi belanja paket sembako 3.000, Bunda bisa:</h6>-->

    <!--        <div class="card">-->
    <!--            <div class="card-body">-->
    <!--                <div class="event__syarat_num">-->
    <!--                    <img src="{{ asset('images/event/num1.png') }}" alt="">-->
    <!--                </div>-->
    <!--                <h6>Mendapatkan belanjaan pilihan bunda senilai Rp.110.000</h6>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="card">-->
    <!--            <div class="card-body">-->
    <!--                <div class="event__syarat_num">-->
    <!--                    <img src="{{ asset('images/event/num2.png') }}" alt="">-->
    <!--                </div>-->
    <!--                <h6>Ikutan workshop reseller minimal 1 hari, mendapatkan belanjaan pilihan bunda plus satu karton LPCB,-->
    <!--                    dengan total senilai 300.000.-->
    <!--                </h6>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="card">-->
    <!--            <div class="card-body">-->
    <!--                <div class="event__syarat_num">-->
    <!--                    <img src="{{ asset('images/event/num3.png') }}" alt="">-->
    <!--                </div>-->
    <!--                <h6>Mengikuti 3 hari workshop reseller, dapet belanjaan pilihan bunda, satu karton LPCB dan juga-->
    <!--                    kesempatan-->
    <!--                    memenangkan emas batangan 9 gram untuk 9 bunda yang beruntung!-->
    <!--                </h6>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->

    <!--<div class="event__cart_card" id="event_cart">-->
    <!--    <div class="container">-->
    <!--        <div class="d-flex" style="justify-content: space-between; align-items:center">-->
    <!--            <div>-->
    <!--                <h3>Total Belanja</h3>-->
    <!--                <h3 id="total_cart"></h3>-->
    <!--            </div>-->
    <!--            <div>-->
    <!--                <button class="btn" onclick="checkout()">Checkout-->
    <!--                    Sekarang</button>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->

    <!-- Modal-->
    <div class="modal fade event__modal" id="modal_conf" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h6>Apakah Bunda sudah yakin dengan pesanannya?</h6>
                <p>Pesanan yang sudah dipilih tidak dapat ditukar dengan barang lain ya Bund!</p>

                <div id="userPesanan"></div>

                <div class="row my-3">
                    <div class="col-lg-6">
                        <button class="btn ghost" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-lg-6">
                        <button class="btn" onclick="openForm()">OK!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Form-->
    <div class="modal fade event__modal_form" id="event__modal_form" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h6 class="mb-5">Yuk Bunda di isi dulu data dirinya!</h6>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Nama</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" id="userNama" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Kota/Kabupaten</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" id="userLokasi" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">No. Whatsapp</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="number" id="userNo" autocomplete="off" required />
                    </div>
                </div>

                <label class="checkbox">
                    <input type="checkbox" id="cek_syarat" name="remember" onclick="cekSyarat()" />
                    <span class="mr-2"></span>
                    Saya menyetujui Syarat dan Ketentuan yang berlaku
                </label>

                <button class="btn mt-5" onclick="submitOrder()" disabled id="btn_submit">Lanjut</button>
            </div>
        </div>
    </div>

@endsection

@section('pageJS')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/dynamic-marquee@2"></script>
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>


    <script>
        // Setting Marquee
        var $marquee = document.getElementById('marquee');
        var $marqueeBot = document.getElementById('marquee_bot');

        var marquee = (window.m = new dynamicMarquee.Marquee($marquee, {
            rate: -100,
        }));
        var marqueeBot = (window.m = new dynamicMarquee.Marquee($marqueeBot, {
            rate: 90,
        }));

        window.l = dynamicMarquee.loop(
            marquee,
            [() => '99 ribu'],
            function() {
                var $separator = document.createElement('div');
                $separator.innerHTML = '&nbsp';
                return $separator;
            }
        );

        window.l = dynamicMarquee.loop(
            marqueeBot,
            [() => '99 ribu'],
            function() {
                var $separatorbot = document.createElement('div');
                $separatorbot.innerHTML = '&nbsp';
                return $separatorbot;
            }
        );
    </script>

    <script>
        var cartTotal = 187880

        $(`#total_cart`).html(`Rp${numeral(cartTotal).format('0,0')}`)

        var produkKirim = []
        // var produkItem = []

        function addToCart(id) {



            var cek = $(`input[id=produk${id}]:checked`);
            $("#event_cart").removeClass("d-none")
            var val = document.getElementById(`product${id}`).value




            if (cek.length == 1) {

                if (cartTotal + parseInt(val) > 300000) {
                    alert('Maksimal Total Belanja Rp300.000 ya bund')
                    $(`#produk${id}`).prop('checked', false);
                    return
                } else {
                    cartTotal += parseInt(val)
                    produkKirim.push(id)
                    // produkItem.push({
                    //     valName,
                    //     valPrice
                    // })

                }
            } else if (cek.length == 0) {
                cartTotal -= parseInt(val)

                const index = produkKirim.indexOf(id);
                if (index > -1) {
                    produkKirim.splice(index, 1);
                }

                if (cartTotal == 0) {
                    $("#event_cart").addClass("d-none")
                }

            }

            // console.log(cartTotal)
            // console.log(produkKirim)
            $(`#total_cart`).html(`Rp${numeral(cartTotal).format('0,0')}`)

        }


        function submitOrder() {

            const nama = document.getElementById('userNama').value
            const location = document.getElementById('userLokasi').value
            const whatsapp = document.getElementById('userNo').value


            if (nama == "" || location == "" || whatsapp == "") {
                return Swal.fire("Oops masih ada input kosong", "Silahkan isi data diri dulu ya bund", "warning");
            }



            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('processEvent99') }}',
                data: {
                    name: nama,
                    lokasi: location,
                    products: produkKirim,
                    whatsapp: whatsapp
                },
                success: function(data) {
                    $(`#event__modal_form`).modal('hide')
                    window.location =
                        `{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`;
                    // location.replace(`{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`)
                    // windows.location(`{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`)
                    // Swal.fire("Berhasil Daftar!", "Jangan lupa join yaa!", "success");

                },
                error: function(data) {
                    Swal.fire("Ada kesalahan dalam server", "Silahkan refresh dan coba lagi", "error");
                    $(`#event__modal_form`).modal('hide')
                }

            });
        }

        function openForm() {
            $(`#modal_conf`).modal('hide')
            $(`#event__modal_form`).modal('show')
        }

        function checkout() {

            if (cartTotal == 187880 || produkKirim.length == 0) {
                return Swal.fire("Oops Belum ada Produk lain yang dipilih", "Silahkan Pilih Produknya dulu yaa bund",
                    "warning");
            }

            $('#userPesanan').empty();

            $('#userPesanan').append(`
            <div class="row mb-3" style="list-style: none;">
                <div class="col-5" style="text-align: left;">Larutan Penyegar Cap Badak Koin Gatotkaca</div>
                <div class="col-1" style="text-align: center">:</div>
            <div class="col-5" style="text-align: left;">Rp187,880</div>
            </div>`)

            produkKirim.forEach(el => {
                var valName = document.getElementById(`productNa${el}`).value
                var valPrice = document.getElementById(`product${el}`).value

                $('#userPesanan').append(`
                <div class="row mb-3" style="list-style: none;">
                    <div class="col-5" style="text-align: left">${valName}</div>
                    <div class="col-1" style="text-align: center">:</div>
                <div class="col-5" style="text-align: left">Rp${numeral(valPrice).format('0,0')}</div>
                </div>
                `)

            });

            $('#userPesanan').append(`
            <div class="row mb-3" style="list-style: none;">
                <div class="col-5" style="text-align: left; font-weight: 700">Total</div>
                <div class="col-1" style="text-align: center">:</div>
            <div class="col-5" style="text-align: left; font-weight: 700; text-decoration: line-through;">Rp${numeral(cartTotal).format('0,0')}</div>
            <div style="margin: 15px auto">
                <h6>BUNDA CUKUP BAYAR RP 3.000!</h6>
            </div>
            </div>`)

            $('#modal_conf').modal('show')
        }
    </script>

    <script>
        $(".event__banner_btn").click(function() {
            $('html, body').animate({
                scrollTop: $(".event__product").offset().top - 200
            }, 1500);
        });



        function cekSyarat() {
            var cekk = $('#cek_syarat:checkbox:checked').length > 0;
            if (cekk == true) {
                $(`#btn_submit`).prop('disabled', false);
            } else {
                $(`#btn_submit`).prop('disabled', true);
            }
        }
    </script>
@endsection
