@extends('landing-page.master')

@section('v2_assets')
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('landing-page/labha.css') }}">
@endsection

@section('content')
    <section class="harianak__hero">
      <img src="{{ asset('images/event/hari-anak/top-cloud.svg') }}" alt="" class="top-cloud cloud" data-speed="5">
      <img src="{{ asset('images/event/hari-anak/harianak.png') }}" alt="" class="harianak__poster cloud" data-speed="2">
      <img src="{{ asset('images/event/hari-anak/bot-cloud.svg') }}" alt="" class="bot-cloud" >
    </section>

    <section class="harbolnas__desc">
      <div class="container">
        <div class="harbolnas__desc_txt text-center">
          <h2 style="text-align: center; color:#27aef1">Anak Bunda suka mewarnai dan menyanyi?</h2>
          <p style="width: 100%">Yuk, ikutan Lomba Mewarnai dan Menyanyi dalam rangka Festival Hari Anak Sedunia bareng Club Sobat Badak.</p>
          <p style="width: 100%">Lomba ini khusus untuk anak berusia 6 - 12 tahun ya dan akan diadakan pada Sabtu, 20 November 2021! </p>
          <p style="width: 100%">Menangkan total hadiah <strong>JUTAAN RUPIAH!</strong></p>
          <p style="width: 100%">Dapatkan Piala dan Sertifikat</p>
        </div>
      </div>
    </section>

    <section class="harianak__rule">
      <div class="container">
        <button class="btn" data-toggle="modal" data-target="#modal_mewarnai">Peraturan Lomba Mewarnai</button>
        <button class="btn" data-toggle="modal" data-target="#modal_menyanyi">Peraturan Lomba Menyanyi</button>
      </div>
      <div class="text-center mt-5">
        <button class="btn" style="background:#fff; color: #27aef1; border: 1px solid #27aef1" onclick="downloadPDF()">Download Gambar Disini</button>
      </div>
    </section>

    <section class="harbolnas__form">
      <div class="container">
          <div class="harbolnas__form_wrapper">
              <h3 style="color:#27aef1">Langsung aja yuk daftar di bawah ini!</h3>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Nama Anak</label>
                  <div class="col-lg-9">
                      <input class="form-control" type="text" id="namaPeserta" autocomplete="off" required
                          placeholder="Contoh: Annisa Indriyani" />
                  </div>
              </div>

              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Usia</label>
                  <div class="col-lg-9">
                      <input class="form-control" type="number" id="userUsia" autocomplete="off" required
                          placeholder="Contoh: 7" />
                  </div>
              </div>

              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Jenis Kelamin</label>
                  <div class="col-lg-9">
                      <!-- <input class="form-control" type="text" id="userProv" autocomplete="off" required
                          placeholder="Isi Nama Disini" /> -->
                      <select class="form-control" id="userGender">
                       <option selected disabled>Pilih Jenis Kelamin</option>
                       <option value="L">Laki Laki</option>
                       <option value="P">Perempuan</option>
                      </select>
                  </div>
              </div>


              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Alamat</label>
                  <div class="col-lg-9">
                      <!-- <input class="form-control" type="text" id="userAlamat" autocomplete="off" required
                          placeholder="Contoh: Jl. Cendrawasih No. 32 RT 04/02" /> -->
                      <textarea class="form-control" name="" id="userAlamat" cols="10" rows="5"
                          placeholder="Contoh: Jl. Cendrawasih No. 32 RT 04/02"></textarea>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Pilih Provinsi</label>
                  <div class="col-lg-9">
                      <!-- <input class="form-control" type="text" id="userProv" autocomplete="off" required
                          placeholder="Isi Nama Disini" /> -->
                      <select class="form-control" id="userProv">
                       <option selected disabled>Pilih Provinsi</option>
                       @foreach ($provinces as $province)
                         <option value="{{ $province->name }}">{{ $province->name }}</option>
                       @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Pilih Lomba</label>
                  <div class="col-lg-9">
                      <!-- <input class="form-control" type="text" id="userProv" autocomplete="off" required
                          placeholder="Isi Nama Disini" /> -->
                      <select class="form-control" id="userLomba">
                       <option selected disabled>Pilih Lomba</option>
                       <option value="Mewarnai">Lomba Mewarnai</option>
                       <!-- <option value="Menyanyi">Lomba Menyanyi</option>
                       <option value="Semua Lomba">Ikut Semua Lomba</option> -->
                      </select>
                  </div>
              </div>


              <!-- <input type="hidden" id="fromCampaign" value="wgm"> -->

              <div class="text-center">
                  <button style="background: #27aef1" class="btn mt-5" disabled style="cursor: not-allowed">Pendaftaran Ditutup</button>
              </div>
          </div>
      </div>
    </section>

    <section class="harbolnas__footer">
      <div class="container">
        <div class="text-center">
          <h2 style="color:#27aef1">Yuk Segera Daftar<br>karena SLOT TERBATAS!</h2>
        </div>
      </div>
    </section>

    <!-- <button onclick="loading()">asd</button> -->
    <!-- Modal-->
    <div class="modal fade harbolnas__modal" id="modal_mewarnai" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h2 class="ketentuan" style="color:#27aef1">Cara ikutan dan syarat ketentuan Lomba Mewarnai</h2>
                <ol>
                  <li class="mb-3">Peserta wajib berusia 6 - 12 tahun</li>
                  <li class="mb-3">Anak-anak diwajibkan download dan print gambar yang telah disediakan</li>
                  <li class="mb-3">Gambar bisa di-download melalui link yang ada di Website Club Sobat Badak</li>
                  <li class="mb-3">Anak-anak akan diberikan waktu 1,5 jam untuk mewarnai</li>
                  <li class="mb-3">Orang tua dilarang membantu anak ketika mewarnai</li>
                  <li class="mb-3">Anak-anak wajib on cam saat acara berlangsung</li>
                  <li class="mb-3">Perlengkapan mewarnai yang dibutuhkan disediakan oleh anak dan orang tua</li>
                  <li class="mb-3">Wajib mengikuti kegiatan sampai selesai</li>
                  <li class="mb-3">Keputusan juri bersifat mutlak dan tidak bisa diganggu gugat</li>
                </ol>
                <div class="row my-3">
                    <div class="col-12 text-center">
                        <button style="background: #27aef1" class="btn" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade harbolnas__modal" id="modal_conf_mewarnai" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h2 class="ketentuan" style="color:#27aef1">Cara ikutan dan syarat ketentuan Lomba Mewarnai</h2>
                <ol>
                  <li class="mb-3">Peserta wajib berusia 6 - 12 tahun</li>
                  <li class="mb-3">Anak-anak diwajibkan download dan print gambar yang telah disediakan</li>
                  <li class="mb-3">Gambar bisa di-download melalui link yang ada di Website Club Sobat Badak</li>
                  <li class="mb-3">Anak-anak akan diberikan waktu 1,5 jam untuk mewarnai</li>
                  <li class="mb-3">Orang tua dilarang membantu anak ketika mewarnai</li>
                  <li class="mb-3">Anak-anak wajib on cam saat acara berlangsung</li>
                  <li class="mb-3">Perlengkapan mewarnai yang dibutuhkan disediakan oleh anak dan orang tua</li>
                  <li class="mb-3">Wajib mengikuti kegiatan sampai selesai</li>
                  <li class="mb-3">Keputusan juri bersifat mutlak dan tidak bisa diganggu gugat</li>
                </ol>
                <div class="row my-3">
                    <div class="col-12 text-center">
                        <button style="background: #27aef1" class="btn" onclick="submitRegister()">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade harbolnas__modal" id="modal_menyanyi" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h2 class="ketentuan" style="color:#27aef1">Cara ikutan dan syarat ketentuan Lomba Menyanyi</h2>
                <ol>
                  <li class="mb-3">Peserta wajib berusia 6 - 12 tahun</li>
                  <li class="mb-3">Anak-anak wajib menyanyikan lagu anak-anak (jaman dahulu ataupun modern)</li>
                  <li class="mb-3">Setiap anak akan diberi waktu selama 2 menit untuk bernyanyi</li>
                  <li class="mb-3">Anak-anak diperbolehkan memakai kostum atau menggunakan properti untuk menunjang penampilan</li>
                  <li class="mb-3">Pastikan suara anak terdengar dengan jelas</li>
                  <li class="mb-3">Saat tampil, anak-anak wajib on cam</li>
                  <li class="mb-3">Keputusan juri bersifat mutlak dan tidak bisa diganggu gugat</li>
                </ol>
                <div class="row my-3">
                    <div class="col-12 text-center">
                        <button style="background: #27aef1" class="btn" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade harbolnas__modal" id="modal_conf_menyanyi" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h2 class="ketentuan" style="color:#27aef1">Cara ikutan dan syarat ketentuan Lomba Menyanyi</h2>
                <ol>
                  <li class="mb-3">Peserta wajib berusia 6 - 12 tahun</li>
                  <li class="mb-3">Anak-anak wajib menyanyikan lagu anak-anak (jaman dahulu ataupun modern)</li>
                  <li class="mb-3">Setiap anak akan diberi waktu selama 2 menit untuk bernyanyi</li>
                  <li class="mb-3">Anak-anak diperbolehkan memakai kostum atau menggunakan properti untuk menunjang penampilan</li>
                  <li class="mb-3">Pastikan suara anak terdengar dengan jelas</li>
                  <li class="mb-3">Saat tampil, anak-anak wajib on cam</li>
                  <li class="mb-3">Keputusan juri bersifat mutlak dan tidak bisa diganggu gugat</li>
                </ol>
                <div class="row my-3">
                    <div class="col-12 text-center">
                        <button style="background: #27aef1" class="btn" onclick="submitRegister()">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Success -->
    <div class="modal fade harbolnas__modal" id="modal_success" tabindex="-1" role="dialog" data-backdrop="static"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <img src="{{asset('images/event/1111/success.png')}}" alt="">
                <h3 style="color:#27aef1">Selamat! Anda sudah berhasil mendaftar</h3>
                <h5>Selanjutnya akan diarahkan menuju Whatsapp dan chatting dengan Admin kami ya!</h5>

                <div class="row my-3">
                    <div class="col-12 text-center">
                        <button style="background: #27aef1" class="btn" onclick="proceedWA()">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>

    <script>
        $(document).ready(function() {
            Swal.fire("Halo Bunda, Kuota Lomba Anak sudah penuh ya", "Semangat terus Bunda!", "warning");
        });
    </script>

    <script type="text/javascript">
      document.addEventListener("mousemove", parallax);
      function parallax(e) {
        this.querySelectorAll('.cloud').forEach(layer => {
          const speed = layer.getAttribute('data-speed')

          const x = (window.innerWidth - e.pageX*speed)/100
          const y = (window.innerHeight - e.pageY*speed)/100

          layer.style.transform = `translateX(${x}px) translateY(${y}px)`
        })
      }
    </script>

    <script>
      function downloadPDF() {
        let filePDF = 'https://sobatbadak.club/uploads/download/csb_mewarnai.pdf'
        window.open(filePDF, '_blank');
      }
    </script>

    <script>
        let userRegis
        let lombaAnak
        let namaAnak

        function cekForm() {
          let namaPeserta = document.getElementById('namaPeserta').value
          let alamat = document.getElementById('userAlamat').value
          let provinsi = document.getElementById('userProv').value
          let usia = document.getElementById('userUsia').value
          let lomba = document.getElementById('userLomba').value
          let gender = document.getElementById('userGender').value
          // let from = document.getElementById('fromCampaign').value

          if (namaPeserta == "" || alamat == "" || provinsi == "" || lomba == "" || usia == "" || gender == "" ) {
            return Swal.fire("Oops", "Mohon diisi semua formnya ya", "warning");
          }else{
            // console.log(namaPeserta, alamat, provinsi, usia, lomba)
            // $(`#modal_conf`).modal('toggle')
            if (lomba == 'Mewarnai') {
              $(`#modal_conf_mewarnai`).modal('toggle')
            }else if (lomba == 'Menyanyi') {
              $(`#modal_conf_menyanyi`).modal('toggle')
            }else{
              submitRegister()
            }
          }

        }

        let submitRegister = () => {
          let namaPeserta = document.getElementById('namaPeserta').value
          let alamat = document.getElementById('userAlamat').value
          let provinsi = document.getElementById('userProv').value
          let usia = document.getElementById('userUsia').value
          let lomba = document.getElementById('userLomba').value
          let gender = document.getElementById('userGender').value
          $(`#modal_conf_mewarnai`).modal('hide')
          $(`#modal_conf_menyanyi`).modal('hide')
          loading()

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
              }
          });

          $.ajax({
              type: 'POST',
              url: 'https://sobatbadak.club/api/v1/register-anak',
              data: {
                  name: namaPeserta,
                  age: usia,
                  address: alamat,
                  gender: gender,
                  province: provinsi,
                  lomba: lomba
              },
              success: function(data) {
                  stopLoading()
                  $(`#modal_success`).modal('toggle')
                  userRegis = data
                  namaAnak = namaPeserta
                  lombaAnak = lomba
              },
              error: function(data) {
                stopLoading()
                // console.log(data)
                  Swal.fire("Maaf, ada kesalahan.",
                      "Silahkan coba daftar kembali beberapa saat lagi", "error");
              }

          });
        }

        function loading() {
          KTApp.blockPage({
           overlayColor: '#000000',
           state: 'warning', // a bootstrap color
           size: 'lg' //available custom sizes: sm|lg
          });
        }

        function stopLoading() {
           KTApp.unblockPage();
        }

        const proceedWA = () => {
            window.location =
                `https://api.whatsapp.com/send/?phone=6281287628068&text=Halo+Mimin%2C+Bunda+mau+mendaftarkan+anak+Bunda+nih%21+Berikut+datanya%2C+Min%3A%0D%0A%0D%0ANama+Anak%3A${namaAnak}%0D%0ALomba+yang+Diikuti%3A${lombaAnak}%0D%0AKode+Unik%3A${userRegis.uuid}+%0D%0A%0D%0ATerima+kasih%2C+Mimin%21`;
        }
    </script>
@endsection
