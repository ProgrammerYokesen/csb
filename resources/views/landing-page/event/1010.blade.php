@extends('landing-page.master')

@section('content')
    <section class="event__1010">
        <div class="container" style="position: relative; height: 100%">
            <div class="event__hero_wrapper">
                <h1>Kabulkan kebutuhan bulananmu <br>
                    hanya dengan belajar jadi <br>
                    <span>Reseller Warisan Gajahmada</span>
                </h1>
                <button class="btn" id="event__hero_btnn">Ikutan Sekarang</button>
            </div>

            <img src="{{ asset('images/event/1010/csbxwgm.png') }}" alt="" class="csbxwgm">
            <img src="{{ asset('images/event/1010/bunda.png') }}" alt="" class="bunda__img">
            <img src="{{ asset('images/event/1010/lunas.png') }}" alt="" class="bunda__cloud">
            <img src="{{ asset('images/event/1010/10.png') }}" alt="" class="bunda__promo">

        </div>
    </section>

    <section class="event__desc">
        <div class="container">
            <h2>Kabulkan kebutuhan bulanan Bunda hanya dengan belajar jadi Reseller Warisan Gajahmada senilai Rp 10.100,-
            </h2>
            <p>Siapa di sini yang mau dibayarin kebutuhan bulanannya? Bisa banget! Caranya gampang, loh!</p>
            <p>Bunda-Bunda hebat hanya perlu isi apa yang jadi kebutuhan Bunda dan cerita singkat tentang kebutuhan Bunda!
                Jangan lupa juga ikutan workshop Reseller selama 3 hari, ya!</p>
            <p>Akan ada 10 Bunda Hebat yang beruntung untuk dibayarin kebutuhannya! Jadi, segera daftarkan dirimu dan jangan
                lupa ikuti rangkaian acaranya, ya!</p>
        </div>
    </section>

    <section class="event__choices">
        <div class="container">
            <h3>Yuk Pilih kebutuhan bulanan yang mau dikabulkan</h3>
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-2 col-6">
                    <label class="text-center">
                        <input type="radio" name="pilih_kebutuhan" class="card-input-element d-none" value="uang sekolah"
                            onclick="notOthers()">
                        <div class="card">
                            Uang Sekolah
                        </div>
                    </label>
                </div>
                <div class="col-lg-2 col-6">
                    <label class="text-center">
                        <input type="radio" name="pilih_kebutuhan" class="card-input-element d-none" value="tagihan listrik"
                            onclick="notOthers()">
                        <div class="card">
                            Listrik
                        </div>
                    </label>
                </div>
                <div class="col-lg-2 col-6">
                    <label class="text-center">
                        <input type="radio" name="pilih_kebutuhan" class="card-input-element d-none" value="taghihan air"
                            onclick="notOthers()">
                        <div class="card">
                            Air
                        </div>
                    </label>
                </div>
                <div class="col-lg-2 col-6">
                    <label class="text-center">
                        <input type="radio" name="pilih_kebutuhan" class="card-input-element d-none" value="cicilan"
                            onclick="notOthers()">
                        <div class="card">
                            Cicilan
                        </div>
                    </label>
                </div>
                <div class="col-lg-2 col-6">
                    <label class="text-center">
                        <input type="radio" name="pilih_kebutuhan" class="card-input-element d-none" value="lainnya"
                            onclick="others()">
                        <div class="card">
                            Lainnya
                        </div>
                    </label>
                </div>
            </div>

            <div class="event__custom_input d-none" id="other_needs">
                <input class="form-control" type="text" placeholder="Isi Kebutuhanmu Disini" id="other_kebutuhan">
            </div>

            <div class="event__essay text-center">
                <h3>Ceritain dong keresahanmu!</h3>
                <textarea class="form-control" name="" id="keresahan_user" cols="30" rows="10"
                    placeholder="Isi Keresahanmu disini"></textarea>
                <button class="btn" onclick="submitNeed()">Lanjut</button>
            </div>

        </div>
    </section>

    <!-- Modal-->
    <div class="modal fade event__makeup_modal" id="modal_conf" tabindex="-1" role="dialog" data-backdrop="static"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h6>Terima kasih telah mengisi form. Selanjutnya Bunda akan mengirimkan pesan kepada kami melalui WA ya!
                </h6>
                <p>*Notes: Jangan ubah kata kata yang akan dikirim ke WA ya bund</p>

                <div class="row my-3">
                    <div class="col-12">
                        <button class="btn" onclick="cekRegis()">Selesai</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Form-->
    <div class="modal fade event__modal_form" id="event__modal_form" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h6 class="mb-5">Yuk Bunda di isi dulu data dirinya!</h6>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Nama</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="text" id="userNama" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">No. Whatsapp</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="number" id="userNo" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Email</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="email" id="userEmail" autocomplete="off" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-left">Alamat Lengkap</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" name="" id="userAlamat" cols="10" rows="5"
                            placeholder="Isi Alamat Lengkap Disini"></textarea>
                    </div>
                </div>

                <label class="checkbox">
                    <input type="checkbox" id="cek_syarat" name="remember" onclick="cekSyarat()" />
                    <span class="mr-2"></span>
                    Saya menyetujui Syarat dan Ketentuan yang berlaku
                </label>

                <button class="btn mt-5" id="btn_submit" onclick="sumbitFormNeeds()">Daftar</button>
            </div>
        </div>
    </div>

@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>


    <script>
        $("#event__hero_btnn").click(function() {
            $('html, body').animate({
                scrollTop: $(".event__desc").offset().top - 150
            }, 1500);
        });
    </script>

    <script>
        const others = () => {
            $('#other_needs').removeClass('d-none')
        }

        const notOthers = () => {
            $('#other_needs').addClass('d-none')
        }


        let kebutuhan = ""
        let keresahan = ""
        let user_regis

        const submitNeed = () => {

            let userNeed = $('input[name="pilih_kebutuhan"]:checked').val();
            keresahan = document.getElementById('keresahan_user').value

            if (userNeed == undefined || keresahan == "") {
                return Swal.fire("Oops!", "Mohon isi semua Form terlebih dahulu", "warning");
            }

            if (userNeed == 'lainnya') {
                kebutuhan = document.getElementById('other_kebutuhan').value
            } else {
                kebutuhan = userNeed
            }

            $('#event__modal_form').modal('toggle')

        }

        const sumbitFormNeeds = () => {
            let namaUser = document.getElementById('userNama').value
            let noWa = document.getElementById('userNo').value
            let email = document.getElementById('userEmail').value
            let alamat = document.getElementById('userAlamat').value


            if (namaUser !== "" && noWa !== "" && email !== "" && alamat !== "") {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user-harbolnas.post') }}',
                    data: {
                        nama: namaUser,
                        no_wa: noWa,
                        email: email,
                        alamat: alamat,
                        kebutuhan: kebutuhan,
                        cerita: keresahan
                    },
                    success: function(data) {
                        user_regis = data

                        $(`#event__modal_form`).modal('hide')

                        if (!data.success) {
                            Swal.fire("Maaf, email yang kamu masukkan sudah digunakan.",
                                "Silahkan mencoba lagi dengan email yang berbeda", "error");
                        } else {
                            $(`#modal_conf`).modal('toggle')
                        }
                        // window.location =
                        //     `{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`;
                        // location.replace(`{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`)
                        // windows.location(redirect);
                        // Swal.fire("Berhasil Daftar!", "Jangan lupa join yaa!", "success");

                    },
                    error: function(data) {
                        Swal.fire("Maaf, email yang kamu masukkan sudah digunakan.",
                            "Silahkan mencoba lagi dengan email yang berbeda", "error");
                    }

                });

            } else {
                return Swal.fire("Oops!", "Mohon isi semua Form terlebih dahulu", "warning");
            }
        }

        const cekRegis = () => {
            window.location =
                `https://api.whatsapp.com/send/?phone=${ user_regis.user.whatsapp }&text=Hallo+admin%2C+Bunda+mau+ikutan+belajar+jadi+Reseller+Warisan+Gajahmada+dan+mau+bayar+Rp10.100+agar+dapat+kesempatan+dibayarin+kebutuhannya.+Ini+pesanan+Bunda+ya%3A%0D%0A-+1+Bundle+Larutan+Penyegar+Cap+Badak%0D%0A-+${kebutuhan}%0D%0A%28order+ID+%3D+${ user_regis.user.order_id }%29&app_absent=0`;
        }
    </script>
@endsection
