@extends('landing-page.master')

@section('v2_assets')
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
@endsection

@section('content')
    <section class="harbolnas__hero3">
    </section>

    <section class="harbolnas__desc">
      <div class="container" data-aos="fade-up">
        <div class="row align-items-center justify-contents-center">
          <div class="col-lg-6 harbolnas__desc_img">
            <img src="{{asset('images/event/1111/spesial11.png')}}" alt="">
          </div>
          <div class="col-lg-6 harbolnas__desc_txt">
            <h2>Spesial Harbolnas 11.11</h2>
            <p>Pilih yang ini atau yang itu ya? PILIH YANG BENAR dong!</p>
            <p>Bunda berkesempatan untuk ikut game “Pilih yang Benar” hanya dengan cara mendaftar jadi Reseller Warisan Gajahmada dan mengikuti workshop tentang Reseller.</p>
            <p>KHUSUS HARBOLNAS 11.11! Pendaftarannya cukup dengan membayar Rp11.110, loh!</p>
          </div>
        </div>
      </div>
    </section>

    <section class="harbolnas__benefit">
      <div class="container" data-aos="fade-up">
        <div class="row align-items-center justify-contents-center">
          <div class="col-lg-6 harbolnas__benefit_desc">
            <h2>Keuntungan jadi Reseller</h2>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">1</div>
              <div class="harbolnas__benefit_txt">Bisa mengikuti workshop menjadi Reseller Gratis</div>
            </div>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">2</div>
              <div class="harbolnas__benefit_txt">Berkesempatan mengikuti games “Pilih yang Benar” di Club Sobat Badak</div>
            </div>
            <div class="d-flex align-items-center" style="margin-bottom: 25px">
              <div class="harbolnas__benefit_num">3</div>
              <div class="harbolnas__benefit_txt">Berhak mendapatkan harga spesial Reseller di Warisan Gajahmada</div>
            </div>
          </div>
          <div class="col-lg-6 harbolnas__benefit_img">
            <img src="{{asset('images/event/1111/reseller11.png')}}" alt="">
          </div>
        </div>
      </div>
    </section>

    <section class="harbolnas__desc">
      <div class="container" data-aos="fade-up">
        <div class="row align-items-center justify-contents-center">
          <div class="col-lg-6 harbolnas__desc_img">
            <img src="{{asset('images/event/1111/pilihyangbenar.png')}}" alt="">
          </div>
          <div class="col-lg-6 harbolnas__desc_txt">
            <h2>"Pilih Yang Benar" game apa sih?</h2>
            <p style="margin-bottom: 25px">Pilih yang ini atau yang itu ya? PILIH YANG BENAR dong!
            Yuk sama-sama mainkan games "Pilih yang Benar"! Bunda akan diberikan batas waktu tertentu untuk menentukan pilihan hadiah yang Bunda bisa dapatkan!
            Pokoknya dijamin seru deh, Bund! Jangan sampai ketinggalan ya!
            </p>
            <button class="btn prim" data-toggle="modal" data-target="#modal_cara">Cara Bermain</button>
            <button class="btn sec" data-toggle="modal" data-target="#modal_conf">Syarat dan Ketentuan</button>
          </div>
        </div>
      </div>
    </section>

    <section class="harbolnas__form">
      <div class="container" data-aos="fade-up">
          <div class="harbolnas__form_wrapper">
              <h3>Langsung aja yuk daftar di bawah ini!</h3>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Nama</label>
                  <div class="col-lg-9">
                      <input class="form-control" type="text" id="userNama" autocomplete="off" required
                          placeholder="Contoh: Annisa Indriyani" />
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Alamat</label>
                  <div class="col-lg-9">
                      <!-- <input class="form-control" type="text" id="userAlamat" autocomplete="off" required
                          placeholder="Contoh: Jl. Cendrawasih No. 32 RT 04/02" /> -->
                      <textarea class="form-control" name="" id="userAlamat" cols="10" rows="5"
                          placeholder="Contoh: Jl. Cendrawasih No. 32 RT 04/02"></textarea>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Pilih Provinsi</label>
                  <div class="col-lg-9">
                      <select class="form-control" id="userProv">
                        <option selected disabled>Pilih Provinsi</option>
                         @foreach ($provinces as $province)
                           <option value="{{ $province->name }}">{{ $province->name }}</option>
                         @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">No. Whatsapp</label>
                  <div class="col-lg-9">
                      <input class="form-control" type="number" id="userNo" autocomplete="off" required
                          placeholder="Contoh: 62813940294721" />
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Email</label>
                  <div class="col-lg-9">
                      <input class="form-control" type="email" id="userEmail" autocomplete="off" required
                          placeholder="Contoh: annisaindriyani@gmail.com" />
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-lg-3 col-form-label text-left">Usia</label>
                  <div class="col-lg-9">
                      <input class="form-control" type="number" id="userUsia" autocomplete="off" required
                          placeholder="Contoh: 42" />
                  </div>
              </div>

              <input type="hidden" id="fromCampaign" value="csb">

              <div class="text-center">
                  <button class="btn mt-5" id="btn_submit" disabled style="cursor: not-allowed">Sold Out</button>
              </div>
          </div>
      </div>
    </section>

    <section class="harbolnas__footer">
      <div class="container" data-aos="fade-up">
        <div class="text-center">
          <h2>Ayo Daftar sekarang! <br> Jangan sampai kehabisan slot ya, Bund!</h2>

          <h2 class="ketentuan">Ketentuan</h2>
          <div class="d-flex align-items-center" style="margin-bottom: 25px">
            <div class="harbolnas__benefit_num">1</div>
            <div class="harbolnas__benefit_txt">Pengiriman akan dilakukan mulai tanggal 25 November 2021 secara bertahap</div>
          </div>
          <div class="d-flex align-items-center" style="margin-bottom: 25px">
            <div class="harbolnas__benefit_num">2</div>
            <div class="harbolnas__benefit_txt">Kuota/pulsa yang digunakan selama acara ditanggung oleh peserta acara</div>
          </div>
          <div class="d-flex align-items-center" style="margin-bottom: 25px">
            <div class="harbolnas__benefit_num">3</div>
            <div class="harbolnas__benefit_txt">Yang berhak mengikuti kegiatan ini adalah Bunda yang telah menyelesaikan transaksi</div>
          </div>

        </div>
      </div>
    </section>

    <!-- <button data-toggle="modal" data-target="#modal_conf">asd</button> -->
    <!-- Modal-->
    <div class="modal fade harbolnas__modal" id="modal_conf" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h2 class="ketentuan">Syarat & Ketentuan</h2>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">1</div>
                  <div class="harbolnas__benefit_txt">Bunda wajib mengikuti event Harbolnas 11.11 Warisan Gajahmada (menyelesaikan transaksi dan mengikuti workshop reseller tanggal 12 November 2021)</div>
                </div>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">2</div>
                  <div class="harbolnas__benefit_txt">Periode Event Pilih yang benar: 15 November 2021 - 11 Desember 2021</div>
                </div>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">3</div>
                  <div class="harbolnas__benefit_txt">Setiap harinya akan ada 20 Bunda yang terpilih untuk bermain</div>
                </div>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">4</div>
                  <div class="harbolnas__benefit_txt">Kuota/pulsa yang digunakan selama acara ditanggung oleh peserta acara</div>
                </div>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">5</div>
                  <div class="harbolnas__benefit_txt">Yang berhak mengikuti kegiatan ini adalah Bunda yang telah menyelesaikan transaksi</div>
                </div>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">6</div>
                  <div class="harbolnas__benefit_txt">Keputusan Club Sobat Badak  adalah mutlak dan tidak bisa diganggu gugat</div>
                </div>
                <!-- <div class="row my-3">
                    <div class="col-12 text-center">
                        <button class="btn">Saya menyetujui semua ketentuan</button>
                    </div>
                </div> -->
            </div>
        </div>
    </div>

    <!-- Modal Conf -->
    <div class="modal fade harbolnas__modal" id="modal_syarat" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h2 class="ketentuan">Ketentuan</h2>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">1</div>
                  <div class="harbolnas__benefit_txt">Pengiriman akan dilakukan mulai tanggal 25 November 2021 secara bertahap</div>
                </div>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">2</div>
                  <div class="harbolnas__benefit_txt">Kuota/pulsa yang digunakan selama acara ditanggung oleh peserta acara</div>
                </div>
                <div class="d-flex align-items-center" style="margin-bottom: 25px">
                  <div class="harbolnas__benefit_num">3</div>
                  <div class="harbolnas__benefit_txt">Yang berhak mengikuti kegiatan ini adalah Bunda yang telah menyelesaikan transaksi</div>
                </div>

                <div class="row my-3">
                    <div class="col-12 text-center">
                        <button class="btn" onclick="submitRegister()">Saya menyetujui semua ketentuan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Cara Bermain -->
    <div class="modal fade harbolnas__modal" id="modal_cara" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <h2 class="ketentuan">Cara Bermain</h2>
              <img src="{{asset('images/event/1111/cara-bermain.png')}}" alt="" class="img-fluid cara-desk" style="width: 100%">
              <img src="{{asset('images/event/1111/cara-bermainm.png')}}" alt="" class="img-fluid cara-mob" style="width: 100%">
            </div>
        </div>
    </div>

    <!-- Modal Success -->
    <div class="modal fade harbolnas__modal" id="modal_success" tabindex="-1" role="dialog" data-backdrop="static"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <img src="{{asset('images/event/1111/success.png')}}" alt="">
                <h3>Selamat! Bunda sudah berhasil mendaftar</h3>
                <h5>Bunda akan diarahkan menuju Whatsapp dan chatting dengan Admin kami ya!</h5>

                <div class="row my-3">
                    <div class="col-12 text-center">
                        <button class="btn" onclick="proceedWA()">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pageJS')
<script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
<script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>

<script>
  let userRegis

    function cekForm() {
      let namaBunda = document.getElementById('userNama').value
      let alamat = document.getElementById('userAlamat').value
      let provinsi = document.getElementById('userProv').value
      let phone = document.getElementById('userNo').value
      let email = document.getElementById('userEmail').value
      let usia = document.getElementById('userUsia').value
      let from = document.getElementById('fromCampaign').value

      if (namaBunda == "" || alamat == "" || provinsi == "" || phone == "" || email == "" || usia == "" ) {
        return Swal.fire("Oops", "Mohon diisi semua formnya ya Bunda", "warning");
      }else{
        $(`#modal_syarat`).modal('toggle')
      }

      // console.log(namaBunda, alamat, provinsi, phone, email, usia, from)
    }

    let submitRegister = () => {
      let namaBunda = document.getElementById('userNama').value
      let alamat = document.getElementById('userAlamat').value
      let provinsi = document.getElementById('userProv').value
      let phone = document.getElementById('userNo').value
      let email = document.getElementById('userEmail').value
      let usia = document.getElementById('userUsia').value
      let from = document.getElementById('fromCampaign').value

      $(`#modal_syarat`).modal('hide')
      loading()

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          type: 'POST',
          url: '{{ route('postHarbolnas11') }}',
          data: {
              name: namaBunda,
              whatsapp: phone,
              email: email,
              alamat: alamat,
              usia: usia,
              provinsi: provinsi,
              from: from
          },
          success: function(data) {
              stopLoading()
              $(`#modal_success`).modal('toggle')
              userRegis = data

              // $(`#event__modal_form`).modal('hide')
              //
              // if (!data.success) {
              //     Swal.fire("Maaf, email yang kamu masukkan sudah digunakan.",
              //         "Silahkan mencoba lagi dengan email yang berbeda", "error");
              // } else {
              //     $(`#modal_conf`).modal('toggle')
              // }
              // window.location =
              //     `{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`;
              // location.replace(`{{ env('APP_URL') }}/eventspesialharbolnas99/send-whatsapp/${data.orderId}`)
              // windows.location(redirect);
              // Swal.fire("Berhasil Daftar!", "Jangan lupa join yaa!", "success");

          },
          error: function(data) {
            stopLoading()
            // console.log(data)
              Swal.fire("Maaf, email yang kamu masukkan sudah digunakan.",
                  "Silahkan mencoba lagi dengan email yang berbeda", "error");
          }

      });
    }

    function loading() {
      KTApp.blockPage({
       overlayColor: '#000000',
       state: 'warning', // a bootstrap color
       size: 'lg' //available custom sizes: sm|lg
      });
    }

    function stopLoading() {
       KTApp.unblockPage();
    }

    const proceedWA = () => {
        window.location =
            `https://api.whatsapp.com/send/?phone=${ userRegis.whatsapp_admin }&text=Halo+Admin%2C+Bunda+mau+ikut+daftar+jadi+Reseller+Warisan+Gajahmada+dan+bayar+Rp11.110+agar+bisa+dapet+kesempatan+mengikuti+workshop+tentang+Reseller+dan+main+game+%22Pilih+yang+Benar%22.%0D%0A%0D%0AIni+pesanan+Bunda+ya%3A%0D%0A-+1+Bundle+Larutan+Penyegar+Cap+Badak%0D%0A%0D%0ATerima+kasih+banyak%2C+Min%21%0D%0A%0D%0A%28Order+ID+%3D+${userRegis.orderId}%29`;
    }
</script>
@endsection
