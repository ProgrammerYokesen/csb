@extends('landing-page.master')

@section('v2_assets')
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="{{ asset('landing-page/12.css') }}?v=1.3">
@endsection

@section('content')
    <section class="hero__12">
        <img src="{{ asset('images/event/1212/spesial.png') }}" alt="">
        <img src="{{ asset('images/event/1212/hadiah.png') }}" alt="">

        <button class="btn" id="hero__btn">Daftar Sekarang</button>
        <p>Geser ke bawah untuk mengetahui lebih lanjut</p>
        <img src="{{ asset('images/event/1212/switch.png') }}" alt="" class="switch__12">
    </section>

    <section class="desc__12">
      <div class="container">
        <div class="row align-items-center justify-contents-center">
          <div class="col-lg-6 desc__12_txt">
            <h2>Harbolnas 12.12</h2>
            <p style="margin-bottom: 25px">Hanya dengan membayar Rp61.000, Sobat Badak bisa menjadi <strong>Reseller Warisan Gajahmada</strong> dan <strong>mengikuti workshop Reseller secara GRATIS</strong> loh!</p>
            <p style="margin-bottom: 25px">Sobat Badak juga akan <strong>mendapatkan satu mystery box</strong>, loh! Tidak ketinggalan, Sobat Badak juga berkesempatan untuk <span>mengikuti games berhadiah khusus Harbolnas 12.12!</span></p>
            <p><span>Ps. *Harga Rp 61.000 hanya berlaku hingga tanggal 12 Desember, setelah itu harga kembali normal menjadi Rp 100.000 hingga 30 Desember 2021*</span></p>
          </div>

          <div class="col-lg-6 desc__12_btn">
            <button class="btn" id="reseller">Cari tahu keuntungan jadi Reseller <img class="ml-3" src="{{asset('images/event/1212/arrow-down.png')}}" alt=""></button>
          </div>

        </div>
      </div>
    </section>

    <section class="benefit__12">
      <div class="container">

        <h2>Keuntungan jadi Reseller</h2>

        <div class="benefit__12_card">
          <div class="card">
            <div class="card-body">
              <h5>Bisa mengikuti workshop menjadi Reseller Gratis</h5>
            </div>
          </div>
        </div>

        <div class="benefit__12_card">
          <div class="card">
            <div class="card-body">
              <h5>Berhak mendapatkan harga spesial Reseller di Warisan Gajahmada</h5>
            </div>
          </div>
        </div>

        <div class="benefit__12_card">
          <div class="card">
            <div class="card-body">
              <h5>Berkesempatan mengikuti games berhadiah khusus Harbolnas 12.12 di Club Sobat Badak</h5>
            </div>
          </div>
        </div>

      </div>
    </section>

    <section class="game__12">
      <div class="container">
        <h2>Ikuti juga Games Harbolnas 12.12!</h2>
      </div>

      <div class="owl-carousel">
        <div><img src="{{asset('images/event/1212/games/tp.png')}}" class="img-fluid"></div>
        <div><img src="{{asset('images/event/1212/games/akt.png')}}" class="img-fluid"></div>
        <div><img src="{{asset('images/event/1212/games/tys.png')}}" class="img-fluid"></div>
        <div><img src="{{asset('images/event/1212/games/sybj.png')}}" class="img-fluid"></div>
        <div><img src="{{asset('images/event/1212/games/ic.png')}}" class="img-fluid"></div>
        <div><img src="{{asset('images/event/1212/games/ps.png')}}" class="img-fluid"></div>
        <div><img src="{{asset('images/event/1212/games/pyb.png')}}" class="img-fluid"></div>
      </div>

    </section>

    <section class="footer__12">
      <div class="container">

        <div class="desc__12_btn">
          <button class="btn">Pendaftaran Sudah Ditutup</button>
        </div>

        <!-- <h2>Tunggu apa lagi? Yuk Daftar Sekarang! <br> Jangan sampai kehabisan slot ya!</h2>
        <button class="btn" data-toggle="modal" data-target="#modal_conf"><img class="mr-3" src="{{ asset('images/icons/google.svg') }}" alt=" "> Daftar Sekarang Menggunakan Google</button>

        <h5>Atau sudah punya akun Club Sobat Badak?</h5>
        <button class="btn csb__btn" data-toggle="modal" data-target="#modal_csb_conf"><img src="{{asset('images/csb-logo.png')}}" alt="" style="width: 35px; height: 35px; margin-right: 10px"> Daftar dengan Akun CSB</button> -->



      </div>
    </section>


    <!-- Modal-->
    <div class="modal fade modal__12" id="modal_conf" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h4 class="ketentuan">Ketentuan mengikuti Event Harbolnas 12.12 Club Sobat Badak:</h4>
                <ol>
                  <li>Pengiriman akan dilakukan mulai tanggal 27 Desember 2021 secara bertahap</li>
                  <li>Kuota/pulsa yang digunakan selama acara ditanggung oleh peserta acara</li>
                  <li>Yang berhak mengikuti kegiatan ini adalah Sobat Badak yang telah menyelesaikan transaksi</li>
                </ol>
                <span>Dengan klik tombol "Lanjut", Anda menyetujui semua ketentuan yang telah dibuat</span>

                    <div class="text-center">
                      <a href="{{ route('login.process-oAuth', 'google') }}">
                        <button class="btn">Lanjut</button>
                      </a>
                    </div>

            </div>
        </div>
    </div>

    <!-- Modal CSB conf-->
    <div class="modal fade modal__12" id="modal_csb_conf" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <h4 class="ketentuan">Ketentuan mengikuti Event Harbolnas 12.12 Club Sobat Badak:</h4>
                <ol>
                  <li>Pengiriman akan dilakukan mulai tanggal 27 Desember 2021 secara bertahap</li>
                  <li>Kuota/pulsa yang digunakan selama acara ditanggung oleh peserta acara</li>
                  <li>Yang berhak mengikuti kegiatan ini adalah Sobat Badak yang telah menyelesaikan transaksi</li>
                </ol>
                <span>Dengan klik tombol "Lanjut", Anda menyetujui semua ketentuan yang telah dibuat</span>

                <div class="text-center">
                    <button class="btn" onclick="openLoginModal()">Lanjut</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Success -->
    <div class="modal fade harbolnas__modal" id="modal_login" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <h2 style="margin-bottom: 25px">Daftar dengan Akun CSB</h2>
                <!-- <img src="{{asset('images/event/1111/success.png')}}" alt=""> -->
                <div class="form-auth">
                    <form class="form" id="form-auth" action="{{ route('processLogin') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input id="input-email" type="email" name="email" class="form-control form-control-solid"
                                placeholder="Email" required/>
                            <div class="input-text-error" id="error-email"></div>
                        </div>
                        <div class="form-group">
                            <div style="position: relative">
                                <input id="input-password" type="password" name="password"
                                    class="form-control form-control-solid" placeholder="Password" required />
                                <i class="fas fa-eye" id="togglePassword" onclick="togglePassword('input-password')"
                                    style="position: absolute; right:0.5rem;top:0.9rem; cursor: pointer;"></i>
                            </div>
                            <div class="input-text-error" id="error-password">
                            </div>
                        </div>

                        <div class="form-group text-left">
                            {!! NoCaptcha::display() !!}
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group d-flex justify-content-end">
                            <div>
                                <a href="{{ route('forgotPage') }}" class="forgot-pass-text">
                                    Lupa password?
                                </a>
                            </div>
                        </div>

                        <div class="form-button text-center">
                            <button type="submit" class="btn custom-btn-primary-rounded" style="width: unset;">Masuk</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('pageJS')
    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
    <script src="{{asset('assets/js/pages/features/miscellaneous/blockui.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="//www.google.com/recaptcha/api.js"></script>

    <script>
      AOS.init();
    </script>

    <script>
    function togglePassword(elmID) {
        let password = document.getElementById(elmID);
        if (password.type === "password") {
            password.type = "text";
            $("#togglePassword").removeClass("fa-eye")
            $("#togglePassword").addClass("fa-eye-slash")
        } else {
            password.type = "password";
            $("#togglePassword").removeClass("fa-eye-slash")
            $("#togglePassword").addClass("fa-eye")
        }
    }

    $(function() {
        @if (Session::has('error'))
            Swal.fire("Gagal!", "Email atau password salah!", "error")
        @endif
    })

    const openLoginModal = () => {
      $(`#modal_csb_conf`).modal('hide')
      $(`#modal_login`).modal('toggle')
    }
    </script>

    <script>
    $(document).ready(function(){
      $(".owl-carousel").owlCarousel({
          center: true,
          items:3.5,
          loop:true,
          margin:10,
          autoplay: true,
          responsive:{
              0 : {
                items:1.5
              },
                // breakpoint from 480 up
              425 : {
                items: 2.5
              },
              768 : {
                items: 3.5
              },
              1024 : {
                items: 4
              }
          }
      });
    });
    </script>

    <script>
    $("#reseller").click(function() {
       $('html, body').animate({
           scrollTop: $(".benefit__12").offset().top - 150
       }, 1500);
    });

    $("#hero__btn").click(function() {
       $('html, body').animate({
           scrollTop: $(".footer__12").offset().top - 150
       }, 1500);
    });
    </script>
@endsection
