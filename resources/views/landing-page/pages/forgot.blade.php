@section('title')
    Forgot Password
@endsection

@extends('landing-page.auth-master')

@section('content')
    <section class="section-auth">
        <div class="container">
            <center>
                <div class="form-logo">
                    <img src="/images/csb-logo-big.png" alt="">
                </div>
                <div class="black-bold-32">
                    Kamu lupa password?
                </div>
                <div class="black-normal-18">
                    Masukin emailmu disini dan kamu akan diarahkan untuk reset passwordmu.
                </div>
                <div class="form-auth">
                    <form class="form" id="form-forgot" action="{{ route('sendEmailforgotPassword') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input id="input-email" type="email" name="email" class="form-control form-control-solid"
                                placeholder="Email" required />
                            <div class="input-text-error" id="error-email">
                            </div>
                        </div>
                        <div class="form-button">
                            <button id="buttom-submit" type="submit" class="btn btn-blue-auth">Kirim</button>
                            <a href="/login" id="buttom-submit" class="btn btn-outline-blue-auth">Batal</a>
                        </div>
                    </form>
                </div>
            </center>
        </div>
    </section>
@endsection

@section('pageJS')
    <script>
        $(function() {
            $('#form-forgot').on('submit', function(e) {
                let email = $('#input-email').val();
                let isValid = email.length > 0;

                if (!isValid) {
                    e.preventDefault();
                    if (email.length < 1) {
                        $("#error-email").text("Email is required!")
                        $("#error-email").show()

                        Swal.fire("Warning!", "Email tidak boleh kosong!", "error")
                    }
                } else {
                    $("#error-password").text("")
                    $("#error-password").hide()

                    $("#error-email").text("")
                    $("#error-email").hide()
                }
            })
        })

        @if (Session::has('Success'))
            Swal.fire("Email berhasil terkirim!", "Tolong cek email kamu untuk melanjutkan proses penggantian password",
            "success")
            .then(function(result) {
            window.location.href = "/";
            });
        @endif

        @if (Session::has('Failed'))
        
            @if (Session::get('Failed') == 1)
                Swal.fire("Warning!", "Email tidak dapat ditemukan atau tidak valid!", "error");
            @elseif (Session::get('Failed') == 2)
                Swal.fire("Warning!", "Email belum terdaftar!", "error");
            @endif
        
        @endif
    </script>
@endsection
