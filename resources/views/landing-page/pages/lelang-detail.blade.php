@extends('landing-page.master')

@section('content')
    <section class="lelang_hero">
        <img src="{{ asset('images/lelang/hero.png') }}" alt="" style="width: 100%">
    </section>

    <section class="section-content" style="margin-bottom: 50px">
        <div class="container">
            {{-- <div class="lelang_heading">
                <h2>Gak jaman barang impian cuman dibayangin, <br>
                    <span>YOK LAH wujudkan tanpa perlu ribet!</span>
                </h2>
            </div> --}}

            <div class="row">
                <div class="col-lg-6 lelang_detail_col left">
                    <img src="{{ asset('images/lelang/ip12.png') }}" alt="">
                </div>

                <div class="col-lg-6 lelang_detail_col">
                    <h2>iPhone 12</h2>
                    <h5>Minimum Bid: <span>50.000 Baper Poin</span></h5>

                    {{-- Detail Navigation --}}

                    <ul class="nav nav-tabs nav-tabs-line lelang_detail_nav">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1">Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_2">Leaderboard</a>
                        </li>

                    </ul>
                    <div class="tab-content mt-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel"
                            aria-labelledby="kt_tab_pane_2">
                            <p>iPhone XR (iPhone X🅁, angka Romawi "X" dibaca "ten") adalah sebuah smartphone yang didesain
                                dan diproduksi oleh Apple, Inc.</p>

                            <p>
                                Merupakan generasi ke duabelas dari iPhone. Diumumkan oleh CEO Apple Tim Cook pada tanggal
                                12 September 2018, di Steve Jobs Theater kampus Apple Park, bersama dengan iPhone XS dan
                                iPhone XS Max yang berharga lebih tinggi. Pemesanan dimulai pada tanggal 19 Oktober 2018,
                                dengan rilis resmi pada tanggal 26 Oktober 2018.</p>

                            <div class="lelang_detail_winner">
                                <h5>Winner</h5>
                                <div class="plex">
                                    <img src="{{ asset('images/badak-baper.png') }}" alt="">
                                    <p>Bambang Subambang</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                            <div class="lelang_detail_table">
                                <table class="table table-borderless table-hover">
                                    <thead>
                                        <tr>
                                            <th>Peringkat</th>
                                            <th>Nama</th>
                                            <th>Bid</th>
                                            <th>Waktu Bid</th>
                                        </tr>
                                    </thead>
                                    <tbody id="leaderBoardAuction">

                                        <tr>
                                            <td class="number first">1</td>
                                            <td>Bambang</td>
                                            <td>20.000</td>
                                            <td>15 Juli 2021</td>
                                        </tr>
                                        <tr>
                                            <td class="number second">2</td>
                                            <td>Bambang</td>
                                            <td>20.000</td>
                                            <td>15 Juli 2021</td>
                                        </tr>
                                        <tr>
                                            <td class="number third">3</td>
                                            <td>Bambang</td>
                                            <td>20.000</td>
                                            <td>15 Juli 2021</td>
                                        </tr>
                                        <tr>
                                            <td class="number">4</td>
                                            <td>Bambang</td>
                                            <td>20.000</td>
                                            <td>15 Juli 2021</td>
                                        </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- ---------------- --}}
                </div>
            </div>
        </div>

    </section>
@endsection


@section('pageJS')


@endsection
