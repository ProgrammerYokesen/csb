@extends('landing-page.master')

@section('content')
    <section class="section-content" style="position: relative">
        <div class="container" style="position: relative; z-index: 2">
            <div class="row reseller__hero">
                <div class="col-lg-6">
                    <h1>CUAN JUTAAN RUPIAH HANYA DARI JADI RESELLER?!</h1>
                    <p>Siapa bilang jadi reseller ga bisa hasilin untung banyak? Nih, coba dulu gabung jadi reseller Larutan
                        Penyegar Cap Badak edisi Koin Gatotkaca sekarang dan dapetin berbagai keuntungan secara mudah!
                    </p>
                    <p>Yuk, kepoin lengkapnya di sini!</p>
                    <button class="btn btn__hero">DAFTAR <img src="{{ asset('images/reseller/icons/right.svg') }}" alt=""
                            style="margin-top: -3px; margin-left: 5px"></button>
                </div>
                <div class="col-lg-6">
                    <img class="reseller__hero_banner" src="{{ asset('images/reseller/landing-hero.png') }}" alt="">
                </div>
            </div>
        </div>

        <img src="{{ asset('images/reseller/icons/hero-orn.png') }}" alt="" class="reseller__hero_orn">
    </section>

    <section class="section-content reseller__desc">
        <div class="container text-center">
            <h2>Mau tau caranya?</h2>
            <h5>Ini dia 3 langkah mudah buat gabung jadi Reseller Larutan Penyegar Cap Badak edisi Koin Gatotkaca</h5>
        </div>
    </section>

    <section class="slider_section">
        {{-- Lelang Arrow --}}
        <div class="text-center slick__btn">
            <button class="slick-prev lelang_carousel_arrow btn" aria-label="Previous" type="button" style="">
                <i class="fas fa-chevron-left"></i>
            </button>

            <button class="slick-next lelang_carousel_arrow btn" aria-label="Next" type="button" style="">
                <i class="fas fa-chevron-right"></i>
            </button>
        </div>
        <div class="how_carousel">
            <div class="carousel__item row slick-slide slick-current slick-active slick-center" data-slick-index="0"
                aria-hidden="false" tabindex="0">
                <div class="col-sm-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img class="index__img" style="width: 64px;height:64px"
                            src="{{ asset('images/reseller/icons/1.png') }}" alt="">
                        <p class="carousel_title">
                            Isi Data Diri
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Gampang banget, kalian tinggal isi form yang ada di halaman ini, ya!
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/reseller/slider1.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-sm-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img class="index__img" style="width: 64px;height:64px"
                            src="{{ asset('images/reseller/icons/2.png') }}" alt="">
                        <p class="carousel_title">
                            Konfirmasi Pendaftaran
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Nah, setelah itu kalian bakal dihubungi sales representatif kita buat konfirmasi data diri
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12 text-center">
                    <div class="pt-6 img__carousel"><img src="{{ asset('images/reseller/slider2.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-sm-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img class="index__img" style="width: 64px;height:64px"
                            src="{{ asset('images/reseller/icons/3.png') }}" alt="">
                        <p class="carousel_title">
                            Order barang untuk toko mu
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Order barang dari Warisan Gajahmada pusat.
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/reseller/slider3.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-sm-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img class="index__img" style="width: 64px;height:64px"
                            src="{{ asset('images/reseller/icons/1.png') }}" alt="">
                        <p class="carousel_title">
                            Isi Data Diri
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Gampang banget, kalian tinggal isi form yang ada di halaman ini, ya!
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/reseller/slider1.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-sm-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img class="index__img" style="width: 64px;height:64px"
                            src="{{ asset('images/reseller/icons/2.png') }}" alt="">
                        <p class="carousel_title">
                            Konfirmasi Pendaftaran
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Nah, setelah itu kalian bakal dihubungi sales representatif kita buat konfirmasi data diri
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12 text-center">
                    <div class="pt-6 img__carousel"><img src="{{ asset('images/reseller/slider2.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-sm-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img class="index__img" style="width: 64px;height:64px"
                            src="{{ asset('images/reseller/icons/3.png') }}" alt="">
                        <p class="carousel_title">
                            Order barang untuk toko mu
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Order barang dari Warisan Gajahmada pusat.
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/reseller/slider3.png') }}" alt=""></div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-content reseller__benefit">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Kalo jadi Reseller Larutan Penyegar Cap Badak edisi Gatotkaca bisa untung ini semua? BENERAN?</h2>
                    <h5>Siapa yang ga tau Koin Gatotkaca? Ituloh, Koin VIRAL yang banyak dicari sama orang-orang! Eits, koin
                        ini bisa datengin keuntungan besar buat kamu, loh!</h5>
                    <div class="d-flex" style="align-items: center">
                        <button class="btn btn__hero">Daftar Sekarang</button>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body d-flex align-items-center">
                            <img src="{{ asset('images/reseller/icons/koin.svg') }}" alt="" class="mr-5">
                            <div>
                                <h4>Koin VIRAL yang kejual 100 pack setiap harinya! </h4>
                                <h5>Beneran sebanyak itu! Masih ragu juga, nih?</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body d-flex align-items-center">
                            <img src="{{ asset('images/reseller/icons/cuan.svg') }}" alt="" class="mr-5">
                            <div>
                                <h4>CUAN sampai dengan 25%!</h4>
                                <h5>Produknya cepet kejual, untungnya juga makin cepet & besar!</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body d-flex align-items-center">
                            <img src="{{ asset('images/reseller/icons/materi.svg') }}" alt="" class="mr-5">
                            <div>
                                <h4>Materi promosi DISEDIAIN!</h4>
                                <h5>Kalian langsung terima beres semua bahan konten! Tinggal simpan, copy, terus sebarin
                                    deh! Gampang banget ga sih?</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section-content container reseller__bukti">
        <div class=" text-center">
            <h2>Kepo sama buktinya?</h2>
            <h5>Biar kalian pada yakin, nih langsung liat aja buktinya!
                Udah viral, daftarnya mudah, dijamin bakal bisa hasilin untung yang besar untuk kamu!
            </h5>
        </div>
        <div class="d-flex" style="align-items: center;padding-top: 2rem">
            <img class="tabel__img" src="{{ asset('/images/reseller/bukti-2.png') }}" alt="">
            <img class="sign_hand" src="{{ asset('/images/reseller/champ.png') }}" alt="">
        </div>
        <div class="d-flex" style="align-items: center">
            <img class="sign_hand" src="{{ asset('/images/reseller/like.png') }}" alt="">
            <img class="tabel__img" src="{{ asset('/images/reseller/bukti-1.png') }}" alt="">
        </div>
    </section>

    <section class="section-content reseller__form">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 left__img">
                    <img src="{{ asset('images/reseller/landing-form.png') }}" alt="">
                </div>
                <div class="col-lg-6">
                    <h2>Yuk, langsung gabung jadi Reseller Larutan Penyegar Cap Badak edisi Koin Gatotkaca sekarang juga!
                    </h2>
                    <h6>Jangan nunggu-nunggu lagi! Cuan sudah menanti nih!</h6>
                    <form action="#" method="post" class="form_daftar">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nama" name="nama" autocomplete="off"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email" name="email" autocomplete="off"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" name="password"
                                autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control" placeholder="No. Whatsapp" name="whatsapp"
                                autocomplete="off" required>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="linktoko">
                            <label class="form-check-label" for="linktoko">Saya sudah memiliki toko online</label>
                        </div>
                        <div class="form-group" id="url_tf">
                            <input type="url" class="form-control" placeholder="Link Toko" name="linkToko"
                                autocomplete="off">
                            <span style="font-size: 12px; opacity: 0.4">*Harap isi dengan contoh format seperti ini:
                                <em>https://namaweb.xyz</em></span>
                        </div>

                        <div class="d-flex justify-content-start">
                            <button type="submit" class="btn primary__btn" style="margin-top: 2rem">Daftar
                                Sekarang</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 right__img">
                    <img src="{{ asset('images/reseller/landing-form.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="section-content container reseller__footer">
        <div class="row">
            <div class="col-md-4 col-12">
                <img src="{{ asset('images/reseller/landing-footer.png') }}" alt="" class="mr-5">
            </div>
            <div class="col-md-8 col-12">
                <div>
                    <h2>Masih mau tanya-tanya? Tenang aja, Admin kita selalu siap sedia buat menjawab setiap pertanyaan
                        kalian!
                    </h2>
                    <button class="btn">Tanya via Whatsapp</button>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('pageJS')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script>
        $(function() {
            $('#url_tf').hide()
            $('#linktoko').change(function() {
                if (this.checked) {
                    $('#url_tf').show()
                } else {
                    $('#url_tf').hide()
                }
            });
        })

        $('.how_carousel').slick({
            centerMode: true,
            arrows: true,
            slidesToShow: 3,
            contentPadding: '20px',
            // autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            variableWidth: true,
            nextArrow: '.slick-next',
            prevArrow: '.slick-prev',
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>
@endsection
