@extends('landing-page.master')


@section('content')
    <section class="hut_hero">
        <img src="{{ asset('images/bg-17.png') }}" alt="" style="width: 100%" class="hut_img_desk">
        <img src="{{ asset('images/lomba/mainmenu/bg-425.png') }}" alt="" style="width: 100%" class="hut_img_mbl">
    </section>

    <section class="hut__card">
        <div class="container">

            <div class="card">
                <div class="card-body">
                    <h3>YUK DAFTAR DISINI!</h3>
                    <p>PPKM (Pesta Penuh Kejutan Menarik) adalah rangkaian acara yang diadakan oleh Club Sobat Badak
                        dalam rangka merayakan HUT RI ke-76. Acara ini akan diisi dengan berbagai lomba menarik dan juga
                        konser online yang pastinya seru banget! Jadi, jangan lupa ikutan, ya!</p>
                    <a href="{{ route('registerPage') }}">
                        <button class="btn">DAFTAR</button>
                    </a>
                </div>
            </div>

            <div class="lomba__thumb">
                <div class="row g-3">
                    <div class="col-lg-4 lomba__thumb_img">
                        <img src="{{ asset('images/lomba/mainmenu/panjat.png') }}" alt="" class="img-fluid"
                            style="cursor: pointer">
                    </div>
                    <div class="col-lg-4 lomba__thumb_img">
                        <img src="{{ asset('images/lomba/mainmenu/pompa.png') }}" alt="" class="img-fluid"
                            style="cursor: pointer">
                    </div>
                    <div class="col-lg-4 lomba__thumb_img">
                        <img src="{{ asset('images/lomba/mainmenu/kerupuk.png') }}" alt="" class="img-fluid"
                            data-toggle="modal" data-target="#modal_kerupuk" style="cursor: pointer">
                    </div>
                    <div class="col-lg-4 lomba__thumb_img">
                        <img src="{{ asset('images/lomba/mainmenu/fashion.png') }}" alt="" class="img-fluid"
                            data-toggle="modal" data-target="#modal_fashion" style="cursor: pointer">
                    </div>
                    <div class="col-lg-4 lomba__thumb_img">
                        <img src="{{ asset('images/lomba/mainmenu/tebak.png') }}" alt="" class="img-fluid"
                            data-toggle="modal" data-target="#modal_tebak" style="cursor: pointer">
                    </div>
                    <div class="col-lg-4 lomba__thumb_img">
                        <img src="{{ asset('images/lomba/mainmenu/lagu.png') }}" alt="" class="img-fluid"
                            data-toggle="modal" data-target="#modal_lagu" style="cursor: pointer">
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection
