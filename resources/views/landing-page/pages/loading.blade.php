<section id="loading-section" class="elm-loading d-flex">
    <div class="body container text-center">
        <div class="loading-head">
            <p class="black70-normal-24">
                SELAMAT DATANG
            </p>
            <p class="black-bold-48">
                Club Sobat Badak
            </p>
        </div>
        <div class="loading-body d-flex">
            <div class="progress">
                <div id="custom-progress" class="progress-bar progress-bar-striped bg-primary" role="progressbar"
                    style="width: 0%" aria-valuemin="0" aria-valuemax="100"></div>
            </div> 
            <p id="percentage" class="ml-3 black-bold-22">0%</p>
        </div>

        <div class="loading-tail">
            <p class="black-normal-22">
                Tempat kamu belajar, ngobrol, bertukar cerita dan pengalaman setiap hari bersama Sobat Badak lainnya
                di
                seluruh Indonesia
            </p>
        </div>
    </div>
</section>

<script>
    $(function() {
        move()
    })
    var i = 0;

    function move() {
        if (i == 0) {
            i = 1;
            var elem = document.getElementById("custom-progress");
            var percentage = document.getElementById("percentage"); 
            var width = 1;
            var id = setInterval(frame, 15);

            function frame() {
                if (width >= 100) {
                    var loading = document.getElementById("loading-section");
                    var mobile = document.getElementById("kt_header_mobile");
                    var desktop = document.getElementById("header-desktop");

                    clearInterval(id);
                    i = 0;

                    loading.style.opacity = "0"
                    loading.style.visibility = "hidden"
                    desktop.style.display = "flex"
                    if (screen.width < 992) {
                        mobile.style.display = "flex"
                    }
                } else {
                    width++;
                    elem.style.width = width + "%";
                    percentage.textContent = `${width}%` 
                }
            }
        }
    }
</script>
