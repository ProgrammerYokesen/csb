@extends('landing-page.master')

@section('content')
    <section class="section-content">
        <div class="container">
            <div class="disclaimer__wrapper">
                <h1>Syarat dan Ketentuan</h1>
                <h6 style="margin-bottom: 50px">PERATURAN UMUM BESERTA SYARAT DAN KETENTUAN (TERMS OF SERVICES) CLUB SOBAT
                    BADAK (CSB)</h6>

                <div class="one_sec">
                    <h6>UMUM</h6>
                    <p>Selamat datang di situs CSB yang berdiri dengan teknologi milik PT Yokesen Teknologi Indonesia
                        ("YTI")
                        dan dengan PT. Yokesen Media Kagumi (YMK) sebagai pengelola dan penyelenggara kegiatan didalamnya.
                        Club
                        Sobat Badak (CSB) merupakan komunitas yang dibentuk oleh oleh masyarakat Indonesia yang memiliki
                        situs
                        dengan nama sobatbadak.club. Situs ini berisi kegiatan komunitas berbagi ilmu dan keterampilan serta
                        berprinsip kebebasan menyatakan pendapat yang bertanggung jawab. Club Sobat Badak ini merupakan
                        komunitas yang dibangun secara mandiri dan tidak ada hubungannya dengan Larutan Penyegar Cap Badak
                        dari
                        PT. Sinde Budi Sentosa.</p>
                    <p>Sebelum melakukan akses ke sobatbadak.club kami menyarankan agar Anda membaca Peraturan Umum beserta
                        Syarat dan Ketentuan terlebih dahulu. Dengan mengakses dan mendaftar di sobatbadak.club, Anda setuju
                        untuk terikat dengan Peraturan Umum beserta Syarat dan Ketentuan ini. Jika Anda tidak menyetujui
                        Peraturan Umum beserta Syarat dan Ketentuan yang ditetapkan di bawah ini, mohon tidak mengakses dan
                        mendaftar di sobatbadak.club.</p>
                    <p>Peraturan Umum beserta Syarat dan Ketentuan ini dapat kami ubah sewaktu-waktu tanpa pemberitahuan
                        terlebih dahulu. Keputusan yang diambil berdasarkan peraturan ini bersifat mutlak dan tidak dapat
                        diganggu gugat.</p>
                </div>

                <div class="one_sec">
                    <h6>DEFINISI</h6>
                    <p>“CSB” adalah komunitas online untuk berbagi ilmu dan keterampilan bersama Sobat Badak.</p>
                    <p>“Sobatbadak.club” adalah situs komunitas online untuk berbagi ilmu dan keterampilan anggota komunitas
                        Club Sobat Badak.</p>
                    <p>“Pengelola” adalah PT. Yokesen Media Kagumi.</p>
                    <p>“Moderator” adalah Admin sobatbadak.club yang bisa kamu hubungi melalui nomor WhatsApp yang tersedia
                        di
                        situs atau ke <a href="mailto:clubsobatbadak@gmail.com">clubsobatbadak@gmail.com</a></p>
                    <p>“Pengguna” adalah pengunjung dan user yang sudah mendaftar.</p>
                    <p>“Pengunjung” adalah semua orang yang mengakses website sobatbadak.club.</p>
                    <p>“Spam” adalah penggunaan perangkat elektronik untuk mengirimkan dan mendaftarkan data seseorang yang
                        tidak valid ke server dan database kami.</p>
                </div>


                <div class="one_sec">
                    <h6>KETENTUAN UMUM PENGGUNAAN sobatbadak.club</h6>
                    <p>Halaman ini (beserta dokumen yang disebut di dalamnya) memuat syarat-syarat penggunaan yang menjadi
                        dasar
                        bagi PENGGUNA dalam mengakses sobatbadak.club. sobatbadak.club dikelola dan dimiliki oleh PT YTI,
                        suatu
                        Perseroan Terbatas yang didirikan berdasarkan hukum Republik Indonesia dan berdomisili di Indonesia.
                        Setiap pertanyaan mengenai sobatbadak.club dapat dikirim ke <a
                            href="mailto:clubsobatbadak@gmail.com">clubsobatbadak@gmail.com</a></p>
                    <p>Pastikan Anda membaca Ketentuan Penggunaan ini secara cermat. Dengan menggunakan dan/atau mengunjungi
                        dan/atau melakukan registrasi di sobatbadak.club maka Anda menyatakan bahwa : </p>

                    <ol>
                        <li>Anda setuju untuk terikat dengan Syarat dan Ketentuan yang ditetapkan serta kebijakan mengenai
                            perubahan atau perbaikan Syarat dan Ketentuan di kemudian hari. Anda dipersilahkan untuk tidak
                            menggunakan dan/atau mengunjungi sobatbadak.club jika tidak setuju untuk terikat dengan
                            Ketentuan
                            Penggunaan ini.</li>
                        <li>Anda yang dianggap belum dewasa berdasarkan ketentuan perundang-undangan yang berlaku, wajib
                            mendapatkan bimbingan dan pengawasan orang tua dalam melakukan akses sobatbadak.club</li>
                    </ol>

                    <ol>
                        <li>Ketentuan Registrasi Umum
                            <p>Untuk dapat mendaftar menjadi member, Anda diharuskan memahami dan menyetujui untuk mematuhi
                                hal-hal berikut:</p>
                            <ol>
                                <li>Melakukan pendaftaran/registrasi dengan menyampaikan Nama, alamat email, nomor handphone
                                    dan
                                    kota domisili.</li>
                                <li>sobatbadak.club berhak untuk menolak pendaftaran atau membatalkan pendaftaran terkait
                                    jika
                                    secara sistem terdeteksi spam</li>
                                <li>Anda bertanggung jawab untuk menjaga kerahasiaan e-mail dan password.
                                </li>
                                <li>sobatbadak.club tidak akan meminta password untuk alasan apapun, oleh karena itu
                                    sobatbadak.club menghimbau kepada MEMBER agar tidak memberikan password Anda kepada
                                    pihak
                                    manapun, baik kepada pihak ketiga maupun kepada pihak yang mengatasnamakan
                                    sobatbadak.club
                                </li>
                                <li>sobatbadak.club tidak bertanggung jawab atas kerugian atau kerusakan yang timbul dari
                                    penyalahgunaan akun MEMBER.</li>
                                <li>Anda bertanggungjawab sepenuhnya atas kebenaran data-data/identitas yang disampaikan
                                    kepada
                                    sobatbadak.club dalam melakukan pendaftaran/registrasi
                                </li>
                                <li>Sehubungan dengan hal tersebut diatas, kami tidak bertanggung jawab atas segala kerugian
                                    yang timbul yang diakibatkan atas ketidakbenaran data-data/identitas yang disampaikan
                                    kepada
                                    sobatbadak.club ataupun karena ketidakpatuhan terhadap ketentuan registrasi ini.
                                </li>
                            </ol>
                        </li>

                        <li>Penolakan Pertanggungjawaban/Disclaimer
                            <p>Akses Anda, penggunaan dan kepercayaan pada sobatbadak.club dan konten yang diakses melalui
                                sobatbadak.club sepenuhnya adalah risiko Anda sendiri. sobatbadak.club (termasuk, namun
                                tidak
                                terbatas pada situs, program, layanan, email dan password untuk login di website) disediakan
                                "sebagaimana adanya" atau "sebagaimana tersedia" tanpa adanya jaminan berupa apapun.
                            </p>
                            <p>sobatbadak.club dengan jelas menolak apapun dan segala yang termasuk garansi, termasuk,
                                tetapi
                                tidak terbatas pada, jaminan barang atau jasa yang diperdagangkan dan kecocokan untuk tujuan
                                tertentu.</p>

                            <h6>Batasan Tanggung Jawab
                            </h6>
                            <p>Dalam keadaan apapun dan sejauh diperbolehkan menurut hukum yang berlaku, baik
                                sobatbadak.club
                                afiliasi, karyawan, perwakilan hukum, administrator, penerus, wakil, ahli waris, dan
                                penerima
                                hibah dari sobatbadak.club, tidak bertanggung jawab baik secara langsung maupun tidak
                                langsung
                                atas segala akibat, kerugian dan/atau pelanggaran hukum yang timbul dari penggunaan situs
                                ini
                                atau situs pihak ketiga yang terhubung ke situs ini, termasuk namun tidak terbatas pada
                                komunikasi yang diposting, diunggah, ditampilkan atau dikirimkan oleh pengguna situs
                                sobatbadak.club pada situs ini atau situs pihak ketiga, informasi atau layanan yang
                                ditampilkan
                                pada situs ini, segala bentuk interaksi antar pengguna sobatbadak.club, segala bentuk
                                partisipasi Anda dalam kegiatan yang diadakan sobatbadak.club.
                            </p>
                            <p>Segala informasi yang diposting di sobatbadak.club adalah tanggung-jawab orang atau
                                orang-orang
                                yang melakukan posting pesan tersebut. Pengguna manapun yang melanggar syarat dan ketentuan
                                dapat dilarang selamanya untuk menggunakan sobatbadak.club. Anda mengerti bahwa seluruh
                                konten
                                yang ditampilkan, atau dikirimkan melalui, atau terhubung melalui sobatbadak.club, adalah
                                sepenuhnya tanggung-jawab dari orang yang di awal menyebarkan komunikasi tersebut. Anda
                                mengerti
                                bahwa sobatbadak.club tidak mengendalikan, dan tidak bertanggung-jawab atas konten yang
                                tersedia
                                di sobatbadak.club. Anda setuju bahwa sobatbadak.club tidak melakukan penyaringan,
                                pemantauan
                                atau menyetujui konten apapun, tapi sobatbadak.club akan memiliki hak, tapi bukan keharusan
                                untuk menghapus, memindahkan (termasuk memindahkan konten atau posting ke bagian lain),
                                menolak,
                                mengubah atau menghapus komunikasi apapun untuk alasan apapun. sobatbadak.club tidak akan
                                bertanggung-jawab atas interaksi antar pengguna sobatbadak.club. Hubungan antar pengguna
                                melalui
                                sobatbadak.club adalah murni antara Anda dan pengguna lainnya. Dalam keadaan apapun,
                                sobatbadak.club tidak akan bertanggung-jawab atas barang, jasa, sumber daya atau konten
                                apapun
                                yang tersedia melalui hubungan atau komunikasi dengan pihak ketiga tersebut, atau untuk
                                segala
                                bentuk kecelakaan atau kerugian atau kerusakan terkait dengan hal tersebut. sobatbadak.club
                                tidak memiliki kewajiban untuk terlibat dalam segala bentuk perselisihan antara Anda dan
                                pengguna sobatbadak.club lainnya atau antara Anda dan pihak ketiga lainnya. Anda setuju
                                bahwa
                                layanan sobatbadak.club adalah jasa untuk para pengguna sobatbadak.club. Apabila disuatu
                                hari
                                terjadi perselisihan antara Anda dan salah satu pengguna sobatbadak.club lainnya, Anda
                                setuju
                                bahwa sobatbadak.club tidak memiliki kewajiban apapun untuk terlibat dalam hal tersebut.
                                Anda
                                juga setuju untuk membebaskan sobatbadak.club dari semua dan segala klaim, tuntutan, dan
                                kerugian yang timbul akibat atau sehubungan dengan perselisihan tersebut. Anda sepenuhnya
                                bertanggung-jawab atas konten apapun yang Anda posting dan juga segala konten yang di
                                posting
                                melalui akun Anda.
                            </p>
                            <p>Anda mengakui, memberikan persetujuan dan setuju bahwa sobatbadak.club mungkin mengakses,
                                menyimpan dan memberikan informasi dan konten akun yang Anda unggah, posting
                                (kirimkan/tampilkan), atau dengan cara apapun ada di sobatbadak.club jika diharuskan oleh
                                hukum
                                yang berlaku atau dalam kepercayaan dengan niat baik bahwa akses, penyimpanan dan pemberian
                                informasi tersebut secara masuk akal diperlukan untuk: (i) mematuhi proses hukum; (ii)
                                mematuhi
                                persyaratan hukum yang diterapkan oleh hukum atau otoritas pemerintah pusat, daerah atau
                                lokal;
                                (iii) menjalankan syarat dan ketentuan ini; (iv) memberikan respon terhadap klaim bahwa
                                suatu
                                konten melanggar hak pihak ketiga; (v) memberikan respon terhadap permintaan Anda akan
                                layanan
                                pelanggan; atau (vi) melindungi hak, kepemilikan atau keamanan pribadi sobatbadak.club, para
                                penggunanya dan masyarakat umum.
                            </p>
                            <p>Informasi, layanan dan produk yang tersaji bagi Anda di situs ini mungkin memiliki kesalahan
                                dan
                                dapat terganggu pada suatu saat. Walaupun sobatbadak.club selalu berusaha sebaik mungkin
                                untuk
                                menjaga informasi, layanan dan produk yang ditawarkan di sobatbadak.club, sobatbadak.club
                                tidak
                                akan bertanggung-jawab atas segala kesalahan, kecacatan, kehilangan keuntungan atau kerugian
                                konsekuensial apapun yang terjadi akibat penggunaan sobatbadak.club.</p>
                        </li>

                        <li>Tautan/Link Pihak Ketiga
                            <p>sobatbadak.club mungkin berisi tautan ke situs lain yang dimiliki oleh selain
                                sobatbadak.club.
                                sobatbadak.club tidak bertanggung jawab atas konten situs-situs tersebut. Kami memiliki hak,
                                tetapi bukanlah merupakan suatu keharusan untuk melarang penggunaan tautan situs lain di
                                sobatbadak.club. Pencantuman tautan situs tersebut pada Situs sobatbadak.club bukan
                                merupakan
                                tanda pengabsahan materi yang ada pada situs atau tautan yang terkait dengan penyelenggara
                                tautan tersebut. Pencantuman tautan oleh MEMBER di layanan Situs sobatbadak.club adalah hak
                                dan
                                tanggung jawab langsung MEMBER. Entitas apapun yang Anda pilih sebagai kontak atau
                                berinteraksi
                                baik yang terdaftar dalam direktori atau di tempat lain pada situs tersebut adalah
                                sepenuhnya
                                tanggung jawab layanan tersebut pada Anda, dan Anda setuju bahwa sobatbadak.club tidak dapat
                                dikenakan tindakan apapun untuk kerusakan atau biaya yang keluar atas interaksi yang terjadi
                                antara Anda dengan layanan yang bersangkutan.
                            </p>
                        </li>

                        <li>Yurisdiksi dan hukum yang berlaku
                            <p>Syarat-syarat penggunaan ini dan hubungan Anda dengan sobatbadak.club diatur menurut hukum
                                Republik dan Anda sepakat untuk terikat pada yurisdiksi pengadilan Republik Indonesia.</p>
                        </li>

                        <li>Perubahan Ketentuan Layanan
                            <p>Atas kebijaksanaan sobatbadak.club, kami dapat merevisi syarat-syarat penggunaan ini pada
                                saat
                                kapanpun dengan mengubah halaman ini tanpa pemberitahuan sebelumnya. Anda diharapkan untuk
                                memeriksa halaman ini dari waktu ke waktu untuk memperhatikan setiap perubahan yang kami
                                buat,
                                karena hal itu mengikat Anda. Sebagian dari ketentuan yang terdapat dalam syarat-syarat
                                penggunaan ini juga dapat digantikan oleh ketentuan-ketentuan atau pemberitahuan yang
                                dipublikasi di bagian lain di situs sobatbadak.club.</p>
                        </li>
                    </ol>
                </div>

                <div class="one_sec">
                    <h6>KETENTUAN-KETENTUAN LAINNYA</h6>
                    <p><strong>sobatbadak.club Server Abuse / Tindakan Merusak Server sobatbadak.club</strong></p>
                    <p>Kami mempunyai hak untuk menentukan segala tindakan apakah tindakan tersebut merupakan tindakan
                        merusak
                        Server sobatbadak.club atau bukan, termasuk tetapi tidak terbatas crawling, spamming, scamming, DoS
                        (Denial of Service) dan DDoS (Distributed Denial of Service), dan Injection. Server sobatbadak.club
                        memiliki program untuk mendeteksi serangan terhadap server, apabila IP dan USERNAME Anda ternyata
                        tertera dalam daftar blacklist Server sobatbadak.club, maka USERNAME dan IP Anda akan kami BANNED
                        untuk
                        SELAMANYA Selain itu tindakan melalui jalur hukum juga akan diambil untuk kerugian yang kami alami.
                        Apabila Anda merasa tidak melakukan kesalahan tetapi masuk dalam daftar black list, harap
                        menghubungi CS
                        sobatbadak.club.</p>
                </div>

                <div class="one_sec">
                    <h6>Banned</h6>
                    <p>Apabila ada USERNAME yang di BANNED, kami mempunyai hak untuk menghapus semua postingan USERNAME
                        tersebut.</p>
                </div>

                <div class="one_sec">
                    <h6>Kebebasan Menyatakan Pendapat yang Bertanggungjawab/Responsible Freedom of Speech</h6>
                    <p>sobatbadak.club menganut prinsip Bebas Menyatakan Pendapat yang Bertanggung jawab (Responsible
                        Freedom of
                        Speech). Bagi Anda yang merasa dirugikan oleh MEMBER lain karena akibat dari prinsip ini harap
                        segera
                        menghubungi CS sobatbadak.club untuk diambil tindakan lebih lanjut.
                    </p>
                </div>

                <div class="one_sec">
                    <h6>Penggunaan Nama sobatbadak.club</h6>
                    <p>Menggunakan/mengatasnamakan sobatbadak.club dan turunannya untuk tujuan apapun dan untuk kepentingan
                        pribadi/kelompok tertentu tanpa seijin pihak sobatbadak.club adalah DILARANG! Pelanggaran terhadap
                        aturan ini bisa berakibat pelanggaran Hak Cipta & Hak Kekayaan Intelektual sobatbadak.club.</p>
                </div>

                <div class="one_sec">
                    <h6>Kritik/Saran</h6>
                    <p>Semua bentuk Kritik/Saran, harap dikirimkan melalui email ke <a
                            href="mailto:clubsobatbadak@gmail.com">clubsobatbadak@gmail.com</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
