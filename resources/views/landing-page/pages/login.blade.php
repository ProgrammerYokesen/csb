@section('title')
    Masuk
@endsection

@extends('landing-page.auth-master')

@section('content')
    @php
    if (Session::has('error')) {
    }
    @endphp
    <section class="section-auth">
        <div class="container">
            <center>
                <div class="form-logo">
                    <img src="/images/csb-logo-big.png" alt="">
                </div>
                <div class="black-bold-32">
                    Sini masuk yuk ke Dashboard
                </div>

                <div class="login_with_google">
                    <a href="{{ route('login.process-oAuth', 'google') }}">
                        <button>
                            <img src="{{ asset('images/icons/google.svg') }}" alt=" ">
                            Masuk Dengan Google
                        </button>
                    </a>
                </div>


                <div class="black-normal-22 mb-4">
                    Atau
                </div>


                {{-- <div class="black-normal-18">
                    Masukin email dan passwordmu dan bermain di Club Sobat Badak!
                </div> --}}
                <div class="form-auth">
                    <form class="form" id="form-auth" action="{{ route('processLogin') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input id="input-email" type="email" name="email" class="form-control form-control-solid"
                                placeholder="Email" />
                            <div class="input-text-error" id="error-email"></div>
                        </div>
                        <div class="form-group">
                            <div style="position: relative">
                                <input id="input-password" type="password" name="password"
                                    class="form-control form-control-solid" placeholder="Password" />
                                <i class="fas fa-eye" id="togglePassword" onclick="togglePassword('input-password')"
                                    style="position: absolute; right:0.5rem;top:0.9rem; cursor: pointer;"></i>
                            </div>
                            <div class="input-text-error" id="error-password">
                            </div>
                        </div>
                        <div class="form-group text-left">
                            {!! NoCaptcha::display() !!}
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group d-flex justify-content-between">
                            <div>
                                <label class="checkbox">
                                    <input type="checkbox" value="true" name="remember" />
                                    <span class="mr-2"></span>
                                    Ingat saya
                                </label>
                            </div>
                            <div>
                                <a href="/forgot-password" class="forgot-pass-text">
                                    Lupa password?
                                </a>
                            </div>
                        </div>
                        
                        <div class="form-button">
                            <button id="buttom-submit" class="btn custom-btn-primary-rounded">Masuk</button>
                        </div>
                    </form>
                </div>
                <div class="black-normal-15">
                    <b>Belum punya akun? <a href="/register" class="pass-text">Daftar!</a></b>
                </div>
            </center>
        </div>
    </section>

    <script>
        function togglePassword(elmID) {
            let password = document.getElementById(elmID);
            if (password.type === "password") {
                password.type = "text";
                $("#togglePassword").removeClass("fa-eye")
                $("#togglePassword").addClass("fa-eye-slash")
            } else {
                password.type = "password";
                $("#togglePassword").removeClass("fa-eye-slash")
                $("#togglePassword").addClass("fa-eye")
            }
        }

        $(function() {
            @if (Session::has('error'))
                Swal.fire("Gagal!", "Email atau password salah!", "error")
            @endif
        })
    </script>

    <script>
        function validateEmail(email) {
            const re =
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        $(function() {
            var emailValid = true

            $('#input-email').focusout(function() {
                let text = $(this).val();

                // validate email using regex
                if (validateEmail(text)) {
                    emailValid = true
                    $("#error-email").text("")
                    $("#error-email").hide()
                } else {
                    emailValid = false
                    $("#error-email").text("Please enter a vaild email address")
                    $("#error-email").show()
                }
            })

            $('#form-auth').on('submit', function(e) {
                let email = $('#input-email').val();
                let password = $('#input-password').val();
                let isValid = emailValid &&
                    email.length > 0 &&
                    password.length >= 6;

                if (!isValid) {
                    e.preventDefault();
                    if (email.length < 1) {
                        $("#error-email").text("Email is required")
                        $("#error-email").show()
                    }
                    if (password.length < 1) {
                        $("#error-password").text("Password is required")
                        $("#error-password").show()
                    } else if (password.length > 1 && password.length < 6) {
                        $("#error-password").text("Please enter min. 6 characters")
                        $("#error-password").show()
                    }
                } else {
                    $("#error-password").text("")
                    $("#error-password").hide()

                    $("#error-email").text("")
                    $("#error-email").hide()
                }
            })
        });
    </script>
@endsection

@section('pageJS')
<script src="//www.google.com/recaptcha/api.js"></script>
@endsection