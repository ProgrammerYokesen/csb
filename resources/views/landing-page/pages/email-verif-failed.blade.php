@extends('landing-page.auth-master')

@section('content')
    <section class="section-auth">
        <div class="container">
            <center>
                <div class="form-logo">
                    <img src="/images/csb-logo-big.png" alt="">
                </div>
                <div class="black-bold-32">
                    Email anda gagal diverifikasi!
                </div>
                <div class="black-normal-18">
                    Token verifikasi tidak ditemukan, silahkan kirim email konfirmasi lagi dan klik verifikasi pada email terbaru
                </div>

                <a href="{{ route('homeDash') }}">
                    <button class="btn btn-blue-auth">Lanjut</button>
                </a>
            </center>
        </div>
    </section>
@endsection
