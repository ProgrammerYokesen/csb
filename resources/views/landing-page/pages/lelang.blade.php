@extends('landing-page.master')

@section('content')
    <section class="lelang_hero">
        <img src="{{ asset('images/lelang/hero.png') }}" alt="" style="width: 100%">
    </section>

    <section class="section-content">
        <div class="container">
            <div class="lelang_heading">
                <h2>Gak jaman barang impian cuman dibayangin, <br>
                    <span>YOK LAH wujudkan tanpa perlu ribet!</span>
                </h2>
            </div>
        </div>

        {{-- Lelang Carousel --}}
        <div class="lelang_carousel">


            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/ip12.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>iPhone 12</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/tws.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>JBL Tune 220TWS</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/supreme.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>Supreme Bag</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/vacuum.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>Xiaomi Robot Vacuum</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/belt.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>Off White Belt</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/bape.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>BAPE Collage Slide Sandal</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/converse.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>Converse</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/adidas.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>Adidas NMD R1 Vapour Pink</h5>
                    </div>
                </div>
            </div>

            <div class="lelang_carousel_item">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <img src="{{ asset('images/lelang/sendal.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h5>Sandal</h5>
                    </div>
                </div>
            </div>


        </div>

        {{-- Lelang Arrow --}}
        <div class="text-center">
            <button class="slick-prev lelang_carousel_arrow" aria-label="Previous" type="button" style="">
                <i class="fas fa-chevron-left"></i>
            </button>

            <button class="slick-next lelang_carousel_arrow" aria-label="Next" type="button" style="">
                <i class="fas fa-chevron-right"></i>
            </button>
        </div>

        <div class="container">
            {{-- Lelang Desc --}}
            <div class="row justify-content-center align-items-center lelang_desc_wrapper">
                <div class="col-lg-6">
                    <div class="lelang_desc">
                        <h3>Ikuti auction viral barang-barang impianmu hanya di <span>Club Sobat Badak</span>!</h3>
                        <h6>Cuma dengan mengumpulkan Baper Poin melalui Games-Games yang ada, ajak Teman Sobat & transaksi
                            langsung di room Warisan Gajahmada Live</h6>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="lelang_desc lelang_desc_img">
                        <img src="{{ asset('images/lelang/csb.png') }}" alt="">
                    </div>
                </div>
            </div>


            @if ($riwayatLelang->count() > 0)
                {{-- Lelang History --}}
                <div class="lelang_history">

                    <div class="lelang_history_header">
                        <h2>Riwayat Auction</h2>


                        <div class="quick-search">
                            <!--begin:Form-->
                            <form method="get" class="quick-search-form">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="svg-icon svg-icon-lg">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                    viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                            fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                        <path
                                                            d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                            fill="#000000" fill-rule="nonzero" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </div>
                                    <input id="cari" type="text" class="form-control" placeholder="Cari..." />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="quick-search-close ki ki-close icon-sm text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                            <!--begin::Scroll-->
                            <div class="quick-search-wrapper scroll" data-scroll="true" data-height="325"
                                data-mobile-height="200"></div>
                            <!--end::Scroll-->
                        </div>


                    </div>
                    <div id="searchLelang">
                        <div class="row justify-content-center" id="infinite-data">
                            @foreach ($riwayatLelang as $rl)
                                <div class="col-lg-4 col-sm-4 col-6 lelang_history_item">
                                    <div class="card card-custom gutter-b">
                                        <div class="card-header">
                                            <img loading=”lazy” src="https://data.sobatbadak.club/{{ $rl->photo }}" alt="">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="mb-3">{{ $rl->name }}</h5>

                                            <div class="lelang_history_summary">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <span>Bid Tertinggi</span>
                                                        <p>{{ number_format($rl->bidTertinggi) }}</p>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <span>Pemenang</span>
                                                        <p>{{ $rl->username }}</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="lelang_history_summary">
                                                <span>Tanggal dan waktu</span>
                                                <p>{{ date('d M Y, H:i', strtotime($rl->starttime)) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="ajax-load text-center" style="display:none;align-content: center;">
                            <img src="{{asset('images/loader.gif')}}">
                        </div>

                        @if (count($riwayatLelang) == 3)
                            <div class="text-center">
                                {{-- Trigger Show More --}}
                                <button class="btn lelang_all" id="showMore">Lihat Selengkapnya</button>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
            
            
            <div class="lelang_testimoni" id="testimoni">
                <h1>Begini <span>Kata Mereka</span></h1>


                <div class="cardz-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img src="{{ asset('images/lelang/testimoni/1.jpeg') }}" alt="dummy-image">
                        </div>

                        <div class="swiper-slide">
                            <img src="{{ asset('images/lelang/testimoni/2.jpeg') }}" alt="dummy-image">
                        </div>

                        <div class="swiper-slide">
                            <video src="{{ asset('images/lelang/testimoni/3.mp4') }}#t=0.001" style="width: 100%"
                                controls></video>
                        </div>

                        <div class="swiper-slide">
                            <img src="{{ asset('images/lelang/testimoni/4.jpeg') }}" alt="dummy-image">
                        </div>

                        <div class="swiper-slide">
                            <video src="{{ asset('images/lelang/testimoni/5.mp4') }}#t=0.001" style="width: 100%"
                                controls></video>
                        </div>

                        <div class="swiper-slide">
                            <video src="{{ asset('images/lelang/testimoni/6.mp4') }}#t=0.001" style="width: 100%"
                                controls></video>
                        </div>

                        <div class="swiper-slide">
                            <img src="{{ asset('images/lelang/testimoni/7.jpeg') }}" alt="dummy-image">
                        </div>
                    </div>
                </div>
                
                <div class="slider__navigation">
                    <button class="btn__prev lelang_carousel_arrow"><i class="fas fa-chevron-left"></i></button>
                    <button class="btn__next lelang_carousel_arrow"><i class="fas fa-chevron-right"></i></button>
                </div>
    
            </div>


            <!--<div class="lelang_testimoni" id="testimoni">-->
            <!--    <h1>Begini <span>Kata Mereka</span></h1>-->


            <!--    <div class="cardz-slider">-->
            <!--        <div class="swiper-wrapper">-->
            <!--            <div class="swiper-slide">-->
            <!--                <img src="{{ asset('images/lelang/testimoni/1.jpeg') }}" alt="dummy-image">-->
            <!--            </div>-->

            <!--            <div class="swiper-slide">-->
            <!--                <img src="{{ asset('images/lelang/testimoni/2.jpeg') }}" alt="dummy-image">-->
            <!--            </div>-->

            <!--            <div class="swiper-slide">-->
            <!--                <video src="{{ asset('images/lelang/testimoni/3.mp4') }}" style="width: 100%"-->
            <!--                    controls></video>-->
            <!--            </div>-->

            <!--            <div class="swiper-slide">-->
            <!--                <img src="{{ asset('images/lelang/testimoni/4.jpeg') }}" alt="dummy-image">-->
            <!--            </div>-->

            <!--            <div class="swiper-slide">-->
            <!--                <video src="{{ asset('images/lelang/testimoni/5.mp4') }}" style="width: 100%"-->
            <!--                    controls></video>-->
            <!--            </div>-->

            <!--            <div class="swiper-slide">-->
            <!--                <video src="{{ asset('images/lelang/testimoni/6.mp4') }}" style="width: 100%"-->
            <!--                    controls></video>-->
            <!--            </div>-->

            <!--            <div class="swiper-slide">-->
            <!--                <img src="{{ asset('images/lelang/testimoni/7.jpeg') }}" alt="dummy-image">-->
            <!--            </div>-->
            <!--        </div>-->


            <!--    </div>-->

            <!--    <div class="slider__navigation">-->
            <!--        <button class="btn__prev lelang_carousel_arrow"><i class="fas fa-chevron-left"></i></button>-->
            <!--        <button class="btn__next lelang_carousel_arrow"><i class="fas fa-chevron-right"></i></button>-->
            <!--    </div>-->


            <!--</div>-->




            {{-- Lelang CTA --}}
            <div class="lelang_cta">
                <h1><span>Daftarkan</span> Dirimu Sekarang!</h1>
                <h5>Bermain games di Club Sobat Badak atau ajak teman kamu untuk join Club Sobat Badak kamu udah bisa ikutan
                    auction ini!</h5>

                <a href="{{ route('registerPage') }}">
                    <button class="btn">Bergabung Sekarang</button>
                </a>
            </div>
        </div>

    </section>
@endsection


@section('pageJS')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script>
        $('.lelang_carousel').slick({
            centerMode: true,
            centerPadding: '60px',
            arrows: true,
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            nextArrow: '.slick-next',
            prevArrow: '.slick-prev',
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
        document.getElementById("cari").oninput = function() {
            var data = $(this).val();
            console.log(data.length);
            if (data.length >= 2) {
                // Ajax get data
                console.log('masuk');
                $.ajax({
                    type: 'GET',
                    url: '{{ route('searchLelang') }}', // This is the url that will be requested
                    data: {
                        q: data
                    },
                    success: function(data) {
                        console.log(data.html);
                        document.getElementById('searchLelang').innerHTML = data.html;
                    }
                });
            } else if (data.length <= 1) {
                $.ajax({
                    type: 'GET',
                    url: '{{ route('searchLelang') }}', // This is the url that will be requested
                    success: function(data) {
                        console.log('masuk');
                        document.getElementById('searchLelang').innerHTML = data.html;
                    }
                });
            }
        };
    </script>
    
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        let previewSlide = 3
        if (window.screen.width <= 1024) {
            previewSlide = 3
        } else if (window.screen.width <= 426) {
            previewSlide = 1
        }

        var swiper = new Swiper('.cardz-slider', {
            slidesPerView: previewSlide,
            centeredSlides: true,
            loop: true,
            spaceBetween: 0,
            autoplay: true,
            navigation: {
                nextEl: ".btn__next",
                prevEl: ".btn__prev",
            }
        });
    </script>


    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        let previewSlide = 3
        if (window.screen.width <= 1024) {
            previewSlide = 3
        } else if (window.screen.width <= 426) {
            previewSlide = 1
        }

        var swiper = new Swiper('.cardz-slider', {
            slidesPerView: previewSlide,
            centeredSlides: true,
            loop: true,
            spaceBetween: 0,
            autoplay: true,
            navigation: {
                nextEl: ".btn__next",
                prevEl: ".btn__prev",
            },
        });
    </script>
    @include('dashboard.pages.showmore.showmore')
@endsection
