@extends('landing-page.auth-master')

@section('content')
    <section class="section-auth">
        <div class="container">
            <center>
                <div class="form-logo">
                    <img src="/images/csb-logo-big.png" alt="">
                </div>
                <div class="black-bold-32">
                    Email berhasil dikirim!
                </div>
                <div class="black-normal-18">
                    Silahkan cek email kamu untuk melanjutkan proses Reset Password
                </div>

                <a href="{{ route('defaultPage') }}">
                    <button class="btn btn-blue-auth">Kembali</button>
                </a>
            </center>
        </div>
    </section>
@endsection
