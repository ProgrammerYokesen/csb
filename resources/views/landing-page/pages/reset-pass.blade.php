@extends('landing-page.auth-master')

@section('content')
    <section class="section-auth">
        <div class="container">
            <center>
                <div class="form-logo">
                    <img src="/images/csb-logo-big.png" alt="">
                </div>
                {{-- <div class="black-bold-32">
                    Kamu lupa password ?
                </div> --}}
                <div class="black-bold-32">
                    Masukkan password baru kamu disini.
                </div>
                <div class="form-auth">
                    <form class="form" action="{{ route('resetPassword') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <div class="d-flex" style="position: relative">
                                <input id="input-password" type="password" name="password"
                                    class="form-control form-control-solid" placeholder="Password" />
                                <i class="fas fa-eye input-password" id="togglePassword"
                                    onclick="togglePassword('input-password')"
                                    style="position: absolute; right:3rem;top:0.9rem;cursor: pointer;"></i>
                                <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                    data-toggle="tooltip" data-placement="right" title="Password wajib diisi">
                            </div>
                            <div class="input-text-error" id="error-password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex" style="position: relative">
                                <input id="input-confirm" type="password" name="password_confirmation"
                                    class="form-control form-control-solid" placeholder="Konfirmasi Password" />
                                <i class="fas fa-eye input-confirm" id="togglePassword"
                                    onclick="togglePassword('input-confirm')"
                                    style="position: absolute; right:3rem;top:0.9rem;cursor: pointer;"></i>
                                <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                    data-toggle="tooltip" data-placement="right"
                                    title="Wajib diisi dan sama dengan password di atas">
                            </div>
                            <div class="input-text-error" id="error-confirm">
                            </div>
                        </div>
                        <div class="form-button">
                            <button id="buttom-submit" type="submit" class="btn btn-blue-auth">Simpan</button>
                        </div>
                    </form>
                </div>
            </center>
        </div>
    </section>

    <script>
        function togglePassword(elmID) {
            let password = document.getElementById(elmID);
            if (password.type === "password") {
                password.type = "text";
                console.log(`#${elmID}`)
                $(`.${elmID}`).removeClass("fa-eye")
                $(`.${elmID}`).addClass("fa-eye-slash")
            } else {
                password.type = "password";
                $(`.${elmID}`).removeClass("fa-eye-slash")
                $(`.${elmID}`).addClass("fa-eye")
            }
        }
    </script>

@endsection
