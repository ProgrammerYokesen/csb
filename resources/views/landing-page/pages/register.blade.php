@section('title', 'Registrasi')
    @extends('landing-page.auth-master')
@section('content')
    <section class="section-auth">
        <div class="container">
            <center>
                <div class="form-logo">
                    <img src="/images/csb-logo-big.png" alt="">
                </div>
                <div class="black-bold-32">
                    Daftar dulu sini
                </div>

                <!-- <div class="login_with_google">
                    <a href="{{ route('login.process-oAuth', 'google') }}">
                    <button>
                        <img src="{{ asset('images/icons/google.svg') }}" alt=" ">
                        Masuk Dengan Google
                    </button>
                    </a>
                </div>


                <div class="black-normal-22 mb-4">
                    Atau
                </div> -->

                <div class="form-auth">
                    <form id="form-auth" class="form" action="{{ route('processRegister') }}" method="post">
                        @csrf
                        <div class="form-group pr-8">
                            <input id="input-name" type="name" name="name" class="form-control form-control-solid"
                                placeholder="Nama" />
                            <div class="input-text-error" id="error-name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex">
                                <input id="input-email" type="email" name="email" class="form-control form-control-solid"
                                    placeholder="Email" />
                                <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                    data-toggle="tooltip" data-placement="right" title="Email wajib diisi">
                            </div>
                            <div class="input-text-error" id="error-email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text"
                                            style="border: transparent">+62</span></div>
                                    <input id="input-wa" name="whatsapp" type="number"
                                        class="form-control form-control-solid" placeholder="No. Whatsapp" />
                                </div>
                                <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                    data-toggle="tooltip" data-placement="right"
                                    title="Wajib diisi nomor whatsapp yang aktif untuk pengiriman link Zoom">
                            </div>
                            <div class="input-text-error" id="error-wa">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex" style="position: relative">
                                <input id="input-password" type="password" name="password"
                                    class="form-control form-control-solid" placeholder="Password" />
                                <i class="fas fa-eye input-password" id="togglePassword"
                                    onclick="togglePassword('input-password')"
                                    style="position: absolute; right:3rem;top:0.9rem;cursor: pointer;"></i>
                                <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                    data-toggle="tooltip" data-placement="right" title="Password wajib diisi">
                            </div>
                            <div class="input-text-error" id="error-password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex" style="position: relative">
                                <input id="input-confirm" type="password" name="password_confirmation"
                                    class="form-control form-control-solid" placeholder="Konfirmasi Password" />
                                <i class="fas fa-eye input-confirm" id="togglePassword"
                                    onclick="togglePassword('input-confirm')"
                                    style="position: absolute; right:3rem;top:0.9rem;cursor: pointer;"></i>
                                <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                    data-toggle="tooltip" data-placement="right"
                                    title="Wajib diisi dan sama dengan password di atas">
                            </div>
                            <div class="input-text-error" id="error-confirm">
                            </div>
                        </div>
                        <div class="form-group text-left">
                            {!! NoCaptcha::display() !!}
                            @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                             @endif
                        </div>
                        <div class="form-group mt-4">
                            <div>
                                <label class="checkbox">
                                    <input type="checkbox" value="1" name="remember" />
                                    <span class="mr-2"></span>
                                    Saya bersedia dihubungi melalui Email
                                </label>
                            </div>
                        </div>
                        <div class="form-button">
                            <button id="submitButton" type="submit" class="btn custom-btn-primary-rounded">Daftar</button>
                        </div>
                    </form>
                </div>

                <div class="black-normal-15">
                    <b>Sudah punya akun? <a href="{{ route('loginPage') }}" class="pass-text">Masuk!</a></b>
                </div>
            </center>
        </div>
    </section>

    <script>
        function togglePassword(elmID) {
            let password = document.getElementById(elmID);
            if (password.type === "password") {
                password.type = "text";
                console.log(`#${elmID}`)
                $(`.${elmID}`).removeClass("fa-eye")
                $(`.${elmID}`).addClass("fa-eye-slash")
            } else {
                password.type = "password";
                $(`.${elmID}`).removeClass("fa-eye-slash")
                $(`.${elmID}`).addClass("fa-eye")
            }
        }

        $(function() {
            @if ($errors->has('email'))
                Swal.fire("Gagal!", "Email telah dipakai!", "error");
            @endif
            @if ($errors->has('whatsapp'))
                Swal.fire("Gagal!", $errors->get('whatsapp'), "error");
            @endif
        })
    </script>

    <script>
        function validateEmail(email) {
            const re =
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        function validateNumber(number) {
            var reg = /^\d+$/;
            return reg.test(number);
        }

        $(function() {
            var emailValid = true
            var waValid = true
            var nameValid = true

            $('#input-name').focusout(function() {
                let text = $(this).val();

                // validate name using regex
                if (text.length > 0) {
                    nameValid = true
                    $("#error-name").text("")
                    $("#error-name").hide()
                } else {
                    nameValid = false
                    $("#error-name").text("Nama tidak boleh kosong")
                    $("#error-name").show()
                }
            })

            $('#input-email').focusout(function() {
                let text = $(this).val();

                // validate email using regex
                if (validateEmail(text)) {
                    emailValid = true
                    $("#error-email").text("")
                    $("#error-email").hide()
                } else {
                    emailValid = false
                    $("#error-email").text("Tolong masukan email yang valid")
                    $("#error-email").show()
                }
            })

            $('#input-wa').focusout(function() {
                let text = $(this).val();

                // validate wa using regex
                if (validateNumber(text)) {
                    waValid = true
                    $("#error-wa").text("")
                    $("#error-wa").hide()
                } else {
                    waValid = false
                    $("#error-wa").text("Tolong masukan nomor yang valid")
                    $("#error-wa").show()
                }
            })

            $('#form-auth').on('submit', function(e) {
                let email = $('#input-email').val();
                let password = $('#input-password').val();
                let confirm = $('#input-confirm').val();
                let wa = $('#input-wa').val();
                let name = $('#input-name').val();

                let isValid = emailValid &&
                    waValid &&
                    nameValid &&
                    email.length > 0 &&
                    confirm.length > 0 &&
                    wa.length > 0 &&
                    name.length > 0 &&
                    confirm.length >= 6 &&
                    password.length >= 6 &&
                    confirm == password;

                if (!isValid) {
                    console.log('atas');
                    e.preventDefault();
                    if (email.length < 1) {
                        $("#error-email").text("Email tidak boleh kosong")
                        $("#error-email").show()
                    }
                    if (password !== confirm) {
                        $("#error-password").text("Password tidak sama!")
                        $("#error-password").show()
                        $("#error-confirm").text("Password tidak sama!")
                        $("#error-confirm").show()
                    }
                    if (password.length < 6) {
                        $("#error-password").text("Tolong masukan min. 6 karakter")
                        $("#error-password").show()
                    }
                    if (confirm.length < 6) {
                        $("#error-confirm").text("Tolong masukan min. 6 karakter")
                        $("#error-confirm").show()
                    }
                    if (password.length < 1) {
                        $("#error-password").text("Password tidak boleh kosong!")
                        $("#error-password").show()
                    }
                    if (confirm.length < 1) {
                        $("#error-confirm").text("Password tidak boleh kosong!")
                        $("#error-confirm").show()
                    }
                    if (wa.length < 1) {
                        $("#error-wa").text("Phone number tidak boleh kosong")
                        $("#error-wa").show()
                    }
                    if (name.length < 1) {
                        $("#error-name").text("Name tidak boleh kosong")
                        $("#error-name").show()
                    }

                } else {
                    $("#error-email").hide()
                    $("#error-name").hide()
                    $("#error-wa").hide()
                    $("#error-password").hide()
                    $("#error-confirm").hide()
                }
            })
        });
    </script>
@endsection

@section('pageJS')
<script src="//www.google.com/recaptcha/api.js"></script>
@endsection
