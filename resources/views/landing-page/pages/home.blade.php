@extends('landing-page.master')

@section('content')
    <section class="home-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="header-ornament">
                        <img src="/svg/ornament/pattern-1.svg" alt="">
                    </div>
                    <div class="black-bold-48 big-text-header">
                        Bawa perubahan positif untuk diri sendiri dan orang lain.
                    </div>
                    <div class="black75-normal-18 subtitle-text-header">
                        Club Sobat Badak adalah sebuah komunitas yang dibentuk atas dasar tujuan yang sama, yaitu
                        #bawaperubahan positif untuk diri sendiri dan orang lain.
                    </div>
                    <a href="{{ route('registerPage') }}" class="custom-btn-primary btn btn-warning" style="width: 250px">
                        Bergabung sekarang
                    </a>
                    @php
                        $jam = date('H');
                    @endphp

                    @if ($jam >= 15 && $jam <= 21)
                        <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09" target="_blank"
                            class="livenow">
                            <button class="btn">
                                <div class="live-icon blink"></div> LIVE SEKARANG
                            </button>
                        </a>
                    @endif


                </div>
                <div class="col-md-6 col-sm-6 header-right">
                </div>
                <div class="little-box-header text-center">
                    <div class="black-bold-36">
                        {{ number_format($users, 0, ',', '.') }}<sup>+</sup>
                    </div>
                    <div class="black-normal-16">
                        Sobat Badak bergabung
                    </div>
                </div>
                <img src="{{ asset('images/baper.png') }}" alt="unknown" class="baper_hero">
            </div>
        </div>
    </section>

    <section class="section-content">
        <div class="black-bold-36 text-center mb-5">
            Apa yang bisa kamu lakukan disini?
        </div>
        <div class="cardz-slider">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="/images/game-quiz.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Quiz Tebak Kata
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Tebak kata dengan mudah dan dapatkan kejutannya
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">
                    <img src="/images/game-auction.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Bid Auction
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Mulai bid barang kesayanganmu dan jangan sampai barang kesayanganmu hilang diambil orang
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">
                    <img src="/images/game-ref.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Temukan Komunitasmu
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Temukan sobat badak mu yang lain dan disini tempatnya
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">
                    <img src="/images/game-kata.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Quiz Rejeki Sobat
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Mau dapat rejeki dengan mudah?? Rejeki Sobat solusinya
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
        <script>
            let previewSlide = 5
            if (window.screen.width <= 1024) {
                previewSlide = 3
            } else if (window.screen.width <= 426) {
                previewSlide = 1.5
            }

            var swiper = new Swiper('.cardz-slider', {
                slidesPerView: previewSlide,
                centeredSlides: true,
                loop: true,
                spaceBetween: 0,
                autoplay: true
            });
        </script>
    </section>


    <section class="section-content container">
        <div class="row" style="margin-top: 75px; margin-bottom:75px">
            <div class="col-sm-6">
                <div class="black-bold-36">
                    Club Sobat Badak
                </div>
            </div>
            <div class="col-sm-6">
                <div class="black-normal-16">
                    Tempat kamu belajar, ngobrol, bertukar cerita dan pengalaman setiap hari bersama Sobat Badak lainnya
                    di
                    seluruh Indonesia
                </div>
            </div>
        </div>
        <div class="list-item row">
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <a href="{{ route('uniTownhall') }}">
                    <div class="custom-card mx-2">
                        <img class="townhall" src="/images/town-hall.svg" alt="">
                        <div class="custom-card-body text-center">
                            <div class="black-semi-36">
                                Townhall
                            </div>
                            <div class="black-normal-14">
                                Tempatnya kamu bisa berjumpa dan ngobrol bareng dengan Badak Baper
                            </div>
                            @if ($jam >= 15 && $jam <= 21)
                                <div class="d-flex justify-content-center align-items-center" style="margin: 15px auto">
                                    <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                        target="_blank" class="livenow">
                                        <button class="btn">
                                            <div class="live-icon blink"></div> LIVE SEKARANG
                                        </button>
                                    </a>
                                </div>
                            @endif
                        </div>

                    </div>

                </a>

            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <a href="{{ route('uniLapak') }}">
                    <div class="custom-card mx-2">
                        <img class="lapak" src="/images/amusemarket.svg" alt="">
                        <div class="custom-card-body text-center">
                            <div class="black-semi-36">
                                Lapak Mamak
                            </div>
                            <div class="black-normal-14">
                                Tempatnya para kaum hobi belanja berkumpul. Temukan kebutuhanmu di Amusemarket, dan unboxing
                                hadiah yang kamu dapatkan di sini
                            </div>
                            @if ($jam >= 15 && $jam <= 21)
                                <div class="d-flex justify-content-center align-items-center" style="margin: 15px auto">
                                    <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                        target="_blank" class="livenow">
                                        <button class="btn">
                                            <div class="live-icon blink"></div> LIVE SEKARANG
                                        </button>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <a href="{{ route('uniAuction') }}">
                    <div class="custom-card mx-2">
                        <img src="/images/auction.svg" alt="">
                        <div class="custom-card-body text-center">
                            <div class="black-semi-36">
                                Auction House
                            </div>
                            <div class="black-normal-14">
                                Tempat lelang terpercaya dan terunik karena dapat diikuti hanya dengan Baper Point
                            </div>
                            @if ($jam >= 15 && $jam <= 21)
                                <div class="d-flex justify-content-center align-items-center" style="margin: 15px auto">
                                    <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                        target="_blank" class="livenow">
                                        <button class="btn">
                                            <div class="live-icon blink"></div> LIVE SEKARANG
                                        </button>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <a href="{{ route('uniTheater') }}">
                    <div class="custom-card mx-2">
                        <img src="/images/cinema.svg" alt="">
                        <div class="custom-card-body text-center">
                            <div class="black-semi-36">
                                Theater
                            </div>
                            <div class="black-normal-14">
                                Tempatnya kamu melepas penat di room Theater sambil karoke dan nonton bareng Sobat Badak
                                lainnya
                            </div>
                            @if ($jam >= 15 && $jam <= 21)
                                <div class="d-flex justify-content-center align-items-center" style="margin: 15px auto">
                                    <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                        target="_blank" class="livenow">
                                        <button class="btn">
                                            <div class="live-icon blink"></div> LIVE SEKARANG
                                        </button>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <a href="{{ route('uniBaba') }}">
                    <div class="custom-card mx-2">
                        <img class="baba" src="/images/baba.svg" alt="">
                        <div class="custom-card-body text-center">
                            <div class="black-semi-36">
                                Baba Mencari Bakat
                            </div>
                            <div class="black-normal-14">
                                Tempat Baba mencari Sobat Badak yang memiliki bakat terpendam. Yuk tunjukkan bakatmu!
                            </div>
                            @if ($jam >= 15 && $jam <= 21)
                                <div class="d-flex justify-content-center align-items-center" style="margin: 15px auto">
                                    <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                        target="_blank" class="livenow">
                                        <button class="btn">
                                            <div class="live-icon blink"></div> LIVE SEKARANG
                                        </button>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <a href="{{ route('uniGamezone') }}">
                    <div class="custom-card mx-2">
                        <img src="/images/cafe.svg" alt="">
                        <div class="custom-card-body text-center">
                            <div class="black-semi-36">
                                Gamezone & Cafe
                            </div>
                            <div class="black-normal-14">
                                Mau ngumpulin Baper Poin sambil nongkrong ngobrolin hobi kamu? Gamezone & Cafe bisa jadi
                                pilihan
                                yang pas!
                            </div>
                            @if ($jam >= 15 && $jam <= 21)
                                <div class="d-flex justify-content-center align-items-center" style="margin: 15px auto">
                                    <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                        target="_blank" class="livenow">
                                        <button class="btn">
                                            <div class="live-icon blink"></div> LIVE SEKARANG
                                        </button>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <section class="container mb-8 section-content">
        <div class="black-bold-36 text-center" style="margin-bottom: 4rem">
            Kenalan sama sobat Badak
        </div>
        <div class="list-user row">
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/badak-baper.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Badak Baper
                        </div>
                        <div class="black-normal-14">
                            Bukan bawa perasaan tapi bawa perubahan. Berasal dari meteor jatuh. Badak Baper selalu memiliki
                            tekad untuk bawa perubahan positif dalam kehidupannya. Hobinya rebahan dan menghibur orang.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/badak-mama.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Mamak
                        </div>
                        <div class="black-normal-14">
                            Sosok Ibu yang telah merawat Badak Baper. Ciri khas nya bibir dengan lipstick merah merekah,
                            meskipun suka ngegas dan omelin Badak Baper, tapi cinta dan kasih sayang Mamak ke Badak Baper
                            sungguh tak ternilai
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/jamet-kuproy.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Jamet Kuproy
                        </div>
                        <div class="black-normal-14">
                            Merupakan sahabat sehidup semati Badak Baper. Memiliki hobi yang bikin keringetan, yaitu joget
                            dan
                            karaoke
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/bang-jago.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Bang Jago
                        </div>
                        <div class="black-normal-14">
                            Sepupu Badak Baper yang hobinya marah dan paling ngerasa bener. Tapi dibalik sifatnya itu, Bang
                            Jago
                            paling bisa menghibur suasana dengan celotehannya
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img class="boleng" src="/images/boleng-getol.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Boleng & Getol
                        </div>
                        <div class="black-normal-14">
                            Bagai pinang dibelah dua, Boleng (Bocah Kaleng) dan Getol (Genk Botol) gak bisa terpisahkan.
                            Tiada
                            hari tanpa bermain, begitulah kehidupan mereka berdua. Ada mereka suasana jadi makin rame.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/c-adodo.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            C. Adodo
                        </div>
                        <div class="black-normal-14">
                            Sosok yang paling up to date dan tau semua gossip yang lagi viral di jagat raya. Gak heran C.
                            Adodo
                            sering dijadiin teman ngobrol
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-user row">
            <div class="col-sm-6 col-6 d-flex justify-content-end">
                <div class="custom-card mx-2">
                    <img src="/images/kucing-baba.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Baba
                        </div>
                        <div class="black-normal-14">
                            Ini bokapnya Badak Baper, tapi Baba bukan kucing sembarang kucing, melainkan peramal cinta
                            paling hits karena suka memberikan ramalam cinta yang ngaco. Tapi anehnya Baba selalu diincar
                            sama kaum jomblo akut
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-6 d-flex justify-content-start">
                <div class="custom-card mx-2">
                    <img src="/images/bos-tudung.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Bos Tudung
                        </div>
                        <div class="black-normal-14">
                            Sosok misterius yang gak pernah menunjukkan wajah aslinya kepada siapapun. Tapi dibalik sosok
                            misterius tersebut, Bos Tudung dicintai oleh banyak orang karena selalu memberikan kejutan
                            barang
                            lelang super berkualitas
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="faq_section" class="container faq section-content">
        <div class="black-bold-36 text-center" style="margin-bottom: 4rem">
            Tanya Badak Baper
        </div>
        <div class="justify-content-center d-flex" style="margin-bottom: 3rem">
            <div id="faq__poin" class="faq-card active text-center" onclick="changeFAQ('poin')">
                <img src="{{ asset('images/icons/redeem.svg') }}" alt="csb" class="mb-3">
                <div class="title">
                    FAQ Poin</div>
            </div>
            <div id="faq__auction" class="faq-card text-center" onclick="changeFAQ('auction')">
                <img src="{{ asset('images/icons/lelang.svg') }}" alt="csb" class="mb-3">
                <div class="title">
                    FAQ Auction/Lelang</div>
            </div>
            <div id="faq__quiz" class="faq-card text-center" onclick="changeFAQ('quiz')">
                <img src="{{ asset('images/icons/rejeki-sobat.svg') }}" alt="csb" class="mb-3">
                <div class="title">
                    FAQ Quiz</div>
            </div>
        </div>
        <div id="faq-baper">
            {{-- ACCORDION POIN --}}
            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                id="accordion__poin">
                <div class="card">
                    <div class="card-header" id="h__poinOne8">
                        <div class="card-title" data-toggle="collapse" data-target="#c__poinOne8">
                            <div class="card-label black-bold-14">Apa itu Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinOne8" class="collapse show" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper poin adalah reward bagi kalian yang join club sobat badak dan
                                    dapat mengikuti Lelang barang impian kamu.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinTwo8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinTwo8">
                            <div class="card-label black-bold-14"> Bagaimana cara mendapatkan baper poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinTwo8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Mengikuti Zoom
                                </li>
                                <li class="black-normal-14">
                                    Kuis di Zoom
                                </li>
                                <li class="black-normal-14">
                                    Tukar dengan Koin Gatotkaca
                                </li>
                                <li class="black-normal-14">
                                    Permainan di Website
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinThree8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinThree8">
                            <div class="card-label black-bold-14">Bagaimana cara melihat Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinThree8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper poin dapat dilihat di profil pada saat login sobatbadak.club
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinFour8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinFour8">
                            <div class="card-label black-bold-14">Baper Poin bisa digunakan untuk apa saja?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinFour8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Bisa digunakan untuk Auction yang akan diadakan setiap harinya
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinFive8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinFive8">
                            <div class="card-label black-bold-14">Bagaimana cara menukar Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinFive8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper Poin akan otomatis terpotong saat memenangkan barang
                                    Auction, dan tidak akan terpotong jika kalah dalam Auction.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinSix8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinSix8">
                            <div class="card-label black-bold-14">Apakah ada jangka waktu kadaluarsa Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinSix8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Tidak ada, Baper Poin tidak akan hangus
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinSeven8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinSeven8">
                            <div class="card-label black-bold-14">Apakah Baper Poin bisa diuangkan?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinSeven8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper Poin tidak bisa diuangkan
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            {{-- ACCORDION LELANG --}}
            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                id="accordion__auction">
                <div class="card">
                    <div class="card-header" id="h__auctionOne8">
                        <div class="card-title" data-toggle="collapse" data-target="#c__auctionOne8">
                            <div class="card-label black-bold-14">Apa itu Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionOne8" class="collapse show" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Menanggapi maraknya auction/lelang di Instagram, Club Sobat Badak
                                    membuat Auction yang jujur dan adil, kalian bisa memenangkan
                                    barang impian menggunakan Baper Poin
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionTwo8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionTwo8">
                            <div class="card-label black-bold-14">Kapan dan dimana Auction akan diadakan?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionTwo8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Kegiatan Auction akan dilakukan sewaktu-waktu (tidak terjadwal) di
                                    breakout room Zoom WGM Live
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionThree8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionThree8">
                            <div class="card-label black-bold-14">Apa saja barang yang akan dilelang di room WGM Live?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionThree8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Setiap harinya akan ada 4 Hadiah yang akan dilelang di room WGM
                                    Live.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionFour8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionFour8">
                            <div class="card-label black-bold-14">Bagaimana cara mengikuti Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionFour8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Pastikan kalian memiliki baper poin
                                </li>
                                <li class="black-normal-14">
                                    Host akan mengumumkan waktu lelang di room WGM Live
                                </li>
                                <li class="black-normal-14">
                                    Sobat Badak dapat melakukan penawaran pada website
                                    sobatbadak.club
                                </li>
                                <li class="black-normal-14">
                                    Ikuti penawaran sesuai dengan peraturan
                                </li>
                                <li class="black-normal-14">
                                    Host akan mengumumkan pemenang lelang
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionFive8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionFive8">
                            <div class="card-label black-bold-14">Bagaimana cara membeli barang Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionFive8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Kita gak ada sistem redeem tapi “Redeem Now”
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionSix8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionSix8">
                            <div class="card-label black-bold-14">Apakah ada jumlah bid maksimal dalam sehari?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionSix8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Tidak ada. Kamu bebas melakukan bid selama tidak melebihi jumlah
                                    Baper Poin kamu.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionSeven8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionSeven8">
                            <div class="card-label black-bold-14">Berapa jumlah minimal Baper Poin untuk bisa ikut bid?
                            </div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionSeven8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Kamu bisa bid barang Auction mulai dari 100 Baper Poin
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionEight8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionEight8">
                            <div class="card-label black-bold-14">Apa yang harus saya lakukan jika menang Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionEight8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Pemenang Auction harus mengkonfirmasikan data diri dan menunjukkan foto
                                    KTP/Paspor/SIM yang sesuai dengan data diri yang didaftarkan, ke Whatsapp Official
                                    Club Sobat Badak maksimal 1x24 jam. Jika data diri yang didaftarkan tidak sesuai
                                    dengan yang tertera pada KTP/Paspor/SIM, maka kemenangan tidak valid dan paket tidak
                                    dapat dikirimkan ke pemenang.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionNine">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionNine">
                            <div class="card-label black-bold-14">Kapan barang Auction yang sudah berhasil dimenangkan
                                dikirim kepada
                                pemenang?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionNine" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    H+7 maksimal barang akan diterima
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionTen">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionTen">
                            <div class="card-label black-bold-14">Apakah boleh mengikuti Auction lebih dari 1 kali?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionTen" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Sangat diperbolehkan.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            {{-- ACCORDION QUIZ --}}
            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                id="accordion__quiz">
                <div class="card">
                    <div class="card-header" id="h__quizOne8">
                        <div class="card-title" data-toggle="collapse" data-target="#c__quizOne8">
                            <div class="card-label black-bold-14">Apakah setiap hari akan ada Quiz?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizOne8" class="collapse show" data-parent="#accordion__quiz">
                        <div class="card-body faq-content faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Ya, benar. Setiap hari akan ada Quiz Tebak Kata dan Quiz Rejeki
                                    Sobat.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizTwo8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizTwo8">
                            <div class="card-label black-bold-14">Apakah soal quiz akan selalu berubah?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizTwo8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Ya, soal quiz akan berbeda setiap harinya.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizThree8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizThree8">
                            <div class="card-label black-bold-14"> Berapa kali saya bisa mainkan quiz per hari?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizThree8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Quiz Tebak Kata dan Quiz Rejeki Sobat bisa dimainkan masing-masing
                                    1x per hari.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizFour8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizFour8">
                            <div class="card-label black-bold-14">Berapa Baper Poin yang bisa didapatkan dari quiz?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizFour8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    100 Baper Poin untuk setiap jawaban benar Quiz Tebak Kata. 200
                                    Baper Poin untuk setiap jawaban benar Quiz Rejeki Sobat.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizFive8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizFive8">
                            <div class="card-label black-bold-14">Jika hanya benar satu atau dua pertanyaan, apakah tetap
                                dapat Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizFive8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Setiap 1 soal yang benar akan mendapatkan Baper Poin.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function() {
            $(`#faq__poin`).addClass('active');
            $(`#accordion__poin`).show();
        })

        function changeFAQ(elementID) {
            $(`#faq__${elementID}`).addClass('active');
            $(`#accordion__${elementID}`).show();
            if (elementID === "quiz") {
                $(`#faq__poin`).removeClass('active');
                $(`#accordion__poin`).css("display", "none");
                $(`#faq__auction`).removeClass('active');
                $(`#accordion__auction`).css("display", "none");
            } else if (elementID === "auction") {
                $(`#faq__poin`).removeClass('active');
                $(`#accordion__poin`).css("display", "none");
                $(`#faq__quiz`).removeClass('active');
                $(`#accordion__quiz`).css("display", "none");
            } else if (elementID === "poin") {
                $(`#faq__auction`).removeClass('active');
                $(`#accordion__auction`).css("display", "none");
                $(`#faq__quiz`).removeClass('active');
                $(`#accordion__quiz`).css("display", "none");
            }
        }
    </script>
@endsection
