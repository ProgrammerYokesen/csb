@extends('landing-page.master')

@section('content')
    <section class="faq-header">
    </section>

    <section id="faq_section" class="container faq section-content">
        <div class="black-bold-36 text-center" style="margin-bottom: 4rem">
            Tanya Badak Baper
        </div>
        <div class="justify-content-center d-flex" style="margin-bottom: 3rem">
            <div id="faq__poin" class="faq-card active text-center" onclick="changeFAQ('poin')">
                <img src="{{ asset('images/icons/redeem.svg') }}" alt="csb" class="mb-3">
                <div class="title">
                    FAQ Poin</div>
            </div>
            <div id="faq__auction" class="faq-card text-center" onclick="changeFAQ('auction')">
                <img src="{{ asset('images/icons/lelang.svg') }}" alt="csb" class="mb-3">
                <div class="title">
                    FAQ Auction/Lelang</div>
            </div>
            <div id="faq__quiz" class="faq-card text-center" onclick="changeFAQ('quiz')">
                <img src="{{ asset('images/icons/rejeki-sobat.svg') }}" alt="csb" class="mb-3">
                <div class="title">
                    FAQ Quiz</div>
            </div>
        </div>
        <div id="faq-baper">
            {{-- ACCORDION POIN --}}
            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                id="accordion__poin">
                <div class="card">
                    <div class="card-header" id="h__poinOne8">
                        <div class="card-title" data-toggle="collapse" data-target="#c__poinOne8">
                            <div class="card-label black-bold-14">Apa itu Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinOne8" class="collapse show" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper poin adalah reward bagi kalian yang join club sobat badak dan
                                    dapat mengikuti Lelang barang impian kamu.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinTwo8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinTwo8">
                            <div class="card-label black-bold-14"> Bagaimana cara mendapatkan baper poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinTwo8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Mengikuti Zoom
                                </li>
                                <li class="black-normal-14">
                                    Kuis di Zoom
                                </li>
                                <li class="black-normal-14">
                                    Tukar dengan Koin Gatotkaca
                                </li>
                                <li class="black-normal-14">
                                    Permainan di Website
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinThree8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinThree8">
                            <div class="card-label black-bold-14">Bagaimana cara melihat Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinThree8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper poin dapat dilihat di profil pada saat login sobatbadak.club
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinFour8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinFour8">
                            <div class="card-label black-bold-14">Baper Poin bisa digunakan untuk apa saja?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinFour8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Bisa digunakan untuk Auction yang akan diadakan setiap harinya
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinFive8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinFive8">
                            <div class="card-label black-bold-14">Bagaimana cara menukar Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinFive8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper Poin akan otomatis terpotong saat memenangkan barang
                                    Auction, dan tidak akan terpotong jika kalah dalam Auction.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinSix8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinSix8">
                            <div class="card-label black-bold-14">Apakah ada jangka waktu kadaluarsa Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinSix8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Tidak ada, Baper Poin tidak akan hangus
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__poinSeven8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__poinSeven8">
                            <div class="card-label black-bold-14">Apakah Baper Poin bisa diuangkan?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__poinSeven8" class="collapse" data-parent="#accordion__poin">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Baper Poin tidak bisa diuangkan
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            {{-- ACCORDION LELANG --}}
            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                id="accordion__auction">
                <div class="card">
                    <div class="card-header" id="h__auctionOne8">
                        <div class="card-title" data-toggle="collapse" data-target="#c__auctionOne8">
                            <div class="card-label black-bold-14">Apa itu Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionOne8" class="collapse show" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Menanggapi maraknya auction/lelang di Instagram, Club Sobat Badak
                                    membuat Auction yang jujur dan adil, kalian bisa memenangkan
                                    barang impian menggunakan Baper Poin
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionTwo8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionTwo8">
                            <div class="card-label black-bold-14">Kapan dan dimana Auction akan diadakan?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionTwo8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Kegiatan Auction akan dilakukan sewaktu-waktu (tidak terjadwal) di
                                    breakout room Zoom WGM Live
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionThree8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionThree8">
                            <div class="card-label black-bold-14">Apa saja barang yang akan dilelang di room WGM Live?
                            </div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionThree8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Setiap harinya akan ada 4 Hadiah yang akan dilelang di room WGM
                                    Live.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionFour8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionFour8">
                            <div class="card-label black-bold-14">Bagaimana cara mengikuti Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionFour8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Pastikan kalian memiliki baper poin
                                </li>
                                <li class="black-normal-14">
                                    Host akan mengumumkan waktu lelang di room WGM Live
                                </li>
                                <li class="black-normal-14">
                                    Sobat Badak dapat melakukan penawaran pada website
                                    sobatbadak.club
                                </li>
                                <li class="black-normal-14">
                                    Ikuti penawaran sesuai dengan peraturan
                                </li>
                                <li class="black-normal-14">
                                    Host akan mengumumkan pemenang lelang
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionFive8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionFive8">
                            <div class="card-label black-bold-14">Bagaimana cara membeli barang Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionFive8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Kita gak ada sistem redeem tapi “Redeem Now”
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionSix8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionSix8">
                            <div class="card-label black-bold-14">Apakah ada jumlah bid maksimal dalam sehari?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionSix8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Tidak ada. Kamu bebas melakukan bid selama tidak melebihi jumlah
                                    Baper Poin kamu.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionSeven8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionSeven8">
                            <div class="card-label black-bold-14">Berapa jumlah minimal Baper Poin untuk bisa ikut bid?
                            </div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionSeven8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Kamu bisa bid barang Auction mulai dari 100 Baper Poin
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionEight8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionEight8">
                            <div class="card-label black-bold-14">Apa yang harus saya lakukan jika menang Auction?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionEight8" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Pemenang Auction harus mengkonfirmasikan data diri dan menunjukkan foto
                                    KTP/Paspor/SIM yang sesuai dengan data diri yang didaftarkan, ke Whatsapp Official
                                    Club Sobat Badak maksimal 1x24 jam. Jika data diri yang didaftarkan tidak sesuai
                                    dengan yang tertera pada KTP/Paspor/SIM, maka kemenangan tidak valid dan paket tidak
                                    dapat dikirimkan ke pemenang.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionNine">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionNine">
                            <div class="card-label black-bold-14">Kapan barang Auction yang sudah berhasil dimenangkan
                                dikirim kepada
                                pemenang?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionNine" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    H+7 maksimal barang akan diterima
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__auctionTen">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__auctionTen">
                            <div class="card-label black-bold-14">Apakah boleh mengikuti Auction lebih dari 1 kali?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__auctionTen" class="collapse" data-parent="#accordion__auction">
                        <div class="card-body">
                            <ul>
                                <li class="black-normal-14">
                                    Sangat diperbolehkan.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            {{-- ACCORDION QUIZ --}}
            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle custom-accordion"
                id="accordion__quiz">
                <div class="card">
                    <div class="card-header" id="h__quizOne8">
                        <div class="card-title" data-toggle="collapse" data-target="#c__quizOne8">
                            <div class="card-label black-bold-14">Apakah setiap hari akan ada Quiz?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizOne8" class="collapse show" data-parent="#accordion__quiz">
                        <div class="card-body faq-content faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Ya, benar. Setiap hari akan ada Quiz Tebak Kata dan Quiz Rejeki
                                    Sobat.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizTwo8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizTwo8">
                            <div class="card-label black-bold-14">Apakah soal quiz akan selalu berubah?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizTwo8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Ya, soal quiz akan berbeda setiap harinya.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizThree8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizThree8">
                            <div class="card-label black-bold-14"> Berapa kali saya bisa mainkan quiz per hari?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizThree8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Quiz Tebak Kata dan Quiz Rejeki Sobat bisa dimainkan masing-masing
                                    1x per hari.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizFour8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizFour8">
                            <div class="card-label black-bold-14">Berapa Baper Poin yang bisa didapatkan dari quiz?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizFour8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    100 Baper Poin untuk setiap jawaban benar Quiz Tebak Kata. 200
                                    Baper Poin untuk setiap jawaban benar Quiz Rejeki Sobat.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="h__quizFive8">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#c__quizFive8">
                            <div class="card-label black-bold-14">Jika hanya benar satu atau dua pertanyaan, apakah tetap
                                dapat Baper Poin?</div>
                            <span class="svg-icon">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div id="c__quizFive8" class="collapse" data-parent="#accordion__quiz">
                        <div class="card-body faq-content">
                            <ul>
                                <li class="black-normal-14">
                                    Setiap 1 soal yang benar akan mendapatkan Baper Poin.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function() {
            $(`#faq__poin`).addClass('active');
            $(`#accordion__poin`).show();
        })

        function changeFAQ(elementID) {
            $(`#faq__${elementID}`).addClass('active');
            $(`#accordion__${elementID}`).show();
            if (elementID === "quiz") {
                $(`#faq__poin`).removeClass('active');
                $(`#accordion__poin`).css("display", "none");
                $(`#faq__auction`).removeClass('active');
                $(`#accordion__auction`).css("display", "none");
            } else if (elementID === "auction") {
                $(`#faq__poin`).removeClass('active');
                $(`#accordion__poin`).css("display", "none");
                $(`#faq__quiz`).removeClass('active');
                $(`#accordion__quiz`).css("display", "none");
            } else if (elementID === "poin") {
                $(`#faq__auction`).removeClass('active');
                $(`#accordion__auction`).css("display", "none");
                $(`#faq__quiz`).removeClass('active');
                $(`#accordion__quiz`).css("display", "none");
            }
        }
    </script>
@endsection
