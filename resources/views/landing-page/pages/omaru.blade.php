@extends('landing-page.master')

@section('content')
    <section class="landing-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-sm-6 landing-header-left">
                    <span>ONLINE PODCAST & TALKSHOW</span>
                    <h2>OM BADAK</h2>
                    <h5>Yuk ikutan keseruan online podcast dan talkshow bareng Badak Baper dan Omaru Community!</h5>
                    <a href="{{ route('registerPage') }}" class="custom-btn-primary btn btn-warning">
                        Bergabung sekarang
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 landing-header-right">
                    <img src="{{ asset('images/landing-page/ombadak.png') }}" alt="ombadak" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    @php
    $date = date('d-M-Y');
    $now = strtotime($date);
    @endphp

    <section class="section-content container landing_wrapper">
        <div class="landing_jadwal">
            <h2><span>Jadwal</span> Om Badak</h2>

            <div class="row justify-content-center align-items-center" style="margin-top: 50px">

                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">Rabu, 4 Agustus</h3>
                                <h3>19.00 - 21.00</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Mie Rebus vs Mie Goreng</h3>
                            {{-- <h5>Kamu tim Mie Rebus atau Mie Goreng?</h5> --}}

                            @if ($now > strtotime('04-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('04-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif
                            {{-- <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn" {{ $now != strtotime('01-07-2021') ? 'disabled' : '' }}>Join
                                        Sekarang</button>
                                </a> --}}
                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    Jumat, 6 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Pamali</h3>
                            {{-- <h5>Siapa disini yang percaya pamali?</h5> --}}

                            @if ($now > strtotime('06-08-2021'))
                                <a href="javascript:void(0)">
                                    <button class="btn expired" disabled>Expired</button>
                                </a>
                            @elseif ($now != strtotime('06-08-2021'))
                                <a href="javascript:void(0)">
                                    <button class="btn" disabled>Join
                                        Sekarang</button>
                                </a>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    senin, 9 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Tetangga Kepo</h3>
                            {{-- <h5>Kita bahas tentang tetangga yang suka kepo disini</h5> --}}

                            @if ($now > strtotime('09-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('09-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    selasa, 10 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Pasangan Romantis vs Berkarir</h3>
                            {{-- <h5>Tips bagi yang punya pasangan</h5> --}}

                            @if ($now > strtotime('10-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('10-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    kamis, 12 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Perempuan Selalu Benar</h3>
                            {{-- <h5>Kenapa perempuan selalu benar?</h5> --}}

                            @if ($now > strtotime('12-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('12-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif


                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    jum'at, 13 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Forex untuk Pemula</h3>
                            {{-- <h5>Yang mau belajar forex yuk!</h5> --}}

                            @if ($now > strtotime('13-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('13-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    senin, 16 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Gunung vs Pantai</h3>
                            {{-- <h5>Liburan ke Gunung atau ke Pantai</h5> --}}

                            @if ($now > strtotime('16-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('16-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    rabu, 18 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Trend Ikan Cupang</h3>
                            {{-- <h5>Bahas tentang trend ikan cupang</h5> --}}

                            @if ($now > strtotime('18-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('18-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    Jum'at, 20 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Agustus Waktu Kecil</h3>
                            {{-- <h5>Agustusan jaman dulu</h5> --}}

                            @if ($now > strtotime('20-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('20-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    senin, 23 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Android vs iPhone</h3>
                            {{-- <h5>kamu Tim Android / iPhone?</h5> --}}

                            @if ($now > strtotime('23-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('23-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    rabu, 25 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Masak Yuk</h3>
                            {{-- <h5>Belajar masak yuuuk</h5> --}}

                            @if ($now > strtotime('25-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('25-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}
                {{-- Begin Item --}}
                <div class="col-xl-3 col-lg-3 col-md-4 col-6 landing-jadwal-card">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 style="text-transform: uppercase">
                                    jum'at, 27 Agustus
                                </h3>
                                <h3>19.00 - 21.00</h3>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Prediksi Masa Depan</h3>
                            {{-- <h5>Bisa gak kita prediksi masa depan?</h5> --}}

                            @if ($now > strtotime('27-08-2021'))
                                <button class="btn expired" disabled>Expired</button>
                            @elseif ($now != strtotime('27-08-2021'))
                                <button class="btn" disabled>Join
                                    Sekarang</button>
                            @else
                                <a href="https://us02web.zoom.us/j/85169183549?pwd=bk9GQStOS3lmOWFGZDh2QjVaZU1KUT09"
                                    target="_blank">
                                    <button class="btn">Join
                                        Sekarang</button>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                {{-- End Item --}}

            </div>
        </div>

        <div class="landing_heading" style="margin-top: 100px">
            <h2><span>Daftarkan</span> Dirimu Sekarang</h2>
            <h4>Yuk daftar di Club Sobat Badak dan saksikan online podcast & talkshow serta menangkan
                banyak hadiah di
                dalamnya.</h4>

            <div class="text-center">
                <a href="{{ route('registerPage') }}">
                    <button class="btn join_btn">Bergabung Sekarang</button>
                </a>
            </div>
        </div>



        {{-- <div class="landing_heading" style="margin-top: 50px">
            <h2><span>Dropshipper</span> Joining Fee</h2>
            <h4>Dengan bayar Rp. 50.000 kamu udah bisa join ke Om Badak Dropshipper</h4>

            <div class="row justify-content-center dropship_join">

                <div class="col-xl-6 col-lg-6 col-md-6">
                    <img src="{{ asset('images/landing-page/dropship.png') }}" alt="img">
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="dropship_join_text">
                        <div class="dropship_join_num">
                            <img src="{{ asset('images/landing-page/num1.svg') }}" alt="">
                        </div>
                        <div style="flex: 11">
                            <h3>1 set Larutan Cap Badak</h3>
                            <h5>Kamu bisa dapatkan 1 set Larutan Cap Badak jika join menjadi Dropshipper</h5>
                        </div>
                    </div>
                    <div class="dropship_join_text">
                        <div class="dropship_join_num">
                            <img src="{{ asset('images/landing-page/num2.svg') }}" alt="">
                        </div>
                        <div style="flex: 11">
                            <h3>OHS Classic</h3>
                            <h5>Kamu bisa dapatkan OHS Classic jika join menjadi Dropshipper</h5>
                        </div>
                    </div>
                    <div class="dropship_join_text">
                        <div class="dropship_join_num">
                            <img src="{{ asset('images/landing-page/num3.svg') }}" alt="">
                        </div>
                        <div style="flex: 11">
                            <h3>Om Badak T-Shirt (Tentative)</h3>
                            <h5>Kamu bisa dapatkan T-Shirt Om Badak jika join menjadi Dropshipper</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="landing_heading" style="margin: 75px auto">


            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-8 col-md-8 dropship_wgm">
                    <h2><span>Dropshipper</span> Data WGM</h2>
                    <h4>Dengan bayar Rp. 50.000 kamu udah bisa join ke Om Badak Dropshipper</h4>

                    <form action="" style="width: 80%">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nama" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone" />
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" id="exampleTextarea" rows="5" placeholder="Alamat"></textarea>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Instagram ID" />
                        </div>
                        <button class="btn">Bergabung Sekarang</button>
                    </form>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 dropship_wgm">
                    <img src="{{ asset('images/landing-page/drop-wgm.png') }}" alt="img">
                </div>
            </div>
        </div>

        <div class="landing_heading" style="margin-top: 50px">
            <h2><span>Dropshipper</span> Benefit</h2>
            <h4 style="width:100%">Benefit sebagai Dropshipper diantara lainnya adalah</h4>

            <div class="row justify-content-center" style="margin-top: 25px">
                <div class="col-xl-3 col-lg-3 col-md-3 col-6 dropship_benefit">
                    <div class="card card-custom gutter-b">
                        <div class="card-body">
                            <img src="{{ asset('images/landing-page/benefit1.svg') }}" alt="img">
                            <p>Online sales force for WGM (Larutan Cap Badak)</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-6 dropship_benefit">
                    <div class="card card-custom gutter-b">
                        <div class="card-body">
                            <img src="{{ asset('images/landing-page/benefit2.svg') }}" alt="img">
                            <p>Branding at their Social Media (sharing & promotion)</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-6 dropship_benefit">
                    <div class="card card-custom gutter-b">
                        <div class="card-body">
                            <img src="{{ asset('images/landing-page/benefit3.svg') }}" alt="img">
                            <p>Selling at Marketplace with their marketplace account</p>
                            <span>1 set Larutan Cap Badak = Rp. 48.500 with Gatot Kaca Coin</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-6 dropship_benefit">
                    <div class="card card-custom gutter-b">
                        <div class="card-body">
                            <img src="{{ asset('images/landing-page/benefit4.svg') }}" alt="img">
                            <p>Selling at Omaru with omaru dropshipper package for Larutan Cap Badak</p>
                            <span>2 set Larutan Cap Badak + Gift + Gatotkaca coin + Referal Commision = Rp. 200.000</span>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </section>

    {{-- <section class="section-content">
        <div class="black-normal-48 text-center mb-5">
            Apa yang bisa kamu lakukan disini ?
        </div>
        <div class="cardz-slider">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="/images/game-quiz.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Quiz Tebak Kata
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Tebak kata dengan mudah dan dapatkan kejutannya
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">
                    <img src="/images/game-auction.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Bid Auction
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Mulai bid barang kesayanganmu dan jangan sampai barang kesayanganmu hilang diambil orang
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">
                    <img src="/images/game-ref.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Temukan Komunitasmu
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Temukan sobat badak mu yang lain dan disini tempatnya
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">
                    <img src="/images/game-kata.png" alt="dummy-image">
                    <div class="custom-cardz-body">
                        <div class="custom-card-title white-semi-32 text-center">
                            Quiz Rejeki Sobat
                        </div>
                        <div class="custom-card-description white-normal-14 text-center">
                            Mau dapat rejeki dengan mudah?? Rejeki Sobat solusinya
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
        <script>
            let previewSlide = 5
            if (window.screen.width <= 1024) {
                previewSlide = 3
            } else if (window.screen.width <= 426) {
                previewSlide = 1.5
            }

            var swiper = new Swiper('.cardz-slider', {
                slidesPerView: previewSlide,
                centeredSlides: true,
                loop: true,
                spaceBetween: 0,
                autoplay: true
            });
        </script>
    </section>


    <section class="section-content px-8">
        <div class="row" style="margin-top: 75px; margin-bottom:75px">
            <div class="col-sm-6">
                <div class="black-bold-48">
                    Club Sobat Badak
                </div>
            </div>
            <div class="col-sm-6">
                <div class="black-normal-16">
                    Tempat kamu belajar, ngobrol, bertukar cerita dan pengalaman setiap hari bersama Sobat Badak lainnya
                    di
                    seluruh Indonesia
                </div>
            </div>
        </div>
        <div class="list-item row">
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img class="townhall" src="/images/town-hall.svg" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Townhall
                        </div>
                        <div class="black-normal-14">
                            Tempatnya kamu bisa berjumpa dan ngobrol bareng dengan Badak Baper
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/amusemarket.svg" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Lapak Mamak
                        </div>
                        <div class="black-normal-14">
                            Tempatnya para kaum hobi belanja berkumpul. Temukan kebutuhanmu di Amusemarket, dan unboxing
                            hadiah yang kamu dapatkan di sini
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/auction.svg" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Auction House
                        </div>
                        <div class="black-normal-14">
                            Tempat lelang terpercaya dan terunik karena dapat diikuti hanya dengan Baper Point
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/cinema.svg" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Theater
                        </div>
                        <div class="black-normal-14">
                            Tempatnya kamu melepas penat di room Theater sambil karoke dan nonton bareng Sobat Badak lainnya
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/pojok-jodoh1.svg" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Pojok Jodoh
                        </div>
                        <div class="black-normal-14">
                            Tempat yang pas untuk kamu yang udah menyandang status jomblo akut atau pengen diramal mengenai
                            urusan cintanya!
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/cafe.svg" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Gamezone & Cafe
                        </div>
                        <div class="black-normal-14">
                            Mau ngumpulin Baper Poin sambil nongkrong ngobrolin hobi kamu? Gamezone & Cafe bisa jadi pilihan
                            yang pas!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="px-4 mb-8 section-content">
        <div class="black-bold-48" style="margin-bottom: 3rem">
            Kenalan sama sobat Badak
        </div>
        <div class="list-user row">
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/badak-baper.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Badak Baper
                        </div>
                        <div class="black-normal-14">
                            Bukan bawa perasaan tapi bawa perubahan. Berasal dari meteor jatuh. Badak Baper selalu memiliki
                            tekad untuk bawa perubahan positif dalam kehidupannya. Hobinya rebahan dan menghibur orang.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/badak-mama.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Mamak
                        </div>
                        <div class="black-normal-14">
                            Sosok Ibu yang telah merawat Badak Baper. Ciri khas nya bibir dengan lipstick merah merekah,
                            meskipun suka ngegas dan omelin Badak Baper, tapi cinta dan kasih sayang Mamak ke Badak Baper
                            sungguh tak ternilai
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/jamet-kuproy.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Jamet Kuproy
                        </div>
                        <div class="black-normal-14">
                            Merupakan sahabat sehidup semati Badak Baper. Memiliki hobi yang bikin keringetan, yaitu joget
                            dan
                            karaoke
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/bang-jago.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Bang Jago
                        </div>
                        <div class="black-normal-14">
                            Sepupu Badak Baper yang hobinya marah dan paling ngerasa bener. Tapi dibalik sifatnya itu, Bang
                            Jago
                            paling bisa menghibur suasana dengan celotehannya
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img class="boleng" src="/images/boleng-getol.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Boleng & Getol
                        </div>
                        <div class="black-normal-14">
                            Bagai pinang dibelah dua, Boleng (Bocah Kaleng) dan Getol (Genk Botol) gak bisa terpisahkan.
                            Tiada
                            hari tanpa bermain, begitulah kehidupan mereka berdua. Ada mereka suasana jadi makin rame.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-center">
                <div class="custom-card mx-2">
                    <img src="/images/c-adodo.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            C. Adodo
                        </div>
                        <div class="black-normal-14">
                            Sosok yang paling up to date dan tau semua gossip yang lagi viral di jagat raya. Gak heran C.
                            Adodo
                            sering dijadiin teman ngobrol
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-user row">
            <div class="col-sm-6 col-6 d-flex justify-content-end">
                <div class="custom-card mx-2">
                    <img src="/images/kucing-baba.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Baba
                        </div>
                        <div class="black-normal-14">
                            Ini bokapnya Badak Baper, tapi Baba bukan kucing sembarang kucing, melainkan peramal cinta
                            paling hits karena suka memberikan ramalam cinta yang ngaco. Tapi anehnya Baba selalu diincar
                            sama kaum jomblo akut
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-6 d-flex justify-content-start">
                <div class="custom-card mx-2">
                    <img src="/images/bos-tudung.png" alt="">
                    <div class="custom-card-body text-center">
                        <div class="black-semi-36">
                            Bos Tudung
                        </div>
                        <div class="black-normal-14">
                            Sosok misterius yang gak pernah menunjukkan wajah aslinya kepada siapapun. Tapi dibalik sosok
                            misterius tersebut, Bos Tudung dicintai oleh banyak orang karena selalu memberikan kejutan
                            barang
                            lelang super berkualitas
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
@endsection
