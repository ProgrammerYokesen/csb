<div class="footer custom-footer d-flex flex-lg-column" id="kt_footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="footer-image">
                    <img src="/images/csb-logo-footer.png" alt="footer">
                </div>
                <div class="white-semi-20">
                    Bawa Perubahan positif untuk diri sendiri dan orang lain.
                </div>
                <div class="footer_tele">
                    <a href="https://t.me/joinchat/dIVXmX2tEOJlZjZl" target="_blank">
                        <button class="btn">
                            <img src="{{ asset('images/icons/tele.svg') }}" alt="ico"> Join Telegram
                            Group
                        </button>
                    </a>
                </div>
                <div class="socmed-footer">
                    <ul>
                        <li class="pr-4">
                            <a href="https://www.instagram.com/clubsobatbadak" target="_blank">
                                <img src="/images/icons/landing-instagram.svg" alt="footer">
                            </a>
                        </li>
                        <li class="pr-4">
                            <a href="https://web.facebook.com/clubsobatbadak/" target="_blank">
                                <img src="/images/icons/landing-facebook.svg" alt="footer">
                            </a>
                        </li>
                        <li class="pr-4">
                            <a href="https://twitter.com/clubsobatbadak" target="_blank">
                                <img src="/images/icons/landing-twitter.svg" alt="footer">
                            </a>
                        </li>
                    </ul>
                </div>

                <a href="{{ route('disclaimerPage') }}" style="color: #ffffff; font-size: 16px;">
                    Disclaimer
                </a>
            </div>
            <div class="col-md-6">
                <ul class="list-nav-footer">
                    <li class="py-3 white-semi-20">
                        <a href="{{ route('defaultPage') }}">
                            Home
                        </a>
                    </li>
                    <li class="py-3 white-semi-20">
                        <a href="{{ route('omaru') }}">
                            Om Badak - Online Podcast & Talkshow
                        </a>
                    </li>
                    <li class="py-3 white-semi-20">
                        <a href="{{ route('loginPage') }}">
                            Masuk
                        </a>
                    </li>
                    <li class="py-3 white-semi-20">
                        <a href="{{ route('registerPage') }}">
                            Daftar
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="white-normal-20 footer-copyright">
            © {{ now()->year }} Club Sobat Badak — Teknologi didukung oleh YTI
        </div>
    </div>
</div>
