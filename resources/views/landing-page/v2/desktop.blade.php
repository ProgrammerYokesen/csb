@extends('landing-page.master')

@section('v2_assets')
    <link href="{{ asset('assets/css/slick-theme.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing-page/v2.css') }}?v=1.1.7" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <section id="layout__booklet" style="overflow:hidden">
        <div class="layout__desktop">
          <div class="hero__iframe">
            <div class="container d-flex justify-content-center align-items-center" style="height: 100%">
              <div class="row align-items-center">
                <div class="col-md-6">
                  <h1>Club Sobat Badak</h1>
                  <h1 class="strong">NGASIH HADIAH MOTOR?!</h1>

                  <h4>Yuk intip keseruan penyerahan hadiah motor untuk Bunda Ida kemarin di video ini!</h4>

                  <div class="d-flex" style="align-items: center;margin-top:54px">
                    <a href="{{ route('registerPage') }}" class="btn btn-warning button_primary">Bergabung
                      Sekarang</a>

                      @php
                      $jam = date('H');
                      @endphp


                      @if ($jam >= 15 && $jam <= 21)
                      <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                      target="_blank" class="livenow ml-3">
                      <button class="btn">
                        <div class="live-icon blink"></div> LIVE SEKARANG
                      </button>
                    </a>
                    @endif

                  </div>
                </div>
                <div class="col-md-6">
                  <iframe src="https://www.youtube.com/embed/Oxktxr9BLyE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>

            <!-- <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="csb__carousel">
                            <div class="side__left_carousel">
                                <p class="primary_text_normal_64">
                                    Welcome to
                                </p>
                                <p class="primary_text_bold_64">
                                    Club Sobat Badak
                                </p>
                                <p class="d_purple_text_normal_24">
                                    Selamat datang di Club Sobat Badak!
                                    Ini dia tempat nongki online dan seru-seruan bareng!
                                </p>
                                <div class="d-flex" style="align-items: center;margin-top:54px">
                                    <a href="#" class="btn btn-warning button_primary">Bergabung
                                        Sekarang</a>

                                    @php
                                        $jam = date('H');
                                    @endphp


                                    @if ($jam >= 15 && $jam <= 21)
                                        <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                            target="_blank" class="livenow ml-3">
                                            <button class="btn">
                                                <div class="live-icon blink"></div> LIVE SEKARANG
                                            </button>
                                        </a>
                                    @endif


                                    {{-- <a href="" class="primary_text_normal_18 ml-4">Learn More</a> --}}
                                </div>
                            </div>
                            <img class="carousel__img" src="{{ asset('/images/carousel_badak.png') }}" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="csb__carousel">
                            <div class="side__left_carousel">
                                <p class="primary_text_normal_64">
                                    Welcome to
                                </p>
                                <p class="primary_text_bold_64">
                                    Club Sobat Badak
                                </p>
                                <p class="d_purple_text_normal_24">
                                    Temen-temen dari Universe Club Sobat Badak bakal terus nemenin Sobat Badak dari pukul
                                    <span class="d_purple_text_bold_24">15.00 - 21.00 WIB setiap harinya!</span>
                                </p>
                                <div class="d-flex" style="align-items: center;margin-top:54px">
                                    <a href="" class="btn btn-warning button_primary" style="z-index:99">Bergabung
                                        Sekarang</a>
                                    @if ($jam >= 15 && $jam <= 21)
                                        <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                            target="_blank" class="livenow ml-3">
                                            <button class="btn">
                                                <div class="live-icon blink"></div> LIVE SEKARANG
                                            </button>
                                        </a>
                                    @endif
                                    {{-- <a href="" class="primary_text_normal_18 ml-4">Learn More</a> --}}
                                </div>
                            </div>
                            <img class="carousel__img" src="{{ asset('/images/carousel_jamet.png') }}" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="csb__carousel">
                            <div class="side__left_carousel">
                                <p class="primary_text_normal_64">
                                    Welcome to
                                </p>
                                <p class="primary_text_bold_64">
                                    Club Sobat Badak
                                </p>
                                <p class="d_purple_text_normal_24">
                                    Kalian juga bisa ikut <span class="d_purple_text_bold_24">Event Auction</span> kita dan
                                    menangin
                                    banyak hadiah impianmu tanpa
                                    perlu rupiah sedikitpun!
                                </p>
                                <div class="d-flex" style="align-items: center;margin-top:54px">
                                    <a href="" class="btn btn-warning button_primary" style="z-index:99">Bergabung
                                        Sekarang</a>
                                    @if ($jam >= 15 && $jam <= 21)
                                        <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                            target="_blank" class="livenow ml-3">
                                            <button class="btn">
                                                <div class="live-icon blink"></div> LIVE SEKARANG
                                            </button>
                                        </a>
                                    @endif
                                    {{-- <a href="" class="primary_text_normal_18 ml-4">Learn More</a> --}}
                                </div>
                            </div>
                            <img class="carousel__img" src="{{ asset('/images/carousel_mamak.png') }}" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="csb__carousel">
                            <div class="side__left_carousel">
                                <p class="primary_text_normal_64">
                                    Welcome to
                                </p>
                                <p class="primary_text_bold_64">
                                    Club Sobat Badak
                                </p>
                                <p class="d_purple_text_normal_24">
                                    So, tunggu apalagi? <br>
                                    Pastiin Sobat Badak udah daftar ke sobatbadak.club
                                </p>
                                <div class="d-flex" style="align-items: center;margin-top:54px">
                                    <a href="" class="btn btn-warning button_primary" style="z-index:99">Bergabung
                                        Sekarang</a>

                                    @if ($jam >= 15 && $jam <= 21)
                                        <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                            target="_blank" class="livenow ml-3">
                                            <button class="btn">
                                                <div class="live-icon blink"></div> LIVE SEKARANG
                                            </button>
                                        </a>
                                    @endif
                                    {{-- <a href="" class="primary_text_normal_18 ml-4">Learn More</a> --}}
                                </div>
                            </div>
                            <img class="carousel__img" src="{{ asset('/images/carousel_adodo.png') }}" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="csb__carousel">
                            <div class="side__left_carousel">
                                <p class="primary_text_normal_64">
                                    Welcome to
                                </p>
                                <p class="primary_text_bold_64">
                                    Club Sobat Badak
                                </p>
                                <p class="d_purple_text_normal_24">
                                    Jangan lupa pantengin terus Instagram <a style="color: #FFAA3A;"
                                        href="https://www.instagram.com/clubsobatbadak/">@clubsobatbadak</a> untuk info-info
                                    penting
                                    lainnya, ya!
                                </p>
                                <div class="d-flex" style="align-items: center;margin-top:54px">
                                    <a href="" class="btn btn-warning button_primary" style="z-index:99">Bergabung
                                        Sekarang</a>

                                    @if ($jam >= 15 && $jam <= 21)
                                        <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                            target="_blank" class="livenow ml-3">
                                            <button class="btn">
                                                <div class="live-icon blink"></div> LIVE SEKARANG
                                            </button>
                                        </a>
                                    @endif
                                    {{-- <a href="" class="primary_text_normal_18 ml-4">Learn More</a> --}}
                                </div>
                            </div>
                            <img class="carousel__img" src="{{ asset('/images/carousel_fam.png') }}" alt="">
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->

            <section id="paralax" class="___class_+?71___" style="overflow: hidden">
                <div id="content_1" class="text-center">
                    <p class="primary_text_normal_48" style="margin-top: 6rem">
                        Welcome to <br>
                        <span class="title_inner_shadow">
                            Universe Club Sobat Badak!
                        </span>
                    </p>
                    <p class="black-semi-32">
                        Jalan - jalan yuk!
                    </p>
                    <img src="{{ asset('/images/scroll-down.png') }}" alt="" style="margin-top: 110px">
                    <p class="d_blue_normal_18" style="margin-top: 15px">
                        Scroll down
                    </p>
                </div>
                <img id="par_img1" class="par__img" src="{{ asset('/images/awan_1.png') }}" alt=""
                    style="z-index: 1">
                {{-- <img id="par_img2" class="par__img" src="/images/awan_3.png" alt=""> --}}
                <div id="par_img3" class="d-flex justify-content-center" style="position: relative;margin-bottom:2rem">
                    <img class="par__img" src="{{ asset('/images/paralax3.png') }}" alt=""
                        style="width: 80%;z-index: -1">
                    <a data-toggle="modal" data-target="#modal3" class="click_area click__bakat">
                        <img class="click__area" src="{{ asset('/images/click-area.png') }}" alt="">
                    </a>
                    <a data-toggle="modal" data-target="#modal5" class="click_area click__lelang">
                        <img class="click__area" src="{{ asset('/images/click-area.png') }}" alt="">
                    </a>
                    <a data-toggle="modal" data-target="#modal2" class="click_area click__cinema">
                        <img class="click__area" src="{{ asset('/images/click-area.png') }}" alt="">
                    </a>
                    <a data-toggle="modal" data-target="#modal4" class="click_area click__mamak">
                        <img class="click__area" src="{{ asset('/images/click-area.png') }}" alt="">
                    </a>
                    <a data-toggle="modal" data-target="#modal1" class="click_area click__town">
                        <img class="click__area" src="{{ asset('/images/click-area.png') }}" alt="">
                    </a>
                </div>
            </section>

            <section class="cara__join text-center container" style="margin-top: 5rem;margin-bottom:8rem">
                <div class="title_inner_shadow">
                    Cara Join
                </div>
                <div class="black60_text_normal_18" style="margin-top: 24px">
                    Nah, jadi kepo cara join-nya kan? <br>
                    Tenang aja! Cusss kita langsung kasih tau caranya
                </div>
                <div class="row justify-content-between" style="margin-top: 90px">
                    <div class="col-md-4 col-sm-6 col-12 card__v2" data-toggle="modal" style="cursor: pointer;"
                        data-target="#modal_join">
                        <img src="{{ asset('/images/gabung_cara.png') }}" alt="">
                        <div class="black60_text_normal_18" style="margin-top: 24px">
                            Cara Bergabung
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12 card__v2" data-toggle="modal" style="cursor: pointer;"
                        data-target="#modal_login">
                        <img src="{{ asset('/images/daftar_cara.png') }}" alt="">
                        <div class="black60_text_normal_18" style="margin-top: 24px">
                            Cara Daftar
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12 card__v2" data-toggle="modal" style="cursor: pointer;"
                        data-target="#modal_lupa">
                        <img src="{{ asset('/images/lupa_cara.png') }}" alt="">
                        <div class="black60_text_normal_18" style="margin-top: 24px">
                            Lupa Password
                        </div>
                    </div>
                </div>
            </section>

            <section class="fitur__csb text-left container-fluid" style="margin-top: 5rem;overflow:hidden">
                <div class="row">
                    <div class="col-md-5 col-12">
                        <img src="{{ asset('/images/hp.png') }}" alt="">
                    </div>
                    <div class="col-md-7 col-12">
                        <p class="primary_text_normal_48">
                            Kamu berhasil memasuki <br>
                            <span class="title_inner_shadow">
                                UNIVERSE BADAK BAPER!
                            </span>
                        </p>
                        <div class="black60_text_normal_18" style="margin-top: 24px">
                            Nah, jadi ini tampilan website Club Sobat Badak. <br>
                            Udah pada gak sabar kan buat tau ada apa aja di website CSB. <br>
                            Cuss kita jelasin!
                        </div>
                        <div class="lelang__box" style="margin-top: 30px;">
                            <div class="lelang_carousel">
                                <div class="lelang_carousel_item">
                                    <div class="card card-custom gutter-b">
                                        <img src="{{ asset('/images/slide_v2_1.png') }}" alt="">
                                    </div>
                                </div>

                                <div class="lelang_carousel_item">
                                    <div class="card card-custom gutter-b">
                                        <img src="{{ asset('/images/slide_v2_2.png') }}" alt="">
                                    </div>
                                </div>

                                <div class="lelang_carousel_item">
                                    <div class="card card-custom gutter-b">
                                        <img src="{{ asset('/images/slide_v2_3.png') }}" alt="">
                                    </div>
                                </div>

                                <div class="lelang_carousel_item">
                                    <div class="card card-custom gutter-b">
                                        <img src="{{ asset('/images/slide_v2_4.png') }}" alt="">
                                    </div>
                                </div>

                                <div class="lelang_carousel_item">
                                    <div class="card card-custom gutter-b">
                                        <img src="{{ asset('/images/slide_v2_5.png') }}" alt="">
                                    </div>
                                </div>

                                <div class="lelang_carousel_item">
                                    <div class="card card-custom gutter-b">
                                        <img src="{{ asset('/images/slide_v2_6.png') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="dots__box"></div>
                        </div>
                    </div>
                </div>
                <div class="slide__rejeki justify-content-center" style="position: relative">
                    <div class="explain__box row">
                        <div class="col-md-8 col-12">
                            <p class="black60_text_normal_18">
                                Di website Club Sobat Badak, ada 2 game seru nih, salah satunya
                            </p>
                            <p class="title_inner_shadow_24">
                                Quiz Sobat Badak
                            </p>
                            <p class="black60_text_normal_18">
                                Sobat Badak akan diminta untuk melengkapi satu kata yang ada. <br>
                                Eits, tapi Sobat Badak harus bisa berpikir out of the box ya hihi
                            </p>
                        </div>
                        <div class="col-md-4 col-12">
                            <img src="{{ asset('/images/boleng-box.png') }}" alt="">
                        </div>
                    </div>
                </div>

                <div class="slide__auction justify-content-center" style="position: relative">
                    <div class="explain__box row">
                        <div class="col-md-8 col-12">
                            <p class="title_inner_shadow_24">
                                Event Auction
                            </p>
                            <p class="black60_text_normal_18">
                                Sobat Badak bisa ikutan Event Auction di Club Sobat Badak. <br>
                                Ni caranyaaa, cekidot!
                            </p>
                            <ul class="black60_text_normal_18">
                                <li>
                                    Pertama, masuk ke website Club Sobat Badak
                                </li>
                                <li>
                                    Pilih menu <b>“Event Auction”</b>
                                </li>
                                <li>
                                    Kalau ada tulisan <b>“LIVE NOW!” artinya lagi ada lelang yang berlangsung</b>
                                    (inget-inget
                                    ye)
                                </li>
                                <li>
                                    Lalu, kalian bisa langsung pasang bid deh dengan Baper Poin kalian
                                </li>
                                <li>
                                    Dan tentunya berharap semoga bisa memenangkan lelangnya hihi
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-12">
                            <img src="{{ asset('/images/boleng-box.png') }}" alt="">
                        </div>
                    </div>
                </div>

                <div class="slide__teman justify-content-center" style="position: relative">
                    <div class="explain__box row">
                        <div class="col-md-8 col-12">
                            <p class="title_inner_shadow_24">
                                Bawa Teman Sobat
                            </p>
                            <p class="black60_text_normal_18">
                                Ajak teman bisa dapet <span class="primary_text_normal_18"><b>1000 Baper Poin</b></span>
                                per
                                teman
                                yang diajak!!!
                            </p>
                            <p class="black60_text_normal_18">
                                Nah, Sobat Badak bisa ajak temen-temen kalian dengan cara
                                klik media sosial yang mau kalian pake untuk ajak temen-temen
                                kalian. Nah, pastiin mereka join dengan cara klik link yang
                                Sobat Badak kirimkan.
                            </p>
                        </div>
                        <div class="col-md-4 col-12">
                            <img src="{{ asset('/images/boleng-box.png') }}" alt="">
                        </div>
                    </div>
                </div>

                <div class="slide__poin justify-content-center" style="position: relative">
                    <div class="explain__box row">
                        <div class="col-md-8 col-12">
                            <p class="title_inner_shadow_24">
                                Baper Poin
                            </p>
                            <p class="black60_text_normal_18">
                                Nah sebelumnya pasti pada penasaran kan, ape sih Baper Poin itu? <br>
                                Kok daritadi disebut mulu <br>
                                Baper Poin adalah poin-poin yang Sobat Badak dapat kumpulkan, <br>
                                kemudian bisa digunakan untuk mengikuti Event Auction setiap hari-nya!
                            </p>
                            <p class="black60_text_normal_18">
                                Penasaran kan cara dapet Baper Poin?
                            </p>
                            <a href="#" class="btn btn-warning button_primary" style="margin-top:1rem">Bergabung
                                Sekarang</a>
                        </div>
                        <div class="col-md-4 col-12">
                            <img src="{{ asset('/images/boleng-box.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </section>
            <section class="join__zoom container-fluid" style="margin-top: 5rem;margin-bottom:8rem">
                <img src="/images/bg-join-zoom.png" alt="" class="bg__zoom">
                <div style="margin-top: 4rem;margin-left:10rem">
                    <p class="white_text_normal_48">
                        Ikutan ZOOM <br>
                        Club Sobat Badak, yuk!
                    </p>
                    <p class="white_text_500_18">
                        Sobat Badak hanya perlu masuk ke Website dan pencet Tiga Garis di <br>
                        Pojok Kanan Atas, lalu pilih “Join Zoom” <br>
                        And.. Voilaa! Sobat Badak akan langsung masuk ke Zoom kita
                    </p>
                    <p class="white_text_500_18">
                        Kita akan menemani dari pukul
                    </p>
                    <p class="white_text_500_22">
                        15.00 - 21.00 WIB setiap harinya!
                    </p>
                    <div class="d-flex" style="margin-bottom: 5rem">
                        <div class="card__soft text-center mr-6">
                            <img src="{{ asset('/images/kios.png') }}" alt="">
                            <p class="primary_text_bold_18 mt-2">
                                Games di
                                Room Ngopi
                            </p>
                        </div>
                        <div class="card__soft text-center mr-6">
                            <img src="{{ asset('/images/magnify.png') }}" alt="">
                            <p class="primary_text_bold_18 mt-2">
                                Baba Mencari
                                Bakat di Room
                                Lounge
                            </p>
                        </div>
                        <div class="card__soft text-center mr-6">
                            <img src="{{ asset('/images/live.png') }}" alt="">
                            <p class="primary_text_bold_18 mt-2">
                                WGM Live
                            </p>
                        </div>
                    </div>
                    <a href="{{ route('registerPage') }}" class="outline_btn_prim">Bergabung Sekarang</a>
                </div>
            </section>
            <section class="tips text-center" style="margin-top: 5rem;margin-bottom:8rem">
                <p class="title_inner_shadow">
                    TIPS & TRICK
                </p>
                <p class="black60_text_normal_18">
                    Untuk kalian yang gak ikut WGM Live, tapi ingin melakukan <br>
                    Spin The Wheel Baper Poin Kalian
                </p>
                <img src="{{ asset('/images/bg-coin.png') }}" alt="" style="width: 100%">
                <div style="padding-top: 10rem">

                    @if (Auth::user())
                        <a href="{{ route('homeDash') }}" class="btn btn-warning button_primary">Pelajari Sekarang</a>
                    @else
                        <a href="{{ route('loginPage') }}" class="btn btn-warning button_primary">Pelajari Sekarang</a>
                    @endif

                </div>
            </section>
            <section class="qr-code container-fluid " style="margin-top: 5rem;margin-bottom:8rem">
                <div class="d-flex justify-content-center">
                    <div class="___class_+?172___">
                        <img src="{{ asset('/images/qr-code.png') }}" alt="">
                    </div>
                    <div class="text-box">
                        <p class="white_text_600_42">
                            Jangan Ketinggalan!!!
                        </p>
                        <p class="white_text_500_18" style="margin-bottom: 4rem">
                            Sobat Badak mau tau informasi seputar Club Sobat Badak? <br>
                            Atau mau chatting-an juga sama sesama Sobat Badak? <br>
                            Kalian bisa scan QR code disamping atau klik tombol dibawah!
                        </p>
                        <a href="{{ route('registerPage') }}" class="outline_btn_prim">Bergabung Sekarang</a>
                    </div>
                </div>
            </section>

            <section class="sticker container " style="margin-top: 5rem;margin-bottom:8rem">
                <div class="row">
                    <div class="col-md-6 col-12 d-flex justify-content-end">
                        <div>
                            <p class="primary_text_bold_42" style="margin-top: 6rem">
                                Sticker Kece Edisi <br>
                                Club Sobat Badak!
                            </p>
                            <p class="black_text_normal_18">
                                Sobat Badak juga jangan lupa pake Sticker CSB ya, <br>
                                biar chatting-an nya lebih asoy!
                            </p>
                            <div class="d-flex" style="margin-top: 4rem">
                                <a href="https://t.me/addstickers/clubsobatbadak" target="_blank"
                                    class="btn btn-warning button_primary">Telegram Stiker</a>
                                <a href="https://sticker.ly/s/WDDG9S" target="_blank"
                                    class="button_primary_outline ml-6">Whatsapp Stiker</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12 d-flex justify-content-start">
                        <img src="/images/hp-sticker.png" alt="">
                    </div>
                </div>
            </section>

            <section class="faq__section container-fluid d-flex justify-content-around"
                style="align-items:center;margin-top: 5rem;">
                <p class="primary_text_bold_26">
                    Yang masih bingung boleh cek <br>
                    F.A.Q kita, yak!!
                </p>
                <div>
                    <a href="{{ route('faqPage') }}" class="btn btn-warning button_primary">F.A.Q Club Sobat Badak</a>
                </div>
            </section>

            <!-- Modal Banner-->
            <div class="modal" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1"
                aria-hidden="true">
                <div class="modal_banner">
                    <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                    <div class="d-flex" style="position: relative; height: 100%; width: 100%;">
                        <div>
                            <img class="modal__img" src="{{ asset('images/modal_v2_1.png') }}" alt=""
                                class="banner_desktop">
                        </div>
                        <div class="modal__body">
                            <p class="title_inner_shadow__modal" style="text-align: left">
                                Town Hall bersama <br>
                                Badak Baper
                            </p>
                            <p class="d_purple_normal_14">
                                Mau nongki-nongki cantik sama Badak Baper? <br>
                                Bisa banget! <br>
                                Tapi inget ya, Baper bukan berarti bawa perasaan ya, tapi <b>#bawaperubahan</b>, mantap!
                            </p>
                            <p class="d_purple_normal_14">
                                Si Badak Baper ini asalnya jauh banget loh, dari meteor jatuh. Tapi tenang, kalau temenan
                                sama
                                Badak
                                Baper pasti selalu ngakak karena hobinya ngehibur orang.
                            </p>
                            @if ($jam >= 15 && $jam <= 21)
                                <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                    target="_blank" class="livenow ml-3">
                                    <button class="btn">
                                        <div class="live-icon blink"></div> LIVE SEKARANG
                                    </button>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2"
                aria-hidden="true">
                <div class="modal_banner">
                    <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                    <div class="d-flex" style="position: relative;height: 100%;width: 100%;">
                        <div>
                            <img class="modal__img" src="{{ asset('images/modal_v2_2.png') }}" alt=""
                                class="banner_desktop">
                        </div>
                        <div class="modal__body">
                            <p class="title_inner_shadow__modal" style="text-align: left">
                                Theater bersama <br>
                                Jamet Kuproy & Bang Jago
                            </p>
                            <p class="d_purple_normal_14">
                                Buat lo pada yang suka <b>karaoke dan joget</b>,
                                tempat ini cocok buat kalian!
                            </p>
                            <p class="d_purple_normal_14">
                                Di Theater ini, Sobat Badak bakal seru-seruan bareng ditemenin si Jamet Kuproy dan Bang
                                Jago!
                            </p>
                            @if ($jam >= 15 && $jam <= 21)
                                <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                    target="_blank" class="livenow ml-3">
                                    <button class="btn">
                                        <div class="live-icon blink"></div> LIVE SEKARANG
                                    </button>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="modal3" tabindex="-1" role="dialog" aria-labelledby="modal3"
                aria-hidden="true">
                <div class="modal_banner">
                    <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                    <div class="d-flex"
                        style="position: relative;
                                                                                                                                                height: 100%;
                                                                                                                                                width: 100%;">
                        <div>
                            <img class="modal__img" src="{{ asset('images/modal_v2_3.png') }}" alt=""
                                class="banner_desktop">
                        </div>
                        <div class="modal__body">
                            <p class="title_inner_shadow__modal" style="text-align: left">
                                Baba Mencari Bakat
                            </p>
                            <p class="d_purple_normal_14">
                                Bokapnya Badak Baper yang hits banget dah pokoknya. <br>
                                Si kocheng eksis ini jago banget ngasihin ramalan cinta yang yak, rada ngaco.
                            </p>
                            <p class="d_purple_normal_14">
                                Sering kelihatan bareng Jamet Kuproy dan Bang Jago di Lounge. Punya acara sendiri di Lounge,
                                namanya
                                <br>
                                <b>“Baba Mencari Bakat”. </b> <br>
                                Jan lupa ikutan yaa! Bisa dapet Baper Poin gratis loh.
                            </p>
                            @if ($jam >= 15 && $jam <= 21)
                                <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                    target="_blank" class="livenow ml-3">
                                    <button class="btn">
                                        <div class="live-icon blink"></div> LIVE SEKARANG
                                    </button>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="modal4" tabindex="-1" role="dialog" aria-labelledby="modal4"
                aria-hidden="true">
                <div class="modal_banner">
                    <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                    <div class="d-flex"
                        style="position: relative;
                                                                                                                                                height: 100%;
                                                                                                                                                width: 100%;">
                        <div>
                            <img class="modal__img" src="{{ asset('images/modal_v2_4.png') }}" alt=""
                                class="banner_desktop">
                        </div>
                        <div class="modal__body">
                            <p class="title_inner_shadow__modal" style="text-align: left">
                                Lapak Mamak <br>
                                bersama Mamak
                            </p>
                            <p class="d_purple_normal_14">
                                Tempatnya si Mamak jualan Koin sama main main <br>
                                <b>Spin the Wheel</b> tiap harinya.
                            </p>
                            <p class="d_purple_normal_14">
                                Semua Sobat Badak pasti kenal sama si Mamak apalagi lipstick merah wadidawnya, omongan
                                ngegas
                                kek
                                gak ada rem, dan sandal jepit andalannya.
                            </p>
                            @if ($jam >= 15 && $jam <= 21)
                                <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                    target="_blank" class="livenow ml-3">
                                    <button class="btn">
                                        <div class="live-icon blink"></div> LIVE SEKARANG
                                    </button>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="modal5" tabindex="-1" role="dialog" aria-labelledby="modal5"
                aria-hidden="true">
                <div class="modal_banner">
                    <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                    <div class="d-flex"
                        style="position: relative;
                                                                                                                                                height: 100%;
                                                                                                                                                width: 100%;">
                        <div>
                            <img class="modal__img" src="{{ asset('images/modal_v2_5.png') }}" alt=""
                                class="banner_desktop">
                        </div>
                        <div class="modal__body">
                            <p class="title_inner_shadow__modal" style="text-align: left">
                                Auction House <br>
                                bersama BOS TUDUNG
                            </p>
                            <p class="d_purple_normal_14">
                                <b>Satu-satunya tempat AUCTION yang ASLI</b> tanpa rupiah!! <br>
                                Cuma pake Baper Poin weh!
                            </p>
                            <p class="d_purple_normal_14">
                                Penjaga tempat ini misterius banget, bahkan mukanya aja tuh belom ada yang pernah liattt.
                                <br>
                                Tapi gitu-gitu, dia disayang banyak orang, maklum suka ngasih barang lelang yang muantep
                                banget
                                kualitasnya.
                            </p>
                            @if ($jam >= 15 && $jam <= 21)
                                <a href="https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09"
                                    target="_blank" class="livenow ml-3">
                                    <button class="btn">
                                        <div class="live-icon blink"></div> LIVE SEKARANG
                                    </button>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal" id="modal_join" tabindex="-1" role="dialog" aria-labelledby="modal_join"
                aria-hidden="true">
                <div class="modal-dialog-centered">
                    <div class="modal_banner">
                        <div class="text-center title_inner_shadow mt-4">
                            Cara Join
                        </div>
                        <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                        <div class="modal-body" style="padding:3rem">
                            <ol class="black60_text_normal_18 list_custom_point">
                                <li>
                                    <div class="poin_list text-center">1</div>
                                    <div>
                                        Sobat Badak bisa buka website sobatbadak.club
                                        atau scan barcode di pojok kanan atas.
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">2</div>
                                    <div>
                                        Buat yang belum punya akun, kalian bisa
                                        pencet garis tiga yang ada di kanan atas
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">3</div>
                                    <div>
                                        Setelah itu, pencet “DAFTAR” yang ada di
                                        paling bawah, ya!
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">4</div>
                                    <div>
                                        Kemudian, sobat Badak tinggal isi data diri
                                        yang diperlukan. <br> Nah, kalo mau lebih gampang, langsung pilih masuk dengan akun
                                        Google kalian
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">5</div>
                                    <div>
                                        Jangan lupa buat verifikasi nomor WhatsApp
                                        kalian ya!
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">6</div>
                                    <div>
                                        SELESAI DEH! Selamat kamu udah resmi
                                        terdaftar di Club Sobat Badak!
                                    </div>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal" id="modal_login" tabindex="-1" role="dialog" aria-labelledby="modal_login"
                aria-hidden="true">
                <div class="modal-dialog-centered">
                    <div class="modal_banner">
                        <div class="text-center title_inner_shadow mt-4">
                            Cara Log In
                        </div>
                        <div class="text-center">
                            <img class="modal_img_center mt-4" src="{{ asset('images/barcode_login_modal.png') }}"
                                alt="">
                        </div>
                        <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                        <div class="modal-body" style="padding:3rem">
                            <ol class="black60_text_normal_18 list_custom_point">
                                <li>
                                    <div class="poin_list text-center">1</div>
                                    <div>
                                        Sobat Badak bisa buka website sobatbadak.club
                                        atau scan barcode di atas ya!
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">2</div>
                                    <div>
                                        Buat yang sudah punya akun, kalian bisa
                                        pencet garis tiga yang ada di kanan atas
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">3</div>
                                    <div>
                                        Kemudian, Sobat Badak bisa tekan tulisan
                                        “MASUK”
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">4</div>
                                    <div>
                                        Kelar dehh! Sobat Badak berhasil masuk di
                                        website Club Sobat Badak!
                                    </div>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal" id="modal_lupa" tabindex="-1" role="dialog" aria-labelledby="modal_lupa"
                aria-hidden="true">
                <div class="modal-dialog-centered">
                    <div class="modal_banner">
                        <div class="text-center title_inner_shadow mt-4">
                            Lupa Password ?
                        </div>
                        <button type="button" class="close__banner" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" style="color: white" class="ki ki-close"></i></button>
                        <div class="modal-body" style="padding:3rem">
                            <ol class="black60_text_normal_18 list_custom_point">
                                <li>
                                    <div class="poin_list text-center">1</div>
                                    <div>
                                        Pas kalian masuk terus lupa sama password,
                                        kalian bisa pencet tulisan
                                        “Lupa Password?”
                                    </div>
                                </li>
                                <li>
                                    <div class="poin_list text-center">2</div>
                                    <div>
                                        Selanjutnya, kalian tinggal masukin email
                                        kalian dan mengikuti arahannya, deh!
                                    </div>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('pageJS')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js"
        integrity="sha512-UxP+UhJaGRWuMG2YC6LPWYpFQnsSgnor0VUF3BHdD83PS/pOpN+FYbZmrYN+ISX8jnvgVUciqP/fILOXDjZSwg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script>
        $(window).scroll(function(event) {
            var scroll = $(window).scrollTop();
            if (scroll < 2200) {
                $("#par_img3").addClass('hide__animation')
                $("#par_img3").removeClass('show__animation')
            } else {
                $("#par_img3").removeClass('hide__animation')
                if (!$("#par_img3").hasClass('show__animation')) {
                    $("#par_img3").addClass('show__animation')
                }
            }
        });
        $(function() {
            $("#par_img3").addClass('hide__animation')
            $("#par_img3").removeClass('show__animation')
            var wipeAnimation = new TimelineMax()
                .fromTo("#content_1", 1, {
                    y: "0%"
                }, {
                    y: "-100%",
                    ease: Linear.easeNone
                })
                .fromTo("#par_img1", 1, {
                    y: "-60%"
                }, {
                    y: "-140%",
                    ease: Linear.easeNone
                })
                // .fromTo("#par_img2", 1, {
                //     y: "20%"
                // }, {
                //     y: "-160%",
                //     ease: Linear.easeNone
                // })
                .fromTo("#par_img3", 1, {
                    y: "-20%"
                }, {
                    y: "-100%",
                    ease: Linear.easeNone
                });

            var controller = new ScrollMagic.Controller();
            new ScrollMagic.Scene({
                    triggerElement: "#paralax",
                    triggerHook: "onLeave",
                    duration: 4000
                })
                .setPin("#paralax")
                .setTween(wipeAnimation)
                .addTo(controller);
        })

        $('.slide__rejeki').css("display", 'flex')
        $('.slide__auction').css('display', 'none')
        $('.slide__teman').css('display', 'none')
        $('.slide__poin').css('display', 'none')

        $('.lelang_carousel').slick({
            dots: true,
            appendDots: '.dots__box',
            arrows: true,
            slidesToShow: 5,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            focusOnSelect: true,
            nextArrow: '.slick-next',
            prevArrow: '.slick-prev',
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        centerPadding: '10px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        centerPadding: '10px',
                        slidesToShow: 3
                    }
                }
            ]
        });

        $('.lelang_carousel').on('afterChange', function(event, slick, currentSlide) {
            if (currentSlide == 0) {
                $('.slide__rejeki').css("display", 'flex')
                $('.slide__auction').css('display', 'none')
                $('.slide__teman').css('display', 'none')
                $('.slide__poin').css('display', 'none')
            } else if (currentSlide == 1) {
                $('.slide__rejeki').css('display', 'none')
                $('.slide__auction').css("display", 'flex')
                $('.slide__teman').css('display', 'none')
                $('.slide__poin').css('display', 'none')
            } else if (currentSlide == 2) {
                $('.slide__rejeki').css('display', 'none')
                $('.slide__auction').css('display', 'none')
                $('.slide__teman').css("display", 'flex')
                $('.slide__poin').css('display', 'none')
            } else if (currentSlide == 3) {
                $('.slide__rejeki').css('display', 'none')
                $('.slide__auction').css('display', 'none')
                $('.slide__teman').css('display', 'none')
                $('.slide__poin').css("display", 'flex')
            } else {
                $('.slide__rejeki').css("display", 'flex')
                $('.slide__auction').css('display', 'none')
                $('.slide__teman').css('display', 'none')
                $('.slide__poin').css('display', 'none')
            }
        })
    </script>
@endsection
