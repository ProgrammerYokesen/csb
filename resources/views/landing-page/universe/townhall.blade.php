@extends('landing-page.master')

@section('content')
    <section class="universe_hero">

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6 universe_img">
                    <img src="{{ asset('images/universe/townhall.png') }}" alt="">
                </div>
                <div class="col-lg-6 universe_hero_txt">
                    <h1>Townhall</h1>
                    <h3>/ˈˌtoun ˈhôl/ n</h3>
                    <h5>Tempatnya kamu bisa berjumpa dan ngobrol bareng dengan Badak Baper</h5>

                    {{-- <div class="text-right"> --}}
                    <a href="{{ route('joinZoom') }}" target="_blank">
                        <button class="btn">Join Zoom</button>
                    </a>
                    {{-- </div> --}}
                </div>
            </div>
        </div>

    </section>

    <section class="section-content universe_town_section">
        <div class="container-fluid" style="overflow: hidden">
            <div class="row">
                <div class="col-lg-6 universe_content_txt">
                    <h2>PPKM Bikin Kamu Gabut !?</h2>
                    <h3>Join Club Sobat Badak aja yuk!</h3>
                    <h5>Tempat kamu belajar, ngobrol, bertukar cerita dan pengalaman setiap hari bersama Sobat Badak lainnya
                        di seluruh Indonesia</h5>
                </div>

                <div class="col-lg-6 text-right universe_content_img">
                    <img src="{{ asset('images/universe/city.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="section-content universe_footer">
        <div class="container">
            <h2>Yuk kita ngumpul Disini!</h2>
            <a href="{{ route('joinZoom') }}" target="_blank">
                <button class="btn">Join Zoom</button>
            </a>
        </div>
    </section>



    <!-- Button trigger modal-->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button>

    <!-- Modal Login Register-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: transparent">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left: auto">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- ========== --}}
                    <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#login">
                                <span class="nav-text">Login</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#register" aria-controls="profile">
                                <span class="nav-text">Register</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content mt-5" id="myTabContent">

                        {{-- LOGIN FORM --}}
                        <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="home-tab">
                            <div class="form text-center">
                                <div class="form-group">
                                    <input id="email-log" type="email" name="email" class="form-control form-control-solid"
                                        placeholder="Email" />
                                    <div class="input-text-error" id="error-email"></div>
                                </div>
                                <div class="form-group">
                                    <div style="position: relative">
                                        <input id="pass-log" type="password" name="password"
                                            class="form-control form-control-solid" placeholder="Password" />
                                        <i class="fas fa-eye" id="togglePassword" onclick="togglePassword('pass-log')"
                                            style="position: absolute; right:0.5rem;top:0.9rem; cursor: pointer;"></i>
                                    </div>
                                    <div class="input-text-error" id="error-password">
                                    </div>
                                </div>
                                <div class="form-group text-left">
                                    {!! NoCaptcha::display() !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <div>
                                        <label class="checkbox">
                                            <input type="checkbox" value="true" name="remember" />
                                            <span class="mr-2"></span>
                                            Ingat saya
                                        </label>
                                    </div>
                                    <div>
                                        <a href="/forgot-password" class="forgot-pass-text">
                                            Lupa password?
                                        </a>
                                    </div>
                                </div>

                                <div class="form-button">
                                    <button id="buttom-submit" class="btn custom-btn-primary-rounded"
                                        onclick="login()">Masuk</button>
                                </div>
                            </div>
                        </div>

                        {{-- REGISTER FORM --}}
                        <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="profile-tab">
                            <div id="form-auth" class="form text-center">
                                <div class="form-group pr-8">
                                    <input id="name-regis" type="name" name="name" class="form-control form-control-solid"
                                        placeholder="Nama" />
                                    <div class="input-text-error" id="error-name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex">
                                        <input id="email-regis" type="email" name="email"
                                            class="form-control form-control-solid" placeholder="Email" />
                                        <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                            data-toggle="tooltip" data-placement="right" title="Email wajib diisi">
                                    </div>
                                    <div class="input-text-error" id="error-email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"
                                                    style="border: transparent">+62</span></div>
                                            <input id="wa-regis" name="whatsapp" type="number"
                                                class="form-control form-control-solid" placeholder="No. Whatsapp" />
                                        </div>
                                        <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                            data-toggle="tooltip" data-placement="right"
                                            title="Wajib diisi nomor whatsapp yang aktif untuk pengiriman link Zoom">
                                    </div>
                                    <div class="input-text-error" id="error-wa">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex" style="position: relative">
                                        <input id="pass-regis" type="password" name="password"
                                            class="form-control form-control-solid" placeholder="Password" />
                                        <i class="fas fa-eye input-password" id="togglePassword"
                                            onclick="togglePasswordRegis('pass-regis')"
                                            style="position: absolute; right:3rem;top:0.9rem;cursor: pointer;"></i>
                                        <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                            data-toggle="tooltip" data-placement="right" title="Password wajib diisi">
                                    </div>
                                    <div class="input-text-error" id="error-password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex" style="position: relative">
                                        <input id="confirm-regis" type="password" name="password_confirmation"
                                            class="form-control form-control-solid" placeholder="Konfirmasi Password" />
                                        <i class="fas fa-eye input-confirm" id="togglePassword"
                                            onclick="togglePasswordRegis('confirm-regis')"
                                            style="position: absolute; right:3rem;top:0.9rem;cursor: pointer;"></i>
                                        <img class="ml-2" src="/images/icons/tooltip.svg" alt="" data-container="body"
                                            data-toggle="tooltip" data-placement="right"
                                            title="Wajib diisi dan sama dengan password di atas">
                                    </div>
                                    <div class="input-text-error" id="error-confirm">
                                    </div>
                                </div>
                                <div class="form-group text-left">
                                    {!! NoCaptcha::display() !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group mt-4">
                                    <div>
                                        <label class="checkbox">
                                            <input type="checkbox" value="1" name="remember" />
                                            <span class="mr-2"></span>
                                            Saya bersedia dihubungi melalui Email
                                        </label>
                                    </div>
                                </div>
                                <div class="form-button">
                                    <button id="submitButton" class="btn custom-btn-primary-rounded"
                                        onclick="register()">Daftar</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    {{-- ========== --}}
                </div>
                <div class="modal-footer" style="border: transparent">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pageJS')

    <script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>

    <script>
        function togglePassword(elmID) {
            let password = document.getElementById(elmID);
            if (password.type === "password") {
                password.type = "text";
                $("#togglePassword").removeClass("fa-eye")
                $("#togglePassword").addClass("fa-eye-slash")
            } else {
                password.type = "password";
                $("#togglePassword").removeClass("fa-eye-slash")
                $("#togglePassword").addClass("fa-eye")
            }
        }

        function togglePasswordRegis(elmID) {
            let password = document.getElementById(elmID);
            if (password.type === "password") {
                password.type = "text";
                console.log(`#${elmID}`)
                $(`.${elmID}`).removeClass("fa-eye")
                $(`.${elmID}`).addClass("fa-eye-slash")
            } else {
                password.type = "password";
                $(`.${elmID}`).removeClass("fa-eye-slash")
                $(`.${elmID}`).addClass("fa-eye")
            }
        }
    </script>

    <script>
        // Process Login
        // function login() {
        //     let email = document.getElementById('email-log').value
        //     let password = document.getElementById('pass-log').value

        //     // console.log(email, password)

        //     if (email == "" || password == "") {
        //         Swal.fire("Mohon isi Email dan Password", "", "warning");
        //         return
        //     }

        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
        //         }
        //     });

        //     $.ajax({
        //         type: 'POST',
        //         url: '{{ route('postRegister') }}',
        //         data: {
        //             email: email,
        //             password: password
        //         },
        //         success: function(data) {
        //             console.log(data)
        //             $('#email-log').val('');
        //             $('#pass-log').val('');
        //         },
        //         error: function(data) {
        //             Swal.fire("Email atau Password Salah", "Silahkan coba lagi", "error");
        //         }

        //     });
        // }


        // Process Register
        // function register() {
        //     let nama = document.getElementById('name-regis').value
        //     let email = document.getElementById('email-regis').value
        //     let whatsapp = document.getElementById('wa-regis').value
        //     let password = document.getElementById('pass-regis').value
        //     let confirmPass = document.getElementById('confirm-regis').value

        //     console.log(nama, email, whatsapp, password, confirmPass)

        //     if (nama == "" || email == "" || whatsapp == "" || password == "" || confirmPass == "") {
        //         Swal.fire("Mohon isi form dengan lengkap", "", "warning");
        //         return
        //     }

        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
        //         }
        //     });

        //     $.ajax({
        //         type: 'POST',
        //         url: '{{ route('postRegister') }}',
        //         data: {
        //             name: name,
        //             email: email,
        //             whatsapp: whatsapp,
        //             password: password,
        //             confirmPass: confirmPass
        //         },
        //         success: function(data) {
        //             console.log(data)
        //             $('#input-name').val('');
        //             $('#input-email').val('');
        //             $('#input-wa').val('');
        //             $('#input-masukan').val('');

        //             Swal.fire("Berhasil Daftar!", "Jangan lupa join yaa!", "success");
        //         },
        //         error: function(data) {
        //             Swal.fire("Ada kesalahan dalam server", "Silahkan refresh dan coba lagi", "error");
        //         }

        //     });
        // }
    </script>

@endsection
