@extends('landing-page.master')

@section('content')
    <section class="universe_hero">

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6 universe_img">
                    <img src="{{ asset('images/universe/lapak-mamak.png') }}" alt="">
                </div>
                <div class="col-lg-6 universe_hero_txt">
                    <h1>Lapak Mamak</h1>
                    <h3>/la.pak/ n</h3>
                    <h5>Tempatnya para kaum hobi belanja berkumpul. Temukan kebutuhanmu di Lapak Mamak, dan unboxing hadiah
                        yang kamu dapatkan di sini.
                    </h5>

                    <a href="{{ route('joinZoom') }}" target="_blank">
                        <button class="btn">Join Zoom</button>
                    </a>
                </div>
            </div>
        </div>

    </section>

    <section class="section-content universe_town_section">
        <div class="container-fluid" style="overflow: hidden">
            <div class="row">
                <div class="col-lg-6 universe_content_txt">
                    <h2>PPKM Bikin Kamu Gabut !?</h2>
                    <h3>Join Club Sobat Badak aja yuk!</h3>
                    <h5>Tempat kamu belajar, ngobrol, bertukar cerita dan pengalaman setiap hari bersama Sobat Badak lainnya
                        di seluruh Indonesia</h5>
                </div>

                <div class="col-lg-6 text-right universe_content_img">
                    <img src="{{ asset('images/universe/city.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="section-content universe_footer">
        <div class="container">
            <h2>Dapetin Baper Poin melimpah di sini!</h2>
            <a href="{{ route('joinZoom') }}" target="_blank">
                <button class="btn">Join Zoom</button>
            </a>
        </div>
    </section>
@endsection


@section('pageJS')

@endsection
