@extends('landing-page.master')

@section('content')
    <section class="universe_hero">

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6 universe_img">
                    <img src="{{ asset('images/universe/baba.png') }}" alt="">
                </div>
                <div class="col-lg-6 universe_hero_txt">
                    <h1>Baba Mencari Bakat-BMB</h1>
                    <h3>/ba·ba . men·ca·ri . ba·kat/ n </h3>
                    <h5>Tempat Baba mencari Sobat Badak yang memiliki bakat terpendam!</h5>

                    <a href="{{ route('joinZoom') }}" target="_blank">
                        <button class="btn">Join Zoom</button>
                    </a>
                </div>
            </div>
        </div>

    </section>

    <section class="section-content universe_town_section">
        <div class="container-fluid" style="overflow: hidden">
            <div class="row">
                <div class="col-lg-6 universe_content_txt">
                    <h2>PPKM Bikin Kamu Gabut !?</h2>
                    <h3>Join Club Sobat Badak aja yuk!</h3>
                    <h5>Tempat kamu belajar, ngobrol, bertukar cerita dan pengalaman setiap hari bersama Sobat Badak lainnya
                        di seluruh Indonesia</h5>
                </div>

                <div class="col-lg-6 text-right universe_content_img">
                    <img src="{{ asset('images/universe/city.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="section-content universe_footer">
        <div class="container">
            <h2>Tunjukan bakat dan minatmu ke Baba!</h2>
            <a href="{{ route('joinZoom') }}" target="_blank">
                <button class="btn">Join Zoom</button>
            </a>
        </div>
    </section>
@endsection


@section('pageJS')

@endsection
