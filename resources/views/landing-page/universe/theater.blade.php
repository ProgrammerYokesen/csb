@extends('landing-page.master')

@section('content')
    <section class="universe_hero">

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6 universe_img">
                    <img src="{{ asset('images/universe/cinema.png') }}" alt="">
                </div>
                <div class="col-lg-6 universe_hero_txt">
                    <h1>Theater</h1>
                    <h3>/the·a·ter/ /téater/ n</h3>
                    <h5>Tempatnya kamu melepas penat di room Theater sambil karoke dan nonton bareng Sobat Badak lainnya.
                    </h5>

                    <a href="{{ route('joinZoom') }}" target="_blank">
                        <button class="btn">Join Zoom</button>
                    </a>
                </div>
            </div>
        </div>

    </section>

    <section class="section-content universe_town_section">
        <div class="container-fluid" style="overflow: hidden">
            <div class="row">
                <div class="col-lg-6 universe_content_txt">
                    <h2>PPKM Bikin Kamu Gabut !?</h2>
                    <h3>Join Club Sobat Badak aja yuk!</h3>
                    <h5>Tempat kamu belajar, ngobrol, bertukar cerita dan pengalaman setiap hari bersama Sobat Badak lainnya
                        di seluruh Indonesia</h5>
                </div>

                <div class="col-lg-6 text-right universe_content_img">
                    <img src="{{ asset('images/universe/city.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="section-content universe_footer">
        <div class="container">
            <h2>Ayo Nyanyi & joget bareng biar asik!</h2>
            <a href="{{ route('joinZoom') }}" target="_blank">
                <button class="btn">Join Zoom</button>
            </a>
        </div>
    </section>
@endsection


@section('pageJS')

@endsection
