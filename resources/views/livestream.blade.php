<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Club Sobat Badak</title>
    <meta name="description" content="Club Sobat Badak" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('live-event/livestream.css') }}">

    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <!--end::Layout Themes-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}" />
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body"
    class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading"
    style="background: #05f62d">
    <!--begin::Main-->

    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

                {{-- ===================================================================================================================================================================== --}}
                <div class="livestream_wrapper">
                    {{-- <a href="javascript:;" id="kt_notify_btn_sopi" class="btn btn-success">Display Sopi</a>
                    <a href="javascript:;" id="kt_notify_btn_tokped" class="btn btn-success">Display Tokped</a>
                    <a href="javascript:;" id="kt_notify_btn_bli" class="btn btn-success">Display Bli</a> --}}
                    <button class="btn" onclick="getOrder()">Click</button>

                    <div class="livestream_marquee_wrapper">
                        <!--<div class="livestream_marquee">-->
                        <!--    <div class="marquee" id="running_text">-->
                        <!--        {{-- <div class="marquee_content">Yoke Endarto - INV2001/201/31241523*** - 100 Item</div>-->
                        <!--        <div class="marquee_content">Yoke Endarto - INV2001/201/31241523*** - 100 Item</div>-->
                        <!--        <div class="marquee_content">Yoke Endarto - INV2001/201/31241523*** - 100 Item</div>-->
                        <!--        <div class="marquee_content">Yoke Endarto - INV2001/201/31241523*** - 100 Item</div>-->
                        <!--        <div class="marquee_content">Yoke Endarto - INV2001/201/31241523*** - 100 Item</div> --}}-->
                        <!--    </div>-->

                        <!--</div>-->
                        <div id="marquee"></div>
                    </div>


                </div>

                {{-- ===================================================================================================================================================================== --}}

            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->


    <!--begin::Global Config(global config for global JS scripts)-->
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#6993FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1E9FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <!--end::Global Theme Bundle-->
    <!--end::Page Vendors-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>
    <!--end::Page Scripts-->

    <!--<script src="https://cdn.jsdelivr.net/npm/jquery.marquee@1.6.0/jquery.marquee.min.js"></script>-->
    <!--<script>-->
    <!--    $('.marquee').marquee({-->
    <!--        duration: 10000,-->
    <!--        gap: 50,-->
    <!--        delayBeforeStart: 0,-->
    <!--        direction: 'left',-->
    <!--        duplicated: false-->
    <!--    });-->
    <!--</script>-->

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/dynamic-marquee@2"></script>

    <script>
        // Setting Bubble
        let notifTrigger = (nama, inv, tgl, qty, platform, orderItemName, orderSKU, orderType) => {
            var content = {};



            content.message = `<div class="order_detail">
                                    <p>${nama}</p>
                                    <p>${inv}</p>
                                    ${orderItemName.map(el => `<p>${el}</p>`).join("")}
                                    ${orderSKU.map(el => `<p>${el}</p>`).join("")}
                                    <p>${qty} Bundle</p>
                                </div>`;

            content.title = `<div class="order_header">
                                <p>${orderType == "CSB" ? "Unboxing" : orderType == "STW" ? "Pre-Order" : "Order"}</p>
                                <p>${tgl}</p>
                            </div>`;

            var notify = $.notify(content, {
                type: platform == "Shopee" ? "danger" : platform == "Tokopedia" ? "dark" :
                    // platform == "Blibli" ? "primary" :
                    "dark", //Warna Background Toastr, primary = blibli, success = tokped, danger = shopee
                allow_dismiss: false, //Close Button
                newest_on_top: true,
                mouse_over: true,
                showProgressbar: false,
                spacing: 10, //Jarak antar toast
                timer: 0, //Waktu Toastr Muncul
                placement: {
                    from: 'top',
                    align: 'right'
                },
                offset: {
                    x: 5,
                    y: 5
                },
                delay: 3000,
                z_index: 10000,
                animate: {
                    enter: 'animate__animated animate__' + 'bounceInRight',
                    exit: 'animate__animated animate__' + 'bounceInRight'
                }
            });
        }

        // Setting Marquee
        var $marquee = document.getElementById('marquee');
        var marquee = (window.m = new dynamicMarquee.Marquee($marquee, {
            rate: -100,
        }));
        window.l = dynamicMarquee.loop(
            marquee,
            [],
            function() {
                var $separator = document.createElement('div');
                $separator.innerHTML = '&nbsp &nbsp|&nbsp &nbsp';
                return $separator;
            }
        );



        function appendMarq(orderData) {
            const $item = document.createElement('div');
            // console.log('updated')
            var dataOrderan = []
            orderData.forEach(el => {
                dataOrderan.push(() => `${el}`)
            })
            // console.log(dataOrderan)


            // window.l.update([() => 'new item 1', () => 'new item 2']);
            window.l.update(dataOrderan);

            if (!marquee.isWaitingForItem()) {
                marquee.appendItem($item);
            }
        }


        // Variable u/ Tampung Data Order
        let tampungDataOrder = []

        // AJAX for get Order
        function getOrder() {

            var currentdate = new Date();
            let year = currentdate.getFullYear()
            let month = currentdate.getMonth() < 10 ? ("0" + (currentdate.getMonth() + 1)) : currentdate.getMonth()
            let day = currentdate.getDate() < 10 ? ("0" + currentdate.getDate()) : currentdate.getDate()
            let hour = currentdate.getHours() < 10 ? ("0" + currentdate.getHours()) : currentdate.getHours()
            let minute = currentdate.getMinutes() < 10 ? ("0" + currentdate.getMinutes()) : currentdate.getMinutes()
            let second = currentdate.getSeconds() < 10 ? ("0" + currentdate.getSeconds()) : currentdate.getSeconds()

            let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second

            $.ajax({
                type: 'GET',
                url: `/getdata/${datetime}`,

                success: function(data) {
                    let dataOrder = data
                    // console.log('get')
                    // console.log(dataOrder);
                    dataOrder.forEach(element => {
                        if (tampungDataOrder.includes(element.id) == false) {
                            tampungDataOrder.push(element.id);
                            let timenya = element.created_at.split(" ")
                            var orderInv = element.invoice.substring(0, element.invoice.length - 3)
                            orderInv += "***"

                            orderItemName = []
                            orderSKU = []
                            element.order.forEach(e => {
                                orderItemName.push(e.item)
                                if (e.sku !== null) {
                                    orderSKU.push(e.sku)
                                }
                            })

                            var tipeOrder
                            var orderType
                            if (element.sku !== null) {
                              var tipeOrder = element.sku.split("-")
                              var orderType = tipeOrder[0]
                            }
                            // console.log(orderType)

                            notifTrigger(element.nama, orderInv, timenya[1],
                                element.qtyTotal, element.platform, orderItemName, orderSKU, orderType)
                            $('.js-marquee').append(
                                `<div class="marquee_content">${element.nama} - ${orderInv} - ${element.qtyTotal} Bundle</div>`
                            );
                        }


                    });
                }
            });

            // Get All data today
            $.ajax({
                type: 'GET',
                url: `/getdata-day`,

                success: function(data) {
                    // console.log(data)
                    let dataMarq = []
                    data.forEach(el => {
                        dataMarq.push(`${el.nama} - ${el.invoice.substring(0, el.invoice.length - 3)}*** - ${el.qtyTotal} Bundle`)
                    })
                    // console.log(dataMarq)

                    appendMarq(dataMarq)
                }
            });
        }

        // Get Data tiap 1 menit sekali
        $(document).ready(function() {
            setInterval(function() {
                getOrder()
            }, 100000);
        });
    </script>



</body>
<!--end::Body-->

</html>
