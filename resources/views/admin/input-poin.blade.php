@extends("crudbooster::admin_template")

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div style='background-color:white;padding:20px; width:50%;'>
        <div>
            <h2><b>Input Baper Poin</b></h2>
            <form style='margin-top:50px;' action='{{route('bpKoinTidakSesuai')}}' method='POST'>
                @csrf
              <div class="form-group">
                <label for="user">User</label>
                <select class="form-control" id="user" name='user' required>
                    @foreach($users as $user)
                     <option value={{$user->id}}>{{$user->name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group">
                <input type="hidden" class="form-control" id="from" value='Tukar Koin' name='from'>
                <input type="hidden" class="form-control" id="dkId" value='{{$id}}' name='fkId'>
              </div>
              <div class="form-group">
                <label for="poin">Poin</label>
                <input type="text" class="form-control" id="poin" name='poin' required>
              </div>
              <div class="form-group">
                <label for="poin">Date</label>
                <input type="text" class="form-control" id="date" name='date'required>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection

@section('pageJS')
 <script>
     $('#date').datepicker();
 </script>
@endsection
