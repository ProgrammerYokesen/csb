@extends("crudbooster::admin_template")

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="{{ set_active('riwayatBP') }}"><a href="{{ route('riwayatBP') }}">Per transaction</a></li>
              <li class="{{ set_active(['riwayatBPUser', 'riwayatBPuserDetail']) }}"><a href="{{ route('riwayatBPUser') }} ">Per user</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Per User</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Total Poin</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data )

                                <tr>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ number_format($data->sum) }}</td>
                                    <td>   <a class="btn btn-primary" href="{{ route('riwayatBPuserDetail', $data->id) }}">
                                        <i class="fa fa-align-center"></i> Detail
                                      </a>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}

                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('jsPage')

@endsection
