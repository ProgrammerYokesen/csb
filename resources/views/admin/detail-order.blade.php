@extends("crudbooster::admin_template")
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section("content")

<div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Dari

      <address>
        <strong>Warisan Gajahmada</strong><br>
        The Prominence Office Tower <br>
        Alam Sutera, Tangerang Selatan<br>
        Email: info@warisangajahmada.com
        <br>
        <br>
        <br>
        <label for="toko">Cabang/Depo</label>
        <br>
        <strong>{{$toko->nama_ukm}}</strong>
      </address>

    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      To
      <address>
        <p><strong>{{$pesanan->nama}}</strong></p>
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      Detail Transaksi
        <p><b>No Invoice : {{$pesanan->invoice}}</a> </b></p>
        <p>Payment : PAID</p>
        <p>Platform : {{$pesanan->platform}}</p>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Product</th>
          <th>Qty</th>
        </tr>
        </thead>
        <tbody>
          @foreach($datas as $row)
        <tr>

          <td>{{$row->nama_item}}</td>
          <td>{{$row->qty}}</td>
        </tr>
          @endforeach

        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  
    <!-- Table row -->
      <div class="row">
          
        <form method="POST" action="{{route('spinwheelPoin')}}">
            @csrf
        <!--<input class="form-control" name="poin[{{$i}}]" type="hidden">-->
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th class="text-center">Undian</th>
              <th class="text-center">Jenis Spinwheel (berdasarkan koin)</th>
              <th class="text-center">Jumlah Poin</th>
            </tr>
            </thead>
            <tbody>
              @for($i = 1;$i <= $pesanan->qtyTotal ; $i++)
            <tr>
              <td class="text-center">
                    {{$i}}
              </td>
              <td class="text-center">
                  <div class="form-group">
                      <select class="form-control" name="cat_spinwheel[{{$i}}]">
                          <option> Pilih Spin Wheel</option>
                          @foreach($cat as $ct)
                            <option>{{ $ct->category }}</option>
                          @endforeach
                      </select>
                    </div>
              </td>
              <td class="text-center">
                  <input class="form-control" name="poin[{{$i}}]" type="text">
              </td>
            </tr>
              @endfor
              <tr>
                  <td>Masukan Email User</td>
                  <td colspan="2">
                        <select class="cari form-control" style="width:100%;" name="cari" required></select>
                  </td>
              </tr>
    
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
      <input type="hidden" name="invoice" value="{{$pesanan->invoice}}">
      <input type="hidden" name="orderId" value="{{$id}}">
    <div class="col-xs-12">
      <button type="submit" class="btn btn-primary pull-right">SUBMIT</button>
    </div>
  </div>

              </form>
</section>
<div id="editor"></div>

<!-- /.content -->
<div class="clearfix"></div>

@endsection

@section('jsPage')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
 $('.cari').select2({
    placeholder: 'Cari...',
    ajax: {
      url: '{{route('cariEmail')}}',
      dataType: 'json',
      delay: 100,
      processResults: function (data) {
          console.log(data);
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.email,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });
</script>
@endsection