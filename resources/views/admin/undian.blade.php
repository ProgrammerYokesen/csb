@extends("crudbooster::admin_template")

@section('css')

@endsection

@section('content')
<div class="pad margin no-print">
    <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h3>Undian Hari Ini</h3>
    </div>
  </div>

    <div class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <form action="{{ route('undianHariIni') }}" method="GET">
                <!-- Date -->
                <div class="form-group">
                    <label>Fillter Date:</label>

                    <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="date" class="form-control pull-right" id="datepicker">
                    </div>
                    <!-- /.input group -->
                </div>
                <button class="btn btn-primary">Submit</button>
                </form>
                <!-- /.form group -->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>User Yang diinvite</th>
                            <th>Undian</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach ($datas as $data)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->userDiInvite }}</td>
                                <td>{{ $data->undian }}</td>
                            </tr>
                            @php $i++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('jsPage')
<script type="text/javascript">
	$(function(){
	  $("#datepicker").datepicker({
		 dateFormat:"yy-mm-dd",
	  });
	});
</script>
@endsection
