@extends("crudbooster::admin_template")

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="{{ set_active('riwayatTicketGacha') }}"><a href="{{ route('riwayatTicketGacha') }}">Per transaction</a></li>
              <li class="{{ set_active(['riwayatTicketGachaUser', 'riwayatTicketGachaUserDetail']) }}"><a href="{{ route('riwayatTicketGachaUser') }} ">Per user</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Per transaction</h3>
                    </div>
                     <!--/.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <th>Jumlah Poin</th>
                                    <th>Waktu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data )
                                    
                                <tr>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->from }}</td>
                                    <td>{{ number_format($data->poin) }}</td>
                                    <td> {{ $data->created_at }}</td>
                                </tr>
            
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-center">
                                        {{ $datas->links() }}
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                        
                    </div>
                     <!--/.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                 <!--/.box -->
            </div>
        </div>
    </section>
@endsection

@section('jsPage')

@endsection