<?php

// use DB;

function set_active($uri, $output = "active")
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}

function fillter($string){
  $badwords = config('badword');
  $text = strtolower($string);
  foreach($badwords as $badword){
      $lowerBadword = strtolower($badword);
      $text = str_replace($lowerBadword, '****', $text);
  }
  // ... and so on
  return $text;
}

function poin(){
    $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
    return $poin;
}

function badgeUser($userId){
  $badges = DB::table('badge_users')
            ->join('badges', 'badges.id', 'badge_users.badge_id')
            ->where('badge_users.user_id', $userId)
            ->where('badge_users.status', 1)
            ->select('badges.photo', 'badge_users.on_hover')
            ->get();
  $arrayBadges = array();
  $string = "";
  foreach($badges as $key => $value){
    $string = $string." <img data-toggle='tooltip' style='margin-left : 3px; margin-top: -5px; margin-right: 5px; width: 20px' title='{$value->on_hover}' src='https://data.sobatbadak.club/{$value->photo}'>";
  }
  return $string;
}
