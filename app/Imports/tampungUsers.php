<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\DB;
use App\tampung_users_ngabuburit;

class tampungUsers implements ToModel
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
            return new tampung_users_ngabuburit([
                'id' => $row[1],
                'countRef'=> $row[2],
                'spam'=> $row[3],
                'spamRatio'=> $row[4],
                'emailTrue'=> $row[5],
                'emailFalse'=> $row[6],
                'rejekiNomplokPoin'=> $row[7],
                'name'=> $row[8],
                'email'=> $row[9],
                'emailValidation'=> $row[10],
                'password'=> $row[11],
                'whatsapp'=> $row[12],
                'subscribeEmail'=> $row[13],
                'subscribeWhatsapp'=> $row[14],
                'msgEmail'=> $row[15],
                'msgWhatsapp'=> $row[16],
                'kota'=> $row[17],
                'provinsi'=> $row[18],
                'area' => $row[19],
                'ref_id'=> $row[20],
                'ip_address'=> $row[21],
                'device'=> $row[22],
                'utm_campaign'=> $row[23],
                'utm_source'=> $row[24],
                'utm_medium'=> $row[25],
                'utm_content'=> $row[26],
                'alamat_rumah'=> $row[27],
                'alamat_detail'=> $row[28],
                'alamat_device'=> $row[29],
                'lat_rumah'=> $row[30],
                'lng_rumah'=> $row[31],
                'lat_device'=> $row[31],
                'lng_device'=> $row[32],
                'remember_token'=> $row[33],
                'email_verified_at'=> $row[34],
                'created_at'=> $row[35],
                'updated_at'=> $row[36]
            ]);
  
    }
}
