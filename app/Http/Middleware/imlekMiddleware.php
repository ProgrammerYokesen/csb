<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use Session;

class imlekMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user())
        {
          return redirect()->route('imlekLandingPage');
        }
        $imlek = DB::table('imlek_users')->where('user_id', Auth::id())->first();
        if($imlek){
          if($imlek->invoice != null && $imlek->pay == 1){
            return redirect()->route('imlekPaymentStatusPage');
          }
          return $next($request);
        }
        return $next($request);
    }
}
