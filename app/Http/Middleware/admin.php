<?php

namespace App\Http\Middleware;

use Closure;
use crocodicstudio\crudbooster\helpers\CRUDBooster as HelpersCRUDBooster;
use CRUDBooster;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(CRUDBooster::myId()){
            return $next($request);
        }
        return redirect()->route('defaultPage');
    }
}
