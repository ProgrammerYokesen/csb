<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class maintenance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $date = date('Y-m-d');
        // $jam = date('H');
        // if($date = '2021-08-30'){
        //     if($jam >= 0 && $jam < 5){
        //         return redirect()->route('maintenance');
        //     }
        // }
        return $next($request);
    }
}
