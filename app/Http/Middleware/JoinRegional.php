<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Auth;

class JoinRegional
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $checkJoin = DB::table('regional_users')->where('user_id', Auth::id())->first();
       if(!$checkJoin)
       {
         return redirect()->route('pilihRegional');
       }
       else{
         if($checkJoin->join_regional == 0)
         {
           return redirect()->route('daftarRegional');
         }
         else{
           return $next($request);
         }
       }

    }
}
