<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class lelang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->whatsapp_verification == 1 && Auth::user()->alamat_rumah != NULL && Auth::user()->ktp_verification == 1){
            return $next($request);
        }
        return redirect()->back();
    }
}
