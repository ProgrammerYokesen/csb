<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class saveUtm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('ref')){
            $request->session()->put('referral_code',$request->ref);
        }
        if($request->has('utm_medium')){
            $request->session()->put('utm_medium',$request->utm_medium);
        }
        if($request->has('utm_campaign')){
            $request->session()->put('utm_campaign',$request->utm_campaign);
        }
        if($request->has('utm_source')){
            $request->session()->put('utm_source',$request->utm_source);
        }
        if($request->has('utm_content')){
            $request->session()->put('utm_content',$request->utm_content);
        }
        return $next($request);
    }
}
