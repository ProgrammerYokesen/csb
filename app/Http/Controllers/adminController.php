<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class adminController extends Controller
{
    public function index(){
        $datas = DB::table('user_poins')->select('users.name', 'users.email', 'user_poins.created_at', 'user_poins.poin','user_poins.from')
                ->join('users', 'users.id', 'user_poins.user_id')
                ->orderByDesc('user_poins.id')
                ->where('user_poins.status', 1)
                ->paginate(50);
        return view('admin.riwayat-baper-poin', compact('datas'));
    }
    public function users(){
        $datas = DB::table('user_poins')->groupBy('user_poins.user_id')
        ->select(DB::raw('sum(user_poins.poin) as sum'), 'users.email', 'users.name', 'users.id')
        ->where('user_poins.status', 1)
        ->join('users', 'users.id', 'user_poins.user_id')
        ->get()->sortByDesc('sum');
        return view('admin.users-poin', compact('datas'));
    }
    public function userDetail($id){
        $datas = DB::table('user_poins')->join('users', 'users.id', 'user_poins.user_id')
                ->select('users.name', 'users.email', 'user_poins.created_at', 'user_poins.poin','user_poins.from')
                ->where('users.id', $id)
                ->where('user_poins.status', 1)
                ->orderByDesc('user_poins.id')
                ->paginate(50);
        return view('admin.users-poin-detail', compact('datas'));
    }
    public function indexTicketGacha(){
        $datas = DB::table('user_poins')->where('from', 'diy')->select('users.name', 'users.email', 'user_poins.created_at', 'user_poins.poin','user_poins.from')
                ->join('users', 'users.id', 'user_poins.user_id')
                ->join('')
                ->orderByDesc('user_poins.id')
                ->where('user_poins.status', 1)
                ->paginate(20);
        return view('admin.history-tickets-gacha', compact('datas'));

    }
    
    public function calculateBPTidakSesuai($id, $value){
        $getPoin = DB::table('koins')->where('id', $id)->first();
        $totalPoin = $getPoin->poin * $value;
        return response()->json($totalPoin);
    }
}
