<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Jenssegers\Agent\Agent;
use GuzzleHttp\Client;
use DB;
use Session;
use DateTime;
use Carbon\Carbon;

class ImlekController extends Controller
{
    public function landingPage()
    {
      // \Session::flush();
      Session::put('link', route('imlekOrderPage'));
      Session::put('daftar_imlek_2022', true);
      // dd(Session::all());
      return view('landing-page.event.imlek.landing-page');
    }

    public function register()
    {
      $agent = new Agent();
      $platform = $agent->platform();
      $version = $agent->version($platform);
      $browser = $agent->browser();
      $versionbrowser = $agent->version($browser);

      /**Register users */

      $userCount = DB::table('imlek_users')->where('user_id', Auth::id())->count();

      // dd(Auth::user());
      if($userCount == 0)
      {

        DB::table('imlek_users')->insert([
          'user_id' => Auth::id(),
          'created_at' => now(),
          'updated_at' => now(),
          'ip_address' => \Request::ip(),
          'device' => $agent->device(),
          'platform' => $agent->platform(),
          'version_platform' => $version,
          'browser'  => $agent->browser(),
          'version_browser' => $versionbrowser,
          'languages' => $agent->languages()
        ]);

        /** Get footprint **/

        $cookie = $_COOKIE['footprints'];

        if ($cookie != NULL) {
            // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
            // $decryptedString = $encrypter->decrypt($cookie);
            $decryptedString = \Crypt::decrypt($cookie, false);
            $cookies = explode("|", $decryptedString);
            // dd($decryptedString, $cookies);
            $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                'user_imlek' => Auth::id()
            ]);
        }
      }


      /**
      * Return redirect()->checkoutpage
      */

      return redirect()->route('imlekOrderPage');

    }

    public function createInvoice(Request $request){
      // dd($request);
      $client = new Client();
      $ovo_number = "";
      if($request->ovo_number != null){
        // dd('atas');
        preg_match_all('!\d+!', $request->ovo_number, $matches);
        foreach($matches as $key => $value){
          foreach($value as $v){
            $ovo_number = $ovo_number. "$v";
          }
        }
      }
      else{
        preg_match_all('!\d+!', $request->whatsapp, $matches);
        // dd($matches, $request->phone_number);
        foreach($matches as $key => $value){
          foreach($value as $v){
            $ovo_number = $ovo_number. "$v";
          }
        }
      }
      $requestBody = array(
        "first_name" =>  $request->first_name,
        "address" => $request->address,
        "subdistrict" => $request->subdistrict,
        "city" => $request->city,
        "province" => $request->province,
        "country" => "indonesia",
        "vendor_product_id" => $request->vendor_product_id,
        "payment_channel" => $request->payment_channel,
        "note" => $request->note,
        "phone_number" => $request->whatsapp,
        "ovo_number" => "62$ovo_number",
        "vendor_product_id" => $request->session()->get('vendor_product_id'),
        "email" => Auth::user()->email
      );
      // dd($requestBody);
      if($request->session()->has('vendor_product_id'))
      {
        $response = $client->request('POST', 'https://api.warisan.co.id/api/csb/order', [
          \GuzzleHttp\RequestOptions::JSON => $requestBody
        ]);

        $contents = json_decode($response->getBody());
        // dd($contents);
        DB::table('imlek_users')->where('user_id', Auth::id())->update([
          'invoice' => $contents->orders->invoice,
          'pay' => 1
        ]);

        /**Return Redirect payment page */

        return redirect()->route('imlekPaymentStatusPage');
      }
      else {
        return redirect()->route('imlekLandingPage');
      }

    }

    public function callbackPayment(Request $request){

      DB::table('imlek_users')->where('invoice', $request->invoice)->update([
        'status' => 'join',
        'updated_at' => now()
      ]);

    }

    public function orderPage(){

    //   dd(Session::get('user'), Session::get('user')->id, Session::all());
      $client = new Client();

      $invoice = DB::table('imlek_users')->where('user_id', Auth::id())->first();

      $product = $client->request('GET', 'https://api.warisan.co.id/api/csb/product');
      $products = json_decode($product->getBody());
      $products = $products->data;

      /**Sudah create Invoice sebelumnya */
      if($invoice){
        $response = $client->request('GET', 'https://api.warisan.co.id/api/csb/order?invoice='.$invoice->invoice);

        $contents = json_decode($response->getBody());

        /**
        *Cek invoice sudah kadaluarsa atau belum
        */

        /**Sudah kadaluarsa */
        if($contents->payment_status == 2)
        {
          /**Return to order page */

        }

        /** Belum kadaluarsa*/
        elseif($contents->payment_status == 0) {
          /**Return to payment page */

        }
        /** Sudah Bayar*/
        elseif($contents->payment_status == 1){
          DB::table('imlek_users')->where('user_id', Auth::id())->update([
            'status' => 'join',
            'updated_at' => now()
          ]);
        }

      }
      /** Belum create invoice sama sekali*/
      else{

      }

      // dd($products);

      return view('landing-page.event.imlek.order', compact('products'));

    }

    public function paymentPage(Request $request)
    {
      $client = new Client();
      $product = $client->request('GET', 'https://api.warisan.co.id/api/csb/product');
      $products = json_decode($product->getBody());
      $products = $products->data;

      $product = collect($products);
      $product = $product->where('id', $request->session()->get('vendor_product_id'))->first();
      // dd($product, $request->session()->get('vendor_product_id'));
      return view('landing-page.event.imlek.payment', compact('product'));
    }

    public function addToCart(Request $request){
      // dd($request);
      if($request->session()->has('vendor_product_id')){
  			$request->session()->forget('vendor_product_id');
  		}
      $request->session()->put('vendor_product_id', $request->vendor_product_id);

      /**
      *Return redirect payment page
      */

      return redirect()->route('imlekPaymentPage');

    }

    public function paymentStatus()
    {
      // return view('landing-page.event.imlek.payment-status');
      $client = new Client();

      $imlek = DB::table('imlek_users')->where('user_id', Auth::id())->first();

      if(!$imlek){
        return redirect()->route('imlekLandingPage');
      }
      else{
        if($imlek->invoice == null){
          return redirect()->route('imlekOrderPage');
        }
      }

      $order = $client->request('GET', 'https://api.warisan.co.id/api/csb/order?invoice='.$imlek->invoice);
      $order = json_decode($order->getBody());
      $order = $order->order;
      // dd($order);
      return view('landing-page.event.imlek.payment-status', compact('order'));
    }

    public function resetInvoice()
    {
      DB::table('imlek_users')->where('user_id', Auth::id())->update([
        'pay' => 0,
        'created_at' => now()
      ]);
      return redirect()->route('imlekOrderPage');
    }

    public function imlekQuiz()
    {
      $paidStatus = DB::table('imlek_users')
                        ->where('user_id', Auth::id())
                        ->where('status', 'join')
                        ->count();
      $zoomStatus = DB::table('imlek_users')
                        ->where('user_id', Auth::id())
                        ->where('join_zoom_workshop', 1)
                        ->count();

      date_default_timezone_set('Asia/Jakarta');
      $now = new DateTime('now');
      $origin = new DateTime('2022-01-23T14:30:00');

      if ($now > $origin) {
          return redirect()->route('quizShowImlekDetail');
      }

      return view('dashboard.pages.quiz-show-imlek.special-imlek', compact('paidStatus', 'zoomStatus'));
    }

    public function recreateInvoice()
    {
      $client = new Client();
      $imlek = DB::table('imlek_users')->where('user_id', Auth::id())->first();
      $order = $client->request('GET', 'https://api.warisan.co.id/api/csb/order?invoice='.$imlek->invoice);
      $order = json_decode($order->getBody());
      $order = $order->order;

      $requestBody = array(
        "first_name" =>  $order->first_name,
        "address" => $order->address,
        "subdistrict" => $order->subdistrict,
        "city" => $order->city,
        "province" => $order->province,
        "country" => "indonesia",
        "vendor_product_id" => $order->vendor_product_id,
        "payment_channel" => $order->payment_channel,
        "note" => $order->note,
        "phone_number" => $order->phone_number,
        "ovo_number" => $order->phone_number,
        "email" => $order->email
      );

      $response = $client->request('POST', 'https://api.warisan.co.id/api/csb/order', [
        \GuzzleHttp\RequestOptions::JSON => $requestBody
      ]);

      $contents = json_decode($response->getBody());
      // dd($contents);
      DB::table('imlek_users')->where('user_id', Auth::id())->update([
        'invoice' => $contents->orders->invoice,
        'pay' => 1
      ]);

      /**Return Redirect payment page */

      return redirect()->route('imlekPaymentStatusPage');

    }

    public function updateStatus(Request $request)
    {
      if($request->token == 'pzajkxypynaufruwqpnzqbdsbjeeszcx')
      {
        DB::table('imlek_users')->where('invoice', $request->invoice)->update([
          'status' => 'join',
          'updated_at' => now()
        ]);
        return response()->json([
          'status' => 'success',
          'message' => 'berhasil melakukan pembayaran'
        ]);
      }
    }

    public function quizShowImlek()
    {
      return view('dashboard.pages.quiz-show-imlek.quiz-show');
    }

    public function quizShowImlekDetail()
    {
      $date = date('Y-m-d H:i:s');
      $quiz = DB::table('imlek_events')
      ->whereDate('imlek_events.start_date', '=', Carbon::today())
      // ->where('imlek_events.start_date', '<', $date)
      // ->where('imlek_events.end_date', '>', $date)
      ->first();
      $startD = new \DateTime($quiz->start_date);
      $endD = new \DateTime($quiz->end_date);
      $startD = $startD->format(\DateTime::ATOM);
      $endD = $endD->format(\DateTime::ATOM);
      $totalUser = DB::table('imlek_event_users')->where('imlek_event_id', $quiz->id)->count();

      $startDate = explode('+',$startD)[0];
      $endDate = explode('+',$endD)[0];

      $winner = DB::table('imlek_event_users')->where('user_id', Auth::id())->where('status', 'win')->count();

      return view('dashboard.pages.quiz-show-imlek.quiz-show-detail', compact('startDate', 'endDate', 'totalUser', 'winner', 'quiz'));
    }

    public function listPesertaImlek($id)
    {
      $date = date('Y-m-d H:i:s');

      $data = DB::table('imlek_event_users')
              ->join('users', 'users.id', 'imlek_event_users.user_id')
              ->where('imlek_event_users.imlek_event_id', $id)
              ->groupBy('users.id')
              ->where('imlek_event_users.status', '!=', 'failed')
              ->select('users.name', 'imlek_event_users.status', 'imlek_event_users.prize')
              ->paginate(15);

      $events = DB::table('imlek_events')
                ->where('start_date', '<', $date)
                ->get();

      $currentEvent = DB::table('imlek_events')
                      ->where('id', $id)
                      ->first();


      return view('dashboard.pages.quiz-show-imlek.list-peserta-imlek', compact('data', 'events', 'currentEvent'));
    }

    public function overlayImlek()
    {
      return view('dashboard.pages.quiz-show-imlek.overlay');
    }

    public function registerQuizShow()
    {

      /**
      *Check user already paid imlek event
      */
      $checkPaid = DB::table('imlek_users')->where('user_id', Auth::id())->first();

      if(!$checkPaid)
      {
        // dd('002');
        return \Redirect::back()->with([
          'status' => 'failed',
          'message' => 'belum register event imlek',
          'response_code' => '002'
        ]);
      }
      else{

        /** Check if user already paid*/

        if($checkPaid->status != 'join')
        {
          /**User belum bayar */
          // dd('003');
          return \Redirect::back()->with([
            'status' => 'failed',
            'message' => 'belum melakukan pembayaran',
            'response_code' => '003'
          ]);

        }

        if($checkPaid->join_zoom_workshop == 0)
        {
          return \Redirect::back()->with([
            'status' => 'failed',
            'message' => 'Tidak ikut workshop',
            'response_code' => '008'
          ]);
        }


        /** Process Register*/

        $date = date('Y-m-d H:i:s');

        /** Check active quiz*/
        $quiz =DB::table('imlek_events')
        ->where('imlek_events.start_date', '<', $date)
        ->where('imlek_events.end_date', '>', $date)
        ->first();

        /** Tidak ada quiz yang active*/

        if(!$quiz)
        {
          // dd('004');
          return \Redirect::back()->with([
            'status' => 'failed',
            'message' => 'Tidak ada quiz active',
            'response_code' => '004'
          ]);

        }

        /** Ada quiz active*/

        else {

          /** Check if user already won*/

          $win = DB::table('imlek_event_users')->where('user_id', Auth::id())->where('status', 'win')->count();

          if($win != 0)
          {
            return \Redirect::back()->with([
              'status' => 'failed',
              'message' => 'Sudah menang sebelumnya',
              'response_code' => '007'
            ]);
          }

          /** Check sudah daftar*/
          $checkDaftar = DB::table('imlek_event_users')->where('imlek_event_id', $quiz->id)->where('user_id', Auth::id())->count();

          if($checkDaftar != 0)
          {
            // dd('005');
            return \Redirect::back()->with([
              'status' => 'failed',
              'message' => 'Sudah mendaftar sebelumnya',
              'response_code' => '005'
            ]);
          }

          $count = DB::table('imlek_event_users')->where('imlek_event_id', $quiz->id)->count();
          if($count < 25)
          {
            /** Process Insert*/
            DB::table('imlek_event_users')->insert([
              'imlek_event_id' => $quiz->id,
              'user_id' => Auth::id(),
              'status' => 'new',
              'created_at' => now(),
              'updated_at' => now()
            ]);

            /** Proses cek 60 pertama*/
            // dd('001');
            return \Redirect::back()->with([
              'status' => 'success',
              'message' => 'Berhasil daftar',
              'response_code' => '001'
            ]);
          }
          else {
            // dd('006');
            return \Redirect::back()->with([
              'status' => 'failed',
              'message' => 'Kuota game sudah penuh',
              'response_code' => '006'
            ]);

          }
        }

      }

    }

    public function apiString()
    {
      /** Check active quiz*/
      $date = date('Y-m-d H:i:s');
      $quiz =DB::table('imlek_events')
      ->where('imlek_events.start_date', '<', $date)
      ->where('imlek_events.end_date', '>', $date)
      ->first();

      $users = DB::table('imlek_event_users')
                ->where('imlek_event_id', $quiz->id)
                ->join('users', 'users.id', 'imlek_event_users.user_id')
                ->orderBy('imlek_event_users.id', 'asc')
                ->limit(25)
                ->select('users.name', 'imlek_event_users.status', 'imlek_event_users.prize')
                ->get();
      return response()->json([
        'data' => $users
      ]);

    }

    public function joinZoom(){

      DB::table('imlek_users')->where('user_id', Auth::id())->update([
        'join_zoom_workshop' => 1,
        'updated_at' => now()
      ]);
      $linkZoom = 'https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09';
      return redirect($linkZoom);

    }

    public function getNomerUrut()
    {
      /** Check active quiz*/
      $date = date('Y-m-d H:i:s');
      $quiz =DB::table('imlek_events')
      ->where('imlek_events.start_date', '<', $date)
      ->where('imlek_events.end_date', '>', $date)
      ->first();
      if($quiz)
      {
        $data = DB::table('imlek_event_users')
        ->where('imlek_event_id', $quiz->id)
        ->get();
        $user = $data->where('user_id', Auth::id());
        foreach($user as $key => $value)
        {
          $index = $key;
        }
        $indek++;
      }
      else {
        {
          $indek = null;
        }
      }
      return response()->json([
        'noUrut' => $indek
      ]);
    }

}
