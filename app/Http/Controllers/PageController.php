<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Jenssegers\Agent\Agent;

class PageController extends Controller
{
    public function homePage()
    {
        return view('landing-page.master');
    }

    public function landingPage()
    {
        // Get Total User
        $users = DB::table('users')->count();
        return view('landing-page.pages.home', compact('users'));
    }

    public function loginPage(Request $request)
    {
        if ($request->has('APP')) {
            $request->session()->put('link', $request->link);
        } else {
            Session::forget('link');
        }
        return view('landing-page.pages.login');
    }

    public function forgotPage()
    {
        return view('landing-page.pages.forgot');
    }

    public function registerPage(Request $request)
    {
        /**Put referral code into session, if url has referral_code */
        if ($request->has('ref')) {
            $request->session()->put('referral_code', $request->ref);
        }
        if ($request->has('utm_medium')) {
            $request->session()->put('utm_medium', $request->utm_medium);
        }
        if ($request->has('utm_campaign')) {
            $request->session()->put('utm_campaign', $request->utm_campaign);
        }
        if ($request->has('utm_source')) {
            $request->session()->put('utm_source', $request->utm_source);
        }
        if ($request->has('utm_content')) {
            $request->session()->put('utm_content', $request->utm_content);
        }
        return view('landing-page.pages.register');
    }

    public function livestreamPage()
    {
        return view('livestream');
    }

    public function omaru()
    {
        return view('landing-page.pages.omaru');
    }

    public function auction(Request $request)
    {
        $date = date('Y-m-d H:i:s');
        $riwayatLelang = DB::table('auctions')
            ->select('auctions.id', 'auctions.name', 'auctions.photo', 'auctions.starttime', 'users.name as username', 'auction_bids.bid_price as bidTertinggi')
            ->leftJoin('auction_bids', 'auction_bids.auction_id', 'auctions.id')
            ->join('users', 'users.id', 'auction_bids.user_id')
            ->groupBy('auctions.id')
            ->where('auction_bids.winner_status', 1)
            ->where('auctions.end_date', '<', $date)
            ->orderByDesc('auctions.id')
            ->get();
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($riwayatLelang);
        $perPage = 3;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $riwayatLelang = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        if ($request->ajax()) {
            $view = view('dashboard.pages.showmore.riwayatauction', compact('riwayatLelang'))->render();
            return response()->json(['html' => $view]);
        }
        return view('landing-page.pages.lelang', compact('riwayatLelang'));
    }

    public function auctionDetail()
    {
        return view('landing-page.pages.lelang-detail');
    }

    public function resetPass(Request $request)
    {
        if ($request->has('token')) {
            $request->session()->put('token', $request->token);
        }
        return view('landing-page.pages.reset-pass');
    }

    public function uniTheater()
    {
        return view('landing-page.universe.theater');
    }

    public function uniLapak()
    {
        return view('landing-page.universe.lapak');
    }

    public function uniAuction()
    {
        return view('landing-page.universe.auction-house');
    }

    public function uniGamezone()
    {
        return view('landing-page.universe.gamezone');
    }

    public function uniTownhall()
    {
        return view('landing-page.universe.townhall');
    }

    public function uniBaba()
    {
        return view('landing-page.universe.baba');
    }

    public function forgotDone()
    {
        return view('landing-page.pages.forgot-done');
    }

    public function emailVerifDone()
    {
        return view('landing-page.pages.email-verif-done');
    }

    public function homeBooklet()
    {
        $agent = new Agent();
        if ($agent->isDesktop()) {
            return view('landing-page.v2.desktop');
        }
        return view('landing-page.v2.mobile');
    }

    public function webinarPage()
    {
        return view('landing-page.pages.webinar');
    }

    public function tombol()
    {
        $data = [
            'email' => 'tama.cembuh.copid@eaaa.com'
        ];

        return view('zoom.tombol', compact('data'));
    }

    public function meeting()
    {
        return view('zoom.meeting');
    }

    public function faqPage()
    {
        return view('landing-page.pages.faq');
    }

    public function event99()
    {
        $products = DB::table('sembako')->where('status', 1)->get();
        return view('landing-page.event.99', compact('products'));
    }

    public function eventMakeup()
    {
        return view('landing-page.event.makeup');
    }

    public function event1010()
    {
        return view('landing-page.event.1010');
    }

    public function disclaimerPage()
    {
        return view('landing-page.pages.disclaimer');
    }

    public function event11()
    {
      $provinces = DB::table('provinces')->select('id', 'name')->get();
      return view('landing-page.event.1111', compact('provinces'));
    }
    public function pilihBenar11()
    {
      $provinces = DB::table('provinces')->select('id', 'name')->get();
      return view('landing-page.event.pilihyangbenar', compact('provinces'));
    }
    public function harbolnas11()
    {
      $provinces = DB::table('provinces')->select('id', 'name')->get();
      return view('landing-page.event.harbolnas11', compact('provinces'));
    }
    public function hariAnak()
    {
      $provinces = DB::table('provinces')->select('id', 'name')->get();
      return view('landing-page.event.hari-anak',compact('provinces'));
    }
    public function harbolnas1212()
    {
      Session::put('harbolnas_1212', true);
      Session::put('link', route('harbolnas1212form'));
      return view('landing-page.event.1212');
    }
    public function harbolnas1212form()
    {
      $regencies = DB::table('regencies')->select('id', 'name')->get();
      return view('landing-page.event.1212-form', compact('regencies'));
    }
    public function harbolnas1212v2()
    {
      Session::put('harbolnas_1212', true);
      Session::put('link', route('harbolnas1212form'));
      return view('landing-page.event.1212-v2');
    }
}
