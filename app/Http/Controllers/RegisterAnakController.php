<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use DB;

class RegisterAnakController extends Controller
{
    public function post(Request $request)
    {
        date_default_timezone_set("Asia/Bangkok");
        $agent = new Agent();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'age' => 'required|numeric',
            'gender' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => $validator->errors()
            ], 400);
        }

        if ($agent->isPhone()) {
            $screen = "Phone";
        } elseif ($agent->isTablet()) {
            $screen = "Tablet";
        } elseif ($agent->isDesktop()) {
            $screen = "Desktop";
        }

        $uuid = $this->createUuid();

        $userId = DB::table('users_anak')->insertGetId([
            'uuid' => $uuid,
            'name' => $request->name,
            'age' => $request->age,
            'address' => $request->address,
            'gender' => $request->gender,
            'status' => 'new',
            'province' => $request->province,
            'lomba' => $request->lomba,
            'ip_address' => \Request::ip(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'screen' => $screen,
            'version_platform' => $agent->version($agent->platform()),
            'browser' => $agent->browser(),
            'version_browser' =>  $agent->version($agent->browser()),
            'languages' => $agent->languages()[2],
            'created_at' => now(),
        ]);

        $cookie = $_COOKIE['footprints'];
        if($cookie != NULL){
            // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
            // $decryptedString = $encrypter->decrypt($cookie);
            $decryptedString = \Crypt::decrypt($cookie, false);
            $cookies = explode("|",$decryptedString);

            $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                'user_anak_id' => $userId
            ]);
        }

        return response()->json([
            'status' => 'success',
            'uuid' => $uuid,
            'message' => 'Berhasil registrasi'
        ]);
    }

    public function createUuid()
    {
        $uuid = Str::random(10);
        $check = DB::table('users_anak')->where('uuid', $uuid)->count();

        if ($check > 0) {
            $this->createUuid();
        }

        return $uuid;
    }
}
