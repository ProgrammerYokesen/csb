<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;
use Session;

class RegionalController extends Controller
{
    public function pilihRegional()
    {
      /** If user already set regional*/
      $check = DB::table('regional_users')->where('user_id', Auth::id())->first();

      if($check){
        return redirect()->route('daftarRegional');
      }
      /** End*/

      $user =DB::table('users')
              ->where('users.id', Auth::id())
              ->leftJoin('provinces_ro', 'provinces_ro.id', 'users.province_id')
              ->leftJoin('cities_ro', 'cities_ro.id', 'users.city_id')
              ->leftJoin('subdistricts_ro', 'subdistricts_ro.id', 'users.subdistrict_id')
              ->select('users.id', 'users.name', 'users.email', 'users.whatsapp', 'users.province_id', 'users.city_id', 'users.subdistrict_id', 'provinces_ro.province_name', 'cities_ro.city_name', 'subdistricts_ro.subdistrict_name')
              ->first();
      $province = DB::table('provinces_ro')->get();
      // dd($user, $province);
      return view('dashboard.pages.regional.pendaftaran.pilih-regional', compact('user', 'province'));
    }

    public function daftarRegional()
    {
      /** Belum isi regional*/
      $check = DB::table('regional_users')->where('user_id', Auth::id())->first();

      if(!$check){
        return redirect()->route('pilihRegional');
      }

      /** Sudah Join*/

      if($check->join_regional == 1)
      {
        return redirect()->route('tebakKataRegional');
      }

      /** Belum Join*/

      $regional = DB::table('regional_users')
                  ->leftJoin('provinces_ro', 'provinces_ro.id', 'regional_users.province_id')
                  ->leftJoin('cities_ro', 'cities_ro.id', 'regional_users.city_id')
                  ->leftJoin('subdistricts_ro', 'subdistricts_ro.id', 'regional_users.subdistrict_id')
                  ->where('user_id', Auth::id())
                  ->first();
      $regionalUsers = DB::table('regional_users')
                      ->where('regional_users.city_id', $regional->city_id)
                      ->join('users', 'users.id', 'regional_users.user_id')
                      ->where('users.id', '!=', Auth::id())
                      ->select('users.name', 'regional_users.join_regional')
                      ->orderByDesc('regional_users.id')
                      ->take(12)
                      ->get();
      $countUser = DB::table('regional_users')
                      ->where('regional_users.city_id', $regional->city_id)
                      ->join('users', 'users.id', 'regional_users.user_id')
                      ->where('users.id', '!=', Auth::id())
                      ->where('regional_users.join_regional', 1)
                      ->select('users.name', 'regional_users.join_regional')
                      ->orderByDesc('regional_users.id')
                      ->count();
                      // ->take(12);
      // dd($regional, $regionalUsers);
      return view('dashboard.pages.regional.pendaftaran.daftar-regional', compact('regional', 'regionalUsers', 'countUser'));
    }

    public function tebakKataRegional()
    {
      /** Belum isi regional*/
      $checkRegis = DB::table('regional_users')->where('user_id', Auth::id())->first();

      if(!$checkRegis){
        return redirect()->route('pilihRegional');
      }

      /** Sudah Join*/

      if($checkRegis->join_regional == 0)
      {
        return redirect()->route('daftarRegional');
      }

      $totalPoin = 0;
      $date = date('Y-m-d H:i:s');
      $quiz = DB::table('regional_tk_event')
          ->where('regional_tk_event.status', 1)
          ->where('regional_tk_event.city_id', $checkRegis->city_id)
          ->where('regional_tk_event.start_date', '<=', $date)
          ->where('regional_tk_event.end_date', '>=', $date)
          ->count();
      // dd($quiz);
      if ($quiz > 0) {
          $totalPoin = DB::table('regional_tk_event')
              ->leftJoin('regional_tk_event_lists', 'regional_tk_event_lists.regional_tk_event_id', 'regional_tk_event.id')
              ->leftJoin('regional_tebak_kata', 'regional_tebak_kata.id', 'regional_tk_event_lists.pertanyaan_id')
              ->where('regional_tk_event.status', 1)
              ->where('regional_tk_event.city_id', $checkRegis->city_id)
              ->where('regional_tk_event.start_date', '<', $date)
              ->where('regional_tk_event.end_date', '>', $date)
              ->sum('poin');
      }
      $datas = DB::table('regional_tk_event')
          ->select('regional_tk_event.id as id', 'regional_tk_event_lists.pertanyaan_id', 'regional_tebak_kata.kata', 'regional_tebak_kata.pertanyaan as deskripsi', 'regional_tebak_kata.kata')
          ->leftJoin('regional_tk_event_lists', 'regional_tk_event_lists.regional_tk_event_id', 'regional_tk_event.id')
          ->leftJoin('regional_tebak_kata', 'regional_tebak_kata.id', 'regional_tk_event_lists.pertanyaan_id')
          ->where('regional_tk_event.status', 1)
          ->where('regional_tk_event.start_date', '<', $date)
          ->where('regional_tk_event.end_date', '>', $date)
          ->paginate(1);
      /**Check pengerjaan selesai */
      $check = DB::table('jawaban_tebak_kata')->where('status_pengerjaan', 1)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->count();

      if ($check != 0) {
          $doneQuiz = true;
      }
      /**End check */


      return view('dashboard.pages.regional.tebak-kata.tebak-kata', compact('quiz', 'totalPoin', 'doneQuiz'));
    }

    public function inputTebakKataRegional()
    {
      /** Belum isi regional*/
      $checkRegis = DB::table('regional_users')->where('user_id', Auth::id())->first();

      if(!$checkRegis){
        return redirect()->route('pilihRegional');
      }

      /** Sudah Join*/

      if($checkRegis->join_regional == 0)
      {
        return redirect()->route('daftarRegional');
      }

      $check = DB::table('regional_tebak_kata')
              ->whereDate('created_at', Carbon::today())
              ->where('user_id', Auth::id())->count();
      $qty = 5 - $check;
      $lists = DB::table('regional_tebak_kata')
          ->where('user_id', Auth::id())
          ->whereDate('created_at', Carbon::today())->get();
      return view('dashboard.pages.regional.tebak-kata.tebak-kata-input', compact('qty', 'lists'));
    }

    public function getCity(Request $request)
    {
      $query = DB::table('cities_ro');
      if($request->has('province_id'))
      {
        $data = $query->where('province_id', $request->province_id)->get();
      }
      else{
        $data = $query->get();
      }

      return response()->json([
        'status' => 'success',
        'cities' => $data
      ]);
    }

    public function getSubdistricts(Request $request)
    {
      $query = DB::table('subdistricts_ro');
      if($request->has('city_id'))
      {
        $data = $query->where('city_id', $request->city_id)->get();
      }
      else{
        $data = $query->get();
      }

      return response()->json([
        'status' => 'success',
        'subdistricts' => $data
      ]);
    }

    public function saveCity(Request $request)
    {

      $validator = \Validator::make($request->all(), [
          'province_id' => 'required',
          'city_id' => 'required',
          'subdistrict_id' => 'required'
      ]);

      if ($validator->fails()) {
          return \Redirect::back()->withErrors($validator);
      }

      $checkRegional = DB::table('regional_users')->where('user_id', Auth::id())->first();

      if(!$checkRegional)
      {

        DB::table('users')->where('id', Auth::id())->update([
          'province_id' => $request->province_id,
          'city_id' => $request->city_id,
          'subdistrict_id' => $request->subdistrict_id,
          'updated_at' => now()
        ]);

        DB::table('regional_users')->updateOrInsert([
          'user_id' => Auth::id()
        ],[
          'province_id' => $request->province_id,
          'city_id' => $request->city_id,
          'subdistrict_id' => $request->subdistrict_id,
          'created_at' => now(),
          'updated_at' => now()
        ]);

      }


      return redirect()->route('daftarRegional')->with('success', true);
    }

    public function joinRegional()
    {
      // dd('s');
      $checkData = DB::table('regional_users')->where('user_id', Auth::id())->first();
      if($checkData)
      {
        if($checkData->join_regional == 0)
        {
          $countUser = DB::table('regional_users')->where('city_id', $checkData->city_id)->count();

          DB::table('cities_ro')->where('id', $checkData->city_id)->update([
            'total_user' => $countUser
          ]);

          DB::table('regional_users')->where('user_id', Auth::id())->update([
            'join_regional' => 1,
            'updated_at' => now()
          ]);

          DB::table('user_poins')->insert([
              'user_id' => Auth::id(),
              'poin' => 100000,
              'fk_id' => null,
              'from' => 'Join Regional',
              'created_at' => date('Y-m-d H:i:s'),
              'updated_at' => date('Y-m-d H:i:s')
          ]);

        }

        return redirect()->route('listUserReg')->with('success', true);

      }
      else
      {
        return redirect()->route('pilihRegional');
      }
    }

    public function submitTebakKataRegional(Request $request){
        $check = DB::table('regional_tebak_kata')
                ->whereDate('created_at', Carbon::today())
                ->where('user_id', Auth::id())->count();
        $region = DB::table('regional_users')
                ->where('user_id', Auth::id())->first();
        $jawaban = strtoupper($request->kata);
        if($check < 5){
            DB::table('regional_tebak_kata')->insert([
                'kata' => $jawaban,
                'pertanyaan' => $request->pertanyaan,
                'poin' => 1000,
                'user_id' => Auth::id(),
                'city_id' => $region->city_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return redirect()->route('inputTebakKataRegional')->with('success', true);
        }
        return redirect()->back()->with('failed', true);
    }

    public function riwayatSoal(Request $request)
    {
        if ($request->has('date')) {
            $lists = DB::table('regional_tebak_kata')
                ->where('user_id', Auth::id())
                ->whereDate('created_at', $request->date)->get();
            return response()->json(compact('lists'));
        }
    }

    public function jawabTebakKataRegional(Request $request)
    {
      $date = date('Y-m-d H:i:s');
      $regionalUser = DB::table('regional_users')->where('user_id', Auth::id())->first();
      $datas = DB::table('regional_tk_event')
          ->select('regional_tk_event.id as id', 'regional_tk_event_lists.pertanyaan_id', 'regional_tebak_kata.kata', 'regional_tebak_kata.pertanyaan', 'regional_tebak_kata.kata')
          ->leftJoin('regional_tk_event_lists', 'regional_tk_event_lists.regional_tk_event_id', 'regional_tk_event.id')
          ->leftJoin('regional_tebak_kata', 'regional_tebak_kata.id', 'regional_tk_event_lists.pertanyaan_id')
          ->where('regional_tk_event.status', 1)
          ->where('regional_tk_event.city_id', $regionalUser->city_id)
          ->where('regional_tk_event.start_date', '<', $date)
          ->where('regional_tk_event.end_date', '>', $date)
          ->paginate(1);
      // dd($datas, $datas[0]->id);
      /**Check pengerjaan selesai */
      $check = DB::table('regional_tebak_kata_pengerjaan_answers')->where('status_pengerjaan', 1)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->count();
      // dd($check);
      if ($check != 0) {
          $request->session()->put('quizId_regional', $datas[0]->id);
          return redirect()->route('winTebakKataRegional');
      }
      /**End check */
      /**Check pertanyaan sudah dijawab */
      $answered = DB::table('regional_tebak_kata_pengerjaan_answers')->where('status_pengerjaan', 0)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->where('pertanyaan_id', $datas[0]->pertanyaan_id)->first();
      if ($answered) {
          $page = (int)$request->page + 1;
          return redirect()->route('jawabTebakKataRegional', ["page" => $page]);
      }
      /**End check */
      /**Check page lebih dari totalpage */
      $answered = DB::table('regional_tebak_kata_pengerjaan_answers')->where('status_pengerjaan', 0)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->count();
      $page = $answered + 1;
      if ($request->page > $datas->lastPage() || $request->page > $page) {
        return redirect()->route('jawabTebakKataRegional', ["page" => $page]);
      }
      /**End */
      $percentage = ($datas->currentPage() / $datas->lastPage()) * 100;
      $kata = str_split($datas[0]->kata);
      $request->session()->put('quizId_regional', $datas[0]->id);
      $request->session()->put('pertanyaanId_regional', $datas[0]->pertanyaan_id);
      $request->session()->put('currentPage_regional', $datas->currentPage());
      $request->session()->put('lastPage_regional', $datas->lastPage());
      if (empty(Session::get('tebak_regional'))) {
          $huruf = $datas[0]->kata;
          $length = strlen($huruf) - 1;
          $p = random_int(0, (int)$length);
          $huruf = str_split($huruf);
          $huruf = strtoupper($huruf[$p]);
          $tebak[0] = $huruf;
          $last = "betul";
          $nilai = 0;
          $salah = 0;
          foreach ($tebak as $value) {
              if (in_array($value, $kata)) {
                  $nilai += 1;
              } else {
                  $salah += 1;
              }
          }

          $percent_nilai = $nilai / count($kata);
          $percent_salah = ($nilai - $salah) / count($kata);
          // dd($percent_nilai);


          $request->session()->put('last_regional', $last);
          $request->session()->put('salah_regional', $salah);
          $request->session()->put('nilai_regional', $percent_nilai);
          $request->session()->put('nilai_salah_regional', $percent_salah);
          $request->session()->put('tebak_regional', $tebak);
      }
      $tebakan = Session::get('tebak_regional');

      return view('dashboard.pages.regional.tebak-kata.tebak-kata-game', compact('datas', 'percentage', 'kata', 'tebakan'));

    }

    public function satu_submit(Request $request, $id){
          // $kata = ['T','E','R','B','A','I','K'];
          $word = DB::table('regional_tebak_kata')->where('id', $id)->first();
          $kata_split = str_split($word->kata);
          $kata = array_unique($kata_split);


          $huruf = strtoupper($request->huruf);
          // dd($huruf);

          if(empty(Session::get('tebak_regional'))){
              $tebak[0] = $huruf;
              if(in_array($huruf, $kata)){
                  $last = "betul";
              }else{
                  $last = "salah";
              }
          }else{
              $tebak = Session::get('tebak_regional');
              if (in_array($huruf, $tebak)) {
                  $last = Session::get('last_regional');
              }else{
                  array_push($tebak,$huruf);
                  if(in_array($huruf, $kata)){
                  $last = "betul";
              }else{
                  $last = "salah";
              }

              }
          }

          $nilai = 0;
          $salah = 0;
          foreach ($tebak as $value) {
              if(in_array($value, $kata)){
                  $nilai += 1;
              }else{
                  $salah += 1;
              }
          }

          $percent_nilai = $nilai / count($kata);
          $percent_salah = ($nilai - $salah) / count($kata);
          // dd($percent_nilai);


          $request->session()->put('last_regional', $last);
          $request->session()->put('salah_regional', $salah);
          $request->session()->put('nilai_regional', $percent_nilai);
          $request->session()->put('nilai_salah_regional', $percent_salah);
          $request->session()->put('tebak_regional', $tebak);
          $page = (int)Session::get('currentPage_regional');

          return redirect()->route('jawabTebakKataRegional', ["page" => $page]);
      }

      public function satu_reset($id){
        $word = DB::table('regional_tebak_kata')->where('id', $id)->first();
        if(Session::get('nilai_regional') == 1 || Session::get('salah_regional') == 3){
            if(Session::get('nilai_regional') == 1){
                $statusJawaban = 1;
                $answer = $word->kata;
            }
            elseif(Session::get('salah_regional') == 3){
                $statusJawaban = 0;
                $answer = implode(Session::get('tebak_regional'));
            }
            DB::table('regional_tebak_kata_pengerjaan_answers')->insert([
                'user_id' => Auth::id(),
                'quiz_tebak_kata_id' => Session::get('quizId_regional'),
                'jawaban_user' => $answer,
                'status_jawaban' => $statusJawaban,
                'pertanyaan_id' => Session::get('pertanyaanId_regional'),
                'poin' => $word->poin,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $page =(int)Session::get('currentPage_regional') +1;
            if(Session::get('currentPage_regional') == Session::get('lastPage_regional')){
              $quizId = Session::get('quizId_regional');
              $totalPoin = DB::table('regional_tebak_kata_pengerjaan_answers')->where('user_id', Auth::id())->where('status_jawaban', 1)->where('quiz_tebak_kata_id', $quizId)->sum('poin');
              DB::table('user_poins')->insert([
                  'user_id' => Auth::id(),
                  'poin' => $totalPoin,
                  'fk_id' => $quizId,
                  'from' => 'Quiz Tebak Kata Regional',
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s')
              ]);
              //Rubah semua status jadi true disini
              DB::table('regional_tebak_kata_pengerjaan_answers')->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $quizId)->update([
                  'status_pengerjaan' => 1
              ]);
              //End
              Session::forget([
                'currentPage_regional', 'last_regional', 'salah_regional', 'nilai_regional', 'nilai_salah_regional', 'tebak_regional', 'lastPage_regional'
              ]);

              /**Update total baper poin */

              $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
              $newPoint = $baperPoin + $totalPoin;
              DB::table('users')->where('id', Auth::id())->update([
                    'baper_poin' => $newPoint
                ]);
              return redirect()->route('winTebakKataRegional');
            }
            Session::forget([
                'currentPage_regional', 'last_regional', 'salah_regional', 'nilai_regional', 'nilai_salah_regional', 'tebak_regional'
              ]);
            // return "Page Selanjutnya";
            return redirect()->route('jawabTebakKataRegional', ["page" => $page]);
        }
        // dd('horo', Session::get('nilai_regional'), Session::get('salah_regional'));
          // return redirect()->route('minigame', [$id]);
      }

      public function winTebakKata()
      {
          if (Session::get('quizId_regional')) {
              $answer = DB::table('regional_tebak_kata_pengerjaan_answers')->where('user_id', Auth::id())
                  ->where('quiz_tebak_kata_id', Session::get('quizId_regional'))
                  ->where('status_jawaban', 1)
                  ->count();
              $check = DB::table('regional_tebak_kata_pengerjaan_answers')->where('user_id', Auth::id())
                  ->where('quiz_tebak_kata_id', Session::get('quizId_regional'))
                  ->count();
              if ($check > 0) {
                  $question = DB::table('regional_tk_event_lists')->where('regional_tk_event_id', Session::get('quizId_regional'))
                      ->count();
                  $poin = DB::table('regional_tebak_kata_pengerjaan_answers')->where('user_id', Auth::id())
                      ->where('quiz_tebak_kata_id', Session::get('quizId_regional'))
                      ->where('status_jawaban', 1)
                      ->sum('poin');
                  return view('dashboard.pages.regional.tebak-kata.tebak-kata-win', compact('answer', 'question', 'poin'));
              }
          }
          return redirect()->route('homeDash');
      }

      public function listUserReg()
      {
        $regional = DB::table('regional_users')
                    ->leftJoin('provinces_ro', 'provinces_ro.id', 'regional_users.province_id')
                    ->leftJoin('cities_ro', 'cities_ro.id', 'regional_users.city_id')
                    ->leftJoin('subdistricts_ro', 'subdistricts_ro.id', 'regional_users.subdistrict_id')
                    ->where('user_id', Auth::id())
                    ->first();
        $regionalUsers = DB::table('regional_users')
                        ->where('regional_users.city_id', $regional->city_id)
                        ->join('users', 'users.id', 'regional_users.user_id')
                        ->select('users.name', 'regional_users.join_regional')
                        ->orderByDesc('regional_users.id')
                        ->take(12)
                        ->get();
        $countUser = DB::table('regional_users')
                        ->where('regional_users.city_id', $regional->city_id)
                        ->join('users', 'users.id', 'regional_users.user_id')
                        ->where('regional_users.join_regional', 1)
                        ->select('users.name', 'regional_users.join_regional')
                        ->orderByDesc('regional_users.id')
                        ->count();

        return view('dashboard.pages.regional.list-user.users', compact('regional','regionalUsers','countUser'));
      }

}
