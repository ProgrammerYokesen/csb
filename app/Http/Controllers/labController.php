<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use DateTime;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Session;
use GuzzleHttp\Client;
use Jenssegers\Agent\Agent;


class labController extends Controller
{

  public function labLandingPage()
  {

    $agent = new Agent();
    if ($agent->isDesktop()) {
      return view('lab.lp');
    }
    return view('lab.lpm');
  }

  public function labRejekiSobat()
  {
    $date = date('Y-m-d H:i:s');
    $datas = DB::table('quiz_rejeki_sobat')
        ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d', 'pertanyaan_rejeki_sobat.link')
        ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
        ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
        ->where('quiz_rejeki_sobat.status', 1)
        ->where('quiz_rejeki_sobat.start_date', '<', $date)
        ->where('quiz_rejeki_sobat.end_date', '>', $date)
        ->get();
    $answer = DB::table('jawaban_users')->where('user_id', Auth::id())
        ->where('quiz_id', $datas[0]->quizId)
        ->count();
    if ($answer == 0) {
        return view('lab.rs', compact('datas'));
    }
    return redirect()->route('winPage');
  }

  public function rejekiSobat(Request $request){

      $temp_poin = 0;
      $poin = 0;
      $check = DB::table('user_poins')
               ->where('user_id', Auth::id())
               ->where('fk_id', $request->quizId)
               ->where('from', 'quiz rejeki sobat')
               ->count();
      if($check != 0){
          return redirect()->route('winPage');
      }
      // dd($request->except(['_token', 'quizId']));
      foreach ($request->except(['_token', 'quizId']) as $key => $value) {
          $answerStatus = $this->checkAnswer($key, $value);
          if($answerStatus == 1){
              $poin = DB::table('pertanyaan_rejeki_sobat')->where('id', $key)->first()->poin;
              $temp_poin = $temp_poin + $poin;
              // echo $temp_poin.'<br>';
          }
          DB::table('jawaban_users')->insert([
              'user_id' => Auth::user()->id,
              'pertanyaan_id' => $key,
              'jawaban_user' => $value,
              'quiz_id' => $request->quizId,
              'status_jawaban' => $answerStatus,
              'created_at' => date('Y-m-d H:i:s'),
              'updated_at' => date('Y-m-d H:i:s'),
          ]);
        }

        /**Check max poin if $temp_poin > $maxPoin return redirect back */
          date_default_timezone_set('Asia/Jakarta');
          $date = date('Y-m-d H:i:s');
          $maxPoin = DB::table('quiz_rejeki_sobat')
          ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d')
          ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
          ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
          ->where('quiz_rejeki_sobat.status', 1)
          ->where('quiz_rejeki_sobat.start_date', '<', $date)
          ->where('quiz_rejeki_sobat.end_date', '>', $date)
          ->sum('poin');

          dd($maxPoin);

          if($temp_poin > $maxPoin){
              abort(406);
          }
        /** */

        DB::table('user_poins')->insert([
          'user_id' => Auth::id(),
          'poin' => $temp_poin,
          'fk_id' => $request->quizId,
          'from' => 'quiz rejeki sobat',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
      ]);

      /**Update total baper poin */

      $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
      $newPoint = $baperPoin + $temp_poin;
      DB::table('users')->where('id', Auth::id())->update([
          'baper_poin' => $newPoint
      ]);

      $request->session()->put('poinRejekiSobat', $temp_poin);
      $request->session()->put('quizId', $request->quizId);
      return redirect()->route('winPage');
  }

  private function checkAnswer($id, $answer){
      $soal = DB::table('pertanyaan_rejeki_sobat')->where('id', $id)->first();
      if($soal->jawaban_benar == $answer){
          return 1;
      }
      else{
          return 0;
      }
  }

    public function deteksi_spam(){
  $satujamlalu = date('Y-m-d H:i:s',strtotime('-7 minutes'));
  //$satujamlalu = date('2021-04-10 00:00:00');
  $ref = DB::table('users')->where('ref_id','>',9)->where('created_at','>',$satujamlalu)->distinct()->select('ref_id')->get();
  //dd($satujamlalu,$ref);
  foreach ($ref as $value) {
      $getref = DB::table('users')->where('ref_id',$value->ref_id)->count();
      //$check_spam = DB::table('users')->where('id',$value->ref_id)->first();
      if($getref > 24){
          $ipdistinct = DB::table('users')->where('ref_id',$value->ref_id)->distinct()->select('ip_address')->get();
          $ipcount = count($ipdistinct);
          $selisih = $getref - $ipcount;
          $ratio = $selisih/$getref *100;
          if($ratio>50){
            $total += $getref;
            echo $value->ref_id." --> ".$getref." --> ".$ipcount." --> ".$selisih." --> [".number_format($ratio,2,'.','.')." % same IP Address ]<br>";
            $update = DB::table('users')->where('id',$value->ref_id)->update([
              'spam' => 1
            ]);
            $update = DB::table('users')->where('ref_id',$value->ref_id)->update([
              'spam' => 2
            ]);
          }
      }
  }
  $totaluser = DB::table('users')->count();
  $percentuser = $total/$totaluser*100;
  echo "<br>-------------------------------------<br>Count Spam data : ".$total."<br>";
  echo "Total Data Register : ".$totaluser."<br>";
  echo "Percent spam from total data : ".number_format($percentuser,2,'.','.')." %";
}

public function deteksi_spam_harian(){
  $ref = DB::table('today_referal')->whereDate('created_at', Carbon::today())->get();
  foreach ($ref as $value) {
      if($value->jumlah > 4){
          $ipdistinct = DB::table('users')->whereDate('created_at', Carbon::today())->where('ref_id',$value->user_id)->distinct()->select('ip_address')->get();
          $ipcount = count($ipdistinct);
          $selisih = $value->jumlah - $ipcount;
          $ratio = $selisih/$value->jumlah *100;
          echo $value->user_id." --> ".$value->jumlah." --> ".$ipcount." --> ".$selisih." --> [".number_format($ratio,2,'.','.')." % same IP Address ]<br>";
          if($ipcount < $value->jumlah){
            $update = DB::table('today_referal')->where('id',$value->id)->update([
              'spamRatio' => $ratio
            ]);
          }

      }
  }
}

    public function spamManual(){
        DB::table('users')->where('ref_id', 294369)->update([
            'spam' => 2
            ]);
        return 'Success';
    }
    public function hariIni(){
        $date = date('Y-m-d');
        $get = DB::table('users')->distinct()->select('ref_id')->whereDate('created_at', $date)->get();
        foreach($get as $isi){
          $peserta = DB::table('users')->where('id',$isi->ref_id)->first();
          echo $peserta->name."<br>";
        }
    }

    public function userPoin(){
      $datas = DB::table('user_poins')->groupBy('user_poins.user_id')
               ->select(DB::raw('sum(user_poins.poin) as sum'), 'users.email', 'users.name')
               ->join('users', 'users.id', 'user_poins.user_id')
               ->get()->sortByDesc('sum');
      foreach($datas as $data){
          echo $data->name.' - '.$data->email.'-'.number_format($data->sum).'</br>';
      }
    }

    public function spamRejekiSobat(){
        $datas = DB::table('user_poins')
                 ->join('users', 'users.id', 'user_poins.user_id')
                 ->groupBy('user_poins.user_id', 'user_poins.fk_id')
                 ->where('user_poins.from', 'quiz rejeki sobat')
                 ->having(DB::raw('count(user_poins.fk_id)'), '>', 1)
                 ->select('user_poins.user_id', 'users.name', 'users.email')->get();
        foreach($datas as $data){
            $spam = DB::table('user_poins')->where('user_id', $data->user_id)->where('from', 'quiz rejeki sobat')
                    ->get();
            echo 'Cleaning Data '. $data->namel.'<br>';
            foreach($spam as $key => $value){
                $index = $key +1;
                $d1 = new DateTime($spam[$index]->created_at);
                $d2 = new DateTime($spam[$key]->created_at);
                $firstDate = $d1->format('Y-m-d');
                $secondDate = $d2->format('Y-m-d');
                if( $firstDate == $secondDate ){
                    DB::table('user_poins')->where('id', $spam[$index]->id)->update([
                        'status' => 0
                        ]);
                }
            }
            echo 'Done Cleaning Data '. $data->name.'<br>';
            sleep(1);
        }
    }

    public function carbonWeek(){
      $weekMap = [
        0 => 'SU',
        1 => 'MO',
        2 => 'TU',
        3 => 'WE',
        4 => 'TH',
        5 => 'FR',
        6 => 'SA',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $weekday = $weekMap[$dayOfTheWeek];
      dd($weekday);
    }

    public function eventTemanSobat(){
      $date = date('Y-m-d H:i:s');
      $userId = 9;
      $activeEvent = DB::table('referral_events')
      ->where('referral_events.start_time', '<', $date)
      ->where('referral_events.end_time', '>', $date)
      ->first();
      if($activeEvent){
        if(DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()){
          /**Update */
          $count = DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()->amount;
          $count = $count + 1;
          DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->update([
            'amount' => $count
          ]);

        }
        else{
          /**Insert */
          DB::table('leaderboards_referral')->insert([
            'user_id' => $userId,
            'referral_events' => $activeEvent->id,
            'amount' => 1,
            'created_at' => now(),
            'updated_at' => now()
          ]);

        }
      }
      return 'success';
    }

    public function leaderBoardBunda(){
        $data = DB::table('user_harbolnas_1010')->where('status', '!=', 'new')->get();
        foreach($data as $key => $value){
            $quiz1 = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)
                    ->where('quiz_bunda_id', 1)
                    ->where('user_id', $value->id)
                    ->first();
            $quiz2 = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)
                    ->where('quiz_bunda_id', 2)
                    ->where('user_id', $value->id)
                    ->first();
            $quiz3 = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)
                    ->where('quiz_bunda_id', 3)
                    ->where('user_id', $value->id)
                    ->first();

            $poin = ($quiz1->nilai + $quiz2->nilai + $quiz3->nilai);

            $time1 = strtotime($quiz1->created_at);
            $time2 = strtotime($quiz2->created_at);
            $time3 = strtotime($quiz3->created_at);
            $time = $time1 + $time2 + $time3;

            $data[$key]->poinRataRata = $poin;
            $data[$key]->time = $time;
        }
        $data = $data->sortByDesc('poinRataRata');
        // dd($data);
        $maxPoin = $data->max('poinRataRata');
        $data = $data->where('poinRataRata', $maxPoin);
        $data = $data->sortBy('time')->take(20);
        dd($data);
        foreach($data as $key => $value){
            DB::table('quiz_bunda_leaderboards_2')->insert([
                'user_id' => $value->id,
                'poin' => $value->poinRataRata,
                'time' => $value->time
            ]);
        }
    }

    public function addBaperPoin(){
        $datas = DB::table('temp_baper')->where('migrate', 0)->get();
        foreach($datas as $key => $value){
            $user = DB::table('users')->where('id', $value->uuid)->first();
            if($user){
                DB::table('user_poins')->insert([
                    'user_id' => $user->id,
                    'poin' => 1000000,
                    'from' => 'Harbolnas1010',
                    'created_at' => now(),
                    'updated_at' => now()
                    ]);
                $baperPoin = $user->baper_poin + 1000000;
                DB::table('users')->where('id', $user->id)->update([
                    'baper_poin' => $baperPoin
                    ]);
                DB::table('temp_baper')->where('id', $value->id)->update([
                    'migrate' => 1
                    ]);
                echo "Nomor $user->whatsapp sucess <br>";
            }
            else{
                echo "Nomor $user->whatsapp tidak dapat temukan <br>";
            }
        }
    }

    public function temanSobatlab()
    {
        $date = date('Y-m-d H:i:s');
        $temanSobat = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->count();
        // $temanSobats = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->get();
        $temanSobats = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->paginate(10);
        $activeEvent = DB::table('referral_events')
        ->where('referral_events.start_time', '<', $date)
        ->where('referral_events.end_time', '>', $date)
        ->first();
        if($activeEvent){
            $data = DB::table('leaderboards_referral')->join('users', 'users.id', 'leaderboards_referral.user_id')->where('leaderboards_referral.status', 1)->where('referral_events', $activeEvent->id)->orderByDesc('amount')->take(20)->get();
        }
        else{
            $data = array();
        }
        // dd($data);
        return view('lab.tsl', compact('temanSobat', 'temanSobats', 'data'));
    }

    public function specialAuctionLab()
    {
      $setting = DB::table('auction_special_settings')->first();
      $date = date('Y-m-d H:i:s');
      $time = date('H:i:s');
      $riwayatLelang = DB::table('auction_specials')
      ->select('auction_specials.id', 'auction_specials.name', 'auction_specials.photo', 'auction_specials.starttime', 'users.name as username', 'auction_special_bids.bid_price as bidTertinggi')
      ->leftJoin('auction_special_bids', 'auction_special_bids.auction_id', 'auction_specials.id')
      ->join('users', 'users.id', 'auction_special_bids.user_id')
      ->groupBy('auction_specials.id')
      ->where('auction_special_bids.winner_status', 1)
      ->where('auction_specials.end_date', '<', $date)
      ->orderByDesc('auction_specials.id')
      ->get();
      $currentPage = LengthAwarePaginator::resolveCurrentPage();
      $col = new Collection($riwayatLelang);
      $perPage = 4;
      $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
      $riwayatLelang = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);

      $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
      $data = DB::table('auction_specials')
          ->where('status', 1)
          ->where('winner_status', 0)
          // ->whereRaw("STR_TO_DATE('%Y-%m-%d %H:%i:%s', starttime) <= $date")
          ->where(function ($query) {
              $query->where('auction_specials.starttime', '<=', now('Asia/Jakarta'));
          })
          ->where(function ($query) {
              $query->where('auction_specials.end_date', '>=', now('Asia/Jakarta'));
          })
          ->first();

      // change space into T
      if ($data) {
          $data->end_date = preg_replace('/\s+/', 'T', $data->end_date);
      }

      $photos = DB::table('auction_special_photos')->where('auction_id', $data->id)->limit(3)->get();
      $leaderBoards = DB::table('auction_special_bids')
          ->select('auction_special_bids.created_at', 'users.name', 'auction_special_bids.bid_price')
          ->where('auction_special_bids.auction_id', $data->id)
          ->leftJoin('users', 'users.id', 'auction_special_bids.user_id')
          ->limit(10)
          ->where('status_bid', 1)
          ->orderByDesc('bid_price')
          ->get();
      $highestBid = DB::table('auction_special_bids')->where('status_bid', 1)->where('auction_special_bids.auction_id', $data->id)->max('bid_price');
      $lelangMendatang = DB::table('auction_specials')
          ->select('auction_specials.name', 'auction_specials.photo', 'users.name as username', DB::raw('MAX(auction_special_bids.bid_price) as bidTertinggi'))
          ->leftJoin('auction_special_bids', 'auction_special_bids.auction_id', 'auction_specials.id')
          ->join('users', 'users.id', 'auction_special_bids.user_id')
          ->groupBy('auction_specials.id')
          ->whereDate('auction_specials.starttime', '>', $date)
          ->where('auction_special_bids.status_bid', 1)
          ->limit(3)
          ->get();


          if ($poin > $setting->baper_poin_maksimal) {
              return redirect()->route('auctionPage');
          }

          // dd($riwayatLelang);

      return view('lab.ma', compact('data', 'photos', 'leaderBoards', 'highestBid', 'lelangMendatang', 'riwayatLelang', 'poin', 'setting'));
    }

    public function labHariAnak()
    {
      $provinces = DB::table('provinces')->select('id', 'name')->get();
      return view('lab.ha',compact('provinces'));
    }

    public function labKTP()
    {
      return view('lab.upload-ktp');
    }

    public function lab12()
    {
      Session::put('harbolnas_1212', true);
      Session::put('link', route('lab12form'));
      return view('lab.12');
    }

    public function lab12form()
    {
      $regencies = DB::table('regencies')->select('id', 'name')->get();
      return view('lab.12-form' ,compact('regencies'));
    }

    public function labGames()
    {
      $now = date('Y-m-d H:i:s');
      // $games = DB::table('harbolnas_1212_games')
      //           ->leftJoin('harbolnas_1212', 'harbolnas_1212.game_id', 'harbolnas_1212_games.id')
      //           ->join(DB::raw("(Select min(start_date) as date from harbolnas_1212 where start_date > '$now' group by game_id) LatestEvent"), function($join) {
      //               $join->on('harbolnas_1212.start_date', '=', 'LatestEvent.date');
      //            })
      //           ->select('harbolnas_1212_games.game_name', 'harbolnas_1212.id as event_id', 'harbolnas_1212_games.id as game_id', 'harbolnas_1212_games.photo', 'harbolnas_1212.start_date', 'harbolnas_1212.end_date')
      //           ->get();
      $games = DB::table('harbolnas_1212_games')
              ->join('harbolnas_1212', 'harbolnas_1212.game_id', 'harbolnas_1212_games.id')
              ->whereDate('harbolnas_1212.start_date', Carbon::today())
              ->select('harbolnas_1212_games.image_drive', 'harbolnas_1212_games.game_name', 'harbolnas_1212.id as event_id', 'harbolnas_1212_games.id as game_id', 'harbolnas_1212_games.photo', 'harbolnas_1212.start_date', 'harbolnas_1212.end_date')
              ->get();
      foreach($games as $k => $v)
      {
        $join = DB::table('harbolnas_1212_event_users')->where('user_id', Auth::id())->where('event_id', $v->event_id)->count();
        $join = $join == 1 ? true:false;

        $minutes_to_add = 30;
        $end_register = new \DateTime($v->start_date);
        $end_register->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
        $stamp = $end_register->format(\DateTime::ATOM);
        $endregister = explode("+", $stamp);

        $startRegister = strtotime($v->start_date);
        $startRegister = $startRegister - (30 * 60);
        $startRegister = date("Y-m-d H:i:s", $startRegister);
        $startRegister = new \DateTime($startRegister);
        $start_register = $startRegister->format(\DateTime::ATOM);
        $sRegister = explode("+", $start_register);

        $games[$k]->start_register = $sRegister[0];
        $games[$k]->end_register = $endregister[0];
        $games[$k]->join = $join;
      }
      $user = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->first();
      $checkUser = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->count();
      $register = false;
      $paid = false;
      $winner = false;
      if($checkUser != 0)
      {

        $register = true;
        if($user->status == 'join')
        {
          $paid = true;
        }
        if($user->winner_status == 1)
        {
          $winner = true;
        }
      }

      // dd($games, $register, $paid, $winner);

      return view('lab.12-games', compact('games', 'register', 'paid', 'winner'));
    }

    public function labGamesRules()
    {
      return view('lab.12-games-rule');
    }

    public function homeLab()
    {
      // dd(Auth::user());
      $date = date('Y-m-d H:i:s');
      $temanSobat = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->count();
      $temanSobats = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->take(5)->get();
      $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');

      $tebakKataPoin = DB::table('quiz_tebak_kata')
          ->leftJoin('quiz_tebak_kata_list', 'quiz_tebak_kata_list.quiz_tebak_kata_id', 'quiz_tebak_kata.id')
          ->leftJoin('tebak_kata', 'tebak_kata.id', 'quiz_tebak_kata_list.pertanyaan_id')
          ->where('quiz_tebak_kata.status', 1)
          ->whereDate('quiz_tebak_kata.start_date', '<', $date)
          ->whereDate('quiz_tebak_kata.end_date', '>', $date)
          ->sum('poin');

      $todayMissionPoin = DB::table('today_missions')->whereDate('date', date('Y-m-d'))->sum('poin');

      $rejekiSobatPoin = DB::table('quiz_rejeki_sobat')
          ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d')
          ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
          ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
          ->where('quiz_rejeki_sobat.status', 1)
          ->where('quiz_rejeki_sobat.start_date', '<', $date)
          ->where('quiz_rejeki_sobat.end_date', '>', $date)
          ->sum('poin');

      $todayPoin = $tebakKataPoin + $todayMissionPoin + $rejekiSobatPoin;

      $todayMissions = DB::table('today_missions')
          ->whereDate('date', date('Y-m-d'))
          ->get();
      $riwayats = DB::table('user_poins')->where('user_id', Auth::id())->where('poin', '!=', 0)->whereMonth('created_at', date('m'))->orderByDesc('created_at')->limit(5)->get();

      $activeTebakKata = DB::table('quiz_tebak_kata')
          ->where('quiz_tebak_kata.status', 1)
          ->whereDate('quiz_tebak_kata.start_date', '<', $date)
          ->whereDate('quiz_tebak_kata.end_date', '>', $date)
          ->first();
      $centangTebakKata = 0;
      if ($activeTebakKata) {
          $centangTebakKata = DB::table('jawaban_tebak_kata')->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $activeTebakKata->id)->count();
      }
      $activeRejekiSobat = DB::table('quiz_rejeki_sobat')
          ->where('quiz_rejeki_sobat.status', 1)
          ->where('quiz_rejeki_sobat.start_date', '<', $date)
          ->where('quiz_rejeki_sobat.end_date', '>', $date)
          ->first();
      $centangRejekiSobat = 0;
      if ($activeRejekiSobat) {
          $centangRejekiSobat = DB::table('jawaban_users')->where('user_id', Auth::id())->where('quiz_id', $activeRejekiSobat->id)->count();
      }
      $fullRiwayats = DB::table('user_poins')
          ->where('user_id', Auth::id())
          ->orderByDesc('created_at')
          ->whereMonth('created_at', date('m'))
          ->whereYear('created_at', date('Y'))
          ->get();


      // Check ada lelang berjalan atau tidak
      $ongoingLelang = DB::table('auctions')
          ->where('status', 1)
          ->where('winner_status', 0)
          ->where(function ($query) {
              $query->where('auctions.starttime', '<=', now('Asia/Jakarta'));
          })
          ->where(function ($query) {
              $query->where('auctions.end_date', '>=', now('Asia/Jakarta'));
          })
          ->first();
          if (env('APP_ENV') != "local") {
              $banners = DB::table('banners')->where('status', 1)->get();
          }
      return view('lab.homelab', compact('temanSobat', 'temanSobats', 'poin', 'todayPoin', 'tebakKataPoin', 'rejekiSobatPoin', 'todayMissions', 'riwayats', 'centangTebakKata', 'centangRejekiSobat', 'fullRiwayats', 'ongoingLelang', 'banners'));
    }

    public function removeSession()
    {
      // dd(Session::all());
      Session::forget('harbolnas_1212');
      Session::forget('link');
      dd(Session::all());
    }

    public function labProfile()
    {
      try {
          $client = new Client();
          $city = $client->request(
              'GET',
              "https://pro.rajaongkir.com/api/city",
              [
                  'headers' => [
                      'key' => "a906fd8fc45a816184224df29f246d93"
                      // 'key' => "437db99af91a23c64bf1bed279bc4d0f"

                  ]
              ]
          );

          // if ($city->getStatusCode() == '200') {
              $data['city'] = json_decode($city->getBody())->rajaongkir->results;
          // } else {
          //     $data['city'] = [];
          // }
      } catch (\GuzzleHttp\Exception\RequestException $e) {
          $data['city'] = [];
      }

      $city = $data['city'];
      $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
      // dd($city);
      return view('lab.profile', compact('city', 'poin'));
    }

    public function labhandleEditProfile(Request $request)
    {
        dd($request);
        date_default_timezone_set("Asia/Jakarta");
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            // 'city' => 'required|string',
            'kode_pos' => 'required|numeric',
            'no_wa' => 'required|string|min:6',
            // 'nik' => 'required|numeric',
        ]);


        // dd($request->all());
        if ($validator->fails()) {
            // dd($validator->fails(), $validator);
            return \Redirect::back()->withErrors($validator);
        }

        $user_id = auth()->user()->id;

        // Check NIK if exists
        $data['nik'] = $request->nik;
        $exitsNIK = DB::table('users')
                    ->where('nik', $request->nik)
                    ->where('id', '<>', $user_id)
                    ->first();
        if ($exitsNIK) {
            if ($exitsNIK->ktp_verification == 1) {
                return redirect()->route('editProfile')->with("errorMsg", "NIK Sudah terdaftar!");
            }
        }

        if($request->ktp){
            $ktp_name = auth()->user()->username . time() . '.' . $request->file('ktp')->getClientOriginalExtension();
            $data['ktp'] = $ktp_name;
            $data['ktp_verification'] = 0;
        }
        // Check for unique email
        // $checkEmail = DB::table('users')
        //     ->where('id', '<>', $user_id)
        //     ->where('email', $request->email)
        //     ->count();

        // if ($checkEmail > 0) {
        //     return \Redirect::back()->with("email", "Email tidak valid!");
        // }

        // Parse no wa to number
        $whatsapp = $request->no_wa;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);

        if ($whatsapp[0] == '0') {
            if ($whatsapp[1] == '8') {
                $whatsapp = ltrim($whatsapp, "0");;
                $whatsapp = "62" . $whatsapp;
            } else {
                $whatsapp = '-';
            }
        } else {
            if ($whatsapp[0] == '8') {
                $whatsapp = "62" . $whatsapp;
            } elseif ($whatsapp[0] == '6' && $whatsapp[1] == '2') {
                $whatsapp;
            } elseif ($whatsapp[0] == '+') {
                if ($whatsapp[1] == '6' && $whatsapp[2] == '2') {
                    $whatsapp = str_replace("+", "", $whatsapp);
                } else {
                    $whatsapp = '-';
                }
            } else {
                $whatsapp = '-';
            }
        }

        // check is WA valid
        if ($whatsapp == "-") {
            return \Redirect::back()->with("no_wa", "Nomor tidak valid!");
        }
        if($request->no_wa == Auth::user()->whatsapp){
            date_default_timezone_set("Asia/Jakarta");
            $data['name'] = $request->name;
            $data['kota'] = $request->city;
            $data['alamat_rumah'] = $request->alamat;
            $data['kode_pos'] = $request->kode_pos;
            $data['updated_at'] = date('Y-m-d H:i:s');

            DB::table('users')
                ->where('id', $user_id)
                ->update($data);
            // DB::table('users')
            //     ->where('id', $user_id)
            //     ->update([
            //         'name' => $request->name,
            //         'kota' => $request->city,
            //         'alamat_rumah' => $request->alamat,
            //         'kode_pos' => $request->kode_pos,
            //         'nik' => $request->nik,
            //         'ktp' => $ktp_name,
            //         'ktp_verification' => 0,
            //         'updated_at' => date('Y-m-d H:i:s')
            //     ]);

            // put ktp on storage if success
            if($request->ktp){
              Storage::putFileAs('public/ktp', $request->ktp, $ktp_name);
            }

            $names = explode(' ', trim($request->name));
            Session::put('user_name', $names[0]);
            return redirect()->route('labProfile')->with("success", "Berhasil memperbarui profil!");
        }

        $checkWA = DB::table('users')
            ->where('id', '!=', Auth::id())
            ->where('whatsapp_verification', 1)
            ->where('whatsapp', $whatsapp)
            ->count();

        if ($checkWA > 0) {
            return \Redirect::back()->with("no_wa", "Nomor sudah dipakai!");
        }

        date_default_timezone_set("Asia/Jakarta");
        $data['name'] = $request->name;
        $data['kota'] = $request->city;
        $data['alamat_rumah'] = $request->alamat;
        $data['kode_pos'] = $request->kode_pos;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['whatsapp'] = $whatsapp;

        DB::table('users')
            ->where('id', $user_id)
            ->update($data);
        // DB::table('users')
        //     ->where('id', $user_id)
        //     ->update([
        //         'name' => $request->name,
        //         'kota' => $request->city,
        //         'alamat_rumah' => $request->alamat,
        //         'kode_pos' => $request->kode_pos,
        //         'whatsapp' => $whatsapp,
        //         'nik' => $request->nik,
        //         'ktp' => $ktp_name,
        //         'ktp_verification' => 0,
        //         'updated_at' => date('Y-m-d H:i:s')
        //     ]);
        // put ktp on storage if success
        if($request->ktp){
          Storage::putFileAs('public/ktp', $request->ktp, $ktp_name);
        }

        $names = explode(' ', trim($request->name));
        Session::put('user_name', $names[0]);
        return redirect()->route('labProfile')->with("success", "Berhasil memperbarui profil!");
    }

    public function labSurvey()
    {
      return view('lab.survey');
    }

    public function getWelcome(){
        $daftar = DB::table('sppa_videos')->where('user_id', Auth::id())->orderByDesc('created_at')->first();
        /**
        * $daftar = null : belum upload sama sekali
        * $daftar->status ? accepted, rejected, new
        */
        //dd($daftar);
        return view('lab.sppa-welcome', compact('daftar'));
    }

    public function getPageForm(){
        $daftar = DB::table('sppa_videos')->where('user_id', Auth::id())->orderByDesc('created_at')->first();
        if($daftar != null){
          return redirect()->route('sppaWelcomePage')->with('errorMsg', 'Kamu sudah pernah isi!');
        }
        $categories = DB::table('sppa_categories')->where('status', 'active')->get();
        /** id, category_name, photo */
        return view('lab.sppa-form', compact('categories'));
    }
    public function sppaReferensi(){
      return view('lab.sppa-referensi');
    }
    public function postResult(Request $request){
        dd($request);
        $link = $request->link;
        $kategori = $request->kategori;

        //LOGIC TO DB
        DB::table('sppa_videos')->insert([
          'original_link' => $link,
          'status' => 'new',
          'user_id' => Auth::id(),
          'sppa_category_id' => $kategori,
          'created_at' => now(),
          'updated_at' => now()
        ]);
        ///
        $success = true;
        if($success){
            return redirect()->route('sppaWelcomePage')->with('successInput','Sukses Memasukan data' );
        }
    }
    public function sppaList(Request $request)
    {
      $data['vote'] = false;
      if ($request->has('vote')) {
        $data['vote'] = true;
      }

      /** id, category_name, photo */
      $data["categories"] = DB::table('sppa_categories')->where('status', 'active')->get();

      $query = DB::table('sppa_videos')->where('sppa_videos.status', 'accepted')
                        ->leftJoin('sppa_video_likes', 'sppa_video_likes.sppa_video_id', 'sppa_videos.id')
                        ->groupBy('sppa_videos.id')
                        ->join('sppa_categories', 'sppa_categories.id', 'sppa_videos.sppa_category_id')
                        ->join('users', 'users.id', 'sppa_videos.user_id')
                        ->select('sppa_videos.sppa_category_id as cat_id', 'sppa_videos.id as id', 'sppa_videos.link', 'users.name', 'sppa_categories.category_name', DB::raw("count(sppa_video_likes.sppa_video_id) as totalLike"));
                        // ->paginate(9);
      if($request->has('q'))
      {
        // dd($request->q);
        if($request->q == 'all')
        {
          $data["videos"] = $query->paginate(9);
        }
        else {
          $arr = json_decode($request->q);
          $data["videos"] = $query->whereIn('sppa_videos.sppa_category_id', $arr)->paginate(9);
        }
      }
      else{
        $data["videos"] = $query->paginate(9);
      }
      // dd($data);

      return view('lab.sppa-list', $data);
    }

    public function requestCheck(Request $request){
      if(!$request->name){
        dd('gak ada name');
      }
      if($request->name){
        dd('ada name');
      }
    }

    public function seesession(){
      dd(Session::all());
    }

    public function labQsi()
    {
      $date = date('Y-m-d H:i:s');
      $quiz =DB::table('imlek_events')
      ->whereDate('imlek_events.start_date', '=', Carbon::today())
      // ->where('imlek_events.start_date', '<', $date)
      // ->where('imlek_events.end_date', '>', $date)
      ->first();
      $startDate = new \DateTime($quiz->start_date);
      $endDate = new \DateTime($quiz->end_date);
      $startDate = $startDate->format(\DateTime::ATOM);
      $endDate = $endDate->format(\DateTime::ATOM);

      $mulai = explode('+',$startDate)[0];


      $totalUser = DB::table('imlek_event_users')->where('imlek_event_id', $quiz->id)->count();


      return view('dashboard.pages.quiz-show-imlek.quiz-show-detail', compact('startDate', 'endDate', 'totalUser'));
    }

    public function labQsiLu($id)
    {
      $date = date('Y-m-d H:i:s');

      $data = DB::table('imlek_event_users')
              ->join('users', 'users.id', 'imlek_event_users.user_id')
              ->where('imlek_event_users.imlek_event_id', $id)
              ->groupBy('users.id')
              ->where('imlek_event_users.status', '!=', 'failed')
              ->select('users.name', 'imlek_event_users.status', 'imlek_event_users.prize')
              ->paginate(15);

      $events = DB::table('imlek_events')
                ->where('start_date', '<', $date)
                ->get();

      $currentEvent = DB::table('imlek_events')
                      ->where('id', $id)
                      ->first();


      // dd($currentEvent);

      return view('lab.qsilu', compact('data', 'events', 'currentEvent'));
    }

    public function getDataRajaOngkir()
    {

      // $client = new Client();
      //
      // $cities = DB::table('cities_ro')->where('city_id', '>=', 401)->get();
      // // $cities = DB::table('cities_ro')->where('city_id', '=', 400)->get();
      // foreach ($cities as $k => $v) {
      //   // code...
      //   $data = $client->get("https://pro.rajaongkir.com/api/subdistrict?city=$v->id",[
      //     'headers' => [
      //         'key' => 'a906fd8fc45a816184224df29f246d93',
      //     ]
      //   ]);
      //
      //   $res = json_decode($data->getBody());
      //   $data = $res->rajaongkir->results;
      //   // dd($data);
      //   foreach($data as $key => $value)
      //   {
      //     DB::table('subdistricts_ro')->insert([
      //       'id' => $value->subdistrict_id,
      //       'city_id' => $v->id,
      //       'province_id' => $value->province_id,
      //       'subdistrict_name' => $value->subdistrict_name,
      //       'created_at' => now(),
      //       'updated_at' => now()
      //     ]);
      //   }
      // }

      $kotaBaru = DB::table('regencies')->get();
      foreach ($kotaBaru as $key => $value) {
        $check = DB::table('cities_ro')->where('city_name','like' ,"%$value->name%")->count();
        if($check == 0)
        {
          echo $value->name."<br>";
        }
      }

    }

    public function seragaminDataKota()
    {
      $users = DB::table('users')->whereNotNull('kota')->where('flag_edit_kota', 0)->limit(2000)->get();
      foreach($users as $key => $value)
      {
          if(is_numeric($value->kota))
          {
            /** Kota berisi id */
            $kota = DB::table('regencies')->where('id', $value->kota)->first()->name;
            // dd($kota);
          }
          else{
            /** Kota Berisi String di database*/
            $kota = $value->kota;
          }

          /** Cari Id Data kota dari rajaongkir*/
          $cityCount = DB::table('cities_ro')->where('city_name', 'like', "%$kota%")->count();
          // dd($cityCount, $kota, $value->email);
          if($cityCount == 1)
          {
            $city = DB::table('cities_ro')->where('city_name', 'like', "%$kota%")->first();

            /** Update Users data kota and province*/
            DB::table('users')->where('id', $value->id)->update([
              'city_id' => $city->id,
              'province_id' => $city->province_id,
              'flag_edit_kota' => 1,
              'updated_at' => now()
            ]);
            echo "Berhasil Edit : $value->email"."<br>";
          }
          elseif($cityCount > 1)
          {
            $city = DB::table('cities_ro')->where('city_name', $kota)->first();

            /** Update Users data kota and province*/
            DB::table('users')->where('id', $value->id)->update([
              'city_id' => $city->id,
              'province_id' => $city->province_id,
              'flag_edit_kota' => 1,
              'updated_at' => now()
            ]);
            echo "Berhasil Edit2 : $value->email"."<br>";
          }
          else{
            DB::table('users')->where('id', $value->id)->update([
              'flag_edit_kota' => 2,
              'updated_at' => now()
            ]);
            echo "Gagal Edit : $value->email"."<br>";
          }
      }
    }

    public function badgeUsers($userId)
    {
      $string = badgeUser($userId);
      // dd($string);
      return view('lab.badge-lab', compact('string'));
    }
}
