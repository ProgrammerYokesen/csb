<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ErrorController extends Controller
{
    public function error401()
    {
        return view('errors.401');
    }

    public function error403()
    {
        return view('errors.403');
    }

    public function error404()
    {
        return view('errors.404');
    }

    public function error405()
    {
        return view('errors.405');
    }

    public function error429()
    {
        return view('errors.429');
    }

    public function error500()
    {
        return view('errors.500');
    }

    public function error503()
    {
        return view('errors.503');
    }

    public function maintenance(){
      // \Session::flush(); 
      dd(\Session::all());
        return view('errors.maintenance');
    }
}
