<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\tampungUsers;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class importController extends Controller
{
    public function importUsersNgabuburit()
    {
        dd(request()->file('file'));
        Excel::import(new tampungUsers, request()->file('file'));

        return response()->json([
            'Status' => 'Success'
        ]);
    }

    public function importPage()
    {
        return view('imports.import');
    }
    public function transferData()
    {
        // Script start
        $rustart = getrusage();

        $datas = DB::table('tampung_users_ngabuburit')->where('flag', NULL)->limit(3000)->get()->sortBy('created_at')->values();
        // dd($datas);
        if ($datas->count() != 0) {
            foreach ($datas as $data) {
                
                $username = $this->createUsername();
                $referral = $this->createReferral();

                $check = DB::table('users')->where('email', $data->email)->first();
                /**User sudah terdaftar sendiri */
                if($check){
                    DB::table('tampung_users_ngabuburit')->where('email', $check->email)->update([
                        'flag' => 1,
                        'hit' => 1,
                        'user_id' => $check->id
                    ]);
                    DB::table('users')->where('email', $data->email)->update([
                        'countRef' => $data->countRef,
                        'spam' => $data->spam,
                        'spamRatio' => $data->spamRatio,
                        'emailTrue' => $data->emailTrue,
                        'emailFalse' => $data->emailFalse,
                        'rejekiNomplokPoin' => $data->rejekiNomplokPoin,
                        'subscribeEmail' => $data->subscribeEmail,
                        'subscribeWhatsapp' => $data->subscribeWhatsapp,
                        'msgEmail' => $data->msgEmail,
                        'msgWhatsapp' => $data->msgWhatsapp,
                        'kota' => $data->kota,
                        'provinsi' => $data->provinsi,
                        'ip_address' => $data->ip_address,
                        'device' => $data->device,
                        'utm_campaign' => $data->utm_campaign,
                        'utm_source' => $data->utm_source,
                        'utm_content' => $data->utm_content,
                        'alamat_rumah' => $data->alamat_rumah,
                        'alamat_detail' => $data->alamat_detail,
                        'alamat_device' => $data->alamat_device,
                        'lat_rumah' => $data->lat_rumah,
                        'lng_rumah' => $data->lng_rumah,
                        'email_verified_at' => $data->email_verified_at,
                        'utm_medium' => $data->utm_medium
                    ]);
                }
                /**User belum ada */
                $checkUser = DB::table('users')->where('email', $data->email)->first();
                if(!$checkUser){
                    $userId = DB::table('users')->insertGetId([
                        'username' => $username,
                        'referral_code' => $referral,
                        'countRef' => $data->countRef,
                        'spam' => $data->spam,
                        'spamRatio' => $data->spamRatio,
                        'emailTrue' => $data->emailTrue,
                        'emailFalse' => $data->emailFalse,
                        'rejekiNomplokPoin' => $data->rejekiNomplokPoin,
                        'name' => $data->name,
                        'email' => $data->email,
                        'emailValidation' => $data->emailValidation,
                        'password' => $data->password,
                        'whatsapp' => $data->whatsapp,
                        'subscribeEmail' => $data->subscribeEmail,
                        'subscribeWhatsapp' => $data->subscribeWhatsapp,
                        'msgEmail' => $data->msgEmail,
                        'msgWhatsapp' => $data->msgWhatsapp,
                        'kota' => $data->kota,
                        'provinsi' => $data->provinsi,
                        'ip_address' => $data->ip_address,
                        'device' => $data->device,
                        'utm_campaign' => $data->utm_campaign,
                        'utm_source' => $data->utm_source,
                        'utm_content' => $data->utm_content,
                        'alamat_rumah' => $data->alamat_rumah,
                        'alamat_detail' => $data->alamat_detail,
                        'alamat_device' => $data->alamat_device,
                        'lat_rumah' => $data->lat_rumah,
                        'lng_rumah' => $data->lng_rumah,
                        'remember_token' => $data->remember_token,
                        'email_verified_at' => $data->email_verified_at,
                        'created_at' => $data->created_at,
                        'updated_at' => $data->updated_at,
                        'utm_medium' => $data->utm_medium,
                        'providerOrigin' => 'ngabuburit'
                    ]);
                    $hit = $data->hit + 1;
                    DB::table('tampung_users_ngabuburit')->where('id', $data->id)->update([
                        'flag' => 2,
                        'hit' => $hit,
                        'user_id' => $userId
                    ]);
                }
            }
            $ru = getrusage();
            echo "This process used " . $this->rutime($ru, $rustart, "utime") .
                " ms for its computations\n";
            echo "It spent " . $this->rutime($ru, $rustart, "stime") .
                " ms in system calls\n";
        }
        else{
            return 'Semua data sudah di transfer';
        }
    }

    public function createReferral()
    {
        $referral = \Str::random(8);
        $check = DB::table('users')->where('referral_code', $referral)->count();
        if ($check == 1) {
            $this->createReferral();
        }
        return $referral;
    }

    public function createUsername()
    {
        $username = 'User' . rand();
        $check = DB::table('users')->where('username', $username)->count();
        if ($check == 1) {
            $this->createUsername();
        }
        return $username;
    }

    public function rutime($ru, $rus, $index) {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
         -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }

    public function auctionTest(){
                $auctions = DB::table('auctions')->where('auctions.winner_status', 0)->where('auctions.end_date', '<=', now('Asia/Jakarta'))->get();
            foreach($auctions as $auction){
  
                $bid_price = DB::table('auction_bids')->where('auction_id', $auction->id)->max('bid_price');
                $winner = DB::table('auction_bids')->where('auction_id', $auction->id)->where('bid_price', $bid_price)->first();
                DB::table('auctions')->where('id', $auction->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('auction_bids')->where('id', $winner->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('user_poins')->insert([
                    'user_id' => $winner->user_id,
                    'poin' => -$bid_price,
                    'from' => 'Bid - '.$auction->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            return 'Success';
    }

}
