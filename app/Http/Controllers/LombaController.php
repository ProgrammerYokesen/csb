<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class LombaController extends Controller
{
    public function tambalBan(Request $request)
    {
        $agent = new Agent();
        if($agent->isMobile()){
            return view('dashboard.pages.17an.tambal-mobile');
        }
        return view('dashboard.pages.17an.tambal-ban');
    }

    public function goTambal(Request $request)
    {
        return redirect()->route('tambalBan')->with('go', true);
    }

    public function tambalBanMobile(Request $request)
    {
        # code...
        $agent = new Agent();
        if($agent->isDesktop()){
            return view('dashboard.pages.17an.tambal-ban');
        }
        return view('dashboard.pages.17an.tambal-mobile');
    }

    public function goBanMobile(Request $request)
    {
        return redirect()->route('tambalBanMobile')->with('go', true);
    }

    public function panjatPinang()
    {
        $agent = new Agent();
        if($agent->isMobile()){
            return view('dashboard.pages.17an.panjat-pinang-mobile');
        }
        return view('dashboard.pages.17an.panjat-pinang');
    }

    public function panjatPinangGame()
    {
        return view('dashboard.pages.17an.panjat-pinang-game');
    }

    public function panjatPinangMobile()
    {
        $agent = new Agent();
        if($agent->isDesktop()){
            return view('dashboard.pages.17an.panjat-pinang');
        }
        return view('dashboard.pages.17an.panjat-pinang-mobile');
    }

    public function panjatLeaderboard()
    {
        // $leaderBoard = DB::table('game_panjat_pinangs')
        //                 ->join('users', 'users.id', 'game_panjat_pinangs.user_id')
        //                 ->where('game_panjat_pinangs.status', 1)
        //                 ->where('game_panjat_pinangs.game_end', 1)
        //                 ->groupBy('game_panjat_pinangs.user_id')
        //                 ->limit(20)
        //                 ->orderByDesc('time')
        //                 ->get();
        // $leaderBoard = DB::table('game_panjat_pinangs')
        //          ->select('game_panjat_pinangs.user_id', 'users.name')
        //          ->join('users', 'users.id', 'game_panjat_pinangs.user_id')
        //          ->where('game_panjat_pinangs.status', 1)
        //          ->groupBy('user_id')
        //          ->get();
        // foreach($leaderBoard as $ky => $lb){
        //     $value = DB::table('game_panjat_pinangs')
        //             ->where('user_id', $lb->user_id)
        //             ->max('time');
        //     $leaderBoard[$ky]->time = $value;
        // }
        $leaderBoard = DB::table('game_panjat_pinangs')
                 ->select('game_panjat_pinangs.user_id', 'users.name', 'game_panjat_pinangs.created_at')
                 ->join('users', 'users.id', 'game_panjat_pinangs.user_id')
                 ->where('game_panjat_pinangs.status', 1)
                 ->groupBy('user_id')
                 ->get();
        foreach($leaderBoard as $ky => $lb){
            $value = DB::table('game_panjat_pinangs')
                    ->where('user_id', $lb->user_id)
                    ->where('game_panjat_pinangs.status', 1)
                    ->max('time');
            $leaderBoard[$ky]->time = $value;
        }

        $leaderBoard = $leaderBoard->sortByDesc('time')->values();
        $temp = [];
        foreach($leaderBoard as $key => $value){
            $Index = $key + 1;
            if($value->time == $leaderBoard[$Index]->time){
                $time1 = strtotime($value->created_at);
                $time2 = strtotime($leaderBoard[$Index]->created_at);
                if($time1 < $time2){
                    $temp = $leaderBoard[$key];
                    $leaderBoard[$key] = $leaderBoard[$Index];
                    $leaderBoard[$Index] = $temp;
                }
            }
        }
        $leaderBoard = $leaderBoard->sortByDesc('time')->values();
        return view('dashboard.pages.17an.panjat-leaderboard', compact('leaderBoard'));
    }
    public function mainMenu()
    {
        $userRegistered = DB::table('pre_registers')->where('user_id', Auth::id())->count();
        $fashionShow = DB::table('lomba_fashion_show')->where('user_id', Auth::id())->count();
        $makanKrupuk = DB::table('lomba_makan_krupuk')->where('user_id', Auth::id())->count();
        $tebakan = DB::table('lomba_tebakan')->where('user_id', Auth::id())->count();
        $tebakLagu = DB::table('lomba_tebak_lagu')->where('user_id', Auth::id())->count();
        return view('dashboard.pages.17an.main-menu', compact('userRegistered', 'fashionShow', 'makanKrupuk', 'tebakan', 'tebakLagu'));
    }
    
    public function leaderboardTambalBan()
    {
        $data['leaderboard'] = DB::table('pompa_ban')
            ->join('users', 'users.id', 'pompa_ban.user_id')
            ->where('pompa_ban.status', 1)->where('pompa_ban.game_end', 1)
            ->where('total_ban', 3)
            ->groupBy('pompa_ban.user_id')
            ->select('pompa_ban.user_id', 'users.name')
            ->orderBy('pompa_ban.time', 'asc')
            ->get();
        foreach($data['leaderboard'] as $key => $lb){
             $value = DB::table('pompa_ban')
                     ->where('total_ban', 3)
                     ->where('pompa_ban.status', 1)
                     ->where('pompa_ban.status', 1)
                     ->where('pompa_ban.game_end', 1)
                    ->where('user_id', $lb->user_id)
                    ->min('time');
            $data['leaderboard'][$key]->time = $value;
        }
        $data['leaderboard'] = $data['leaderboard']->sortBy('time')->values();

        return view('dashboard.pages.17an.leaderboard-ban', $data);
    }

    public function ppkmUser()
    {
        return view('landing-page.pages.17an');
    }
    
    public function hadiahPanjat()
    {
        return view('dashboard.pages.17an.hadiah.hadiah-panjat');
    }
    public function hadiahPompa()
    {
        return view('dashboard.pages.17an.hadiah.hadiah-pompa');
    }
}
