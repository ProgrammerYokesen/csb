<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class harbolnasController extends Controller
{
    public function store(Request $request)
    {
        $random = Str::random(30);

        $agent = new Agent();
        $platform = $agent->platform();
        $version = $agent->version($platform);
        $browser = $agent->browser();
        $versionbrowser = $agent->version($browser);

        $whatsapp = $request->whatsapp;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if ($check_number[0] == '0') {
            foreach ($check_number as $n => $number) {
                if ($n > 0) {
                    if ($check_number[1] == '8') {
                        $new_number .= $number;
                    } else {
                        $new_number = '-';
                    }
                }
            }
        } else {
            if ($check_number[0] == '8') {
                $new_number = "62" . $whatsapp;
            } elseif ($check_number[0] == '6') {
                $new_number = $whatsapp;
            } elseif ($check_number[0] == '+') {
                foreach ($check_number as $n => $number) {
                    if ($n > 2) {
                        $new_number .= $number;
                    }
                }
            } else {
                $new_number = '-';
            }
        }

        $userId = DB::table('user_harbolnas')->insertGetId([
            'name' => $request->name,
            'lokasi' => $request->lokasi,
            'status' => 'new',
            'ip_address' => \Request::ip(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'version_platform' => $version,
            'browser'  => $agent->browser(),
            'version_browser' => $versionbrowser,
            'languages' => $agent->languages(),
            'created_at' => now(),
            'whatsapp' => $new_number,
            'token' => $random
        ]);

        $lpcbId = 5;

        DB::table('user_sembako')->insert([
            'user_id' => $userId,
            'sembako_id' => $lpcbId
        ]);

        if ($request->has('products')) {
            foreach ($request->products as $key => $value) {
                DB::table('user_sembako')->insert([
                    'user_id' => $userId,
                    'sembako_id' => $value
                ]);
            }
        }

        $cookie = $_COOKIE['footprints'];

        if ($cookie != NULL) {
            // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
            // $decryptedString = $encrypter->decrypt($cookie);
            $decryptedString = \Crypt::decrypt($cookie, false);
            $cookies = explode("|", $decryptedString);
            // dd($decryptedString, $cookies);
            $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                'user_id' => $userId
            ]);
        }
        // return $cookie;

        $count = DB::table('whatsapp_cs')->where('status', 1)->count();
        $arr = $count - 1;
        $id = random_int(0, $arr);
        $data = DB::table('whatsapp_cs')->where('id', $id)->first();

        return response()->json([
            'success' => true,
            'message' => 'Berhasil menambahkan data',
            'orderId' => $random
        ]);
    }

    public function whatsappRedirect($orderId)
    {
        $user = DB::table('user_harbolnas')->where('token', $orderId)->first();
        $belanja = DB::table('user_sembako')
            ->select('sembako.name')
            ->join('sembako', 'sembako.id', 'user_sembako.sembako_id')
            ->where('user_id', $user->id)
            ->get();
        $data = array();
        $enter = '%0a';
        $string = '';
        $temp_string = '';
        foreach ($belanja as $bj) {
            $string = $bj->name . '%0a';
            $temp_string = $temp_string . $string;
        }
        // $whatsapp = DB::table('whatsapp_cs')->where('status', 1)->count();
        $rnd = $this->random();
        DB::table('user_harbolnas')->where('token', $orderId)->update([
            'whatsapp_cs_id' => $rnd
        ]);
        $nomer = DB::table('whatsapp_cs')->where('id', $rnd)->first();

        // return redirect('https://api.whatsapp.com/send/?phone=6285155094177&text=Hallo%20admin,%20Bunda%20mau%20beli%20belanjaan%20ini%20nih%0aBarang%201%0a%20Barang%202&app_absent=0');
        return redirect('https://api.whatsapp.com/send/?phone=' . $nomer->whatsapp . '&text=Hallo%20admin,%20Bunda%20mau%20beli%20belanjaan%20ini%20nih%0a%0a' . $temp_string . '%0a(order ID = ' . $orderId . ')&app_absent=0');
    }
    
   public function random(){
        $lastId = DB::table('user_harbolnas')->orderByDesc('created_at')->skip(1)->first()->whatsapp_cs_id;
        $endId = $lastId + 1;

        $data = DB::table('whatsapp_cs')->where('id', $endId)->first();
        if($data){
            return $data->id;
        }
        else{
            return DB::table('whatsapp_cs')->first()->id;
        }

    }
    
    public function postMakeup(Request $request)
    {
        $agent = new Agent();
        $platform = $agent->platform();
        $version = $agent->version($platform);
        $browser = $agent->browser();
        $versionbrowser = $agent->version($browser);
        
        // $request->validate([
        //     'nama' => 'required|min:2',
        //     'no_wa' => 'required',
        //     'email' => 'required|email|unique:user_makeup,email',
        //     'alamat' => 'required'
        // ]);
        
        $validator = Validator::make($request->all(), [
            'nama' => 'required|min:2',
            'no_wa' => 'required',
            'email' => 'required|email|unique:user_makeup,email',
            'alamat' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }

        $whatsapp = $request->no_wa;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if ($check_number[0] == '0') {
            foreach ($check_number as $n => $number) {
                if ($n > 0) {
                    if ($check_number[1] == '8') {
                        $new_number .= $number;
                    } else {
                        $new_number = '-';
                    }
                }
            }
        } else {
            if ($check_number[0] == '8') {
                $new_number = "62" . $whatsapp;
            } elseif ($check_number[0] == '6') {
                $new_number = $whatsapp;
            } elseif ($check_number[0] == '+') {
                foreach ($check_number as $n => $number) {
                    if ($n > 2) {
                        $new_number .= $number;
                    }
                }
            } else {
                $new_number = '-';
            }
        }
        $data  = [
            'nama' => $request->nama,
            'no_wa' => $new_number,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'status' => 'new',
            'ip_address' => \Request::ip(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'browser'  => $agent->browser(),
            'languages' => $agent->languages(),
            'version_platform' => $version,
            'version_browser' => $versionbrowser,
            'created_at' => now(),
        ];

        $userId = DB::table('user_makeup')->insertGetId($data);

        $cookie = $_COOKIE['footprints'];

        if($cookie != NULL){
            // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
            // $decryptedString = $encrypter->decrypt($cookie);
            $decryptedString = \Crypt::decrypt($cookie, false);
            $cookies = explode("|",$decryptedString);
            // dd($decryptedString, $cookies);
            $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                'user_makeup_id' => $userId
            ]);
        }

        // $rnd = $this->randomCsMakeUp();

        // DB::table('user_makeup')->where('id', $userId)->update([
        //     'whatsapp_cs_id' => $rnd
        // ]);

        // $nomer = DB::table('whatsapp_cs')->where('id', $rnd)->first();
        $user = DB::table('user_makeup')->where('id', $userId)->first();
        return response()->json([
            'success' => true,
            'message' => 'Berhasil menambahkan data',
            'user' => $user
        ]);
    }

    public function post1010(Request $request)
    {
        $agent = new Agent();
        $platform = $agent->platform();
        $version = $agent->version($platform);
        $browser = $agent->browser();
        $versionbrowser = $agent->version($browser);
        $orderId = 'Invoice-'.\Str::random(7);
        
        // $request->validate([
        //     'nama' => 'required|min:2',
        //     'no_wa' => 'required',
        //     'email' => 'required|email',
        //     'alamat' => 'required',
        //     'kebutuhan' => 'required',
        //     'cerita' => 'required'
        // ]);
        
        $validator = Validator::make($request->all(), [
            'nama' => 'required|min:2',
            'no_wa' => 'required',
            'email' => 'required|email|unique:user_harbolnas_1010,email',
            'alamat' => 'required',
            'kebutuhan' => 'required',
            'cerita' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'Failed',
                'message' => $validator->errors()
            ], 200);
        }

        $whatsapp = $request->no_wa;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if ($check_number[0] == '0') {
            foreach ($check_number as $n => $number) {
                if ($n > 0) {
                    if ($check_number[1] == '8') {
                        $new_number .= $number;
                    } else {
                        $new_number = '-';
                    }
                }
            }
        } else {
            if ($check_number[0] == '8') {
                $new_number = "62" . $whatsapp;
            } elseif ($check_number[0] == '6') {
                $new_number = $whatsapp;
            } elseif ($check_number[0] == '+') {
                foreach ($check_number as $n => $number) {
                    if ($n > 2) {
                        $new_number .= $number;
                    }
                }
            } else {
                $new_number = '-';
            }
        }


        $data  = [
            'nama' => $request->nama,
            'no_wa' => $new_number,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'status' => 'new',
            'kebutuhan' => $request->kebutuhan,
            'cerita' => $request->cerita,
            'ip_address' => \Request::ip(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'browser'  => $agent->browser(),
            'languages' => $agent->languages(),
            'version_platform' => $version,
            'version_browser' => $versionbrowser,
            'created_at' => now(),
            'order_id' => $orderId
        ];

        $userId = DB::table('user_harbolnas_1010')->insertGetId($data);

        $cookie = $_COOKIE['footprints'];

        if($cookie != NULL){
            // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
            // $decryptedString = $encrypter->decrypt($cookie);
            $decryptedString = \Crypt::decrypt($cookie, false);
            $cookies = explode("|",$decryptedString);
            // dd($decryptedString, $cookies);
            $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                'user_harbolnas_id' => $userId
            ]);
        }

        $rnd = $this->randomCs();

        DB::table('user_harbolnas_1010')->where('id', $userId)->update([
            'whatsapp_cs_id' => $rnd
        ]);

        $nomer = DB::table('whatsapp_cs')->where('id', $rnd)->first();
        $user = DB::table('user_harbolnas_1010')->where('user_harbolnas_1010.id', $userId)->select('user_harbolnas_1010.*', 'whatsapp_cs.whatsapp')->join('whatsapp_cs', 'whatsapp_cs.id', 'user_harbolnas_1010.whatsapp_cs_id')->first();
        return response()->json([
            'success' => true,
            'message' => 'Berhasil menambahkan data',
            'user' => $user
        ]);
        return redirect('https://api.whatsapp.com/send/?phone=' . $nomer->whatsapp . '&text=Hallo+admin%2C+Bunda+mau+ikutan+belajar+jadi+Reseller+Warisan+Gajahmada+dan+mau+bayar+Rp10.100+agar+dapat+kesempatan+dibayarin+kebutuhannya.+Ini+pesanan+Bunda+ya%3A%0D%0A-+1+Bundle+Larutan+Penyegar+Cap+Badak%0D%0A-+'.$request->kebutuhan.'%0D%0A%28order+ID+%3D+variableId%29&app_absent=0');
    }

    public function randomCs(){
        $lastId = DB::table('user_harbolnas_1010')->orderByDesc('created_at')->skip(1)->first()->whatsapp_cs_id;
        if($lastId == 6){
            $endId = 8;
        }
        else{
            $endId = $lastId + 1;
        }

        $data = DB::table('whatsapp_cs')->where('id', $endId)->first();
        if($data){
            return $data->id;
        }
        else{
            return DB::table('whatsapp_cs')->where('status', 1)->first()->id;
        }

    }

    public function randomCsMakeUp(){
        $lastId = DB::table('user_makeup')->orderByDesc('created_at')->skip(1)->first()->whatsapp_cs_id;
        $endId = $lastId + 1;

        $data = DB::table('whatsapp_cs')->where('id', $endId)->first();
        if($data){
            return $data->id;
        }
        else{
            return DB::table('whatsapp_cs')->where('status', 1)->first()->id;
        }

    }
    
}