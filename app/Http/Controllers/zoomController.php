<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class zoomController extends Controller
{
    public function joinZoom(){
        if(Auth::user()){
            $hours = date('H');
            $date = date('Y-m-d H:i:s');
            if($hours >= 15 && $hours <= 21){

                /**Cek user poin from join zoom dan created_at jam dan hari ini*/
                $poin = DB::table('user_poins')
                        ->where('user_id', Auth::id())
                        ->where('from', 'Join Zoom')
                        ->whereDate('created_at', Carbon::today())
                        ->where(DB::raw("HOUR(created_at)"), $hours)
                        ->first();
                if(!$poin){
                    DB::table('user_poins')->insert([
                        'user_id' => Auth::id(),
                        'poin' => 1000,
                        'from' => 'Join Zoom',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    
                    /**Update total baper poin */

                    $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
                    $newPoint = $baperPoin + 1000;
                    DB::table('users')->where('id', Auth::id())->update([
                        'baper_poin' => $newPoint
                    ]);
                }
            }
            return redirect('https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09');
        }
        else{
            return redirect(ENV('APP_URL').'/login?APP=true&&link=https://us02web.zoom.us/j/86824115301?pwd=eGNyS2NaUURZK0tueXQzMlhMdDBndz09');
        }
    }
}
