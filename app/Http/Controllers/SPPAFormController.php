<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class SPPAFormController extends Controller
{

  public function getFormPage(Request $request){
    return view('views.lab.sppa-form');
  }

}
