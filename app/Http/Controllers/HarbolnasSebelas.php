<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class HarbolnasSebelas extends Controller
{
  public function store(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'name' => 'required|min:2',
        'whatsapp' => 'required',
        'email' => 'required|email|unique:user_harbolnas_1111,email',
        'alamat' => 'required'
    ]);

    if ($validator->fails()) {
        return response()->json([
            'success' => false,
            'message' => $validator->errors()
        ], 422);
    }


    $random = Str::random(15);

    $agent = new Agent();
    $platform = $agent->platform();
    $version = $agent->version($platform);
    $browser = $agent->browser();
    $versionbrowser = $agent->version($browser);
    $whatsapp = $this->formatWhatsapp($request->whatsapp);
    $whatsapp_cs_id = $this->randomWhatsapp();

    $userId = DB::table('user_harbolnas_1111')->insertGetId([
        'nama' => $request->name,
        'alamat' => $request->alamat,
        'email' => $request->email,
        'status' => 'new',
        'ip_address' => \Request::ip(),
        'device' => $agent->device(),
        'platform' => $agent->platform(),
        'version_platform' => $version,
        'browser'  => $agent->browser(),
        'version_browser' => $versionbrowser,
        'languages' => $agent->languages(),
        'created_at' => now(),
        'no_wa' => $whatsapp,
        'provinsi' => $request->provinsi,
        'order_id' => $random,
        'from' => $request->from,
        'whatsapp_cs_id' => $whatsapp_cs_id,
        'usia' => $request->usia
    ]);

    $cookie = $_COOKIE['footprints'];

    if ($cookie != NULL) {
        // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
        // $decryptedString = $encrypter->decrypt($cookie);
        $decryptedString = \Crypt::decrypt($cookie, false);
        $cookies = explode("|", $decryptedString);
        // dd($decryptedString, $cookies);
        $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
            'user_harbolnas_sebelas' => $userId
        ]);
    }
    // return $cookie;

    $whatsapp = DB::table('user_harbolnas_1111')->join('whatsapp_cs', 'whatsapp_cs.id', 'user_harbolnas_1111.whatsapp_cs_id')->where('user_harbolnas_1111.id', $userId)->first()->whatsapp;
    return response()->json([
        'success' => true,
        'message' => 'Berhasil menambahkan data',
        'orderId' => $random,
        'whatsapp_admin' => $whatsapp
    ]);

  }

  public function randomWhatsapp(){
    $whatsapp = DB::table('whatsapp_cs')->where('status', 1)->get();
    $lastId = DB::table('user_harbolnas_1111')->orderByDesc('created_at')->first()->whatsapp_cs_id;
    $index = $whatsapp->where('id', $lastId);
    foreach ($index as $key => $value) {
      $indexKe = $key;
    }
    $nextKey = $indexKe + 1;
    $use = $whatsapp[$nextKey];
    if($use == null){
      return DB::table('whatsapp_cs')->where('status', 1)->first()->id;
    }
    else{
      return $use->id;
    }
    // return $use->id;
  }

  private function formatWhatsapp($whatsapp){
    $whatsapp = str_replace('-', '', $whatsapp);
    $whatsapp = str_replace(' ', '', $whatsapp);
    $whatsapp = str_replace(' ', '', $whatsapp);
    $whatsapp = str_replace('.', '', $whatsapp);
    $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
    $check_number = str_split($whatsapp);
    $new_number = "62";

    if ($check_number[0] == '0') {
        foreach ($check_number as $n => $number) {
            if ($n > 0) {
                if ($check_number[1] == '8') {
                    $new_number .= $number;
                } else {
                    $new_number = '-';
                }
            }
        }
    } else {
        if ($check_number[0] == '8') {
            $new_number = "62" . $whatsapp;
        } elseif ($check_number[0] == '6') {
            $new_number = $whatsapp;
        } elseif ($check_number[0] == '+') {
            foreach ($check_number as $n => $number) {
                if ($n > 2) {
                    $new_number .= $number;
                }
            }
        } else {
            $new_number = '-';
        }
    }

    return $new_number;

  }
}
