<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;


class SpecialAuctionController extends Controller
{
    public function specialAuction(Request $request)
    {
        $setting = DB::table('auction_special_settings')->first();
        $date = date('Y-m-d H:i:s');
        $time = date('H:i:s');
        $riwayatLelang = DB::table('auction_specials')
        ->select('auction_specials.id', 'auction_specials.name', 'auction_specials.photo', 'auction_specials.starttime', 'users.name as username', 'auction_special_bids.bid_price as bidTertinggi')
        ->leftJoin('auction_special_bids', 'auction_special_bids.auction_id', 'auction_specials.id')
        ->join('users', 'users.id', 'auction_special_bids.user_id')
        ->groupBy('auction_specials.id')
        ->where('auction_special_bids.winner_status', 1)
        ->where('auction_specials.end_date', '<', $date)
        ->orderByDesc('auction_specials.id')
        ->get();
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($riwayatLelang);
        $perPage = 4;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $riwayatLelang = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);

        if ($request->ajax()) {
            $view = view('dashboard.pages.showmore.riwayatlelang',compact('riwayatLelang'))->render();
            return response()->json(['html'=>$view]);
        }

        $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
        $data = DB::table('auction_specials')
            ->where('status', 1)
            ->where('winner_status', 0)
            // ->whereRaw("STR_TO_DATE('%Y-%m-%d %H:%i:%s', starttime) <= $date")
            ->where(function ($query) {
                $query->where('auction_specials.starttime', '<=', now('Asia/Jakarta'));
            })
            ->where(function ($query) {
                $query->where('auction_specials.end_date', '>=', now('Asia/Jakarta'));
            })
            ->first();

        // change space into T
        if ($data) {
            $data->end_date = preg_replace('/\s+/', 'T', $data->end_date);
        }

        $photos = DB::table('auction_special_photos')->where('auction_id', $data->id)->limit(3)->get();
        $leaderBoards = DB::table('auction_special_bids')
            ->select('auction_special_bids.created_at', 'users.name', 'auction_special_bids.bid_price', 'auction_special_bids.user_id')
            ->where('auction_special_bids.auction_id', $data->id)
            ->leftJoin('users', 'users.id', 'auction_special_bids.user_id')
            ->limit(10)
            ->where('status_bid', 1)
            ->orderByDesc('bid_price')
            ->get();
        $highestBid = DB::table('auction_special_bids')->where('status_bid', 1)->where('auction_special_bids.auction_id', $data->id)->max('bid_price');
        $lelangMendatang = DB::table('auction_specials')
            ->select('auction_specials.name', 'auction_specials.photo', 'users.name as username', DB::raw('MAX(auction_special_bids.bid_price) as bidTertinggi'))
            ->leftJoin('auction_special_bids', 'auction_special_bids.auction_id', 'auction_specials.id')
            ->join('users', 'users.id', 'auction_special_bids.user_id')
            ->groupBy('auction_specials.id')
            ->whereDate('auction_specials.starttime', '>', $date)
            ->where('auction_special_bids.status_bid', 1)
            ->limit(3)
            ->get();


            if ($poin > $setting->baper_poin_maksimal) {
                return redirect()->route('auctionPage');
            }

        return view('dashboard.pages.special-auction', compact('data', 'photos', 'leaderBoards', 'highestBid', 'lelangMendatang', 'riwayatLelang', 'poin', 'setting'));
    }
}
