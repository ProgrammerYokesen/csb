<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GachaController extends Controller
{
    public function tukarInvoice()
    {
        $riwayats = DB::table('ticket')
                    ->select('category_gacha.cat_name', 'ticket.created_at')
                    ->where('ticket.user_id', Auth::id())
                    ->whereMonth('ticket.created_at', date('m'))
                    ->orderByDesc('ticket.created_at')
                    ->join('category_gacha', 'category_gacha.id', 'ticket.category_gatcha_id')
                    ->get();
        return view('dashboard.pages.gacha.invoice', compact('riwayats'));
    }

    public function gachaTicket()
    {
        $penukaran = DB::table('gachas')
            ->where('user_id', Auth::id())
            ->sum('qty');
        $gacha = DB::table('ticket')->where('user_id', Auth::id())->count();
        $sisa = $penukaran - $gacha;
        // dd($gacha, $sisa, $penukaran);
        if($sisa == 0){
            return \Redirect::back()->with('error', 1);
        }
        $riwayats = DB::table('ticket')
                    ->select('category_gacha.cat_name', 'ticket.created_at')
                    ->where('ticket.user_id', Auth::id())
                    ->whereMonth('ticket.created_at', date('m'))
                    ->orderByDesc('ticket.created_at')
                    ->join('category_gacha', 'category_gacha.id', 'ticket.category_gatcha_id')
                    ->get();
        return view('dashboard.pages.gacha.gacha-ticket', compact('sisa', 'riwayats'));
    }

    public function chooseWheel()
    {
        return view('dashboard.pages.gacha.choose-wheel');
    }

    public function spinWheel()
    {
        $ticketPremium = DB::table('ticket')
            ->where('user_id', Auth::id())
            ->where('category_gatcha_id', 1)
            ->where('spinwheel_status', 0)
            ->count();
        $ticketReguler = DB::table('ticket')
            ->where('user_id', Auth::id())
            ->where('category_gatcha_id', 2)
            ->where('spinwheel_status', 0)
            ->count();
        $premiumPrize = DB::table('spinwheel_list_poin')->select('point')->where('cat_id', 1)->where('status', 1)->get();
        $regularPrize = DB::table('spinwheel_list_poin')->select('point')->where('cat_id', 2)->where('status', 1)->get();

        $riwayats = DB::table('user_poins')
                    ->where(function($query){
                        $query->where('from', 'Spin It Yourself - Reguler')
                        ->orWhere('from', 'Spin It Yourself - Premium');
                    })
                    ->whereMonth('created_at', date('m'))
                    ->where('user_id', Auth::id())
                    ->orderByDesc('created_at')
                    ->get();
        return view('dashboard.pages.gacha.winwheel', compact('ticketPremium', 'ticketReguler', 'premiumPrize', 'regularPrize', 'riwayats'));
    }

    public function adminWheel()
    {
        // $ticketPremium = DB::table('ticket')
        //     ->where('user_id', Auth::id())
        //     ->where('category_gatcha_id', 1)
        //     ->where('spinwheel_status', 0)
        //     ->count();
        // $ticketReguler = DB::table('ticket')
        //     ->where('user_id', Auth::id())
        //     ->where('category_gatcha_id', 2)
        //     ->where('spinwheel_status', 0)
        //     ->count();
        $premiumPrize = DB::table('cat_gacha_points')->select('point')->where('cat_id', 1)->where('status', 1)->get();
        $regularPrize = DB::table('cat_gacha_points')->select('point')->where('cat_id', 2)->where('status', 1)->get();

        // $riwayats = DB::table('user_poins')
        //             ->where(function($query){
        //                 $query->where('from', 'Spin It Yourself - Reguler')
        //                 ->orWhere('from', 'Spin It Yourself - Premium');
        //             })
        //             ->whereMonth('created_at', date('m'))
        //             ->orderByDesc('created_at')
        //             ->get();
        return view('dashboard.pages.gacha.admin-wheel', compact('premiumPrize', 'regularPrize'));
    }

    public function handleSpin()
    {
        //Res bakal dapet dari logic backend
        $res = 90;
        // hasilnya bakal dikurangin
        // atau ditambah dengan angka random
        // range -20 s.d 20
        $val = $res + (rand(0, 20) - 20);

        return $val;
    }
}
