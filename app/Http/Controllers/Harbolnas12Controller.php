<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Harbolnas12Controller extends Controller
{
  public function whatsappCallback(Request $request)
  {
    $data = json_decode($request->getContent());
    $appPackageName = $data->appPackageName;
    // package name of messenger to detect which messenger the message comes from
    $messengerPackageName = $data->messengerPackageName;
    // name/number of the message sender (like shown in the Android notification)
    $sender = $data->query->sender;
    // text of the incoming message
    $message = $data->query->message;
    // is the sender a group? true or false
    $isGroup = $data->query->isGroup;
    // id of the AutoResponder rule which has sent the web server request
    $str = $sender;
    preg_match_all('!\d+!', $sender, $matches);
    $number = '';
    foreach($matches[0] as $k => $v)
    {
    	$number = $number.$v;
    }
    DB::table('whatsapp_inbound_messaage')->insert([
      'whatsapp_number' => $number,
      'message' => $message
    ]);
    $code = substr($message, -10);
    $order = DB::table('harbolnas_1212_users')->join('users', 'users.id', 'harbolnas_1212_users.user_id')->where('uuid', $code)->count();
    if($order == 0){

    }
    else {
      // code...
      $order = DB::table('harbolnas_1212_users')->join('users', 'users.id', 'harbolnas_1212_users.user_id')->where('uuid', $code)->first();
      if($order->whatsapp_verifiation == 0)
      {
        DB::table('users')->where('id', $order->user_id)->update([
          'whatsapp' => $number,
          'whatsapp_verification' => 1,
          'whatsapp_verification_at' => now(),
          'whatsapp_verification_ip_address' => \Request::ip()
        ]);
      }
    }
    return response()->json(compact('order'));
  }

  public function saveKota(Request $request)
  {
    $validator = \Validator::make($request->all(), [
        'kota' => 'required|string|max:255',
        'alamat_rumah' => 'required|string',
    ]);
    if ($validator->fails()) {
        return response()->json([
          'status' => 'failed',
          'message' => $validator->errors()
        ], 400);
    }
    $whatsapp_cs_id = $this->randomWhatsapp();
    if(DB::table('harbolnas_1212_users')->where('whatsapp_cs_id', null)->where('user_id', Auth::id())->count() == 1)
    {
      DB::table('users')->where('id', Auth::id())->update([
        'kota' => $request->kota,
        'alamat_rumah' => $request->address,
        'kode_pos' => $request->kode_pos,
        'updated_at' => now()
      ]);

      DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->update([
        'whatsapp_cs_id' => $whatsapp_cs_id,
        'name' => $request->name
      ]);
    }
    $order = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->first();
    $nomor = DB::table('whatsapp_cs')->where('id', $order->whatsapp_cs_id)->first();

    return redirect('https://api.whatsapp.com/send/?phone=' . $nomor->whatsapp . "&text=Halo+Admin%2C+Saya+mau+ikutan+Event+Special+Harbolnas+12.12+dan+bayar+Rp100.000+agar+bisa+dapet+kesempatan+mengikuti+workshop+tentang+Reseller+dan+main+game+spesial+12.12%0D%0A%0D%0AIni+pesanan+Bunda+ya%3A%0D%0A-+1+Mystery+Box%0D%0A%0D%0ATerima+kasih+banyak%2C+Min%21%0D%0A%0D%0Aorderidharbolnas+%3D+$order->uuid&app_absent=0");
  }

  public function randomWhatsapp(){
    $whatsapp = DB::table('whatsapp_cs')->where('status', 1)->get();
    $lastId = DB::table('harbolnas_1212_users')->where('whatsapp_cs_id', '!=', null)->orderByDesc('created_at')->first();
    if($lastId)
    {
      $lastId = $lastId->whatsapp_cs_id;
      $index = $whatsapp->where('id', $lastId);
      foreach ($index as $key => $value) {
        $indexKe = $key;
      }
      $nextKey = $indexKe + 1;
      $use = $whatsapp[$nextKey];
      if($use == null){
        return DB::table('whatsapp_cs')->where('status', 1)->first()->id;
      }
      else{
        return $use->id;
      }
    }
    else {
      return DB::table('whatsapp_cs')->where('status', 1)->first()->id;
    }
    // return $use->id;
  }

  public function labSaveKota(Request $request)
  {
    $validator = \Validator::make($request->all(), [
        'kota' => 'required|string|max:255',
        'alamat_rumah' => 'required|string',
    ]);
    if ($validator->fails()) {
        return response()->json([
          'status' => 'failed',
          'message' => $validator->errors()
        ], 400);
    }
    $whatsapp_cs_id = $this->randomWhatsapp();
    if(DB::table('harbolnas_1212_users')->where('whatsapp_cs_id', null)->where('user_id', Auth::id())->count() == 1)
    {
      DB::table('users')->where('id', Auth::id())->update([
        'kota' => $request->kota,
        'alamat_rumah' => $request->address,
        'kode_pos' => $request->kode_pos,
        'updated_at' => now()
      ]);

      DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->update([
        'whatsapp_cs_id' => $whatsapp_cs_id,
        'name' => $request->name
      ]);
    }
    $order = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->first();
    $nomor = DB::table('whatsapp_cs')->where('id', $order->whatsapp_cs_id)->first();
    $link = 'https://api.whatsapp.com/send/?phone=' . $nomor->whatsapp . "&text=Halo+Admin%2C+Saya+mau+ikutan+Event+Special+Harbolnas+12.12+dan+bayar+Rp100.000+agar+bisa+dapet+kesempatan+mengikuti+workshop+tentang+Reseller+dan+main+game+spesial+12.12%0D%0A%0D%0AIni+pesanan+Bunda+ya%3A%0D%0A-+1+Mystery+Box%0D%0A%0D%0ATerima+kasih+banyak%2C+Min%21%0D%0A%0D%0Aorderidharbolnas+%3D+$order->uuid&app_absent=0";
    return response()->json([
      'redirect_link' => $link,
    ]);
  }

  public function joinGame($eventId)
  {
    $count = DB::table('harbolnas_1212_event_users')->where('user_id', Auth::id())->where('event_id', $eventId)->count();
    if($count == 0)
    {
      DB::table('harbolnas_1212_event_users')->insert([
        'user_id' => Auth::id(),
        'event_id' => $eventId,
        'created_at' => now(),
        'updated_at' => now()
      ]);
    }
    return redirect()->back()->with('success_regist', true);
  }

}
