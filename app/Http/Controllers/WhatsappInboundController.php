<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class WhatsappInboundController extends Controller
{
  public function inbound(Request $request)
  {
    $data = json_decode($request->getContent());
    $appPackageName = $data->appPackageName;
    // package name of messenger to detect which messenger the message comes from
    $messengerPackageName = $data->messengerPackageName;
    // name/number of the message sender (like shown in the Android notification)
    $sender = $data->query->sender;
    // text of the incoming message
    $message = $data->query->message;
    // is the sender a group? true or false
    $isGroup = $data->query->isGroup;
    // id of the AutoResponder rule which has sent the web server request
    $ruleId = $data->query->ruleId;
    $verify   = 'verify';
    $registerQuiz = 'kuisbundafita';
    $checkVerify = strpos($message, $verify);
    $checkQuiz = strpos($message, $registerQuiz);

    $str = $sender;
    preg_match_all('!\d+!', $sender, $matches);
    $number = '';
    foreach($matches[0] as $k => $v)
    {
    	$number = $number.$v;
    }

    // The !== operator can also be used.  Using != would not work as expected
    // because the position of 'a' is 0. The statement (0 != false) evaluates
    // to false.

    if ($checkVerify !== false) {
      $verifyID = explode('verify ', $message);
      $verify = $verifyID[1];

      //   $insert = DB::table('whatsapp')->insert([
      //     'content' => $appPackageName.'|'.$messengerPackageName.'|'.$sender.'|'.$message.'|'.$isGroup.'|'.$ruleId,
      //     'phone' => $sender,
      //     'message' => $message
      //   ]);

      $user = DB::table('users')->where('referral_code', $verify)->first();
      $checkWhatsapp = DB::table('users')->where('whatsapp', $sender)->where('whatsapp_verification', 1)->count();
      $checkNumber = DB::table('users')->where('whatsapp', $number)->where('whatsapp_verification', 1)->count();

      if (!empty($user)) {
        if ($checkWhatsapp != 0 || $checkNumber != 0) {
          return json_encode(array("replies" => array(
            array("message" => "Nomer whatsapp $sender sudah terdaftar, silahkan gunakan nomer lain")
          )));
        }
        if ($user->emailValidation == "validated" && $user->ref_id != NULL && $user->whatsapp_verification == 0) {
          DB::table('user_poins')->insert([
            'user_id' => $user->ref_id,
            'fk_id' => $user->id,
            'poin' => 1000,
            'from' => 'Bawa teman sobat',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ]);

          /**Update total baper poin */

          $baperPoin = DB::table('users')->where('id', $user->ref_id)->first()->baper_poin;
          $newPoint = $baperPoin + 1000;
          DB::table('users')->where('id', $user->ref_id)->update([
              'baper_poin' => $newPoint
          ]);

          /** update teman sobat */

          $temanSobat = DB::table('users')->where('id', $user->ref_id)->first()->countRef;
          $newTemanSobat = $temanSobat + 1;
          DB::table('users')->where('id', $temanSobat->id)->update([
              'countRef' => $newTemanSobat
          ]);

          /**Update teman sobat event */
          $date = date('Y-m-d H:i:s');
          $userId = $user->ref_id;
          $activeEvent = DB::table('referral_events')
          ->where('referral_events.start_time', '<', $date)
          ->where('referral_events.end_time', '>', $date)
          ->first();
          if($activeEvent){
            if(DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()){
              /**Update */
              $count = DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()->amount;
              $count = $count + 1;
              DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->update([
                'amount' => $count,
                'updated_at' => now()
              ]);

            }
            else{
              /**Insert */
              DB::table('leaderboards_referral')->insert([
                'user_id' => $userId,
                'referral_events' => $activeEvent->id,
                'amount' => 1,
                'created_at' => now(),
                'updated_at' => now()
              ]);

            }
          }

                  /**Update teman sobat event */
          $date = date('Y-m-d H:i:s');
          $userId = $user->ref_id;
          $activeEvent = DB::table('referral_events')
          ->where('referral_events.start_time', '<', $date)
          ->where('referral_events.end_time', '>', $date)
          ->first();
          if($activeEvent){
            if(DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()){
              /**Update */
              $count = DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()->amount;
              $count = $count + 1;
              DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->update([
                'amount' => $count,
                'updated_at' => now()
              ]);

            }
            else{
              /**Insert */
              DB::table('leaderboards_referral')->insert([
                'user_id' => $userId,
                'referral_events' => $activeEvent->id,
                'amount' => 1,
                'created_at' => now(),
                'updated_at' => now()
              ]);

            }
          }

        }
        $update = DB::table('users')->where('referral_code', $verify)->update([
          'whatsapp' => $number,
          'whatsapp_verification' => 1,
          'whatsapp_verification_at' => now(),
          'whatsapp_verification_ip_address' => \Request::ip()
        ]);

        // $update = DB::table('users')->where('uuid',$verify)->update([
        //     'id_cms_privileges' => 9
        // ]);

        // $priv = DB::table("cms_privileges")->where("id", 9)->first();
        // $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', 9)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();

        // Session::put('priv', $priv);
        // Session::put('modul', $roles);

        return json_encode(array("replies" => array(
          array("message" => "Halo $user->name, terima kasih atas verifikasi whatsapp $sender di " . env('APP_NAME')),
          array("message" => "Success :white_check_mark: kode " . time() . " Sekarang silahkan kembali ke Platfrom " . env('APP_NAME') . ". Terima kasih!")
        )));

        // return redirect()->route('viewProfile');
      } else {
        return json_encode(array("replies" => array(
          array("message" => "Sepertinya kode verifikasi $verify ga valid, coba klik lagi dari website, jangan ada yang diubah ya")
        )));
      }
    }

    /**
     * Join Quiz Bunda
     */

    elseif($checkQuiz !== false) {
        // preg_match_all('!\d+!', $sender, $matches);
        // $no_wa = "";
        // foreach($matches[0] as $key => $value){
        // $no_wa = "$no_wa"."$value";
        // }

        // $checkNomer = DB::table('user_harbolnas_1010')->where('no_wa', $no_wa)->count();
        // // $user = DB::table('user_harbolnas_1010')->where('no_wa', $no_wa)->first();

        // if($checkNomer != 0){
        //     $user = DB::table('user_harbolnas_1010')->where('no_wa', $no_wa)->where('status', '!=', 'new')->first();
        //   if($user->status != 'new'){
        //     $createToken = \Str::random(255);
        //     DB::table('user_harbolnas_1010')->where('no_wa', $no_wa)->update([
        //       'token' => $createToken
        //     ]);
        //     return json_encode(array("replies" => array(
        //       array("message" => "Halo $user->nama Selamat datang di Club Warisan Berikut adalah kuis untuk Day 2 Workshop Reseller hari ini  ".env('APP_URL')."/quiz-bunda-hebat?token=".$createToken." Silakan jawab pertanyaan-pertanyaan di bawah ini secara tepat ya Bunda Semoga beruntung")
        //       // array("message" => "Halo $user->nama <br>Selamat datang di Club Warisan! <br> Berikut adalah kuis untuk Day 1 - Workshop Reseller hari ini <br>".env('APP_URL')."/post-quiz-bunda?token=".$createToken."<br>Silakan jawab pertanyaan-pertanyaan di bawah ini secara tepat, ya Bunda! <br>Semoga beruntung!")
        //     )));
        //   }
        //   else{
        //     return json_encode(array("replies" => array(
        //       array("message" => "Status tidak join Contact Admin")
        //     )));
        //   }
        // }
        // else{
        //   return json_encode(array("replies" => array(
        //     array("message" => "Nomor tidak terdaftar untuk mengikuti event ini contact admin")
        //   )));
        // }
    }


  }
}
