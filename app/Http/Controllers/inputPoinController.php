<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class inputPoinController extends Controller
{
    public function searchEmail(Request $request){
          if ($request->has('q')) {
    		$cari = $request->q;
    		$data = DB::table('users')->select('id', 'email')->where('email', 'LIKE', $cari.'%')->get();
    		return response()->json($data);
    	}
    }
    public function spinwheelPoin(Request $request){
        $count = count($request->cat_spinwheel);
        for($i=1;$i<=$count;$i++){
                  DB::table('user_poins')->insert([
                  'user_id' => $request->cari,
                  'fk_id' => $request->orderId,
                  'poin' => $request->poin[$i],
                  'from' => $request->cat_spinwheel[$i],
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s')
              ]);
        }
        DB::table('orders')->where('invoice', $request->invoice)->update([
            'status_spinwheel' => 'joined',
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect(ENV('app_url').'/'.'admin/order');
    }
}
