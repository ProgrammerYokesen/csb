<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResellerController extends Controller
{
    public function homeReseller()
    {
        return view('dashboard.pages.reseller.home');
    }

    public function modulReseller()
    {
        return view('dashboard.pages.reseller.module');
    }

    public function modulDetailReseller()
    {
        return view('dashboard.pages.reseller.module-detail');
    }

    public function modulQuizReseller()
    {
        return view('dashboard.pages.reseller.module-quiz');
    }

    public function modulQuizDone()
    {
        return view('dashboard.pages.reseller.module-quiz-done');
    }

    public function stempelReseller()
    {
        return view('dashboard.pages.reseller.stempel');
    }

    public function leaderboardReseller()
    {
        return view('dashboard.pages.reseller.leaderboard');
    }

    public function jadwalReseller()
    {
        return view('dashboard.pages.reseller.jadwal');
    }

    public function riwayatReseller()
    {
        return view('dashboard.pages.reseller.riwayat');
    }

    public function landingReseller()
    {
        return view('landing-page.pages.reseller');
    }
}
