<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class undianController extends Controller
{
    public function index(Request $request){
        if($request->has('date')){
            $datas = DB::table('users as u')
            ->leftJoin('users as t', 't.ref_id', 'u.id')
            ->select('u.id', 'u.email', 'u.name', 'u.ref_id', 't.name as userDiInvite', 't.undian')
            ->whereDate('t.created_at', $request->date)
            ->get();
            return view('admin.undian', compact('datas'));
        }
        $datas = DB::table('users as u')
                ->leftJoin('users as t', 't.ref_id', 'u.id')
                ->select('u.id', 'u.email', 'u.name', 'u.ref_id', 't.name as userDiInvite', 't.undian')
                ->where('u.spam', 0)
                ->where('t.spam', 0)
                ->whereDate('t.created_at', Carbon::today())
                ->get();
        return view('admin.undian', compact('datas'));
    }
    
    public function getData(Request $request){
        $datas = DB::table('users as u')
                ->leftJoin('users as t', 't.ref_id', 'u.id')
                ->select('u.name', 'u.email' ,'t.undian')
                ->where('u.spam', 0)
                ->where('t.spam', 0)
                ->whereDate('t.created_at', Carbon::today())
                ->get();
        foreach($datas as $value){
            echo $value->name.' - '.$value->email.'-'.$value->undian.'<br>';
        }
    }
}
