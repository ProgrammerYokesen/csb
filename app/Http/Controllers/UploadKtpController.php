<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;

class UploadKtpController extends Controller
{
    public function post(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            // 'city' => 'required|string',
            'kode_pos' => 'required|numeric',
            'no_wa' => 'required|string|min:6',
            'nik' => 'required|numeric',
        ]);


        // dd($request->all());
        if ($validator->fails()) {
            // dd($validator->fails(), $validator);
            return \Redirect::back()->withErrors($validator);
        }

        $user_id = auth()->user()->id;

        // Check NIK if exists
        $data['nik'] = $request->nik;
        $exitsNIK = DB::table('users')
                    ->where('nik', $request->nik)
                    ->where('id', '<>', $user_id)
                    ->first();
        if ($exitsNIK) {
            if ($exitsNIK->ktp_verification == 1) {
                return redirect()->route('editProfile')->with("errorMsg", "NIK Sudah terdaftar!");
            }
        }

        if($request->ktp){
            $ktp_name = auth()->user()->username . time() . '.' . $request->file('ktp')->getClientOriginalExtension();
            $data['ktp'] = $ktp_name;
            $data['ktp_verification'] = 0;
        }
        // Check for unique email
        // $checkEmail = DB::table('users')
        //     ->where('id', '<>', $user_id)
        //     ->where('email', $request->email)
        //     ->count();

        // if ($checkEmail > 0) {
        //     return \Redirect::back()->with("email", "Email tidak valid!");
        // }

        // Parse no wa to number
        $whatsapp = $request->no_wa;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);

        if ($whatsapp[0] == '0') {
            if ($whatsapp[1] == '8') {
                $whatsapp = ltrim($whatsapp, "0");;
                $whatsapp = "62" . $whatsapp;
            } else {
                $whatsapp = '-';
            }
        } else {
            if ($whatsapp[0] == '8') {
                $whatsapp = "62" . $whatsapp;
            } elseif ($whatsapp[0] == '6' && $whatsapp[1] == '2') {
                $whatsapp;
            } elseif ($whatsapp[0] == '+') {
                if ($whatsapp[1] == '6' && $whatsapp[2] == '2') {
                    $whatsapp = str_replace("+", "", $whatsapp);
                } else {
                    $whatsapp = '-';
                }
            } else {
                $whatsapp = '-';
            }
        }

        // check is WA valid
        if ($whatsapp == "-") {
            return \Redirect::back()->with("no_wa", "Nomor tidak valid!");
        }
        if($request->no_wa == Auth::user()->whatsapp){
            date_default_timezone_set("Asia/Jakarta");
            $data['name'] = $request->name;
            $data['kota'] = $request->city;
            $data['alamat_rumah'] = $request->alamat;
            $data['kode_pos'] = $request->kode_pos;
            $data['updated_at'] = date('Y-m-d H:i:s');

            DB::table('users')
                ->where('id', $user_id)
                ->update($data);
            // DB::table('users')
            //     ->where('id', $user_id)
            //     ->update([
            //         'name' => $request->name,
            //         'kota' => $request->city,
            //         'alamat_rumah' => $request->alamat,
            //         'kode_pos' => $request->kode_pos,
            //         'nik' => $request->nik,
            //         'ktp' => $ktp_name,
            //         'ktp_verification' => 0,
            //         'updated_at' => date('Y-m-d H:i:s')
            //     ]);

            // put ktp on storage if success
            if($request->ktp){
              Storage::putFileAs('public/ktp', $request->ktp, $ktp_name);
              $file = $request->file('ktp');
              $path = $file->storeAs('public/ktp', $ktp_name);
            }

            $names = explode(' ', trim($request->name));
            Session::put('user_name', $names[0]);
            return redirect()->route('editProfile')->with("success", "Berhasil memperbarui profil!");
        }

        $checkWA = DB::table('users')
            ->where('id', '!=', Auth::id())
            ->where('whatsapp_verification', 1)
            ->where('whatsapp', $whatsapp)
            ->count();

        if ($checkWA > 0) {
            return \Redirect::back()->with("no_wa", "Nomor sudah dipakai!");
        }

        date_default_timezone_set("Asia/Jakarta");
        $data['name'] = $request->name;
        $data['kota'] = $request->city;
        $data['alamat_rumah'] = $request->alamat;
        $data['kode_pos'] = $request->kode_pos;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['whatsapp'] = $whatsapp;

        DB::table('users')
            ->where('id', $user_id)
            ->update($data);
        // DB::table('users')
        //     ->where('id', $user_id)
        //     ->update([
        //         'name' => $request->name,
        //         'kota' => $request->city,
        //         'alamat_rumah' => $request->alamat,
        //         'kode_pos' => $request->kode_pos,
        //         'whatsapp' => $whatsapp,
        //         'nik' => $request->nik,
        //         'ktp' => $ktp_name,
        //         'ktp_verification' => 0,
        //         'updated_at' => date('Y-m-d H:i:s')
        //     ]);
        // put ktp on storage if success
        if($request->ktp){
          Storage::putFileAs('public/ktp', $request->ktp, $ktp_name);
          $file = $request->file('ktp');
          $path = $file->storeAs('public/ktp', $ktp_name);
        }

        $names = explode(' ', trim($request->name));
        Session::put('user_name', $names[0]);
        return redirect()->route('editProfile')->with("success", "Berhasil memperbarui profil!");
    }
}
