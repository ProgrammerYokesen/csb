<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class QuizBundaController extends Controller
{
    public function homeBunda(Request $request)
    {
        /**
         * Check Token
         */

         if(!DB::table('user_harbolnas_1010')->where('token', $request->token)->first()){
             abort(401);
         }

        /**
         * Save Token
         */

        $token = $request->token;
        $request->session()->put('quizToken', $token);

        /**
         * Get Quiz Data
         */

        $quiz = DB::table('quiz_bunda')->where('id', 3)->first();
        $soals = DB::table('quiz_bunda_soals')->where('quiz_bunda_id', $quiz->id)->get();
        return view('landing-page.quiz-bunda.module', compact('quiz', 'soals'));
    }

    public function postQuiz(Request $request)
    {
        /**
         * Check Token
         */
        if(!$request->has('token')){
            abort(401);
        }
        $exist = DB::table('user_harbolnas_1010')->where('token', $request->token)->count();
        if($exist == 0){
            abort(401);
        }

       /**
        * Save Token
        */

       $token = $request->token;
       $request->session()->put('quizToken', $token);

       /**
        * Get Quiz Data
        */

       $quiz = DB::table('quiz_bunda')->where('id', 3)->first();
       $soals = DB::table('quiz_bunda_soals')->where('quiz_bunda_id', $quiz->id)->get();

        /**
        * Check If user already doing the quiz
        */
        $user = DB::table('user_harbolnas_1010')->where('status', '!=', 'new')->where('token', $request->token)->first();
        $count = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)->where('quiz_bunda_id', $quiz->id)->where('user_id', $user->id)->count();
        return view('landing-page.quiz-bunda.quiz-bunda-end');
        if($count != 0){
            return redirect()->route('postQuizDone');
        }

        return view('landing-page.quiz-bunda.module-quiz', compact('soals','quiz'));
    }

    public function postQuizDone()
    {
        $user = DB::table('user_harbolnas_1010')->where('status', '!=', 'new')->where('token', \Session::get('quizToken'))->first();
        $quiz = DB::table('quiz_bunda')->where('id', 3)->first();
        $hasil = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)->where('user_id', $user->id)->where('quiz_bunda_id', $quiz->id)->first();
        return view('landing-page.quiz-bunda.module-quiz-done', compact('hasil'));
    }

    public function processQuiz(Request $request){
        // dd($request);
        $temp_poin = 0;
        $user = DB::table('user_harbolnas_1010')->where('status', '!=', 'new')->where('token', \Session::get('quizToken'))->first();
        if(!$user){
            abort(401);
        }
        $quiz = DB::table('quiz_bunda')->where('id', 3)->first();

        foreach ($request->except(['_token']) as $key => $value) {
            $answerStatus = $this->checkAnswer($key, $value);
            if($answerStatus == 1){
                $temp_poin++;
            }
          }
        $countSoal = DB::table('quiz_bunda_soals')->where('quiz_bunda_id', $quiz->id)->count();
        $nilai = $temp_poin / $countSoal * 100;
        DB::table('quiz_bunda_starts')->insert([
            'user_id' => $user->id,
            'quiz_bunda_id' => $quiz->id,
            'start_date' => now(),
            'status_pengerjaan' => 1,
            'nilai' => $nilai,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        return redirect()->route('postQuizDone');
    }

    public function checkAnswer($key, $value){
        $soal = DB::table('quiz_bunda_soals')->where('id', $key)->first();
        if($soal->jawaban_benar == $value){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    public function leaderboardBundaHebat()
    {
        $dataLeaderboard = DB::table('quiz_bunda_leaderboards')->where('version', 1)->select('quiz_bunda_leaderboards.time', 'user_harbolnas_1010.nama', 'quiz_bunda_leaderboards.poin')->join('user_harbolnas_1010', 'user_harbolnas_1010.id', 'quiz_bunda_leaderboards.user_id')->orderBy('time', 'asc')->get();
        // dd($dataLeaderboard);
        return view('landing-page.quiz-bunda.leaderboard', compact('dataLeaderboard'));
    }
    
    public function leaderboardBunda1()
    {
        $dataLeaderboard = DB::table('quiz_bunda_leaderboards')->where('version', 1)->select('quiz_bunda_leaderboards.time', 'user_harbolnas_1010.nama', 'quiz_bunda_leaderboards.poin')->join('user_harbolnas_1010', 'user_harbolnas_1010.id', 'quiz_bunda_leaderboards.user_id')->orderBy('time', 'asc')->get();
        // dd($dataLeaderboard);
        return view('landing-page.quiz-bunda.leaderboard-1', compact('dataLeaderboard'));
    }
    
    public function leaderboardBunda2()
    {
        $dataLeaderboard = DB::table('quiz_bunda_leaderboards_2')->select('quiz_bunda_leaderboards_2.time', 'user_harbolnas_1010.nama', 'quiz_bunda_leaderboards_2.poin')->join('user_harbolnas_1010', 'user_harbolnas_1010.id', 'quiz_bunda_leaderboards_2.user_id')->orderBy('time', 'asc')->get();
        // dd($dataLeaderboard);
        return view('landing-page.quiz-bunda.leaderboard-2', compact('dataLeaderboard'));
    }
}
