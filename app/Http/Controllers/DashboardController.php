<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function homeDash()
    {
        // dd(session()->all());
        $date = date('Y-m-d H:i:s');
        $temanSobat = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->count();
        $temanSobats = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->take(5)->get();
        $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');

        $tebakKataPoin = DB::table('quiz_tebak_kata')
            ->leftJoin('quiz_tebak_kata_list', 'quiz_tebak_kata_list.quiz_tebak_kata_id', 'quiz_tebak_kata.id')
            ->leftJoin('tebak_kata', 'tebak_kata.id', 'quiz_tebak_kata_list.pertanyaan_id')
            ->where('quiz_tebak_kata.status', 1)
            ->whereDate('quiz_tebak_kata.start_date', '<', $date)
            ->whereDate('quiz_tebak_kata.end_date', '>', $date)
            ->sum('poin');

        $todayMissionPoin = DB::table('today_missions')->whereDate('date', date('Y-m-d'))->sum('poin');

        $rejekiSobatPoin = DB::table('quiz_rejeki_sobat')
            ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d')
            ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
            ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->sum('poin');

        $todayPoin = $tebakKataPoin + $todayMissionPoin + $rejekiSobatPoin;

        $todayMissions = DB::table('today_missions')
            ->whereDate('date', date('Y-m-d'))
            ->get();
        $riwayats = DB::table('user_poins')->where('user_id', Auth::id())->where('poin', '!=', 0)->whereMonth('created_at', date('m'))->orderByDesc('created_at')->limit(5)->get();

        $activeTebakKata = DB::table('quiz_tebak_kata')
            ->where('quiz_tebak_kata.status', 1)
            ->whereDate('quiz_tebak_kata.start_date', '<', $date)
            ->whereDate('quiz_tebak_kata.end_date', '>', $date)
            ->first();
        $centangTebakKata = 0;
        if ($activeTebakKata) {
            $centangTebakKata = DB::table('jawaban_tebak_kata')->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $activeTebakKata->id)->count();
        }
        $activeRejekiSobat = DB::table('quiz_rejeki_sobat')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->first();
        $centangRejekiSobat = 0;
        if ($activeRejekiSobat) {
            $centangRejekiSobat = DB::table('jawaban_users')->where('user_id', Auth::id())->where('quiz_id', $activeRejekiSobat->id)->count();
        }
        $fullRiwayats = DB::table('user_poins')
            ->where('user_id', Auth::id())
            ->orderByDesc('created_at')
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->get();


        // Check ada lelang berjalan atau tidak
        $ongoingLelang = DB::table('auctions')
            ->where('status', 1)
            ->where('winner_status', 0)
            ->where(function ($query) {
                $query->where('auctions.starttime', '<=', now('Asia/Jakarta'));
            })
            ->where(function ($query) {
                $query->where('auctions.end_date', '>=', now('Asia/Jakarta'));
            })
            ->first();
            if (env('APP_ENV') != "local") {
                $banners = DB::table('banners')->where('status', 1)->get();
            }
        return view('dashboard.pages.home', compact('temanSobat', 'temanSobats', 'poin', 'todayPoin', 'tebakKataPoin', 'rejekiSobatPoin', 'todayMissions', 'riwayats', 'centangTebakKata', 'centangRejekiSobat', 'fullRiwayats', 'ongoingLelang', 'banners'));
    }

    public function temanSobat()
    {
        $date = date('Y-m-d H:i:s');
        $temanSobat = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->count();
        // $temanSobats = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->get();
        $temanSobats = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->paginate(10);
        $activeEvent = DB::table('referral_events')
        ->where('referral_events.start_time', '<', $date)
        ->where('referral_events.end_time', '>', $date)
        ->first();
        if($activeEvent){
            $data = DB::table('leaderboards_referral')->join('users', 'users.id', 'leaderboards_referral.user_id')->where('leaderboards_referral.status', 1)->where('referral_events', $activeEvent->id)->orderByDesc('amount')->take(20)->get();
        }
        else{
            $data = array();
            // $data = DB::table('leaderboards_referral')->join('users', 'users.id', 'leaderboards_referral.user_id')->where('leaderboards_referral.status', 1)->where('referral_events', 3)->orderByDesc('amount')->take(20)->get();
        }
        return view('dashboard.pages.teman-sobat', compact('temanSobat', 'temanSobats', 'data'));
    }

    public function tebakKata()
    {
        $totalPoin = 0;
        $date = date('Y-m-d H:i:s');
        $quiz = DB::table('quiz_tebak_kata')
            ->where('quiz_tebak_kata.status', 1)
            ->whereDate('quiz_tebak_kata.start_date', '<=', $date)
            ->whereDate('quiz_tebak_kata.end_date', '>=', $date)
            ->count();
        // dd($quiz);
        if ($quiz > 0) {
            $totalPoin = DB::table('quiz_tebak_kata')
                ->leftJoin('quiz_tebak_kata_list', 'quiz_tebak_kata_list.quiz_tebak_kata_id', 'quiz_tebak_kata.id')
                ->leftJoin('tebak_kata', 'tebak_kata.id', 'quiz_tebak_kata_list.pertanyaan_id')
                ->where('quiz_tebak_kata.status', 1)
                ->whereDate('quiz_tebak_kata.start_date', '<', $date)
                ->whereDate('quiz_tebak_kata.end_date', '>', $date)
                ->sum('poin');
        }
        $datas = DB::table('quiz_tebak_kata')
            ->select('quiz_tebak_kata.id as id', 'quiz_tebak_kata_list.pertanyaan_id', 'tebak_kata.kata', 'tebak_kata.deskripsi', 'tebak_kata.kata')
            ->leftJoin('quiz_tebak_kata_list', 'quiz_tebak_kata_list.quiz_tebak_kata_id', 'quiz_tebak_kata.id')
            ->leftJoin('tebak_kata', 'tebak_kata.id', 'quiz_tebak_kata_list.pertanyaan_id')
            ->where('quiz_tebak_kata.status', 1)
            ->whereDate('quiz_tebak_kata.start_date', '<', $date)
            ->whereDate('quiz_tebak_kata.end_date', '>', $date)
            ->paginate(1);
        /**Check pengerjaan selesai */
        $check = DB::table('jawaban_tebak_kata')->where('status_pengerjaan', 1)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->count();

        if ($check != 0) {
            $doneQuiz = true;
        }
        /**End check */



        return view('dashboard.pages.tebak-kata', compact('quiz', 'totalPoin', 'doneQuiz'));
    }

    public function rejekiSobat()
    {
        $date = date('Y-m-d H:i:s');
        $quiz = DB::table('quiz_rejeki_sobat')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->count();
        $rejekiSobatPoin = DB::table('quiz_rejeki_sobat')
            ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d', 'pertanyaan_rejeki_sobat.link')
            ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
            ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->sum('poin');
        $datas = DB::table('quiz_rejeki_sobat')
            ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d', 'pertanyaan_rejeki_sobat.link')
            ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
            ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->get();
        $answer = DB::table('jawaban_users')->where('user_id', Auth::id())
            ->where('quiz_id', $datas[0]->quizId)
            ->count();
        // dd($answer);
        if ($answer > 0) {
            $doneQuiz = true;
        }

        return view('dashboard.pages.rejeki-sobat', compact('quiz', 'rejekiSobatPoin', 'doneQuiz'));
    }

    public function winPage()
    {
        $date = date('Y-m-d H:i:s');
        $quizId = DB::table('quiz_rejeki_sobat')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->first();
        if ($quizId) {
            $answer = DB::table('jawaban_users')->where('user_id', Auth::id())
                ->where('quiz_id', $quizId->id)
                ->where('status_jawaban', 1)
                ->count();
            $check = DB::table('jawaban_users')->where('user_id', Auth::id())
                ->where('quiz_id', $quizId->id)
                ->count();
            if ($check > 0) {
                $quizSobat = true;
                $question = DB::table('quiz_rejeki_sobat_list')->where('quiz_rejeki_sobat_id', $quizId->id)
                    ->count();
                $poin = DB::table('jawaban_users')->where('jawaban_users.user_id', Auth::id())
                    ->leftJoin('pertanyaan_rejeki_sobat', 'pertanyaan_rejeki_sobat.id', 'jawaban_users.pertanyaan_id')
                    ->where('jawaban_users.quiz_id', $quizId->id)
                    ->where('jawaban_users.status_jawaban', 1)->sum('pertanyaan_rejeki_sobat.poin');
                return view('dashboard.pages.quiz-win', compact('answer', 'question', 'poin', 'quizSobat'));
            }
        }
        return redirect()->route('homeDash');
    }

    public function gameTebakKata(Request $request)
    {
        $date = date('Y-m-d H:i:s');
        $datas = DB::table('quiz_tebak_kata')
            ->select('quiz_tebak_kata.id as id', 'quiz_tebak_kata_list.pertanyaan_id', 'tebak_kata.kata', 'tebak_kata.deskripsi', 'tebak_kata.kata')
            ->leftJoin('quiz_tebak_kata_list', 'quiz_tebak_kata_list.quiz_tebak_kata_id', 'quiz_tebak_kata.id')
            ->leftJoin('tebak_kata', 'tebak_kata.id', 'quiz_tebak_kata_list.pertanyaan_id')
            ->where('quiz_tebak_kata.status', 1)
            ->whereDate('quiz_tebak_kata.start_date', '<', $date)
            ->whereDate('quiz_tebak_kata.end_date', '>', $date)
            ->paginate(1);
        /**Check pengerjaan selesai */
        $check = DB::table('jawaban_tebak_kata')->where('status_pengerjaan', 1)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->count();
        if ($check != 0) {
            $request->session()->put('quizId', $datas[0]->id);
            return redirect()->route('winPageTebakKata');
        }
        /**End check */
        /**Check pertanyaan sudah dijawab */
        $answered = DB::table('jawaban_tebak_kata')->where('status_pengerjaan', 0)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->where('pertanyaan_id', $datas[0]->pertanyaan_id)->first();
        if ($answered) {
            $page = (int)$request->page + 1;
            return redirect(env('APP_URL') . '/tebak-kata/today-quiz?page=' . $page);
        }
        /**End check */
        /**Check page lebih dari totalpage */
        $answered = DB::table('jawaban_tebak_kata')->where('status_pengerjaan', 0)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->count();
        $page = $answered + 1;
        if ($request->page > $datas->lastPage() || $request->page > $page) {
            return redirect(env('APP_URL') . '/tebak-kata/today-quiz?page=' . $page);
        }
        /**End */
        $percentage = ($datas->currentPage() / $datas->lastPage()) * 100;
        $kata = str_split($datas[0]->kata);
        $request->session()->put('quizId', $datas[0]->id);
        $request->session()->put('pertanyaanId', $datas[0]->pertanyaan_id);
        $request->session()->put('currentPage', $datas->currentPage());
        $request->session()->put('lastPage', $datas->lastPage());
        if (empty(Session::get('tebak'))) {
            $huruf = $datas[0]->kata;
            $length = strlen($huruf) - 1;
            $p = random_int(0, (int)$length);
            $huruf = str_split($huruf);
            $huruf = strtoupper($huruf[$p]);
            $tebak[0] = $huruf;
            $last = "betul";
            $nilai = 0;
            $salah = 0;
            foreach ($tebak as $value) {
                if (in_array($value, $kata)) {
                    $nilai += 1;
                } else {
                    $salah += 1;
                }
            }

            $percent_nilai = $nilai / count($kata);
            $percent_salah = ($nilai - $salah) / count($kata);
            // dd($percent_nilai);


            $request->session()->put('last', $last);
            $request->session()->put('salah', $salah);
            $request->session()->put('nilai', $percent_nilai);
            $request->session()->put('nilai_salah', $percent_salah);
            $request->session()->put('tebak', $tebak);
        }
        $tebakan = Session::get('tebak');
        // dd($datas);
        return view('dashboard.pages.tebak-kata-game', compact('datas', 'percentage', 'kata', 'tebakan'));
    }

    public function handleTebakKata()
    {
        $date = date('Y-m-d H:i:s');
        $datas = DB::table('quiz_tebak_kata')
            ->select('quiz_tebak_kata.id as id', 'quiz_tebak_kata_list.pertanyaan_id', 'tebak_kata.kata', 'tebak_kata.deskripsi', 'tebak_kata.kata')
            ->leftJoin('quiz_tebak_kata_list', 'quiz_tebak_kata_list.quiz_tebak_kata_id', 'quiz_tebak_kata.id')
            ->leftJoin('tebak_kata', 'tebak_kata.id', 'quiz_tebak_kata_list.pertanyaan_id')
            ->where('quiz_tebak_kata.status', 1)
            ->whereDate('quiz_tebak_kata.start_date', '<', $date)
            ->whereDate('quiz_tebak_kata.end_date', '>', $date)->get();
        $pengerjaan = DB::table('jawaban_tebak_kata')->where('status_pengerjaan', 0)->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $datas[0]->id)->count();
        if ($pengerjaan > 0 && $pengerjaan != $datas->count()) {
            $page = $pengerjaan + 1;
            return redirect(env('APP_URL') . '/tebak-kata/today-quiz?page=' . $page);
        } else {
            return redirect()->route('gameTebakKata');
        }
    }

    public function quizRejekiSobat()
    {
        $date = date('Y-m-d H:i:s');
        $datas = DB::table('quiz_rejeki_sobat')
            ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d', 'pertanyaan_rejeki_sobat.link')
            ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
            ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->get();
        $answer = DB::table('jawaban_users')->where('user_id', Auth::id())
            ->where('quiz_id', $datas[0]->quizId)
            ->count();
        if ($answer == 0) {
            return view('dashboard.pages.rejeki-sobat-game', compact('datas'));
        }
        return redirect()->route('winPage');
    }

    public function auctionPage(Request $request)
    {
        $date = date('Y-m-d H:i:s');
        $time = date('H:i:s');
        $riwayatLelang = DB::table('auctions')
        ->select('auctions.id', 'auctions.name', 'auctions.photo', 'auctions.starttime', 'users.name as username', 'auction_bids.bid_price as bidTertinggi')
        ->leftJoin('auction_bids', 'auction_bids.auction_id', 'auctions.id')
        ->join('users', 'users.id', 'auction_bids.user_id')
        ->groupBy('auctions.id')
        ->where('auction_bids.winner_status', 1)
        ->where('auctions.end_date', '<', $date)
        ->orderByDesc('auctions.id')
        ->get();
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($riwayatLelang);
        $perPage = 4;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $riwayatLelang = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        if ($request->ajax()) {
            $view = view('dashboard.pages.showmore.riwayatlelang',compact('riwayatLelang'))->render();
            return response()->json(['html'=>$view]);
        }
        $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
        $data = DB::table('auctions')
            ->where('status', 1)
            ->where('winner_status', 0)
            // ->whereRaw("STR_TO_DATE('%Y-%m-%d %H:%i:%s', starttime) <= $date")
            ->where(function ($query) {
                $query->where('auctions.starttime', '<=', now('Asia/Jakarta'));
            })
            ->where(function ($query) {
                $query->where('auctions.end_date', '>=', now('Asia/Jakarta'));
            })
            ->first();

        // change space into T
        if ($data) {
            $data->end_date = preg_replace('/\s+/', 'T', $data->end_date);
        }

        $photos = DB::table('auction_photos')->where('auction_id', $data->id)->limit(3)->get();
        $leaderBoards = DB::table('auction_bids')
            ->select('auction_bids.created_at', 'users.name', 'auction_bids.bid_price', 'auction_bids.user_id')
            ->where('auction_bids.auction_id', $data->id)
            ->leftJoin('users', 'users.id', 'auction_bids.user_id')
            ->limit(10)
            ->orderByDesc('bid_price')
            ->get();
        $highestBid = DB::table('auction_bids')->where('auction_bids.auction_id', $data->id)->max('bid_price');
        $lelangMendatang = DB::table('auctions')
            ->select('auctions.name', 'auctions.photo', 'users.name as username', DB::raw('MAX(auction_bids.bid_price) as bidTertinggi'))
            ->leftJoin('auction_bids', 'auction_bids.auction_id', 'auctions.id')
            ->join('users', 'users.id', 'auction_bids.user_id')
            ->groupBy('auctions.id')
            ->whereDate('auctions.starttime', '>', $date)
            ->limit(3)
            ->get();
        $setting = DB::table('auction_special_settings')->first();
        return view('dashboard.pages.auction', compact('data', 'photos', 'leaderBoards', 'highestBid', 'lelangMendatang', 'riwayatLelang', 'poin', 'setting'));
    }

    public function winTebakKata()
    {
        if (Session::get('quizId')) {
            $answer = DB::table('jawaban_tebak_kata')->where('user_id', Auth::id())
                ->where('quiz_tebak_kata_id', Session::get('quizId'))
                ->where('status_jawaban', 1)
                ->count();
            $check = DB::table('jawaban_tebak_kata')->where('user_id', Auth::id())
                ->where('quiz_tebak_kata_id', Session::get('quizId'))
                ->count();
            if ($check > 0) {
                $question = DB::table('quiz_tebak_kata_list')->where('quiz_tebak_kata_id', Session::get('quizId'))
                    ->count();
                $poin = DB::table('jawaban_tebak_kata')->where('user_id', Auth::id())
                    ->where('quiz_tebak_kata_id', Session::get('quizId'))
                    ->where('status_jawaban', 1)
                    ->sum('poin');
                return view('dashboard.pages.quiz-win', compact('answer', 'question', 'poin'));
            }
        }
        return redirect()->route('homeDash');
    }

    public function jadwalPage()
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $jadwalLounge = DB::table('jadwal_omaru')->whereBetween('time_event', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->orderBy('jam_mulai', 'asc')->where('ruangan', 'lounge')->get();
        $jadwalNgopi = DB::table('jadwal_omaru')->whereBetween('time_event', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->orderBy('jam_mulai', 'asc')->where('ruangan', 'ngopi')->get();
        $jadwalWgmlive = DB::table('jadwal_omaru')->whereBetween('time_event', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->orderBy('jam_mulai', 'asc')->where('ruangan', 'wgmlive')->get();
        return view('dashboard.pages.jadwal', compact('jadwalLounge', 'jadwalNgopi', 'jadwalWgmlive'));
    }

    public function coinPage()
    {
        $datas = DB::table('koins')->get();
        return view('dashboard.components.coins.change-coin', compact('datas'));
    }

    public function submitCoinPage()
    {
        $data = DB::table('temp_tukar_koin')->where('user_id', Auth::id())->count();
        if ($data > 0) {
            return view('dashboard.components.coins.submit-coin');
        }
        return redirect()->route('coinPage');
    }

    public function getCoinCodePage()
    {
        return view('dashboard.components.coins.get-code');
    }

    public function doneCoinPage()
    {
        return view('dashboard.components.coins.done-coin');
    }

    public function checkCoinPage()
    {
        $getData = DB::table('penukaran_poins')->where('user_id', Auth::id())->paginate(10);
        return view('dashboard.components.coins.check-coin', compact('getData'));
    }

    public function trackCoinPage(Request $request)
    {
        $kode = $request->kode;
        $data = DB::table('penukaran_poins')->where('kode_penukaran', $kode)->first();
        $countPoin = DB::table('user_poins')->where('fk_id', $data->id)->where('from', 'Tukar Koin')->count();
        $baperPoin = 0;
        if ($countPoin > 0) {
            $baperPoin = DB::table('user_poins')->where('fk_id', $data->id)->where('from', 'Tukar Koin')->first();
        }
        return view('dashboard.components.coins.track-coin-status', compact('data', 'baperPoin'));
    }

    public function redeemCuponPage()
    {
        return view('dashboard.components.coins.redeem-cupon');
    }

    public function doneRedeemPage()
    {
        return view('dashboard.components.coins.done-redeem');
    }

    public function jawabanRejekiSobat()
    {
        $quizId = DB::table('jawaban_users')->where('user_id', Auth::id())->orderByDesc('quiz_id')->pluck('quiz_id')->first();
        $datas =  DB::table('jawaban_users')->where('jawaban_users.user_id', Auth::id())
            ->where('jawaban_users.quiz_id', $quizId)
            ->join('pertanyaan_rejeki_sobat', 'pertanyaan_rejeki_sobat.id', 'jawaban_users.pertanyaan_id')
            ->get();
        dd($datas);
    }

    public function rejekiSobatAns()
    {
        $quizId = DB::table('jawaban_users')->where('user_id', Auth::id())->orderByDesc('quiz_id')->pluck('quiz_id')->first();
        $datas =  DB::table('jawaban_users')->where('jawaban_users.user_id', Auth::id())
            ->where('jawaban_users.quiz_id', $quizId)
            ->join('pertanyaan_rejeki_sobat', 'pertanyaan_rejeki_sobat.id', 'jawaban_users.pertanyaan_id')
            ->get();

        // dd($datas);
        return view('dashboard.pages.rejeki-sobat-answer', compact('datas'));
    }

    public function coinManagementpage($id)
    {
        $getData = DB::table('penukaran_poins')->where('id', $id)->first();
        $getDate = date('Y-m-d', strtotime($getData->tanggal_status_redeem));
        $getKoins = DB::table('tukar_koins')->where('user_id', $getData->user_id)->where('periode', $getData->id)->get();
        $getAllkoin = DB::table('koins')->get();
        $totalPoin = 0;
        foreach ($getKoins as $koin) {
            $index = (int)$koin->koin - 1;
            $getAllkoin[$index]->qty = $koin->qty;
            $totalPoin = $totalPoin + $koin->poin;
        }
        // dd($getKoins, $getAllkoin[0]->id, $getData, $poin, $totalPoin);

        return view('dashboard.pages.form-admin-coin', compact('getData', 'getAllkoin', 'totalPoin'));
    }

    public function faqPoinPage()
    {
        return view('dashboard.components.faq.poin');
    }

    public function faqAuctionPage()
    {
        return view('dashboard.components.faq.auction');
    }

    public function faqQuizPage()
    {
        return view('dashboard.components.faq.quiz');
    }

    public function editProfile()
    {
        // $data['user'] = DB::table('users')->where('id', Session::get('user_id'))->first();
        try {
            $client = new Client();
            $city = $client->request(
                'GET',
                "https://api.rajaongkir.com/starter/city",
                [
                    'headers' => [
                        'key' => "a906fd8fc45a816184224df29f246d93",
                        // 'key' => "437db99af91a23c64bf1bed279bc4d0f"

                    ]
                ]
            );

            // if ($city->getStatusCode() == '200') {
                $data['city'] = json_decode($city->getBody())->rajaongkir->results;
            // } else {
            //     $data['city'] = [];
            // }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $data['city'] = [];
        }
        $city = $data['city'];
        $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
        // dd($city);
        return view('dashboard.pages.edit-profile', compact('city', 'poin'));
    }

    public function handleEditProfile(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            // 'city' => 'required|string',
            'kode_pos' => 'required|numeric',
            'no_wa' => 'required|string|min:6',
            // 'nik' => 'required|numeric',
        ]);


        // dd($request->all());
        if ($validator->fails()) {
            // dd($validator->fails(), $validator);
            return \Redirect::back()->withErrors($validator);
        }

        $user_id = auth()->user()->id;

        // Check NIK if exists
        $data['nik'] = $request->nik;
        $exitsNIK = DB::table('users')
                    ->where('nik', $request->nik)
                    ->where('id', '<>', $user_id)
                    ->first();
        if ($exitsNIK) {
            if ($exitsNIK->ktp_verification == 1) {
                return redirect()->route('editProfile')->with("errorMsg", "NIK Sudah terdaftar!");
            }
        }

        if($request->ktp){
            $ktp_name = auth()->user()->username . time() . '.' . $request->file('ktp')->getClientOriginalExtension();
            $data['ktp'] = $ktp_name;
            $data['ktp_verification'] = 0;
        }
        // Check for unique email
        // $checkEmail = DB::table('users')
        //     ->where('id', '<>', $user_id)
        //     ->where('email', $request->email)
        //     ->count();

        // if ($checkEmail > 0) {
        //     return \Redirect::back()->with("email", "Email tidak valid!");
        // }

        // Parse no wa to number
        $whatsapp = $request->no_wa;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);

        if ($whatsapp[0] == '0') {
            if ($whatsapp[1] == '8') {
                $whatsapp = ltrim($whatsapp, "0");;
                $whatsapp = "62" . $whatsapp;
            } else {
                $whatsapp = '-';
            }
        } else {
            if ($whatsapp[0] == '8') {
                $whatsapp = "62" . $whatsapp;
            } elseif ($whatsapp[0] == '6' && $whatsapp[1] == '2') {
                $whatsapp;
            } elseif ($whatsapp[0] == '+') {
                if ($whatsapp[1] == '6' && $whatsapp[2] == '2') {
                    $whatsapp = str_replace("+", "", $whatsapp);
                } else {
                    $whatsapp = '-';
                }
            } else {
                $whatsapp = '-';
            }
        }

        // check is WA valid
        if ($whatsapp == "-") {
            return \Redirect::back()->with("no_wa", "Nomor tidak valid!");
        }
        if($request->no_wa == Auth::user()->whatsapp){
            date_default_timezone_set("Asia/Jakarta");
            $data['name'] = $request->name;
            $data['kota'] = $request->city;
            $data['alamat_rumah'] = $request->alamat;
            $data['kode_pos'] = $request->kode_pos;
            $data['updated_at'] = date('Y-m-d H:i:s');

            DB::table('users')
                ->where('id', $user_id)
                ->update($data);
            // DB::table('users')
            //     ->where('id', $user_id)
            //     ->update([
            //         'name' => $request->name,
            //         'kota' => $request->city,
            //         'alamat_rumah' => $request->alamat,
            //         'kode_pos' => $request->kode_pos,
            //         'nik' => $request->nik,
            //         'ktp' => $ktp_name,
            //         'ktp_verification' => 0,
            //         'updated_at' => date('Y-m-d H:i:s')
            //     ]);

            // put ktp on storage if success
            if($request->ktp){
              Storage::putFileAs('public/ktp', $request->ktp, $ktp_name);
            }

            $names = explode(' ', trim($request->name));
            Session::put('user_name', $names[0]);
            return redirect()->route('editProfile')->with("success", "Berhasil memperbarui profil!");
        }

        $checkWA = DB::table('users')
            ->where('id', '!=', Auth::id())
            ->where('whatsapp_verification', 1)
            ->where('whatsapp', $whatsapp)
            ->count();

        if ($checkWA > 0) {
            return \Redirect::back()->with("no_wa", "Nomor sudah dipakai!");
        }

        date_default_timezone_set("Asia/Jakarta");
        $data['name'] = $request->name;
        $data['kota'] = $request->city;
        $data['alamat_rumah'] = $request->alamat;
        $data['kode_pos'] = $request->kode_pos;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['whatsapp'] = $whatsapp;

        DB::table('users')
            ->where('id', $user_id)
            ->update($data);
        // DB::table('users')
        //     ->where('id', $user_id)
        //     ->update([
        //         'name' => $request->name,
        //         'kota' => $request->city,
        //         'alamat_rumah' => $request->alamat,
        //         'kode_pos' => $request->kode_pos,
        //         'whatsapp' => $whatsapp,
        //         'nik' => $request->nik,
        //         'ktp' => $ktp_name,
        //         'ktp_verification' => 0,
        //         'updated_at' => date('Y-m-d H:i:s')
        //     ]);
        // put ktp on storage if success
        if($request->ktp){
          Storage::putFileAs('public/ktp', $request->ktp, $ktp_name);
        }

        $names = explode(' ', trim($request->name));
        Session::put('user_name', $names[0]);
        return redirect()->route('editProfile')->with("success", "Berhasil memperbarui profil!");
    }

    public function changePassword()
    {
        return view('dashboard.pages.change-password');
    }

    public function handleChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string|min:6',
            'new_password' => 'required|string|min:6',
            'confirm_new_password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator);
        }

        $user = Auth::user();
        if (!Hash::check($request->old_password, $user->password)) {
            return \Redirect::back()->with("beda", "Password lama tidak valid atau salah!");
        }

        if ($request->new_password != $request->confirm_new_password) {
            return \Redirect::back()->with("beda", "Password baru tidak sama!");
        }

        $user_id = Session::get('user_id');
        $password = Hash::make($request->new_password);

        DB::table('users')
            ->where('id', $user_id)
            ->update([
                'password' => $password,
            ]);

        return redirect()->route('changePassword')->with("success", "Berhasil memperbarui password!");
    }

    public function comingSoonPage()
    {
        return view('dashboard.pages.coming-soon');
    }

    public function postBlog()
    {
        return view('dashboard.pages.add-blog');
    }

    public function userBlog()
    {
        $data['blogs'] = DB::table('blogs')
            ->leftJoin('blog_likes', 'blog_likes.blog_id', 'blogs.id')
            ->select('blogs.*', DB::raw('COUNT(blog_likes.blog_id) as likes'),  DB::raw('(CASE WHEN blog_likes.user_id = ' . Auth::id() . ' AND blog_likes.status = 1 THEN true ELSE false END) as likeStatus'))
            ->groupBy('blogs.id')
            ->orderBy('blogs.created_at', 'desc')
            ->where('blogs.user_id', Auth::id())
            ->orderBy('blogs.created_at', 'desc')->get();

        return view('dashboard.pages.user-blog', $data);
    }

    public function handlePost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator);
        }
        if ($request->file('blog_img')) {
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('blog_img');
            $uniqueFileName = str_replace(" ", "", time() . "-" . Auth::id() . ".png");
            $path = $file->storeAs('public/uploads', $uniqueFileName);
            $path = "storage/uploads/" . $uniqueFileName;
        } else if ($request->blog_img) {

            $base64_image = $request->blog_img;
            $base64_image = "data:image/png;base64," . $base64_image;

            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);

                $file = base64_decode($data);
                $uniqueFileName = time() . "-" . Auth::id() . ".png";

                \Storage::disk('local')->put("public/uploads/" . $uniqueFileName, $file);
                $path = "storage/uploads/" . $uniqueFileName;
            }
        } else {
            $path = "";
        }

        if ($path != "") {
            $blogID = DB::table('blogs')->insertGetId([
                'user_id' => Auth::id(),
                'title' => $request->title,
                'body' => $request->description,
                'image' => $path,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            if ($blogID != "" && $blogID != 0) {
                return redirect()->route('postBlog')->with("success", "Silahkan menunggu agar tulisan kamu mendapatkan konfirmasi admin!");
            } else {
                return redirect()->route('postBlog')->with("error", "Gagal memasukan data ke server!");
            }
        } else {
            return redirect()->route('postBlog')->with("error", "Data gambar yang dimasukan tidak valid!");
        }
    }

    public function bikinQuiz()
    {
        $submitCount = DB::table('soal_tebak_kata_users')
            ->whereDate('created_at', Carbon::today())
            ->where('user_id', Auth::id())->count();
        $qty = 5 - $submitCount;
        $lists = DB::table('soal_tebak_kata_users')
            ->where('user_id', Auth::id())
            ->whereDate('created_at', Carbon::today())->get();
        return view('dashboard.pages.bikin-quiz.user-quiz', compact('qty', 'lists'));
    }

    public function successBikinQuiz()
    {
        return view('dashboard.pages.bikin-quiz.quiz-success');
    }

    public function babaMencari()
    {
        $days = [];
        $date1 = Carbon::now()->startOfWeek();
        $date2 = Carbon::now()->startOfWeek();
        $date3 = Carbon::now()->startOfWeek();
        /**
         * 2 5 6
         */
        $new_date = $date1->addDay(2);
        $days[0]['date'] = $new_date->format('d M Y');
        $days[0]['value'] = 2;
        $days[0]['hari'] = 'Rabu';
        $days[0]['time_format'] = $new_date->format('Y-m-d');
        $new_date = $date2->addDay(5);
        $days[1]['date'] = $new_date->format('d M Y');
        $days[1]['value'] = 5;
        $days[1]['hari'] = "Sabtu";
        $days[1]['time_format'] = $new_date->format('Y-m-d');
        $new_date = $date3->addDay(6);
        $days[2]['date'] = $new_date->format('d M Y');
        $days[2]['value'] = 6;
        $days[2]['hari'] = "Minggu";
        $days[2]['time_format'] = $new_date->format('Y-m-d');
        $times = DB::table('baba_times')->select('id', 'time_event')->get();
        // dd($days, $times);
        $sisaSlot = 0;
        $segmenSlot = 3;
        foreach ($days as $day) {
            foreach ($times as $time) {
                $event_time = $day["time_format"] . ' ' . $time->time_event . ':00';
                $check = DB::table('auditions')->where('waktu_tampil', $event_time)->count();
                $slotSisa = $segmenSlot - $check;
                // if(!$check){
                $sisaSlot = $sisaSlot + $slotSisa;
                // }
            }
        }
        $joinBaba = DB::table('auditions')->where('user_id', Auth::id())->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->count();
        return view('dashboard.pages.baba-mb.baba', compact('days', 'joinBaba', 'sisaSlot'));
    }

    public function babaSukses()
    {
        $data = DB::table('auditions')->where('user_id', Auth::id())->orderByDesc('id')->first();
        return view('dashboard.pages.baba-mb.baba-sukses', compact('data'));
    }

    public function listTemanSobat()
    {
        $temanSobat = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->count();
        $temanSobats = DB::table('users')->where('ref_id', Auth::id())->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->paginate(10);

        return view('dashboard.pages.list-teman-sobat', compact('temanSobat', 'temanSobats'));
    }
}
