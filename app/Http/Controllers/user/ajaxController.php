<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ajaxController extends Controller
{
    public function riwayat(Request $request)
    {
        if ($request->has('month')) {
            $riwayats = DB::table('user_poins')
                ->where('user_id', Auth::id())
                ->whereMonth('created_at', $request->month)
                ->whereYear('created_at', date('Y'))
                ->get();
            return response()->json(compact('riwayats'));
        }
    }
    public function riwayatSitw(Request $request)
    {
        if ($request->has('month')) {
            $riwayats = DB::table('ticket')
                ->select('category_gacha.cat_name', 'ticket.created_at')
                ->where('ticket.user_id', Auth::id())
                ->whereMonth('ticket.created_at', $request->month)
                ->whereYear('ticket.created_at', date('Y'))
                ->orderByDesc('ticket.created_at')
                ->join('category_gacha', 'category_gacha.id', 'ticket.category_gatcha_id')
                ->get();
            return response()->json(compact('riwayats'));
        }
    }

    public function riwayatSpin(Request $request)
    {
        if ($request->has('month')) {
            $riwayats = DB::table('user_poins')
                ->where(function ($query) {
                    $query->where('from', 'Spin It Yourself - Reguler')
                        ->orWhere('from', 'Spin It Yourself - Premium');
                })
                ->whereMonth('created_at', $request->month)
                ->whereYear('user_poins.created_at', date('Y'))
                ->orderByDesc('created_at')
                ->where('user_poins.user_id', Auth::id())
                ->get();
            return response()->json(compact('riwayats'));
        }
    }

    public function riwayatSoal(Request $request)
    {
        if ($request->has('date')) {
            $lists = DB::table('soal_tebak_kata_users')
                ->where('user_id', Auth::id())
                ->whereDate('created_at', $request->date)->get();
            return response()->json(compact('lists'));
        }
    }

    public function sisaSlotBaba(Request $request){
        // ->toDateString()
        $date = Carbon::now()->startOfWeek();
        if($request->day == 1){
            $newDate =  $date->format('d M Y');
            $temp_date = $date->format('Y-m-d');
        }
        else{
            $time_event = $date->addDay($request->day);
            $newDate = $time_event->format('d M Y');
            $temp_date = $time_event->format('Y-m-d');
        }
        /**check sisa stok disini */
        $baba_times = DB::table('baba_times')->get();
        $slotPerSegmen = 3;
        foreach($baba_times as $key => $value){
            $waktu_tampil = $temp_date.' '.$value->time_event.':00';
            $check = DB::table('auditions')->where('waktu_tampil', $waktu_tampil)->count();
            $sisaSlot = $slotPerSegmen - $check;
            $baba_times[$key]->slot = $sisaSlot;
        }

        return response()->json([
            'date' => $newDate,
            'day' => $request->day,
            'slot' => $baba_times
        ]);
    }
}
