<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class emailVerification extends Controller
{
    public function verifyEmail(Request $request){
        $user = DB::table('users')->where('token_verify_email', $request->token)->first();

        if($user)
        {
          if($user->emailValidation != 'validated'){
            DB::table('users')->where('token_verify_email', $request->token)->update([
              'emailValidation' => 'validated',
              'email_verified_at' => now()
            ]);
            if($user->whatsapp_verification == 1 && $user->ref_id != NULL){
              DB::table('user_poins')->insert([
                'user_id' => $user->ref_id,
                'fk_id' => $user->id,
                'poin' => 1000,
                'from' => 'Bawa teman sobat',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
              ]);

              /**Update total baper poin */

              $baperPoin = DB::table('users')->where('id', $user->ref_id)->first()->baper_poin;
              $newPoint = $baperPoin + 1000;
              DB::table('users')->where('id', $user->ref_id)->update([
                'baper_poin' => $newPoint
              ]);

              /** update teman sobat */

              $temanSobat = DB::table('users')->where('id', $user->id)->first()->countRef;
              $newTemanSobat = $temanSobat + 1;
              DB::table('users')->where('id', $user->ref_id)->update([
                'countRef' => $newTemanSobat
              ]);

              /**Update teman sobat event */
              $date = date('Y-m-d H:i:s');
              $userId = $user->ref_id;
              $activeEvent = DB::table('referral_events')
              ->where('referral_events.start_time', '<', $date)
              ->where('referral_events.end_time', '>', $date)
              ->first();

              if($activeEvent){
                if(DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()){
                  /**Update */
                  $count = DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->first()->amount;
                  $count = $count + 1;
                  DB::table('leaderboards_referral')->where('user_id', $userId)->where('referral_events', $activeEvent->id)->update([
                    'amount' => $count,
                    'updated_at' => now()
                  ]);

                }
                else{
                  /**Insert */
                  DB::table('leaderboards_referral')->insert([
                  'user_id' => $userId,
                  'referral_events' => $activeEvent->id,
                  'amount' => 1,
                  'created_at' => now(),
                  'updated_at' => now()
                  ]);

                }
              }
            }
          }
          return view('landing-page.pages.email-verif-done');
        }
        else{
          /** Token not found*/
          return view('landing-page.pages.email-verif-failed');
        }
        
    }

    public function sendEmailVerification(){

        $client = new Client();
        try {

            $send = $client->post('https://mailgun.sobatbadak.club/api/v1/verifikasi-email', [
                'form_params' => [
                    'email' => Auth::user()->email
                ]
            ]);
            return \Redirect::back()->with('emailVerification', 'Success');

        } catch (\BadResponseException $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            // do something with json string...
            abort(500);
            dd($jsonBody);
        }


    }

}
