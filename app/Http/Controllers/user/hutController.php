<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class hutController extends Controller
{
    public function preRegister(){
        $check = DB::table('pre_registers')->Where('user_id', Auth::id())->first();
        if($check){
            return redirect()->back()->with('Status', 'Error');
        }
        DB::table('pre_registers')->insert([
            'user_id' => Auth::id(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        return redirect()->back()->with('lomba', 'Success');
    }

    public function registerMakanKrupuk(){
        if($this->checkRegister() == false){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda belum mendaftar pre-register'
            ]);
        }
        $check = DB::table('lomba_makan_krupuk')->Where('user_id', Auth::id())->first();
        if($check){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda sudah terdaftar sebelumnya'
            ]);
        }
        DB::table('lomba_makan_krupuk')->insert([
            'user_id' => Auth::id(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        return redirect()->back()->with('lomba', 'Lomba Makan Kerupuk');
    }

    public function registerFashionShow(){
        if($this->checkRegister() == false){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda belum mendaftar pre-register'
            ]);
        }
        $check = DB::table('lomba_fashion_show')->Where('user_id', Auth::id())->first();
        if($check){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda sudah terdaftar sebelumnya'
            ]);
        }
        DB::table('lomba_fashion_show')->insert([
            'user_id' => Auth::id(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        return redirect()->back()->with('lomba', 'CSB Fashion Show'); 
    }

    public function registerTebakan(){
        if($this->checkRegister() == false){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda belum mendaftar pre-register'
            ]);
        }
        $check = DB::table('lomba_tebakan')->Where('user_id', Auth::id())->first();
        if($check){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda sudah terdaftar sebelumnya'
            ]);
        }
        DB::table('lomba_tebakan')->insert([
            'user_id' => Auth::id(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        return redirect()->back()->with('lomba', 'Tebak Rumah dan Pakaian Adat'); 
    }

    public function registerTebakLagu(){
        if($this->checkRegister() == false){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda belum mendaftar pre-register'
            ]);
        }
        $check = DB::table('lomba_tebak_lagu')->Where('user_id', Auth::id())->first();
        if($check){
            return response()->Json([
                'Status' => 'Error',
                'Message' => 'Anda sudah terdaftar sebelumnya'
            ]);
        }
        DB::table('lomba_tebak_lagu')->insert([
            'user_id' => Auth::id(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        return redirect()->back()->with('lomba', 'Tebak Lagu Nasional');  
    }

    public function checkRegister(){
        $check = DB::table('pre_registers')->Where('user_id', Auth::id())->first();
        if($check){
            return true;
        }
        else{
            return false;
        }
    }

}
