<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;

class resellerController extends Controller
{
    public function register(Request $request){
        date_default_timezone_set("Asia/Jakarta");
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'whatsapp' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator);
        }
    }
}
