<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use CRUDBooster;

class tukarKoinController extends Controller
{
    public function saveTemp(Request $request)
    {
        // check apakah sudah tukar koin hari ini
        $check = DB::table('penukaran_poins')->where('user_id', Auth::id())->whereDate('tanggal_status_redeem', date('Y-m-d'))->count();
        if($check > 0){
            return redirect()->back()->with('error', 'Notes: Pengajuan Penukaran Koin hanya dapat dilakukan sehari sekali!');
        }
        DB::table('temp_tukar_koin')->where('user_id', Auth::id())->delete();
        foreach ($request->except(['_token']) as $key => $value) {
            if ($value != 0) {
                DB::table('temp_tukar_koin')->insert([
                    'user_id' => Auth::id(),
                    'koin' => $key,
                    'qty' => $value,
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
            }
        }
 
        return redirect()->route('submitCoinPage');
    }
   public function store(Request $request)
    {
        // check apakah sudah tukar koin hari ini
        $check = DB::table('penukaran_poins')->where('user_id', Auth::id())->whereDate('tanggal_status_redeem', date('Y-m-d'))->count();
        if($check > 0){
            return redirect()->back()->with('error', 'Notes: Pengajuan Penukaran Koin hanya dapat dilakukan sehari sekali!');
        }
        // $new_number = $this->formatWhatsapp($request->whatsapp);
        // $datas = DB::table('temp_tukar_koin')->where('user_id', Auth::id())->get();
        // if($datas->count() != 0){
        $userData = DB::table('users')->where('id', Auth::id())->select('name', 'email', 'whatsapp', 'alamat_rumah')->first();
            $code = $this->generateCode();
            $penukaran_id = DB::table('penukaran_poins')->insertGetId([
                'user_id' => Auth::id(),
                'name' => $userData->name,
                'whatsapp' => $userData->whatsapp,
                'email' => $userData->email,
                'alamat' => $userData->alamat_rumah,
                'status_redeem' => 'menunggu penerimaan koin',
                'tanggal_status_redeem' => date('Y-m-d H:i:s'),
                'status_koin' => 'belum diterima',
                'status_baper_poin' => 'belum diberikan',
                'kode_penukaran' => $code
            ]);
            foreach($request->except(['_token']) as $key => $value){
                if ($value != 0) {
                    $poin = DB::table('koins')->where('id', $key)->first();
                    $totalPoin = (int)$poin->poin * (int)$value;
                    DB::table('tukar_koins')->insert([
                        'periode' => $penukaran_id,
                        'user_id' => Auth::id(),
                        'penukaran_id' => $penukaran_id,
                        'koin' => $key,
                        'qty' => $value,
                        'poin' => $totalPoin,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            }
            $penukaran = DB::table('penukaran_poins')->where('id', $penukaran_id)->first();
            return redirect()->route('getCoinCodePage')->with('code', $penukaran->kode_penukaran);
        // }
        return redirect()->route('coinPage');
    }

    public function generateCode(){
        $code = \Str::random(8);
        $check = DB::table('penukaran_poins')->where('kode_penukaran', $code)->count();
        if($check == 1){
            $this->createReferral();
        }
        return $code;
    }

    public function formatWhatsapp($number)
    {
        $whatsapp = $number;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if ($check_number[0] == '0') {
            foreach ($check_number as $n => $number) {
                if ($n > 0) {
                    if ($check_number[1] == '8') {
                        $new_number .= $number;
                    } else {
                        $new_number = '-';
                    }
                }
            }
        } else {
            if ($check_number[0] == '8') {
                $new_number = "62" . $whatsapp;
            } elseif ($check_number[0] == '6') {
                $new_number = $whatsapp;
            } elseif ($check_number[0] == '+') {
                foreach ($check_number as $n => $number) {
                    if ($n > 2) {
                        $new_number .= $number;
                    }
                }
            } else {
                $new_number = '-';
            }
        }
        return $new_number;
    }
    
    public function inputKoin($id){
        $users = DB::table('users')->select('id', 'name')->get();
        return view('admin.input-poin', compact('id', 'users'));
    }
    
    public function bpKoinTidakSesuai(Request $request){
        $insert = DB::table('user_poins')->insert([
         'user_id'=>$request->user,
         'fk_id'=>$request->fkId,
         'poin'=>$request->poin,
         'from'=>$request->from,
         'created_at'=>date('Y-m-d H:i:s')
        ]);
    CRUDBooster::redirect('admin/penukaran_poins34', 'poin telah ditambahkan', 'success');
    }
    
    public function statusTukarKoin($kode){
        $status = DB::table('penukaran_poins')->where('kode_penukaran', $kode)->select('status_koin', 'id')->first();
        $poin = DB::table('user_poins')->where('fk_id', $status->id)->where('from', 'Tukar Koin')->first();
        // dd($poin);
        return response()->json([$status, $poin]);
    }
    
    public function editTukarKoin(Request $request){
        // dd($request);
        $getUserId = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->first();
        if($request->radioes4 == "0"){
            DB::table('user_poins')->insert([
               'user_id'=>$getUserId->user_id, 
               'fk_id'=>$getUserId->id, 
               'poin'=>$request->jumlah_poin, 
               'from'=>'Tukar Koin', 
               'created_at'=>date('Y-m-d H:i:s')
            ]);
            
            $update = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->update([
                    'status_koin'=>'diterima sesuai',
                    'tanggal_status_koin'=>date('Y-m-d H:i:s'),
                    'status_baper_poin'=>'sudah diproses',
                    'tanggal_status_baper_poin'=>date('Y-m-d H:i:s'),
                ]);
            CRUDBooster::redirect('admin/penukaran_poins33', 'poin telah ditambahkan', 'success');
        }else{
             DB::table('user_poins')->insert([
              'user_id'=>$getUserId->user_id, 
              'fk_id'=>$getUserId->id, 
              'poin'=>$request->gived_poin, 
              'from'=>'Tukar Koin', 
              'created_at'=>date('Y-m-d H:i:s')
            ]);
            $update = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->update([
                    'status_koin'=>'diterima tidak sesuai',
                    'tanggal_status_koin'=>date('Y-m-d H:i:s'),
                    'status_baper_poin'=>'sudah diproses',
                    'tanggal_status_baper_poin'=>date('Y-m-d H:i:s'),
                ]);
            $getId = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->first();
            $delete = DB::table('tukar_koins')->where('periode', $getId->id)->delete();
            foreach($request->except(['_token', 'nama', 'alamat', 'no_wa', 'email', 'kode', 'koin1', 'koin2', 'koin3', 'koin4', 'koin5', 'koin6', 'koin7', 'koin8', 'koin9', 'koin10', 'jumlah_poin', 'radioes4', 'gived_poin', 'submit']) as$key=>$value){
               if($value != 0){
                    $poin = DB::table('koins')->where('id', $key)->first();
                    $totalPoin = (int)$poin->poin * $value;
                    $insertNewTukarKoins = DB::table('tukar_koins')->insert([
                        'periode'=>$getId->id,
                        'user_id'=>$getId->user_id,
                        'koin'=>$key,
                        'qty'=>$value,
                        'poin'=>$totalPoin,
                        'penukaran_id'=>$getId->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
               }
            }
            
            CRUDBooster::redirect('admin/penukaran_poins34', 'poin telah ditambahkan', 'success');

        }
    }
}
