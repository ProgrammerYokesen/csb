<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Session, Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Jenssegers\Agent\Agent;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class userController extends Controller
{
    public function login(Request $request)
    {
        if (env('APP_URL') == "https://sobatbadak.club") {
            $validator = Validator::make($request->all(), [
                'g-recaptcha-response' => 'required'
            ]);

            if ($validator->fails()) {
                return \Redirect::back()->withErrors($validator);
            }
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $names = explode(' ', trim(Auth::user()->name));
            Session::put('user_name', $names[0]);
            Session::put('user_id', Auth::user()->id);

            if(Session::has('daftar_imlek_2022'))
            {
              // dd(Session::all());
              $agent = new Agent();
              $platform = $agent->platform();
              $version = $agent->version($platform);
              $browser = $agent->browser();
              $versionbrowser = $agent->version($browser);

              /**Register users */

              $daftarImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
              // dd($userCount, Auth::id());
              // dd(Auth::user());
              // dd($daftarImlek, Auth::user());
              if($daftarImlek == 0)
              {
                // dd(Auth::user());
                DB::table('imlek_users')->insert([
                  'user_id' => Auth::id(),
                  'created_at' => now(),
                  'updated_at' => now(),
                  'ip_address' => \Request::ip(),
                  'device' => $agent->device(),
                  'platform' => $agent->platform(),
                  'version_platform' => $version,
                  'browser'  => $agent->browser(),
                  'version_browser' => $versionbrowser,
                  'languages' => $agent->languages()
                ]);
                $newImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
                // dd($daftarImlek, $newImlek, Auth::user());
                /** Get footprint **/

                $cookie = $_COOKIE['footprints'];

                if ($cookie != NULL) {
                    // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
                    // $decryptedString = $encrypter->decrypt($cookie);
                    $decryptedString = \Crypt::decrypt($cookie, false);
                    $cookies = explode("|", $decryptedString);
                    // dd($decryptedString, $cookies);
                    $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                        'user_imlek' => Auth::id()
                    ]);
                }
              }
              Session::forget('daftar_imlek_2022');
            }

            if (Session::has('link')) {
                $link = Session::get('link');
                Session::forget('link');
                // dd(Auth::user());
                return redirect($link);
            }
            if($request->has('link')){
                return redirect($request->link);
            }

            // Authentication passed...
            // return redirect()->route('homeDash');
            return redirect()->route('homeDash')->with('login_aug', 'Login');
        }
        if(Session::has('harbolnas_1212'))
        {
          return \Redirect::back()->with('error', 'Wrong password or email');
        }
        return \Redirect::back()->with('error', 'Wrong password or email');
        // return redirect()->route('loginPage')->with('error', 'Wrong password or email');
    }

    /**
     * Function register new user
     */

    public function register(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");

        if (env('APP_URL') == "https://sobatbadak.club") {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'whatsapp' => 'required|string|min:6',
                'g-recaptcha-response' => 'required'
            ]);

            if ($validator->fails()) {
                return \Redirect::back()->withErrors($validator);
            }
        }

        /**Data jensenger */
        $agent = new Agent();
        if ($agent->isPhone()) {
            $screen = "Phone";
        } elseif ($agent->isTablet()) {
            $screen = "Tablet";
        } elseif ($agent->isDesktop()) {
            $screen = "Desktop";
        }
        $platform = $agent->platform();
        $version_platform = $agent->version($platform);
        $device = $agent->device();
        $browser = $agent->browser();
        $version_browser = $agent->version($browser);
        $languages = serialize($agent->languages());
        $robot = $agent->robot();
        $ip = request()->ip();
        /**End data jensenger */
        $ref_id = NULL;
        if (Session::has('referral_code')) {
            $ref_id = DB::table('users')->where('referral_code', Session::get('referral_code'))->first()->id;
        }
        $referral = $this->createReferral();
        $undian = $this->createUndian();
        $username = $this->createUsername();
        $password = \Hash::make($request->password);
        $subscribeEmail = 0;
        if ($request->remember == 1) {
            $subscribeEmail = 1;
        }
        $whatsapp = $request->whatsapp;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if ($check_number[0] == '0') {
            foreach ($check_number as $n => $number) {
                if ($n > 0) {
                    if ($check_number[1] == '8') {
                        $new_number .= $number;
                    } else {
                        $new_number = '-';
                    }
                }
            }
        } else {
            if ($check_number[0] == '8') {
                $new_number = "62" . $whatsapp;
            } elseif ($check_number[0] == '6') {
                $new_number = $whatsapp;
            } elseif ($check_number[0] == '+') {
                foreach ($check_number as $n => $number) {
                    if ($n > 2) {
                        $new_number .= $number;
                    }
                }
            } else {
                $new_number = '-';
            }
        }
        $userId = DB::table('users')->insertGetId([
            'username' => $username,
            'ref_id' => $ref_id,
            'name' => $request->name,
            'email' => $request->email,
            'referral_code' => $referral,
            'subscribeEmail' => $subscribeEmail,
            'ref_id' => $ref_id,
            'utm_source' => Session::get('utm_source'),
            'utm_campaign' => Session::get('utm_campaign'),
            'utm_medium' => Session::get('utm_medium'),
            'utm_content' => Session::get('utm_content'),
            'password' => $password,
            'whatsapp' => $new_number,
            'undian' => $undian,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'screen' => $screen,
            'ip_address' => $ip,
            'platform' => $platform,
            'version_platform' => $version_platform,
            'device' => $device,
            'browser' => $browser,
            'version_browser' => $version_browser,
            'languages' => $languages,
            'robot' => $robot
        ]);
        /**Kirim email */
        try {
              $client = new Client();
              $send = $client->post('https://mailgun.sobatbadak.club/api/v1/verifikasi-email', [
                    'form_params' => [
                        'email' => $request->email
                    ]
                ]);

        } catch (\Exception $e) {


        }

        /**
         * Comment untuk merubah dapat poin ketika yang diundang
         * sudah verifikasi nomer whatsapp dan email
         */

        // if(Session::has('referral_code')){
        //     DB::table('user_poins')->insert([
        //         'user_id' => $ref_id,
        //         'poin' => 1000,
        //         'from' => 'Bawa teman sobat',
        //         'created_at' => date('Y-m-d H:i:s'),
        //         'updated_at' => date('Y-m-d H:i:s')
        //     ]);
        // }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
          if(Session::has('daftar_imlek_2022'))
          {
            // dd(Session::all());
            $agent = new Agent();
            $platform = $agent->platform();
            $version = $agent->version($platform);
            $browser = $agent->browser();
            $versionbrowser = $agent->version($browser);

            /**Register users */

            $daftarImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
            // dd($userCount, Auth::id());
            // dd(Auth::user());
            // dd($daftarImlek, Auth::user());
            if($daftarImlek == 0)
            {
              // dd(Auth::user());
              DB::table('imlek_users')->insert([
                'user_id' => Auth::id(),
                'created_at' => now(),
                'updated_at' => now(),
                'ip_address' => \Request::ip(),
                'device' => $agent->device(),
                'platform' => $agent->platform(),
                'version_platform' => $version,
                'browser'  => $agent->browser(),
                'version_browser' => $versionbrowser,
                'languages' => $agent->languages()
              ]);
              $newImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
              // dd($daftarImlek, $newImlek, Auth::user());
              /** Get footprint **/

              $cookie = $_COOKIE['footprints'];

              if ($cookie != NULL) {
                  // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
                  // $decryptedString = $encrypter->decrypt($cookie);
                  $decryptedString = \Crypt::decrypt($cookie, false);
                  $cookies = explode("|", $decryptedString);
                  // dd($decryptedString, $cookies);
                  $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                      'user_imlek' => Auth::id()
                  ]);
              }
            }
            Session::forget('daftar_imlek_2022');
          }
            // Authentication passed...
            if($request->has('link')){
                return redirect($request->link);
            }

            if (Session::has('link')) {
                $link = Session::get('link');
                Session::forget('link');
                // dd(Auth::user());
                return redirect($link);
            }

            return redirect()->route('homeDash')->with('success_regis', 'Anda telah berhasil mendaftar !');
        }
        return redirect()->route('loginPage');
    }

    public function createReferral()
    {
        $referral = \Str::random(8);
        $check = DB::table('users')->where('referral_code', $referral)->count();
        if ($check == 1) {
            $this->createReferral();
        }
        return $referral;
    }
    public function createUndian()
    {
        $referral = \Str::random(8);
        $check = DB::table('users')->where('undian', $referral)->count();
        if ($check == 1) {
            $this->createUndian();
        }
        return $referral;
    }

    public function createUsername()
    {
        $username = 'User' . rand();
        $check = DB::table('users')->where('username', $username)->count();
        if ($check == 1) {
            $this->createUsername();
        }
        return $username;
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->flush();
        return redirect()->route('defaultPage');
    }

    public function avatar(Request $request)
    {
        DB::table('users')->where('id', Auth::id())->update([
            'avatars' => $request->avatar
        ]);
        return redirect()->back();
    }

    public function redirectToFacebook(Request $request, $provider)
    {
        if($request->has('link')){
            Session::put('link', $request->link);
        }
        return Socialite::driver($provider)->redirect();
    }

    public function handleFacebookCallback($provider)
    {

        $users = Socialite::driver($provider)->user();
        $exist = User::where('email', $users->email)->first();
        // dd($users, $provider, $exist);
        $undian = $this->createUndian();
        $username = $this->createUsername();
        $referral = $this->createReferral();

        /**Data jensenger */
        $agent = new Agent();
        if ($agent->isPhone()) {
            $screen = "Phone";
        } elseif ($agent->isTablet()) {
            $screen = "Tablet";
        } elseif ($agent->isDesktop()) {
            $screen = "Desktop";
        }
        $platform = $agent->platform();
        $version_platform = $agent->version($platform);
        $device = $agent->device();
        $browser = $agent->browser();
        $version_browser = $agent->version($browser);
        $languages = serialize($agent->languages());
        $robot = $agent->robot();
        $ip = request()->ip();
        /**End data jensenger */

        if ($exist == null) {
            $ref_id = NULL;
            if (Session::has('referral_code')) {
                $ref_id = DB::table('users')->where('referral_code', Session::get('referral_code'))->first()->id;
            }
            if(!$users->name){
              $email = $users->email;
              $name = explode("@", $email)[0];
            }
            if($users->name){
              $name = $users->name;
            }
            $insert = DB::table('users')->insertGetId([
                'name' => $name,
                'email' => $users->email,
                'username' => $username,
                'referral_code' => $referral,
                'ref_id' => $ref_id,
                'undian' => $undian,
                'providerOrigin' => $provider,
                'utm_source' => Session::get('utm_source'),
                'utm_campaign' => Session::get('utm_campaign'),
                'utm_medium' => Session::get('utm_medium'),
                'utm_content' => Session::get('utm_content'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'screen' => $screen,
                'emailValidation' => "validated",
                'ip_address' => $ip,
                'platform' => $platform,
                'version_platform' => $version_platform,
                'device' => $device,
                'browser' => $browser,
                'version_browser' => $version_browser,
                'languages' => $languages,
                'robot' => $robot
            ]);

            $users = User::where('id', $insert)->first();
            Auth::login($users);
            $names = explode(' ', trim(Auth::user()->name));
            Session::put('user_name', $names[0]);
            Session::put('user_id', Auth::user()->id);
            if(Session::has('daftar_imlek_2022'))
            {
              // dd(Session::all());
              $agent = new Agent();
              $platform = $agent->platform();
              $version = $agent->version($platform);
              $browser = $agent->browser();
              $versionbrowser = $agent->version($browser);

              /**Register users */

              $daftarImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
              // dd($userCount, Auth::id());
              // dd(Auth::user());
              // dd($daftarImlek, Auth::user());
              if($daftarImlek == 0)
              {
                // dd(Auth::user());
                DB::table('imlek_users')->insert([
                  'user_id' => Auth::id(),
                  'created_at' => now(),
                  'updated_at' => now(),
                  'ip_address' => \Request::ip(),
                  'device' => $agent->device(),
                  'platform' => $agent->platform(),
                  'version_platform' => $version,
                  'browser'  => $agent->browser(),
                  'version_browser' => $versionbrowser,
                  'languages' => $agent->languages()
                ]);
                $newImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
                // dd($daftarImlek, $newImlek, Auth::user());
                /** Get footprint **/

                $cookie = $_COOKIE['footprints'];

                if ($cookie != NULL) {
                    // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
                    // $decryptedString = $encrypter->decrypt($cookie);
                    $decryptedString = \Crypt::decrypt($cookie, false);
                    $cookies = explode("|", $decryptedString);
                    // dd($decryptedString, $cookies);
                    $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                        'user_imlek' => Auth::id()
                    ]);
                }
              }
              Session::forget('daftar_imlek_2022');
            }
            if (Session::has('link')) {
              $link = Session::get('link');
              Session::forget('link');
              // dd($users, Auth::user(), $exist);
              return redirect($link);
            }
            return redirect()->route('homeDash');
        } else {
            Auth::login($exist);
            // dd($users, $provider, $exist, Auth::user()->email);
            if(Session::has('daftar_imlek_2022'))
            {
              // dd(Session::all());
              $agent = new Agent();
              $platform = $agent->platform();
              $version = $agent->version($platform);
              $browser = $agent->browser();
              $versionbrowser = $agent->version($browser);

              /**Register users */

              $daftarImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
              // dd($userCount, Auth::id());
              // dd(Auth::user());
              // dd($daftarImlek, Auth::user());
              if($daftarImlek == 0)
              {
                // dd(Auth::user());
                DB::table('imlek_users')->insert([
                  'user_id' => Auth::id(),
                  'created_at' => now(),
                  'updated_at' => now(),
                  'ip_address' => \Request::ip(),
                  'device' => $agent->device(),
                  'platform' => $agent->platform(),
                  'version_platform' => $version,
                  'browser'  => $agent->browser(),
                  'version_browser' => $versionbrowser,
                  'languages' => $agent->languages()
                ]);
                $newImlek = DB::table('imlek_users')->where('user_id', Auth::id())->count();
                // dd($daftarImlek, $newImlek, Auth::user());
                /** Get footprint **/

                $cookie = $_COOKIE['footprints'];

                if ($cookie != NULL) {
                    // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
                    // $decryptedString = $encrypter->decrypt($cookie);
                    $decryptedString = \Crypt::decrypt($cookie, false);
                    $cookies = explode("|", $decryptedString);
                    // dd($decryptedString, $cookies);
                    $visits = DB::table('visits')->where('cookie_token', $cookies[1])->update([
                        'user_imlek' => Auth::id()
                    ]);
                }
              }
              Session::forget('daftar_imlek_2022');
            }
            $names = explode(' ', trim(Auth::user()->name));
            Session::put('user_name', $names[0]);
            Session::put('user_id', Auth::user()->id);
            if (Session::has('link')) {
                $link = Session::get('link');
                Session::forget('link');
                // dd($link, $exist, Auth::user(), $users);
                return redirect($link);
            }
            return redirect()->route('homeDash')->with('login_aug', 'Login');
        }
        return redirect()->route('homeDash')->with('login_aug', 'Login');
    }

    public function handleFacebookFailed($provider)
    {
        dd("failed");
    }

    public function handleFacebookUnsubscribe($provider)
    {
        dd("unsubscribe");
    }

    public function uploadKTP(Request $request){

        $file = $request->file('attachment');

        // Get File Content
        $fileContent = $file->get();

        // Encrypt the Content
        $encryptedContent = encrypt($fileContent);

        $name = Auth::id().'-'.date('Y-m-d H:i:s').'.dat';

        // Store the encrypted Content
        Storage::put($name, $encryptedContent);

        DB::table('users')->insert([
            'ktp' => $name
        ]);

        //How to get data  in your Controller

        // $encryptedContent = Storage::get('file.dat');
        // $decryptedContent = decrypt($encryptedContent);

        // return view('welcome', compact('encryptedContent', 'decryptedContent'));

        /**How to show data encrypted */
        // <img src="data:image/png;base64,{!! base64_encode($decryptedContent) !!}" alt="" />


        return redirect()->back();

    }

}
