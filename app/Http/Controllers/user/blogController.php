<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class blogController extends Controller
{
    public function like(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'blog_id' => 'required'
        ]);

        $action = "like";

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $check = DB::table('blog_likes')
            ->where('user_id', Auth::id())
            ->where('blog_id', $request->blog_id)
            ->first();
        if ($check) {
            DB::table('blog_likes')
                ->where('user_id', Auth::id())
                ->where('blog_id', $request->blog_id)
                ->delete();

            $action = "dislike";
        } else {
            DB::table('blog_likes')->insert([
                'blog_id' => $request->blog_id,
                'user_id' => Auth::id(),
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $action = "like";
        }

        return response()->json([
            'Status' => 'Success',
            'action' => $action,
            // 'data' => $data
        ]);
    }

    public function comment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'blog_id' => 'required',
            'user_id' => 'required',
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $id = DB::table('blog_comments')->insertGetId([
            'blog_id' => $request->blog_id,
            'user_id' => $request->user_id,
            'comment' => $request->comment,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $user = DB::table('users')->where('id', $request->user_id)->first();

        if ($id && $user) {
            return response()->json([
                'status' => 'success',
                'name' => $user->name,
                'avatar' => $user->avatars,
                'comment' => $request->comment
            ]);
        } else {
            return response()->json([
                'Status' => 'Error',
                'message' => "Internal server error",
            ], 500);
        }
    }

    public function blogPage()
    {
        // if user login, check the status for like button
        if ((Auth::check())) {
            $data['blogs'] = DB::table('blogs')
                ->leftJoin('blog_likes', 'blog_likes.blog_id', 'blogs.id')
                ->select('blogs.*', DB::raw('COUNT(blog_likes.blog_id) as likes'),  DB::raw('(CASE WHEN blog_likes.user_id = ' . Auth::id() . ' AND blog_likes.status = 1 THEN true ELSE false END) as likeStatus'))
                ->groupBy('blogs.id')
                ->orderBy('blogs.created_at', 'desc')
                ->get();
        } else {
            $data['blogs'] = DB::table('blogs')
                ->leftJoin('blog_likes', 'blog_likes.blog_id', 'blogs.id')
                ->select('blogs.*', DB::raw('COUNT(blog_likes.blog_id) as likes'),  DB::raw('(blog_likes.status = 0) as likeStatus'))
                ->groupBy('blogs.id')
                ->orderBy('blogs.created_at', 'desc')
                ->get();
        }

        // dd($data); 
        return view('blogs.pages.home', $data);
    }

    public function detailBlogPage($id)
    {
        
        if (Auth::check()) { 
            $data['blog'] = DB::table('blogs')
                ->leftJoin('blog_likes', 'blog_likes.blog_id', 'blogs.id')
                ->select('blogs.*', 'users.name', DB::raw('COUNT(blog_likes.blog_id) as likes'),  DB::raw('(CASE WHEN blog_likes.user_id = ' . Auth::id() . ' THEN true ELSE false END) as likeStatus'))
                ->groupBy('blogs.id')
                ->orderBy('blogs.created_at', 'desc')
                ->join('users', 'users.id', 'blogs.user_id')
                ->where('blogs.id', $id)
                ->first();
            if(DB::table('blog_likes')->where('blog_id', $id)->where('user_id', Auth::id())->first()){
                $data['blog']->likeStatus = 1;
            }
            else{
                $data['blog']->likeStatus = 0;
            }
            $data['blogs'] = DB::table('blogs')->where('user_id', '<>', Auth::id())->orderBy('created_at', 'desc')->take(3)->get();
        } else {
            $data['blog'] = DB::table('blogs')
                ->leftJoin('blog_likes', 'blog_likes.blog_id', 'blogs.id')
                ->select('blogs.*', 'users.name', DB::raw('COUNT(blog_likes.blog_id) as likes'),  DB::raw('(blog_likes.status = 0) as likeStatus'))
                ->groupBy('blogs.id')
                ->orderBy('blogs.created_at', 'desc')
                ->join('users', 'users.id', 'blogs.user_id')
                ->where('blogs.id', $id)
                ->first();
            $data['blogs'] = DB::table('blogs')->where('blogs.id', '<>', $id)->orderBy('created_at', 'desc')->take(3)->get();
        }

        $data['comments'] = DB::table('blog_comments')
            ->select('blog_comments.*', 'users.name', 'users.avatars')
            ->join('users', 'users.id', 'blog_comments.user_id')
            ->where('blog_comments.blog_id', $id)->take(7)->get();

        $data['totalComments'] = DB::table('blog_comments')->where('blog_id', $id)->count();

        // dd($data); 
        return view('blogs.pages.detail-blog', $data);
    }

    public function loadMoreComment(Request $request)
    {

        $data = DB::table('blog_comments')
            ->select('blog_comments.*', 'users.name', 'users.avatars')
            ->join('users', 'users.id', 'blog_comments.user_id')
            ->where('blog_id', $request->blog_id)->paginate(7);


        if ($data) {
            return response()->json([
                'status' => 'success',
                'data' => $data,
            ]);
        } else {
            return response()->json([
                'Status' => 'Error',
                'message' => "Internal server error",
            ], 500);
        }
    }
}
