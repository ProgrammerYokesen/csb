<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Session;

class resetPassword extends Controller
{
    public function sendEmail(Request $request){
        $client = new Client();
        $send = $client->post('https://mailgun.sobatbadak.club/api/v1/forgot-password', [
          'form_params' => [
            'email' => $request->email
          ]
        ]);
        $data = json_decode($send->getBody());
        /**
         * Contoh data response
         *   "Status": "Success"
         *   "Message": "Email reset password telah terkirim"
         */
        if($data->Status == "Success"){
            // return redirect()->route('forgotPage')->with('Success', 'Email reset password telah terkirim');
            return redirect()->route('forgotDone');
        }
        else{
            return redirect()->route('forgotPage')->with('Failed', $data->Message);
        }

    }
    public function resetPass(Request $request){
        $user = DB::table('users')->where('forgot_password_token', Session::get('token'))->first();
        if($user){
            DB::table('users')->where('id', $user->id)->update([
                'password' => \Hash::make($request->password)
            ]);
            return redirect()->route('loginPage')->with('Success', 'Berhasil');
        }
        return redirect()->route('resetPass')->with('Failed', 'Token invalid');
    }
}
