<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class voucherController extends Controller
{
    public function redeem(Request $request){
        $date = date('Y-m-d H:i:s');
        $check = DB::table('vouchers')->where('voucher', $request->voucher)->first();
        $usage = DB::table('voucher_usages')->where('voucher_id', $check->id)->count();
        if($usage < $check->qty){
            /**Insert poin */
            if($check->start_voucher == NULL){            
                DB::table('voucher_usages')->insert([
                    'user_id' => Auth::id(),
                    'voucher_id' => $check->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                DB::table('user_poins')->insert([
                    'user_id' => Auth::id(),
                    'poin' => $check->poin,
                    'from' => 'Redeem Voucher',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            
                /**Update total baper poin */

                $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
                $newPoint = $baperPoin + $check->poin;
                DB::table('users')->where('id', Auth::id())->update([
                    'baper_poin' => $newPoint
                ]);

                return redirect()->route('editProfile')->with('Voucher', $check->poin);
            }
            else{
                $check = DB::table('vouchers')->where('voucher', $request->voucher)
                        ->whereDate('vouchers.start_voucher', '<', $date)
                        ->whereDate('vouchers.end_voucher', '>', $date)
                        ->first();
                if($check){
                    DB::table('voucher_usages')->insert([
                        'user_id' => Auth::id(),
                        'voucher_id' => $check->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    DB::table('user_poins')->insert([
                        'user_id' => Auth::id(),
                        'poin' => $check->poin,
                        'from' => 'Redeem Voucher',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    /**Update total baper poin */

                    $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
                    $newPoint = $baperPoin + $check->poin;
                    DB::table('users')->where('id', Auth::id())->update([
                        'baper_poin' => $newPoint
                    ]);
                    
                    return redirect()->route('editProfile')->with('Voucher', $check->poin); 
                }
                else{
                    return redirect()->route('editProfile')->with('Voucher', 0);
                }
            }
        }
        /**Voucher doesn't exist */
        else{
            return redirect()->route('editProfile')->with('Voucher', 0);
        }
    }

}
