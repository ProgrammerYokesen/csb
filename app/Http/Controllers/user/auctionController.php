<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class auctionController extends Controller
{

    public function bid(Request $request){

        /**Check if user have active bid in other auction */
        $checkActive = DB::table('auction_specials')
        ->where('status', 1)
        ->where('winner_status', 0)
        ->where(function ($query)  {
            $query->where('auction_specials.starttime', '<=', now('Asia/Jakarta'));
        })
        ->where(function ($query)  {
            $query->where('auction_specials.end_date', '>=', now('Asia/Jakarta'));
        })
        ->first();

        if(Auth::user()->whatsapp_verification == 0||Auth::user()->alamat_rumah == null||Auth::user()->ktp_verification !== 1)
        {
          return response()->json([
              'Status' => 'Error',
              'Message' => 'User belum terverif',
              'Code' => 400
          ]);
        }

        if($checkActive){
            /**check bid */
            $check = DB::table('auction_special_bids')->where('status_bid', 1)->where('auction_id', $checkActive->id)->where('user_id', Auth::id())->first();
            if($check){
                return response()->json([
                    'Status' => 'Error',
                    'Message' => 'Tidak bisa mengikuti dua bid bersamaan',
                    'Code' => 5
                ]);
            }
        }
        /** */

        $auction = DB::table('auctions')->where('uuid', $request->auction_id)->first();
        $auctionId = $auction->id;
        /**Check if auction still active */
        $checkActive = DB::table('auctions')
                ->where('uuid', $request->auction_id)
                ->where('status', 1)
                ->where('winner_status', 0)
                ->where(function ($query)  {
                    $query->where('auctions.starttime', '<=', now('Asia/Jakarta'));
                })
                ->where(function ($query)  {
                    $query->where('auctions.end_date', '>=', now('Asia/Jakarta'));
                })
                ->first();
        if(!$checkActive){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 4
            ]);
        }
        /**Check bid condition */
        $highestBid = DB::table('auction_bids')->where('auction_bids.auction_id', $auctionId)->max('bid_price');
        $kelipatan = DB::table('auctions')->where('uuid', $request->auction_id)->first()->kelipatan;
        $baperPoin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
        /**Check lelang telah berakhir */
        if($auction->winner_status == 1){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 4
            ]);
        }
        /**Check kelipatan apakah benar */
        if(fmod($request->price, $kelipatan) != 0){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 0
            ]);
        }
        /**Bid lebih kecil dari bid tertinggi */
        elseif($request->price <= $highestBid){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 2
            ]);
        }

        /**Baper poin kurang */
        elseif($request->price > $baperPoin){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 3
            ]);
        }
        DB::table('auction_bids')->insert([
            'user_id' => Auth::id(),
            'auction_id' => $auctionId,
            'bid_price' => $request->price,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // Modul Pompom

        return response()->json([
            'Status' => 'Success',
            'Message' => 'Bid berhasil di tambahkan',
            'Code' => 1
        ]);
    }

    public function refreshLeaderboard(Request $request){
        $leaderBoards = DB::table('auction_bids')
        ->select('auction_bids.created_at', 'users.name', 'auction_bids.bid_price', 'auction_bids.user_id')
        ->where('auction_bids.auction_id', $request->id)
        ->leftJoin('users', 'users.id', 'auction_bids.user_id')
        ->limit(10)
        ->orderByDesc('bid_price')
        ->get();

        foreach($leaderBoards as $key => $value)
        {
          $leaderBoards[$key]->badge = badgeUser($value->user_id);
        }
        $highestBid = DB::table('auction_bids')->where('auction_bids.auction_id', $request->id)->max('bid_price');

        return response()->json([
            'leaderBoards' => $leaderBoards,
            'highestBid' => $highestBid
        ]);
    }

    public function lastWinner(){
        sleep(3);
        $auction = DB::table('auction_bids')
                    ->select('users.name', 'auction_bids.bid_price')
                    ->join('users', 'users.id', 'auction_bids.user_id')
                    ->orderByDesc('auction_bids.id')->first();
        return response()->json(compact('auction'));
    }

    public function submitRumah(Request $request){
        if($request->alamat == NULL){
            if(Auth::user()->whatsapp_verification == 0){
                return redirect()->route('homeDash')->with('verifWhatsapp', 0);
            }
            else{
                return redirect()->route('auctionPage');
            }
        }
        DB::table('users')->where('id', Auth::id())->update([
            'alamat_rumah' => $request->alamat
        ]);
        if(Auth::user()->whatsapp_verification == 0){
            return redirect()->route('homeDash')->with('verifWhatsapp', 0);
        }
        return redirect()->route('auctionPage');
    }

    public function bidSpecial(Request $request){

        /**Check user register date after 01 october 2021 */
        // if(strtotime(Auth::user()->created_at) < 1633021200){
        //     return response()->json([
        //         'Status' => 'Error',
        //         'Message' => 'Registrasi user sebelum tanggal 1 oktober 2021',
        //         'Code' => 7,
        //     ]);
        // }
        /** */

        $setting = DB::table('auction_special_settings')->first();

        /**Check if user have active bid in other auction */
        $checkActive = DB::table('auctions')
        ->where('status', 1)
        ->where('winner_status', 0)
        ->where(function ($query)  {
            $query->where('auctions.starttime', '<=', now('Asia/Jakarta'));
        })
        ->where(function ($query)  {
            $query->where('auctions.end_date', '>=', now('Asia/Jakarta'));
        })
        ->first();
        if(Auth::user()->whatsapp_verification == 0||Auth::user()->alamat_rumah == null||Auth::user()->ktp_verification !== 1)
        {
          return response()->json([
              'Status' => 'Error',
              'Message' => 'User belum terverif',
              'Code' => 400
          ]);
        }
        if($checkActive){
            /**check bid */
            $check = DB::table('auction_bids')->where('auction_id', $checkActive->id)->where('user_id', Auth::id())->first();
            if($check){
                return response()->json([
                    'Status' => 'Error',
                    'Message' => 'Tidak bisa mengikuti dua bid bersamaan',
                    'Code' => 5
                ]);
            }
        }
        /** */

        /**Validasi for baper poin user > 500000 */
        $poin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
        if($poin > $setting->baper_poin_maksimal){
            return response()->json([
                'Status' => 'Error',
                'Message' => "Baper poin lebih dari ".$setting->baper_poin_maksimal,
                'Code' => 6
            ]);
        }
        /** */


        $auction = DB::table('auction_specials')->where('uuid', $request->auction_id)->first();
        $auctionId = $auction->id;
        /**Check if auction still active */
        $checkActive = DB::table('auction_specials')
                ->where('uuid', $request->auction_id)
                ->where('status', 1)
                ->where('winner_status', 0)
                ->where(function ($query)  {
                    $query->where('auction_specials.starttime', '<=', now('Asia/Jakarta'));
                })
                ->where(function ($query)  {
                    $query->where('auction_specials.end_date', '>=', now('Asia/Jakarta'));
                })
                ->first();
        if(!$checkActive){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 4
            ]);
        }
        /**Check bid condition */
        $highestBid = DB::table('auction_special_bids')->where('auction_special_bids.status_bid', 1)->where('auction_special_bids.auction_id', $auctionId)->max('bid_price');
        $kelipatan = DB::table('auction_specials')->where('uuid', $request->auction_id)->first()->kelipatan;
        $baperPoin = DB::table('user_poins')->where('user_id', Auth::id())->where('status', 1)->sum('poin');
        /**Check lelang telah berakhir */
        if($auction->winner_status == 1){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 4
            ]);
        }
        /**Check kelipatan apakah benar */
        if(fmod($request->price, $kelipatan) != 0){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 0
            ]);
        }
        /**Bid lebih kecil dari bid tertinggi */
        elseif($request->price <= $highestBid){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 2
            ]);
        }

        /**Baper poin kurang */
        elseif($request->price > $baperPoin){
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Bid gagal ditambahkan',
                'Code' => 3
            ]);
        }
        DB::table('auction_special_bids')->insert([
            'user_id' => Auth::id(),
            'auction_id' => $auctionId,
            'bid_price' => $request->price,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // Modul Pompom

        return response()->json([
            'Status' => 'Success',
            'Message' => 'Bid berhasil di tambahkan',
            'Code' => 1
        ]);
    }

    public function refreshLeaderboardSpecial(Request $request){
        $leaderBoards = DB::table('auction_special_bids')
        ->select('auction_special_bids.created_at', 'users.name', 'auction_special_bids.bid_price')
        ->where('auction_special_bids.auction_id', $request->id)
        ->leftJoin('users', 'users.id', 'auction_special_bids.user_id')
        ->limit(10)
        ->where('status_bid', 1)
        ->orderByDesc('bid_price')
        ->get();
        foreach($leaderBoards as $key => $value)
        {
          $leaderBoards[$key]->badge = badgeUser($value->user_id);
        }
        $highestBid = DB::table('auction_special_bids')->where('status_bid', 1)->where('auction_special_bids.auction_id', $request->id)->max('bid_price');
        return response()->json([
            'leaderBoards' => $leaderBoards,
            'highestBid' => $highestBid
        ]);
    }

    public function lastWinnerSpecial(){
        sleep(4);
        $auction = DB::table('auction_special_bids')
                    ->select('users.name', 'auction_special_bids.bid_price')
                    ->join('users', 'users.id', 'auction_special_bids.user_id')
                    ->orderByDesc('auction_special_bids.id')->first();
        return response()->json(compact('auction'));
    }

}
