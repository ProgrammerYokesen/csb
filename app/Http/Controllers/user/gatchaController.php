<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class gatchaController extends Controller
{
    public function checkInvoice(Request $request)
    {
        $check = DB::table('orders')
            ->where(function ($query) {
                $query->where('sku', 'STW')
                    ->orWhere('sku', 'STW-LECI')
                    ->orWhere('sku', 'STW-JAMBU')
                    ->orWhere('sku', 'STW-JERUK')
                    ->orWhere('sku', 'WIB-JAMBU')
                    ->orWhere('sku', 'WIB-LECI')
                    ->orWhere('sku', 'WIB-JERUK')
                    ->orWhere('sku', 'HARBOLNAS')
                    ->orWhere('sku', 'WIB');
            })
            ->where('invoice', $request->invoice)
            ->first();

        if ($check) {
            /** Invoice Ditemukan */
            if ($check->status_redeem == 0) {
                /**Bisa di redeem */
                return \Redirect::back()->with([
                    'Status' => 'Success',
                    'Data' => $check,
                    'Code' => 1
                ]);
            } else {
                /**Sudah di redeem */
                return \Redirect::back()->with([
                    'Status' => 'Success',
                    'Code' => 2
                ]);
            }
        } else {
            /**Check orders where invoice exist but sku not STW */
            $order = DB::table('orders')
                ->where('invoice', $request->invoice)
                ->first();
            if ($order) {
                return \Redirect::back()->with([
                    'Status' => 'Error',
                    'Code' => 4
                ]);
            }
            /** Invoice Tidak Ditemukan */
            return \Redirect::back()->with([
                'Status' => 'Error',
                'Code' => 0
            ]);
        }
    }

    public function gachaTiket(Request $request)
    {
        DB::table('orders')->where('invoice', $request->invoice)->update([
            'status_redeem' => 1
        ]);
        $data = DB::table('orders')->where('invoice', $request->invoice)->first();
        $check = DB::table('gachas')->where('order_id', $data->id)->first();
        if (!$check) {
            DB::table('gachas')->insert([
                'user_id' => Auth::id(),
                'order_id' => $data->id,
                'qty' => $data->qtyTotal,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        return redirect()->route('gachaTicket');
    }

    public function gacha()
    {
        $check = $this->checkSisa();
        if ($check > 0) {
            /**Bisa gacha */
            $ticketId = $this->gachaAlgorithm();
            DB::table('ticket')->insert([
                'category_gatcha_id' => $ticketId,
                'user_id' => Auth::id(),
                'spinwheel_status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $hasilGacha = DB::table('category_gacha')->where('id', $ticketId)->first();
            if ($hasilGacha->cat_name == 'Premium') {
                $image = 'images/premium.png';
            } elseif ($hasilGacha->cat_name == 'Reguler') {
                $image = 'images/reguler.png';
            }
            $sisaTicket = $this->checkSisa();
            return response()->json([
                'Status' => 'Success',
                'Code' => 1,
                'Ticket' => $hasilGacha->cat_name,
                'Image' => $image,
                'sisaTiket' => $sisaTicket
            ]);
        } else {
            /**Gak bisa gacha */
            return response()->json([
                'Status' => 'Error',
                'Code' => 0
            ]);
        }
    }

    public function checkSisa()
    {
        $penukaran = DB::table('gachas')
            ->where('user_id', Auth::id())
            ->sum('qty');
        $gacha = DB::table('ticket')->where('user_id', Auth::id())->count();
        $sisa = $penukaran - $gacha;
        return $sisa;
    }

    public function gachaAlgorithm()
    {
        // $gatcha = DB::table('category_gacha')->get();
        $temp = [];

        /**Check gacha user ke 9 dapet Premium */
        $done = DB::table('ticket')->where('user_id', Auth::id())->count();
        if ($done % 9 == 8) {
            $id = DB::table('category_gacha')->where('cat_name', 'Premium')->first()->id;
            return $id;
        }

        /**
         *  80% Reguler 20% Premium 
         * 4,8 Premium Lainnya reguler
         * */

        $randomNumber = rand(0, 9);
        if ($randomNumber == 8 || $randomNumber == 4) {
            /**Get Reguler ticket */
            $id = DB::table('category_gacha')->where('cat_name', 'Premium')->first()->id;
            return $id;
        } else {
            /**Get Premium ticket */
            $id = DB::table('category_gacha')->where('cat_name', 'Reguler')->first()->id;
            return $id;
        }
    }

    public function spinWheel(Request $request)
    {
        /**
         * Check Kesempatan Spinwheel
         */
        $ticket = DB::table('ticket')->where('category_gatcha_id', $request->type)
            ->where('user_id', Auth::id())
            ->where('spinwheel_status', 0)
            ->count();
        if ($ticket > 0) {
            /**Algoritm gacha */

            $sudut = $this->spinwheelAlgorithm($request->type);

            /**Get sisa ticket user */

            $ticketPremium = DB::table('ticket')
                ->where('user_id', Auth::id())
                ->where('category_gatcha_id', 1)
                ->where('spinwheel_status', 0)
                ->count();

            $ticketReguler = DB::table('ticket')
                ->where('user_id', Auth::id())
                ->where('category_gatcha_id', 2)
                ->where('spinwheel_status', 0)
                ->count();

            /**return json */

            return response()->json([
                'status' => 'Success',
                'code' => 1,
                'sudut' => $sudut,
                'ticketPremium' => $ticketPremium,
                'ticketReguler' => $ticketReguler
            ]);
        } else {
            return response()->json([
                'status' => 'Success',
                'code' => 0
            ]);
        }
    }

    public function spinwheelAlgorithm($id)
    {
        $data = DB::table('spinwheel_list_poin')
            ->select('spinwheel_list_poin.id', 'spinwheel_list_poin.point', 'category_gacha.cat_name')
            ->join('category_gacha', 'category_gacha.id', 'spinwheel_list_poin.cat_id')
            ->where('spinwheel_list_poin.cat_id', $id)
            ->where('spinwheel_list_poin.status', 1)
            ->get();
        $totalData = $data->count();

        $randomNumber = rand(1, $totalData); //2
        $index = $randomNumber - 1; //1

        $output = ($randomNumber / $totalData) * 360; // 32
        $i = (360 / $totalData) - 1; //15
        $rnd = rand(1, $i); //14
        $output = $output - $rnd; //18

        $data = $data[$index];
        $cat = DB::table('category_gacha')->where('id', $data->cat_id)->first();

        /**Change ticket */
        DB::table('ticket')->where('category_gatcha_id', $id)
            ->where('user_id', Auth::id())
            ->where('spinwheel_status', 0)
            ->limit(1)
            ->update([
                'spinwheel_status' => 1
            ]);

        /**Insert to user poin */
        DB::table('user_poins')->insert([
            'user_id' => Auth::id(),
            'poin' => $data->point,
            'fk_id' => $data->id,
            'from' => 'Spin It Yourself - ' . $data->cat_name,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        /**Update total baper poin */

        $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
        $newPoint = $baperPoin + $data->point;
        DB::table('users')->where('id', Auth::id())->update([
            'baper_poin' => $newPoint
        ]);


        return $output;
    }

    public function spinwheelAdmin(Request $request)
    {
        /**Check user email */
        $user = DB::table('users')->where('email', $request->email)->first();
        if ($user) {
            /**Algoritm gacha */

            $sudut = $this->spinwheelAlgorithmAdmin($request->type, $user->id);

            /**Get sisa ticket user */

            $ticketPremium = DB::table('ticket')
                ->where('user_id', Auth::id())
                ->where('category_gatcha_id', 1)
                ->where('spinwheel_status', 0)
                ->count();

            $ticketReguler = DB::table('ticket')
                ->where('user_id', Auth::id())
                ->where('category_gatcha_id', 2)
                ->where('spinwheel_status', 0)
                ->count();

            /**return json */

            return response()->json([
                'status' => 'Success',
                'code' => 1,
                'sudut' => $sudut,
                'ticketPremium' => $ticketPremium,
                'ticketReguler' => $ticketReguler
            ]);
        }
    }
    
    public function spinwheelAlgorithmAdmin($id, $user_id)
    {
        $data = DB::table('cat_gacha_points')
            ->select('cat_gacha_points.id', 'cat_gacha_points.point', 'category_gacha.cat_name')
            ->join('category_gacha', 'category_gacha.id', 'cat_gacha_points.cat_id')
            ->where('cat_gacha_points.cat_id', $id)
            ->where('cat_gacha_points.status', 1)
            // ->orderBy('cat_gacha_points.point', 'asc')
            ->get();
        $totalData = $data->count();

        $randomNumber = rand(1, $totalData); //2
        $index = $randomNumber - 1; //1

        $output = ($randomNumber / $totalData) * 360; // 32
        $i = (360 / $totalData) - 1; //15
        $rnd = rand(1, $i); //14
        $output = $output - $rnd; //18

        $data = $data[$index];
        $cat = DB::table('category_gacha')->where('id', $data->cat_id)->first();

        /**Change ticket */
        DB::table('ticket')->where('category_gatcha_id', $id)
            ->where('user_id', Auth::id())
            ->where('spinwheel_status', 0)
            ->limit(1)
            ->update([
                'spinwheel_status' => 1
            ]);

        /**Insert to user poin */
        DB::table('user_poins')->insert([
            'user_id' => $user_id,
            'poin' => $data->point,
            'fk_id' => $data->id,
            'from' => 'Spin It Yourself - ' . $data->cat_name,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        return $output;
    }
}
