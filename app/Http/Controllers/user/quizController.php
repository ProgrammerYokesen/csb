<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Session;

class quizController extends Controller
{
    public function rejekiSobat(Request $request){
        $temp_poin = 0;
        $poin = 0;
        $check = DB::table('user_poins')
                 ->where('user_id', Auth::id())
                 ->where('fk_id', $request->quizId)
                 ->where('from', 'quiz rejeki sobat')
                 ->count();
        if($check != 0){
            return redirect()->route('winPage');
        }
        // dd($request->except(['_token', 'quizId']));
        foreach ($request->except(['_token', 'quizId']) as $key => $value) {
            $answerStatus = $this->checkAnswer($key, $value);
            if($answerStatus == 1){
                $poin = DB::table('pertanyaan_rejeki_sobat')->where('id', $key)->first()->poin;
                $temp_poin = $temp_poin + $poin;
                // echo $temp_poin.'<br>';
            }
            DB::table('jawaban_users')->insert([
                'user_id' => Auth::user()->id,
                'pertanyaan_id' => $key,
                'jawaban_user' => $value,
                'quiz_id' => $request->quizId,
                'status_jawaban' => $answerStatus,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
          }

          /**Check max poin if $temp_poin > $maxPoin return redirect back */
            date_default_timezone_set('Asia/Jakarta');
            $date = date('Y-m-d H:i:s');
            $maxPoin = DB::table('quiz_rejeki_sobat')
            ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d')
            ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
            ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->sum('poin');

            if($temp_poin > $maxPoin){
                abort(406);
            }
          /** */

          DB::table('user_poins')->insert([
            'user_id' => Auth::id(),
            'poin' => $temp_poin,
            'fk_id' => $request->quizId,
            'from' => 'quiz rejeki sobat',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        /**Update total baper poin */

        $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
        $newPoint = $baperPoin + $temp_poin;
        DB::table('users')->where('id', Auth::id())->update([
            'baper_poin' => $newPoint
        ]);

        $request->session()->put('poinRejekiSobat', $temp_poin);
        $request->session()->put('quizId', $request->quizId);
        return redirect()->route('winPage');
    }

    /**
     * function to check answer
     */
    private function checkAnswer($id, $answer){
        $soal = DB::table('pertanyaan_rejeki_sobat')->where('id', $id)->first();
        if($soal->jawaban_benar == $answer){
            return 1;
        }
        else{
            return 0;
        }
    }

}
