<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class searchController extends Controller
{
    public function searchAuction(Request $request){
        $date = date('Y-m-d H:i:s');
        if($request->has('q')){
            $riwayatLelang = DB::table('auctions')
            ->select('auctions.id','auctions.name', 'auctions.photo', 'auctions.starttime', 'users.name as username', 'auction_bids.bid_price as bidTertinggi')
            ->leftJoin('auction_bids', 'auction_bids.auction_id', 'auctions.id')
            ->join('users', 'users.id', 'auction_bids.user_id')
            ->groupBy('auctions.id')
            ->where('auction_bids.winner_status', 1)
            ->where('auctions.end_date', '<', $date)
            ->where(function ($query) use ($request) {
                $query->where('auctions.name', 'like', $request->q.'%')
                      ->orWhere('auctions.name', 'like', '%'.$request->q.'%')
                      ->orWhere('auctions.name', 'like', '%'.$request->q)
                      ->orWhere('users.name', 'like', '%'.$request->q.'%')
                      ->orWhere('users.name', 'like', '%'.$request->q);
            })
            ->orderByDesc('auctions.id')
            ->limit(4)
            ->get();
            $view = view('dashboard.search.auction-search',compact('riwayatLelang'))->render();
            return response()->json(['html'=>$view]);
        }
        else{
            $riwayatLelang = DB::table('auctions')
            ->select('auctions.id','auctions.name', 'auctions.photo', 'auctions.starttime', 'users.name as username', 'auction_bids.bid_price as bidTertinggi')
            ->leftJoin('auction_bids', 'auction_bids.auction_id', 'auctions.id')
            ->join('users', 'users.id', 'auction_bids.user_id')
            ->groupBy('auctions.id')
            ->where('auction_bids.winner_status', 1)
            ->where('auctions.end_date', '<', $date)
            ->orderByDesc('auctions.id')
            ->limit(4)
            ->get();
            $view = view('dashboard.search.auction-search',compact('riwayatLelang'))->render();
            return response()->json(['html'=>$view]);
        }
    }
    public function searchLelang(Request $request){
        $date = date('Y-m-d H:i:s');
        if($request->has('q')){
            $riwayatLelang = DB::table('auctions')
            ->select('auctions.id','auctions.name', 'auctions.photo', 'auctions.starttime', 'users.name as username', 'auction_bids.bid_price as bidTertinggi')
            ->leftJoin('auction_bids', 'auction_bids.auction_id', 'auctions.id')
            ->join('users', 'users.id', 'auction_bids.user_id')
            ->groupBy('auctions.id')
            ->where('auction_bids.winner_status', 1)
            ->where('auctions.end_date', '<', $date)
            ->where(function ($query) use ($request) {
                $query->where('auctions.name', 'like', $request->q.'%')
                      ->orWhere('auctions.name', 'like', '%'.$request->q.'%')
                      ->orWhere('auctions.name', 'like', '%'.$request->q)
                      ->orWhere('users.name', 'like', '%'.$request->q.'%')
                      ->orWhere('users.name', 'like', '%'.$request->q);
            })
            ->orderByDesc('auctions.id')
            ->limit(4)
            ->get();
            $view = view('dashboard.search.lelang-search',compact('riwayatLelang'))->render();
            return response()->json(['html'=>$view]);
        }
        else{
            $riwayatLelang = DB::table('auctions')
            ->select('auctions.id','auctions.name', 'auctions.photo', 'auctions.starttime', 'users.name as username', 'auction_bids.bid_price as bidTertinggi')
            ->leftJoin('auction_bids', 'auction_bids.auction_id', 'auctions.id')
            ->join('users', 'users.id', 'auction_bids.user_id')
            ->groupBy('auctions.id')
            ->where('auction_bids.winner_status', 1)
            ->where('auctions.end_date', '<', $date)
            ->orderByDesc('auctions.id')
            ->limit(4)
            ->get();
            $view = view('dashboard.search.lelang-search',compact('riwayatLelang'))->render();
            return response()->json(['html'=>$view]);
        }
    }
}
