<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Auth;

class gameController extends Controller
{
    public function satu_submit(Request $request, $id){
          // $kata = ['T','E','R','B','A','I','K'];
          $word = DB::table('tebak_kata')->where('id', $id)->first();
          $kata_split = str_split($word->kata);
          $kata = array_unique($kata_split);
  
  
          $huruf = strtoupper($request->huruf);
          // dd($huruf);
  
          if(empty(Session::get('tebak'))){
              $tebak[0] = $huruf;
              if(in_array($huruf, $kata)){
                  $last = "betul";
              }else{
                  $last = "salah";
              }
          }else{
              $tebak = Session::get('tebak');
              if (in_array($huruf, $tebak)) {
                  $last = Session::get('last');
              }else{
                  array_push($tebak,$huruf);
                  if(in_array($huruf, $kata)){
                  $last = "betul";
              }else{
                  $last = "salah";
              }
  
              }
          }
  
          $nilai = 0;
          $salah = 0;
          foreach ($tebak as $value) {
              if(in_array($value, $kata)){
                  $nilai += 1;
              }else{
                  $salah += 1;
              }
          }
  
          $percent_nilai = $nilai / count($kata);
          $percent_salah = ($nilai - $salah) / count($kata);
          // dd($percent_nilai);
  
  
          $request->session()->put('last', $last);
          $request->session()->put('salah', $salah);
          $request->session()->put('nilai', $percent_nilai);
          $request->session()->put('nilai_salah', $percent_salah);
          $request->session()->put('tebak', $tebak);
          $page = (int)Session::get('currentPage');
          return redirect(env('APP_URL').'/tebak-kata/today-quiz?page='.$page);
      }
  
      public function satu_reset($id){
        $word = DB::table('tebak_kata')->where('id', $id)->first();
        if(Session::get('nilai') == 1 || Session::get('salah') == 3){
            if(Session::get('nilai') == 1){
                $statusJawaban = 1;
                $answer = $word->kata;
            }
            elseif(Session::get('salah') == 3){
                $statusJawaban = 0;
                $answer = implode(Session::get('tebak'));
            }
            DB::table('jawaban_tebak_kata')->insert([
                'user_id' => Auth::id(),
                'quiz_tebak_kata_id' => Session::get('quizId'),
                'jawaban_user' => $answer,
                'status_jawaban' => $statusJawaban,
                'pertanyaan_id' => Session::get('pertanyaanId'),
                'poin' => $word->poin,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $page =(int)Session::get('currentPage') +1;
            if(Session::get('currentPage') == Session::get('lastPage')){
              $quizId = Session::get('quizId');
              $totalPoin = DB::table('jawaban_tebak_kata')->where('user_id', Auth::id())->where('status_jawaban', 1)->where('quiz_tebak_kata_id', $quizId)->sum('poin');
              DB::table('user_poins')->insert([
                  'user_id' => Auth::id(),
                  'poin' => $totalPoin,
                  'fk_id' => $quizId,
                  'from' => 'tebak-kata',
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s')
              ]);
              //Rubah semua status jadi true disini
              DB::table('jawaban_tebak_kata')->where('user_id', Auth::id())->where('quiz_tebak_kata_id', $quizId)->update([
                  'status_pengerjaan' => 1
              ]);
              //End
              Session::forget([
                'currentPage', 'last', 'salah', 'nilai', 'nilai_salah', 'tebak', 'lastPage'
              ]);
            
              /**Update total baper poin */

              $baperPoin = DB::table('users')->where('id', Auth::id())->first()->baper_poin;
              $newPoint = $baperPoin + $totalPoin;
              DB::table('users')->where('id', Auth::id())->update([
                    'baper_poin' => $newPoint
                ]);

              return redirect()->route('winPageTebakKata');
            }
            Session::forget([
                'currentPage', 'last', 'salah', 'nilai', 'nilai_salah', 'tebak'
              ]);
            return redirect(env('APP_URL').'/tebak-kata/today-quiz?page='.$page);
        }
          // return redirect()->route('minigame', [$id]);
      }

      public function joinPanjatPinang(Request $request){
          $month = date('d');

          $jam = date('H');
        //   if($month == 17 && $jam >= 15){
        //     DB::transaction(function () {
        //     $gameId = DB::table('game_panjat_pinangs')->insertGetId([
        //         'user_id' => Auth::id(),
        //         'status' => 1,
        //         'created_at' => date('Y-m-d H:i:s'),
        //         'updated_at' => date('Y-m-d H:i:s')
        //     ]);
        //       /**Kurangi baper poin 1000 */
        //       DB::table('user_poins')->insert([
        //         'user_id' => Auth::id(),
        //         'poin' => -1000,
        //         'fk_id' => $gameId,
        //         'from' => 'Ikut Game Panjat Pinang',
        //         'created_at' => date('Y-m-d H:i:s'),
        //         'updated_at' => date('Y-m-d H:i:s')
        //         ]);
        //     }, 5);
        //   }
        //   else{

            $gameId = DB::table('game_panjat_pinangs')->insertGetId([
                'user_id' => Auth::id(),
                'status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        //   }

         return response()->json([
             'Status' => 'Success',
             'Message' => 'Berhasil mengikuti game panjat pinang'
         ]);
      }

      public function endPanjatPinang(Request $request){
          $gameId = DB::table('game_panjat_pinangs')
                    ->where('user_id', Auth::id())
                    ->where('game_end', 0)
                    ->orderByDesc('id')
                    ->first();
          if($gameId){
              $gameId = $gameId->id;
              DB::table('game_panjat_pinangs')->where('id', $gameId)->update([
                  'time' => $request->time,
                  'game_end' => 1,
                  'game_end_date' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s')
              ]);
              return response()->json([
                'Status' => 'Success',
                'Message' => 'Berhasil menyelesaikan game panjat pinang'
            ]);
          }
      }

      public function leaderboardPanjatPinang(){
        $leaderBoard = DB::table('game_panjat_pinangs')
                 ->select('game_panjat_pinangs.user_id', 'users.name', 'game_panjat_pinangs.created_at')
                 ->join('users', 'users.id', 'game_panjat_pinangs.user_id')
                 ->where('game_panjat_pinangs.status', 1)
                 ->groupBy('user_id')
                 ->get();
        foreach($leaderBoard as $ky => $lb){
            $value = DB::table('game_panjat_pinangs')
                    ->where('user_id', $lb->user_id)
                    ->max('time');
            $leaderBoard[$ky]->time = $value;
        }

        $leaderBoard = $leaderBoard->sortByDesc('time')->values();
        $temp = [];
        foreach($leaderBoard as $key => $value){
            $Index = $key + 1;
            if($value->time == $leaderBoard[$Index]->time){
                $time1 = strtotime($value->created_at);
                $time2 = strtotime($leaderBoard[$Index]->created_at);
                if($time1 < $time2){
                    $temp = $leaderBoard[$key];
                    $leaderBoard[$key] = $leaderBoard[$Index];
                    $leaderBoard[$Index] = $temp;
                }
            }
        }
          return response()->json([
              'Status' => 'Success',
              'Data' => $leaderBoard
          ]);
      }

      public function joinTambalBan(Request $request){
        $month = date('d');
        $jam = date('H');
        // if($month == 17 && $jam >= 15){
        //     DB::transaction(function () {   
        //         $gameId = DB::table('pompa_ban')->insertGetId([
        //             'user_id' => Auth::id(),
        //             'status' => 1,
        //             'created_at' => date('Y-m-d H:i:s'),
        //             'updated_at' => date('Y-m-d H:i:s')
        //         ]);
        //           /**Kurangi baper poin 1000 */
        //           DB::table('user_poins')->insert([
        //             'user_id' => Auth::id(),
        //             'poin' => -1000,
        //             'fk_id' => $gameId,
        //             'from' => 'Ikut Game Tambal Ban Online',
        //             'created_at' => date('Y-m-d H:i:s'),
        //             'updated_at' => date('Y-m-d H:i:s')
        //             ]);
        //     }, 5);
        // }
        // else{
            $gameId = DB::table('pompa_ban')->insertGetId([
                'user_id' => Auth::id(),
                'status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        // }
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Berhasil mengikuti game tambal ban online'
        ]);
      }

      public function endTambalBan(Request $request){
        $gameId = DB::table('pompa_ban')
                  ->where('user_id', Auth::id())
                  ->where('game_end', 0)
                  ->orderByDesc('id')
                  ->first();
        if($gameId){
            $gameId = $gameId->id;
            DB::table('pompa_ban')->where('id', $gameId)->update([
                'time' => $request->time,
                'total_ban' => $request->totalBan,
                'game_end' => 1,
                'game_end_date' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            /**Berhasil menyelesaikan 3 ban */
            if($request->totalBan == 3){
                return response()->json([
                  'Status' => 'Success',
                  'Message' => 'Berhasil menyelesaikan game Tambal Ban',
                  'Time' => $request->time,
                  'Total Ban' => $request->totalBan
              ]);
            }
            /**Gagal menyelesaikan 3 ban */
            else{
                return response()->json([
                    'Status' => 'Failed',
                    'Message' => 'Gagal menyelesaikan game Tambal Ban'
                ]);
            }
        }else{
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Gagal'
            ]);
        }
    }

    public function leaderboardTambalBan(){
        $leaderBoard = DB::table('pompa_ban')
            ->join('users', 'users.id', 'pompa_ban.user_id')
            ->where('pompa_ban.status', 1)->where('pompa_ban.game_end', 1)
            ->where('total_ban', 3)
            ->groupBy('pompa_ban.user_id')
            ->select('pompa_ban.user_id', 'users.name', 'pompa_ban.created_at')
            ->orderBy('pompa_ban.time', 'asc')
            ->get();
        foreach($data as $key => $lb){
             $value = DB::table('pompa_ban')
                     ->where('total_ban', 3)
                    ->where('user_id', $lb->user_id)
                    ->min('time');
            $data[$key]->time = $value;
        }
        return response()->json([
            'Status' => 'Success',
            'Data' => $leaderBoard
        ]);
    }

}