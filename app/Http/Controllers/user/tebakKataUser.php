<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
// use Carbon\Carbon;
use Carbon\Carbon;

class tebakKataUser extends Controller
{
    public function insert(Request $request){
        $check = DB::table('soal_tebak_kata_users')
                ->whereDate('created_at', Carbon::today())
                ->where('user_id', Auth::id())->count();
        if($check < 5){
            DB::table('soal_tebak_kata_users')->insert([
                'kata' => $request->kata,
                'pertanyaan' => $request->pertanyaan,
                'poin' => 100,
                'user_id' => Auth::id(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return redirect()->route('successBikinQuiz');
        }
        return redirect()->back();
    }

    public function audition(Request $request){
        $check = DB::table('auditions')->where('user_id', Auth::id())->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                ->count();
        if($check < 1){
            /**Convert waktu tampil */
            $date1 = Carbon::now()->startOfWeek();
            $new_date = $date1->addDay($request->data_hari);
            $temp_date = $new_date->format('Y-m-d');
            $jam = DB::table('baba_times')->where('id', $request->data_jam)->first();
            $waktu_tampil = $temp_date.' '.$jam->time_event;
            /**End convert waktu tampil */
            $check = DB::table('auditions')->where('waktu_tampil', $waktu_tampil)->count();
            if($check != 3){
                $nomerAudisi = $this->generateAudisi();
                DB::table('auditions')->insert([
                    'user_id' => Auth::id(),
                    'waktu_tampil' => $waktu_tampil,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'nomer_audisi' => $nomerAudisi
                ]);
                return response()->json([
                    'Status' => 'Success',
                    'Message' => 'Berhasil tambah data'
                ]);
            }
            else{
                return response()->json([
                    'Status' => 'Error',
                    'Message' => 'Jadwal tidak tersedia'
                ]);
            }
        }
        else{
            return response()->json([
                'Status' => 'Error',
                'Message' => 'Anda sudah mengikuti baba mencari bakat minggu ini'
            ]);
        }
    }

    public function generateAudisi(){
        $data = DB::table('auditions')->count();
        return 'A0000'.$data;
    }

}
