<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
class DataController extends Controller
{
    public function getdata($time){
        $dateNow = date('Y-m-d');
         $startTime = $dateNow.' 14:00:00';
         $endTime = $dateNow.' 21:00:00';
        // $startTime = \DB::table('event_time')->where('time','<=',$time)->where('end_time', '>', $time)->first();
        // $getData = \DB::table('orders')->where('time_event_id', $startTime->id)->where('status_spinwheel', 'waiting')->get();


        // $getData = \DB::table('orders')->where('created_at', '>=', $startTime)->where('created_at', '<=', $endTime)->where('status_spinwheel', 'waiting')->whereIn('sku', array('CSB', 'CSB-LECI', 'CSB-JAMBU', 'CSB-JERUK', 'BADAK-22364', 'BADAK-22312', 'PROMO-LECI', 'PROMO-JERUK','PROMO-JAMBU','STW', 'STW-LECI', 'STW-JAMBU', 'STW-JERUK','LPCB200ML', 'LPCB500ML'))->get();
        $getData = \DB::table('orders')->where('created_at', '>=', $startTime)->where('created_at', '<=', $endTime)->where('status_spinwheel', 'waiting')->get();

        $qtyTotal = 0;

        foreach($getData as $key=>$value){
            $getDetail = \DB::table('order_detail')->where('order_id', $value->id)->get();
            $qtyTotal = 0;
            foreach($getDetail as $index=>$item){
                $getData[$key]->order[$index] = [
                'item'=>$item->nama_item,
                'qty'=>$item->qty,
                'sku'=>$item->sku
                ];
                $qtyTotal = $qtyTotal + $item->qty;
            }
            $getData[$key]->qtyTotal = $qtyTotal;
            $updateDisplayStatus = \DB::table('orders')->where('id', $value->id)->update([
                'display_status'=>1
            ]);
        }
        // dd($getData);
        return response()->json($getData);
    }

    public function getDataPerDay(){
        $now = date('Y-m-d');
        // $getDataText = \DB::table('orders')->whereDate('created_at', $now)->select('nama', 'id', 'invoice')->whereIn('sku', array('CSB', 'CSB-LECI', 'CSB-JAMBU', 'CSB-JERUK', 'BADAK-22364', 'BADAK-22312', 'PROMO-LECI', 'PROMO-JERUK','PROMO-JAMBU','STW','STW-LECI', 'STW-JAMBU', 'STW-JERUK','LPCB200ML', 'LPCB500ML'))->get();
        $getDataText = \DB::table('orders')->whereDate('created_at', $now)->select('nama', 'id', 'invoice')->get();
        $qtyTotal =0;
        foreach($getDataText as $key=>$value){
            $getDetail = \DB::table('order_detail')->where('order_id', $value->id)->get();
            $qtyTotal = 0;
            foreach($getDetail as $index=>$item){
                // $getDataText[$key]->order[$index] = [
                // 'item'=>$item->nama_item,
                // 'qty'=>$item->qty,
                // 'sku'=>$item->sku
                // ];
                $qtyTotal = $qtyTotal + $item->qty;
            }
            $getDataText[$key]->qtyTotal = $qtyTotal;
            unset($getDataText[$key]->id);
        }
        // dd($getDataText);
        return response()->json($getDataText);
    }
}
