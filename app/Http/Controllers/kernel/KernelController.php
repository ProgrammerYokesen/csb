<?php

namespace App\Http\Controllers\kernel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;

class KernelController extends Controller
{
    public function auctionWinner(){
        $auctions = DB::table('auctions')->where('auctions.winner_status', 0)->where('auctions.end_date', '<=', now('Asia/Jakarta'))->get();
        foreach($auctions as $auction){
            $haveBid = DB::table('auction_bids')->where('auction_id', $auction->id)->count();
            if($haveBid != 0){
                $bid_price = DB::table('auction_bids')->where('auction_id', $auction->id)->max('bid_price');
                $winner = DB::table('auction_bids')->where('auction_id', $auction->id)->where('bid_price', $bid_price)->first();
                DB::table('auctions')->where('id', $auction->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('auction_bids')->where('id', $winner->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('user_poins')->insert([
                    'user_id' => $winner->user_id,
                    'poin' => -$bid_price,
                    'from' => 'Bid - '.$auction->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                
                $baperPoin = DB::table('users')->where('id', $winner->user_id)->first()->baper_poin;
                $bp = $baperPoin - $bid_price;
                DB::table('users')->where('id', $winner->user_id)->update([
                    'baper_poin' => $bp
                    ]);
            }
        }
    }

    public function miniAuctionWinner(){
        $auctions = DB::table('auction_specials')->where('auction_specials.winner_status', 0)->where('auction_specials.end_date', '<=', now('Asia/Jakarta'))->get();
        foreach($auctions as $auction){
            $haveBid = DB::table('auction_special_bids')->where('auction_id', $auction->id)->count();
            if($haveBid != 0){
                $bid_price = DB::table('auction_special_bids')->where('status_bid', 1)->where('auction_id', $auction->id)->max('bid_price');
                $winner = DB::table('auction_special_bids')->where('status_bid', 1)->where('auction_id', $auction->id)->where('bid_price', $bid_price)->first();
                DB::table('auction_specials')->where('id', $auction->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('auction_special_bids')->where('id', $winner->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('user_poins')->insert([
                    'user_id' => $winner->user_id,
                    'poin' => -$bid_price,
                    'from' => 'Bid Auction Mini - '.$auction->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                
                $baperPoin = DB::table('users')->where('id', $winner->user_id)->first()->baper_poin;
                $bp = $baperPoin - $bid_price;
                DB::table('users')->where('id', $winner->user_id)->update([
                    'baper_poin' => $bp
                    ]);
            }
        }
    }
    public function removeLeaderboardMiniAuction(){
        $setting = DB::table('auction_special_settings')->first();
        $activeAuction = DB::table('auction_specials')
        ->where('status', 1)
        ->where('winner_status', 0)
        ->where(function ($query) {
            $query->where('auction_specials.starttime', '<=', now('Asia/Jakarta'));
        })
        ->where(function ($query) {
            $query->where('auction_specials.end_date', '>=', now('Asia/Jakarta'));
        })
        ->first();
        if($activeAuction){
            $userId = DB::table('auction_special_bids')
            ->groupBy('auction_special_bids.user_id')
            ->where('auction_special_bids.auction_id', $activeAuction->id)->pluck('user_id');
            $listId = DB::table('users')
                    ->whereIn('users.id', $userId)
                    ->leftJoin('user_poins', 'user_poins.user_id', 'users.id')
                    ->groupBy('users.id')
                    ->selectRaw('SUM(user_poins.poin) as user_poin, users.id')
                    // ->select('users.id', DB::raw('SUM(user_poins.poin) as user_poin'))
                    ->get();
            // dd($listId);
            $finalId = $listId->where('user_poin', '>', $setting->baper_poin_maksimal)->pluck('id');
            // dd($userId, $listId, $finalId);
            DB::table('auction_special_bids')->where('auction_id', $activeAuction->id)->whereIn('user_id', $finalId)->update(['status_bid' => 0]);
        }
    }

}
