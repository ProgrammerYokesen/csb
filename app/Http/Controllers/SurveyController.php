<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class SurveyController extends Controller
{
  public function index()
  {
    $input_status = DB::table('survey_responders')->where('user_id', Auth::id())->where('created_at', '>', '2022-01-03 00:00:00')->count() == 0 ? false : true;
    $question = DB::table('survey_questions')->where('status', 'active')->get();
    $count = DB::table('survey_responders')->where('created_at', '>', '2022-01-03 00:00:00')->count();
    // dd($input_status, $question, $count);
    return view('dashboard.pages.survey', compact('question', 'count', 'input_status'));
  }
  public function store(Request $request)
  {
    $count = DB::table('survey_responders')->where('created_at', '>', '2022-01-03 00:00:00')->count();
    // if($count > 50){
    //   return redirect()->back()->with([
    //     'success' => false,
    //     'message' => 'kuota penuh'
    //   ]);
    // }
    $end_date = strtotime('2022-01-05 23:59:00');
    $now = strtotime(date('Y-m-d H:i:s'));
    if($now < $end_date)
    {
      if(DB::table('survey_responders')->where('created_at', '>', '2022-01-03 00:00:00')->where('user_id', Auth::id())->count() == 0)
      {
        $responderId = DB::table('survey_responders')->insertGetId([
          'user_id' => Auth::id(),
          'created_at' => now(),
          'updated_at' => now()
        ]);

        foreach ($request->except(['_token']) as $key => $value) {
            DB::table('survey_responder_answers')->insert([
              'survey_responder_id' => $responderId,
              'survey_question_id' => $key,
              'answer' => $value,
              'created_at' => now(),
              'updated_at' => now()
            ]);
        }

        DB::table('user_poins')->insert([
          'user_id' => Auth::id(),
          'poin' => 100000,
          'fk_id' => $responderId,
          'from' => 'Survey Pendapat Sobat',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
        $curentBP = Auth::user()->baper_poin + 100000;
        DB::table('users')->where('id', Auth::id())->update([
          'baper_poin' => $curentBP,
          'updated_at' => now()
        ]);
      }
    }
    return redirect()->back()->with('success', true);
  }
}
