<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use GuzzleHttp\Client;
use Storage;

class SppaController extends Controller
{
  public function getWelcome(){
      $daftar = DB::table('sppa_videos')->join('sppa_categories', 'sppa_categories.id', 'sppa_videos.sppa_category_id')->select('sppa_videos.*', 'sppa_categories.category_name')->where('user_id', Auth::id())->orderByDesc('created_at')->first();
      /**
      * $daftar = null : belum upload sama sekali
      * $daftar->status ? accepted, rejected, new
      */
      // dd($daftar);
      return view('dashboard.pages.sppa.sppa-welcome', compact('daftar'));
  }

  public function getPageForm(){
      $daftar = DB::table('sppa_videos')->where('user_id', Auth::id())->where(function ($query){
        $query->where('status', 'new')->orWhere('status', 'new');
      })->first();
      $acceptedvideo = DB::table('sppa_videos')->where('user_id', Auth::id())->where('user_id', Auth::id())->where(function ($query){
        $query->where('status', 'new')->orWhere('status', 'new');
      })->count();
      // dd($daftar, $acceptedvideo, Auth::id());
      if($acceptedvideo != 0){
        return redirect()->route('sppaWelcomePage')->with('errorMsg', 'Kamu sudah pernah isi!');
      }
      $categories = DB::table('sppa_categories')->where('status', 'active')->get();
      /** id, category_name, photo */
      return view('dashboard.pages.sppa.sppa-form', compact('categories'));
  }

  public function postResult(Request $request){
      $link = $request->link;
      $kategori = $request->cat_id;

      //LOGIC TO DB
      DB::table('sppa_videos')->insert([
        'original_link' => $link,
        'status' => 'new',
        'user_id' => Auth::id(),
        'sppa_category_id' => $kategori,
        'created_at' => now(),
        'updated_at' => now()
      ]);
      ///
      $success = true;
      if($success){
          return redirect()->route('sppaWelcomePage')->with('successInput','Sukses Memasukan data' );
      }
  }

  public function sppaList(Request $request)
  {
    $data["video_like_id"] = DB::table('sppa_video_likes')->where('user_id', Auth::id())->count() != 0 ? DB::table('sppa_video_likes')->where('user_id', Auth::id())->first()->sppa_video_id : null;
    /** id, category_name, photo */
    $data["categories"] = DB::table('sppa_categories')->where('status', 'active')->get();

    $query = DB::table('sppa_videos')->where('sppa_videos.status', 'accepted')
                      ->leftJoin('sppa_video_likes', 'sppa_video_likes.sppa_video_id', 'sppa_videos.id')
                      ->groupBy('sppa_videos.id')
                      ->join('sppa_categories', 'sppa_categories.id', 'sppa_videos.sppa_category_id')
                      ->join('users', 'users.id', 'sppa_videos.user_id')
                      ->select('sppa_videos.embed_url', 'sppa_videos.thumbnail', 'sppa_videos.sppa_category_id as cat_id', 'sppa_videos.id as id', 'sppa_videos.original_link as link', 'users.name', 'sppa_categories.category_name', DB::raw("count(sppa_video_likes.sppa_video_id) as totalLike"));
                      // ->paginate(9);
    if($request->has('q'))
    {
      // dd($request->q);
      if($request->q == 'all')
      {
        $data["videos"] = $query->paginate(9);
      }
      else {
        $arr = json_decode($request->q);
        $data["videos"] = $query->whereIn('sppa_videos.sppa_category_id', $arr)->paginate(9);
      }
    }
    else{
      $data["videos"] = $query->paginate(9);
    }
    // dd($data["videos"]);

    return view('dashboard.pages.sppa.sppa-list', $data);
  }

  public function store(Request $request)
  {
    $validator = \Validator::make($request->all(), [
        'link' => 'required',
        'cat_id' => 'required'
    ]);

    if ($validator->fails()) {
        return redirect()->back()->with($validator->errors());
    }
    /** Check Video*/
    $uploadVideo = DB::table('sppa_videos')->where('user_id', Auth::id())->where('status', 'accepted')->count();
    if($uploadVideo != 0)
    {
      return redirect()->back()->with('errorInput', 'Hanya bisa upload video satu kali');
    }
    /** Insert Data Link and cattegory*/
    $sppaId = DB::table('sppa_videos')->insertGetId([
      'user_id' => Auth::id(),
      'original_link' => $request->link,
      'status' => 'new',
      'created_at' => now(),
      'updated_at' => now(),
      'sppa_category_id' => $request->cat_id
    ]);

    try {
      $client = new Client();
      $response = $client->request('GET', 'https://www.tiktok.com/oembed?url='.$request->link);
      $res = $response->getBody()->getContents();
      $responses = json_decode($res);

      $url = $responses->thumbnail_url;
      DB::table('sppa_videos')->where('id', $sppaId)->update([
        'thumbnail' => $url,
        'embed_url' => $responses->html
      ]);
      // dd($responses);
    } catch (\Exception $e) {

    }


    return redirect()->route('sppaWelcomePage')->with('successInput','Sukses Memasukan data' );

  }

  public function sppaReferensi(){
    return view('dashboard.pages.sppa.sppa-referensi');
  }

  public function vote(Request $request)
  {
    $validator = \Validator::make($request->all(), [
        'video_id' => 'required'
    ]);

    if ($validator->fails()) {
        return redirect()->back()->with($validator->errors());
    }
    $voted = DB::table('sppa_video_likes')->where('user_id', Auth::id())->count();
    if($voted != 0)
    {
      return redirect()->route('sppaList')->with([
        'errorMsg' => "Hanya Bisa Vote satu Kali"
      ]);
    }
    DB::table('sppa_video_likes')->insert([
      'sppa_video_id' => $request->video_id,
      'user_id' => Auth::id(),
      'created_at' => now(),
      'updated_at' => now()
    ]);
    return redirect()->route('sppaList')->with([
      'successMsg' => "Berhasil Vote Video"
    ]);
  }

}
