<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use DateTime;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Session;


class QuizShowController extends Controller
{
  public function quizShow()
  {
    $now = date('Y-m-d H:i:s');
    // $games = DB::table('harbolnas_1212_games')
    //         ->join('harbolnas_1212', 'harbolnas_1212.game_id', 'harbolnas_1212_games.id')
    //         ->whereDate('harbolnas_1212.start_date', Carbon::today())
    //         ->select('harbolnas_1212_games.image_drive', 'harbolnas_1212_games.game_name', 'harbolnas_1212.id as event_id', 'harbolnas_1212_games.id as game_id', 'harbolnas_1212_games.photo', 'harbolnas_1212.start_date', 'harbolnas_1212.end_date')
    //         ->orderBy('harbolnas_1212.start_date', 'asc')
    //         ->get();
    // foreach($games as $k => $v)
    // {
    //   $join = DB::table('harbolnas_1212_event_users')->where('user_id', Auth::id())->where('event_id', $v->event_id)->count();
    //   $join = $join == 1 ? true:false;
    //
    //   $minutes_to_add = 30;
    //   $end_register = new \DateTime($v->start_date);
    //   $end_register->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
    //   $stamp = $end_register->format(\DateTime::ATOM);
    //   $endregister = explode("+", $stamp);
    //
    //   $startRegister = strtotime($v->start_date);
    //   $startRegister = $startRegister - (30 * 60);
    //   $startRegister = date("Y-m-d H:i:s", $startRegister);
    //   $startRegister = new \DateTime($startRegister);
    //   $start_register = $startRegister->format(\DateTime::ATOM);
    //   $sRegister = explode("+", $start_register);
    //
    //   $games[$k]->start_register = $sRegister[0];
    //   $games[$k]->end_register = $endregister[0];
    //   $games[$k]->join = $join;
    // }
    // $user = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->first();
    // $checkUser = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->count();
    // $register = false;
    // $paid = false;
    // $winner = false;
    // if($checkUser != 0)
    // {
    //
    //   $register = true;
    //   if($user->status == 'join')
    //   {
    //     $paid = true;
    //   }
    //   if($user->winner_status == 1)
    //   {
    //     $winner = true;
    //   }
    // }

    // $users = DB::table('users')
    //         ->where('users.id', Auth::id())
    //         ->join('harbolnas_1212_users', 'harbolnas_1212_users.user_id', 'users.id')
    //         ->join('whatsapp_cs', 'whatsapp_cs.id', 'harbolnas_1212_users.whatsapp_cs_id')
    //         ->select('whatsapp_cs.whatsapp', 'harbolnas_1212_users.uuid as orderId')
    //         ->first();


    // NEW QUIZ SHOW FORMAT
    $games = DB::table('harbolnas_1212_games')
            ->select('harbolnas_1212_games.image_drive', 'harbolnas_1212_games.game_name', 'harbolnas_1212_games.id as game_id', 'harbolnas_1212_games.photo')
            ->get();
    // dd($games);

    // return view('dashboard.pages.quiz-show.list-quiz', compact('games', 'register', 'paid', 'winner', 'users'));
    return view('dashboard.pages.quiz-show.list-quiz', compact('games'));
  }

  public function quizTp()
  {
    return view('dashboard.pages.quiz-show.tp');
  }

  public function quizAkt()
  {
    return view('dashboard.pages.quiz-show.akt');
  }
  public function quizSybj()
  {
    return view('dashboard.pages.quiz-show.sybj');
  }
  public function quizTys()
  {
    return view('dashboard.pages.quiz-show.tys');
  }
  public function quizIc()
  {
    return view('dashboard.pages.quiz-show.ic');
  }
  public function quizPs()
  {
    return view('dashboard.pages.quiz-show.ps');
  }
  public function quizPyb()
  {
    return view('dashboard.pages.quiz-show.pyb');
  }

  public function gameDetail($eventId)
  {
    // $v = DB::table('harbolnas_1212')->where('id', $eventId)->first();
    // $game = DB::table('harbolnas_1212_games')->where('id', $v->game_id)->first();
    $games = DB::table('harbolnas_1212_games')->where('id', $eventId)->first();
    $gameRules = DB::table('harbolnas_1212_game_rules')->where('game_id', $eventId)->get();
    // dd($games, $gameRules);

    // $minutes_to_add = 30;
    // $end_register = new \DateTime($v->start_date);
    // $end_register->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
    // $stamp = $end_register->format(\DateTime::ATOM);
    // $endregister = explode("+", $stamp);
    //
    // $startRegister = strtotime($v->start_date);
    // $startRegister = $startRegister - (30 * 60);
    // $startRegister = date("Y-m-d H:i:s", $startRegister);
    // $startRegister = new \DateTime($startRegister);
    // $start_register = $startRegister->format(\DateTime::ATOM);
    // $sRegister = explode("+", $start_register);
    //
    // $game->start_register = $sRegister[0];
    // $game->end_register = $endregister[0];
    //
    // $data["game"] = $game;
    // $data["rules"] = DB::table('harbolnas_1212_game_rules')->where('game_id', $game->id)->get();
    //
    // $user = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->first();
    // $checkUser = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->count();
    // $register = false;
    // $paid = false;
    // $winner = false;
    // if($checkUser != 0)
    // {
    //
    //   $register = true;
    //   if($user->status == 'join')
    //   {
    //     $paid = true;
    //   }
    //   if($user->winner_status == 1)
    //   {
    //     $winner = true;
    //   }
    // }
    // $user = DB::table('users')
    //         ->where('users.id', Auth::id())
    //         ->join('harbolnas_1212_users', 'harbolnas_1212_users.user_id', 'users.id')
    //         ->join('whatsapp_cs', 'whatsapp_cs.id', 'harbolnas_1212_users.whatsapp_cs_id')
    //         ->select('whatsapp_cs.whatsapp', 'harbolnas_1212_users.uuid as orderId')
    //         ->first();
    // $event_id = $eventId;
    //
    // return view('dashboard.pages.quiz-show.detail-quiz', compact('data', 'register', 'paid', 'winner', 'user', 'event_id'));
    return view('dashboard.pages.quiz-show.detail-quiz', compact('games', 'gameRules'));
  }

  public function riwayatQuiz()
  {
    $query = DB::table('harbolnas_1212_event_users')
            ->where('harbolnas_1212_event_users.user_id', Auth::id())
            ->join('harbolnas_1212', 'harbolnas_1212.id', 'harbolnas_1212_event_users.event_id')
            ->join('harbolnas_1212_games', 'harbolnas_1212_games.id', 'harbolnas_1212.game_id')
            ->select('harbolnas_1212_games.game_name', 'harbolnas_1212.start_date as date')
            ->orderByDesc('harbolnas_1212.start_date');
    $data = $query
            ->paginate(10);
    $participate = $query->count();
    $user = DB::table('harbolnas_1212_users')->where('user_id', Auth::id())->first();
    // dd($user, $data);
    return view('dashboard.pages.quiz-show.riwayat', compact('data', 'user', 'participate'));
  }

  public function listPeserta($id)
  {
    // $id = 10;
    $data = DB::table('harbolnas_1212_event_users')
            ->where('harbolnas_1212_event_users.event_id', $id)
            ->join('users', 'users.id', 'harbolnas_1212_event_users.user_id')
            ->join('harbolnas_1212_users', 'harbolnas_1212_users.user_id', 'users.id')
            ->where('users.id', '!=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->paginate(15);
    $mailList = array();
    foreach($data as $k => $v)
  		{
        $email = '';
  			$txt = $v->email;
  			$mail = explode("@",$txt);
  			$len = strlen($mail[0]);
  			$i = $len/2;
  			$k = $len - 1;
  			$r = '';
  			for($j = 0 ;$j < $i ; $j++)
  			{
  				$r = $r.'*';
  			}
  			$replace = substr_replace($mail[0], $r, $i, $k);
  			$email = $replace.'@'.$mail[1];
        array_push($mailList,$email);
      }
    $totalData = DB::table('harbolnas_1212_event_users')
            ->where('event_id', $id)
            ->count();
    $event = DB::table('harbolnas_1212')
             ->where('harbolnas_1212.id', $id)
             ->join('harbolnas_1212_games', 'harbolnas_1212_games.id', 'harbolnas_1212.game_id')
             ->select('harbolnas_1212.game_id', 'harbolnas_1212_games.game_name as name', 'harbolnas_1212.start_date as date')
             ->first();
    $user =  DB::table('harbolnas_1212_event_users')
            ->where('harbolnas_1212_event_users.event_id', $id)
            ->join('users', 'users.id', 'harbolnas_1212_event_users.user_id')
            ->join('harbolnas_1212_users', 'harbolnas_1212_users.user_id', 'users.id')
            ->where('users.id', '=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->first();
    // dd($totalData);

    $events = DB::table('harbolnas_1212')->select('id', 'start_date')->where('game_id', $event->game_id)->get();
    return view('dashboard.pages.quiz-show.list-peserta', compact('data', 'totalData', 'mailList', 'event', 'user', 'events'));
  }

  public function listPemenang()
  {
    $data = DB::table('harbolnas_1212_users')
            ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
            ->where('harbolnas_1212_users.status', '=', 'join')
            ->where('harbolnas_1212_users.winner_status', 1)
            ->where('users.id', '!=', 1)
            ->where('users.id', '!=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->paginate(15);

    $mailList = array();
    foreach($data as $k => $v)
  		{
        $email = '';
  			$txt = $v->email;
  			$mail = explode("@",$txt);
  			$len = strlen($mail[0]);
  			$i = $len/2;
  			$k = $len - 1;
  			$r = '';
  			for($j = 0 ;$j < $i ; $j++)
  			{
  				$r = $r.'*';
  			}
  			$replace = substr_replace($mail[0], $r, $i, $k);
  			$email = $replace.'@'.$mail[1];
        array_push($mailList,$email);
      }

    $user = DB::table('harbolnas_1212_users')
            ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
            ->where('harbolnas_1212_users.status', '=', 'join')
            ->where('harbolnas_1212_users.winner_status', 1)
            ->where('users.id', '=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->first();

    return view('dashboard.pages.quiz-show.list-pemenang', compact('data', 'mailList', 'user'));
  }

  public function listBelumMain()
  {
    $data = DB::table('harbolnas_1212_users')
            ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
            ->where('harbolnas_1212_users.status', '=', 'join')
            ->where('harbolnas_1212_users.winner_status', 0)
            ->where('users.id', '!=', 1)
            ->where('users.id', '!=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->paginate(15);

    $mailList = array();
    foreach($data as $k => $v)
    {
      $email = '';
      $txt = $v->email;
      $mail = explode("@",$txt);
      $len = strlen($mail[0]);
      $i = $len/2;
      $k = $len - 1;
      $r = '';
      for($j = 0 ;$j < $i ; $j++)
      {
        $r = $r.'*';
      }
      $replace = substr_replace($mail[0], $r, $i, $k);
      $email = $replace.'@'.$mail[1];
      array_push($mailList,$email);
    }

    $user = DB::table('harbolnas_1212_users')
            ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
            ->where('harbolnas_1212_users.status', '=', 'join')
            ->where('harbolnas_1212_users.winner_status', 0)
            ->where('users.id', '=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->first();

            // dd($user);

    return view('dashboard.pages.quiz-show.list-belum-main', compact('data', 'mailList', 'user'));
  }


  public function lablistPemenang()
  {
    $data = DB::table('harbolnas_1212_users')
            ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
            ->where('harbolnas_1212_users.status', '=', 'join')
            ->where('harbolnas_1212_users.winner_status', 1)
            ->where('users.id', '!=', 1)
            ->where('users.id', '!=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->paginate(15);

    $mailList = array();
    foreach($data as $k => $v)
  		{
        $email = '';
  			$txt = $v->email;
  			$mail = explode("@",$txt);
  			$len = strlen($mail[0]);
  			$i = $len/2;
  			$k = $len - 1;
  			$r = '';
  			for($j = 0 ;$j < $i ; $j++)
  			{
  				$r = $r.'*';
  			}
  			$replace = substr_replace($mail[0], $r, $i, $k);
  			$email = $replace.'@'.$mail[1];
        array_push($mailList,$email);
      }

    $user = DB::table('harbolnas_1212_users')
            ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
            ->where('harbolnas_1212_users.status', '=', 'join')
            ->where('harbolnas_1212_users.winner_status', 1)
            ->where('users.id', '=', Auth::id())
            ->groupBy('users.id')
            ->select('users.email as email', 'harbolnas_1212_users.name')
            ->first();

    return view('lab.search-lp', compact('data', 'mailList', 'user'));
  }

  public function searchPemenang(Request $request)
  {
    if ($request->has('q')) {
        $cari = $request->q;

        $data = DB::table('harbolnas_1212_users')
                ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
                ->where('harbolnas_1212_users.status', '=', 'join')
                ->where('harbolnas_1212_users.winner_status', 1)
                ->where('users.id', '!=', 1)
                ->where('harbolnas_1212_users.name','LIKE','%'.$cari.'%')
                ->groupBy('users.id')
                ->select('users.email as email', 'harbolnas_1212_users.name')
                ->get();

        $mailList = array();
        foreach($data as $k => $v)
      		{
            $email = '';
      			$txt = $v->email;
      			$mail = explode("@",$txt);
      			$len = strlen($mail[0]);
      			$i = $len/2;
      			$k = $len - 1;
      			$r = '';
      			for($j = 0 ;$j < $i ; $j++)
      			{
      				$r = $r.'*';
      			}
      			$replace = substr_replace($mail[0], $r, $i, $k);
      			$email = $replace.'@'.$mail[1];
            array_push($mailList,$email);
          }
        // dd($data);

        $view = view('dashboard.components.quiz-show.search-result',compact('data', 'mailList'))->render();
        return response()->json(['html'=>$view]);
    }
  }

  public function searchBelumMenang(Request $request)
  {
    if ($request->has('q')) {
        $cari = $request->q;

        $data = DB::table('harbolnas_1212_users')
                ->join('users', 'users.id', 'harbolnas_1212_users.user_id')
                ->where('harbolnas_1212_users.status', '=', 'join')
                ->where('harbolnas_1212_users.winner_status', 0)
                ->where('users.id', '!=', 1)
                ->where('harbolnas_1212_users.name','LIKE','%'.$cari.'%')
                ->groupBy('users.id')
                ->select('users.email as email', 'harbolnas_1212_users.name')
                ->get();

        $mailList = array();
        foreach($data as $k => $v)
      		{
            $email = '';
      			$txt = $v->email;
      			$mail = explode("@",$txt);
      			$len = strlen($mail[0]);
      			$i = $len/2;
      			$k = $len - 1;
      			$r = '';
      			for($j = 0 ;$j < $i ; $j++)
      			{
      				$r = $r.'*';
      			}
      			$replace = substr_replace($mail[0], $r, $i, $k);
      			$email = $replace.'@'.$mail[1];
            array_push($mailList,$email);
          }
        // dd($data);

        $view = view('dashboard.components.quiz-show.search-result',compact('data', 'mailList'))->render();
        return response()->json(['html'=>$view]);
    }
  }
}
