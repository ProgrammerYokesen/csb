<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // Get auction Winner
        $schedule->call(function () {
                    $auctions = DB::table('auctions')->where('auctions.winner_status', 0)->where('auctions.end_date', '<=', now('Asia/Jakarta'))->get();
            foreach($auctions as $auction){

                $bid_price = DB::table('auction_bids')->where('auction_id', $auction->id)->max('bid_price');
                $winner = DB::table('auction_bids')->where('auction_id', $auction->id)->where('bid_price', $bid_price)->first();
                DB::table('auctions')->where('id', $auction->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('auction_bids')->where('id', $winner->id)->update([
                    'winner_status' => 1
                ]);
                DB::table('user_poins')->insert([
                    'user_id' => $winner->user_id,
                    'poin' => -$bid_price,
                    'from' => 'Bid - '.$auction->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                $baperPoin = DB::table('users')->where('id', $winner->user_id)->first()->baper_poin;
                $bp = $baperPoin - $bid_price;
                DB::table('users')->where('id', $winner->user_id)->update([
                    'baper_poin' => $bp
                    ]);
            }
            return 'Success';
        })->everyMinute();

        $schedule->call('App\Http\Controllers\kernel\KernelController@miniAuctionWinner')->everyMinute();
        $schedule->call('App\Http\Controllers\kernel\KernelController@removeLeaderboardMiniAuction')->everyMinute();
        $schedule->call('App\Http\Controllers\labController@seragaminDataKota')->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
