<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSpecialAuctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction_specials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('photo')->nullable();
            $table->integer('start_poin')->nullable();
            $table->text('detail')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('winner_status')->default(0);
            $table->integer('views')->default(0);
            $table->string('uuid', 100);
            $table->dateTime('starttime')->nullable();
            $table->integer('kelipatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_specials');
    }
}
