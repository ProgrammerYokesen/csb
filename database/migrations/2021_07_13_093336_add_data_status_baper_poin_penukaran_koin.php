<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataStatusBaperPoinPenukaranKoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penukaran_poins', function (Blueprint $table) {
            $table->string('status_baper_poin');
            $table->dateTime('tanggal_status_baper_poin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penukaran_poins', function (Blueprint $table) {
            //
        });
    }
}
