<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuizTebakKataIdOnJawabanTebakKata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jawaban_tebak_kata', function (Blueprint $table) {
            $table->integer('quiz_tebak_kata_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawaban_tebak_kata', function (Blueprint $table) {
            //
        });
    }
}
