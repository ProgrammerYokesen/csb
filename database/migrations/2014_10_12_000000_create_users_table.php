<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->nullable();
            $table->string('referral_code', 100)->unique();
            $table->integer('countRef')->default(0);
            $table->integer('spam')->default(0);
            $table->integer('spamRatio')->default(0);
            $table->integer('emailTrue')->default(0);
            $table->integer('emailFalse')->default(0);
            $table->integer('rejekiNomplokPoin')->default(0);
            $table->string('name');
            $table->string('email')->unique();
            $table->string('emailValidation', 20)->default('new');
            $table->string('password');
            $table->string('whatsapp', 100)->nullable();
            $table->string('subscribeEmail',11)->default(0);
            $table->string('subscribeWhatsapp',11)->default(0);
            $table->string('msgEmail', 11)->default(0);
            $table->string('msgWhatsapp', 11)->default(0);
            $table->string('kota')->nullable();
            $table->string('provinsi')->nullable();
            $table->integer('ref_id')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('device')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_content')->nullable();
            $table->text('alamat_rumah')->nullable();
            $table->text('alamat_detail')->nullable();
            $table->text('alamat_device')->nullable();
            $table->string('lat_rumah')->nullable();
            $table->string('lng_rumah')->nullable();      
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
