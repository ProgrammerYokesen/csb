<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailRegisterOnLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('screen')->nullable();
            $table->string('platform')->nullable();
            $table->string('version_platform')->nullable();
            $table->string('browser')->nullable();
            $table->string('version_browser')->nullable();
            $table->string('languages')->nullable();
            $table->string('robot')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
