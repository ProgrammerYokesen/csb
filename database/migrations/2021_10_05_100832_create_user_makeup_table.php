<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMakeupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_makeup', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('no_wa');
            $table->string('email')->unique();
            $table->text('alamat');
            $table->string('status')->default('new');
            $table->integer('whatsapp_cs_id')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('device')->nullable();
            $table->string('screen')->nullable();
            $table->string('platform')->nullable();
            $table->string('version_platform')->nullable();
            $table->string('browser')->nullable();
            $table->string('languages')->nullable();
            $table->string('version_browser')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_makeup');
    }
}
