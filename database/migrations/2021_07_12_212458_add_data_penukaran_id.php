<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataPenukaranId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tukar_koins', function (Blueprint $table) {
            $table->integer('penukaran_id');
            $table->string('status_edeem')->nullable();
            $table->string('tanggal_status_redeem')->nullable();
            $table->string('status_koin')->nullable();
            $table->string('tanggal_status_koin')->nullable();
            $table->string('status_pengembalian_koin')->nullable();
            $table->string('tanggal_status_pengembalian_koin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tukar_koins', function (Blueprint $table) {
            //
        });
    }
}
