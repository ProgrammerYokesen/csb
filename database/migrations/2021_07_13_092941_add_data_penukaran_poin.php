<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataPenukaranPoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penukaran_poins', function (Blueprint $table) {
            $table->string('status_redeem')->nullable();
            $table->string('tanggal_status_redeem')->nullable();
            $table->string('status_koin')->nullable();
            $table->string('tanggal_status_koin')->nullable();
            $table->string('status_pengembalian_koin')->nullable();
            $table->string('tanggal_status_pengembalian_koin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penukaran_poins', function (Blueprint $table) {
            //
        });
    }
}
