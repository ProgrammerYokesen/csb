<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullalbeAtFromInUserPoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_poins', function (Blueprint $table) {
            $table->string('from', 50)->nullable()->change();
            $table->integer('fk_id')->nullable()->change();
            $table->integer('cat_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_poin', function (Blueprint $table) {
            //
        });
    }
}
