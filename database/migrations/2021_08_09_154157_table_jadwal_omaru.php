<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableJadwalOmaru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_omaru', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('time_event')->nullable();
            $table->string('nama_acara')->nullable();
            $table->string('host')->nullable();
            $table->string('ruangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_omaru');
    }
}
