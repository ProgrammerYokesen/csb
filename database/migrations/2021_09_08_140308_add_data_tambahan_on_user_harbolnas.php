<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataTambahanOnUserHarbolnas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_harbolnas', function (Blueprint $table) {
            $table->string('ip_address')->nullable();
            $table->string('device')->nullable();
            $table->string('screen')->nullable();
            $table->string('platform')->nullable();
            $table->string('version_platform')->nullable();
            $table->string('browser')->nullable();
            $table->string('languages')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_harbolnas', function (Blueprint $table) {
            //
        });
    }
}
