<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TebakKataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tebak_kata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 100);
            $table->string('kata', 100);
            $table->text('deskripsi')->nullable();
            $table->integer('poin');
            $table->boolean('status')->default(1);
            $table->integer('created_by');           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tebak_kata');
    }
}
