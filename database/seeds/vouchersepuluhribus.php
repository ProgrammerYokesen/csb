<?php

use Illuminate\Database\Seeder;

class vouchersepuluhribus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           for($i = 1; $i<=900; $i++){
            DB::table('vouchers')->insert([
                'voucher' => Str::random(10),
                'poin' => 10000,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
