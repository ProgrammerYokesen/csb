<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class quizBundaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quizId = DB::table('quiz_bunda')->insertGetId([
            'name' => 'Day 1',
            'description' => 'Bunda Fita',
            'show_at' => '2021-10-13',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Usaha + Tenaga + Waktu + Pikiran = ...',
            'jawaban_a' => 'Gaji',
            'jawaban_b' => 'Penghasilan',
            'jawaban_benar' => 'B',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Siapakah yang membawakan materi pada hari pertama Workshop Reseller?',
            'jawaban_a' => 'Bunda Fita',
            'jawaban_b' => 'Bunda Fati',
            'jawaban_benar' => 'A',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Pembicara pada hari pertama Workshop Reseller adalah seorang..',
            'jawaban_a' => 'Karyawan',
            'jawaban_b' => 'Bundapreneur',
            'jawaban_benar' => 'B',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $quizId = DB::table('quiz_bunda')->insertGetId([
            'name' => 'Day 2',
            'description' => 'Selly',
            'show_at' => '2021-10-14',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Apa nama aplikasi yang dipakai untuk membuat tagihan belanja?',
            'jawaban_a' => 'Selly',
            'jawaban_b' => 'Sally',
            'jawaban_benar' => 'A',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Apa kegunaan "Auto Text" pada Selly?',
            'jawaban_a' => 'Menghitung ongkir',
            'jawaban_b' => 'Membuat template pesan',
            'jawaban_benar' => 'B',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Dimana kita bisa men-download Selly?',
            'jawaban_a' => 'Playstore',
            'jawaban_b' => 'Google',
            'jawaban_benar' => 'A',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $quizId = DB::table('quiz_bunda')->insertGetId([
            'name' => 'Day 3',
            'description' => 'Copywriting',
            'show_at' => '2021-10-15',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Copywriting adalah.....',
            'jawaban_a' => 'Seni dan ilmu untuk agar tidak ketinggalan trend',
            'jawaban_b' => 'Seni dan ilmu untuk menjual',
            'jawaban_benar' => 'B',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Cara pendekatan ke pelanggan dibagi menjadi 3 yaitu',
            'jawaban_a' => 'Audio, Visual, Kinetis',
            'jawaban_b' => 'Audio, Visual, Data',
            'jawaban_benar' => 'A',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('quiz_bunda_soals')->insert([
            'quiz_bunda_id' => $quizId,
            'pertanyaan' => 'Cara pelanggan mengambil keputusan dibagi menjadi 3 yaitu',
            'jawaban_a' => 'Data, Referensi, Diri Sendiri',
            'jawaban_b' => 'Hadiah, Referensi, Data',
            'jawaban_benar' => 'A',
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
