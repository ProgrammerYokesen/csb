<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class time_event_baba extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('baba_times')->insert([
            'time_event' => '19:00'
        ]);
        DB::table('baba_times')->insert([
            'time_event' => '19:15'
        ]);
        DB::table('baba_times')->insert([
            'time_event' => '19:30'
        ]);
        DB::table('baba_times')->insert([
            'time_event' => '19:45'
        ]);
        DB::table('baba_times')->insert([
            'time_event' => '20:00'
        ]);
        DB::table('baba_times')->insert([
            'time_event' => '20:15'
        ]);
        DB::table('baba_times')->insert([
            'time_event' => '20:30'
        ]);
        DB::table('baba_times')->insert([
            'time_event' => '20:45'
        ]);
    }
}
