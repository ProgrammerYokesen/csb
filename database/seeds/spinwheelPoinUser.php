<?php

use Illuminate\Database\Seeder;

class spinwheelPoinUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                /**Insert premium poin */
                $premium = DB::table('category_gacha')->where('cat_name', 'Premium')->first();
                $points = [
                    20000,
                    25000,
                    30000,
                    35000,
                    40000,
                    45000,
                    50000,
                    60000,
                    70000,
                    80000,
                    90000,
                    100000,
                    500000
                ];
                foreach ($points as $pt){
                    DB::table('spinwheel_list_poin')->insert([
                        'cat_id' => $premium->id,
                        'point' => $pt,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
        
                /**Insert poin Reguler */
        
                $reguler = DB::table('category_gacha')->where('cat_name', 'Reguler')->first();
                $points = [
                    5000,
                    10000,
                    15000,
                    20000,
                    25000,
                    30000,
                    35000,
                    40000,
                    45000,
                    50000,
                    5000,
                    10000,
                    15000,
                    20000
                ];
                foreach ($points as $pt){
                    DB::table('spinwheel_list_poin')->insert([
                        'cat_id' => $reguler->id,
                        'point' => $pt,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
    }
}
