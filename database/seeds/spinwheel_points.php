<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class spinwheel_points extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_gacha_points')->truncate();   
        /**Insert premium poin */
        $premium = DB::table('category_gacha')->where('cat_name', 'Premium')->first();
        $points = [
            40000,
            1750000,
            50000,
            600000,
            70000,
            90000,
            800000,
            1500000,
            100000,
            20000,
            1000000,
            200000,
            400000,
            300000,
            150000,
            500000,
            700000,
            900000,
            1250000,
            50000,
            30000,
            
           
            2000000
        ];
        foreach ($points as $pt){
            DB::table('cat_gacha_points')->insert([
                'cat_id' => $premium->id,
                'point' => $pt,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        /**Insert poin Reguler */

        $reguler = DB::table('category_gacha')->where('cat_name', 'Reguler')->first();
        $points = [
            5000,
            15000,
            20000,
            30000,
            75000,
            10000,
            100000,
            25000,
            35000,
            5000,
            15000,
            20000,
            40000,
            25000,
            10000,
            50000,
            150000
        ];
        
        foreach ($points as $pt){
            DB::table('cat_gacha_points')->insert([
                'cat_id' => $reguler->id,
                'point' => $pt,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        
    }
}
