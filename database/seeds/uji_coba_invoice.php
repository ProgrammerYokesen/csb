<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class uji_coba_invoice extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $total = 78;
        for($i=1 ; $i<=78 ;$i++){
            $rnd = rand(1, 10000);
            DB::table('orders')->insert([
                'invoice' => 'INV-Test-'.$rnd,
                'sku' => 'STW',
                'nama' => 'RND-Name-'.$rnd,
                'noHp' => $rnd,
                'platform' => 'Test Data',
                'qtyTotal' => 3,
                'status_spinwheel' => 'notjoined',
                'display_status' => 1,
                'status_redeem' => 0,
                'created_at' => '2021-07-15 00:00:00',
                'updated_at' => '2021-07-15 00:00:00'
                ]);
        }
    }
}
