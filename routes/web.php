<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['maintenance', 'web'])->group(function () {
  // Route For Landing Page
  // Route::get('/', 'PageController@landingPage')->name('defaultPage')->middleware('saveUtm');
  Route::get('/', 'PageController@homeBooklet')->name('defaultPage')->middleware('saveUtm');
  Route::get('/booklet', 'PageController@homeBooklet')->name('homeBooklet')->middleware('saveUtm');
  Route::get('/faq', 'PageController@faqPage')->name('faqPage');
  // Route::get('/landing','PageController@landingPage')->name('landingPage');
  Route::get('/forgot-password', 'PageController@forgotPage')->name('forgotPage');
  Route::post('/forgot-password', 'user\resetPassword@sendEmail')->name('sendEmailforgotPassword');
  Route::get('/forgot-password/berhasil', 'PageController@forgotDone')->name('forgotDone');
  Route::get('/login', 'PageController@loginPage')->name('loginPage')->middleware('user.guest');
  Route::get('/register', 'PageController@registerPage')->name('registerPage')->middleware('user.guest');
  Route::get('/reset-password', 'PageController@resetPass')->name('resetPass');
  Route::post('/reset-password', 'user\resetPassword@resetPass')->name('resetPassword');

  Route::get('/email-verification', 'PageController@emailVerifDone')->name('emailVerifDone');

  Route::get('/livestream', 'PageController@livestreamPage')->name('livestreamPage');
  Route::get('/omaru', 'PageController@omaru')->name('omaru')->middleware('saveUtm');
  Route::get('/auction', 'PageController@auction')->name('auction')->middleware('saveUtm');
  Route::get('/auction/detail', 'PageController@auctionDetail')->name('auctionDetail');
  Route::get('/getdata/{time}', 'DataController@getdata')->name('getdata');
  Route::get('/getdata-day', 'DataController@getDataPerDay')->name('getDataPerDay');

  Route::get('/ppkmcsbhutri76', 'LombaController@ppkmUser')->middleware('saveUtm')->name('ppkmUser');

  // Route for Universe
  Route::get('/universe/theater', 'PageController@uniTheater')->name('uniTheater')->middleware('saveUtm');
  Route::get('/universe/lapak-mamak', 'PageController@uniLapak')->name('uniLapak')->middleware('saveUtm');
  Route::get('/universe/auction-house', 'PageController@uniAuction')->name('uniAuction')->middleware('saveUtm');
  Route::get('/universe/gamezone', 'PageController@uniGamezone')->name('uniGamezone')->middleware('saveUtm');
  Route::get('/universe/townhall', 'PageController@uniTownhall')->name('uniTownhall')->middleware('saveUtm');
  Route::get('/universe/baba-mencari-bakat', 'PageController@uniBaba')->name('uniBaba')->middleware('saveUtm');

  // Routes for blogs
  Route::get('/blogs', 'user\blogController@blogPage')->name('blogPage');
  Route::get('/detail-blog/{id}', 'user\blogController@detailBlogPage')->name('detailBlogPage');


  // route login fb
  Route::group(['as' => 'login.', 'prefix' => 'login'], function () {
    Route::get('oAuth/process/{provider}', 'user\userController@redirectToFacebook')->name('process-oAuth');
    Route::get('oAuth/callback/{provider}', 'user\userController@handleFacebookCallback')->name('process-callback-oAuth');
    Route::get('oAuth/failed/{provider}', 'user\userController@handleFacebookFailed')->name('process-failed-oAuth');
  });

  // Route Landing Page Reseller
  Route::get('/l/reseller', 'ResellerController@landingReseller')->name('landingReseller');

  // Route Disclaimer
  Route::get('/disclaimer', 'PageController@disclaimerPage')->name('disclaimerPage');

  // Route Landing Page 9.9
  Route::get('/eventspesialharbolnas99', 'PageController@event99')->middleware('footprint')->name('event99');
  Route::post('/eventspesialharbolnas99', 'harbolnasController@store')->name('processEvent99');
  Route::get('/eventspesialharbolnas99/send-whatsapp/{orderId}', 'harbolnasController@whatsappRedirect')->name('whatsappRedirect');

  //Route Event 10.10
  Route::get('/lomba-makeup-total-10jt', 'PageController@eventMakeup')->middleware('footprint')->name('eventMakeup');
  Route::get('/harbolnas-10-10', 'PageController@event1010')->middleware('footprint')->name('event1010');

  Route::post('/user-makeup', 'harbolnasController@postMakeup')->name('user-makeup.post');
  Route::post('/user-harbolnas', 'harbolnasController@post1010')->name('user-harbolnas.post');

  //Route Event 11.11
  Route::get('/harbolnas-11-11', 'PageController@event11')->middleware('footprint')->name('event11');
  Route::get('/eventlandingpage-pilih-yang-benar', 'PageController@pilihBenar11')->middleware('footprint')->name('pilihBenar11');
  Route::get('/harbolnas-pilih-yang-benar', 'PageController@harbolnas11')->middleware('footprint')->name('harbolnas11');

  Route::post('/harbolnas-sebelas', 'HarbolnasSebelas@store')->name('postHarbolnas11');

  // Route Event Hari Anak
  Route::get('/hari-anak-sedunia-2021', 'PageController@hariAnak')->middleware('footprint')->name('hariAnak');

  // Route Event 12.12
  Route::get('/spesialharbolnas1212', 'PageController@harbolnas1212')->middleware('footprint')->name('harbolnas1212');
  Route::get('/game-show-desember', 'PageController@harbolnas1212v2')->middleware('footprint')->name('harbolnas1212v2');
  // Route::get('/spesialharbolnas1212/form', 'PageController@harbolnas1212form')->middleware(['footprint','harbolnas1212'])->name('harbolnas1212form');

  // Route Event Spesial Imlek
  Route::get('/special-event-imlek-2022', 'ImlekController@landingPage')->middleware('footprint')->name('imlekLandingPage');
  Route::get('/special-event-imlek-2022/order', 'ImlekController@orderPage')->middleware(['imlekMiddleware'])->name('imlekOrderPage');
  Route::get('/special-event-imlek-2022/payment', 'ImlekController@paymentPage')->middleware(['footprint', 'imlekMiddleware'])->name('imlekPaymentPage');
  Route::get('/special-event-imlek-2022/register', 'ImlekController@register')->middleware('footprint')->name('imlekRegisterProcess');
  Route::get('/special-event-imlek-2022/payment-status', 'ImlekController@paymentStatus')->middleware('footprint')->name('imlekPaymentStatusPage');
  Route::post('/special-event-imlek-2022/order', 'ImlekController@addToCart')->name('addToCartImlek');
  Route::post('/special-event-imlek-2022/create-invoice', 'ImlekController@createInvoice')->name('createInvoiceImlek');
  Route::get('/special-event-imlek-2022/resest-invoice', 'ImlekController@resetInvoice')->name('resetInvoiceImlek');
  Route::get('/special-event-imlek-2022/update-status', 'ImlekController@updateStatus')->name('updateStatusImlek');
  Route::get('/special-event-imlek-2022/recreate-invoice', 'ImlekController@recreateInvoice')->name('recreateInvoiceImlek');

  Route::get('/operlei', 'ImlekController@overlayImlek')->name('overlayImlek');

  Route::get('/register-imlek', 'ImlekController@registerQuizShow')->name('registerQuizShow');
  Route::get('/user-imlek-list', 'ImlekController@apiString')->name('apiOverlayImlek');
  Route::get('/user-imlek-nomor-urut', 'ImlekController@getNomerUrut')->name('userImlekNomorUrut');

  // Route For User Dashboard
  Route::middleware('login')->group(function () {
    Route::get('/dashboard', 'DashboardController@homeDash')->name('homeDash');
    Route::get('/coin', 'DashboardController@coinPage')->name('coinPage');
    Route::get('/submit-coin', 'DashboardController@submitCoinPage')->name('submitCoinPage');
    Route::get('/check-coin', 'DashboardController@checkCoinPage')->name('checkCoinPage');
    Route::get('/track-coin', 'DashboardController@trackCoinPage')->name('trackCoinPage');
    Route::get('/coin-code', 'DashboardController@getCoinCodePage')->name('getCoinCodePage');
    Route::get('/coin-done', 'DashboardController@doneCoinPage')->name('doneCoinPage');
    Route::get('/redeem-kupon', 'DashboardController@redeemCuponPage')->name('redeemCuponPage');
    Route::get('/done-redeem', 'DashboardController@doneRedeemPage')->name('doneRedeemPage');
    Route::get('/bawa-sobat', 'DashboardController@temanSobat')->name('temanSobat');
    Route::get('/teman-sobat', 'DashboardController@listTemanSobat')->name('listTemanSobat');
    Route::get('/tebak-kata', 'DashboardController@tebakKata')->name('tebakKata');
    Route::get('/tebak-kata/today-quiz', 'DashboardController@gameTebakKata')->name('gameTebakKata');
    Route::get('/rejeki-sobat', 'DashboardController@rejekiSobat')->name('rejekiSobat');
    Route::get('/rejeki-sobat/today-quiz', 'DashboardController@quizRejekiSobat')->name('quizRejekiSobat');
    Route::get('/rejeki-sobat/jawaban', 'DashboardController@rejekiSobatAns')->name('rejekiSobatAns');
    Route::get('/win', 'DashboardController@winPage')->name('winPage');
    Route::get('/tebak-kata/win', 'DashboardController@winTebakKata')->name('winPageTebakKata');
    Route::get('/lelang', 'DashboardController@auctionPage')->name('auctionPage');
    Route::post('/submit/hangman/{id}', 'user\gameController@satu_submit')->name('submitSatu');
    Route::get('/next-quiz/{id}', 'user\gameController@satu_reset')->name('nextQuiz');
    Route::get('/logout', 'user\userController@logout')->name('logout');
    Route::get('/handle-tebak-kata', 'DashboardController@handleTebakKata')->name('handleTebakKata');
    Route::get('/jadwal', 'DashboardController@jadwalPage')->name('jadwalPage');
    Route::post('/redeem', 'user\voucherController@redeem')->name('redeemVoucher');
    Route::post('/BID', 'user\auctionController@bid')->name('bid');
    Route::get('/avatar', 'user\userController@avatar')->name('avatar');
    Route::get('/get-riwayat', 'user\ajaxController@riwayat')->name('ajaxRiwayat');
    Route::get('/ajax-leaderboard', 'user\auctionController@refreshLeaderboard')->name('ajaxLeaderboard');
    Route::get('/jawaban-rejeki-sobat', 'dashboardController@jawabanRejekiSobat')->name('jawabanRejekiSobat');
    Route::post('/temp-tukar-koin', 'user\tukarKoinController@saveTemp')->name('tempTukar');
    Route::get('/auction-winner', 'user\auctionController@lastWinner')->name('lastWinner');
    Route::post('/tukar-koin', 'user\tukarKoinController@store')->name('storeTukarKoin');
    Route::get('/input-koin/{id}', 'user\tukarKoinController@inputKoin')->name('inputKoin');
    Route::get('/check-status-koin/{kode}', 'user\tukarKoinController@statusTukarKoin')->name('statusTukarKoin');
    Route::post('/edit-tukar-koin', 'user\tukarKoinController@editTukarKoin')->name('editTukarKoin');

    // FAQ
    Route::get('/faq-poin', 'DashboardController@faqPoinPage')->name('faqPoinPage');
    Route::get('/faq-auction', 'DashboardController@faqAuctionPage')->name('faqAuctionPage');
    Route::get('/faq-quiz', 'DashboardController@faqQuizPage')->name('faqQuizPage');

    // Edit profile
    Route::get('/edit-profile', 'DashboardController@editProfile')->name('editProfile');
    Route::post('/editProfileProcess', 'DashboardController@handleEditProfile')->name('handleEditProfile');

    // Ganti Password
    Route::get('/ganti-password', 'DashboardController@changePassword')->name('changePassword');
    Route::post('/changePasswordProcess', 'DashboardController@handleChangePassword')->name('handleChangePassword');

    /**Route search auction */
    Route::get('/search-auction', 'user\searchController@searchAuction')->name('searchAuction');
    Route::get('/search-lelang', 'user\searchController@searchLelang')->name('searchLelang');

    // Route Gacha
    Route::get('/tukar-invoice', 'GachaController@tukarInvoice')->name('tukarInvoice');
    Route::get('/gacha-ticket', 'GachaController@gachaTicket')->name('gachaTicket');

    // Winwheel
    Route::get('/choose-wheel', 'GachaController@chooseWheel')->name('chooseWheel');
    Route::get('/spin-it-yourself', 'GachaController@spinWheel')->name('spinWheel');

    Route::get('/comingsoon', 'DashboardController@comingSoonPage')->name('comingSoonPage');

    //Route process Gacha and Spinwheel
    Route::post('/tukar-invoice', 'user\gatchaController@checkInvoice')->name('checkInvoice');
    Route::post('/gacha-ticket', 'user\gatchaController@gachaTiket')->name('PostGachaTiket');
    Route::get('/gacha', 'user\gatchaController@gacha')->name('postGacha');
    Route::post('/spin-the-wheel', 'user\gatchaController@spinWheel')->name('postSpinwheel');

    //Route upload KTP
    Route::post('/upload-ktp', 'user\userController@uploadKTP')->name('uploadKTP');

    Route::get('/get-riwayat-spin-yourself', 'user\ajaxController@riwayatSitw')->name('ajaxRiwayatSitw');

    // POST BLOG
    Route::get('/post-blog', 'DashboardController@postBlog')->name('postBlog');
    Route::get('/user-blog', 'DashboardController@userBlog')->name('userBlog');
    Route::post('/post-blog', 'DashboardController@handlePost')->name('handlePost');
    Route::post('/like-blog', 'user\blogController@like')->name('blogLike');
    Route::post('/comment-blog', 'user\blogController@comment')->name('blogComment');
    Route::post('/load-comment', 'user\blogController@loadMoreComment')->name('loadMoreComment');
    Route::get('/get-riwayat-spin', 'user\ajaxController@riwayatSpin')->name('ajaxRiwayatSpin');

    // Route Bikin Quiz & Baba Mencari Bakat
    Route::get('/buat-quiz', 'DashboardController@bikinQuiz')->name('bikinQuiz');
    Route::get('/buat-quiz/success', 'DashboardController@successBikinQuiz')->name('successBikinQuiz');
    // Route::get('/baba-mencari-bakat', 'DashboardController@babaMencari')->name('babaMencari');
    // Route::get('/baba-mencari-bakat/status', 'DashboardController@babaSukses')->name('babaSukses');

    /**Route tebak kata user */
    Route::post('/post-tebak-kata', 'user\tebakKataUser@insert')->name('postTebakKata');
    /**Route post audition */
    Route::post('/post-audition', 'user\tebakKataUser@audition')->name('postAudition');

    /**Ajax get riwayat soal */
    Route::get('/riwayat-soal', 'user\ajaxController@riwayatSoal')->name('getRiwayatSoal');

    /**Ajax get slot tersisa baba mencari bakat */
    Route::get('/sisa-slot', 'user\ajaxController@sisaSlotBaba')->name('sisaSlotBaba');

    // Route Lomba 17an
    Route::get('/hut-ri-76-csb', 'LombaController@mainMenu')->name('mainMenu');
    Route::get('/panjat-pinang', 'LombaController@panjatPinang')->name('panjatPinang');
  //   Route::get('/panjat-pinang/leaderboard', 'LombaController@panjatLeaderboard')->name('panjatLeaderboard');
    Route::get('/panjat-pinang/game', 'LombaController@panjatPinangGame')->name('panjatPinangGame');
    Route::get('/m/panjat-pinang', 'LombaController@panjatPinangMobile')->name('panjatPinangMobile');
    Route::get('/tambal-ban', 'LombaController@tambalBan')->name('tambalBan');
    Route::get('/tambal-ban/start', 'LombaController@goTambal')->name('goTambal');
  //   Route::get('/leader/tambal', 'LombaController@leaderboardTambalBan')->name('leaderboardTambalBan');
  //   Route::get('/leaderboard-tambal-ban', 'LombaController@leaderboardTambalBan')->name('leaderboardTambalBan');
    Route::get('/mobile/tambal-ban', 'LombaController@tambalBanMobile')->name('tambalBanMobile');
    Route::get('/mobile/tambal-ban/start', 'LombaController@goBanMobile')->name('goBanMobile');

    // Hadiah
    Route::get('/hut-ri-76-csb/hadiah-panjat', 'LombaController@hadiahPanjat')->name('hadiahPanjat');
    Route::get('/hut-ri-76-csb/hadiah-pompa', 'LombaController@hadiahPompa')->name('hadiahPompa');

    /**Route Game Panjat pinang */
    Route::post('start-panjat-pinang', 'user\gameController@joinPanjatPinang')->name('joinPanjatPinang');
    Route::post('end-panjat-pinang', 'user\gameController@endPanjatPinang')->name('endPanjatPinang');
    Route::get('leaderboard-panjat-pinang', 'user\gameController@leaderboardPanjatPinang')->name('leaderboardPanjatPinang');

    /**Route Game Tambal Ban */
    Route::post('start-tambal-band', 'user\gameController@joinTambalBan')->name('joinTambalBan');
    Route::post('end-tambal-ban', 'user\gameController@endTambalBan')->name('endTambalBan');
    // Route::get('leaderboard-tambal-ban', 'user\LombaController@leaderboardTambalBan')->name('leaderboardTambalBan');

    /**Route pre register tujuh belasan */
    Route::get('pre-register', 'user\hutController@preRegister')->name('preRegisterHUTRI');
    Route::get('register-makan-krupuk', 'user\hutController@registerMakanKrupuk')->name('registerMakanKrupuk');
    Route::get('register-fashion-show', 'user\hutController@registerFashionShow')->name('registerFashionShow');
    Route::get('register-tebakan', 'user\hutController@registerTebakan')->name('registerTebakan');
    Route::get('register-tebak-lagu', 'user\hutController@registerTebakLagu')->name('registerTebakLagu');

    // Route Reseller
    Route::get('/reseller', 'ResellerController@homeReseller')->name('homeReseller');
    Route::get('/reseller/module', 'ResellerController@modulReseller')->name('modulReseller');
    Route::get('/reseller/module/id', 'ResellerController@modulDetailReseller')->name('modulDetailReseller');
    Route::get('/reseller/module/quiz', 'ResellerController@modulQuizReseller')->name('modulQuizReseller');
    Route::get('/reseller/module/quiz-done', 'ResellerController@modulQuizDone')->name('modulQuizDone');
    Route::get('/reseller/stempel', 'ResellerController@stempelReseller')->name('stempelReseller');
    Route::get('/reseller/leaderboard', 'ResellerController@leaderboardReseller')->name('leaderboardReseller');
    Route::get('/reseller/jadwal', 'ResellerController@jadwalReseller')->name('jadwalReseller');
    Route::get('/reseller/riwayat', 'ResellerController@riwayatReseller')->name('riwayatReseller');

    /**Route Special BId */
    Route::post('/special-bid', 'user\auctionController@bidSpecial')->name('bidSpecial');
    Route::get('/ajax-leaderboard-special', 'user\auctionController@refreshLeaderboardSpecial')->name('ajaxLeaderboardSpecial');
    Route::get('/auction-winner-special', 'user\auctionController@lastWinnerSpecial')->name('lastWinnerSpecial');
    Route::get('/mini-auction', 'SpecialAuctionController@specialAuction')->name('specialAuction');

    // Route Game Show
    Route::get('/quiz-show', 'QuizShowController@quizShow')->name('quizShow');
    Route::get('/quiz-show/tentukan-pilihanmu', 'QuizShowController@quizTp')->name('quizTp');
    Route::get('/quiz-show/apakah-kamu-tahu', 'QuizShowController@quizAkt')->name('quizAkt');
    Route::get('/quiz-show/siapa-yang-bisa-jawab', 'QuizShowController@quizSybj')->name('quizSybj');
    Route::get('/quiz-show/temukan-yang-sama', 'QuizShowController@quizTys')->name('quizTys');
    Route::get('/quiz-show/iklan-csb', 'QuizShowController@quizIc')->name('quizIc');
    Route::get('/quiz-show/pendapat-sobat', 'QuizShowController@quizPs')->name('quizPs');
    Route::get('/quiz-show/pilih-yang-benar', 'QuizShowController@quizPyb')->name('quizPyb');
    Route::get('/quiz-show/detail/{id}', 'QuizShowController@gameDetail')->name('gameDetail');
    Route::get('/quiz-show/riwayat', 'QuizShowController@riwayatQuiz')->name('riwayatQuiz');
    Route::get('/quiz-show/list-peserta/{id}', 'QuizShowController@listPeserta')->name('listPeserta');
    Route::get('/quiz-show/list-pemenang', 'QuizShowController@listPemenang')->name('listPemenang');
    Route::get('/quiz-show/list-belum-menang', 'QuizShowController@listBelumMain')->name('listBelumMain');

    Route::get('/quiz-show-imlek', 'ImlekController@imlekQuiz')->name('imlekQuiz');
    Route::get('/qsi', 'ImlekController@quizShowImlek')->name('quizShowImlek');
    Route::get('/quiz-show-special-imlek', 'ImlekController@quizShowImlekDetail')->name('quizShowImlekDetail');
    Route::get('/quiz-show-special-imlek/list-peserta/{id}', 'ImlekController@listPesertaImlek')->name('listPesertaImlek');

    Route::post('/search-pemenang', 'QuizShowController@searchPemenang')->name('searchPemenang');
    Route::post('/search-belum-menang', 'QuizShowController@searchBelumMenang')->name('searchBelumMenang');

    Route::get('/quiz-show/lispem', 'QuizShowController@lablistPemenang')->name('lablistPemenang');

    /** Route Surveys **/
    Route::post('/post-survey', 'SurveyController@store')->name('postSurvey');
    Route::get('/survey', 'SurveyController@index')->name('survey');

    // SPPA
    Route::get('/sppa', 'SppaController@getWelcome')->name('sppaWelcomePage');
    Route::get('/sppa-form', 'SppaController@getPageForm')->name('sppaFormPage');
    Route::get('/sppa-list', 'SppaController@sppaList')->name('sppaList');
    Route::get('/sppa-referensi',  'SppaController@sppaReferensi')->name('sppaReferensiPage');
    Route::post('/post-sppa', 'SppaController@postResult')->name('postSppa');
    Route::post('/post-video-sppa', 'SppaController@store')->name('postVideoSppa');
    Route::post('/vote-video-sppa', 'SppaController@vote')->name('voteVideoSppa');

    // Route Regional
    Route::middleware('joinRegional')->group(function () {

      Route::get('/csb-regional', 'RegionalController@listUserReg')->name('listUserReg');
      Route::get('/regional/tebak-kata', 'RegionalController@tebakKataRegional')->name('tebakKataRegional');
      Route::get('/regional/input-tebak-kata', 'RegionalController@inputTebakKataRegional')->name('inputTebakKataRegional');
      Route::get('/regional/tebak-kata-game', 'RegionalController@jawabTebakKataRegional')->name('jawabTebakKataRegional');

    });

    Route::get('/regional/pilih-regional', 'RegionalController@pilihRegional')->name('pilihRegional');
    Route::get('/regional/join-regional', 'RegionalController@daftarRegional')->name('daftarRegional');

    Route::get('/cities', 'RegionalController@getCity')->name('getCityData');
    Route::get('/subdistricts', 'RegionalController@getSubdistricts')->name('getSubdistricts');
    Route::post('/submit-regional', 'RegionalController@saveCity')->name('submitRegional');
    Route::get('/join-regional', 'RegionalController@joinRegional')->name('joinRegional');
    Route::post('/submit-tb-regional', 'RegionalController@submitTebakKataRegional')->name('submitTebakKataRegional');

    /** Game Tebak Kata Regional*/
    Route::post('/submit/regional/{id}', 'RegionalController@satu_submit')->name('submitSatuRegional');
    Route::get('/next-quiz/regional/{id}', 'RegionalController@satu_reset')->name('nextQuizRegional');
    /**Ajax get riwayat soal regional*/
    Route::get('/riwayat-soal-regional', 'RegionalController@riwayatSoal')->name('getRiwayatSoalRegional');

    /** WIn Page Tebak kata REgional*/
    Route::get('/tebak-kata-regional/win', 'RegionalController@winTebakKata')->name('winTebakKataRegional');

  });

  /**
   * Route process
   */

  // Route::post('/register','user\userController@register')->name('processRegister');
  // Route::post('/rejeki-sobat','user\quizController@rejekiSobat')->name('processQuiz');
  // Route::post('/login','user\userController@login')->name('processLogin');
  // Route::get('admin/input-koin/{id}','user\tukarKoinController@inputKoin')->name('inputKoin');
  // Route::get('/getdata/{time}','DataController@getdata')->name('getdata');
  // Route::post('/bp-koin-tidak-sesuai','user\tukarKoinController@bpKoinTidakSesuai')->name('bpKoinTidakSesuai');

  Route::post('/register', 'user\userController@register')->name('processRegister');
  Route::post('/rejeki-sobat', 'user\quizController@rejekiSobat')->name('processQuiz');
  Route::post('/login', 'user\userController@login')->name('processLogin');


  /**Route Lab */
  Route::prefix('lab')->group(function () {
    // Route::get('/import','importController@importPage')->name('importPage');
    // Route::post('/import','importController@importUsersNgabuburit')->name('importProcess');
    // Route::get('/transfer-data','importController@transferData')->name('transferData');
    // Route::get('/auction-test','importController@auctionTest')->name('auctionTest');
    // Route::get('/delete-spam','labController@deteksi_spam')->name('deteksiSpam');
    // Route::get('/delete-spam-harian','labController@deteksi_spam_harian')->name('deteksiSpamHarian');
    // Route::get('/delete-spam-manual','labController@spamManual')->name('deteksiSpamManual');
    // Route::get('/data-hari-pertama','labController@hariIni')->name('dataHariIni');
    Route::get('/spam-rejeki-sobat', 'labController@spamRejekiSobat')->name('spamRejekiSobat');
    // Route::get('/rahasia-user-poin-bos','labController@userPoin')->name('userPoinListLengkap');
    Route::get('/carbon-week', 'labController@carbonWeek');
    Route::get('/event-teman-sobat', 'labController@eventTemanSobat');
    Route::get('/leaderboard-insert', 'labController@leaderBoardBunda');
    Route::get('/insert-bp-harbolnas', 'labController@addBaperPoin');
    Route::get('/tsl', 'labController@temanSobatlab');
    Route::get('/mini-auc', 'labController@specialAuctionLab');
    Route::get('whatsapp-random', 'HarbolnasSebelas@randomWhatsapp');
    Route::get('/ha', 'labController@labHariAnak');
    Route::post('/process-upload-ktp', 'UploadKtpController@post')->name('processKTP');
    Route::get('/upload-ktp', 'labController@labKTP')->name('uploadKTP');
    Route::get('/12', 'labController@lab12')->name('lab12');
    Route::get('/12-form', 'labController@lab12form')->name('lab12form');
    Route::get('/12-games', 'labController@labGames')->name('labGames');
    Route::get('/12-rule', 'labController@labGamesRules')->name('labGamesRules');
    Route::get('/dashlab', 'labController@homeLab')->name('homeLab');

    Route::get('/labprofile', 'labController@labProfile')->name('labProfile');
    Route::post('/postlabprofile', 'labController@labhandleEditProfile')->name('labhandleEditProfile');
    Route::get('/sppa-list', 'labController@sppaList')->name('sppaListLab');
    Route::get('/sppa-form', 'labController@getPageForm')->name('sppaFormPageLab');
    Route::get('/sppa-welcome', 'labController@getWelcome')->name('sppaWelcomePageLab');
    ROute::get('/sppa-referensi',  'labController@sppaReferensi')->name('sppaReferensiPageLab');
    Route::post('/post-sppa', 'labController@postResult')->name('postSppaLab');
    Route::post('/post-video-sppa', 'SppaController@store')->name('postVideoSppaLab');

    Route::get('/rs', 'labController@labRejekiSobat')->name('labRejekiSobat');
    Route::post('/post-rs', 'labController@rejekiSobat')->name('labProcessQuiz');

    Route::get('/lp', 'labController@labLandingPage')->name('labLandingPage');

    Route::get('/qsi', 'labController@labQsi')->name('labQsi');
    Route::get('/qsilu/{id}', 'labController@labQsiLu')->name('labQsiLu');

    // Lab Korcab
    Route::get('/faqkorcab', 'labKorcabController@faqKorcab')->name('faqKorcab');



  });

  Route::get('/admin', function () {
    abort(404);
  });
  Route::get('/admin/login', function () {
    abort(404);
  });



  // ERROR ROUTES
  Route::get('/401', 'ErrorController@error401')->name('error401');
  Route::get('/403', 'ErrorController@error403')->name('error403');
  Route::get('/404', 'ErrorController@error404')->name('error404');
  Route::get('/405', 'ErrorController@error405')->name('error405');
  Route::get('/429', 'ErrorController@error429')->name('error429');
  Route::get('/500', 'ErrorController@error500')->name('error500');
  Route::get('/503', 'ErrorController@error503')->name('error503');

  //Public function join zoom
  Route::get('/join-zoom', 'zoomController@joinZoom')->name('joinZoom');
  Route::get('/join-zoom-imlek', 'ImlekController@joinZoom')->name('joinZoomImlek');
  Route::post('/spinwheel-poin', 'inputPoinController@spinwheelPoin')->name('spinwheelPoin');
  Route::get('/spinwheel-poin/cari', 'inputPoinController@searchEmail')->name('cariEmail');
  Route::get('/admin/undian', 'undianController@index')->name('undianHariIni')->middleware('admin');
  Route::get('/admin/get-undian', 'undianController@getData')->name('undianHariIniData')->middleware('admin');
  Route::get('/admin/riwayat-bp', 'adminController@index')->name('riwayatBP')->middleware('admin');
  Route::get('/admin/riwayat-bp-user', 'adminController@users')->name('riwayatBPUser')->middleware('admin');
  Route::get('/admin/riwayat-bp-user/{id}', 'adminController@userDetail')->name('riwayatBPuserDetail')->middleware('admin');

  Route::get('/admin/coin-management/{id}', 'DashboardController@coinManagementpage')->name('coinManagementpage');
  Route::get('calculate-bp/tidak-sesuai/{id}/{value}', 'adminController@calculateBPTidakSesuai')->name('calculateBPTidakSesuai');
  Route::get('/admin/riwayat-ticket-gacha', 'adminController@indexTicketGacha')->name('riwayatTicketGacha');
  Route::get('/admin/riwayat-ticket-gacha-user', 'adminController@usersTicketGacha')->name('riwayatTicketGachaUser');
  Route::get('/admin/riwayat-ticket-gacha-user/{id}', 'adminController@userTicketGachaDetail')->name('riwayatTicketGachaUserDetail');
  Route::get('/verifikasi-email', 'user\emailVerification@verifyEmail')->name('verifikasiEmail');
  Route::get('/send-verifikasi-email', 'user\emailVerification@sendEmailVerification')->name('sendVerifikasiEmail');

  Route::post('/submit-rumah', 'user\auctionController@submitRumah')->name('submitRumah');

  Route::get('/webinar', 'PageController@webinarPage')->name('webinarPage');

  Route::get('/z/tombol', 'PageController@tombol');
  Route::get('/z/meeting', 'PageController@meeting');


  /**Route spin by admin */
  Route::get('/admin-spin', 'GachaController@adminWheel')->name('adminWheel');
  Route::post('/spin-the-wheel-admin', 'user\gatchaController@spinWheelAdmin')->name('spinWheelAdmin')->middleware('admin');


  // Route Post-Quiz Bunda
  Route::get('/quiz-bunda', 'QuizBundaController@homeBunda')->name('quizBundaHome');
  Route::get('/quiz-bunda-hebat', 'QuizBundaController@postQuiz')->name('postQuizBunda');
  Route::get('/post-quiz-done', 'QuizBundaController@postQuizDone')->name('postQuizDone');
  Route::post('post-quiz-bunda', 'QuizBundaController@processQuiz')->name('processQuizBunda');
//   Route::get('/quiz-bunda-hebat/leaderboard', 'QuizBundaController@leaderboardBundaHebat')->name('leaderboardBundaHebat');
  Route::get('/quiz-bunda-hebat/leaderboard-1', 'QuizBundaController@leaderboardBunda1')->name('leaderboardBunda1');
  Route::get('/quiz-bunda-hebat/leaderboard-2', 'QuizBundaController@leaderboardBunda2')->name('leaderboardBunda2');

  /**Harbolnas 1212 controller */
  Route::post('save-kota', 'Harbolnas12Controller@saveKota')->name('saveKotaHarbolnas');
  Route::post('lab-save-kota', 'Harbolnas12Controller@labSaveKota')->name('labSaveKotaHarbolnas');
  Route::get('join-game/{eventId}', 'Harbolnas12Controller@joinGame')->name('joinGame');

});

Route::get('/maintenance', 'ErrorController@maintenance')->name('maintenance');

Route::get('lab/delete-session', 'labController@removeSession');

Route::get('lab/request-check', 'labController@requestCheck');

Route::get('lab/get-raja-ongkir', 'labController@getDataRajaOngkir');

Route::get('lab/seragamin-data-kota', 'labController@seragaminDataKota');
/** Badge lab*/
Route::get('lab/badge/{id}', 'labController@badgeUsers');
