<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v3')->group(function () {
  Route::post('/lab/wa/inbound','WhatsappInboundController@inbound')->name('labWaInbound');
  Route::post('harbolnas-12-whatsapp-inbound', 'Harbolnas12Controller@whatsappCallback')->name('whatsappCallbackHarbolnas');
});

Route::prefix('v1')->group(function () {
  Route::post('/like','blogController@like')->name('likeBlog');

  Route::post('/register-anak', 'RegisterAnakController@post');

  Route::post('/upload-ktp', 'UploadKtpController@post');
});
