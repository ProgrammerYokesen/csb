// POST SIGNAL -------------------------------
var imagePostSignal = $("#postUpload")
$(".post-uploaded").hide();
$('.file_remove').hide();

// remove choosed image
// resetting area 
$("#file_remove").on("click", function (e) { 
    $('.default-img').fadeIn().show();
    $('.file_remove').hide();
    $(".post-uploaded").empty()

    $("#uploaded_view").find(".uploaded-img").remove();
    $(".post-uploaded").empty()
    $(".post-process").removeClass("file_uploading"); 
    $('.post-uploaded').hide();
    $(".error_msg").text("");
    $(".post-process").addClass("file_uploading");
    $(".post_upload_text").show();
});

// handle choosing image  
// for drag and drop
$('.post_upload').on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
    e.preventDefault();
    e.stopPropagation();
})

$('.post_upload').on('drop', function (e) {
    e.preventDefault();
    e.stopPropagation();

    // get the file
    let droppedFiles = e.originalEvent.dataTransfer.files;

    // resetting area 
    $(".post-uploaded").empty()
    $(".post-process").removeClass("file_uploading");
    $(".post-process").show();
    $('.post-uploaded').hide();
    $(".error_msg").text("");
    $(".post-process").addClass("file_uploading");
    $(".post_upload_text").hide();

    // handle loading
    setTimeout(function () {
        $(".post-process").hide();
        $('.post-uploaded').show(); 
    }, 3000);

    // create url
    var uploadedFile = URL.createObjectURL(droppedFiles[0]);

    // show uploaded image 
    setTimeout(function () {
        $(".post-uploaded").append(`<img class="img-uploaded" alt="error" height="300px" src="${uploadedFile}" /><p class="image-name">${imagePostSignal.val()}</p>`).addClass("show");
        $('.file_remove').show();
    }, 3500);
});

// for clicked area
imagePostSignal.on("change", function (e) {
    // resetting area 
    $(".post-uploaded").empty()
    $(".post-process").removeClass("file_uploading");
    $(".post-process").show();
    $('.post-uploaded').hide();

    // validation
    var ext = imagePostSignal.val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $(".error_msg").html("Pastikan bahwa yang anda masukan adalah file gambar, <br> dengan ekstensi <b>'gif', 'png', 'jpg', 'jpeg'<b/>");
        $(".post_upload_text").hide();
    } else {
        // handle loading
        $(".error_msg").text("");
        $(".post-process").addClass("file_uploading");
        $(".post_upload_text").hide();

        setTimeout(function () {
            $(".post-process").hide();
            $('.post-uploaded').show();
        }, 3000);

        // create url
        var uploadedFile = URL.createObjectURL(e.target.files[0]);

        // show uploaded image 
        setTimeout(function () {
            $(".post-uploaded").append(`<img class="img-uploaded" alt="error" height="300px" src="${uploadedFile}" /><p class="image-name">${imagePostSignal.val()}</p>`).addClass("show");
            $('.file_remove').show();
        }, 3500);
    }
});