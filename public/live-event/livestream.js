"use strict";

// Class definition

var KTBootstrapNotifyDemo = function () {

    // Private functions

    // basic demo
    var demo = function () {
        // init bootstrap switch
        // $('[data-switch=true]').bootstrapSwitch();

        // handle the demo
        $('#kt_notify_btn').click(function() {
            var content = {};

            content.message = `<div class="order_detail">
                                    <p>Finley Khouwira</p>
                                    <p>INV2001/201/31241523***</p>
                                    <p>10 Bundle</p>
                                </div>`;

            content.title = `<div class="order_header">
                                <p>Order Baru</p>
                                <p>14:08</p>
                            </div>`;

            var notify = $.notify(content, {
                type: "danger", //Warna Background Toastr, primary = blibli, success = tokped, danger = shopee
                allow_dismiss: false, //Close Button
                newest_on_top: true,
                mouse_over:  true,
                showProgressbar: false,
                spacing: 10, //Jarak antar toast
                timer: 0, //Waktu Toastr Muncul
                placement: {
                    from: 'top',
                    align: 'right'
                },
                offset: {
                    x: 30,
                    y: 30
                },
                delay: 3000,
                z_index: 10000,
                animate: {
                    enter: 'animate__animated animate__' + 'bounceInRight',
                    exit: 'animate__animated animate__' + 'bounceInRight'
                }
            });

            if ($('#kt_notify_progress').prop('checked')) {
                setTimeout(function() {
                    notify.update('message', '<strong>Saving</strong> Page Data.');
                    notify.update('type', 'primary');
                    notify.update('progress', 20);
                }, 1000);

                setTimeout(function() {
                    notify.update('message', '<strong>Saving</strong> User Data.');
                    notify.update('type', 'warning');
                    notify.update('progress', 40);
                }, 2000);

                setTimeout(function() {
                    notify.update('message', '<strong>Saving</strong> Profile Data.');
                    notify.update('type', 'danger');
                    notify.update('progress', 65);
                }, 3000);

                setTimeout(function() {
                    notify.update('message', '<strong>Checking</strong> for errors.');
                    notify.update('type', 'success');
                    notify.update('progress', 100);
                }, 4000);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();

jQuery(document).ready(function() {
    KTBootstrapNotifyDemo.init();
});
